/*
 * FullEigenMatrix.hh
 *
 *  Created on: 17.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_BACKEND_FULLEIGENMATRIX_HH_
#define DUNE_RB_BACKEND_FULLEIGENMATRIX_HH_

#if HAVE_EIGEN

#include <fstream>
#include <iostream>
#include <boost/type_traits.hpp>
#include <Eigen/Dense>

namespace Dune
{
namespace RB
{
namespace Backend
{

/*
 *
 */
class FullEigenMatrix
{
public:

#ifdef ECLIPSE
  typedef int size_type;
#else
  typedef typename Eigen::MatrixXd::Index size_type;
#endif
public:
  FullEigenMatrix ();
  virtual ~FullEigenMatrix ();

  //! container construction unifies the constructor, unneeded so far...
  template< typename E = double >
  class Matrix: public Eigen::Matrix< E, Eigen::Dynamic, Eigen::Dynamic >
  {
  public:
    typedef Eigen::Matrix< E, Eigen::Dynamic, Eigen::Dynamic > EigenMatrixType;
#ifdef ECLIPSE
    typedef int size_type;
#else
    typedef typename EigenMatrixType::Index size_type;
#endif
    typedef E ElementType;
    typedef EigenMatrixType BaseT;
    typedef Eigen::Map<const BaseT> MappedType;
    typedef Backend::FullEigenMatrix Backend;

    //! construct container
    Matrix (int rows, int cols)
        : BaseT(rows, cols)
    {
    }

    //! set from element
    Matrix& operator= (const E& x)
    {
      this->fill(x);
      return *this;
    }

    //! for debugging and RB Access
    BaseT& base ()
    {
      return *this;
    }

    //! for debugging and RB access
    const BaseT& base () const
    {
      return *this;
    }
  };

  //==========================
  // individual element access
  //==========================

  //! get const_reference to container element
  /**
   * \note this method does not depend on T!
   */
  template< typename C >
  static const typename C::ElementType& access (const C& c, size_type row,
                                                size_type column)
  {
    return c(row, column);
  }

  //! get non const_reference to container element
  /**
   * \note this method does not depend on T!
   */
  template< typename C >
  static typename C::ElementType& access (C& c, size_type row, size_type column)
  {
    return c(row, column);
  }

  template< typename C >
  static void resize (const C & old, size_type rows, size_type cols, C & mnew)
  {
    mnew.resize(rows, cols);
    mnew.block(0, 0, old.rows(), old.cols()) = old;
  }

  template< typename C >
  static void wrapCMatrix (
      C & c, MatlabComm::SerializedArray::CMatrixWrapper *& cmatrix)
  {
    cmatrix = new MatlabComm::SerializedArray::CMatrixWrapper(c.data(),
        c.rows(), c.cols());

    assert(MatlabComm::SerializedArray::CMatrixWrapper::IsRowMajor == C::IsRowMajor);
  }

  template< typename C >
  static void wrapCMatrix (
      std::vector< C * > & c,
      std::vector< MatlabComm::SerializedArray::CMatrixWrapper * > & cmatrix)
  {
    assert(MatlabComm::SerializedArray::CMatrixWrapper::IsRowMajor == C::IsRowMajor);

    cmatrix.resize(c.size());
    for (unsigned int i = 0; i < c.size(); ++i)
    {
      cmatrix[i] = new MatlabComm::SerializedArray::CMatrixWrapper(c[i]->data(),
          c[i]->rows(), c[i]->cols());
    }
  }

  template<typename Map>
  static Map * mapToBaseType(const MatlabComm::SerializedArray::CMatrixWrapper & cmatrix)
  {
    assert(MatlabComm::SerializedArray::CMatrixWrapper::IsRowMajor == Map::IsRowMajor);

    return new Map(const_cast<double *>(cmatrix.data()), cmatrix.rows(), cmatrix.cols());
  }
};

} /* namespace Backend */
} /* namespace RB */
} /* namespace Dune */

#endif /* HAVE_EIGEN */

#endif /* DUNE_RB_BACKEND_FULLEIGENMATRIX_HH_ */
