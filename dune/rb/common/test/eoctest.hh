#ifndef EOCTEST_TS3V95II
#define EOCTEST_TS3V95II


#include <dune/fem/io/file/dataoutput.hh>

#include <vector>

#include <ctime>
#include <cmath>

#include "../../../../../matlab/rbsocks/rbserverinterface.hh"

#include "../../../../../common/test/utility.hh"
#include "../../../../../common/test/exactsolutions.hh"

namespace Dune {
namespace RBFem {
namespace Example
{

template<typename Traits>
struct EocTest
{
  typedef typename Traits :: ServerType                              ServerType;
  typedef RB::RBServerArgsTraits T;
  typedef typename T::ArrayType                                  ArrayType;
  typedef typename Traits :: ProblemType                             ProblemType;
  typedef typename ProblemType :: SolverType :: StepperType          StepperType;
  typedef typename StepperType :: DiscreteFunctionType               DiscreteFunctionType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridType             GridType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType         GridPartType;

  typedef typename Traits :: FacadeType                              RBMatlabFacade;
  typedef typename Traits :: DiscrType                               ImplementationType;
  typedef typename DiscreteFunctionType :: DomainType                DomainType;
  typedef typename DiscreteFunctionType :: RangeType                 RangeType;
  typedef double                                                     DomainFieldType;
  typedef double                                                     RangeFieldType;

  typedef tuple< DiscreteFunctionType* >                             IOTupleType;
  typedef DataOutput< GridType, IOTupleType >                        DataOutputType;

  EocTest()
    : dummy(RB::InitHelper::one(::std::string(getenv("DUNERBHOME"))+"/dunerbconvdiff.params")),
      linEvol(),
      rbMatIf(linEvol),
      discFuncSpace(linEvol.discFuncSpace()),
      gridPart(discFuncSpace.gridPart()),
      grid(gridPart.grid())
  {
  }

  void finish()
  {
    linEvol.clearReducedSpaceFiles();
    FemEoc::clear();
  }

  void run()
  {
    // count number of errors in program run
    int numErr=0;

    FemEoc::initialize("eoc", "Eoc Table");
    int eocId = FemEoc::addEntry(::std::string("L1-error"));
    int noEocSteps = Parameter::getValue<int>("test.eocSteps");

    errorVec.resize(noEocSteps);

    for (int eocStep=0; eocStep<noEocSteps; ++eocStep)
    {
      // call init model to get a grid, etc.
      ArrayType * retArray;
      linEvol.initModel(retArray);
      delete retArray;
      // call genModelData to compute base function norms
      linEvol.genModelData(retArray);
      delete retArray;

      // DiscreteFunctionType exactSolDiscr("Exact_Solution", discFuncSpace);
      DiscreteFunctionType u("u", discFuncSpace);

      ::std::ostringstream oss;
      oss << "Trajectory" << eocStep;

      // now, get a discrete function list ...
      FunctionList::Xdr<DiscreteFunctionType>
        trajectory(discFuncSpace, oss.str());
      // and perfom a detailed simulation
      // start time measurement
      double tstart = ::std::clock();
      linEvol.solveDetailed(trajectory);
      double took = ::std::clock() - tstart;     // end
      took = took/CLOCKS_PER_SEC;  // rescale to seconds

      int noTimesteps = linEvol.getNoTimeSteps();
      double timestepSize = linEvol.getDt();

/*      GrapeDataDisplay<GridType> grapeDisplay(grid);*/

      // for all timesteps, compare exact to discrete solution
      for (int timestep = 0; timestep != noTimesteps; ++timestep)
      {
        double time=timestep*timestepSize;

        // exactSol.setTime(time);
        // exactSolDiscr.clear();
        // l2pro(exactSol, exactSolDiscr);
        // if (Parameter::getValue<int>("test.dispExact"))
        //   dataOutputExact.write();
        u.clear();
        trajectory.getFunc(timestep, u);
        if(timestep == 0 || timestep == (int)(noTimesteps/2) || timestep==noTimesteps-1)
        {
/*          grapeDisplay.addData(u, false);*/

/*          exactSol.setTime(time);//Parameter::getValue<double>("rb.endTime"));*/
/*          DiscreteFunctionType exactSolDiscr("exact", discFuncSpace);*/
/*          exactSolDiscr.clear();*/
/*          L2Projection<DomainFieldType, RangeFieldType, ExactSolution, DiscreteFunctionType> l2pro;*/
/*          l2pro(exactSol, exactSolDiscr);*/
/*          grapeDisplay.addData(exactSolDiscr, false);*/

/*          grapeDisplay.display();*/
        }
        // if (Parameter::getValue<int>("test.dispU"))
        //   dataOutputU.write();
        // exactSolDiscr -= u;
        // if (Parameter::getValue<int>("test.dispDiff"))
        //   dataOutputExact.write();

        //double uNorm = linEvol.l1norm(u);
        if (timestep==noTimesteps-1) {

          double error = linEvol.errorToExact(u, time);

          errorVec[eocStep]=error;
          if (eocStep>0) {
            double eoc = log(errorVec[eocStep-1]/error)/log(2);
            if (eoc<=0) {
              numErr++;
              printf("Eoc is less than zero, expected greater than zero! Maybe 'rb.dtEst' needs to be adjusted!\n");
            }
            eocVec.push_back(eoc);
          }
          printf("dt: %f\n", timestepSize);
          if (eocStep==0 && error > 0.49) {
            printf("Initial error >0.49, expected <=0.49!\n");
            numErr++;
          }
          FemEoc::setErrors(eocId, error);
          FemEoc::write(GridWidth::calcGridWidth(gridPart),
                        grid.size(0),took,noTimesteps, ::std::cout);
        }
      } // loop over all timesteps

      // Refine the grid for the next EOC Step.
      if(eocStep < noEocSteps-1) {
        GlobalRefine :: apply(grid, DGFGridInfo<GridType>::refineStepsForHalf());
        ArrayType * retArray;
        linEvol.genModelData(retArray);
        delete retArray;
        if (eocStep>0)
        {
          double dtEstFactor = Parameter::getValue<double>("rb.dtEstFactor", 0.5);
          Parameter::replaceKey("rb.dtEst", dtEstFactor*timestepSize);
        }
      }
      trajectory.clear();
    } // eoc loop
  }

private:
  int                        dummy;
  ImplementationType         linEvol;
  RBMatlabFacade             rbMatIf;

  DiscreteFunctionSpaceType &discFuncSpace;
  GridPartType              &gridPart;
  GridType                  &grid;

public:
  ::std::vector<double>        errorVec;
  ::std::vector<double>        eocVec;
};

}  // namespace Examples
} // end of namespace RBFem
} // end of namespace Dune

#endif /* end of include guard: EOCTEST_TS3V95II */
