#ifndef EXACTSOLUTIONS_A84RWA2X
#define EXACTSOLUTIONS_A84RWA2X

#include <dune/common/fmatrix.hh>

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Data {

/* This is the exact solution to the problem @f[
   \partial_t u + a\partial_x u=0
   @f] in
   @f$\Omega = [0,1]\times [0,1]@f$ with @f$a=[1.25,0.8]@f$
   beeing the velocity. The top and right boundary are
   outflow boundaries, on the lower and left boundary
   the exact solution is given as dirichlet data. */
template< class DomainTypeImp >
class ExactTransportSolution {

  typedef DomainTypeImp DomainType;

public:
  typedef typename DomainType :: field_type DomainFieldType;
  typedef typename DomainType :: field_type RangeFieldType;

public:

  ExactTransportSolution() : velocity_(0)
  {
      // the velocity is a constant vector in this example
      velocity_[0] = Parameter::getValue<DomainFieldType>("rb.velocity_x");
      if(DomainType :: dimension > 1)
        velocity_[1] = Parameter::getValue<DomainFieldType>("rb.velocity_y");
      if(DomainType :: dimension > 2)
        velocity_[2] = Parameter::getValue<DomainFieldType>("rb.velocity_z");

  };

  /* evaluate the exact solution */
  template< class RangeType >
  void evaluate(const DomainType &xGlobal, double time, RangeType &ret) const {
    setTime(time);
    evaluate(xGlobal, ret);
  }

  template< class RangeType >
  void evaluate(const DomainType &xGlobal, RangeType &ret) const {
    DomainType velTimeProd = velocity_;
    velTimeProd *= time_;
    ret[0] = sin( 2*M_PI* (xGlobal - velTimeProd).two_norm2() );
  }

  void setTime(double time) const {time_=time;}

private:
  DomainType     velocity_;
  mutable double time_;

};


// The exact solution of the convection diffusion problem
template< class DomainTypeImp >
class ExactConvDiffSolution {

  typedef DomainTypeImp DomainType;

public:
  typedef typename DomainType :: field_type DomainFieldType;
  typedef typename DomainType :: field_type RangeFieldType;

  static const unsigned int dim=DomainType :: dimension;

public:

  ExactConvDiffSolution( )
    : velocity_(0), diffusion_(0)
  {
      // the velocity is a constant vector in this example
      velocity_[0] = Parameter::getValue<DomainFieldType>("rb.velocity_x");
      if(DomainType :: dimension > 1)
        velocity_[1] = Parameter::getValue<DomainFieldType>("rb.velocity_y");
      if(DomainType :: dimension > 2)
        velocity_[2] = Parameter::getValue<DomainFieldType>("rb.velocity_z");

      diffusion_ = Parameter::getValue<DomainFieldType>("rb.diffusion");

      c[0][0]=2.0;    c[0][1]=1.0;    c[0][2]=1.3;  // = c^1
      c[1][0]=0.7;    c[1][1]=0.5;    c[1][2]=0.1;  // = c^2
      c[2][0]=0.8;    c[2][1]=0.4;    c[2][2]=-0.4; // = \hat c^1
      c[3][0]=0.2;    c[3][1]=0.1;    c[3][2]=0.2;  // = \hat c^2
      c[4][0]=0.6;    c[4][1]=1.2;    c[4][2]=0.1;  // = \tilde c^1
      c[5][0]=0.9;    c[5][1]=0.3;    c[5][2]=-0.3; // = \tilde c^2
  }

  /* evaluate the exact solution */
  template< class RangeType >
  void evaluate(const DomainType &xGlobal, double time, RangeType &ret) const {
    setTime(time);
    evaluate(xGlobal, ret);
  }

  template< class RangeType >
  void evaluate(const DomainType &xGlobalArg, RangeType &ret) const
  {
    DomainType xGlobal(xGlobalArg);
    xGlobal -= DomainType(0.5);
    xGlobal.axpy(time_, velocity_);

    double sum = 0.0;
    for (int i=0; i!=dim-1; ++i) {
      double prod=1.0;
      for (unsigned int j = 0; j<dim; ++j)
      {
        double cosTerm = cos( c[i][j]*M_PI*( xGlobal[j] ) );
        double sinTerm = sin( c[i][j]*M_PI*( xGlobal[j] ) );

        prod *= ( c[4+i][j]*cosTerm + c[2+i][j]*sinTerm );
      } //loop over j
      prod *= exp( -1.0*diffusion_*time_*M_PI*M_PI* c[i].two_norm2() );
      sum += prod;
    } // loop over i
    ret[0] = sum;
  }

  void setTime(double time) const
  {
    time_=time;
  }

private:
  DomainType                       velocity_;
  double                           diffusion_;
  mutable double                   time_;
  FieldMatrix<double, 2*dim, dim>  c;
};

}  // namespace Data
}  // namespace Lib
} // end of namespace RBFem
} // end of namespace Dune

#endif /* end of include guard: EXACTSOLUTIONS_A84RWA2X */
