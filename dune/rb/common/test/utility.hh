#ifndef UTILITY_4HJ92BFH
#define UTILITY_4HJ92BFH

#warning "This file is deprecated.  Please use dune/helper-tools/common/mpi.hh instead!"
#include <dune/common/deprecated.hh>

namespace Dune {
namespace RB {

/**
  \deprecated Use Dune::HelperTools::Common::Mpi::InitHelper instead!
  **/
class InitHelper
{

public:
  static int one(const std::string & paramfile)
  {
    static InitHelper inithelper(paramfile);
    return 1;
  }

private:
  InitHelper(const std::string & paramfile)
  {
    int argc = 1;
    const char * argv1[] = { "test" };
    char ** argv = const_cast<char **>(&argv1[0]);
    MPIManager :: initialize( argc, argv );
    Dune::Parameter::append(paramfile);
  }
};

} // end of namespace Dune :: RB
} // end of namespace Dune

#endif /* end of include guard: UTILITY_4HJ92BFH */
