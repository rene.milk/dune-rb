/*
 * runtime_error.hh
 *
 *  Created on: 20.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_ERROR_HH_
#define DUNE_RB_ERROR_HH_

#include <string>
#include <sstream>
#include <exception>

namespace Dune
{
namespace RB
{

/*
 *
 */
class RuntimeError: public std::exception
{
public:
  /**
   * @brief constructor
   *
   * @param msg              a rough explanation of the exception that is thrown
   * @param canBeContinued   a flag indicating whether the connection can be
   *                         left open or needs to be reseted
   */
  RuntimeError(const std::string msg, bool canBeContinued = true)
     : msg_(msg), canBeContinued_(canBeContinued)
   {};

   //! copy constructor
   RuntimeError( const RuntimeError & error )
     : msg_(error.msg_), canBeContinued_(error.canBeContinued_)
   {}

  virtual ~RuntimeError () throw ()
  {
  }

  /**
   * @brief returns the error exception message
   *
   * @return  explanation of the exception
   */
  virtual const char* what () const throw ()
  {
    std::ostringstream oss;
    oss << "Dune-RB Runtime Error: " << msg_;
    if (!canBeContinued_)
    {
      oss
        << "\n\n********************************************************************\n";
      oss
        << "! It probably is safer to restart the Server now  .                !\n";
      oss
        << "********************************************************************\n";
    }
    return oss.str().c_str();
  }

  /**
   * @brief indicates whether the connection can be left open or needs to be
   * reseted
   *
   * @return  a boolean value
   */
  virtual bool canBeContinued () const
  {
    return canBeContinued_;
  }

private:
  std::string msg_;
  bool canBeContinued_;
};

} /* namespace RB */
} /* namespace Dune */
#endif /* DUNE_RB_ERROR_HH_ */
