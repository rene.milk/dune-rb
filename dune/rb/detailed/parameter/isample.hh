#ifndef DUNE_RB_DETAILED_PARAMETER_ISAMPLE
#define DUNE_RB_DETAILED_PARAMETER_ISAMPLE

#include <vector>
#include <map>
#include <string>
#include <dune/rb/matlabcomm/serializedarray/interface.hh>

namespace Dune
{

namespace RB
{

namespace Detailed
{

namespace Parameter
{

//forward declaration
class Vector;

class ISample
{
public:

  virtual ~ISample ()
  {
  }

  virtual const std::vector< std::string > & getMuNames () const = 0;

  virtual const std::vector< MatlabComm::SerializedArray::CMatrixWrapper * > getMuRanges () const = 0;

  virtual bool isValueInRange (int i, double value) const = 0;

  bool isVectorInRange (const Parameter::Vector & vec) const;

  virtual int size () const = 0;

  virtual Parameter::Vector get (const int i) const = 0;

  Parameter::Vector fromMatlab (
      const MatlabComm::SerializedArray::Interface<
          MatlabComm::SerializedArray::CMatrixWrapper > & array);

  void postprocessSymbolicCoefficient (std::string & str) const;
};
// class ISample

}// namespace Parameter

} // namespace Detailed

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_DETAILED_PARAMETER_ISAMPLE
