/*
 * nullsample.hh
 *
 *  Created on: 16.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_DETAILED_PARAMETER_NULLSAMPLE_HH_
#define DUNE_RB_DETAILED_PARAMETER_NULLSAMPLE_HH_

#include "isample.hh"
#include <dune/common/parametertree.hh>

namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Parameter
{

/*
 *
 */
class NullSample: public Dune::RB::Detailed::Parameter::ISample
{
public:

  NullSample (const Dune::ParameterTree & modelPT);

  NullSample (std::vector< std::string > muNames,
              std::vector< std::pair< double, double > > muRanges,
              const Dune::ParameterTree & modelPT);

  virtual ~NullSample ();

  virtual const std::vector< std::string > & getMuNames () const;

  virtual const std::vector< MatlabComm::SerializedArray::CMatrixWrapper * > getMuRanges () const;

  virtual bool isValueInRange (int i, double value) const;

  virtual int size () const;

  virtual Parameter::Vector get (const int i) const;

private:
  Dune::ParameterTree modelPT_;
  std::vector< std::string > muNames_;
  std::vector< std::pair< double, double > > muRanges_;
};

} /* namespace Parameter */
} /* namespace Detailed */
} /* namespace RB */
} /* namespace Dune */
#endif /* DUNE_RB_DETAILED_PARAMETER_NULLSAMPLE_HH_ */
