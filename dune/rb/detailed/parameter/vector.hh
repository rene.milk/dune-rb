#ifndef DUNE_RB_DETAILED_PARAMETER_VECTOR_
#define DUNE_RB_DETAILED_PARAMETER_VECTOR_

#include "isample.hh"
#include <cassert>
#include <map>
#include <limits>
#include <string>

namespace Dune
{

namespace RB
{

namespace Detailed
{

namespace Parameter
{

class Vector
{
private:
  typedef std::map< std::string, double > ParamMapType;
public:

  Vector ();

  Vector (const ISample& sample,
          const MatlabComm::SerializedArray::CMatrixWrapper & values);

  Vector (const ISample& sample, const std::vector< double > & values);

  void setValue (std::string id, double val);

  double getValue (std::string id, double def =
                     std::numeric_limits< double >::quiet_NaN()) const;

  bool isValid () const;

  void print (std::ostream& out) const;

  static Parameter::Vector fromMatlab (
      const ISample & ranges,
      MatlabComm::SerializedArray::Interface<
          MatlabComm::SerializedArray::CMatrixWrapper > & mu_values);

private:
  mutable std::map< std::string, double > parammap_;
  const Parameter::ISample * sample_;
};
// class Vector

}// namespace Parameter

} // namespace Detailed

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_DETAILED_PARAMETER_VECTOR_
