/*
 * isample.cc
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */

#include <config.h>
#include <dune/rb/runtime_error.hh>
#include <limits>
#include "vector.hh"
#include "isample.hh"

//#include <dune/common/parametertree.hh>
//#include "sstream"

namespace Dune
{

//template<>
//struct ParameterTree::Parser<std::pair<double, double> > {
//  static std::pair<double, double>
//  parse(const std::string& str) {
//
//    std::istringstream s(str);
//    double left, right;
//    char sep;
//    s >> left >> sep >> right;
//    if (sep != ':')
//    {
//      std::cerr << "Syntax error!\n" << std::endl;
//      throw(Dune::RB::RuntimeError("Syntax error while parsing for parameter ranges in configuration file!"));
//    }
//    std::pair<double, double> range;
//    range.first = left;
//    range.second = right;
//    return range;
//  }
//};

namespace RB
{

namespace Detailed
{

namespace Parameter
{

// implementation of ISample::isVectorInRange
bool ISample::isVectorInRange (const Parameter::Vector & vec) const
{
  const std::vector< std::string > & muNames = getMuNames();
  bool ok = true;
  for (unsigned int i = 0; i < muNames.size(); ++i)
  {
    double value = vec.getValue(muNames[i],
        (double) std::numeric_limits< double >::quiet_NaN());
    ok &= this->isValueInRange(i, value);
  }
  return ok;
}

Parameter::Vector ISample::fromMatlab (
    const MatlabComm::SerializedArray::Interface<
        MatlabComm::SerializedArray::CMatrixWrapper > & array)
{
  Parameter::Vector vec(*this, array.getMatrix());
  return vec;
}

void ISample::postprocessSymbolicCoefficient (std::string & str) const
{
  typedef std::string::size_type strsize_t;
  typedef std::map< std::string, std::string > MuArgMapType;
  MuArgMapType mu_arg_map;
  std::vector< std::string > mu_names = this->getMuNames();
  for (size_t i = 0; i < mu_names.size(); ++i)
  {
    std::ostringstream oss;
    oss << "mu(" << (i + 1) << ")";
    mu_arg_map.insert(std::make_pair(mu_names[i], oss.str()));
  }
  std::string cname("param.");
  strsize_t cur_pos = str.find(cname);
  while (cur_pos != std::string::npos)
  {
    // find end of token
    strsize_t end_pos = str.find_first_of("+-*/%()0123456789\t ;", cur_pos);
    strsize_t len = (
        end_pos == std::string::npos ? std::string::npos : (end_pos - cur_pos));
    std::string insert_text;
    typedef typename MuArgMapType::const_iterator MuArgMapItType;
    const std::string word = str.substr(cur_pos, len);
    MuArgMapItType it = mu_arg_map.find(word);
    if (it != mu_arg_map.end())
    {
      insert_text = (*it).second;
    }
    else
    {
      // TODO: Decide whether you want to use a ParameterTree or not...
      throw(Dune::RB::RuntimeError("In Parameter::ISample::postprocessSymbolicCoefficients: Is ParameterTree loaded correctly? Otherwise: THIS NEEDS TO BE IMPLEMENTED"));
//      std::ostringstream oss;
//      oss << Dune::Parameter::getValue< double >(word.c_str());
//      insert_text = oss.str();
    }
    str.erase(cur_pos, end_pos);
    str.insert(cur_pos, insert_text);
    cur_pos = str.find(cname, cur_pos + 1);
  }

  /*    str.insert(0, "@(mu) ");*/
}

} // namespace Parameter

} // namespace Detailed

} // namespace RB

} // namespace Dune
