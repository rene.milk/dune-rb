/*
 * vector.cc
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */

#include <config.h>
#include "vector.hh"
#include "nullsample.hh"
#include <dune/rb/runtime_error.hh>

namespace Dune
{

namespace RB
{

namespace Detailed
{

namespace Parameter
{

Vector::Vector ()
    : sample_(NULL)
{

}

Vector::Vector (const ISample& sample,
                const MatlabComm::SerializedArray::CMatrixWrapper & values)
    : sample_(&sample)
{
  const std::vector< std::string > & muNames = sample_->getMuNames();
  assert(muNames.size() == values.rows()*values.cols());
  const double * data = values.data();
  for (unsigned int i = 0; i < muNames.size(); ++i)
  {
    parammap_.insert(std::make_pair(muNames[i], data[i]));
  }
}

Vector::Vector (const ISample& sample, const std::vector< double > & values)
    : sample_(&sample)
{
  const std::vector< std::string > & muNames = sample_->getMuNames();
  assert(muNames.size() == values.size());
  for (unsigned int i = 0; i < muNames.size(); ++i)
  {
    parammap_.insert(std::make_pair(muNames[i], values[i]));
  }
}

void Vector::setValue (std::string id, double value)
{
  typedef ParamMapType::iterator IType;
  IType it = parammap_.find(id);
  if (it != parammap_.end())
  {
    parammap_[id] = value;
  }
  else
  {
    throw(Dune::RB::RuntimeError("Tried to set invalid id in parameter vector!"));
  }
}

double Vector::getValue (std::string id, double def) const
{
  typedef ParamMapType::const_iterator IType;
  IType it = parammap_.find(id);
  if (it != parammap_.end())
  {
    return (*it).second;
  }
  else
  {
#ifdef DEBUG
    std::cout << "Using default value " << def << " for id " << id << std::endl;
#endif
    // This ensures that only one default value is used per ID!
    parammap_.insert(std::make_pair(id, def));
    return def;
  }
}

bool Vector::isValid () const
{
  const Parameter::Vector & thisRef = *this;
  return (sample_ != NULL) && sample_->isVectorInRange(thisRef);
}

void Vector::print (std::ostream& out) const
{
  typedef ParamMapType::const_iterator IType;
  bool begin = true;
  out << "[ ";
  for (IType it = parammap_.begin(); it != parammap_.end(); ++it)
  {
    if (begin)
      begin = false;
    else
      out << ", ";
    out << "\"" << (*it).first << "\": ";
    out << (*it).second;
  }
  out << " ]";
}

Parameter::Vector fromMatlab (
    const ISample & ranges,
    MatlabComm::SerializedArray::Interface<
        MatlabComm::SerializedArray::CMatrixWrapper > & mu_values)
{
  MatlabComm::SerializedArray::CMatrixWrapper& cmatrix = mu_values.getMatrix();
  return Vector(ranges, cmatrix);
}

} // namespace Parameter

} // namespace Detailed

} // namespace RB

} // namespace Dune
