/*
 * nullsample.cc
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */

#include <config.h>
#include "vector.hh"
#include "nullsample.hh"

namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Parameter
{

NullSample::NullSample (std::vector< std::string > muNames,
                        std::vector< std::pair< double, double > > muRanges,
                        const Dune::ParameterTree & modelPT)
    : modelPT_(modelPT), muNames_(muNames), muRanges_(muRanges)
{
}

NullSample::NullSample (const Dune::ParameterTree & modelPT)
    : modelPT_(modelPT)
{
  std::vector<std::string> defaultMuNames;
  defaultMuNames.push_back("mu");
  muNames_ = modelPT_.get("mu_names", defaultMuNames);

  for (unsigned int i = 0; i < muNames_.size(); ++i)
  {
    std::vector<double> defaultMuRanges(2);
    defaultMuRanges[0] = 0.0;
    defaultMuRanges[1] = 1.0;
    std::vector<double> temp = modelPT_.get(std::string(muNames_[i]) + "_ranges", defaultMuRanges);
    muRanges_.push_back(std::pair<double, double>(temp[0], temp[1]));
  }
}

NullSample::~NullSample()
{
}

const std::vector< std::string > & NullSample::getMuNames () const
{
  return muNames_;
}

const std::vector< MatlabComm::SerializedArray::CMatrixWrapper * > NullSample::getMuRanges () const
{
  std::vector< MatlabComm::SerializedArray::CMatrixWrapper* > res(
      muRanges_.size());
  for (unsigned int i = 0; i < muRanges_.size(); ++i)
  {
    MatlabComm::SerializedArray::CMatrixWrapper * range =
      new MatlabComm::SerializedArray::CMatrixWrapper(1, 2);
    (*range)(0,0) = muRanges_[i].first;
    (*range)(0,1) = muRanges_[i].second;
    res[i] = range;
  }

  return res;
}

bool NullSample::isValueInRange (int i, double value) const
{
  return (value >= muRanges_[i].first && value <= muRanges_[i].second);
}

int NullSample::size () const
{
  return 0;
}

Parameter::Vector NullSample::get (const int i) const
{
  std::vector< double > values(muNames_.size(), 0.0);
  Parameter::Vector vec(*this, values);
  return vec;
}

} /* namespace Parameter */
} /* namespace Detailed */
} /* namespace RB */
} /* namespace Dune */

