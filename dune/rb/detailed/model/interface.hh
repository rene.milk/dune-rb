#ifndef __DUNE_RB_DETAILED_MODEL_INTERFACE_HH__
#define __DUNE_RB_DETAILED_MODEL_INTERFACE_HH__

namespace Dune{
namespace RB{
namespace Detailed{
namespace Model{
template< class ParameterImp >
class Interface
{
public:
  typedef ParameterImp ParameterType;

  const ParameterType& minParameter() const;
  const ParameterType& maxParameter() const;

}; // class Interface
} // namespace Model
} // namespace Detailed
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_MODEL_INTERFACE_HH__