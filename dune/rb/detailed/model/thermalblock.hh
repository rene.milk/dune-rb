#ifndef __DUNE_RB_DETAILED_MODEL_THERMALBLOCK_HH__
#define __DUNE_RB_DETAILED_MODEL_THERMALBLOCK_HH__

#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/la/separableparametric/function/interface.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Model {
template< class DomainType, class RangeType, class ParameterType >
class ThermalblockMobilityFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;

  ThermalblockMobilityFunction(const int mySize)
    : subdomainMarker_(Dune::Parameter::getValue< std::string >("model.parameterSubdomainPartitionFile").c_str()),
      size_(mySize)
  {
    subdomainMarker_.readSubdomains();
    if (size() != subdomainMarker_.numSubdomains())
      DUNE_THROW(IOError, "Parameter does not have right size for given thermalblock block file!");
    
    // set symbolic coefficients
    for (int i=0; i!=size(); ++i)
      symbolicCoefficients_.push_back("mu["+boost::lexical_cast<std::string>(i)+"]");
  }

  const int size() const
  {
    return size_;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    assert(i < size() && i >= 0);
    assert(size() == mu.size());

    return mu[i];
  }
  
  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }
  
  const std::string getVariable() const
  {
    return "mu";
  }

  const RangeType component(const int i, const DomainType& x) const
  {
    assert(i < subdomainMarker_.numSubdomains());

    RangeType lm(0.0);
    if (subdomainMarker_.getSubdomain(i).contains(x))
      lm[0] = 1.0;
    else
      lm[0] = 0.0;
    return lm;
  } // component

protected:
  Dune::RB::SubdomainMarker subdomainMarker_;
  const int                 size_;
  std::vector<std::string>  symbolicCoefficients_;
};

template< class DomainType, class RangeType, class ParameterType >
class ThermalblockDirichletValueFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  ThermalblockDirichletValueFunction()
  : symbolicCoefficients_(1, "1.0")
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    return 1.0;
  }
  
  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }

  const std::string getVariable() const
  {
    return "mu";
  }

  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType ret(0.0);

    return ret;
  }
  
private:
  std::vector<std::string> symbolicCoefficients_;
};

template< class DomainType, class RangeType, class ParameterType >
class ThermalblockNeumannValueFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  ThermalblockNeumannValueFunction()
    : symbolicCoefficients_(1, "1.0")
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    return 1.0;
  }
  
  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }

  const std::string getVariable() const
  {
    return "mu";
  }
  
  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType ret(0.0);

    if (fabs(x[1]) < 1e-18) {
      ret[0] = 1.0;
    } else
      ret[0] = 0.0;
    return ret;
  } // component
  
private:
    std::vector<std::string> symbolicCoefficients_;
};

template< class ParameterImp, class GridImp >
class Thermalblock
  : public Dune::RB::Detailed::Model::EllipticInterface< ParameterImp, GridImp >
{
  const static int dimRange = 1;
  typedef Thermalblock< ParameterImp, GridImp >      ThisType;
  typedef EllipticInterface< ParameterImp, GridImp > BaseType;
  typedef GridImp                                    GridType;

public:
  typedef ParameterImp                                                         ParameterType;
  typedef FieldVector< double, GRIDDIM >                                       DomainType;
  typedef FieldVector< double, dimRange >                                      RangeType;
  typedef FieldMatrix< double, DomainType::dimension, DomainType::dimension >            DiffusionMatrixType;

  typedef ThermalblockMobilityFunction< DomainType, RangeType, ParameterType > MobilityFunctionType;
  typedef ThermalblockDirichletValueFunction< DomainType, RangeType,
                                              ParameterType > DirichletValueFunctionType;
  typedef ThermalblockNeumannValueFunction< DomainType, RangeType,
                                            ParameterType > NeumannValueFunctionType;

  typedef typename BaseType::EntityType       EntityType;
  typedef typename BaseType::IntersectionType IntersectionType;


  Thermalblock()
    : mobilityFunction_(minParameter().rows())
  {}

  ParameterType minParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.minParameter");
  }

  ParameterType maxParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.maxParameter");
  }

  RangeType rightHandSide(const DomainType& x) const
  {
    return RangeType(1.0);
  }


  void permeability(const DomainType& x, const EntityType& e, Eigen::MatrixXd& k) const
  {
    k = Eigen::MatrixXd::Zero(2,2);
    k << 1, 0, 0, 1;
    return;
  }

  void permeability(const DomainType& x, const EntityType& e, Dune::FieldMatrix< double, GRIDDIM, GRIDDIM >& k) const
  {
    k       = 0.0;
    k[0][0] = 1.0;
    k[1][1] = 1.0;

    return;
  } // permeability


  MobilityFunctionType& mobility()
  {
    return mobilityFunction_;
  }

  const MobilityFunctionType& mobility() const
  {
    return mobilityFunction_;
  }

  DirichletValueFunctionType& dirichletValues()
  {
    return dirichletValueFunction_;
  }

  const DirichletValueFunctionType& dirichletValues() const
  {
    return dirichletValueFunction_;
  }

  NeumannValueFunctionType& neumannValues()
  {
    return neumannValueFunction_;
  }

  const NeumannValueFunctionType& neumannValues() const
  {
    return neumannValueFunction_;
  }


  // The following is necessary because YASP grid confuses my boundaries ids.
  /** Obtain the boundary type of a given boundary intersection.
   *
   *  @param[in] e Intersection whose boundary type shall be returned.
   *  @returns Returns the boundary type of the given intersection.
   */
  BoundaryType boundaryType(const IntersectionType& intersection) const
  {
    DomainType center = intersection.geometry().center();

    if ((center[1] < 1e-6) || (center[1] > 0.99999)) {
      return Dirichlet;
    } else {
      return Neumann;
    }
    // DUNE_THROW(NotImplemented, "The boundary ID you provided does not exist!");
  } // boundaryType


  void visualizePermeability(GridType& grid)
  {
    typedef GridImp                                                        GridType;
    typedef typename GridType::LeafGridView::template Codim< 0 >::Iterator IteratorType;
    typedef typename GridType::LeafGridView::template Codim< 0 >::Entity   EntityType;
    typedef FieldVector< double, GridType::dimension >                     DomainType;

    Eigen::VectorXd perm_x(grid.size(0));
    Eigen::VectorXd perm_y(grid.size(0));

    const IteratorType gridEnd = grid.leafView().template end< 0 >();
    for (IteratorType it = grid.leafView().template begin< 0 >(); it != gridEnd; ++it) {
      const EntityType& entity = *it;
      // get the barycenter
      DomainType local = Dune::GenericReferenceElements< typename GridType::ctype, GridType::dimension >::general(
        entity.type()).position(0, 0);

      Eigen::MatrixXd permeabilityMatrix;
      permeability(local, entity, permeabilityMatrix);
      int index = grid.leafView().indexSet().index(entity);
      perm_x(index) = permeabilityMatrix(0, 0);
      perm_y(index) = permeabilityMatrix(1, 1);
    }

    VTKWriter< typename GridType::LeafGridView > vtkwriter(grid.leafView());
    vtkwriter.addCellData(perm_x, "perm_x");
    vtkwriter.addCellData(perm_y, "perm_y");
    vtkwriter.write("vis/Permeability");

    return;
  }                 // visualizePermeability

private:
  MobilityFunctionType       mobilityFunction_;
  DirichletValueFunctionType dirichletValueFunction_;
  NeumannValueFunctionType   neumannValueFunction_;
};
}
}
}
}

#endif    // #ifndef __DUNE_RB_DETAILED_MODEL_THERMALBLOCK_HH__