#ifndef __DUNE_RB_DETAILED_MODEL_ELLIPTICINTERFACE_HH_
#define __DUNE_RB_DETAILED_MODEL_ELLIPTICINTERFACE_HH_

#include <dune/rb/detailed/model/interface.hh>

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/la/separableparametric/function/interface.hh>


namespace Dune {
namespace RB {
namespace Detailed {
namespace Model {
  
  enum BoundaryType { Dirichlet, Neumann };

template< class ParameterImp, class GridImp >
  class EllipticInterface : public Dune::RB::Detailed::Model::Interface<ParameterImp>
{
  typedef EllipticInterface< ParameterImp, GridImp > ThisType;
  
public:
  const static int dimRange = 1;
  typedef ParameterImp                                                           ParameterType;
  typedef GridImp                                                                GridType;
  typedef FieldVector< double, GRIDDIM >                                         DomainType;
  typedef FieldVector< double, dimRange >                                        RangeType;
  typedef FieldMatrix< double, DomainType::dimension, DomainType::dimension >              DiffusionMatrixType;
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType > MobilityFunctionType;
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType > DirichletValueFunctionType;
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType > NeumannValueFunctionType;
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType > RHSType;
  typedef typename GridType::template Codim<0>::Entity EntityType;
  typedef typename GridType::LeafIntersection IntersectionType;
  
  virtual ~EllipticInterface() {}

  virtual ParameterType minParameter() const = 0;

  virtual ParameterType maxParameter() const = 0;

  virtual RangeType rightHandSide(const DomainType& x) const = 0;

  virtual void permeability(const DomainType& x, const EntityType& e, Eigen::MatrixXd& k) const = 0;
  
  virtual void permeability(const DomainType& x, const EntityType& e, Dune::FieldMatrix< double, GRIDDIM, GRIDDIM >& k) const = 0;

  virtual MobilityFunctionType& mobility() = 0;

  virtual const MobilityFunctionType& mobility() const = 0;

  virtual DirichletValueFunctionType& dirichletValues() = 0;

  virtual const DirichletValueFunctionType& dirichletValues() const = 0;

  virtual NeumannValueFunctionType& neumannValues() = 0;

  virtual const NeumannValueFunctionType& neumannValues() const = 0;

  virtual BoundaryType boundaryType(const IntersectionType& intersection) const = 0;
  
  virtual void visualizePermeability(GridType& grid) = 0;
};         // class EllipticInterface
}         // namespace Model
}         // namespace Detailed
}         // namespace RB
}         // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_MODEL_ELLIPTICINTERFACE_HH_
