#ifndef __DUNE_RB_DETAILED_MODEL_SPE10_HH_
#define __DUNE_RB_DETAILED_MODEL_SPE10_HH_

#include <dune/rb/detailed/model/ellipticinterface.hh>

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/la/separableparametric/function/interface.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Model {
template< class DomainType, class RangeType, class ParameterType >
class Spe10MobilityFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;

  Spe10MobilityFunction(double dx, double dy, double dz, const int mySize)
    : x0_(0.0),
      epsilon_(0.0),
      alphaVec_(0.0),
  size_(mySize)
  {
    assert(DomainType::dimension == 3);

    int nx = 60;
    int ny = 220;
    int nz = 85;
    
    // x0_ is the point in the domain from which the distance for lambda is measured
    x0_[2]   = nz * dz;
    epsilon_ = (ny * dy * 0.75 / size());
    DomainType endOfDomain(0.0);
    endOfDomain[0] = nx * dx;
    endOfDomain[1] = ny * dy;
    endOfDomain   -= x0_;
    const double diagonalLengthOfDomain = endOfDomain.two_norm();
    for (int i = 0; i != size(); ++i) {
      alphaVec_.push_back(diagonalLengthOfDomain * i / (size()));
    }
    
    // set symbolic coefficients
    for (int i=0; i!=size(); ++i)
      symbolicCoefficients_.push_back("mu["+boost::lexical_cast<std::string>(i)+"]");
  }

  const int size() const
  {
    return size_;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    assert(i < size() && i >= 0);
    assert(size() == mu.rows());

    return mu[i];
  }

  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }
  
  const std::string getVariable() const
  {
    return "mu";
  }

  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType  lm(0.0);
    DomainType difference = x;

    difference -= x0_;
    double distanceToX0 = difference.two_norm();
    if ((distanceToX0 >= alphaVec_[i] - epsilon_) && (distanceToX0 <= alphaVec_[i] + epsilon_)) {
      double sinVal = sin(0.5 * M_PI * (distanceToX0 + epsilon_ - alphaVec_[i]) / epsilon_);
      lm[0] = 1 - sinVal * sinVal;
    } else {
      lm[0] = 1.0;
    }
    return lm;
  }         /* component */

private:
  DomainType            x0_;
  double                epsilon_;
  std::vector< double > alphaVec_;
  const int size_;
  std::vector<std::string> symbolicCoefficients_;
};

template< class DomainType, class RangeType, class ParameterType >
class Spe10DirichletValueFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  Spe10DirichletValueFunction()
  : symbolicCoefficients_(1, "1.0")
  {
  }

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    return 1.0;
  }

  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }
  
  const std::string getVariable() const
  {
    return "mu";
  }

  const RangeType component(const int i, const DomainType& x) const
  {
    return RangeType(10.0);
  }         /* component */
  
private:
  std::vector<std::string> symbolicCoefficients_;
};

template< class DomainType, class RangeType, class ParameterType >
class Spe10NeumannValueFunction
  : public LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
{
  typedef LA::Separableparametric::Function::Interface< DomainType, RangeType, ParameterType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  Spe10NeumannValueFunction()
    : symbolicCoefficients_(1, "1.0")
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterType& mu) const
  {
    return 1.0;
  }

  const std::vector< std::string >& getSymbolicCoefficients() const 
  {
    return symbolicCoefficients_;
  }
  
  const std::string getVariable() const
  {
    return "mu";
  }

  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType ret(0.0);

    if (abs(x[1]) < 1e-6)
      ret[0] = 1;
    return ret;
  }         // component
  
private:
  std::vector<std::string> symbolicCoefficients_;
};


template< class ParameterImp, class GridImp >
  class Spe10 : public Dune::RB::Detailed::Model::EllipticInterface< ParameterImp, GridImp >
{
  typedef Spe10< ParameterImp, GridImp > ThisType;
  typedef EllipticInterface< ParameterImp, GridImp > BaseType;
  typedef GridImp GridType;
public:
  const static int dimRange = 1;
  typedef typename BaseType::ParameterType                                       ParameterType;
  typedef FieldVector< double, GRIDDIM >                                         DomainType;
  typedef FieldVector< double, dimRange >                                        RangeType;
  typedef FieldMatrix< double, DomainType::dimension, DomainType::dimension >              DiffusionMatrixType;
  typedef Spe10MobilityFunction< DomainType, RangeType, ParameterType >          MobilityFunctionType;
  typedef Spe10DirichletValueFunction< DomainType, RangeType, ParameterType >    DirichletValueFunctionType;
  typedef Spe10NeumannValueFunction< DomainType, RangeType, ParameterType >      NeumannValueFunctionType;
  typedef typename BaseType::EntityType EntityType;
  typedef typename BaseType::IntersectionType IntersectionType;
  
  
  Spe10()
    : dx_(6.096),
      dy_(3.048),
      dz_(0.6096),
      mobilityFunction_(dx_, dy_, dz_, minParameter().rows())
  {
    readPermeability();
  }

  ~Spe10()
  {
    delete[] permeability_;
  }

  ParameterType minParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.minParameter");
  }

  ParameterType maxParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.maxParameter");
  }

  RangeType rightHandSide(const DomainType& x) const
  {
    return RangeType(0.0);
  }

  void permeability(const DomainType& x, const EntityType& e, Eigen::MatrixXd& k) const
  {
    k = Eigen::MatrixXd::Zero(3, 3);

    DomainType center = e.geometry().center();

    int xIntervall = std::floor(center[0] / dx_);
    int yIntervall = std::floor(center[1] / dy_);
    int zIntervall = std::floor(center[2] / dz_);

    const int offset = xIntervall + yIntervall * 60 + zIntervall * 220 * 60;
    k(0, 0) = permeability_[offset];
    k(1, 1) = permeability_[offset + 1122000];
    k(2, 2) = permeability_[offset + 2244000];
    return;
  }         // permeability

  void permeability(const DomainType& x, const EntityType& e, Dune::FieldMatrix< double, GRIDDIM, GRIDDIM >& k) const
  {
    k = 0.0;

    // get the barycenter
    DomainType center = e.geometry().center();

    int xIntervall = std::floor(center[0] / dx_);
    int yIntervall = std::floor(center[1] / dy_);
    int zIntervall = std::floor(center[2] / dz_);

    const int offset = xIntervall + yIntervall * 60 + zIntervall * 220 * 60;

    k[0][0] = permeability_[offset];
    k[1][1] = permeability_[offset + 1122000];
    k[2][2] = permeability_[offset + 2244000];

    return;
  }         /* permeability */

  MobilityFunctionType& mobility()
  {
    return mobilityFunction_;
  }

  const MobilityFunctionType& mobility() const
  {
    return mobilityFunction_;
  }

  DirichletValueFunctionType& dirichletValues()
  {
    return dirichletValueFunction_;
  }

  const DirichletValueFunctionType& dirichletValues() const
  {
    return dirichletValueFunction_;
  }

  NeumannValueFunctionType& neumannValues()
  {
    return neumannValueFunction_;
  }

  const NeumannValueFunctionType& neumannValues() const
  {
    return neumannValueFunction_;
  }


  BoundaryType boundaryType(const IntersectionType& intersection) const
  {
    const int boundaryID = intersection.boundaryId();

    if (boundaryID == 1) {
      return Dirichlet;
    } else if (boundaryID == 2) {
      return Neumann;
    }
    DUNE_THROW(NotImplemented, "The boundary ID you provided does not exist!");
  }         // boundaryType


  void visualizePermeability(GridType& grid)
  {
    typedef GridImp                                                        GridType;
    typedef typename GridType::LeafGridView::template Codim< 0 >::Iterator IteratorType;
    typedef typename GridType::LeafGridView::template Codim< 0 >::Entity   EntityType;
    typedef FieldVector< double, GridType::dimension >                     DomainType;

    Eigen::VectorXd perm_x(grid.size(0));
    Eigen::VectorXd perm_y(grid.size(0));
    Eigen::VectorXd perm_z(grid.size(0));

    const IteratorType gridEnd = grid.leafView().template end< 0 >();
    for (IteratorType it = grid.leafView().template begin< 0 >(); it != gridEnd; ++it) {
      const EntityType& entity = *it;
      // get the barycenter
      DomainType local
        = Dune::GenericReferenceElements< typename GridType::ctype, GridType::dimension >::general(
        entity.type()).position(0, 0);

      Eigen::MatrixXd permeabilityMatrix;
      permeability(local, entity, permeabilityMatrix);
      int index = grid.leafView().indexSet().index(entity);
      perm_x(index) = permeabilityMatrix(0, 0);
      perm_y(index) = permeabilityMatrix(1, 1);
      perm_z(index) = permeabilityMatrix(2, 2);
    }

    VTKWriter< typename GridType::LeafGridView > vtkwriter(grid.leafView());
    vtkwriter.addCellData(perm_x, "perm_x");
    vtkwriter.addCellData(perm_y, "perm_y");
    vtkwriter.addCellData(perm_z, "perm_z");
    vtkwriter.write("vis/Permeability");

    return;
  }         // visualizePermeability

private:
  void readPermeability()
  {
    permeability_ = new double[3366000];
    std::string   filename = Dune::Parameter::getValue< std::string >("model.permeabilityFile");
    std::ifstream file(filename.c_str());
    double        val;
    if (!file)                          // file couldn't be opened
      DUNE_THROW(IOError, "Data file for Groundwaterflow permeability could not be opened!");
    file >> val;
    int counter = 0;
    while (!file.eof()) {
      // keep reading until end-of-file
      permeability_[counter++] = val;
      file >> val;         // sets EOF flag if no value found
    }
    file.close();
    return;
  }         /* readPermeability */

  const double dx_;
  const double dy_;
  const double dz_;

  MobilityFunctionType       mobilityFunction_;
  DirichletValueFunctionType dirichletValueFunction_;
  NeumannValueFunctionType   neumannValueFunction_;

  double* permeability_;
};         // class Spe10
}         // namespace Model
}         // namespace Detailed
}         // namespace RB
}         // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_MODEL_SPE10_HH_