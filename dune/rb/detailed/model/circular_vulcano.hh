#ifndef __DUNE_RB_DETAILED_MODEL_CIRCULARVULCANO_HH_
#define __DUNE_RB_DETAILED_MODEL_CIRCULARVULCANO_HH_

#include <dune/rb/detailed/model/ellipticinterface.hh>

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Model {
template< class DomainImp, class RangeImp, class ParameterImp >
class VulcanoSolution
{
public:
  typedef DomainImp    DomainType;
  typedef RangeImp     RangeType;
  typedef ParameterImp ParameterType;
  typedef double RangeFieldType;

  VulcanoSolution(const double radius,
                  const ParameterType& param)
    : radius_(radius),
      param_(param)
  {
    assert(param_.rows() == 1);
  }

  void evaluate(const DomainType& x, RangeType& ret) const
  {
    DomainType offset(0.0);

    assert(offset.size() == 2);
    offset[0] = sin(param_(0) * radius_);
    offset[1] = cos(param_(0) * radius_);
    offset   -= x;
    ret[0]    = exp(-offset.two_norm());
    return;
  } // evaluate

  const double        radius_;
  const ParameterType param_;
};


template< class ParameterImp >
class CircularVulcano
{
public:
  const static int dimRange = 1;
  typedef ParameterImp                                            ParameterType;
  typedef FieldVector< double, GRIDDIM >                          DomainType;
  typedef FieldVector< double, dimRange >                         RangeType;
  typedef VulcanoSolution< DomainType, RangeType, ParameterType > ExactSolutionType;

  CircularVulcano() {}

  ParameterType minParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.minParameter");
  }

  ParameterType maxParameter() const
  {
    return Dune::Parameter::getValue< ParameterType >("model.maxParameter");
  }

  ExactSolutionType exactSolution(const ParameterType& param) const
  {
    double radius = Dune::Parameter::getValue< double >("model.radius");

    return ExactSolutionType(radius, param);
  }
};         // class CircularVulcano
}         // namespace Model
}         // namespace Detailed
}         // namespace RB
}         // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_MODEL_CIRCULARVULCANO_HH_