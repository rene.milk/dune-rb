#ifndef __DUNE_RB_DETAILED_MODEL_PROVIDER_3D_HH_
#define __DUNE_RB_DETAILED_MODEL_PROVIDER_3D_HH_

#include <dune/rb/detailed/model/ellipticinterface.hh>

#include <dune/rb/detailed/model/spe10.hh>
#include <dune/rb/detailed/model/thermalblock.hh>

#include <boost/algorithm/string.hpp>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Model {
namespace Provider {
template< class ParameterImp, class GridImp >
class Default
{
public:
  typedef ParameterImp                                 ParameterType;
  typedef GridImp                                      GridType;
  typedef EllipticInterface< ParameterType, GridType > ModelType;

  Default(std::string modelIdentifier)
  {
    // transform identifier to lower case
    boost::algorithm::to_lower(modelIdentifier);

    if (modelIdentifier == "spe10")
      model_ = new Dune::RB::Detailed::Model::Spe10< ParameterType, GridType >;
    else if (modelIdentifier == "thermalblock")
      model_ = new Dune::RB::Detailed::Model::Thermalblock< ParameterType, GridType >;
    else
      DUNE_THROW(RangeError, "Given model identifier is not known!");
  }

  ~Default()
  {
    if (model_ != NULL)
      delete model_;
  }

  ModelType& model()
  {
    return *model_;
  }

private:
  ModelType* model_;
};            // class Default
}         // namespace Provider
}         // namespace Model
}         // namespace Detailed
}         // namespace RB
}         // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_MODEL_PROVIDER_3D_HH_