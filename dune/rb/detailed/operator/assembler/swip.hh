#ifndef __DUNE_RB_DETAILED_OPERATOR_ASSEMBLER_SWIP_HH__
#define __DUNE_RB_DETAILED_OPERATOR_ASSEMBLER_SWIP_HH__

#include <dune/rb/rbasis/twophaseflow/helpers/progressbar.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Operator {
namespace Assembler {
template< class DiscreteFunctionSpaceImpl, class ModelImpl, class MatrixImpl, class VectorImpl >
class Swip
{
public:
  // ! type of discrete function space
  typedef DiscreteFunctionSpaceImpl DiscreteFunctionSpaceType;
  typedef ModelImpl                 ModelType;
  typedef MatrixImpl                MatrixType;
  typedef VectorImpl                VectorType;

  // ! type of grid part
  typedef typename DiscreteFunctionSpaceType::GridPartType              GridPartType;
  // ! type of grid
  typedef typename GridPartType::GridType                               GridType;
  typedef typename GridType::Traits::template Codim< 0 >::EntityPointer EntityPointer;
  typedef typename EntityPointer::Entity                                EntityType;
  typedef typename EntityType::Geometry                                 GeometryType;
  typedef typename DiscreteFunctionSpaceType::IteratorType              EntityIteratorType;
  typedef typename GridPartType::IntersectionIteratorType               IntIteratorType;
  typedef typename IntIteratorType::Intersection                        IntersectionType;
  // ! type of diffusion matrix
  typedef typename ModelType::DiffusionMatrixType                       DiffusionMatrixType;
  // ! field type of range
  typedef typename DiscreteFunctionSpaceType::RangeFieldType            RangeFieldType;
  // ! type of range
  typedef typename DiscreteFunctionSpaceType::RangeType                 RangeType;
  // ! type of domain
  typedef typename DiscreteFunctionSpaceType::DomainType                DomainType;
  // ! polynomial order of base functions
  enum { polynomialOrder = DiscreteFunctionSpaceType::polynomialOrder };
  // ! The grid's dimension
  enum { dimension = GridType::dimension };
  // We use a caching quadrature for codimension 0 entities
  typedef CachingQuadrature< GridPartType, 0 >                  QuadratureType;
  // ... and also for codimension 1 entities
  typedef CachingQuadrature< GridPartType, 1 >                  FaceQuadratureType;
  // ! type of jacobian
  typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

protected:
  typedef typename DiscreteFunctionSpaceType::BaseFunctionSetType BaseFunctionSetType;

public:
  // ! constructor
  Swip(const DiscreteFunctionSpaceType& dfSpace, const ModelType& model)
    : discFuncSpace_(dfSpace),
      gridPart_(discFuncSpace_.gridPart()),
      model_(model),
      gradCache_(dfSpace.mapper().maxNumDofs()),
      gradDiffusion_(dfSpace.mapper().maxNumDofs()),
      verbose_(Dune::Parameter::getValue< bool >("reducedoperator.verbose", false))
  {}

  ~Swip() {}

public:
  /** \brief perform a grid walkthrough and assemble the global matrices
   *  and the right hand side.  */
  void assemble(MatrixType& systemMatrix, VectorType& rhs)
  {
    const int numMobilityComps  = model_.mobility().size();
    const int numDirichletComps = model_.dirichletValues().size();
    const int numNeumannComps   = model_.neumannValues().size();
    const std::vector<std::string>& dirichletCoeffs = model_.dirichletValues().getSymbolicCoefficients();
    const std::vector<std::string>& neumannCoeffs = model_.neumannValues().getSymbolicCoefficients();
    const std::vector<std::string>& mobilityCoeffs = model_.mobility().getSymbolicCoefficients();
    assert(model_.mobility().getVariable()=="mu");
    assert(model_.dirichletValues().getVariable()=="mu");
    assert(model_.neumannValues().getVariable()=="mu");

    systemMatrix.reserve(numMobilityComps,
                         Eigen::VectorXi::Constant(systemMatrix.rows(), (GRIDDIM + 1) * ((2 * GRIDDIM) + 1)),
                         true);
    
    
    systemMatrix.setSymbolicCoefficients(mobilityCoeffs, "mu");
    
    rhs.reserve(numDirichletComps + numMobilityComps * numDirichletComps + numNeumannComps, true);

    std::vector<std::string> rhsSymbCoeffs(dirichletCoeffs);
    for (int gDComp=0; gDComp!=model_.dirichletValues().size(); ++gDComp)
      for (int mobilityComp=0; mobilityComp!=model_.mobility().size(); ++mobilityComp)
        rhsSymbCoeffs.push_back(dirichletCoeffs[gDComp]+"*"+mobilityCoeffs[mobilityComp]);
    
    rhsSymbCoeffs.insert(rhsSymbCoeffs.end(), neumannCoeffs.begin(), neumannCoeffs.end());
    rhs.setSymbolicCoefficients(rhsSymbCoeffs, "mu");

    /* apply local matrix assembler on each element, also computes
     *  boundary and intersection integrals*/
    boost::progress_display assemblyBar(this->discFuncSpace_.gridPart().grid().size(0));
    EntityIteratorType      gridEnd = this->discFuncSpace_.end();

    for (EntityIteratorType it = this->discFuncSpace_.begin(); it != gridEnd; ++it, ++assemblyBar) {
      assembleOp(*it, systemMatrix);

      // assemble right hand side and intersection parts of operator
      assembleRHS(*it, rhs);
    }

    return;
  }     /* assemble */

private:
  template< class JacobianRangeTypeVec >
  void jacobianAll(const EntityType& entity, const DomainType& quadPoint,
                   JacobianRangeTypeVec& jac) const
  {
    const BaseFunctionSetType& baseFunctionSet = this->discFuncSpace_.baseFunctionSet(entity);

    jac.resize(baseFunctionSet.numBaseFunctions());
    // get jacobian inverse transposed
    const typename EntityType::Geometry::Jacobian& inv = entity.geometry().jacobianInverseTransposed(quadPoint);
    baseFunctionSet.jacobianAll(quadPoint, inv, jac);
    return;
  } // jacobianAll

  template< class RangeTypeVec >
  void evaluateAll(const EntityType& entity, const DomainType& quadPoint,
                   RangeTypeVec& evals) const
  {
    const BaseFunctionSetType& baseFunctionSet = this->discFuncSpace_.baseFunctionSet(entity);

    evals.resize(baseFunctionSet.numBaseFunctions());
    baseFunctionSet.evaluateAll(quadPoint, evals);
    return;
  } // evaluateAll

  template< class RangeTypeVec >
  void evaluateJumpAll(const EntityType& inside, const EntityType& outside,
                       const DomainType& quadPointInInside,
                       const DomainType& quadPointInOutside,
                       RangeTypeVec& evals) const
  {
    if (this->discFuncSpace_.continuous()) {
      const int size = this->discFuncSpace_.baseFunctionSet(inside).numBaseFunctions();
      RangeType zero(0.0);
      evals = RangeTypeVec(size, zero);
    } else
      this->evaluateAll(inside, quadPointInInside, evals);
    return;
  } // evaluateJumpAll

  template< class RangeTypeVec >
  void evaluateMeanAll(const EntityType& inside, const EntityType& outside,
                       const IntersectionType& intersection,
                       const DomainType& quadPointInInside,
                       const DomainType& quadPointInOutside,
                       const int opComp,
                       const DomainType& normal,
                       RangeTypeVec& evals) const
  {
    DomainType xGlobal = inside.geometry().global(quadPointInInside);

    typename ModelType::DiffusionMatrixType Kinside;
    // evaluate diffusion matrix
    this->model_.permeability(quadPointInInside, inside, Kinside);
    RangeType                         lambdaInside = this->model_.mobility().component(opComp, xGlobal);
    std::vector< JacobianRangeType > insideGradientsTemp;
    this->jacobianAll(inside, quadPointInInside, insideGradientsTemp);
    std::vector< JacobianRangeType > insideGradients(insideGradientsTemp.size());
    evals.resize(insideGradientsTemp.size());

    // double weight = 0.5;

    // use weighted mean as supposed in Ern:2009uq
    // the inside and outside values of the permeability field
    Eigen::MatrixXd Ki;
    Eigen::MatrixXd Ko;
    this->model_.permeability(quadPointInInside, inside, Ki);
    this->model_.permeability(quadPointInOutside, outside, Ko);
    Eigen::Matrix< double, GRIDDIM, 1 > intersectionNormal;
    for (int i = 0; i != DomainType::dimension; ++i)
      intersectionNormal(i) = normal[i];

    // for definition of deltaMinus/deltaPlus see Ern:2009uq, page 5
    double deltaMinus = intersectionNormal.transpose() * Ki * intersectionNormal;
    double deltaPlus  = intersectionNormal.transpose() * Ko * intersectionNormal;

    double weight = deltaPlus / (deltaPlus + deltaMinus);
    // computation of weight done

    for (int i = 0; i != insideGradientsTemp.size(); ++i) {
      Kinside.mv(insideGradientsTemp[i][0], insideGradients[i][0]);
      evals[i][0]  = insideGradients[i][0] * normal;
      evals[i][0] *= weight * lambdaInside[0];
    }
    return;
  }     /* evaluateMeanAll */


  RangeType sigma(const DomainType& xLocalInside,
                  const DomainType& xLocalOutside,
                  const IntersectionType& intersection,
                  const DomainType& normal) const
  {
    RangeType         ret(0.0);
    EntityPointer     insidePtr = intersection.inside();
    const EntityType& insideEn  = *insidePtr;

    // Get the inside diffusion matrix
    typedef Eigen::MatrixXd EigenMatrixType;
    EigenMatrixType Kinside;
    model_.permeability(xLocalInside, insideEn, Kinside);

    Eigen::Matrix< double, GRIDDIM, 1 > intersectionNormal;
    for (int i = 0; i != DomainType::dimension; ++i)
      intersectionNormal(i) = normal[i];
    // for definition of deltaMinus/deltaPlus see Ern:2009uq, page 5
    double deltaMinus = intersectionNormal.transpose() * Kinside * intersectionNormal;

    if (intersection.boundary()) {
      ret[0] = deltaMinus;
    } else {
      EntityPointer     outsidePtr = intersection.outside();
      const EntityType& outsideEn  = *outsidePtr;
      // Get the outside diffusion matrix
      EigenMatrixType Koutside;
      model_.permeability(xLocalOutside, outsideEn, Koutside);
      double deltaPlus = intersectionNormal.transpose() * Koutside * intersectionNormal;
      ret[0] = deltaPlus * deltaMinus / (deltaPlus + deltaMinus);
    }
    ret[0] /= computeIntersectionDiameter(intersection);
    ret[0] *= 20;
    return ret;
  } // sigma

  double computeIntersectionDiameter(const IntersectionType& intersection) const
  {
    double diameter = 0.0;

    // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
    DomainType firstCorner = intersection.geometry().corner(0);
    for (int i = 1; i != GRIDDIM; ++i) {
      DomainType corner = intersection.geometry().corner(i);
      corner  -= firstCorner;
      diameter = std::max(diameter, corner.two_norm());
    }
#else // ifndef USE_CPGRID
    diameter = sqrt(intersection.geometry().volume());
#endif // ifndef USE_CPGRID

    return diameter;
  } // computeIntersectionDiameter

protected:
  // ! assemble local matrix for given entity
  void assembleOp(const EntityType& entity, MatrixType& systemMatrix)
  {
    typedef typename EntityType::Geometry Geometry;

    // assert that matrix is not build on ghost elements
    assert(entity.partitionType() != GhostEntity);

    // cache geometry of entity
    const Geometry& geometry = entity.geometry();

    // get number of local base functions
    // create quadrature of appropriate order
    const int      orderOfIntegrand = (polynomialOrder - 1) * 2 + polynomialOrder + 1;
    const int      quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
    QuadratureType quadrature(entity, quadOrder);

    // loop over all quadrature points
    const size_t numQuadraturePoints = quadrature.nop();
    for (size_t pt = 0; pt < numQuadraturePoints; ++pt) {
      // get local coordinate of quadrature point
      const typename QuadratureType::CoordinateType& x = quadrature.point(pt);

      DiffusionMatrixType K;
      // evaluate diffusion matrix
      model_.permeability(x, entity, K);

      this->jacobianAll(entity, quadrature.point(pt), gradCache_);
      const size_t numBaseFunctions = gradCache_.size();

      const int numOpComps = systemMatrix.numComponents();
      for (int opComp = 0; opComp != numOpComps; ++opComp) {
        // LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(entity, entity);

        // evaluate the current parameter-independent part of lambda
        DomainType xGlobal = geometry.global(x);
        RangeType lambda = model_.mobility().component(opComp, xGlobal);

        // apply diffusion tensor
        for (size_t i = 0; i < numBaseFunctions; ++i) {
          K.mv(gradCache_[i][0], gradDiffusion_[i][0]);
          gradDiffusion_[i][0] *= lambda[0];
        }
        // evaluate integration weight
        weight_ = quadrature.weight(pt) * geometry.integrationElement(x);

        for (unsigned int i = 0; i != numBaseFunctions; ++i)
          for (unsigned int j = 0; j != numBaseFunctions; ++j) {
            const int            row   = discFuncSpace_.mapper().mapToGlobal(entity, i);
            const int            col   = discFuncSpace_.mapper().mapToGlobal(entity, j);
            const RangeFieldType value = weight_ * (gradCache_[i][0] * gradDiffusion_[j][0]);
            // localMatrix.add(i, j, value);

            systemMatrix.component(opComp).coeffRef(row, col) += value;
          }

      } // iteration over all operator components
    } // iteration over all quad points

    // assemble intersections parts
    const int       insideIndex = gridPart_.grid().leafIndexSet().index(entity);
    IntIteratorType iend        = gridPart_.iend(entity);
    for (IntIteratorType iit = gridPart_.ibegin(entity); iit != iend; ++iit) {
      const IntersectionType& intersection = *iit;
      // dirichlet boundary parts
      if (intersection.boundary()) {
        if (model_.boundaryType(intersection) == Dune::RB::Detailed::Model::Dirichlet)
          assembleOpOnBoundary(intersection, systemMatrix);
      }
      // inner intersections
      else {
        EntityPointer     outsidePtr = intersection.outside();
        const EntityType& outsideEn  = *outsidePtr;
        // visit the intersection only once
        if (insideIndex > gridPart_.grid().leafIndexSet().index(outsideEn)) {
          assembleOpOnIntersections(intersection, systemMatrix);
        }
      }
    }
    return;
  } // assembleOp

  void assembleRHS(const EntityType& entity, VectorType& rhs) const
  {
    const typename DiscreteFunctionSpaceType::MapperType& mapper = discFuncSpace_.mapper();

    // obtain a reference to the entity's geometry
    const GeometryType& geometry         = entity.geometry();
    const int           orderOfIntegrand = (polynomialOrder + 1) + polynomialOrder;
    const int           quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
    QuadratureType      quadrature(entity, quadOrder);
    const size_t        numQuadraturePoints = quadrature.nop();
    // entity part
    for (size_t qP = 0; qP < numQuadraturePoints; ++qP) {
      // get integration element multiplied with quadrature weight
      const double factor
        = geometry.integrationElement(quadrature.point(qP)) * quadrature.weight(qP);
      // evaluate right hand side function
      DomainType xGlobal = geometry.global(quadrature.point(qP));
      RangeType  phis    = model_.rightHandSide(xGlobal);
      // apply factor
      phis *= factor;

      // evaluate base functions
      std::vector< RangeType > evals;
      this->evaluateAll(entity, quadrature.point(qP), evals);
      for (unsigned int i = 0; i != evals.size(); ++i) {
        double entityIntegral = evals[i] * phis;

        // get global index
        const int row = mapper.mapToGlobal(entity, i);
        rhs.parameterindependentPart() (row) += entityIntegral;
      }
    }
    // intersection, dirichlet-boundary part
    int       numGdComps = model_.dirichletValues().size();
    const int numOpComps = model_.mobility().size();

    // now: iterate over intersections
    IntIteratorType iend = gridPart_.iend(entity);
    for (IntIteratorType iit = gridPart_.ibegin(entity); iit != iend; ++iit) {
      const IntersectionType& intersection = *iit;

      // get the inside entity, its index and the enclosing coarse cell
      EntityPointer      insidePtr   = intersection.inside();
      const EntityType&  insideEn    = *insidePtr;
      const unsigned int insideIndex = gridPart_.grid().leafIndexSet().index(insideEn);

      // if we are on a dirichlet boundary intersection
      if (intersection.boundary() && (model_.boundaryType(intersection) == Dune::RB::Detailed::Model::Dirichlet)) {
        const int          orderOfIntegrand = (polynomialOrder - 1) + 2 * (polynomialOrder + 1);
        const int          quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
        FaceQuadratureType faceQuad(gridPart_, intersection,
                                    quadOrder, FaceQuadratureType::INSIDE);
        const size_t numIntQuadPoints = faceQuad.nop();
        // loop over all quadrature points
        for (unsigned int iqP = 0; iqP != numIntQuadPoints; ++iqP) {
          // get local coordinate of quadrature point
          const typename FaceQuadratureType::LocalCoordinateType& xLocal = faceQuad.localPoint(iqP);
          const typename IntersectionType::Geometry& inGeo               = intersection.geometry();


          // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
          const typename IntersectionType::Geometry& insideGeometry    = intersection.geometryInInside();
          const typename FaceQuadratureType::CoordinateType& xInInside = insideGeometry.global(xLocal);
          const typename FaceQuadratureType::CoordinateType& xGlobal   = inGeo.global(xLocal);
#else // ifndef USE_CPGRID
          typedef typename FaceQuadratureType::CoordinateType CoordinateType;
          const CoordinateType xGlobal   = intersection.geometry().center();
          const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(
            0,
            0);
#endif // ifndef USE_CPGRID

          const double factor = inGeo.integrationElement(xLocal) * faceQuad.weight(iqP);
          const RangeType sigmaVal = sigma(xInInside, xInInside, intersection, intersection.unitOuterNormal(xLocal));

          std::vector< RangeType > baseFuncEvals;
          this->evaluateAll(insideEn, xInInside, baseFuncEvals);
          // evaluate gradients of the inside base function set
          std::vector< JacobianRangeType > tempGradPhii;
          tempGradPhii.resize(baseFuncEvals.size());
          this->jacobianAll(insideEn, xInInside, tempGradPhii);
          for (int gdComp = 0; gdComp != numGdComps; ++gdComp) {
            RangeType gDPart = model_.dirichletValues().component(gdComp, xGlobal);

            // ======================== Assemble all c's for the rhs ======================
            // evaluate the boundary value function
            RangeType value = gDPart;
            // apply the factor
            value *= factor;
            value *= sigmaVal;

            // LocalFunctionType cLF = temp2.localFunction(entity);
            for (unsigned int i = 0; i != baseFuncEvals.size(); ++i) {
              // get global index
              const int row = mapper.mapToGlobal(entity, i);
              rhs.component(gdComp) (row) += value * baseFuncEvals[i];
              // cLF[i]+=value*baseFuncEvals[i];
            }
            // ========================  c's for the rhs done ======================

            // =========================Assemble all d's for the rhs =====================
            for (int opComp = 0; opComp != numOpComps; ++opComp) {
              DiffusionMatrixType K;
              // evaluate permeability matrix
              model_.permeability(xInInside, insideEn, K);
              std::vector< JacobianRangeType > gradients(tempGradPhii.size(), JacobianRangeType(0.0));
              // evaluate the current parameter-independent part of lambda
              RangeType lambda = model_.mobility().component(opComp, xGlobal);
              // apply diffusion tensor
              for (unsigned int i = 0; i < tempGradPhii.size(); ++i) {
                K.mv(tempGradPhii[i][0], gradients[i][0]);
                gradients[i][0] *= lambda[0];
                // get global index
                const int row = mapper.mapToGlobal(entity, i);
                rhs.component(numGdComps + gdComp * numOpComps + opComp) (row)
                  -= factor * gDPart[0] * (gradients[i][0] * intersection.unitOuterNormal(xLocal));
              }
              // ========================== d parts for the rhs done ================================
            } // iteration over mu all components
          } // loop over all g_d comps
        } // loop over all intersection quadpoints
      } // if dirichlet boundary
    }  // loop over intersection

    // assemble Neumann-part of rhs function
    int numGnComps = model_.neumannValues().size();
    for (int gnComp = 0; gnComp != numGnComps; ++gnComp) {
      // DiscreteFunctionType& temp = rhse_.getFunc(gnComp);
      // now: iterate over intersections
      IntIteratorType iend = gridPart_.iend(entity);
      for (IntIteratorType iit = gridPart_.ibegin(entity); iit != iend; ++iit) {
        const IntersectionType& intersection = *iit;
        // make shure we are on a neumann boundary intersection
        if (intersection.neighbor() || (model_.boundaryType(intersection) != Dune::RB::Detailed::Model::Neumann))
          continue;

        const int          orderOfIntegrand = (polynomialOrder + 1) + polynomialOrder;
        const int          quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
        FaceQuadratureType faceQuad(gridPart_, intersection,
                                    quadOrder, FaceQuadratureType::INSIDE);
        const size_t numIntQuadPoints = faceQuad.nop();
        // loop over all quadrature points
        for (unsigned int iqP = 0; iqP != numIntQuadPoints; ++iqP) {
          // get local coordinate of quadrature point
          const typename FaceQuadratureType::LocalCoordinateType& xLocal = faceQuad.localPoint(iqP);
          const typename IntersectionType::Geometry& inGeo               = intersection.geometry();


          // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
          const typename IntersectionType::Geometry& insideGeometry    = intersection.geometryInInside();
          const typename FaceQuadratureType::CoordinateType& xInInside = insideGeometry.global(xLocal);
          const typename FaceQuadratureType::CoordinateType& xGlobal   = inGeo.global(xLocal);
#else // ifndef USE_CPGRID
          typedef typename FaceQuadratureType::CoordinateType CoordinateType;
          const CoordinateType xGlobal   = intersection.geometry().center();
          const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(entity.type()).position(0, 0);
#endif // ifndef USE_CPGRID

          const double factor = inGeo.integrationElement(xLocal) * faceQuad.weight(iqP);

          std::vector< RangeType > baseFuncEvals;
          this->evaluateAll(entity, xInInside, baseFuncEvals);
          RangeType gNPart = model_.neumannValues().component(gnComp, xGlobal);

          // ======================== Assemble all c's for the rhs ======================
          // evaluate the boundary value function
          RangeType value = gNPart;
          // apply the factor
          value *= factor;
          // LocalFunctionType eLF = temp.localFunction(entity);
          for (unsigned int i = 0; i != baseFuncEvals.size(); ++i) {
            // get global index
            const int row = mapper.mapToGlobal(entity, i);
            rhs.component(numGdComps + numGdComps * numOpComps + gnComp) (row) += value * baseFuncEvals[i];
            // eLF[i]+=value*baseFuncEvals[i];
          }
          // ========================  e's for the rhs done ======================
        } // loop over all intersection quadpoints
      } // loop over intersections
    } // loop over all g_n comps
  }   // assembleRHS

  void assembleOpOnIntersections(const IntersectionType& intersection, MatrixType& systemMatrix)
  {
    typedef typename IntersectionType::GlobalCoordinate NormalVector;

    const typename DiscreteFunctionSpaceType::MapperType& mapper = discFuncSpace_.mapper();

    EntityPointer     insidePtr  = intersection.inside();
    const EntityType& insideEn   = *insidePtr;
    EntityPointer     outsidePtr = intersection.outside();
    const EntityType& outsideEn  = *outsidePtr;

    const int          orderOfIntegrand = 3 * polynomialOrder;
    const int          quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
    FaceQuadratureType faceQuad(gridPart_, intersection,
                                quadOrder, FaceQuadratureType::INSIDE);
    const size_t numIntQuadPoints = faceQuad.nop();
    for (unsigned int iqP = 0; iqP != numIntQuadPoints; ++iqP) {
      // get local coordinate of quadrature point
      const typename IntersectionType::Geometry& inGeo = intersection.geometry();
      const double intersectionVolume = inGeo.volume();
      const typename FaceQuadratureType::LocalCoordinateType& xLocal = faceQuad.localPoint(iqP);

      // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
      typedef typename FaceQuadratureType::CoordinateType CoordinateType;
      const CoordinateType xGlobal    = inGeo.global(xLocal);
      const CoordinateType xInInside  = intersection.geometryInInside().global(xLocal);
      const CoordinateType xInOutside = intersection.geometryInOutside().global(xLocal);
#else // ifndef USE_CPGRID
      typedef typename FaceQuadratureType::CoordinateType CoordinateType;
      const CoordinateType xGlobal    = intersection.geometry().center();
      const CoordinateType xInInside  = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(0, 0);
      const CoordinateType xInOutside = GenericReferenceElements< double, 3 >::general(outsideEn.type()).position(0, 0);
#endif // ifndef USE_CPGRID
      const NormalVector normal = intersection.unitOuterNormal(xLocal);
      const double       factor = inGeo.integrationElement(xLocal) * faceQuad.weight(iqP);
      const RangeType sigmaVal = sigma(xInInside, xInOutside, intersection, normal);
      
      std::vector< RangeType > jumpPhii;
      std::vector< RangeType > jumpPhij;

      this->evaluateJumpAll(insideEn, outsideEn, xInInside, xInOutside, jumpPhii);
      this->evaluateJumpAll(outsideEn, insideEn, xInOutside, xInInside, jumpPhij);

      const int jumpPhiiSize = jumpPhii.size();
      const int jumpPhijSize = jumpPhij.size();

      // iterate over all components
      const int numOpComps = model_.mobility().size();
      for (int opComp = 0; opComp != numOpComps; ++opComp) {
        std::vector< RangeType > meanPhii;
        std::vector< RangeType > meanPhij;
        this->evaluateMeanAll(insideEn, outsideEn, intersection, xInInside, xInOutside, opComp, normal, meanPhii);
        this->evaluateMeanAll(outsideEn, insideEn, intersection,
                              xInOutside, xInInside, opComp, normal, meanPhij);

        // update the parameter dependent operator
        double value = 0.0;

        // first: terms in B^\nu that concern functions of both coarse elements
        for (int i = 0; i != jumpPhiiSize; ++i) {
          const int row = mapper.mapToGlobal(insideEn, i);
          for (int j = 0; j != jumpPhijSize; ++j) {
            const int col = mapper.mapToGlobal(outsideEn, j);
            // negative because of the jump, together with minus in operator sum we
            // have a plus
            value                                              = factor * jumpPhij[j][0] * meanPhii[i];
            systemMatrix.component(opComp).coeffRef(row, col) += value;
            // the transposed part (B^\nu)^\top
            systemMatrix.component(opComp).coeffRef(col, row) += value;

            // this time, the jump brings positive sign, thus we get a minus
            value                                              = -1.0 * factor * jumpPhii[i][0] * meanPhij[j];
            systemMatrix.component(opComp).coeffRef(row, col) += value;
            // the transposed part (B^\nu)^\top
            systemMatrix.component(opComp).coeffRef(col, row) += value;
          }
        }

        // second: terms that concern only functions on the inside coarse element
        for (int i = 0; i != jumpPhiiSize; ++i) {
          const int row = mapper.mapToGlobal(insideEn, i);
          for (int ii = 0; ii != jumpPhiiSize; ++ii) {
            const int col = mapper.mapToGlobal(insideEn, ii);
            // minus because the jump gives a plus, together with the minus in the
            // operator sum we have a minus
            value                                              = -1.0 * factor * jumpPhii[ii][0] * meanPhii[i];
            systemMatrix.component(opComp).coeffRef(row, col) += value;
            // the transposed part (B^\nu)^\top
            systemMatrix.component(opComp).coeffRef(col, row) += value;
          }
        }
        // third: terms that concern only functions on the outside coarse element
        for (int j = 0; j != jumpPhijSize; ++j) {
          const int row = mapper.mapToGlobal(outsideEn, j);
          for (int jj = 0; jj != jumpPhijSize; ++jj) {
            const int col = mapper.mapToGlobal(outsideEn, jj);
            // plus because the jump gives a minus, together with the minus in the
            // operator sum we have a plus
            value                                              = factor * jumpPhij[jj][0] * meanPhij[j];
            systemMatrix.component(opComp).coeffRef(row, col) += value;
            // the transposed part (B^\nu)^\top
            systemMatrix.component(opComp).coeffRef(col, row) += value;
          }
        }
      } // loop over all components
        // assemble C
      double value = 0.0;
      for (int i = 0; i != jumpPhiiSize; ++i) {
        const int row = mapper.mapToGlobal(insideEn, i);
        // first: all terms that concern only functions with support in
        // the inside coarse element
        for (int ii = 0; ii <= i; ++ii) {
          const int col = mapper.mapToGlobal(insideEn, ii);
          value = factor * sigmaVal * jumpPhii[i][0] * jumpPhii[ii][0];
          systemMatrix.parameterindependentPart().coeffRef(row, col) += value;

          // C is symmetric
          if (i != ii) {
            systemMatrix.parameterindependentPart().coeffRef(col, row) += value;
          }
        }
        // second: mixed terms
        for (int j = 0; j != jumpPhijSize; ++j) {
          const int col = mapper.mapToGlobal(outsideEn, j);
          value
            = -1.0* factor* sigmaVal * jumpPhii[i][0] * jumpPhij[j][0];
          systemMatrix.parameterindependentPart().coeffRef(row, col) += value;

          // C is symmetric
          if (row != col) {
            systemMatrix.parameterindependentPart().coeffRef(col, row) += value;
          }
        }
      }

      // third: terms that concern only functions with support in
      // the outside coarse element
      for (int j = 0; j != jumpPhijSize; ++j) {
        const int row = mapper.mapToGlobal(outsideEn, j);
        for (int jj = 0; jj <= j; ++jj) {
          const int col = mapper.mapToGlobal(outsideEn, jj);
          value = factor * sigmaVal * jumpPhij[j][0] * jumpPhij[jj][0];
          systemMatrix.parameterindependentPart().coeffRef(row, col) += value;

          // C is symmetric
          if (row != col) {
            systemMatrix.parameterindependentPart().coeffRef(col, row) += value;
          }
        }
      }
      // = C done ====================================
    } // loop over all quad points
  }   // void assembleOnIntersections

  void assembleOpOnBoundary(const IntersectionType& intersection, MatrixType& systemMatrix)
  {
    typedef typename IntersectionType::GlobalCoordinate NormalVector;

    const typename DiscreteFunctionSpaceType::MapperType& mapper = discFuncSpace_.mapper();

    EntityPointer     insidePtr = intersection.inside();
    const EntityType& insideEn  = *insidePtr;

    const int          orderOfIntegrand = 3 * polynomialOrder;
    const int          quadOrder        = std::ceil((orderOfIntegrand + 1) / 2);
    FaceQuadratureType faceQuad(gridPart_, intersection,
                                quadOrder, FaceQuadratureType::INSIDE);
    const size_t numIntQuadPoints = faceQuad.nop();
    for (unsigned int iqP = 0; iqP != numIntQuadPoints; ++iqP) {
      // get local coordinate of quadrature point
      const typename IntersectionType::Geometry& inGeo = intersection.geometry();
      const double intersectionVolume = inGeo.volume();
      const typename FaceQuadratureType::LocalCoordinateType& xLocal = faceQuad.localPoint(iqP);
      // ! \todo check if it makes a difference to use faceQuad.point(iqP) here
      typedef typename FaceQuadratureType::CoordinateType CoordinateType;

      // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
      const CoordinateType xGlobal   = inGeo.global(xLocal);
      const CoordinateType xInInside = intersection.geometryInInside().global(xLocal);
#else // ifndef USE_CPGRID
      // const int faceIndexInInside = intersection.indexInInside();
      // const int insideEntityIndex = gridPart_.grid().leafIndexSet().index(insideEn);
      // int globalFaceIndex = gridPart_.grid().cellFace(insideEntityIndex, faceIndexInInside);

      // ! \todo check if it makes a difference to use faceQuad.point(iqP) here
      typedef typename FaceQuadratureType::CoordinateType CoordinateType;
      const CoordinateType xGlobal   = intersection.geometry().center();
      const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(0, 0);
#endif // ifndef USE_CPGRID

      const NormalVector normal = intersection.unitOuterNormal(xLocal);
      const double       factor = inGeo.integrationElement(xLocal) * faceQuad.weight(iqP);
      const RangeType sigmaVal = sigma(xInInside, xInInside, intersection, normal);
      
      DiffusionMatrixType K;
      // evaluate diffusion matrix
      model_.permeability(xInInside, insideEn, K);

      std::vector< RangeType >         phii;
      std::vector< JacobianRangeType > gradPhii;
      std::vector< JacobianRangeType > tempGradPhii;

      this->evaluateAll(insideEn, xInInside, phii);
      this->jacobianAll(insideEn, xInInside, tempGradPhii);

      gradPhii.resize(tempGradPhii.size());

      const int phiiSize = phii.size();

      // iterate over all components
      const int numOpComps = model_.mobility().size();
      for (int opComp = 0; opComp != numOpComps; ++opComp) {
        // evaluate the current parameter-independent part of lambda
        RangeType lambda = model_.mobility().component(opComp, xGlobal);
        // apply diffusion tensor to gradPhii...
        for (int i = 0; i < phiiSize; ++i) {
          K.mv(tempGradPhii[i][0], gradPhii[i][0]);
          gradPhii[i][0] *= lambda[0];
        }

        // update the parameter dependent operator
        double value = 0.0;
        for (int i = 0; i != phiiSize; ++i) {
          const int row = mapper.mapToGlobal(insideEn, i);
          for (int ii = 0; ii != phiiSize; ++ii) {
            const int col = mapper.mapToGlobal(insideEn, ii);
            // minus because the jump gives a plus, together with the minus in the
            // operator sum we have a minus
            value                                              = -1.0 * factor * phii[ii][0] * (gradPhii[i][0] * normal);
            systemMatrix.component(opComp).coeffRef(row, col) += value;
            // the transposed part (B^\nu)^\top
            systemMatrix.component(opComp).coeffRef(col, row) += value;
          }
        }
      } // loop over all components
        // assemble C
      double           value = 0.0;
      for (int i = 0; i != phiiSize; ++i) {
        const int row = mapper.mapToGlobal(insideEn, i);
        for (int ii = 0; ii <= i; ++ii) {
          const int col = mapper.mapToGlobal(insideEn, ii);
          value = factor * sigmaVal * phii[i][0]
                  * phii[ii][0];
          systemMatrix.parameterindependentPart().coeffRef(row, col) += value;

          // C is symmetric
          if (row != col) {
            systemMatrix.parameterindependentPart().coeffRef(col, row) += value;
          }
        }
      }
      // = C done ====================================
    } // loop over all quad points
  }   // void assembleOpOnBoundary

protected:
  const DiscreteFunctionSpaceType& discFuncSpace_;
  const GridPartType&              gridPart_;
  const ModelType&                 model_;

  mutable std::vector< JacobianRangeType > gradCache_;
  mutable std::vector< JacobianRangeType > gradDiffusion_;
  mutable RangeFieldType                   weight_;

  const bool verbose_;
};
}
}
}
}
}
#endif // __DUNE_RB_DETAILED_OPERATOR_ASSEMBLER_SWIP_HH__
