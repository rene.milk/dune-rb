/*
 * laplaceccfv.hh
 *
 *  Created on: 16.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACECCFV__HH_
#define DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACECCFV__HH_

#include "../interface.hh"
#include <boost/filesystem.hpp>
#include <dune/common/parametertree.hh>
#include <dune/rb/runtime_error.hh>
#include <dune/rb/la/separableparametric/icontainer.hh>
#include <dune/rb/detailed/parameter/vector.hh>

#include <dune/rb/examples/utility/unitcube.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/pdelab/finiteelementmap/p0fem.hh>

#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/constraints.hh>

#include <dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
#include <dune/pdelab/localoperator/laplacedirichletccfv.hh>
#include <dune/pdelab/localoperator/l2.hh>

#include <dune/pdelab/eigenvectorbackend.hh>
#include <dune/pdelab/eigenmatrixbackend.hh>
#include <dune/pdelab/eigensolverbackend.hh>

#include "localoperator.hh"

template< typename GV, typename RF >
class BoundaryFunction: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits< GV, RF, 1 >,
    BoundaryFunction< GV, RF > >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits< GV, RF, 1 > Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase< Traits,
      BoundaryFunction< GV, RF > > BaseT;

  BoundaryFunction (const GV& gv)
      : BaseT(gv)
  {
  }
  inline void evaluateGlobal (const typename Traits::DomainType& x,
                              typename Traits::RangeType& y) const
  {
//    typename Traits::DomainType center;
//    for (int i = 0; i < GV::dimension; i++)
//      center[i] = 0.5;
//    center -= x;
//    y = exp(-2 * center.two_norm2());
    y = 0;
  }
};


namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Solver
{
namespace Connector
{

template< class GridType >
struct LaplaceCCFVTraits
{
  typedef typename GridType::LeafGridView GV;
  typedef typename GV::Grid::ctype DomainFieldType;
  typedef double RangeFieldType;
  static const int dim = GV::dimension;
  typedef Dune::PDELab::P0LocalFiniteElementMap< DomainFieldType,
      RangeFieldType, dim > FEM;
  typedef Dune::PDELab::NoConstraints ConstraintsType;
  typedef Dune::PDELab::EigenVectorBackend VectorBackendType;
  typedef Dune::PDELab::SparseEigenMatrixBackend SparseMatrixBackendType;
  typedef Dune::PDELab::SimpleGridFunctionStaticSize GFStaticSizeType;

  typedef Dune::PDELab::GridFunctionSpace< GV, FEM, ConstraintsType,
      VectorBackendType, GFStaticSizeType > GFS;

  typedef BoundaryFunction< GV, RangeFieldType > BoundaryFunctionType;

  typedef LaplaceDirichletCCFV::LocalOperator< BoundaryFunctionType > LocalOperatorType;

  typedef Dune::PDELab::GridOperatorSpace< GFS, GFS, LocalOperatorType,
      Dune::PDELab::EmptyTransformation, Dune::PDELab::EmptyTransformation,
      SparseMatrixBackendType > GOS;

  typedef typename GOS::template MatrixContainer< RangeFieldType >::Type MatrixContainerType;
  typedef typename MatrixContainerType::BaseT MatrixType;
  typedef typename GFS::template VectorContainer< RangeFieldType >::Type VectorContainerType;
  typedef typename VectorContainerType::BaseT VectorType;

  typedef Dune::PDELab::DiscreteGridFunction< GFS, VectorType > DiscreteGridFunctionType;

  typedef Dune::RB::LA::SeparableParametric::IContainer< RangeFieldType,
      typename MatrixContainerType::BaseT > SeparableMatrixType;
  typedef Dune::RB::LA::SeparableParametric::IContainer< RangeFieldType,
      typename VectorContainerType::BaseT > SeparableVectorType;
  typedef Dune::NotImplemented MapperType;
};

template< class BasicTraits >
struct L2FVTraits: public BasicTraits
{
  typedef Dune::PDELab::L2 LocalOperatorType;
  typedef Dune::PDELab::GridOperatorSpace< typename BasicTraits::GFS,
      typename BasicTraits::GFS, Dune::PDELab::L2,
      Dune::PDELab::EmptyTransformation, Dune::PDELab::EmptyTransformation,
      typename BasicTraits::SparseMatrixBackendType > GOS;

  typedef typename BasicTraits :: RangeFieldType RangeFieldType;
  typedef typename BasicTraits :: GFS GFS;

  typedef typename GOS::template MatrixContainer< RangeFieldType >::Type MatrixContainerType;
  typedef typename MatrixContainerType::BaseT MatrixType;
  typedef typename GFS::template VectorContainer< RangeFieldType >::Type VectorContainerType;
  typedef typename VectorContainerType::BaseT VectorType;
};

// forward declarations

template< class T > class SeparableLaplaceOperator;
template< class T > class SeparableLinearOperator;
template< class T > class SeparableLaplaceResidual;

/*
 *
 */
class LaplaceCCFV: public Dune::RB::Detailed::Solver::Connector::Interface<
    LaplaceCCFVTraits< Dune::YaspGrid< 2 > >, LaplaceCCFV >
{
public:
  typedef Dune::YaspGrid< 2 > GridType;
  typedef LaplaceCCFVTraits< GridType > BasicTraits;

  typedef typename GridType::LeafGridView GV;
  typedef typename BasicTraits::GFS GFS;

  typedef typename BasicTraits::VectorContainerType VectorContainerType;
  typedef typename BasicTraits::SeparableMatrixType SeparableMatrixType;
  typedef typename BasicTraits::SeparableVectorType SeparableVectorType;

public:
  LaplaceCCFV (Dune::ParameterTree & modelPT);

  virtual ~LaplaceCCFV ();

  bool init ();

  bool isInitialized() const;

  bool solve (const Parameter::Vector & mu,
              typename BasicTraits::VectorType & solution) const;

  bool visualize (const typename BasicTraits::VectorType & dofs,
                  std::string prefix, std::string cellArrayName) const;

  SeparableMatrixType & getSystemMatrix (std::string identifier) const;

  SeparableVectorType & getSystemVector (std::string identifier) const;

  const std::string getVtkPath () const
  {
    boost::filesystem::path vtkPath("vtk");
    if (!boost::filesystem::exists(vtkPath))
      boost::filesystem::create_directories(vtkPath);
    return vtkPath.string();
  }

  const GFS & getGridFunctionSpace () const
  {
    const GV & gv = getGridView();
    if ( gridFunctionSpace_ != NULL )
    {
      const GV & gvTemp1 = gridFunctionSpace_->gridview();

      if (&gvTemp1 != &gv)
      {
        delete gridFunctionSpace_;
        gridFunctionSpace_ = NULL;
      }
    }

    if ( gridFunctionSpace_ == NULL )
      gridFunctionSpace_ = new typename BasicTraits::GFS(gv, fem_);

    return *gridFunctionSpace_;
  }

  const GV & getGridView () const
  {
    const int maxLevel = grid_.maxLevel();
    if (gridView_ != NULL)
    {
      if (maxLevel != lastMaxLevel_)
      {
        delete gridView_;
        gridView_ = NULL;
      }
    }
    if (gridView_ == NULL)
    {
      gridView_ = new GV(grid_.leafView());
      lastMaxLevel_ = maxLevel;
    }

    return *gridView_;
  }

  const Dune::ParameterTree & getModelParameterTree() const
  {
    return modelPT_;
  }
private:
  Dune::RB::Examples::UnitCube< GridType, 1 > unitcube_;
  GridType & grid_;
  mutable const GV * gridView_;
  mutable int lastMaxLevel_;
  typename BasicTraits::FEM fem_;
  mutable GFS * gridFunctionSpace_;

  bool init_;

  SeparableLaplaceOperator< BasicTraits > * separableLaplace_;
  SeparableLinearOperator< L2FVTraits< BasicTraits > > * separableL2_;
  SeparableLaplaceResidual< BasicTraits > * separableRHS_;

  Dune::ParameterTree & modelPT_;
};

template< class T >
class SeparableLinearOperator:
    public Dune::RB::LA::SeparableParametric::DefaultMatrixContainer< T >
{
  typedef Dune::RB::LA::SeparableParametric::DefaultMatrixContainer< T > BaseType;
public:
  SeparableLinearOperator (LaplaceCCFV & connector, const std::string identifier)
      : connector_(connector), identifier_(identifier)
  {
  }

  virtual ~SeparableLinearOperator ()
  {
  }

  virtual unsigned int numComponents () const
  {
    return 1;
  }

  virtual typename T::RangeFieldType coefficient (
      const unsigned int q, const Parameter::Vector &mu) const
  {
    switch (q)
    {
    case 0:
      return 1;
      break;
    default:
      throw(Dune::RB::RuntimeError("wrong number of coefficients!"));
    }
  }

  virtual std::vector< std::string > symbolicCoefficients () const
  {
    std::vector< std::string > symbCoeffs(1);
    symbCoeffs[0] = "1";
    return symbCoeffs;
  }

  virtual const std::string getIdentifier() const
  {
    return identifier_;
  }

private:
  virtual typename T::MatrixContainerType * generate_component (
      const unsigned int q) const
  {
    assert(q==0);

    typename T::VectorContainerType x0(connector_.getGridFunctionSpace());

    x0 = 0.0;

    typename T::LocalOperatorType lop;

    typename T::GOS gos(connector_.getGridFunctionSpace(),
        connector_.getGridFunctionSpace(), lop);
    typename T::MatrixContainerType * matrix =
      new typename T::MatrixContainerType(gos);
    typename T::MatrixContainerType & m = *matrix;
    m = 0.0;
    gos.jacobian(x0, m);

    return matrix;
  }

private:
  LaplaceCCFV & connector_;
  const std::string identifier_;
};


} /* namespace Connector */
} /* namespace Solver */
} /* namespace Detailed */
} /* namespace RB */
} /* namespace Dune */

#endif /* DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACECCFV__HH_ */
