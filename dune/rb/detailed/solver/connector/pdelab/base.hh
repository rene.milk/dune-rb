/*
 * Base.hh
 *
 *  Created on: Mar 20, 2012
 *      Author: martin
 */

#ifndef BASE_HH_
#define BASE_HH_

#include <dune/rb/la/separableparametric/icontainer.hh>

namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Solver
{
namespace Connector
{
namespace PDELab
{

/*
 *
 */
template<class ModelImp, class GridOperatorSpaceImp>
class Base
{
public:
  Base (const ModelImp & model)
    : model_(model)
  {
  }

  virtual ~Base ()
  {
  }

  void init();

  template<class ParamType>
  const LA::SeparableParametric::IContainer & getSystemMatrix(ParamType& param)
  {

  }



  private:
  ModelImp model_;
};

} /* namespace PDELab */
} /* namespace Connector */
} /* namespace Solver */
} /* namespace Detailed */
} /* namespace RB */
} /* namespace Dune */
#endif /* BASE_HH_ */
