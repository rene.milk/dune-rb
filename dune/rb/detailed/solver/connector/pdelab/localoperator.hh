// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACEDIRICHLETCCFV_LOCALOPERATOR_HH
#define DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACEDIRICHLETCCFV_LOCALOPERATOR_HH

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/grid/common/genericreferenceelements.hh>

#include <dune/pdelab/localoperator/defaultimp.hh>

#include <dune/pdelab/common/geometrywrapper.hh>
#include <dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>

namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Solver
{
namespace Connector
{
namespace LaplaceDirichletCCFV
{

// a local operator for solving the Laplace equation with Dirichlet boundary conditions
//     - \Delta u = 0 in \Omega,
//              u = g on \partial\Omega
// with cell centered finite volumes on axiparallel cube grids
// G : grid function for Dirichlet boundary conditions
template< typename G >
class LocalOperator:
    public Dune::PDELab::NumericalJacobianApplyVolume< LocalOperator< G > >,
    public Dune::PDELab::NumericalJacobianApplySkeleton< LocalOperator< G > >,
    public Dune::PDELab::NumericalJacobianApplyBoundary< LocalOperator< G > >,
    public Dune::PDELab::NumericalJacobianVolume< LocalOperator< G > >,
    public Dune::PDELab::NumericalJacobianSkeleton< LocalOperator< G > >,
    public Dune::PDELab::NumericalJacobianBoundary< LocalOperator< G > >,
    public Dune::PDELab::FullSkeletonPattern,
    public Dune::PDELab::FullVolumePattern,
    public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  // pattern assembly flags
  enum
  {
    doPatternVolume = true
  };
  enum
  {
    doPatternSkeleton = true
  };

  // residual assembly flags
  enum
  {
    doAlphaVolume = true
  };
  enum
  {
    doAlphaSkeleton = true
  };
  enum
  {
    doAlphaBoundary = true
  };

  LocalOperator (const G& g_, const double k_, const double m_)
      : g(g_), k(k_), m(m_)
  {
  }

  // skeleton integral depending on test and ansatz functions
  // each face is only visited ONCE!
  template< typename IG, typename LFSU, typename X, typename LFSV, typename R >
  void alpha_skeleton (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                       const LFSV& lfsv_s, const LFSU& lfsu_n, const X& x_n,
                       const LFSV& lfsv_n, R& r_s, R& r_n) const
  {
    // lfsu_s, lfsu_n and lfsv_n are not used because local functions evaluate to one
    // for finite volume local functions

    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;

    // center in face's reference element
    const Dune::FieldVector< DF, IG::dimension - 1 >& face_local =
      Dune::GenericReferenceElements< DF, IG::dimension - 1 >::general(
          ig.geometry().type()).position(0, 0);

    // face volume for integration
    RF face_volume = ig.geometry().integrationElement(face_local)
      * Dune::GenericReferenceElements< DF, IG::dimension - 1 >::general(
          ig.geometry().type()).volume();

    // cell centers in references elements
    const Dune::FieldVector< DF, IG::dimension >& inside_local =
      Dune::GenericReferenceElements< DF, IG::dimension >::general(
          ig.inside()->type()).position(0, 0);
    const Dune::FieldVector< DF, IG::dimension >& outside_local =
      Dune::GenericReferenceElements< DF, IG::dimension >::general(
          ig.outside()->type()).position(0, 0);

    // distance between the two cell centers
    Dune::FieldVector< DF, IG::dimension > inside_global =
      ig.inside()->geometry().global(inside_local);
    Dune::FieldVector< DF, IG::dimension > outside_global = ig.outside()
      ->geometry().global(outside_local);
    inside_global -= outside_global;
    RF distance = inside_global.two_norm();

    // contribution to residual on inside element, other residual is computed by symmetric call
    r_s[0] += -k*(x_s[0] - x_n[0]) * face_volume / distance;
    r_n[0] -= -k*(x_s[0] - x_n[0]) * face_volume / distance;
  }

  //! compute \f$\alpha_\text{vol}\f$
  template< typename EG, typename LFSU, typename X, typename LFSV, typename R >
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    r[0] = - m * x[0] + 1;
  }

  // skeleton integral depending on test and ansatz functions
  // We put the Dirichlet evaluation also in the alpha term to save some geometry evaluations
  template< typename IG, typename LFSU, typename X, typename LFSV, typename R >
  void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                       const LFSV& lfsv_s, R& r_s) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;

    // center in face's reference element
    const Dune::FieldVector< DF, IG::dimension - 1 >& face_local =
      Dune::GenericReferenceElements< DF, IG::dimension - 1 >::general(
          ig.geometry().type()).position(0, 0);

    // face volume for integration
    RF face_volume = ig.geometry().integrationElement(face_local)
      * Dune::GenericReferenceElements< DF, IG::dimension - 1 >::general(
          ig.geometry().type()).volume();

    // cell center in reference element
    const Dune::FieldVector< DF, IG::dimension >& inside_local =
      Dune::GenericReferenceElements< DF, IG::dimension >::general(
          ig.inside()->type()).position(0, 0);

    // distance between cell center and face center
    Dune::FieldVector< DF, IG::dimension > inside_global =
      ig.inside()->geometry().global(inside_local);
    Dune::FieldVector< DF, IG::dimension > outside_global =
      ig.geometry().global(face_local);
    inside_global -= outside_global;
    RF distance = inside_global.two_norm();

    // evaluate boundary condition function
    typename G::Traits::DomainType x = ig.geometryInInside().global(face_local);
    typename G::Traits::RangeType y;
    g.evaluate(*(ig.inside()), x, y);

    // contribution to residual on inside element
    r_s[0] += -k*(x_s[0] - y[0]) * face_volume / distance;
  }

private:
  const G& g;
  const double k;
  const double m;
};

//! \} group GridFunctionSpace
}// namespace LaplaceDirichletCCFV
} // namespace Connector
} // namespace Solver
} // namespace Detailed
} // namespace PDELab
} // namespace Dune

#endif // DUNE_RB_DETAILED_SOLVER_CONNECTOR_LAPLACEDIRICHLETCCFV_LOCALOPERATOR_HH
