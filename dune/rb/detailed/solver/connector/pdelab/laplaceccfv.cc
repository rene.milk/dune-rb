/*
 * laplaceccfv.cc
 *
 *  Created on: 16.04.2012
 *      Author: martin
 */

#include <config.h>

#include "laplaceccfv.hh"

#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/common/function.hh>

#include<dune/pdelab/common/vtkexport.hh>

namespace Dune
{
namespace RB
{
namespace Detailed
{
namespace Solver
{
namespace Connector
{

template< class T >
class SeparableLaplaceOperator:
    public Dune::RB::LA::SeparableParametric::DefaultMatrixContainer< T >
{
  typedef Dune::RB::LA::SeparableParametric::DefaultMatrixContainer< T > BaseType;
public:
  SeparableLaplaceOperator (LaplaceCCFV & connector)
      : connector_(connector)
  {
  }

  virtual ~SeparableLaplaceOperator ()
  {
  }

  virtual unsigned int numComponents () const
  {
    return 2;
  }

  virtual typename T::RangeFieldType coefficient (
      const unsigned int q, const Parameter::Vector & mu) const
  {
    switch (q)
    {
    case 0:
      return -mu.getValue("k", 0.5);
      break;
    case 1:
      return -mu.getValue("m", 0.5);
    default:
      throw(Dune::RB::RuntimeError("wrong number of coefficients!"));
    }
  }

  virtual std::vector< std::string > symbolicCoefficients () const
  {
    std::vector< std::string > symbCoeffs(2);
    symbCoeffs[0] = "-param.k";
    symbCoeffs[1] = "-param.m";
    return symbCoeffs;
  }

  virtual const std::string getIdentifier () const
  {
    return "Laplace";
  }

private:
  virtual typename T::MatrixContainerType * generate_component (
      const unsigned int q) const
  {
    double pm = 0.0, pk = 0.0;

    assert(q<=1);
    if (q == 0)
      pk = 1.0;
    else if (q == 1)
      pm = 1.0;

    typename T::VectorContainerType x0(connector_.getGridFunctionSpace());

    x0 = 0.0;

    typename T::BoundaryFunctionType g(connector_.getGridView());

    Dune::PDELab::interpolate(g, connector_.getGridFunctionSpace(), x0);

    typename T::LocalOperatorType lop(g, pk, pm);

    typename T::GOS gos(connector_.getGridFunctionSpace(),
        connector_.getGridFunctionSpace(), lop);
    typename T::MatrixContainerType * matrix =
      new typename T::MatrixContainerType(gos);

    typename T::MatrixContainerType & m = *matrix;
    m = 0.0;
    gos.jacobian(x0, m);
    return matrix;
  }

private:
  LaplaceCCFV & connector_;
};

template< class T >
class SeparableLaplaceResidual:
    public Dune::RB::LA::SeparableParametric::DefaultVectorContainer< T >
{
  typedef Dune::RB::LA::SeparableParametric::DefaultVectorContainer< T > BaseType;
public:
  SeparableLaplaceResidual (LaplaceCCFV & connector)
      : connector_(connector)
  {
  }

  virtual ~SeparableLaplaceResidual ()
  {
  }

  virtual unsigned int numComponents () const
  {
    return 1;
  }

  virtual typename T::RangeFieldType coefficient (
      const unsigned int q, const Parameter::Vector & mu) const
  {
    switch (q)
    {
    case 0:
      return 1;
      break;
    case 1:
      return mu.getValue("m", 0.5);
      break;
    default:
      throw(Dune::RB::RuntimeError("wrong number of coefficients!"));
    }
  }

  virtual std::vector< std::string > symbolicCoefficients () const
  {
    std::vector< std::string > symbCoeffs(1);
    symbCoeffs[0] = "1";
//    symbCoeffs[1] = "- param.m";
    return symbCoeffs;
  }

  virtual const std::string getIdentifier () const
  {
    return "LaplaceResidual";
  }
private:
  virtual typename T::VectorContainerType * generate_component (
      const unsigned int q) const
  {
    assert(q==0);
    double pm = 0.0, pk = 0.0;

    if (q == 0)
      pk = 1.0;
    else if (q == 1)
      pm = 1.0;

    typename T::VectorContainerType x0(connector_.getGridFunctionSpace());

    x0 = 0.0;

    typename T::BoundaryFunctionType g(connector_.getGridView());

    Dune::PDELab::interpolate(g, connector_.getGridFunctionSpace(), x0);

    typename T::LocalOperatorType lop (g, pk, pm);

    typename T::GOS gos(connector_.getGridFunctionSpace(),
        connector_.getGridFunctionSpace(), lop);

    typename T::VectorContainerType * r = new typename T::VectorContainerType(
        connector_.getGridFunctionSpace());
    *r = 0.0;
    gos.residual(x0, *r);

    return r;
  }

private:
  LaplaceCCFV & connector_;
};

LaplaceCCFV::LaplaceCCFV (Dune::ParameterTree & modelPT)
    : unitcube_(), grid_(unitcube_.grid()), gridView_(NULL), lastMaxLevel_(-1),
      fem_(Dune::GeometryType(Dune::GeometryType::cube, BasicTraits::dim)),
      gridFunctionSpace_(NULL), init_(false), modelPT_(modelPT)
{
}

LaplaceCCFV::~LaplaceCCFV ()
{
//  if (gridView_ != NULL)
//    delete gridView_;
  if (gridFunctionSpace_ != NULL) delete gridFunctionSpace_;
}

bool LaplaceCCFV::isInitialized () const
{
  return init_;
}

bool LaplaceCCFV::init ()
{
  if (!init_)
  {
    init_ = true;
    if (grid_.maxLevel() < 6) grid_.globalRefine(modelPT_.get<int>("refinementSteps", 6));

    separableLaplace_ = new SeparableLaplaceOperator< BasicTraits >(*this);
    separableL2_ = new SeparableLinearOperator< L2FVTraits< BasicTraits > >(
        *this, "L2");
    separableRHS_ = new SeparableLaplaceResidual< BasicTraits >(*this);
  }
  return true;
}

bool LaplaceCCFV::solve (const Parameter::Vector &mu,
                         typename BasicTraits::VectorType & solution) const
{
  const typename BasicTraits::GFS & gfs = getGridFunctionSpace();
  const GV & gv = gfs.gridview();

  typename BasicTraits::VectorContainerType x0(gfs);

  x0 = 0.0;

  typename BasicTraits::BoundaryFunctionType g(gv);

  Dune::PDELab::interpolate(g, gfs, x0);

  typename BasicTraits::LocalOperatorType lop(g, mu.getValue("param.k", 0.5), mu.getValue("param.m", 1.0));

  typename BasicTraits::GOS gos(gfs, gfs, lop);
  typename BasicTraits::MatrixContainerType m(gos);
  m = 0.0;
  gos.jacobian(x0, m);

  typename BasicTraits::VectorContainerType r(gfs);
  r = 0.0;
  gos.residual(x0, r);

  r *= -1.0;
  Dune::PDELab::EigenBackend_BiCGSTAB_IILU solver;
  solver.apply(m, solution, r, 1e-12);

  solution += x0;
  return true;
}

bool LaplaceCCFV::visualize (const typename BasicTraits::VectorType & dofs,
                             std::string prefix = "solution",
                             std::string cellArrayName = "u") const
{
  const GFS & gfs = getGridFunctionSpace();
  typename BasicTraits::DiscreteGridFunctionType dgf(gfs, dofs);

  Dune::VTKWriter< GV > vtkwriter(getGridView(),
      Dune::VTKOptions::nonconforming);

  typedef Dune::PDELab::VTKGridFunctionAdapter<
      typename BasicTraits::DiscreteGridFunctionType > VTKExportType;
  try
  {
    boost::filesystem::path prefixPath = this->getVtkPath();
    prefixPath /= boost::filesystem::path(prefix);
    vtkwriter.addVertexData(new VTKExportType(dgf, cellArrayName));
    vtkwriter.write(prefixPath.string(), Dune::VTKOptions::ascii);
  }
  catch (const Dune::IOError & e)
  {
    std::cerr << "IOError: Visualization failed with message\n" << e.what()
      << std::endl;
  }
  return true;
}

typename LaplaceCCFV::SeparableMatrixType & LaplaceCCFV::getSystemMatrix (
    std::string identifier) const
{
  assert(init_);
  if (identifier == "Laplace")
  {
    return *separableLaplace_;
  }
  else if (identifier == "L2")
  {
    return *separableL2_;
  }
  else
  {
    std::ostringstream oss;
    oss << "in Solver::Connector::LaplaceCCFV:\n";
    oss << "invalid identifier " << identifier
      << " for requested system matrix!";
    oss << "Valid identifiers are: 'Laplace', 'L2'";
    throw(Dune::RB::RuntimeError(oss.str()));
  }
}

typename LaplaceCCFV::SeparableVectorType & LaplaceCCFV::getSystemVector (
    std::string identifier) const
{
  assert(init_);
  if (identifier == "RHS")
  {
    return *separableRHS_;
  }
  else
  {
    std::ostringstream oss;
    oss << "in Solver::Connector::LaplaceCCFV:\n";
    oss << "invalid identifier " << identifier
      << " for requested system vector!";
    oss << "Valid identifiers are: 'RHS'";
    throw(Dune::RB::RuntimeError(oss.str()));
  }
}

} /* namespace Connector */
} /* namespace Solver */
} /* namespace Detailed */
} /* namespace RB */
} /* namespace Dune */
