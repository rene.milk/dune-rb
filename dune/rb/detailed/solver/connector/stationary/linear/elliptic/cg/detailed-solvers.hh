#ifndef DUNE_RB_DETAILED_SOLVER_CONNECTOR_STATIONARY_LINEAR_ELLIPTIC_CG_DUNE_DETAILED_SOLVERS_HH
#define DUNE_RB_DETAILED_SOLVER_CONNECTOR_STATIONARY_LINEAR_ELLIPTIC_CG_DUNE_DETAILED_SOLVERS_HH

#ifdef HAVE_CMAKE_CONFIG
  #include "cmake_config.h"
#elif defined (HAVE_CONFIG_H)
  #include <config.h>
#endif // ifdef HAVE_CMAKE_CONFIG

#include <sstream>

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <dune/common/shared_ptr.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/timer.hh>

#include <dune/grid/part/interface.hh>

#include <dune/stuff/common/logging.hh>
#include <dune/stuff/grid/boundaryinfo.hh>

#include <dune/detailed/solvers/stationary/linear/elliptic/cg/detailed-discretizations.hh>

#include <dune/rb/model/stationary/linear/elliptic/interface.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Solver {
namespace Connector {
namespace Stationary {
namespace Linear {
namespace Elliptic {
namespace CG {

template< class GridPartImp, int polynomialOrder,
          class RangeFieldImp, int dimensionRange,
          class ParamFieldImp, int maxNumParams >
class DetailedSolvers;

template< class GridPartImp, int polynomialOrder, class RangeFieldImp, class ParamFieldImp, int maxNumParams >
class DetailedSolvers< GridPartImp, polynomialOrder, RangeFieldImp, 1, ParamFieldImp, maxNumParams >
{
public:
  typedef DetailedSolvers< GridPartImp, polynomialOrder, RangeFieldImp, 1, ParamFieldImp, maxNumParams > ThisType;

  typedef Dune::grid::Part::Interface< typename GridPartImp::Traits > GridPartType;

  static const int polOrder = polynomialOrder;

  typedef typename GridPartType::ctype DomainFieldType;

  static const int dimDomain = GridPartType::dimension;

  typedef RangeFieldImp RangeFieldType;

  static const int dimRange = 1;

  typedef ParamFieldImp ParamFieldType;

  static const int maxParams = maxNumParams;

  typedef Dune::RB
      ::Model
      ::Stationary
      ::Linear
      ::Elliptic::Interface< DomainFieldType, dimDomain, RangeFieldType, dimRange, ParamFieldType, maxParams >
    ModelType;

  typedef typename ModelType::ParamType ParamType;

  typedef Dune::Stuff::Grid::BoundaryInfo::Interface< typename GridPartType::GridViewType > BoundaryInfoType;

private:
  typedef Dune::Detailed::Solvers
      ::Stationary
      ::Linear
      ::Elliptic
      ::CG
      ::Parametric::DetailedDiscretizations<  GridPartType, polOrder,
                                              RangeFieldType, dimRange,
                                              ParamFieldType, maxParams >
    DetailedSolverType;

public:
  typedef typename DetailedSolverType::VectorType VectorType;

  typedef typename DetailedSolverType::MatrixType MatrixType;

  DetailedSolvers(const shared_ptr< const ModelType > _model,
                  const shared_ptr< const GridPartType > _gridPart,
                  const shared_ptr< const BoundaryInfoType > _boundaryInfo,
                  const std::string _linearSolverType = "eigen.iterative.bicgstab.diagonal",
                  const unsigned int _linearSolverMaxIter = 5000,
                  const double _linearSolverPrecision = 1e-12)
    : model_(_model)
    , gridPart_(_gridPart)
    , boundaryInfo_(_boundaryInfo)
    , linearSolverType_(_linearSolverType)
    , linearSolverMaxIter_(_linearSolverMaxIter)
    , linearSolverPrecision_(_linearSolverPrecision)
    , initialized_(false)
    , detailedSolver_(model_, gridPart_, boundaryInfo_)
  {}

  const shared_ptr< const ModelType > model() const
  {
    return model_;
  }

  const shared_ptr< const GridPartType > gridPart() const
  {
    return gridPart_;
  }

  const shared_ptr< const BoundaryInfoType > boundaryInfo() const
  {
    return boundaryInfo_;
  }

  void init(const std::string prefix = "", std::ostream& out = Dune::Stuff::Common::Logger().debug())
  {
    if (!initialized_) {
      detailedSolver_.init(prefix, out);
      initialized_ = true;
    } // if (initialized_)
  } // void init(const std::string prefix = "", std::ostream& out = Dune::Stuff::Common::Logger().debug())

  Dune::shared_ptr< VectorType > createVector() const
  {
    assert(initialized_ && "Please call init() before calling createAnsatzVector()!");
    return detailedSolver_.createAnsatzVector();
  } // Dune::shared_ptr< VectorType > createVector() const

  void solve(const ParamType& mu,
             Dune::shared_ptr< VectorType > solutionVector,
             const std::string prefix = "",
             std::ostream& out = Dune::Stuff::Common::Logger().debug()) const
  {
    assert(initialized_ && "Please call init() before calling solve()!");
    detailedSolver_.solve(mu,
                          solutionVector,
                          linearSolverType_,
                          linearSolverMaxIter_,
                          linearSolverPrecision_,
                          prefix,
                          out);
  } // void solve(...)

  void visualizeVector(Dune::shared_ptr< VectorType > vector,
                       const std::string filename /*= id() + ".vector"*/,
                       const std::string name /*= id() + "vector"*/,
                       const std::string prefix /*= ""*/,
                       std::ostream& out /*= Dune::Stuff::Common::Logger().debug()*/) const
  {
    // preparations
    assert(initialized_ && "Please call init() before calling visualizeAnsatzVector()");
    detailedSolver_.visualizeAnsatzVector(vector, filename, name, prefix, out);
  } // void visualizeVector(...)

  Dune::shared_ptr< MatrixType > getScalarProduct(const std::string type = "fixedSystemMatrix",
                                                        const ParamType mu = ParamType()) const
  {
    assert(type == "fixedSystemMatrix");
    Dune::shared_ptr< MatrixType > scalarProduct = detailedSolver_.getSystemMatrix(mu);
    return scalarProduct;
  } // Dune::shared_ptr< MatrixType > getScalarProduct(const std::string type = "default") const

private:
  const Dune::shared_ptr< const ModelType > model_;
  const Dune::shared_ptr< const GridPartType > gridPart_;
  const Dune::shared_ptr< const BoundaryInfoType > boundaryInfo_;
  const std::string linearSolverType_;
  const unsigned int linearSolverMaxIter_;
  const double linearSolverPrecision_;
  bool initialized_;
  DetailedSolverType detailedSolver_;
}; // class DetailedSolvers

} // namespace CG
} // namespace Elliptic
} // namespace Linear
} // namespace Stationary
} // namespace Connector
} // namespace Solver
} // namespace Detailed
} // namespace RB
} // namespace Dune

#endif // DUNE_RB_DETAILED_SOLVER_CONNECTOR_STATIONARY_LINEAR_ELLIPTIC_CG_DUNE_DETAILED_SOLVERS_HH
