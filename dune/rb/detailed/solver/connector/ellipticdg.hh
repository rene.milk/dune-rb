#ifndef __DUNE_RB_DETAILED_SOLVER_CONNECTOR_ELLIPTICDG_HH__
#define __DUNE_RB_DETAILED_SOLVER_CONNECTOR_ELLIPTICDG_HH__

#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

#include <dune/rb/detailed/operator/assembler/swip.hh>

#include <dune/rb/la/separableparametric/matrix/eigen.hh>

#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

#include <Eigen/Sparse>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Solver {
namespace Connector {
template< class ModelImp, class GridPartImp >
class EllipticDG
{
public:
  typedef ModelImp                                                         ModelType;
  typedef GridPartImp                                                      GridPartType;
  typedef typename ModelType::ParameterType ParameterType;
  typedef Dune::RB::LA::SeparableParametric::Container::EigenSparseMatrix  MatrixType;
  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseVector   VectorType;
  typedef VectorType::ComponentType                                        DofVectorType;
  typedef FunctionSpace< double, double, GridPartType::dimension, 1 >      FunctionSpaceType;
  typedef DiscontinuousGalerkinSpace< FunctionSpaceType, GridPartType, 0 > SpaceType;
  typedef typename SpaceType::MapperType MapperType;
  typedef Operator::Assembler::Swip< SpaceType, ModelType, MatrixType, VectorType > OperatorType;

  EllipticDG(const ModelType& model, GridPartType& grid)
    : model_(model),
      grid_(grid),
      space_(grid),
      systemMatrix_(space_.size(), space_.size()),
      rightHandSide_(space_.size())
  {}

  const ModelType& model() const
  {
    return model_;
  }

  // Assemble the system matrix components and right hand side components
  void init()
  {
    OperatorType op(space_, model_);

    op.assemble(systemMatrix_, rightHandSide_);
    return;
  }

  const MatrixType& getSystemMatrix() const
  {
    return systemMatrix_;
  }


  const VectorType& getRightHandSide() const
  {
    return rightHandSide_;
  }

#if 0
  const MatrixType& getSystemMatrix(const std::string id) const;
  const std::string getIdentifier() const;
  void              visualize(const DofVectorType& vector) const;
#endif // if 0

  bool solve(const ParameterType& param, DofVectorType& solution)
  {
    if (systemMatrix_.rows()==0 || systemMatrix_.cols()==0)
      DUNE_THROW(InvalidStateException, "You need to call DISCO::init before calling DISCO::solve!");
    const MatrixType::CompleteType& systemMatrixComplete  = systemMatrix_.complete(param);
    const DofVectorType&            rightHandSideComplete = rightHandSide_.complete(param);
   
    // typedef Eigen::SuperLU<MatrixType::CompleteType> SolverType;
    // typedef Eigen::BiCGSTAB<MatrixType::CompleteType> SolverType;
    typedef Eigen::BiCGSTAB< MatrixType::CompleteType, Eigen::IncompleteLUT< double > > SolverType;
    // typedef Eigen::BiCGSTAB<MatrixType::CompleteType, Eigen::IdentityPreconditioner> SolverType;
    // typedef Eigen::ConjugateGradient<MatrixType::CompleteType> SolverType;
    // typedef Eigen::SimplicialCholesky<MatrixType::CompleteType> SolverType;

    solution = DofVectorType::Zero(systemMatrix_.rows());
    
    SolverType solver;

    // configure solver and preconditioner
    double solverEps = Dune::Parameter::getValue< double >("algorithm.solver.eps", 1.0e-8);
    solver.setTolerance(solverEps);
    const int solverVerbose = Dune::Parameter::getValue< int >("algorithm.solver.verbose", 0);
    const int maxIterations
      = Dune::Parameter::getValue< int >("algorithm.solver.maxIterations", solution.size());
    solver.setMaxIterations(maxIterations);
    double dropTol = Dune::Parameter::getValue< double >("algorithm.solver.preconditioner.dropTol",
                                                   Eigen::NumTraits< double >::dummy_precision());
    int fillFactor = Dune::Parameter::getValue< int >("algorithm.solver.preconditioner.fillFactor", 10);
    solver.preconditioner().setDroptol(dropTol);
    solver.preconditioner().setFillfactor(fillFactor);

    // compute preconditioner or factorization (iterative/direct solver) for matrix
    solver.compute(systemMatrixComplete);

    // solve
    solution = solver.solve(rightHandSideComplete);

    if (solverVerbose) {
      std::cout << "#iterations:     " << solver.iterations() << std::endl;
      std::cout << "estimated error: " << solver.error() << std::endl;
    }
    
    plotFunction(solution, space_, "solution");
//    plotCellData(solution, space_.gridPart().grid(), "Initial_solution", "./vis");

    return solver.info() == Eigen::Success;
  } // solve

  const MapperType& mapper() const {
    return space_.mapper();
  }
  
private:
  const ModelType&    model_;
  const GridPartType& grid_;
  const SpaceType     space_;
  mutable MatrixType  systemMatrix_;
  mutable VectorType  rightHandSide_;
}; // class EllipticDG
} // namespace Connector
} // namespace Solver
} // namespace Detailed
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_SOLVER_CONNECTOR_ELLIPTICDG_HH__
