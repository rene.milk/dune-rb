#ifndef DUNE_RB_DETAILED_SOLVER_CONNECTOR__
#define DUNE_RB_DETAILED_SOLVER_CONNECTOR__

#include <dune/common/parametertree.hh>
#include <dune/rb/detailed/parameter/vector.hh>
#include <dune/rb/la/separableparametric/icontainer.hh>

namespace Dune
{

namespace RB
{

namespace Detailed
{

namespace Solver
{

namespace Connector
{

template< class Traits, class Imp >
class Interface
{
public:
  typedef LA::SeparableParametric::IContainer<typename Traits::RangeFieldType, typename Traits::MatrixType> SeparableMatrixType;
  typedef LA::SeparableParametric::IContainer<typename Traits::RangeFieldType, typename Traits::VectorType> SeparableVectorType;
  typedef typename Traits::GFS GFS;
public:

  virtual ~Interface ()
  {
  }

  inline void init ()
  {
    asImp().init();
  }

  inline bool isInitialized () const
  {
    return asImp().isInitialized();
  }

  inline void visualize (const typename Traits::VectorType& dofs,
                         const std::string prefix = "solution",
                         const std::string cellArrayName = "u") const
  {
    asImp().visualize(dofs, prefix, cellArrayName);
  }

  const typename Traits::SeparableMatrixType& getSystemMatrix (
      const std::string id) const
  {
    return asImp().getSystemMatrix(id);
  }

  const typename Traits::SeparableVectorType& getSystemVector (
      const std::string id) const
  {
    return asImp().getSystemVector(id);
  }

  const std::string getIdentifier () const
  {
    return asImp().getIdentifier();
  }

  virtual bool solve (const Parameter::Vector& param,
                      typename Traits::VectorType & res) const
  {
    return asImp().solve(param, res);
  }

  const typename Traits::MapperType& mapper () const
  {
    return asImp().mapper();
  }

  const std::string getVtkPath () const
  {
    return asImp().getVtkPath();
  }

  const GFS & getGridFunctionSpace() const
  {
    return asImp().getGridFunctionSpace();
  }

  const Dune::ParameterTree & getModelParameterTree() const
  {
    return asImp().getModelParameterTree();
  }
private:
  Imp & asImp ()
  {
    return static_cast< Imp& >(*this);
  }
  const Imp & asImp () const
  {
    return static_cast< const Imp& >(*this);
  }
};

// class Interface

}// namespace Connector

} // namespace Solver

} // namespace Detailed

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_DETAILED_SOLVER_CONNECTOR__
