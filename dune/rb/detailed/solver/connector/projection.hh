#ifndef __DUNE_RB_DETAILED_SOLVER_CONNECTOR_PROJECTION_HH__
#define __DUNE_RB_DETAILED_SOLVER_CONNECTOR_PROJECTION_HH__

#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/projection/l2projection.hh>

namespace Dune {
namespace RB {
namespace Detailed {
namespace Solver {
namespace Connector {
template< class ModelImp, class GridPartImp, class DictionaryImp >
class Projection
{
public:
  typedef ModelImp                                                         ModelType;
  typedef GridPartImp                                                      GridPartType;
  typedef DictionaryImp                                                    DictionaryType;
  typedef typename ModelType::ExactSolutionType                            ExactSolutionType;
  typedef typename ModelType::ParameterType                                ParameterType;
  typedef Eigen::VectorXd                                                  DofVectorType;
  typedef FunctionSpace< double, double, GridPartType::dimension, 1 >      FunctionSpaceType;
  typedef DiscontinuousGalerkinSpace< FunctionSpaceType, GridPartType, 0 > SpaceType;
  typedef AdaptiveDiscreteFunction< SpaceType >                            DiscreteFunctionType;
  typedef typename SpaceType::MapperType                                   MapperType;
  typedef typename ModelType::DomainType                                   DomainType;
  typedef typename ModelType::RangeType                                    RangeType;
  typedef L2Projection< DomainType, RangeType, ExactSolutionType, DiscreteFunctionType >            L2ProjectionType;

  Projection(const ModelType& model, GridPartType& grid, DictionaryType& dict)
    : model_(model),
      grid_(grid),
      space_(grid),
      dict_(dict),
      l2projection_()
  {}

  const ModelType& model() const
  {
    return model_;
  }

  void init() { return; }


#if 0
  const MatrixType& getSystemMatrix(const std::string id) const;
  const std::string getIdentifier() const;
  void              visualize(const DofVectorType& vector) const;
#endif // if 0

  bool solve(const ParameterType& param, DofVectorType& solution)
  {
    if (dict_.contains(param)) {
      solution = dict_.get(param);
    } else {
      solution = DofVectorType::Zero(space_.size());
      DiscreteFunctionType pro("projection", space_, solution.data());
      l2projection_(model_.exactSolution(param), pro);
    }
    return true;
  } // solve

  const MapperType& mapper() const
  {
    return space_.mapper();
  }

private:
  const ModelType&       model_;
  const GridPartType&    grid_;
  const SpaceType        space_;
  const DictionaryType&  dict_;
  const L2ProjectionType l2projection_;
}; // class Projection
} // namespace Connector
} // namespace Solver
} // namespace Detailed
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_DETAILED_SOLVER_CONNECTOR_PROJECTION_HH__