#ifndef DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_HH
#define DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_HH

#include <string>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/stuff/common/color.hh>
#include <dune/stuff/grid/boundaryinfo.hh>
#include <dune/detailed/solvers/stationary/linear/elliptic/model/interface.hh>

#include "elliptic/interface.hh"
#include "elliptic/cg-detailed-solvers.hh"

namespace Dune {
namespace RB {
namespace Solver {
namespace Stationary {
namespace Linear {
namespace Detailed {
namespace Elliptic {


std::vector< std::string > types()
{
  std::vector< std::string > ret;
  ret.push_back("solver.stationary.linear.detailed.elliptic.cg-detailed-solvers");
  return ret;
} // ... types()


template< class G, class R, int r >
Dune::ParameterTree createSampleDescription(const std::string type)
{
  if (type == types()[0]) {
    return CgDetailedSolvers< G, R, r >::createSampleDescription();
  } else
    DUNE_THROW(Dune::IOError,
               "\n" << Dune::Stuff::Common::colorStringRed("ERROR:")
               << " wrong discretization requested, was '" << type << "', should be one of types()!");
}


template< class G, class R, int r >
Interface< G, R, r >* create(const Dune::shared_ptr< const G > gridPart,
                             const Dune::shared_ptr< const Dune::Detailed::Solvers
                                ::Stationary::Linear::Elliptic::Model::Interface< typename G::ctype,
                                                                                  G::dimension, R, r > > model,
                             const Dune::shared_ptr< const Dune::Stuff
                                ::Grid::BoundaryInfo::Interface< typename G::GridView > > boundaryInfo,
                             std::string type = types()[0])
{
  if (type == types()[0]) {
    assert(false);
  } else
    DUNE_THROW(Dune::IOError,
               "\n" << Dune::Stuff::Common::colorStringRed("ERROR:")
               << " wrong discretization requested, was '" << type << "', should be one of types()!");
} // ... create()


} // namespace Elliptic
} // namespace Detailed
} // namespace Linear
} // namespace Stationary
} // namespace Solver
} // namespace RB
} // namespace Dune

#endif // DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_HH
