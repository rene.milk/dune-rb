#ifndef DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_INTERFACE_HH
#define DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_INTERFACE_HH

#include <string>
#include <vector>

#include <dune/common/shared_ptr.hh>

#include <dune/grid/part/interface.hh>

#include <dune/stuff/common/logging.hh>
#include <dune/stuff/grid/boundaryinfo.hh>
#include <dune/stuff/la/container/eigen.hh>
#include <dune/stuff/common/parameter.hh>
#include <dune/stuff/common/parameter/tree.hh>

#include <dune/detailed/solvers/stationary/linear/elliptic/model/interface.hh>

namespace Dune {
namespace RB {
namespace Solver {
namespace Stationary {
namespace Linear {
namespace Detailed {
namespace Elliptic {


// forward to allow for specialization
template< class GridPartImp, class RangeFieldImp, int rangeDim >
class Interface;


template< class GridPartImp, class RangeFieldImp >
class Interface< GridPartImp, RangeFieldImp, 1 >
{
public:
  typedef typename Dune::grid::Part::Interface< typename GridPartImp::Traits >::GridPartType GridPartType;

  typedef typename GridPartType::ctype  DomainFieldType;
  static const unsigned int             dimDomain = GridPartType::dimension;
  typedef RangeFieldImp                 RangeFieldType;
  static const unsigned int             dimRange = 1;

  typedef Dune::Stuff::Grid::BoundaryInfo::Interface< typename GridPartType::GridViewType >     BoundaryInfoType;
  typedef Dune::Detailed::Solvers::Stationary::Linear::Elliptic::Model::Interface<  DomainFieldType,
                                                                                    dimDomain,
                                                                                    RangeFieldType,
                                                                                    dimRange >  ModelType;

  typedef Dune::Stuff::LA::Container::EigenRowMajorSparseMatrix< RangeFieldType > ScalarProductType;
  typedef Dune::Stuff::LA::Container::EigenDenseVector< RangeFieldType >          VectorType;

  typedef Dune::Stuff::Common::Parameter::Type ParameterType;

  typedef Dune::Stuff::Common::ExtendedParameterTree DescriptionType;

  static std::string id()
  {
    return "solver.stationary.linear.detailed.elliptic";
  }

  virtual ~Interface() {}

  virtual Dune::shared_ptr< const GridPartType > gridPart() const = 0;

  virtual Dune::shared_ptr< const BoundaryInfoType > boundaryInfo() const = 0;

  virtual Dune::shared_ptr< const ModelType > model() const = 0;

  virtual bool parametric() const
  {
    return model()->parametric();
  }

  virtual bool init(const std::string /*prefix = ""*/,
                    std::ostream& /*out = Dune::Stuff::Common::Logger().debug()*/) = 0;

  virtual Dune::shared_ptr< VectorType > createVector() const = 0;

  virtual Dune::shared_ptr< ScalarProductType > createScalarProduct(const std::string /*type*/,
                                                                    const ParameterType /*mu = ParameterType()*/) const = 0;

  virtual bool solve(Dune::shared_ptr< VectorType > /*solutionVector*/,
                     const ParameterType /*mu = ParameterType()*/) const = 0;

  virtual bool visualizeVector(const Dune::shared_ptr< const VectorType > /*vector*/,
                               const std::string /*filename*/,
                               const std::string /*name*/) const = 0;
}; // class Interface


} // namespace Elliptic
} // namespace Detailed
} // namespace Linear
} // namespace Stationary
} // namespace Solver
} // namespace RB
} // namespace Dune


#endif // DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_INTERFACE_HH
