#ifndef DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_CG_DETAILED_SOLVERS_HH
#define DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_CG_DETAILED_SOLVERS_HH

#include <dune/detailed/solvers/stationary/linear/elliptic/cg/detailed-discretizations.hh>

#include "interface.hh"

namespace Dune {
namespace RB {
namespace Solver {
namespace Stationary {
namespace Linear {
namespace Detailed {
namespace Elliptic {


// forward to allow for specialization
template< class GridPartImp, class RangeFieldImp, int rangeDim >
class CgDetailedSolvers;


template< class GridPartImp, class RangeFieldImp >
class CgDetailedSolvers< GridPartImp, RangeFieldImp, 1 >
  : public Interface< GridPartImp, RangeFieldImp, 1 >
{
public:
  typedef CgDetailedSolvers< GridPartImp, RangeFieldImp, 1 >  ThisType;
  typedef Interface< GridPartImp, RangeFieldImp, 1 >          BaseType;

  typedef typename BaseType::GridPartType GridPartType;

  typedef typename BaseType::DomainFieldType  DomainFieldType;
  static const unsigned int                   dimDomain = BaseType::dimDomain;
  typedef typename BaseType::RangeFieldType   RangeFieldType;
  static const unsigned int                   dimRange = BaseType::dimRange;

  typedef typename BaseType::BoundaryInfoType BoundaryInfoType;
  typedef typename BaseType::ModelType        ModelType;

  typedef typename BaseType::ScalarProductType  ScalarProductType;
  typedef typename BaseType::VectorType         VectorType;

  typedef typename BaseType::ParameterType ParameterType;

  typedef typename BaseType::DescriptionType DescriptionType;

  static std::string id()
  {
    return BaseType::id() + ".cg-detailed-solvers";
  }

private:
  typedef Dune::Detailed::Solvers::Stationary::Linear::Elliptic::CG::DetailedDiscretizations< GridPartType,
                                                                                              1,
                                                                                              RangeFieldType,
                                                                                              dimRange >
    DetailedSolverType;

public:
  CgDetailedSolvers()
  {}

  static DescriptionType createSampleDescription(const std::string subName = "")
  {
    // since we do not need anything, we just forward to the detailed solver
    return DetailedSolverType::createSampleDescription(subName);
  } // ... createSampleDescription(...)

  static ThisType* createFromDescription(const Dune::shared_ptr< const GridPartType > _gridPart,
                                         const Dune::shared_ptr< const ModelType > _model,
                                         const Dune::shared_ptr< const BoundaryInfoType > _boundaryInfo,
                                         const DescriptionType& _description,
                                         const std::string _subName = id())
  {
    // get correct description
    DescriptionType description;
    if (_description.hasSub(_subName))
      description = _description.sub(_subName);
    else
      description = _description;
    assert(false);
  } // ... createFromParamTree(...)

  virtual Dune::shared_ptr< const GridPartType > gridPart() const
  {
    assert(false);
  }

  virtual Dune::shared_ptr< const BoundaryInfoType > boundaryInfo() const
  {
    assert(false);
  }

  virtual Dune::shared_ptr< const ModelType > model() const
  {
    assert(false);
  }

  virtual bool parametric() const
  {
    return model()->parametric();
  }

  virtual bool init(const std::string /*prefix = ""*/,
                    std::ostream& /*out = Dune::Stuff::Common::Logger().debug()*/)
  {
    assert(false);
  }

  virtual Dune::shared_ptr< VectorType > createVector() const
  {
    assert(false);
  }

  virtual Dune::shared_ptr< ScalarProductType > createScalarProduct(const std::string /*type*/,
                                                                    const ParameterType /*mu = ParameterType()*/) const
  {
    assert(false);
  }

  virtual bool solve(Dune::shared_ptr< VectorType > /*solutionVector*/,
                     const ParameterType /*mu = ParameterType()*/) const
  {
    assert(false);
  }

  virtual bool visualizeVector(const Dune::shared_ptr< const VectorType > /*vector*/,
                               const std::string /*filename*/,
                               const std::string /*name*/) const
  {
    assert(false);
  }
}; // class CgDetailedSolvers


} // namespace Elliptic
} // namespace Detailed
} // namespace Linear
} // namespace Stationary
} // namespace Solver
} // namespace RB
} // namespace Dune


#endif // DUNE_RB_SOLVER_STATIONARY_LINEAR_DETAILED_ELLIPTIC_CG_DETAILED_SOLVERS_HH
