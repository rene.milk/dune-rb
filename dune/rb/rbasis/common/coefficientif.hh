#ifndef DUNE_RB_COEFFICIENTIF_HH_
#define DUNE_RB_COEFFICIENTIF_HH_

#include "../../misc/ducktyping.hh"
#include <string>

namespace Dune {
namespace RBFem {
namespace Lib {

/** @brief Interface class for \f$\mu\f$-dependent objects.
 *
 *  @interfaceclass
 */
template<class FieldType>
class ICoefficient
{
public:
  virtual ~ICoefficient() {}
  /**
   * @brief evaluation of the parameter dependent factor @f$ \sigma(\mu) @f$
   * for the data function part.
   *
   * @return  parameter dependent factor
   */
  __DUCK_START__
  inline FieldType coefficient() const
  __DUCK_END__

  /**
   * @brief evaluation of the parameter dependent factor @f$ \sigma(\mu) @f$
   * for the data function part.
   *
   * @return symbolic string that can be evaluated by MATLAB interpreter.
   */
  __DUCK_START__
  inline ::std::string symbolicCoefficient() const
  __DUCK_END__

  /**
   * @brief indicates wether the coefficient of this data function (summand)
   * dependents on a parameter given in <tt> rb.mu_names </tt>.
   *
   * @return boolean
   */
  __DUCK_START__
  bool isMuDependent() const
  __DUCK_END__

};

/** @brief Interface class that need a setTime() method. */
class ITime
{
public:
  virtual ~ITime() {}
  /**
   * @brief indicates wether space dependent factor of this data function
   * depends on the time variable.
   *
   * @return boolean
   */
  __DUCK_START__
  bool isTimeDependent() const
  __DUCK_END__

  /**
   * @brief updates in the time for the related space dependent component
   *
   * @param time  new time
   */
  __DUCK_START__
  void setTime(double time)
  __DUCK_END__
};

/** @brief Interface class for \f$\mu\f$ and time dependent objects.
 *
 * @interfaceclass
 * */
template<class FieldType>
class ITimeCoefficient
  : public ICoefficient<FieldType>, public ITime
{};

} // namespace Lib
} // end of namespace RBFem
} // end of namespace Dune

#endif /* DUNE_RB_COEFFICIENTIF_HH_ */

