#ifndef  __FINVOL_HH__
#define  __FINVOL_HH__

#include  <limits>

#include <dune/fem/operator/common/localoperator.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>

#include "../interfaces.hh"

#define MAX_DOUBLE std::numeric_limits<double>::max()

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Operator
{
namespace Fv
{
/** @addtogroup fv
 *@{*/

/** @brief Interface class for a constant addend of an affinely %decomposed
 * flux function.
 */
template<class Model >
class IConstantFluxPart
  : public Dune::RBFem::Lib::ITimeCoefficient<typename Model :: FieldType>
{
public:
  typedef typename Model :: FunctionSpaceType :: RangeType           RangeType;
  typedef typename Model :: FieldType                                FieldType;
  typedef typename Model :: GridPartType :: GridType
            :: template Codim< 0 > :: Entity                         Entity;
  //! quadrature on a grid entity
  typedef Dune::CachingQuadrature< typename Model :: GridPartType, 0 >     VolumeQuadratureType;
protected:
  /** @brief computes the volume of a grid entity en
   *
   *  \param[in] en     grid entity whose volume is to be computed
   *  \param[in] quad   volume quadrature used
   *  \param[out] massVolInv   reciprocal of the volume
   *  \return          the grid entity's volume */
  double volumeElement(const Entity& en,
                       const VolumeQuadratureType& quad,
                       double & massVolInv ) const
  {
    double result = 0.0;
    massVolInv = 0.0;
    const int quadNop = quad.nop();
    for (int qp = 0; qp < quadNop; ++qp)
    {
      result +=
        quad.weight(qp) * en.geometry().integrationElement(quad.point(qp));
    }
    massVolInv = 1/result;
    return result;
  }

public:
  IConstantFluxPart() : en_(0) {}

  virtual ~IConstantFluxPart() {};

  //! sets the entity currently used
  inline void init(const Entity & en)
  {
    en_ = &en;
  }

  /** @brief evaluate the function locally on the grid entity set by init()
   *
   * This method needs to be implemented by an implementation of this interface.
   */
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret)
  {
    DUNE_THROW(NotImplemented, "evaluate method has not been implemented");
  }

protected:
  /** @brief returns the entity last registered via init() */
  const Entity & getEn() const
  {
    return *en_;
  }

private:
  Entity const *en_;
};

/** @brief Interface class for a linear addend of an affinely %decomposed flux
 * function.
 *
 * This is an interface for a linear function
 *   \f[ g: \mathbf{R} \times \mathbf{A}^2 \to \mathbf{B},
 *       (x, u_L, u_R) \mapsto g(x, u_L, u_R). \f]
 * where \f$\mathbf{A}\f$ is the \a ArgumentTuple and \f$\mathbf{B}\f$ the
 * \a ResultType.
 *
 */
template<class Model>
class IFluxPart
  : public Dune::RBFem::Lib::ITimeCoefficient< typename Model :: FieldType >
{
public:
  typedef typename Model :: GridPartType :: GridType
            :: template Codim< 0 > :: Entity
            :: LeafIntersectionIterator :: Intersection              Intersection;
  //! dimension of domain vector space
  static const int dimDomain = Model :: FunctionSpaceType
                                     :: DomainType :: dimension;
  //! vector space type for coordinates on boundary faces
  typedef ::Dune::FieldVector< typename Model :: FieldType, dimDomain - 1 >  FaceDomainType;
public:
  virtual ~IFluxPart() {};

  /** @brief evaluates the flux on inner grid intersections
   *
   * @param it        Intersection on which the flux is evaluated
   * @param localX    local coordinate on the intersection where flux is
   *                  evaluated. This is in most cases the midpoint of the
   *                  intersection, given in coordinates of the reference
   *                  element.
   * @param uLeft     evaluation of the discrete function on the \em left side
   *                  of the intersection \f$u_L\f$
   * @param uRight    evaluation of the discrete function on the \em right side
   *                  of the intersection \f$u_R\f$
   * @param gLeft     flux evaluation \f$g(x; u_L, u_R)\f$
   * @param gRight    flux evaluation \f$g(x; u_R, u_L)\f$
   *
   * @tparam ArgumentTuple   argument type \f$\mathbf{A}\f$
   * @tparam ResultType      result type \f$\mathbf{R}\f$
   * */
  template < class ArgumentTuple, class ResultType>
  double numericalFlux(const Intersection & it,
                       const FaceDomainType & localX,
                       const ArgumentTuple & uLeft,
                       const ArgumentTuple & uRight,
                       ResultType & gLeft,
                       ResultType & gRight) const
  {
    DUNE_THROW(::Dune::NotImplemented, "interface method not implemented");
  }

  /** @brief evaluates the flux on boundary intersections.
   *
   * In the case of Dirichlet boundary values the value of \f$u_R\f$ is simply
   * that to the evaluation of the Dirichlet data function in this region,
   * otherwise the flux reduces to a linear function
   * \f[ g: \mathbf{R} \times \mathbf{A} \to \mathbf{B},
   *       (x, u_L) \mapsto g(x; u_L) \f]
   * on the boundary.
   *
   * @param it        Intersection on which the flux is evaluated
   * @param localX    local coordinate on the intersection where flux is
   *                  evaluated. This is in most cases the midpoint of the
   *                  intersection, given in coordinates of the reference
   *                  element.
   * @param uEl       evaluation of the discrete function on the \em left side
   *                  of the intersection \f$u_L\f$
   * @param gEl       flux evaluation \f$g(x; u_L)\f$
   *
   * @tparam ArgumentTuple   argument type \f$\mathbf{A}\f$
   * @tparam ResultType      result type \f$\mathbf{R}\f$
   * */
  template < class ArgumentTuple, class ResultType>
  double boundaryNumericalFlux(const Intersection & it,
                               const FaceDomainType& localX,
                               const ArgumentTuple & uEl,
                               ResultType & gEl) const
  {
    DUNE_THROW(NotImplemented, "interface method not implemented");
  }
};

/**
 * \brief a finite volume operator
 *
 * This class expects a class FluxType, derived from the Interface class 
 * FluxPartInterface, as template parameter implementing a
 * flux function \f$g(u_i,u_j)\f$ for inner boundaries \f$(i,j)\f$ and a flux
 * function \f$g_\mathrm{j}(u_i)\f$ for boundary interfaces.
 *
 * Then it constructs an operator
 *
 * \f[
 * \left(L[u]\right)_i :=
 *   \frac{1}{|T_i|}\left(
 *                       \sum_{j\in \mathcal{N}_{\mathrm{in}}(i)} g(u_i, u_j) +
 *                       \sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)} g_{j}(u_i)
 *                 \right).
 * \f]
 */
template<class FluxType, class DestinationType>
class Default
  : public Dune::RBFem::Lib::Operator::ILocalParametrized < DestinationType,
                                                Default < FluxType, DestinationType > >
{
public:
  //! discrete function type of solution function
  typedef DestinationType                                            DiscFuncType;
  //! model type
  typedef typename FluxType :: ModelType                             ModelType;
  //! function space type of solution function
  typedef typename ModelType :: FunctionSpaceType                    FunctionSpaceType;
  //! domain type of solution function
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range type of solution function
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! field type of the range vector space (double)
  typedef typename FunctionSpaceType :: RangeFieldType               RangeFieldType;
  //! grid part type
  typedef typename ModelType :: GridPartType                         GridPartType;
  //! grid type
  typedef typename GridPartType :: GridType                          GridType;
  //! local function type of solution function
  typedef typename DestinationType :: LocalFunctionType              LocalFuncType;

  //! base type
  typedef ILocalParametrized< DestinationType,
                                         Default< FluxType,
                                                 DestinationType > > BaseType;


  //! dimension of range vector type
  static const int dimRange = RangeType::dimension;
  //! dimension of domain vector type
  static const int dimDomain = DomainType :: dimension;
  //! the quadrature points should be the mid points of the faces
  typedef FieldVector< RangeFieldType, dimDomain - 1 >               MidType;

  //! quadrature on a grid entity
  typedef Dune::CachingQuadrature< GridPartType, 0 >                       VolumeQuadratureType;
  //! quadrature on a grid face entity
  typedef Dune::CachingQuadrature< GridPartType, 1 >                       FaceQuadratureType;

  //! local it set type
  typedef typename GridType :: Traits :: LocalIdSet                  LocalIdSetType;
  //! intersection iterator type
  typedef typename GridPartType :: IntersectionIteratorType          IntersectionIteratorType;

  using BaseType :: scalar_;
public:
  //! constructor
  Default( FluxType & flux, bool doPrepare = false, bool doFinalize = false )
    : doPrepare_( doPrepare ), doFinalize_( doFinalize ), flux_( flux ), dtMin_(MAX_DOUBLE) {};

  void prepareGlobal(const DiscFuncType & arg, DiscFuncType & dest)
  {
    arg_  = &arg;
    dest_ = &dest;

    assert(&arg != 0); assert(&dest_ != 0);
    DiscFuncType & argTemp = const_cast<DiscFuncType &> (*arg_);

    idset_ = &(argTemp.space().grid().localIdSet());
    gridPart_ = &(argTemp.space().gridPart()) ;

    if(doPrepare_)
    {
      dtMin_ = MAX_DOUBLE;
      dest.clear();
    }
  }

  //! finish the operator call
  void finalizeGlobal()
  {
#if defined(DEBUG) && DEBUG > 1000
    std::cout << "in FinVol :: finalizeGlobal():\ndest=\n";
    dest_->print(std::cout);
#endif
    assert(dtMin_ > 0.0);
  }

  //! set the current time
  void setTime(double time)
  {
    flux_.setTime(time);
  }

  //! return estimate for the next time step
  double timeStepEstimate() const
  {
    return dtMin_;
  }

  //! apply the operator locally
  template <class EntityType>
  void applyLocal(EntityType & en)
  {
    DiscFuncType & argTemp = const_cast<DiscFuncType &> (*arg_);
    onCell( en, argTemp, *dest_ );
  }

public:

  //! return coefficient
  inline double coefficient() const
  {
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in FinVol :: coefficient(): coefficient = " << flux_.coefficient() << "\n";
#endif
    return flux_.coefficient();
  }

  inline ::std::string symbolicCoefficient() const
  {
    return flux_.symbolicCoefficient();
  }

  //! returns wether the local function part depends on \f$\mu\f$. Otherwise,
  //! the coefficient() function returns a constant value.
  bool isMuDependent() const
  {
    return flux_.isMuDependent();
  }

  bool isTimeDependent() const
  {
    return flux_.isTimeDependent();
  }
private:
  //! computes the volume of a grid entity en
  //! \param[in] en     grid entity whose volume is to be computed
  //! \param[in] quad   volume quadrature used
  //! \param[out] massVolInv   reciprocal of the volume
  //! \return          the grid entity's volume
  template <class EntityType>
  double volumeElement(const EntityType& en,
                       const VolumeQuadratureType& quad, 
                       double & massVolInv ) const {
    double result = 0.0;
    massVolInv = 0.0;
    const int quadNop = quad.nop();
    for (int qp = 0; qp < quadNop; ++qp) {
      result += 
        quad.weight(qp) * en.geometry().integrationElement(quad.point(qp));
    }
    massVolInv = 1/result;
    return result;
  }

  //! evaluates the operator on one grid cell entity
  template <class EntityType>
  void onCell(EntityType & en, DiscFuncType & oldSol, DiscFuncType & update)
  {
    VolumeQuadratureType volQuad(en, 0);

    LocalFuncType oldLf = oldSol.localFunction( en );
    LocalFuncType upLf  = update.localFunction( en );

    // get volume and its inverse of the actual element
    double vol_1;
/*    double vol = volumeElement(en, volQuad, vol_1);*/
    volumeElement(en, volQuad, vol_1);

/*    double dtLocal;*/

    typedef typename EntityType :: EntityPointer                     EntityPointerType;

    assert( gridPart_ );
    IntersectionIteratorType endnit = gridPart_->iend(en);
    for( IntersectionIteratorType nit = gridPart_->ibegin(en) ; nit != endnit ; ++nit )
    {
      // compute local face mid point
      FaceQuadratureType faceQuadInner(*gridPart_, *nit, 0, FaceQuadratureType::INSIDE);
      MidType mid = faceQuadInner.localPoint(0);

      if( nit->neighbor() )
      {
        EntityPointerType ep = nit->outside();
        EntityType & neighbor = (*ep);
        VolumeQuadratureType volQuad2(neighbor, 0);


        if( idset_->template id<0>(neighbor) > idset_->template id<0>(en) )
        {

          // get access to local functions
          LocalFuncType neighLf   = oldSol.localFunction( neighbor );
          LocalFuncType upNeighLf = update.localFunction( neighbor );

          // evaluate the local function on the middle point of the actual face
          RangeType valEl, valNeigh;
          oldLf.evaluate   ( volQuad.point(0),  valEl );
          neighLf.evaluate ( volQuad2.point(0), valNeigh );

          // calculate numerical flux g(u,v)
          RangeType fluxLeft, fluxRight;

/*          double dtLocal = flux_.numericalFlux( *nit, mid, valEl, valNeigh, fluxLeft, fluxRight);*/
          flux_.numericalFlux( *nit, mid, valEl, valNeigh, fluxLeft, fluxRight);
#if defined(DEBUG) && DEBUG > 10000
          std::cout << "in FinVol :: onCell(en, ...):  fluxLeft = " << fluxLeft << std::endl;
#endif

          // get volume of neighbor
          double nvol_1;
/*          double nvol =  volumeElement(neighbor, volQuad2, nvol_1);*/
          volumeElement(neighbor, volQuad2, nvol_1);

          // minimum is the volume for dt estimation
/*          double vdt = std::min( vol, nvol );
 *          dtMin_ = this->calcDt( dtMin_, vdt, 1.0, dtLocal );*/

          // add local flux to update function
          for(int l=0; l< oldLf.numDofs(); l++)
          {
            upLf[ l ]      += scalar_ * vol_1   * fluxLeft [l];
            upNeighLf[ l ] += scalar_ * nvol_1  * fluxRight[l];
#if defined(DEBUG) && DEBUG > 100000
            std::cout << "in FinVol :: onCell(en, ...): iterate over dofs [l]:\n";
            std::cout << "scalar_: " << scalar_ << " vol_1: " << vol_1 
              << " nvol_1: " << nvol_1 << std::endl;
            std::cout << "upLf[l]: " << upLf[l] << "\n";
            std::cout << "upNeighLf[l]: " << upNeighLf[l] << "\n";
#endif
          }
        } // end if id(en) > id(neighbor)
      } // end if inner intersection
      else if( nit->boundary() )
      {
        // evaluate the local function in the middle of the actual face
        RangeType valEl;
        oldLf.evaluate  ( volQuad.point(0), valEl );

        // calculate numerical flux g(u,v)
        RangeType fluxLeft;
/*        double dtLocal = flux_.boundaryNumericalFlux(*nit, mid, valEl, fluxLeft);*/
        flux_.boundaryNumericalFlux(*nit, mid, valEl, fluxLeft);
/*        dtMin_ = this->calcDt( dtMin_, vol, 1.0, dtLocal );*/

        for(int l=0; l< oldLf.numDofs(); l++)
        {
          // update 
          upLf[ l ]     += scalar_ * vol_1 * fluxLeft[l];
        }
      } // end if boundary intersection
      else {
        ::std::cerr << "Intersection has no boundary or neighbor!" << ::std::endl;
      }
    } // end for intersection loop
  }

  //! \todo this does not make sense at the moment
  double calcDt (double dtMin, double vol, double face, double dtLoc)
  {
    if( ::std::abs(dtLoc) > 1e-10 )
    {
      const double h = vol/::std::abs(dtLoc);
      return ( (dtMin > h) ? h : dtMin );
    }
    return dtMin;
  }

private:
  const LocalIdSetType *idset_;
  const GridPartType   *gridPart_;
  bool                  doPrepare_;
  bool                  doFinalize_;
  FluxType             &flux_;

  DiscFuncType const   *arg_;
  DiscFuncType         *dest_;

  double                dtMin_;
};

/*@}*/

} // namespace Fv
}  // namespace Operator
}  // namespace Lib
} // end namespace RBFem
} // end namespace Dune

#endif  /*__FINVOL_HH__*/
/* vim: set et sw=2: */
