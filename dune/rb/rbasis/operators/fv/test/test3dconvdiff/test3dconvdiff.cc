#define BOOST_TEST_MODULE ConvectionDiffusionTest
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <config.h>
#include "../../../../../misc/tuples.hh"

#include "../../../../../matlab/rbsocks/rbserver.hh"
#include "../../../../../matlab/facades/linevolfacade.hh"

#include "../../../../linevol/linevoldescr.hh"
#include "../../../../linevol/linevoldiscr.hh"
#include "../../../../linevol/linevolsolve.hh"
#include "../../../../linevol/linevoldefault.hh"

#include "../../../../../common/test/eoctest.hh"

using namespace Dune::RBFem::Example::LinEvol;

namespace Dune
{
namespace RB
{
typedef Description< double, 1, POLORDER, RBFem::Example::LinEvol::SemiImplicit,
    TEST3DCONVDIFF > DescrSiType;

struct ServerSiTraits
{
  typedef DescrSiType ProblemType;

  typedef Library< ServerTypeTraits, DescrSiType > DiscrType;

  typedef RBMatlabLinEvolFacade< ServerTypeTraits > FacadeType;

  typedef typename ServerTypeTraits::ServerType ServerType;
};

typedef Description< double, 1, POLORDER, RBFem::Example::LinEvol::Explicit,
    TEST3DCONVDIFF > DescrExType;

struct ServerExTraits
{
  typedef DescrExType ProblemType;

  typedef Library< ServerTypeTraits, DescrExType > DiscrType;

  typedef RBMatlabLinEvolFacade< ServerTypeTraits > FacadeType;

  typedef typename ServerTypeTraits::ServerType ServerType;
};

}
}

/** @file
 * @ingroup tests
 * @brief Test3dconvdiff Test using 3D convection diffusion problem
 *
 *  From the Dune-Fem-Howto:
 *  The test problem is a scalar advection-diffusion equation on @f$\Omega =
 *  [0,1]^3@f$:
 *  @f{eqnarray*}{
 \partial_{t} u + \nabla \cdot (\mathbf{v} u) - \varepsilon \Delta u
 {}          =&  0             \mbox{ in}\quad \Omega \times [0,0.2]\\
    u           =&  g_{D}         \mbox{ on}\quad \partial\Omega \times [0,0.2] \\
    u(0,\cdot)  =&  u_{0}         \mbox{ in}\quad \Omega \times \{0\}
 @f}
 *  with @f$u@f$ living in a function space @f$V@f$ and @f$\mathbf{v}:=(0.8,0.6,0)^t@f$. An exact
 *  solution is specified by
 *  @f[
 *  u(x,t) = \sum_{i=1}^{2} \exp(-\varepsilon t \pi^2 |{c^{i}|})
 *  \left(\prod_{j=1}^{3} \tilde{c}_{j}^{i}\cos(c_{j}^{i}\pi(x_{j}-\mathbf{v}_{j} t)) +
 *  {}              \hat{c}_{j}^{i}\sin(c_{j}^{i}\pi(x_{j}-\mathbf{v}_{j} t)) \right),
 *  @f]
 *  where
 *  @f{eqnarray*}{
 c^{1}    :=(2,1,1.3)^{t}      &\qquad c^{2}:=(0.7,0.5,0.1) \\
    \hat{c}^{1}:=(0.8,0.4,-0.4)^{t} &\qquad \hat{c}^{2} := (0.2,0.1,0.2) \\
    \tilde{c}^{1}:=(0.6,1.2,0.1)^{t}  &\qquad \tilde{c}^{2} := (0.9,0.3,-0.3).
 @f}
 *  Therefore the initial and boundary functions are defined by
 *  @f{eqnarray*}{
 *  u_{0}(x) = u(x,0), \qquad g_{D}(x,t) = u|_{\partial\Omega}(x,t).
 *  @f}
 *
 *  This test expects the inital error to be less than 0.02, the expected EOC is
 *  greater then 1.0 using the Lax-Friedrich flux and an semiimplicit discretization.
 */

using namespace Dune::RB;

typedef RBFem::Example::EocTest< ServerSiTraits > EocSiTestType;
typedef RBFem::Example::EocTest< ServerExTraits > EocExTestType;

BOOST_AUTO_TEST_SUITE( ConvDiffEocTest )

BOOST_FIXTURE_TEST_CASE( convdiff_explicit_convection_only, EocExTestType )
{
  Parameter::replaceKey("rb.diffusion", 0.0);
  run();

  double meanEoc = std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  std::cout << "Mean eoc is " << std::setprecision(5) << std::setw(6) << meanEoc << ", expected > 0.5!\n";

  BOOST_CHECK(meanEoc >= 0.6);

  finish();
}

BOOST_FIXTURE_TEST_CASE( convdiff_explicit_full, EocExTestType )
{
  Parameter::replaceKey("rb.dtEst", 0.01);
  run();

  double meanEoc = std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  std::cout << "Mean eoc is " << std::setprecision(5) << std::setw(6) << meanEoc << ", expected > 0.5!\n";

  BOOST_CHECK(meanEoc >= 0.6);

  finish();
}

BOOST_FIXTURE_TEST_CASE( convdiff_semiimplicit_convection_only, EocSiTestType )
{
  Parameter::replaceKey("rb.diffusion", 0.0);
  Parameter::replaceKey("rb.dtEst", 0.1);
  run();

  double meanEoc = std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  std::cout << "Mean eoc is " << std::setprecision(5) << std::setw(6) << meanEoc << ", expected > 0.5!\n";

  BOOST_CHECK(meanEoc >= 0.6);

  finish();
}

BOOST_FIXTURE_TEST_CASE( convdiff_seimiimplicit_full, EocSiTestType )
{
  Parameter::replaceKey("rb.diffusion", 0.02);
  Parameter::replaceKey("rb.dtEst", 0.1);
  run();

  double meanEoc = std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  std::cout << "Mean eoc is " << std::setprecision(5) << std::setw(6) << meanEoc << ", expected > 0.5!\n";

  BOOST_CHECK(meanEoc >= 0.6);

  finish();
}

BOOST_FIXTURE_TEST_CASE( convdiff_semiimplicit_strong_diffusion, EocSiTestType )
{
  Parameter::replaceKey("rb.diffusion", 0.05);
  Parameter::replaceKey("rb.dtEst", 0.05);
  run();

  double meanEoc = std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  std::cout << "Mean eoc is " << std::setprecision(5) << std::setw(6) << meanEoc << ", expected > 0.5!\n";

  BOOST_CHECK(meanEoc >= 0.6);

  finish();
}

BOOST_AUTO_TEST_SUITE_END( )

//#include "../../matlab/duneserver/main.inc"

