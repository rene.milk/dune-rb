#define BOOST_TEST_MODULE TransportEocTest
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <config.h>
#include "../../../../../misc/tuples.hh"

#include "../../../../../matlab/rbsocks/rbserver.hh"
#include "../../../../../matlab/facades/linevolfacade.hh"

#include "../../../../linevol/linevoldescr.hh"
#include "../../../../linevol/linevoldiscr.hh"
#include "../../../../linevol/linevolsolve.hh"
#include "../../../../linevol/linevoldefault.hh"


#include "../../../../../common/test/eoctest.hh"


/** @file
 * @ingroup tests
 * @brief This implements a model inspired from the transport example from the
 * dune-femhowto.
 *
 * See dune-femhowto/tutorial/finitevolume for implementation details.
 *
 * \f[
 * \begin{aligned}
 *  \partial_t u + \vec{a} \cdot \nabla u &= 0 && \text{in $\Omega \times [0,T)$},\\
 *    u &= u_{in} && \text{on $\Gamma_{in}$},\\
 *    u( \cdot, 0 ) &= u_0 && \text{in $\Omega$},
 * \end{aligned}
 * \f]
 *
 * where
 * \f$\Gamma_{in} = \lbrace (\vec{x},t) \in \partial\Omega \times [0,T)
 *                  \,\vert\, \vec{a} \cdot \normal( \vec{x} ) < 0 \rbrace\f$
 *
 * \f{align}{
 * \Omega &= [0,1]^n,\\
 * \vec{a} &\equiv (1.25, 0.8, \ldots, 0.8) \in \R^n,\\
 * u_{in}( \vec{x}, t ) &= \sin 2 \pi \lvert \vec{x} - \vec{a} t \rvert^2,\\
 * u_0( \vec{x} ) &= \sin 2 \pi \lvert \vec{x} \rvert^2.
 * \f}
 *
 * The exact solution to this problem is given by
 * \f[
 *  u( \vec{x}, t )
 *    := \sin 2 \pi \lvert \vec{x}
 *        - \vec{a} t \rvert^2
 *            \quad \text{for $(\vec{x},t) \in \Omega \times [0, T)$}.
 *  \f]
 */

using namespace Dune::RBFem::Example::LinEvol;

namespace Dune {
  namespace RB {
    struct ServerTraits
    {
      typedef Description< double, 1, POLORDER, RBFem::Example::LinEvol::Explicit,
                            TEST2DTRANSPORT >                        ProblemType;

      typedef Library< ServerTypeTraits, ProblemType >        DiscrType;

      typedef RBMatlabLinEvolFacade< ServerTypeTraits >              FacadeType;

      typedef typename ServerTypeTraits :: ServerType                ServerType;
    };
  }
}

using namespace Dune::RB;

typedef RBFem::Example::EocTest< ServerTraits >                                      EocTestType;


BOOST_AUTO_TEST_SUITE( TransportEocTest )

BOOST_FIXTURE_TEST_CASE( transport_1, EocTestType )
{

  run();

  double meanEoc=std::accumulate(eocVec.begin(), eocVec.end(), 0.)/double(eocVec.size());
  printf("Mean eoc is %2.3f, expected >0.5!\n", meanEoc);

  BOOST_CHECK(meanEoc>=0.5);

  finish();

}

BOOST_AUTO_TEST_SUITE_END( )

