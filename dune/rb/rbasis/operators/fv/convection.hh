#ifndef  __CONVECTION_HH__
#define  __CONVECTION_HH__

#include  "../../../misc/utility.hh"
#include  "finvol.hh"
#include  "../discretedecomposedoperator.hh"
#include  "../common/makedecomposedoperators.hh"
#include  "../../functions/localmuscaledfunction.hh"
#include  "../../functions/discretedecomposedfunction.hh"
#include  "../../functions/common/makedecomposedfunctions.hh"
#include "../../linevol/linevolmodel.hh"

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Operator
{
namespace Fv
{
namespace Convection
{

/** @addtogroup fv
 * @{ */

/**
 * @brief mu scaled addend of decomposed discretization operator for the
 * convection term
 *
 * A part is a component together with a coefficient function
 * \f$\sigma(\mu)\f$.
 *
 * This part implements
 *
 * \f[
 * (b_{\mathrm{linvel}}^{q,r})_i :=
 *  -\frac{|e_{ij}|}{2|T_i|}\sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)}
 *  \sigma^q_{\mathbf{v}}(\mu) \mathbf{v}^q(\mathbf{c}_{ij})
 *   \cdot \mathbf{n}_{ij}
 * \sigma^r_{b_{\mathrm{dir}}}(\mu)b_{\mathrm{dir}}^r(\mathbf{c}_{ij})
 * \f]
 *
 * where \f$q,r\f$ are set via the template parameters velCompIndex and dirCompIndex.
 *
 * This part can be combined e.g. with a ConvLaxFrLocalFunctionPart
 * \f$b_{\mathrm{lax}}\f$ to a scalar function for the convective part of a
 * numerical Lax-Friedrichs scheme
 *
 * \f[
 * b_{\mathrm{conv}} := \sum_{q=1}^{Q_v} \sum_{r=1}^{Q_d}
 * b_{\mathrm{linvel}}^{q,r} + \sum_{r=1}^{Q_d} b_{\mathrm{lax}}^{r} 
 * \f]
 *
 * This combination \f$b_{\mathrm{conv}}\f$ is done automatically by the
 * ConvMuScaledFunctionComposite class. Together with a combined operator
 * ConvFinVolComposite implementing the decomposed operator 
 * \f$L_{\mathrm{conv}}\f$, we get a finite volume scheme for a convection
 * equation by evaluating
 *
 * \f[
 * u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 * b_{\mathrm{conv}} \right).
 * \f]
 *
 * @see ConvMuScaledFunctionComposite, ConvFinVolComposite, ConvLaxFrLocalFunctionPart
 */
template< int velCompIndex, int dirCompIndex, class ModelImp >
class LinearVelocityLocalFunctionPart: public IConstantFluxPart< ModelImp >
{
public:
  //! model type
  typedef ModelImp ModelType;
  //! type of the decomposed velocity function
  typedef typename ModelType::DecomposedVelocityType DecomposedVelocityType;
  //! type of a function component of the decomposed velocity function
  typedef typename DecomposedVelocityType::ComponentType VelocityComponentType;
  //! range type of a decomposed velocity function
  typedef typename DecomposedVelocityType::RangeType VelocityRangeType;
  //! type of the decomposed dirichlet function
  typedef typename ModelType::DecomposedDirichletType DecomposedDirichletType;
  //! type of a function component of the decomposed dirichlet function
  typedef typename DecomposedDirichletType::ComponentType DirichletComponentType;
  //! range type of a decomposed dirichlet function
  typedef typename DecomposedDirichletType::RangeType DirichletRangeType;
  //! function space type of solution function
  typedef typename ModelType::FunctionSpaceType FunctionSpaceType;
  //! domain type of solution function
  typedef typename FunctionSpaceType::DomainType DomainType;
  //! range type of solution function
  typedef typename FunctionSpaceType::RangeType RangeType;
  //! field type of the range vector space (double)
  typedef typename FunctionSpaceType::RangeFieldType FieldType;
  //! grid part type
  typedef typename ModelType::GridPartType GridPartType;
  //! type of a grid entity
  typedef typename GridPartType::GridType:: template Codim< 0 >::Entity Entity;
  //! type of a pointer to a grid entity
  typedef typename GridPartType::GridType:: template Codim< 0 >::EntityPointer EntityPointer;
  //! intersection iterator type
  typedef typename Entity::LeafIntersectionIterator IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator::Intersection Intersection;
  //! quadrature on a grid entity
  typedef Dune::CachingQuadrature< GridPartType, 0 > VolumeQuadratureType;
  //! quadrature on a grid face entity
  typedef Dune::CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

  static const int dimDomain = DomainType::dimension;

  static const unsigned int numComps = DecomposedVelocityType::numComps
    * DecomposedDirichletType::numComps;
  //! the quadrature points should be the mid points of the faces
  typedef FieldVector< FieldType, dimDomain - 1 > MidType;
private:

  //! evalutes the function for one intersection
  template< class PointType, class ResultType >
  void evaluateLocal (const Intersection & it, PointType & localX,
                      ResultType & gEl)
  {
    typedef typename Intersection::EntityPointer EntityPointer;

    DomainType normal = it.integrationOuterNormal( localX );
    DomainType midPoint = it.geometryInInside().global( localX );
    EntityPointer eptr = it.inside();
    typedef typename VelocityComponentType::LocalFunctionType VelocityLocalFunctionType;
    typedef typename DirichletComponentType::LocalFunctionType DirichletLocalFunctionType;

    VelocityRangeType vel;
    const VelocityLocalFunctionType velLocalFunc = velComp_.localFunction(
        *eptr );
    velLocalFunc.evaluate( midPoint, vel );

    DirichletRangeType dir;
    const DirichletLocalFunctionType dirLocalFunc = dirComp_.localFunction(
        *eptr );
    dirLocalFunc.evaluate( midPoint, dir );

    gEl = dir;
    gEl *= (vel * normal);
    gEl *= (-1. / 2.);
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in ConvLinVelLocalFunctionPart :: evaluateLocal():\n";
    std::cout << "gEl: " << gEl << " vel: " << vel << " normal: " << normal
    << " vel * normal: " << (vel*normal) << " dir: " << dir << std::endl;
#endif
  }

public:
  //! constructor
  //! \param model  the model providing decomposed functions for velocity and dirchlet boundaries
  //! \param gridPart the underlying grid part
  LinearVelocityLocalFunctionPart (ModelType & model,
                                   const GridPartType & gridPart)
      : model_( model ), gridPart_( gridPart ), decVel_( model_.velocity() ),
        velComp_( decVel_.component( velCompIndex ) ),
        decDir_( model_.dirichlet() ),
        dirComp_( decDir_.component( dirCompIndex ) )
  {
    muDependent_ = (decVel_.isMuDependent( velCompIndex )
      || decDir_.isMuDependent( dirCompIndex ));
  }

  //! evaluate the function locally on the grid entity set by init()
  template< class PointType >
  void evaluate (const PointType & localX, RangeType & ret)
  {
    IntersectionIterator endnit = gridPart_.iend( this->getEn() );
    ret = RangeType( 0 );
    for (IntersectionIterator nit = gridPart_.ibegin( this->getEn() );
        nit != endnit; ++nit)
    {
      if (nit->boundary())
      {
        // compute local face mid point
        FaceQuadratureType faceQuadInner( gridPart_, *nit, 0,
                                          FaceQuadratureType::INSIDE );
        MidType localMid = faceQuadInner.localPoint( 0 );

        // compute global face mid point
        DomainType midPoint = nit->geometry().global( localMid );
        if (model_.boundaryType( nit, time_, localMid, midPoint )
          == Example::BoundaryType( Example::Dirichlet ))
        {
          RangeType val;
          evaluateLocal( *nit, localMid, val );
          ret += val;
        }
      }
    }

    VolumeQuadratureType volQuad( this->getEn(), 0 );
    double vol_1;
    this->volumeElement( this->getEn(), volQuad, vol_1 );
    ret *= vol_1;
  }

  //! return the \f$\mu\f$ - dependent coefficient
  inline double coefficient () const
  {
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in ConvLinVelLocalFunctionPart::coefficient()\n" <<
    ":coefficient = " << decVel_.coefficient(velCompIndex) * decDir_.coefficient(dirCompIndex);
#endif
    return decVel_.coefficient( velCompIndex )
      * decDir_.coefficient( dirCompIndex );
  }

  ::std::string symbolicCoefficient () const
  {
    return decVel_.symbolicCoefficient( velCompIndex ) + ::std::string( "*" )
      + decDir_.symbolicCoefficient( dirCompIndex );
  }

  bool isMuDependent () const
  {
    return muDependent_;
  }

  bool isTimeDependent () const
  {
    //!\todo
    return false;
  }

  void setTime (double time)
  {
    decVel_.setTime( velCompIndex, time );
    decDir_.setTime( dirCompIndex, time );
    time_ = time;
  }

private:
  ModelType &model_;
  const GridPartType &gridPart_;
  DecomposedVelocityType &decVel_;
  VelocityComponentType &velComp_;
  DecomposedDirichletType &decDir_;
  DirichletComponentType &dirComp_;
  bool muDependent_;
  double time_;

};

/**
 * @brief mu scaled addend of decomposed discretization operator for the Lax
 * Friedrich term
 *
 * A part is a component together with a coefficient function \f$\sigma(\mu)\f$.
 *
 * This part implements
 *
 * \f[
 * (b^{q})_i := \frac{|e_{ij}|}{2|T_i|}\sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)}
 *  \lambda^{-1}\sigma^r_{b_{\mathrm{dir}}}(\mu)b_{\mathrm{dir}}^r(\mathbf{c}_{ij})
 * \f]
 *
 * where \f$q\f$ is set via the template parameter dirCompIndex
 *
 * This part can be combined e.g. with a ConvLinVelLocalFunctionPart
 * \f$b_{\mathrm{conv}}\f$ to a scalar function for the convective part of a
 * numerical Lax-Friedrichs scheme
 *
 * \f[
 * b_{\mathrm{conv}} := \sum_{q=1}^{Q_v} \sum_{r=1}^{Q_d}
 * b_{\mathrm{linvel}}^{q,r} + \sum_{r=1}^{Q_d} b_{\mathrm{lax}}^{r} 
 * \f]
 *
 * This combination \f$b_{\mathrm{conv}}\f$ is done automatically by the
 * ConvMuScaledFunctionComposite class. Together with a combined operator
 * ConvFinVolComposite implementing the decomposed operator 
 * \f$L_{\mathrm{conv}}\f$, we get a finite volume scheme for a convection
 * equation by evaluating
 *
 * \f[
 * u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 * b_{\mathrm{conv}} \right).
 * \f]
 *
 * @see ConvMuScaledFunctionComposite, ConvFinVolComposite, ConvLinVelLocalFunctionPart
 */
template< int dirCompIndex, class ModelImp >
class LaxFriedrichsLocalFunctionPart: public IConstantFluxPart< ModelImp >
{
public:
  //! model type
  typedef ModelImp ModelType;
  //! type of decomposed dirichlet function
  typedef typename ModelType::DecomposedDirichletType DecomposedDirichletType;
  //! type of a function component of the decomposed dirichlet function 
  typedef typename DecomposedDirichletType::ComponentType DirichletComponentType;
  //! range type of a decomposed dirichlet function 
  typedef typename DecomposedDirichletType::RangeType DirichletRangeType;
  //! function space type of solution function 
  typedef typename ModelType::FunctionSpaceType FunctionSpaceType;
  //! 	domain type of solution function 
  typedef typename FunctionSpaceType::DomainType DomainType;
  //! range type of solution function 
  typedef typename FunctionSpaceType::RangeType RangeType;
  //! field type of the range vector space (double) 
  typedef typename FunctionSpaceType::RangeFieldType FieldType;
  //! grid part type 
  typedef typename ModelType::GridPartType GridPartType;
  //! type of a grid entity 
  typedef typename GridPartType::GridType:: template Codim< 0 >::Entity Entity;
  //! type of a pointer to a grid entity 
  typedef typename GridPartType::GridType:: template Codim< 0 >::EntityPointer EntityPointer;
  //! intersection iterator type 
  typedef typename Entity::LeafIntersectionIterator IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator::Intersection Intersection;
  //! quadrature on a grid entity
  typedef CachingQuadrature< GridPartType, 0 > VolumeQuadratureType;
  //! quadrature on a grid face entity 
  typedef CachingQuadrature< GridPartType, 1 > FaceQuadratureType;
  static const int dimDomain = DomainType::dimension;
  static const unsigned int numComps = DecomposedDirichletType::numComps;
  //! the quadrature points should be the mid points of the faces 
  typedef FieldVector< FieldType, dimDomain - 1 > MidType;
private:
  //! evalutes the function for one intersection
  template< class PointType, class ResultType >
  void evaluateLocal (const Intersection & it, PointType & localX,
                      ResultType & gEl)
  {
    typedef typename Intersection::EntityPointer EntityPointer;

    DomainType normal = it.integrationOuterNormal( localX );
    DomainType midPoint = it.geometry().global( localX );
    DomainType localMid = it.geometryInInside().global( localX );

    EntityPointer eptr = it.inside();
    DirichletRangeType dir;
    typedef typename DirichletComponentType::LocalFunctionType LocalFunctionType;
    LocalFunctionType dirCompLocalFunc = dirComp_.localFunction( *eptr );
    dirCompLocalFunc.evaluate( localMid, dir );

    FieldType lambda;
    //! \todo do sth. with the time
    model_.lambdaScalar( midPoint, 0.0, lambda );

    gEl = (1. / 2.) * (1. / lambda) * dir * normal.two_norm();
  }

public:
  //! constructor
  //! \param model 	the model providing decomposed functions for velocity and dirchlet boundaries
  //! \param gridPart 	the underlying grid part 
  LaxFriedrichsLocalFunctionPart (ModelType & model,
                                  const GridPartType & gridPart)
      : model_( model ), gridPart_( gridPart ), decDir_( model_.dirichlet() ),
        dirComp_( decDir_.component( dirCompIndex ) )
  {
    muDependent_ = decDir_.isMuDependent( dirCompIndex );
  }

  //! evaluate the function locally on the grid entity set by init()
  template< class PointType >
  void evaluate (const PointType & localX, RangeType & ret)
  {
    IntersectionIterator endnit = gridPart_.iend( this->getEn() );
    ret = RangeType( 0 );
    for (IntersectionIterator nit = gridPart_.ibegin( this->getEn() );
        nit != endnit; ++nit)
    {
      if (nit->boundary())
      {
        // compute local face mid point
        FaceQuadratureType faceQuadInner( gridPart_, *nit, 0,
                                          FaceQuadratureType::INSIDE );
        MidType localMid = faceQuadInner.localPoint( 0 );

        // compute global face mid point
        DomainType midPoint = nit->geometry().global( localMid );

        if (model_.boundaryType( nit, time_, localMid, midPoint )
          == Example::BoundaryType(Dune::RBFem::Example::Dirichlet ))
        {
          RangeType val;
          evaluateLocal( *nit, localMid, val );
          ret += val;
        }
      }
    }
    VolumeQuadratureType volQuad( this->getEn(), 0 );
    double vol_1;
    this->volumeElement( this->getEn(), volQuad, vol_1 );
    ret *= vol_1;
  }

  //! return the \f$\mu\f$ - dependent coefficient
  inline double coefficient () const
  {
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in ConvLaxFrLocalFunctionPart :: coefficient()\n" <<
    ": coefficient = " << decDir_.coefficient(dirCompIndex) << " \n";
#endif
    return decDir_.coefficient( dirCompIndex );
  }

  inline ::std::string symbolicCoefficient () const
  {
    return decDir_.symbolicCoefficient( dirCompIndex );
  }

  //! returns wether the local function part depends on \f$\mu\f$. Otherwise,
  //! the coefficient() function returns a constant value.
  bool isMuDependent () const
  {
    return muDependent_;
  }

  //! returns wether the local function part is time dependent
  bool isTimeDependent () const
  {
    //!\todo
    return false;
  }

  void setTime (double time)
  {
    decDir_.setTime( dirCompIndex, time );
    time_ = time;
  }

private:
  ModelType &model_;
  const GridPartType &gridPart_;
  DecomposedDirichletType &decDir_;
  DirichletComponentType &dirComp_;
  bool muDependent_;
  double time_;
};

/**
 * @brief mu scaled addend of decomposed discretization flux for the convection term.
 *
 * A part is a component together with a coefficient function \f$\sigma(\mu)\f$.
 *
 * This flux part implements
 *
 * \f{eqnarray*}{
 * g_{\mathrm{linvel}}^{q}(u_i, u_j) 
 *   &:=& -\frac{1}{2}|e_{ij}|
 *  \sigma^q_{\mathbf{v}}(\mu) \mathbf{v}^q(\mathbf{c}_{ij})
 *   \cdot \mathbf{n}_{ij}
 *   (u_i + u_j) \\
 * g_{\mathrm{linvel,dir}}^{q}(u_i) 
 *   &:=& -\frac{1}{2}|e_{ij}|
 *  \sigma^q_{\mathbf{v}}(\mu) \mathbf{v}^q(\mathbf{c}_{ij})
 *   \cdot \mathbf{n}_{ij} u_i 
 * \f}
 *
 * where \f$q\f$ is set via the template parameters velCompIndex. 
 *
 * The FinVol class generates an operator \f$L_{\mathrm{linvel}}^q\f$ out of
 * this flux part by evaluating
 *
 * \f[
 * \left(L_{\mathrm{linvel}}^{q}[u]\right)_i := 
 *   \frac{1}{|T_i|}\left(
 *                       \sum_{j\in \mathcal{N}_{\mathrm{in}}(i)} g_{\mathrm{linvel}}^q(u_i, u_j) +
 *                       \sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)} g_{\mathrm{linvel,dir}}^q(u_i)
 *                 \right)
 * \f]
 *
 * This part can be combined with a FinVol operator parametrised with a
 * ConvFluxLaxFrPart \f$L_{\mathrm{lax}}\f$ to an operator for convective part
 * of a numerical Lax-Friedrichs scheme
 *
 * \f[
 * L_{\mathrm{conv}} := \sum_{q=1}^{Q_v} L_{\mathrm{linvel}}^q + L_{\mathrm{lax}}.
 * \f]
 *
 * This combination is done automatically by the ConvFinVolComposite class.
 * Together with a combined mu scaled function ConvMuScaledFunctionComposite
 * implementing the decomposed function \f$b_{\mathrm{conv}}\f$ we get a finite
 * volume scheme for a convection equation by evaluating
 * \f[ 
 *   u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 *   b_{\mathrm{conv}} \right). 
 * \f] 
 *
 * @see FinVol, ConvFinVolComposite, ConvMuScaledFunctionComposite, ConvFluxLaxFrPart
 */
template< int velCompIndex, class ModelImpType >
class LinearVelocityFluxPart: public IFluxPart< ModelImpType >
{
public:
  //! model type
  typedef ModelImpType ModelType;
  //! type of the decomposed velocity function
  typedef typename ModelType::DecomposedVelocityType DecomposedVelocityType;
  //! type of a function component of the decomposed velocity function
  typedef typename DecomposedVelocityType::ComponentType VelocityComponentType;
  //! range type of a decomposed velocity function
  typedef typename DecomposedVelocityType::RangeType VelocityVectorType;
  //! function space type of solution function
  typedef typename ModelType::FunctionSpaceType FuncSpaceType;
  //! domain type of solution function
  typedef typename FuncSpaceType::DomainType DomainType;
  //! range type of solution function
  typedef typename FuncSpaceType::RangeType RangeType;
  //! field type of the range vector space (double)
  typedef typename FuncSpaceType::RangeFieldType FieldType;
  //! grid part type
  typedef typename ModelType::GridPartType GridPartType;
  //! grid type of a grid entity
  typedef typename GridPartType::GridType GridType;
  //! type of a grid entity
  typedef typename GridType:: template Codim< 0 >::Entity Entity;
  //! type of a pointer to a grid entity 
  typedef typename GridType:: template Codim< 0 >::EntityPointer EntityPointer;
  //! intersection iterator type
  typedef typename Entity::LeafIntersectionIterator IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator::Intersection Intersection;

  //! dimension of range vector space
  static const int dimRange = RangeType::dimension;
  //! dimension of domain vector space
  static const int dimDomain = DomainType::dimension;
  //! vector space type for coordinates on a boundary face
  typedef FieldVector< FieldType, dimDomain - 1 > FaceDomainType;
public:
  //! constructor
  //! \param model  the model providing a decomposed function for velocity
  LinearVelocityFluxPart (ModelType & model)
      : model_( model ), decVel_( model_.velocity() ),
        velComp_( decVel_.component( velCompIndex ) )
  {
    muDependent_ = decVel_.isMuDependent( velCompIndex );
  }
  ;

  //! evaluates the flux on inner cells
  template< class ArgumentTuple, class ResultType >
  double numericalFlux (const Intersection & it, const FaceDomainType & localX,
                        const ArgumentTuple & uLeft,
                        const ArgumentTuple & uRight, ResultType & gLeft,
                        ResultType & gRight) const
  {
    DomainType normal = it.integrationOuterNormal( localX );
    DomainType mid = it.geometryInInside().global( localX );

    // get velocity vector for this component
    typedef typename Intersection::EntityPointer EntityPointer;
    EntityPointer eptr = it.inside();
    VelocityVectorType vel;
    typedef typename VelocityComponentType::LocalFunctionType VelocityLocalFunctionType;
    VelocityLocalFunctionType velLocalFunc = velComp_.localFunction( *eptr );
    velLocalFunc.evaluate( mid, vel );

    //! \note normal includes the weight \f$|e_{ij}|\f$.
    gLeft = (-1. / 2.) * (vel * normal);
    gLeft *= (uLeft + uRight);
    //! normal changes sign for right flux
    gRight = -gLeft;

    //! \todo this does not make sense if we are mu dependent. Then we probably
    //! need some further method like "dtEstimate" that might also depend on mu.
    return vel * normal;
  }

  //! evaluates the flux on a boundary intersection
  template< class ArgumentTuple, class ResultType >
  double boundaryNumericalFlux (const Intersection & it,
                                const FaceDomainType& localX,
                                const ArgumentTuple & uEl,
                                ResultType & gEl) const
  {
    DomainType midPoint = it.geometry().global( localX );
    DomainType localMid = it.geometryInInside().global( localX );

    // initialize return value
    ArgumentTuple bndUNeigh;
    ResultType gNeigh;
    double factor;

    if (model_.boundaryType( it, time_, localX, midPoint )
      == Example::BoundaryType(Example:: Dirichlet ))
    {
      factor = 0.5;
    }
    else if (model_.boundaryType( it, time_, localX, midPoint )
      == Example::BoundaryType(Example:: OutFlow ))
    {
      factor = 1.0;
    }
    else
    { // noflow or neuman boundary flow which is taken care of in another localfunction
      gEl = 0.0;
      return 0.0;
    }

    DomainType normal = it.integrationOuterNormal( localX );

    // get velocity vector for this component
    typedef typename Intersection::EntityPointer EntityPointer;
    EntityPointer eptr = it.inside();
    VelocityVectorType vel;
    typedef typename VelocityComponentType::LocalFunctionType VelocityLocalFunctionType;
    VelocityLocalFunctionType velLocalFunc = velComp_.localFunction( *eptr );
    velLocalFunc.evaluate( localMid, vel );

    gEl = -factor * (vel * normal) * uEl;

    //! \todo this does not make sense if we are mu dependent. Then we probably
    //! need some further method like "dtEstimate" that might also depend on mu.
    return vel * normal;
  }

  //! \f$\sigma(\mu)\f$
  double coefficient () const
  {
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in ConvFluxLinVelPart :: coefficient():\n"
    << "coefficient = " << decVel_.coefficient(velCompIndex) << "\n";
#endif
    return decVel_.coefficient( velCompIndex );
  }

  inline ::std::string symbolicCoefficient () const
  {
    return decVel_.symbolicCoefficient( velCompIndex );
  }

  //! returns wether the flux part depends on \f$\mu\f$. Otherwise,
  //! the coefficient() method returns a constant value.
  bool isMuDependent () const
  {
    return muDependent_;
  }

  //! returns wether the local flux part is time dependent
  bool isTimeDependent () const
  {
    //! \todo
    return false;
  }

  void setTime (double time)
  {
    decVel_.setTime( velCompIndex, time );
    time_ = time;
  }

private:
  ModelType &model_;
  DecomposedVelocityType &decVel_;
  VelocityComponentType &velComp_;
  bool muDependent_;
  double time_;
};

//! \todo ConvFluxNeuman

/**
 * @brief mu scaled addend of decomposed discretization flux for the Lax-Friedrichs term
 *
 * A part is a component together with a coefficient function \f$\sigma(\mu)\f$.
 *
 * This part implements
 *
 * \f{eqnarray*}{
 * g_{\mathrm{lax}}(u_i, u_j) 
 *   &:=& -\frac{1}{2}|e_{ij}|
 *   \lambda^{-1}
 *   (u_i - u_j), \\
 * g_{\mathrm{lax,dir}}(u_i)
 *  &:=& -\frac{1}{2}|e_{ij}|
 *   \lambda^{-1} u_i. 
 * \f}
 *
 * The FinVol class generates an operator \f$L_{\mathrm{lax}}^q\f$ out of
 * this flux part by evaluating
 *
 * \f[
 * \left(L_{\mathrm{lax}}[u]\right)_i := 
 *   \frac{1}{|T_i|}\left(
 *                       \sum_{j\in \mathcal{N}_{\mathrm{in}}(i)} g_{\mathrm{lax}}(u_i, u_j) +
 *                       \sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)} g_{\mathrm{lax,dir}}(u_i)
 *                 \right)
 * \f]
 *
 * This part can be combined with a FinVol operator parametrised with a
 * ConvFluxLaxFrPart \f$L_{\mathrm{lax}}\f$ to an operator for convective part
 * of a numerical Lax-Friedrichs scheme
 *
 * \f[
 * L_{\mathrm{conv}} := \sum_{q=1}^{Q_v} L_{\mathrm{linvel}}^q + L_{\mathrm{lax}}.
 * \f]
 *
 * This combination is done automatically by the ConvFinVolComposite class.
 * Together with a combined mu scaled function ConvMuScaledFunctionComposite
 * implementing the decomposed function \f$b_{\mathrm{conv}}\f$ we get a finite
 * volume scheme for a convection equation by evaluating
 * \f[ 
 *   u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 *   b_{\mathrm{conv}} \right). 
 * \f] 
 *
 * @see FinVol, ConvFinVolComposite, ConvMuScaledFunctionComposite, ConvFluxLinVelPart
 */
template< class ModelImpType >
class LaxFriedrichsFluxPart: public IFluxPart< ModelImpType >
{
public:
  //! model type
  typedef ModelImpType ModelType;
  //! function space type of solution function
  typedef typename ModelType::FunctionSpaceType FuncSpaceType;
  //! domain type of solution function
  typedef typename FuncSpaceType::DomainType DomainType;
  //! range type of solution function
  typedef typename FuncSpaceType::RangeType RangeType;
  //! field type of the range vector space (double)
  typedef typename FuncSpaceType::RangeFieldType FieldType;
  //! grid part type
  typedef typename ModelType::GridPartType GridPartType;
  //! grid type
  typedef typename GridPartType::GridType GridType;
  //! type of a grid entity
  typedef typename GridType:: template Codim< 0 >::Entity Entity;
  //! type of a pointer to a grid entity
  typedef typename GridType:: template Codim< 0 >::EntityPointer EntityPointer;
  //! intersection iterator type
  typedef typename Entity::LeafIntersectionIterator IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator::Intersection Intersection;

  //! dimension of domain vector space
  static const int dimDomain = DomainType::dimension;
  //! dimension of range vector space
  static const int dimRange = RangeType::dimension;
  //! vector space type for coordinates on boundary faces
  typedef FieldVector< FieldType, dimDomain - 1 > FaceDomainType;
private:
  //! numerical flux method used for boundary and inner intersections
  template< class ArgumentTuple, class ResultType >
  double calcNumericalFlux (const Intersection & it,
                            const FaceDomainType & localX,
                            const ArgumentTuple & uLeft,
                            const ArgumentTuple & uRight, ResultType & gLeft,
                            ResultType & gRight) const
  {
    DomainType normal = it.integrationOuterNormal( localX );
    DomainType midPoint = it.geometry().global( localX );

    FieldType lambda;
    //! \todo do sth. with the time
    model_.lambdaScalar( midPoint, time_, lambda );

    gLeft = uLeft;
    gLeft -= uRight;
    gLeft *= (-1. / 2.) * (1. / lambda) * normal.two_norm();
    gRight = -gLeft;

    return 0.;
  }
public:
  //! constructor
  //! \param model  underlying model instance
  LaxFriedrichsFluxPart (const ModelType & model)
      : model_( model )
  {
  }

  //! evaluates the flux on inner grid intersections
  template< class ArgumentTuple, class ResultType >
  double numericalFlux (const Intersection & it, const FaceDomainType & localX,
                        const ArgumentTuple & uLeft,
                        const ArgumentTuple & uRight, ResultType & gLeft,
                        ResultType & gRight) const
  {
    return calcNumericalFlux( it, localX, uLeft, uRight, gLeft, gRight );
  }

  //! evaluates the flux on boundary intersections
  template< class ArgumentTuple, class ResultType >
  double boundaryNumericalFlux (const Intersection & it,
                                const FaceDomainType& localX,
                                const ArgumentTuple & uEl,
                                ResultType & gEl) const
  {
    DomainType midPoint = it.geometry().global( localX );

    if (model_.boundaryType( it, time_, localX, midPoint )
      == Example::BoundaryType(Example:: Dirichlet ))
    {
      ArgumentTuple bndUNeigh( 0 );
      // initialize return value
      ResultType gNeigh;
      return calcNumericalFlux( it, localX, uEl, bndUNeigh, gEl, gNeigh );
    }
    else if (model_.boundaryType( it, time_, localX, midPoint )
      == Example::BoundaryType(Example:: OutFlow ))
    {
      gEl = 0.0;
      return 0.0;
    }
    else
    { // noflow or neuman boundary flow which is handled in another localfunction
      gEl = 0.0;
      return 0.0;
    }
  }

  //! \f$\sigma(\mu)\f$
  double coefficient () const
  {
#if defined(DEBUG) && DEBUG > 10000
    std::cout << " in ConvFluxLaxFrPart :: coefficient:\n" << "coefficient =  "
    << 1.0 << std::endl;
#endif
    return 1.0;
  }

  ::std::string symbolicCoefficient () const
  {
    return "1.0";
  }

  //! returns false
  bool isMuDependent () const
  {
    return false;
  }

  //! returns wether the local flux part is time dependent
  bool isTimeDependent () const
  {
    return false;
  }

  void setTime (double time)
  {
    time_ = time;
  }

private:
  const ModelType &model_;
  double time_;
};

/** @brief constructs decomposed function out of local function parts
 *
 * This class combines discrete decomposed functions with local function parts
 * ConvLaxFrLocalFunctionPart \f$b_{\mathrm{lax}}\f$ and
 * ConvLinVelLocalFunctionPart \f$b_{\mathrm{linvel}}\f$ to a scalar function
 * for the convective part of a numerical Lax-Friedrichs scheme
 *
 * \f[
 * b_{\mathrm{conv}} := \sum_{q=1}^{Q_v} \sum_{r=1}^{Q_d}
 * b_{\mathrm{linvel}}^{q,r} + \sum_{r=1}^{Q_d} b_{\mathrm{lax}}^{r} 
 * \f]
 *
 * Together with a combined operator ConvFinVolComposite implementing the
 * decomposed operator \f$L_{\mathrm{conv}}\f$, we get a finite volume scheme
 * for a convection equation by evaluating
 *
 * \f[
 * u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 * b_{\mathrm{conv}} \right).
 * \f]
 *
 * @see ConvFinVolComposite, ConvLinVelLocalFunctionPart, ConvLaxFrLocalFunctionPart
 */
template< class ModelImp, class DiscreteFunctionType >
class MuScaledFunctionComposite
{
public:
  //! model type
  typedef ModelImp ModelType;

  //! number of velocity components
  static const int velComponents = ModelType::DecomposedVelocityType::numComps;
  //! number of dirichlet components
  static const int dirComponents = ModelType::DecomposedDirichletType::numComps;

  //! traits class for the maker class of the linvel part
  typedef Decomposed::Function::FactoryParams2< LinearVelocityLocalFunctionPart,
      ModelType, velComponents, dirComponents > LinVelPartParamsType;
  //! maker class for linvel part
  typedef Decomposed::Function::AnalyticalFactory< 0,
      velComponents * dirComponents, LinVelPartParamsType, DiscreteFunctionType > LinVelMakerType;
  //! traits class for the maker class of the lax part
  typedef Decomposed::Function::FactoryParams1< LaxFriedrichsLocalFunctionPart,
      ModelType > LaxFrPartParamsType;
  //! make class for the lax part
  typedef Dune::RBFem::Decomposed::Function::AnalyticalFactory< 0,
      dirComponents, LaxFrPartParamsType, DiscreteFunctionType > LaxFrMakerType;

  //! base class for the resulting decomposed function
  typedef typename LinVelMakerType::DecomposedFunctionType::DecomposedVectorType DecomposedVectorType;

  //! the type for the sum of the two decomposed functions.
  typedef __typeof__(Dune::RB::makeT< typename LinVelMakerType
      :: DecomposedFunctionType > ()
      + Dune::RB::makeRT< typename LaxFrMakerType
      :: DecomposedFunctionType > ()) CompositeType;
  /*  typedef DecomposedVectorType                                       CompositeType;*/

  //! discrete function space type
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

public:
  //! constructor constructing the decomposed function out of local function parts
  //! \param model      model instance providing decomposed velocity and
  //!                   dirichlet functions
  //! \param space      discrete function space
  MuScaledFunctionComposite (ModelType & model,
                             const DiscreteFunctionSpaceType & space)
      : linVelMaker_( model, space ), laxFrMaker_( model, space ),
        composite_( 0 )
  {
    if (velComponents > 0)
    {
      composite_ = new CompositeType(
          linVelMaker_.getFunction() + laxFrMaker_.getFunction() );
    }
  }

  //! return the composite function
  CompositeType & getFunction ()
  {
    return *composite_;
  }

  //! desctructor
  ~MuScaledFunctionComposite ()
  {
    if (composite_ != 0)
    {
      delete composite_;
    }
  }

private:
  LinVelMakerType linVelMaker_;
  LaxFrMakerType laxFrMaker_;
  CompositeType *composite_;
};

/** \brief constructs a decomposed operator out of local flux parts
 *
 * This class combines a decomposed finite volume operator of type FinVol
 * parametrised with a ConvFluxLaxFrPart \f$L_{\mathrm{lax}}\f$ and a
 * decomposed finite volume operator of type FinVol parametrised with a
 * ConvFluxLinVelPart \f$L_{\mathrm{linvel}}\f$ operator to a decomposed operator
 * for the convective part of a numerical Lax-Friedrichs discretization
 *
 * \f[
 * L_{\mathrm{conv}} := \sum_{q=1}^{Q_v} L_{\mathrm{linvel}}^q + L_{\mathrm{lax}}.
 * \f]
 *
 * Together with a combined mu scaled function ConvMuScaledFunctionComposite
 * implementing the decomposed function \f$b_{\mathrm{conv}}\f$ we get a finite
 * volume scheme for a convection equation by evaluating
 * \f[ 
 *   u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{conv}} u^{k} +
 *   b_{\mathrm{conv}} \right). 
 * \f] 
 *
 * @see FinVol, ConvLaxFrLocalFunctionPart, ConvMuScaledFunctionComposite, ConvFluxLinVelPart
 */
template< class ModelImp, class DestinationImp >
class Composite
{
public:
  //! model type
  typedef ModelImp ModelType;
  //! discrete function type of solution function
  typedef DestinationImp DestinationType;

  typedef typename DestinationType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

  //! number of velocity components
  static const int velComponents = ModelType::DecomposedVelocityType::numComps;

  //! traits class for the maker class of the linvel part
  typedef Decomposed::Operator::FromFluxFactoryParams1< LinearVelocityFluxPart,
      Operator::Fv::Default, ModelType, DestinationImp > LinVelPartParamsType;

  //! maker class for the linvel part
  typedef Decomposed::Operator::Factory< 0, velComponents, LinVelPartParamsType > LinVelMakerType;

  //! traits class for the maker class of the lax part
  typedef Decomposed::Operator::FromFluxFactoryParams0< LaxFriedrichsFluxPart,
      Operator::Fv::Default, ModelType, DestinationImp > LaxFrPartParamsType;

  //! maker class for the lax part
  typedef Decomposed::Operator::Factory< 0, 1, LaxFrPartParamsType > LaxFrMakerType;

  //! the type for the sum of the two decomposed operators.
  typedef Decomposed::Operator::Combined<
      typename LinVelMakerType::DecomposedOperatorType,
      typename LaxFrMakerType::DecomposedOperatorType > CompositeType;

  //! base type for the composite type
  typedef Decomposed::Operator::VirtualInterface< DestinationType,
      DestinationType > DecomposedMappingType;

public:
  //! constructor
  //! \param model      model instance providing the decomposed velocity
  //!                   function
  //! \param space      discrete function space
  Composite (const DiscreteFunctionSpaceType & space, ModelType & model)
      : linVelMaker_( space, model ), laxFrMaker_( space, model ),
        composite_( 0 )
  {
    if (velComponents > 0)
    {
      composite_ = new CompositeType(
          linVelMaker_.getOperator() + laxFrMaker_.getOperator() );
    }
  }

  //! return the decomposed operator
  CompositeType & getOperator ()
  {
    return *composite_;
  }

  //! return the decomposed operator as mapping
  DecomposedMappingType & getMapping ()
  {
    return *composite_;
  }

  //! deconstructor
  ~Composite ()
  {
    if (composite_ != 0)
    {
      delete composite_;
    }
  }

private:
  LinVelMakerType linVelMaker_;
  LaxFrMakerType laxFrMaker_;
  CompositeType *composite_;
};

/* @} */
} // end namespace Convection
} // end namespace Fv
} // end namespace Operator
} // end namespace Lib
} // end namespace Dune::RBFem
} // end namespace Dune

#endif  /*__CONVECTION_HH__*/
/* vim: set et sw=2: */
