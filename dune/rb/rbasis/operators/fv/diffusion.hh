#ifndef  __DIFFUSION_HH__
#define  __DIFFUSION_HH__

#include  "finvol.hh"
#include  "../discretedecomposedoperator.hh"
#include  "../common/makedecomposedoperators.hh"
#include  "../../functions/localmuscaledfunction.hh"
#include  "../../functions/discretedecomposedfunction.hh"
#include  "../../functions/common/makedecomposedfunctions.hh"
#include "../../linevol/linevolmodel.hh"

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Operator
{
namespace Fv
{
namespace Diffusion
{

#define DIFF_FACTOR 1.

/** @addtogroup fv
 * @{ */

/**
 * @brief mu scaled addend of decomposed discretization operator for the
 * diffusion term
 *
 * A part is a component together with a coefficient function
 * \f$\sigma(\mu)\f$.
 *
 * This part implements
 *
 * \f[
 * (b_{\mathrm{diff}}^{q,r})_i :=
 *  \frac{-1}{|T_i|}\sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)}
 *  \sigma^q_{d}(\mu)\sigma^r_{b_{\mathrm{dir}}}(\mu)
 *  d^q(\mathbf{s}_{ij}) \frac{|e_{ij}|}{|\mathbf{s}_{i} - \mathbf{s}_{j}|}
 * b_{\mathrm{dir}}^r(\mathbf{s}_{ij})
 * \f]
 *
 * where \f$q,r\f$ are the set via the template parameters diffCompIndex and
 * dirCompIndex.
 *
 * With help of the DiffMuScaledFunctionComposite class, function parts of this
 * type can be automatically assembled - based on a model's diffusion and
 * dirichlet functions - to a scalar mu scaled function \f$b_{\mathrm{diff}}\f$
 * for the diffusion part of a numerical Lax-Friedrichs scheme
 *
 * \f[
 *   b_{\mathrm{diff}} := 
 *    \sum_{q=1}^{Q_{\mathrm{diff}}} \sum_{r=1}^{Q_{\mathrm{dir}}}
 *      b_{\mathrm{diff}}^{q,r}
 * \f]
 *
 * Together with an operator assembled by DiffFinVolComposite implementing the
 * decomposed operator \f$L_{\mathrm{conv}}\f$, we get a finite volume scheme
 * for a diffusion equation by evaluating
 *
 * \f[
 * u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{diff}} u^{k} +
 * b_{\mathrm{diff}} \right).
 * \f]

 * @see DiffFinVolComposite, DiffMuScaledFunctionComposite
 */
template<int diffCompIndex, int dirCompIndex, class ModelImp>
class LinearLocalFunctionPart
  : public IConstantFluxPart< ModelImp >
{
public:
  //! model type
  typedef ModelImp                                                   ModelType;
  //! type of the decomposed diffusion function
  typedef typename ModelType :: DecomposedDiffusionType              DecomposedDiffusionType;
  //! type of a function component of the decomposed diffusion function
  typedef typename DecomposedDiffusionType :: ComponentType          DiffusionComponentType;
  //! range type of a decomposed diffusion function
  typedef typename DecomposedDiffusionType :: RangeType              DiffusionRangeType;
  //! type of the decomposed dirichlet function
  typedef typename ModelType :: DecomposedDirichletType              DecomposedDirichletType;
  //! type of a function component of the decomposed dirichlet function
  typedef typename DecomposedDirichletType :: ComponentType          DirichletComponentType;
  //! range type of a decomposed dirichlet function
  typedef typename DecomposedDirichletType :: RangeType              DirichletRangeType;
  //! function space type of solution function
  typedef typename ModelType :: FunctionSpaceType                    FunctionSpaceType;
  //! domain type of solution function
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range type of solution function
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! field type of the range vector space (double)
  typedef typename FunctionSpaceType :: RangeFieldType               FieldType;
  //! grid part type
  typedef typename ModelType :: GridPartType                         GridPartType;
  //! type of a grid entity
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: Entity                                                Entity;
  //! type of a pointer to a grid entity
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: EntityPointer                                         EntityPointer;
  //! intersection iterator type
  typedef typename Entity :: LeafIntersectionIterator                IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator :: Intersection              Intersection;
  //! quadrature on a grid entity
  typedef CachingQuadrature< GridPartType, 0 >                       VolumeQuadratureType;
  //! quadrature on a grid face entity
  typedef CachingQuadrature< GridPartType, 1 >                       FaceQuadratureType;

  //! dimension of domain vector type
  static const int dimDomain = DomainType::dimension;
  //! dimension of range vector type
  static const int dimRange  = RangeType::dimension;
  //! the quadrature points should be the mid points of the faces 
  typedef FieldVector< FieldType, dimDomain - 1 >                    MidType;
  //! vector space type for coordinates on a boundary face
  typedef FieldVector< FieldType, dimDomain - 1 >                    FaceDomainType;
  static const unsigned int numComps = DecomposedDirichletType :: numComps *
    DecomposedDiffusionType :: numComps;
private:

  //! evalutes the function for one intersection
  template<class ResultType>
  void evaluateLocal(const Intersection & it,
                     FaceDomainType & localX,
                     ResultType & gEl)
  {
    const DomainType normal   = it.integrationOuterNormal(localX);
    const DomainType localMid = it.geometryInInside().global(localX);
    const DomainType midPoint = it.geometry().global(localX);

    typedef typename Intersection :: EntityPointer                   EntityPointer;
    EntityPointer epLeft  = it.inside();

    // get barycenters \f$ \mathbf{s}_{i} and \mathbf{s}_j \f$
    VolumeQuadratureType volQuad(*epLeft, 0);
    DomainType baryLeft  = epLeft->geometry().global(volQuad.point(0));

    // compute \f$ d^{q}(\mathbf{s}_{ij} \f$
    DiffusionRangeType diff;
    typedef typename DiffusionComponentType :: LocalFunctionType     DiffusionLocalFunctionType;
    DiffusionLocalFunctionType diffLocalFunc = diffComp_.localFunction(*epLeft);
    diffLocalFunc.evaluate(localMid, diff);

    // compute \f$ b^{r}_{\mathrm{dir}}(\mathbf{s}_{ij} \f$
    DirichletRangeType dir;
    typedef typename DirichletComponentType :: LocalFunctionType     DirichletLocalFunctionType;
    DirichletLocalFunctionType dirLocalFunc = dirComp_.localFunction(*epLeft);
    dirLocalFunc.evaluate(localMid, dir);

    // compute \f$ \frac{|e_{ij}|}{|\mathbf{s}_i-\mathbf{s}_j|} \f$
    FieldType factor = (normal * normal)/ ( 2.0 * ( (midPoint - baryLeft) * normal) );

    gEl  = dir;
    gEl *= factor * diff[0] * DIFF_FACTOR;
#if defined(DEBUG) && DEBUG > 10000
    std::cout << "in DiffLinLocalFunctionPart :: evaluateLocal(en, ...)\n";
    std::cout << "factor: " << factor << " gEl: " << gEl << " diff: " << diff << std::endl;
#endif
  }

public:
  //! constructor
  //! \param model 	the model providing decomposed functions for velocity and dirichlet boundary data
  //! \param gridPart   the grid part instance
  LinearLocalFunctionPart(ModelType & model, const GridPartType & gridPart)
    : model_(model),
      gridPart_(gridPart),
      decDiff_(model_.diffusion()),
      diffComp_(decDiff_.component(diffCompIndex)),
      decDir_(model_.dirichlet()),
      dirComp_(decDir_.component(dirCompIndex))
  {
    muDependent_ = ( decDiff_.isMuDependent(diffCompIndex) 
                     || decDir_.isMuDependent(dirCompIndex) );
  }

  //! evaluate the function locally on the grid entity set by init()
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret)
  {
    IntersectionIterator endnit = gridPart_.iend(this->getEn());
    ret = RangeType(0);
    for( IntersectionIterator nit = gridPart_.ibegin(this->getEn()) ; nit != endnit ; ++nit )
    {
      if( nit->boundary() )
      {
        // compute local face mid point
        FaceQuadratureType faceQuadInner(gridPart_, *nit, 0, FaceQuadratureType::INSIDE);
        MidType localMid = faceQuadInner.localPoint(0);

        // compute global face mid point
        DomainType midPoint = nit->geometry().global( localMid );

        if( model_.boundaryType(nit, time_, localMid, midPoint) == Example::BoundaryType(Dune::RBFem::Example::Dirichlet) )
        {
          RangeType val;
          evaluateLocal(*nit, localMid, val);
          ret += val;
        }
      }
    }

    VolumeQuadratureType volQuad(this->getEn(), 0);
    double vol_1;
    this->volumeElement(this->getEn(), volQuad, vol_1);
    ret *= vol_1;
  }

  //! return the \f$\mu\f$ - dependent coefficient
  inline double coefficient() const
  {
    return decDiff_.coefficient(diffCompIndex) * decDir_.coefficient(dirCompIndex);
  }

  inline std::string symbolicCoefficient() const
  {
    return decDiff_.symbolicCoefficient(diffCompIndex) + std::string("*") + decDir_.symbolicCoefficient(dirCompIndex);
  }
  //! returns wether the local function part depends on \f$\mu\f$. Otherwise,
  //! the coefficient() function returns a constant value.
  bool isMuDependent() const
  {
    return muDependent_;
  }

  //! returns wether the local function part is time dependent
  bool isTimeDependent() const
  {
    //!\todo
    return false;
  }

  void setTime(double time)
  {
    decDiff_.setTime(diffCompIndex, time);
    decDir_.setTime(diffCompIndex, time);
    time_ = time;
  }

private:
  ModelType               &model_;
  const GridPartType      &gridPart_;
  DecomposedDiffusionType &decDiff_;
  DiffusionComponentType  &diffComp_;
  DecomposedDirichletType &decDir_;
  DirichletComponentType  &dirComp_;
  bool                     muDependent_;
  double                   time_;

};

/**
 * @brief mu scaled addend of decomposed discretization flux for the diffusion term.
 *
 * A part is a component together with a coefficient function \f$\sigma(\mu)\f$.
 *
 * This flux part implements
 *
 * \f{eqnarray*}{
 * g_{\mathrm{diff}}^{q}(u_i, u_j) 
 *   &:=&
 *   \sigma^q_{\mathbf{d}}(\mu) \mathbf{d}^q(\mathbf{s}_{ij})
 *   \frac{|e_{ij}|}{|\mathbf{s}_{i} - \mathbf{s}_{j}|}
 *   (u_i - u_j)
 *   \\
 * g_{\mathrm{diff,dir}}^{q}(u_i)
 *   &:=&
 *   \sigma^q_{\mathbf{d}}(\mu) \mathbf{d}^q(\mathbf{s}_{ij})
 *   \frac{|e_{ij}|}{|\mathbf{s}_{i} - \mathbf{s}_{j}|}
 *   u_i,
 * \f}
 *
 * where \f$q\f$ is set via the template parameter diffCompIndex.
 *
 * The FinVol class generates an operator \f$L_{\mathrm{diff}}^q\f$ out of
 * this flux part by evaluating
 *
 * \f[
 * \left(L_{\mathrm{diff}}^{q}[u]\right)_i := 
 *   \frac{1}{|T_i|}\left(
 *                       \sum_{j\in \mathcal{N}_{\mathrm{in}}(i)} g_{\mathrm{diff}}^q(u_i, u_j) +
 *                       \sum_{j\in \mathcal{N}_{\mathrm{dir}}(i)} g_{\mathrm{diff,dir}}^q(u_i)
 *                 \right)
 * \f]
 *
 * With help of the DiffFinVolComposite class, flux parts of this type can be
 * automatically assembled - based on a model's diffusion and dirichlet
 * functions - to a decomposed operator \f$L_{\mathrm{diff}}\f$ for the
 * diffusion part of a numerical Lax-Friedrichs scheme
 *
 * \f[
 * L_{\mathrm{diff}} := \sum_{q=1}^{Q_d} L_{\mathrm{diff}}^q.
 * \f]
 *
 * Together with a combined mu scaled function DiffMuScaledFunctionComposite
 * implementing the decomposed function \f$b_{\mathrm{diff}}\f$ we get a finite
 * volume scheme for a diffusion equation by evaluating
 * \f[ 
 *   u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{diff}} u^{k} +
 *   b_{\mathrm{diff}} \right). 
 * \f] 
 *
 * @see FinVol, DiffFinVolComposite, DiffMuScaledFunctionComposite, DiffLinLocalFunctionPart
 */
template<int diffCompIndex, class ModelImpType>
class LinearFluxPart
  : public IFluxPart< ModelImpType >
{
public:
  //! model type
  typedef ModelImpType                                               ModelType;
  //! type of the decomposed diffusion function
  typedef typename ModelType :: DecomposedDiffusionType              DecomposedDiffusionType;
  //! type of a function component of the decomposed diffusion function
  typedef typename DecomposedDiffusionType :: ComponentType          DiffusionComponentType;
  //! range type of a decomposed diffusion function
  typedef typename DecomposedDiffusionType :: RangeType              DiffusionRangeType;
  //! type of the decomposed dirichlet function
  typedef typename ModelType :: DecomposedDirichletType              DecomposedDirichletType;
  //! type of a function component of the decomposed dirichlet function
  typedef typename DecomposedDirichletType :: ComponentType          DirichletComponentType;
  //! range type of a decomposed dirichlet function
  typedef typename DecomposedDirichletType :: RangeType              DirichletRangeType;
  //! function space type of solution function
  typedef typename ModelType :: FunctionSpaceType                    FunctionSpaceType;
  //! domain type of solution function
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range type of solution function
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! field type of the range vector space (double)
  typedef typename FunctionSpaceType :: RangeFieldType               FieldType;
  //! grid part type
  typedef typename ModelType :: GridPartType                         GridPartType;
  //! type of a grid entity
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: Entity                                                Entity;
  //! type of a pointer to a grid entity
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: EntityPointer                                         EntityPointer;
  //! intersection iterator type
  typedef typename Entity :: LeafIntersectionIterator                IntersectionIterator;
  //! intersection type
  typedef typename IntersectionIterator :: Intersection              Intersection;

  //! dimension of domain vector type
  static const int dimDomain = DomainType::dimension;
  //! dimension of range vector type
  static const int dimRange = RangeType::dimension;
  //! vector space type for coordinates on a boundary face
  typedef FieldVector< FieldType, dimDomain - 1 >                    FaceDomainType;
  //! quadrature on a grid entity
  typedef CachingQuadrature< GridPartType, 0 >                       VolumeQuadratureType;
public:
  //! constructor
  //! \param model 	the model providing decomposed functions for velocity and dirichlet boundary data
  LinearFluxPart (ModelType & model)
    : model_(model),
      decDiff_(model_.diffusion()),
      diffComp_(decDiff_.component(diffCompIndex))
  {
    muDependent_ = decDiff_.isMuDependent(diffCompIndex);
  }

  template < class ArgumentTuple, class ResultType>
  double numericalFlux(const Intersection & it,
                       const FaceDomainType & localX,
                       const ArgumentTuple & uLeft,
                       const ArgumentTuple & uRight,
                       ResultType & gLeft,
                       ResultType & gRight) const
  {
    const DomainType normal   = it.integrationOuterNormal(localX);
    const DomainType localMid = it.geometryInInside().global(localX);

    EntityPointer epRight = it.outside();
    Entity & enRight      = const_cast<Entity &> (*epRight);

    EntityPointer epLeft  = it.inside();
    Entity & enLeft       = const_cast<Entity &> (*epLeft);

    VolumeQuadratureType volQuad(enLeft,0);

    DomainType baryRight = enRight.geometry().global(volQuad.point(0));
    DomainType baryLeft  = enLeft.geometry().global( volQuad.point(0));

    DiffusionRangeType diff;
    typedef typename DiffusionComponentType :: LocalFunctionType     DiffusionLocalFunctionType;
    DiffusionLocalFunctionType diffLocalFunc = diffComp_.localFunction(*epLeft);
    diffLocalFunc.evaluate(localMid, diff);

    //! \note \f$ \frac{\langle n, n \rangle}{s \cdot n} = \frac{|e_{ij}|\|n\|}{s\cdot n} \f$
    //! \note factor changes sign with normal!!
    FieldType factor = (normal * normal)/ ((baryRight - baryLeft) * normal);

    for( int l = 0 ; l < dimRange ; ++l ) {
      gLeft[l] = (factor) * (diff[l]) * (uRight[l]-uLeft[l]);
    }
    gRight = -gLeft;

    //! \todo this does not make sense if we are mu dependent. Then we probably
    //! need some further method like "dtEstimate" that might also depend on mu.
    return diff[0] * factor * DIFF_FACTOR;
  }

  //! numerical flux at the boundary
  template <class ArgumentTuple, class ResultType>
  double boundaryNumericalFlux(const Intersection& it,
                               const FaceDomainType& localX,
                               const ArgumentTuple& uEl,
                               ResultType& gEl) const 
  {
    RangeType uNeigh;
    const DomainType midPoint = it.geometry().global(localX); 
    const DomainType localMid = it.geometryInInside().global(localX);

    if(model_.boundaryType(it,time_,localX,midPoint) == Example::BoundaryType(Dune::RBFem::Example::Dirichlet)
       || model_.boundaryType(it,time_,localX,midPoint) == Example::BoundaryType(Dune::RBFem::Example::OutFlow))
    {
      DomainType normal        = it.integrationOuterNormal(localX);

      EntityPointer epLeft = it.inside();
      Entity & enLeft      = const_cast<Entity &> (*epLeft);
      
      VolumeQuadratureType volQuad(enLeft,0);

      DomainType baryLeft  = enLeft.geometry().global( volQuad.point(0) );

      DiffusionRangeType diff;
      typedef typename DiffusionComponentType :: LocalFunctionType     DiffusionLocalFunctionType;
      DiffusionLocalFunctionType diffLocalFunc = diffComp_.localFunction(*epLeft);
      diffLocalFunc.evaluate(localMid, diff);

      //! \note \f$ \frac{\langle n, n \rangle}{s \cdot n} = \frac{|e_{ij}|\|n\|}{s\cdot n} \f$
      //! \note factor changes sign with normal
      FieldType factor = (normal * normal) / (2.0 * ( (midPoint - baryLeft) * normal));

      for(int l=0; l<dimRange; l++){
        gEl[l] = (-factor) * diff[l] * (uEl[l]);
      }
#if defined(DEBUG) && DEBUG > 10000
      std::cout << "in LinDiffFluxPart :: boundaryNumericalFlux(it, ...):\n";
      std::cout << "this factor: " << factor << " this gEl: " << gEl << " this uEl: " << uEl << " diff: " << diff << std::endl;
#endif

      return (diff[0] * factor * DIFF_FACTOR);
    } else { // NoFlow, Neuman, OutFlow
      gEl=0.0;
      return 0.0;
    }
  }

  //! \f$\sigma(\mu)\f$
  double coefficient() const
  {
    return decDiff_.coefficient(diffCompIndex);
  }

  inline std::string symbolicCoefficient() const
  {
    return decDiff_.symbolicCoefficient(diffCompIndex);
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }

  bool isTimeDependent() const
  {
    //! \todo
    return false;
  }

  void setTime(double time)
  {
    decDiff_.setTime(diffCompIndex, time);
    time_ = time;
  }

private:
  ModelType               &model_;
  DecomposedDiffusionType &decDiff_;
  DiffusionComponentType  &diffComp_;
  bool                     muDependent_;
  double                   time_;
};

/** \brief constructs a decomposed operator out of local flux parts
 *
 * This class assembles finite volume operators of type FinVol parametrised
 * with LinDiffFluxPart specializations to a decomposed operator
 * \f$L_{\mathrm{diff}}\f$ 
 *
 * \f[
 * L_{\mathrm{diff}} := \sum_{q=1}^{Q_d} L_{\mathrm{diff}}^q
 * \f]
 *
 * Together with a mu scaled function DiffMuScaledFunctionComposite
 * implementing the decomposed function \f$b_{\mathrm{diff}}\f$ we get a finite
 * volume scheme for a diffusion equation by evaluating
 * \f[ 
 *   u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{diff}} u^{k} +
 *   b_{\mathrm{diff}} \right). 
 * \f] 
 *
 * @see FinVol, DiffLinLocalFunctionPart, DiffMuScaledFunctionComposite, LinDiffFluxPart
 */
template<class ModelImp, class DestinationImp>
class Composite
{
public:
  //! model type
  typedef ModelImp                                                   ModelType;
  //! discrete function type of solution function
  typedef DestinationImp                                             DestinationType;
  //! discrete function space type of solution function
  typedef typename DestinationImp :: DiscreteFunctionSpaceType       DiscreteFunctionSpaceType;
  //! number of diffusion components
  static const int diffComponents = ModelType :: DecomposedDiffusionType :: numComps;

  //! traits class for the maker class of the diffusion part
  typedef Decomposed::Operator::FromFluxFactoryParams1< LinearFluxPart,
                                                 Operator::Fv::Default, ModelType,
                                                 DestinationType >   LinDiffPartParamsType;
  //! maker class for the linvel part
  typedef Decomposed::Operator::Factory< 0, diffComponents,
                                  LinDiffPartParamsType >            LinDiffMakerType;

  //! decomposed operator type
  typedef typename LinDiffMakerType :: DecomposedOperatorType        CompositeType;

  //! base type for the composite type
  typedef Decomposed::Operator::VirtualInterface< typename CompositeType :: DomainType,
                             typename CompositeType :: RangeType >   DecomposedMappingType;

public:
  //! constructor
  //! \param model      model instance providing the decomposed diffusion
  //!                   function
  //! \param space      discrete function space
  Composite(const DiscreteFunctionSpaceType & space, ModelType & model)
    : linDiffMaker_(space, model), composite_(0)
  {
    if( diffComponents > 0 ) {
      composite_ = &(linDiffMaker_.getOperator());
    }
  }

  //! return the decomposed operator
  CompositeType & getOperator()
  {
    return *composite_;
  }

  //! return the decomposed operator as mapping
  DecomposedMappingType & getMapping()
  {
    return *composite_;
  }

  //! destructor
  ~Composite()
  {
  }

private:
  LinDiffMakerType  linDiffMaker_;
  CompositeType    *composite_;
};

/** @brief constructs decomposed function out of local function parts
 *
 * This class assembles discrete decomposed functions with local function parts
 * DiffLinLocalFunctionPart \f$b_{\mathrm{lax}}\f$ to a mu scaled function for
 * the diffusive part of a numerical Lax-Friedrichs scheme.
 *
 * \f[
 * b_{\mathrm{diff}} := \sum_{q=1}^{Q_d} \sum_{r=1}^{Q_{\mathrm{dir}}}
 * b_{\mathrm{diff}}^{q,r} 
 * \f]
 *
 * Together with a combined operator DiffFinVolComposite implementing the
 * decomposed operator \f$L_{\mathrm{diff}}\f$, we get a finite volume scheme
 * for a diffusion equation by evaluating
 *
 * \f[
 * u^{k+1} = u^{k} + \Delta t \left( L_{\mathrm{diff}} u^{k} +
 * b_{\mathrm{diff}} \right).
 * \f]
 *
 * @see DiffLinLocalFunctionPart, DiffMuScaledFunctionComposite, LinDiffFluxPart
 */
template<class ModelImp, class DiscreteFunctionType>
class MuScaledFunctionComposite
{
public:
  //! model type
  typedef ModelImp                                                   ModelType;

  //! number of diffusion components
  static const int diffComponents = ModelType :: DecomposedDiffusionType :: numComps;
  //! number of dirichlet components
  static const int dirComponents  = ModelType :: DecomposedDirichletType :: numComps;

  //! traits class for the maker class of the diffusion part
  typedef Decomposed::Function::FactoryParams2< LinearLocalFunctionPart,
                                         ModelType, diffComponents,
                                         dirComponents >             DiffPartParamsType;
  //! maker class for the diffusion part
  typedef Decomposed::Function::AnalyticalFactory< 0, diffComponents*dirComponents,
                                  DiffPartParamsType,
                                  DiscreteFunctionType >             DiffMakerType;

  //! base class for the resulting decomposed function
  typedef typename DiffMakerType :: DecomposedFunctionType
            :: DecomposedVectorType                                  DecomposedVectorType;

  //! decomposed function type
  typedef typename DiffMakerType :: DecomposedFunctionType           CompositeType;

  //! discrete function space type
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;

public:
  //! constructor constructing the decomposed function out of local function parts
  //! \param model      model instance providing decomposed diffusion and dirichlet functions
  //! \param space      discrete function space
  MuScaledFunctionComposite(ModelType & model,
                                const DiscreteFunctionSpaceType & space)
    : diffMaker_(model, space), composite_(0)
  {
    composite_ = &(diffMaker_.getFunction());
  }

  //! return the composite function
  CompositeType & getFunction()
  {
    return *composite_;
  }

  //! desctructor
  ~MuScaledFunctionComposite()
  { }

private:
  DiffMakerType  diffMaker_;
  CompositeType *composite_;
};

/*@}*/
} // namespace Diffusion
} // namespace Fv
} // namespace Operator
} // namespace Lib
} // end namespace Dune::RBFem
} // end namespace Dune

#endif  /*__DIFFUSION_HH__*/
/* vim: set et sw=2: */
