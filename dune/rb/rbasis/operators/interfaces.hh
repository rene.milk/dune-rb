#ifndef DUNE_RB_OPERATORS_INTERFACES_HH_
#define DUNE_RB_OPERATORS_INTERFACES_HH_

#include "../../misc/ducktyping.hh"
#include "../common/coefficientif.hh"

#include  <dune/fem/operator/common/localoperator.hh>

namespace Dune {
namespace RBFem {
namespace Lib
{
namespace Operator
{

/** @brief interface class for a linear addend scaled by its coefficient
 * \f$\sigma(\mu)\f$ in an affinely decomposed operator.
 *
 * \attention An operator of this type does not need to know about its
 * coefficient, i.e. has no methods to return it!
 */
template<class DiscreteFunction, class LocalImp>
class ILocalMuScaled
: public LocalOperatorInterface< DiscreteFunction, DiscreteFunction,
                                 typename DiscreteFunction :: DomainType :: field_type,
                                 LocalImp >
{
public:
  typedef typename DiscreteFunction :: DomainType :: field_type    ScalarType;

public:

  virtual ~ILocalMuScaled() {}


  /** @brief updates the scalar @f$\sigma(\mu)@f$ the local operator is
   * multiplied with
   */
  __DUCK_START__
  void scaleIt(ScalarType scalar)
  __DUCK_END__

  /** @brief sets the new time variable
  */
  __DUCK_START__
  void setTime(double time)
  __DUCK_END__
};

/** @brief interface class for a linear addend in an affinely %decomposed
 * operator */
template<class DiscreteFunction, class LocalImp>
class ILocalParametrized
  : public Dune::LocalOperatorDefault< DiscreteFunction, DiscreteFunction,
                                 typename DiscreteFunction :: DomainType :: field_type,
                                 LocalImp >,
    public ITimeCoefficient<typename DiscreteFunction :: DomainType :: field_type>

{
public:
  typedef typename DiscreteFunction :: DomainType :: field_type      ScalarType;

public:
  ILocalParametrized() : scalar_(1.0) {};

  virtual ~ILocalParametrized()
  {
  }

  /** @brief updates the scalar @f$\sigma(\mu)@f$ the local operator addend is
   * multiplied with
   */
  void scaleIt(ScalarType scalar)
  {
    scalar_ = scalar;
  }

  /** @brief sets the new time variable
   */
  __DUCK_START__
  void setTime(double time)
  __DUCK_END__

protected:
  ScalarType scalar_;
};

}  // namespace Operator
}  // namespace Lib
} // end namespace RBFem
} // end namespace Dune

#endif /* DUNE_RB_OPERATORS_INTERFACES_HH_ */

