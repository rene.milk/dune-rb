#ifndef  __DISCRETEDECOMPOSEDOPERATOR_HH__
#define  __DISCRETEDECOMPOSEDOPERATOR_HH__

#include  "../../misc/tuples.hh"
#include  <dune/fem/operator/discreteoperatorimp.hh>

#include  "localmuscaledoperator.hh"
#include  "decomposedoperator.hh"
#include  "../linevol/spaceoperatorwrapper.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Operator
{

/** @ingroup decomposed
 * @brief creates an affinely decomposed operator from a given tuple of
 * LocalOperator's
 *
 * This class implements an operator that can be written in the form
 *
 * \f[
 * \mathcal{L}(\mu)[U] = \sum_{q=1}^{Q} \sigma^{q}(\mu) L^{q}[U]
 * \f]
 *
 * with scalar and \f$\mu\f$-dependent coefficient function \f$\sigma^{q}\f$
 * and - in RB applications linear - and space variable dependent operator
 * components \f$L^{q}\f$.
 *
 * \note The class itself is *not* a real Dune::Operator, but it encapsulates a
 * Dune::DiscreteOperator that by default applies the operator
 * \f$\mathcal{L}(\mu)\f$ on a discrete function by evaluating the linear
 * combination of the operator components.
 *
 * Furthermore, it serves as a container for the components and coefficient
 * functions.
 *
 * The tuple needs to be an implementation of LocalMuScaledDecorator objects.
 *
 * @sa
 *   - MakeDecomposedOperator for a convenient way to create generate a
 *   DiscreteDecomposedOperator from a ParametrizedOperatorInterface
 *   implemenation
 *   - FinVol for an example implemenation of an ParametrizedOperatorInterface
 *   class
 *
 * \todo write ParametrizedOperatorInterface
 * \todo rename to DiscreteDecomposedOperatorContainer???
 **/
template <typename DecomposedLocalOperatorTuple,
          typename DFDomainType,
          typename DFRangeType>
class Discrete
  : public
    Interface<
      DFDomainType,
      DFRangeType,
      Discrete<
        DecomposedLocalOperatorTuple, DFDomainType, DFRangeType> >
{
typedef Interface< DFDomainType, DFRangeType,
                                        Discrete
                                          < DecomposedLocalOperatorTuple,
                                            DFDomainType,
                                            DFRangeType > >          BaseType;
public:
  //! decomposed mapping type for this decomposed operator
  typedef typename BaseType :: DecomposedMappingType                 DecomposedMappingType;
  typedef Lib::Operator::LocalMuScaled< DecomposedLocalOperatorTuple,
                                 DFDomainType, DFRangeType >         CombinedLocalOperatorType;
  //! type of domain of operator
  typedef DFDomainType                                               DomainType;
  //! type of discrete function space
  typedef typename DomainType :: DiscreteFunctionSpaceType           DiscreteFunctionSpaceType;
  //! field type for domain vector space
  typedef typename DomainType :: DomainFieldType                     DomainFieldType;
  //! type of range of operator
  typedef DFRangeType                                                RangeType;
  //! field type for range vector space
  typedef typename RangeType :: RangeFieldType                       RangeFieldType;

  //! base mapping type for an operator component
  typedef Mapping< DomainFieldType, RangeFieldType, DomainType,
                   RangeType >                                       MappingType;
  //! underlying operator class See also @ref LocalMuScaledOperator
  typedef DiscreteOperator< CombinedLocalOperatorType, DFDomainType,
                            DFRangeType >                            CompleteOperatorType;
  //! discrete operator returned by complete() that is usable in an ode solver,
  //! because it is derived from Dune::SpaceOperatorInterface See also @ref
  //! SpaceOperatorWrapper.
  typedef Lib::Operator::TimeSpaceWrapper< CompleteOperatorType, MappingType,
                                DFDomainType >                       CompleteType;

  typedef Lib::Operator::SpaceOperatorWrapper< MappingType, DFDomainType >      DumbCompleteType;
  //! more general type complete discrete operator that can be used for a
  //! result of a sum of complete operators. See also @ref SpaceOperatorMapping
  typedef Lib::Operator::TimeSpaceMapping< MappingType, DFDomainType >          SpaceMappingType;
  //! type of an operator component \f$L^q\f$
  typedef MappingType                                                ComponentType;

  //! vector type for coefficients 
  //! \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$
  typedef std :: vector< double >                                    CoefficientList;
  //! vector type for symbolic coefficients
  //! \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$
  typedef std :: vector< std :: string >                             SymbolicCoefficientList;
  //! number of components/coefficients
  static const int numComps = RB::Tuple :: Size< DecomposedLocalOperatorTuple > :: value;
private:
  //! list type for operator components
  typedef std :: vector< ComponentType >                             OperatorComponentList;
  //! this type
  typedef Discrete< DecomposedLocalOperatorTuple,
                                      DomainType, RangeType >        ThisType;
private:
  //! This is a simple wrapper that surrounds a possible g++ bug, but also simplifies
  //! the memory management of our discrete operator tuples.
  template<class LocalOperatorImp>
  class DiscreteOperatorPtr
  {
    typedef LocalOperatorImp                                         LocalOperatorType;
    typedef typename LocalOperatorType :: FirstParamType             DomainType;
    typedef typename LocalOperatorType :: SecondParamType            RangeType;
    typedef DiscreteOperator< LocalOperatorType, DomainType,
                              RangeType >                            DiscreteOperatorUnderlying;
  public:
    DiscreteOperatorPtr() : underlying_(0) {};

    //! sets the underlying discrete operator
    void setUnderlying( LocalOperatorType & lop ) 
    {
      if( underlying_ != 0 )
      {
        std::cout << "Underlying discrete operator to be changed.\nDelete the old implementation!" << std::endl;
        delete underlying_;
      }
      underlying_ = new DiscreteOperatorUnderlying(lop); /*, false, true); */
    }

    //! wrap the reference operator
    DiscreteOperatorUnderlying & operator*()
    {
      return *underlying_;
    }

    //! destructor cleans the discrete function again.
    ~DiscreteOperatorPtr()
    {
      if( underlying_ != 0 ) {
        delete underlying_;
      }
    }

  private:
    DiscreteOperatorUnderlying * underlying_;
  };

  //! helper struct for the creation of type of tuples of DiscreteFunction
  // realisations out of the given type of tuples of LocalFunction realisations.
  template <class A>
  struct MakeDiscreteOperatorTypePtrFromLocalDiscreteOperatorType
  {
    typedef DiscreteOperatorPtr<A> Type;
  };

  //! partial specialization for tuples of references
  template <class A>
  struct MakeDiscreteOperatorTypePtrFromLocalDiscreteOperatorType<A&>
  {
    typedef DiscreteOperatorPtr<A> Type;
  };

  //! helper class that creates a list of OperatorComponent objects out of the
  //! given tuple of LocalFunction objects.
  class MakeDiscreteOperatorFromLocalDiscreteOperator
  {
  public:
    MakeDiscreteOperatorFromLocalDiscreteOperator(OperatorComponentList& opcList)
      : opCompList_(opcList)
    {};

    template <class LocalOpType, class OpTypePtr>
    void visit(LocalOpType & lop, OpTypePtr & op)
    {
      op.setUnderlying(lop);

      ComponentType opComp(*op);
      opCompList_.push_back( opComp );
    }
  private:
    OperatorComponentList & opCompList_;
  };

  //! helper class collecting all coefficients of the underlying operators and
  //! pushing them at the end of a list.
  class CollectCoefficients
  {
  public:
    //! constructor
    //! \param coeffs   list of coefficients that is to be filled
    CollectCoefficients(CoefficientList& coeffs)
      : coeffs_(coeffs)
    { } 

    template<class LocalOpType>
    void visit(LocalOpType & lop)
    {
      double scalar = lop.coefficient();
      coeffs_.push_back(scalar);
    }
  private:
    CoefficientList& coeffs_;
  };

  //! helper class collecting all coefficients of the underlying operators and
  //! pushing them at the end of a list.
  class SymbolicCollectCoefficients
  {
  public:
    //! constructor
    //! \param coeffs   list of coefficients that is to be filled
    SymbolicCollectCoefficients(SymbolicCoefficientList& coeffs)
      : coeffs_(coeffs)
    { }

    template<class LocalOpType>
    void visit(LocalOpType & lop)
    {
      std::string symbCoeff = lop.symbolicCoefficient();
      coeffs_.push_back( symbCoeff );
    }
  private:
    SymbolicCoefficientList& coeffs_;
  };

  //! type of tuples of DiscreteFunction realisations made out of type of
  // tuples of LocalFunction realisations.
  typedef typename RB::Tuple
            :: ForEachT
                  < MakeDiscreteOperatorTypePtrFromLocalDiscreteOperatorType,
                    DecomposedLocalOperatorTuple > :: Type           DiscreteOperatorTuple;

private:
  //! hide copy constructor
  Discrete(ThisType & copy);

public:
  //! constructor
  //! \param space      discrete function space
  //! \param dloTuple   a tuple of local operators out of which the decomposed
  //!                   operator is created
  Discrete(const DiscreteFunctionSpaceType & space,
                             DecomposedLocalOperatorTuple & dloTuple)
    :
    localOpTuple_(dloTuple),
    operatorComponentList_(),
    discreteOpTuple_(),
    combLocal_(dloTuple),
    fullOp_(combLocal_),
    fullSpaceOp_(space, fullOp_)
  {
    RB::Tuple::ForEachValPair<DecomposedLocalOperatorTuple, DiscreteOperatorTuple>
      forEach(localOpTuple_, discreteOpTuple_);
    MakeDiscreteOperatorFromLocalDiscreteOperator visitor(operatorComponentList_);
    forEach.apply(visitor);

    CoefficientList coeffList;
    this->coefficients(coeffList);
    dumbFullOp_ = new MappingType(coeffList[0] * component(0));


    for (int i = 1; i < numComponents(); ++i)
    {
      *dumbFullOp_ = *dumbFullOp_ + coeffList[i] * component(i);
    }
    dumbFullSpaceOp_ = new DumbCompleteType(space, *dumbFullOp_);
  }

  //! destructor
  virtual ~Discrete()
  {
    delete dumbFullSpaceOp_;
    delete dumbFullOp_;
  }

  virtual CompleteType & complete()
  {
    return fullSpaceOp_;
  }

  virtual DumbCompleteType & dumbComplete()
  {
    return *dumbFullSpaceOp_;
  }

  virtual int numComponents() const
  {
    return numComps;
  }

  virtual const ComponentType& component(const int i) const
  {
    assert( i < numComponents() );
    return operatorComponentList_[i];
  }

  virtual void coefficients(CoefficientList& coeffs) const
  {
    RB::Tuple::ForEachVal<DecomposedLocalOperatorTuple>
      forEach(localOpTuple_);
    CollectCoefficients visitor(coeffs);
    forEach.apply(visitor);
  }

  virtual void symbolicCoefficients(SymbolicCoefficientList& coeffs) const
  {
    RB::Tuple::ForEachVal<DecomposedLocalOperatorTuple>
      forEach(localOpTuple_);
    SymbolicCollectCoefficients visitor(coeffs);
    forEach.apply(visitor);
  }

  bool isMuDependent(const int i) const
  {
    assert( i < numComponents() );
    std::cerr << "Warning: isMuDependent() is not yet implemented!" << std::endl;
    assert(false);
    return true;
  }

  bool isTimeDependent(const int i) const
  {
    assert( i < numComponents() );
    std::cerr << "Warning: isTimeDependent() is not yet implemented!" << std::endl;
    assert(false);
    return true;
  }

private:
  DecomposedLocalOperatorTuple &localOpTuple_;
  OperatorComponentList         operatorComponentList_;
  DiscreteOperatorTuple         discreteOpTuple_;
  CombinedLocalOperatorType     combLocal_;
  CompleteOperatorType          fullOp_;
  CompleteType                  fullSpaceOp_;
  MappingType                  *dumbFullOp_;
  DumbCompleteType             *dumbFullSpaceOp_;
}; // end of class DiscreteDecomposedOperator

}  // namespace Operator
}  // namespace Decomposed
} // end namespace Dune::RB
} // end namespace Dune

#endif  /*__DISCRETEDECOMPOSEDOPERATOR_HH__*/
/* vim: set et sw=2: */

