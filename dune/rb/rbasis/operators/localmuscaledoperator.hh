#ifndef  __LOCALMUSCALEDOPERATOR_HH__
#define  __LOCALMUSCALEDOPERATOR_HH__

#include  <dune/common/typetraits.hh>


#include  "../../misc/tuples.hh"
#include <dune/fem/operator/common/localoperator.hh>

#include "interfaces.hh"

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Operator
{

/**
 * @brief extension of the CombinedLocalOperator from dune-fem
 *
 * Additionally to the methods by CombinedLocalOperator this class implements
 * the method setTime()
 */
template<class A, class B>
class CombinedLocalMuScaled :
  public CombinedLocalOperator<A, B>
{
  //! base type to be decorated
  typedef CombinedLocalOperator< A, B >                              BaseType;
public:
  CombinedLocalMuScaled( A & a, B & b, bool printMsg = false )
    : BaseType(a, b, printMsg), a_(a), b_(b) {};

  inline void setTime(double time)
  {
    a_.setTime(time);
    b_.setTime(time);
  }

private:
  //! operators A and B
  A & a_;
  B & b_;

};

/** @ingroup decomposed
 * @brief extends an arbitrary local operator class.
 *
 * The class given by the template argument LocalOperatorBaseType is decorated
 * with new prepareGlobal() and finalizeGlobal() methods, that set and reset
 * the coefficients of the local operator before calling the base class's
 * versions of these methods.
 *
 * @tparam LocalOperatorBaseType  a @link LocalParametrizedOperatorInterface
 *                                local @f$mu@f$ scaled operator @endlink
 */
template <class LocalOperatorBaseType>
class LocalMuScaledDecorator : public LocalOperatorBaseType
{
  typedef LocalOperatorBaseType                                      BaseType;
public:
  //! constructor
  //!
  //! \note The base class needs to allow a copy constructor
  //! \param base  base object that is to be decorated
  LocalMuScaledDecorator(LocalOperatorBaseType & base)
    : LocalOperatorBaseType(base)
  { }

  //! new prepareGlobal method
  template <class FirstParamType>
  void prepareGlobal(FirstParamType & arg)
  {
    BaseType :: prepareGlobal(arg);
    (*this).setMu();
  }

  //! new prepareGlobal method
  template <class FirstParamType, class SecondParamType>
  void prepareGlobal(FirstParamType & arg, SecondParamType & dest)
  {
    BaseType :: prepareGlobal(arg, dest);
    (*this).setMu();
  }

  //! new finalize Global method
  void finalizeGlobal()
  {
    BaseType :: finalizeGlobal();
    (*this).unsetMu();
  }

private:
  //! reads in the coefficient \f$\sigma(\mu)\f$ and sets for use in applyLocal
  //! methods.
  void setMu()
  {
    double scalar = (*this).coefficient();
    (*this).scaleIt(scalar);
#if defined(DEBUG) && DEBUG > 1000
    std::cout << "in LocalMuScaledOperatorDecorator :: setMu():\n"
      << "coefficient = " << scalar << std::endl;
/*    std::cout << "after scale: " << BaseType :: BaseType :: scalar_ << std::endl;*/
#endif
  }
  
  //! reset the scalar coefficient
  void unsetMu()
  {
    (*this).scaleIt(1.0);
  }

private:
};

/** recursively constructs a linear combination of LocalMuScaledOperator
 * objects by adding them together.
 *
 * This is just a helper class used by LocalMuScaledOperator
 *
 * @tparam  i           number of elements in TupleType
 * @tparam  TupleType   tuple of discrete operators
*/
template <int i, class TupleType>
class LocalMuScaledDefaultCreator
{
public:
  //! type of i-th local operator in the tuple (if necessary the reference
  //! qualifier is removed)
  typedef typename TypeTraits< typename RB::Tuple :: ElementType< i, TupleType >
                                :: type > :: ReferredType            BBaseType;
  //! type of second local Operator to be combined (note, that the local
  //! operator is decorated)
  typedef LocalMuScaledDecorator< BBaseType >                BType;
  //! recursive template "call"
  typedef LocalMuScaledDefaultCreator< i-1, TupleType >      PrevType;
  //! type of the first local operator to be combined
  typedef typename PrevType :: type                                  AType;
  //! type of the combined local operator
  typedef CombinedLocalMuScaled< AType, BType >              type;
public:
  //! constructor
  LocalMuScaledDefaultCreator(TupleType & t)
    : a_(t),
      b_(RB::Tuple::ElementAccess<i>::get(t)),
      c_(a_.getCombinedOp(),b_) { }

  //! return the combined local operator
  type & getCombinedOp()
  {
    return c_;
  }

private:
  PrevType a_;
  BType b_;
  type c_;
};

template <class TupleType>
class LocalMuScaledDefaultCreator<0, TupleType>
{
public:
  typedef typename TypeTraits< typename RB::Tuple :: ElementType< 0, TupleType >
                                :: type > :: ReferredType            BBaseType;
  typedef LocalMuScaledDecorator< BBaseType >                BType;
  typedef BType                                                      type;
public:
  LocalMuScaledDefaultCreator(TupleType & t)
    : b_(RB::Tuple::ElementAccess<0>::get(t)) {}

  BType & getCombinedOp()
  {
    return b_;
  }

private:
  BType b_;
};


/**
 * @brief constructs a local affinely decomposed operator whose components can
 * be scaled by functions \f$\sigma^{1}(\mu),\ldots,\sigma^{Q}(\mu)\f$.
 *
 * \note It actually wraps a CombinedLocalMuScaledOperator.
 */
template <typename DecomposedLocalOperatorTuple,
          typename DFDomainType,
          typename DFRangeType>
class LocalMuScaled
  : public ILocalMuScaled
             <DFDomainType,
              LocalMuScaled<DecomposedLocalOperatorTuple,
                                    DFDomainType, DFRangeType> >
{
  //! the scalar coefficient's type
  typedef typename DFDomainType :: DomainFieldType                   ScalarType;
  //! realization of the LocalOperatorInterface
//  typedef ILocalMuScaled< DFDomainType,
//      LocalMuScaled< DecomposedLocalOperatorTuple, DFDomainType, DFRangeType > > InterfaceType;
  //! domain type
  typedef DFDomainType                                               DomainType;
  //! range type
  typedef DFRangeType                                                RangeType;
  //! number of components in the affine combination
  static const int numComps = RB::Tuple :: Size< DecomposedLocalOperatorTuple > :: value;
  //! helper type
  typedef LocalMuScaledDefaultCreator
            < numComps-1,
              DecomposedLocalOperatorTuple >                         CreatorType;
  //! the CombinedLocalMuScaledOperator that is wrapped.
  typedef typename CreatorType :: type                               WrapeeType;

public:
  //! constructor
  LocalMuScaled(DecomposedLocalOperatorTuple & tuple)
    : creator_(tuple), localOp_(creator_.getCombinedOp()) 
  { }

  //! prepareGlobal is called before the grid walktrough 
  void prepareGlobal(const DFDomainType &pa, DFRangeType &pb)
  {
    localOp_.prepareGlobal(pa,pb);   
  }
  
  //! prepare for grid walktrough 
  void prepareGlobal () 
  {
    localOp_.prepareGlobal();
  }
  
  //! finalize the walktrough 
  void finalizeGlobal() 
  {
    localOp_.finalizeGlobal(); 
  }
  
  //! called before applyLocal on every entity (one entity version)
  template<class EntityType>
  void prepareLocal (EntityType & en) 
  {
    localOp_.prepareLocal(en);
  } 
  
  //! called after applyLocal on everyEntity (one entity version)
  template<class EntityType>
  void finalizeLocal(EntityType & en) 
  {
    localOp_.finalizeLocal(en);
  } 

  //! called before applyLocal on every entity (two entity version)
  template<class EntityType>
  void prepareLocal (EntityType & en1, EntityType &en2)
  {
    localOp_.prepareLocal(en1,en2);
  } 
  
  //! called after applyLocal on every entity (two entity version)
  template<class EntityType>
  void finalizeLocal(EntityType & en1, EntityType &en2)
  {
    localOp_.finalizeLocal(en1,en2);
  } 
  
  //! things to do on one entity
  template<class EntityType>
  void applyLocal(EntityType & en)
  {
    localOp_.applyLocal(en); 
  } 

  //! things to do on two entities
  template<class EntityType>
  void applyLocal(EntityType & en1, EntityType &en2)
  {
    localOp_.applyLocal(en1,en2);
  }

  //! updates the scalar the local operator is multiplied with
  inline void scaleIt(ScalarType scalar)
  {
    localOp_.scaleIt(scalar);
  }

  inline void setTime(double time)
  {
    localOp_.setTime(time);
  }

private:
  CreatorType creator_;
  WrapeeType & localOp_;
};


}  // namespace Operator
}  // namespace Lib
} // end namespace RBFem
} // end namespace Dune


#endif  /*__LOCALMUSCALEDOPERATOR_HH__*/
/* vim: set et sw=2: */
