#ifndef  __DECOMPOSEDOPERATOR_HH__
#define  __DECOMPOSEDOPERATOR_HH__

#include  <dune/common/bartonnackmanifcheck.hh>
#include <vector>
#include <string>

#include <dune/fem/operator/discreteoperatorimp.hh>

#include "../linevol/spaceoperatorwrapper.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Operator
{


/**
 * @brief an abstract base class for the decomposed operators interface.
 *
 * @see DecomposedOperatorInterface for more details on what a %decomposed
 * operator is.
 *
 */
template< typename DFDomainType, typename DFRangeType >
class VirtualInterface
{
public:
  //! type of domain of operator
  typedef DFDomainType DomainType;
  typedef typename DomainType::DomainFieldType DomainFieldType;
  //! type of range of operator
  typedef DFRangeType RangeType;
  //! field type of range vector
  typedef typename RangeType::RangeFieldType RangeFieldType;
  //! scalar type
  typedef RangeFieldType ScalarType;
  //! coefficient list type
  typedef std::vector< ScalarType > CoefficientList;
  //! symbolic coefficient list type
  typedef std::vector< std::string > SymbolicCoefficientListType;
  //! type of an underlying space operator mapping
  typedef Mapping< DomainFieldType, RangeFieldType, DomainType, RangeType > MappingType;
  //! type of an underlying space operator mapping
  typedef Lib::Operator::TimeSpaceMapping< MappingType, DomainType > OperatorBaseType;
public:
  //! deconstructor
  virtual ~VirtualInterface()
  {
  }
  ;

  /** @brief returns the number of components  */
  virtual int numComponents() const = 0;

  /** @brief returns the full operator \f$\mathcal{L}\f$.
   *
   * @note This is not derived from SpaceOperatorInterface(). Use
   * completeWithTimeDependence() instead if you want to use the operator with
   * an ODE Solver.
   *
   * \return    full operator */
  virtual OperatorBaseType & complete() = 0;

#if 0
  /** @brief returns the full operator \f$\mathcal{L}\f$.
   *
   * @note This is not derived from Mapping(). Use complete() instead if you
   * want to use the operator with the GramianPipeline e.g.
   *
   * \return    full operator */
  virtual SpaceOperatorBaseType & completeWithTimeDependence() = 0;
#endif

  /** @brief returns the ith component @f$L^i@f$
   *
   *  \param[in] i      index of the component to be returned
   *  \return           operator component */
  virtual const MappingType & component(const int i) const = 0;

  /** @brief returns a vector of coefficents
   *  \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$.
   *
   *  \param[out] coeffs  a vector of scalar entries that should a priori be
   *  empty. */
  virtual void coefficients(CoefficientList& coeffs) const = 0;

  /** @brief a vector of all symoblic coefficient strings
   *
   * \param[out]  coeffs  a vector of symbolic coefficient strings that can be
   *                      evaluated by MATLAB&copy; */
  virtual void symbolicCoefficients(
      SymbolicCoefficientListType& coeffs) const = 0;

  /** @brief returns true if the \a i -th component is parameter dependent, i.e.
   * @f$\sigma^i \not \equiv \mbox{const} @f$
   *
   * \param[in] i index of component
   * \return    boolean for mu dependency of \a i -th component */
  virtual inline bool isMuDependent(const int i) const = 0;

  /** @brief returns true if the \a i -th component is time dependent
   *
   * \param[in] i index of component
   * \return    boolean for mu dependency of \a i -th component */
  virtual inline bool isTimeDependent(const int i) const = 0;
};

// forward declaration
template< class DFDomainType, class DFRangeType, class DecomposedOperatorImp >
class Interface;

/**
 * @brief Class for combination of two decomposed operators.
 *
 * Objects of this class are returned by the plus operator of the
 * DecomposedOperatorInterface.
 *
 */
template< class Aop, class Bop >
class Combined: public Interface<
    typename Aop::DomainType, typename Aop::RangeType,
    Combined< Aop, Bop > >
{
public:

  typedef typename Aop::DomainType DomainType;
  typedef typename DomainType::DomainFieldType DomainFieldType;
  typedef typename Aop::RangeType RangeType;
  typedef typename RangeType::RangeFieldType RangeFieldType;

private:
  /**
   * @brief contains the type of the complete operator after adding FullOp1 and
   * FullOp2
   *
   * @tparam FullOp1  first addend
   * @tparam FullOp2  second addend
   */
  template< class FullOp1, class FullOp2 >
  struct FullOperatorAddType
  {
    //! combined type
    typedef Mapping< DomainFieldType, RangeFieldType, DomainType, RangeType > MappingType;
    typedef Lib::Operator::CombinedTimeSpaceWrapper< FullOp1, FullOp2 > type;
//    typedef SpaceOperatorMapping< MappingType, DomainType >          type;
  };

  /**
   * @brief contains the type of the complete operator after adding FullOp1 and
   * FullOp2
   *
   * @todo I think, we do not need this any more, because CompleteType is
   * always a SpaceOperatorMapping
   *
   * @tparam LocalOp1  local operator of first addend
   * @tparam LocalOp2  local operator of second addend
   */
  template< class LocalOp1, class LocalOp2, class DFDomainType,
      class DFRangeType >
  struct FullOperatorAddType<
      DiscreteOperator< LocalOp1, DFDomainType, DFRangeType > ,
      DiscreteOperator< LocalOp2, DFDomainType, DFRangeType > >
  {
    //! combined type
    typedef DiscreteOperator< CombinedLocalOperator< LocalOp1, LocalOp2 > ,
    DFDomainType, DFRangeType > OperatorType;
    typedef Mapping< DomainFieldType, RangeFieldType, DomainType, RangeType > MappingType;
    typedef Lib::Operator::TimeSpaceWrapper< OperatorType, MappingType, DFDomainType > type;
  };

public:
  //! this type
  typedef CombinedLocalOperator< Aop, Bop > ThisType;
  //! type of a component
  typedef typename Aop::ComponentType ComponentType;
  //! base mapping type for a component
  typedef typename Aop::MappingType MappingType;
  //! type for the array of coefficients returned by the coefficients method
  typedef typename Aop::CoefficientList CoefficientList;
  //! type for the array of coefficients returned by the coefficients method
  typedef typename Aop::SymbolicCoefficientList SymbolicCoefficientList;
  //! type of the complete mu scaled operator
  typedef typename FullOperatorAddType< typename Aop::CompleteType,
      typename Bop::CompleteType >::type CompleteType;

  typedef typename Aop::DumbCompleteType DumbCompleteType;

  typedef typename Aop::SpaceMappingType SpaceMappingType;
public:

  //! constructor
  Combined(Aop & a, Bop & b) :
      a_(a), b_(b)
  {
  }

  //! copy constructor
  Combined(ThisType & copy) :
      a_(copy.a), b_(copy.b)
  {
  }

  virtual ~Combined()
  {
  }

  //! \copydoc DecomposedMapping :: numComponents()
  virtual int numComponents() const
  {
    return a_.numComponents() + b_.numComponents();
  }

  //! \copydoc DecomposedMapping :: complete()
  virtual CompleteType & complete()
  {
    return a_.complete() + b_.complete();
  }

  virtual DumbCompleteType & dumbComplete()
  {
    return a_.dumbComplete() + b_.dumbComplete();
  }

  //! \copydoc DecomposedMapping :: component()
  virtual const ComponentType & component(const int i) const
  {
    if (i < a_.numComponents())
    {
      return a_.component(i);
    }
    else
    {
      return b_.component(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedMapping :: coefficients()
  virtual void coefficients(CoefficientList& coeffs) const
  {
    a_.coefficients(coeffs);
    b_.coefficients(coeffs);
  }

  //! \copydoc DecomposedMapping :: symbolicCoefficients()
  virtual void symbolicCoefficients(SymbolicCoefficientList& coeffs) const
  {
    a_.symbolicCoefficients(coeffs);
    b_.symbolicCoefficients(coeffs);
  }

  //! \copydoc DecomposedMapping :: isMuDependent()
  virtual bool isMuDependent(const int i) const
  {
    if (i < a_.numComponents())
    {
      return a_.isMuDependent(i);
    }
    else
    {
      return b_.isMuDependent(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedMapping :: isTimeDependent()
  virtual bool isTimeDependent(const int i) const
  {
    if (i < a_.numComponents())
    {
      return a_.isTimeDependent(i);
    }
    else
    {
      return b_.isTimeDependent(i - a_.numComponents());
    }
  }

private:
  Aop & a_;
  Bop & b_;
};

/** @ingroup decomposed
 * @brief Interface class for decomposed operators.
 *
 * A decomposed operator implements an affine combination of factors of
 * parameter dependent coefficients and linear and parameter-independent
 * operators, i.e.
 *
 * @f[ L(\mu)[u] = \sum_{q=1}^{\mbox{numComps}} L^q[u] \cdot \sigma^q(\mu) @f]
 *
 * We use the following terminology:
 *  - @f$ L @f$ is the @em complete operator
 *  - @f$ L^q @f$ are the @em components of the decomposed operator
 *  - @f$ \sigma^q @f$ return the @em coefficients of the decomposed operator
 *
 * \note A decomposed operator implementation generally is *not* a real
 * Dune::Operator.
 *
 * This interface already adds the operator+ method to a decomposed operator.
 * The CombinedDecomposedOperator that we get after adding two decomposed
 * operators implements the new complete operator such that it can be applied
 * efficiently to a discrete function. Here, "efficiently", means that only one
 * grid iteration is needed in order to apply the operator, because the local
 * operators of the two addends are combined as well.
 *
 * @see CombinedDecomposedOperator, DecomposedMapping
 */
template< class DFDomainType, class DFRangeType, class DecomposedOperatorImp >
class Interface:
    public ObjPointerStorage, public VirtualInterface< DFDomainType,
        DFRangeType >
{
public:
  typedef VirtualInterface< DFDomainType, DFRangeType > DecomposedMappingType;
public:
  /**
   * @brief efficiently adds two decomposed operators.
   *
   * extends the list of operator components and coefficients and updates the
   * complete operator, such that it can still be evaluated efficiently. In
   * order to avoid using the new complex CombinedDecomposedOperator type, the
   * result is usually casted to a DecomposedMapping type in the end.
   *
   * @code
   * typedef typename DecomposedOperatorExample :: MappingType 
   *   DecomposedMappingType;
   * 
   * DecomposedMappingType sum = decOp1 + decOp2 + decOp3;
   * @endcode
   *
   * @tparam     DecomposedOperatorAddend  type of addend
   * @param[in]  addend                     the addend
   * @return sum of decomposed operators 
   **/
  template< class DecomposedOperatorAddend >
  Combined< DecomposedOperatorImp, DecomposedOperatorAddend > &
  operator +(DecomposedOperatorAddend & addend)
  {
    typedef Combined< DecomposedOperatorImp,
        DecomposedOperatorAddend > OpType;

    OpType * sum = new OpType(asImp(), addend);

    this->saveObjPointer(sum);

    return (*sum);
  }

private:
  inline const DecomposedOperatorImp& asImp() const
  {
    return static_cast< const DecomposedOperatorImp& >(*this);
  }

  inline DecomposedOperatorImp& asImp()
  {
    return static_cast< DecomposedOperatorImp& >(*this);
  }
};

} // end namespace Operator
} // end namespace Decomposed
} // end namespace Dune::RBFem
} // end namespace Dune

#endif  /*__DECOMPOSEDOPERATOR_HH__*/
/* vim: set et sw=2: */
