#ifndef  __EYE_HH__
#define  __EYE_HH__

#include "discretedecomposedoperator.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Operator
{

/** @brief creates a decomposed operator that implements an identity matrix
 * scaled with the template parameter @c scale.
 */
template<class DiscreteFunctionType, int scale=1>
class IdentityFactory
{

private:
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;

  template<class dummy, int localScale>
  class LocalEyeOperator
    : public Lib::Operator::ILocalParametrized<DiscreteFunctionType,
                                                LocalEyeOperator<dummy, localScale> >
  {
  public:
    typedef double                                                   ScalarType;

  public:
    LocalEyeOperator() { }

    void prepareGlobal (const DiscreteFunctionType & arg, DiscreteFunctionType & dest)
    {
      arg_ = &arg;
      dest_ = &dest;
    }

    template<class EntityType>
    void applyLocal(EntityType & en)
    { 
      typedef typename DiscreteFunctionType :: LocalFunctionType     LocalFunctionType;
      const LocalFunctionType argLf = (*arg_).localFunction(en);
      LocalFunctionType destLf      = (*dest_).localFunction(en);

      for( int i = 0 ; i < argLf.numDofs()  ; ++i ) {
        destLf[i] = localScale * argLf[i];
      }
    }

    //! one entity
    template<class EntityType>
    void prepareLocal (EntityType & en)
    { }

    template<class EntityType>
    void finalizeLocal (EntityType & en)
    { }

    void finalizeGlobal() { }

    inline ScalarType coefficient() const
    {
      return 1.0;
    }

    inline std::string symbolicCoefficient() const
    {
      return "1.0";
    }

    void setTime(double time)
    {
    };

    bool isMuDependent() const
    {
      return false;
    }

    bool isTimeDependent() const
    {
      return false;
    }

  private:
    const DiscreteFunctionType *arg_;
    DiscreteFunctionType       *dest_;
  };

  template<class dummy>
  class LocalEyeOperator<dummy, 0> 
    : public Lib::Operator::ILocalParametrized<DiscreteFunctionType,
                                                LocalEyeOperator<dummy, 0> >
  {
    public:
      typedef double                                                   ScalarType;
    public:
      LocalEyeOperator() : scalar_(1.0) {}

      void prepareGlobal (const DiscreteFunctionType & arg, DiscreteFunctionType & dest)
      {
        dest_ = &dest;
      }

      template<class EntityType>
      void applyLocal(EntityType & en)
      {
        typedef typename DiscreteFunctionType :: LocalFunctionType     LocalFunctionType;
        LocalFunctionType destLf = (*dest_).localFunction(en);

        for( int i = 0 ; i < destLf.numDofs()  ; ++i ) {
          destLf[i] = 0.0;
        }
      }

      //! one entity
      template<class EntityType>
      void prepareLocal (EntityType & en)
      { }

      template<class EntityType>
      void finalizeLocal (EntityType & en)
      { }

      void finalizeGlobal() { }

      inline ScalarType coefficient() const
      {
        return 0.0;
      }

      inline std::string symbolicCoefficient() const
      {
        return "0.0";
      }

      void scaleIt(ScalarType scalar)
      {
        scalar_ = scalar;
      }

      void setTime(double time)
      {
      };

      bool isMuDependent() const
      {
        return false;
      }

      bool isTimeDependent() const
      {
        return false;
      }

    protected:
      ScalarType scalar_;
    private:
      DiscreteFunctionType       *dest_;
  };


public:
  //! local operator type
  typedef LocalEyeOperator< int, scale >                             LocalEyeType;
  //! tuple of one local operator
  typedef RB::Tuple :: Tuple< LocalEyeType >                             LocalOperatorTuple;
  //! decomposed operator
  typedef Decomposed::Operator::Discrete< LocalOperatorTuple,
                                      DiscreteFunctionType,
                                      DiscreteFunctionType >         OperatorType;
public:
  //! constructor
  IdentityFactory(const DiscreteFunctionSpaceType & space) :
    lop_(),
    lopTuple_(lop_),
    eye_(space, lopTuple_)
  {};

  //! returns the decomposed identity operator
  OperatorType & getOperator() {
    return eye_;
  }

private:
  LocalEyeType       lop_;
  LocalOperatorTuple lopTuple_;
  OperatorType       eye_;
};

/* @brief this is an alias for a MakeEye operator scaled with zero.
 */
template<class DiscreteFunctionType>
class ZeroFactory 
  : public IdentityFactory<DiscreteFunctionType, 0>
{
private:
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef IdentityFactory< DiscreteFunctionType, 0 >                         BaseType;
public:
  ZeroFactory(const DiscreteFunctionSpaceType & space)
    : BaseType(space) {}
};

}  // namespace Operator
} // namespace Decomposed
} // end namespace Dune::RBFem
} // end namespace Dune

#endif  /*__EYE_HH__*/
