#define BOOST_TEST_MODULE LocalDecomposedOperatorTest
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include  <config.h>

#include "../discretedecomposedoperator.hh"
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/grid/sgrid.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

#include  <dune/fem/misc/mpimanager.hh>

#include <ctime>

#include "../interfaces.hh"
#include "dummylocaloperators.hh"
#include "../../../common/test/utility.hh"
#include  <iostream>

namespace std
{
inline std::ostream& operator<< (std::ostream &stream, const Dune::Exception &e)
{
  return stream << e.what();
}
}
namespace boost
{
inline std::ostream& operator<< (std::ostream &stream, const Dune::Exception &e)
{

  return stream << e.what();
}
namespace test_tools
{
inline std::ostream& operator<< (std::ostream &stream, const Dune::Exception &e)
{

  return stream << e.what();
}
}
}

using namespace Dune;
using namespace Dune::RB;

template< bool printMsg = false >
class DecomposedTest
{
public:
  static const int dim = GridSelector::GridType::dimension;

  typedef GridPtr< GridSelector::GridType > GridPtrType;
  typedef LeafGridPart< GridSelector::GridType > GridPartType;

  typedef FunctionSpace< double, double, dim, 1 > FunctionSpaceType;

  typedef FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0, CachingStorage > DiscreteFunctionSpaceType;

  typedef AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
  typedef DiscreteFunctionType DomainType;

public:
  typedef RBFem::Lib::Operator::Test::Local1< DiscreteFunctionType, printMsg > LopType1;
  typedef RBFem::Lib::Operator::Test::Local2< DiscreteFunctionType, printMsg > LopType2;
  // a small tuple of local operators
  typedef Dune::RB::Tuple::Tuple< LopType1, LopType2, LopType1 > LocalOperatorTuple;

  // a big tuple of local operators
  typedef Dune::RB::Tuple::Tuple< LopType1, LopType2, LopType1, LopType1,
      LopType2, LopType1, LopType1, LopType2, LopType1 > BigLocalOperatorTuple;

  // discrete decomposed operator of the small tuple
  typedef RBFem::Decomposed::Operator::Discrete< LocalOperatorTuple, DomainType,
      DomainType > DiscreteDecomposedOperatorType;

  // discrete decomposed operator of the big tuple
  typedef RBFem::Decomposed::Operator::Discrete< BigLocalOperatorTuple,
      DomainType, DomainType > BigDiscreteDecomposedOperatorType;

  // base type: DecomposedMapping
  typedef RBFem::Decomposed::Operator::VirtualInterface< DomainType, DomainType > DecomposedMappingType;

  // operator component type
  typedef typename DiscreteDecomposedOperatorType::ComponentType OperatorComponentImp;

  // list of coefficients
  typedef typename DiscreteDecomposedOperatorType::CoefficientList CoefficientList;

  // complete operator type of small operator
  typedef typename DiscreteDecomposedOperatorType::CompleteType FullOperatorType;
  // complete operator type of big operator
  typedef typename BigDiscreteDecomposedOperatorType::CompleteType BigFullOperatorType;
  // dumb complete operator type of big operator
  typedef typename DiscreteDecomposedOperatorType::DumbCompleteType DumbFullOperatorType;

  // mapping type of underlying operator for decomposedMapping
  typedef DecomposedMappingType::MappingType AddOperatorComponentImp;
  typedef DecomposedMappingType::CoefficientList AddCoefficientList;
  typedef DecomposedMappingType::OperatorBaseType AddFullOperatorType;
public:
  DecomposedTest ()
      : dummy_( InitHelper::one( "../../../common/test/test.params" ) ),
        gridptr_( "../../../common/test/unitcube.dgf" ), grid( *gridptr_ ),
        gridPart( grid ), discreteFunctionSpace( gridPart )
  {
  }

  void init ()
  {
    argument = new DiscreteFunctionType( "argument", discreteFunctionSpace );
    destination = new DiscreteFunctionType( "solution", discreteFunctionSpace );
    argument->clear();
    destination->clear();
  }

  void refineGrid (int refineSteps)
  {
    GlobalRefine::apply(
        grid,
        DGFGridInfo< GridSelector::GridType >::refineStepsForHalf()
          * refineSteps );
  }

private:
  int dummy_;
  GridPtrType gridptr_;
public:
  GridSelector::GridType &grid;
  GridPartType gridPart;
  DiscreteFunctionSpaceType discreteFunctionSpace;
  DiscreteFunctionType *argument;
  DiscreteFunctionType *destination;

  /*  LocalOperator1 lop1;
   *  LocalOperator2 lop2;
   *  LocalOperatorTuple lopTuple1;
   *  LocalOperatorTuple lopTuple2;
   *  LocalOperatorTuple lopTuple3;
   *  BigLocalOperatorTuple bigLopTuple;

   *  DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple);
   *  DiscreteDecomposedOperatorType ddo2(discreteFunctionSpace, lopTuple2);
   *  DiscreteDecomposedOperatorType ddo3(discreteFunctionSpace, lopTuple3);
   *  BigDiscreteDecomposedOperatorType bigDdo(discreteFunctionSpace, bigLopTuple);*/
};

template< class Output >
void vector_print (Output o)
{
  std::cout << o << " ";
}

/*int main(int argc, char *argv[])
 *{

 *  try {
 *  MPIManager :: initialize(argc, argv);
 *  Parameter :: append("../../../common/test/test.params");
 *  ostringstream oslop;
 *  LocalOperator1 lop1(oslop);
 *  LocalOperator2 lop2(oslop);
 *  LocalOperatorTuple lopTuple(lop1, lop2, lop1);
 *  LocalOperatorTuple lopTuple2(lop1, lop2, lop1);
 *  LocalOperatorTuple lopTuple3(lop1, lop2, lop1);

 *  BigLocalOperatorTuple bigLopTuple(lop1, lop2, lop1,
 *                                    lop1, lop2, lop1,
 *                                    lop1, lop2, lop1);

 *  GridPtrType gridptr("unitcube.dgf");
 *  GridType & grid = *gridptr;
 *  GridPartType gridPart( grid );

 *  DiscreteFunctionSpaceType discreteFunctionSpace( gridPart );
 *  DiscreteFunctionType argument( "argument", discreteFunctionSpace );
 *  DiscreteFunctionType destination( "solution", discreteFunctionSpace );
 *  argument.clear();
 *  destination.clear();

 *  typedef DiscreteDecomposedOperator< LocalOperatorTuple, DomainType,
 *                                      DomainType >                   DiscreteDecomposedOperatorType;
 *  typedef DiscreteDecomposedOperator< BigLocalOperatorTuple,
 *                                      DomainType, DomainType >       BigDiscreteDecomposedOperatorType;
 *  typedef DecomposedMapping< DomainType, DomainType >                MappingType;
 *  DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple);
 *  DiscreteDecomposedOperatorType ddo2(discreteFunctionSpace, lopTuple2);
 *  DiscreteDecomposedOperatorType ddo3(discreteFunctionSpace, lopTuple3);
 *  BigDiscreteDecomposedOperatorType bigDdo(discreteFunctionSpace, bigLopTuple);
 *  MappingType & addo = ddo + ddo2 + ddo3;

 *  typedef DiscreteDecomposedOperatorType :: ComponentType            OperatorComponentImp;
 *  typedef DiscreteDecomposedOperatorType :: CoefficientList          CoefficientList;
 *  typedef DiscreteDecomposedOperatorType :: CompleteType             FullOperatorType;
 *  typedef BigDiscreteDecomposedOperatorType :: CompleteType          BigFullOperatorType;
 *  typedef DiscreteDecomposedOperatorType :: DumbCompleteType         DumbFullOperatorType;
 *  typedef MappingType :: CoefficientList                             AddCoefficientList;
 *  typedef MappingType :: MappingType                                 AddFullOperatorType;

 *  const BigFullOperatorType & bigFullOp = bigDdo.complete();
 *  const FullOperatorType & fullOp = ddo.complete();
 *  const DumbFullOperatorType & dumbFullOp = ddo.dumbComplete();
 *  const AddFullOperatorType & fullAddOp = *(addo.complete());
 *|+  const DumbFullOperatorType & fullAddOp = ddo.dumbComplete() + ddo2.dumbComplete() + ddo3.dumbComplete();+|
 *  AddCoefficientList coeffsAll;

 *  std::cout << "\nevaluating complete operator..." << std::endl;

 *  clock_t start = clock();
 *  fullOp(argument, destination);
 *  destination.print(std::cout);
 *  clock_t elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;
 *|+  std::cout << oslop.str();+|
 *  oslop.str("");


 *  std::cout << "\nevaluating dumb complete operator..." << std::endl;

 *  start = clock();
 *  dumbFullOp(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;
 *|+  std::cout << oslop.str();+|
 *  oslop.str("");


 *  std::cout << "\nevaluating combination of complete operators..." << std::endl;

 *  start = clock();
 *  fullAddOp(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *|+  std::cout << oslop.str();+|
 *  oslop.str("");

 *  std::cout << "\nevaluating combination of complete operators...(alternative)" << std::endl;

 *  start = clock();
 *  bigFullOp(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *  oslop.str("");


 *  DiscreteOperator<LocalOperator1, DiscreteFunctionType, DiscreteFunctionType> dop1(lop1);
 *  DiscreteOperator<LocalOperator2, DiscreteFunctionType, DiscreteFunctionType> dop2(lop2);
 *  LocalOperatorFull lopFull(oslop);
 *  DiscreteOperator<LocalOperatorFull, DiscreteFunctionType, DiscreteFunctionType> dopF(lopFull);
 *  DiscreteOperator<CombinedLocalOperator< CombinedLocalOperator < LocalOperator1 , LocalOperator2>, LocalOperator1 >, DiscreteFunctionType, DiscreteFunctionType > dopFull(dop1 + dop2 + dop1);
 *|+  AddFullOperatorType dopFull = dop1 + dop2 + dop1;+|

 *  std::cout << "\nevaluating single operator 1..." << std::endl;
 *  start = clock();
 *  dop1(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *  std::cout << "\nevaluating single operator 2..." << std::endl;
 *  start = clock();
 *  dop2(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *  std::cout << "\nevaluating full mapping..." << std::endl;
 *  start = clock();
 *  dopFull(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *  std::cout << "\nevaluating discrete operator with full local operator..." << std::endl;
 *  start = clock();
 *  dopF(argument, destination);
 *  elapsed = clock() - start;
 *  std::cout << "elapsed time:" << elapsed/(CLOCKS_PER_SEC/1000) << "ms" << std::endl;

 *  CoefficientList coeffs;
 *  ddo.coefficients(coeffs);
 *  addo.coefficients(coeffsAll);

 *  typedef Dune :: RB :: Tuple :: Pair<int, Dune :: RB :: Tuple :: Nil> Pair0Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair0Type > Pair1Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair1Type > Pair2Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair2Type > Pair3Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair3Type > Pair4Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair4Type > Pair5Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair5Type > Pair6Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair6Type > Pair7Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair7Type > Pair8Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair8Type > Pair9Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair9Type > Pair10Type;
 *  typedef Dune :: RB :: Tuple :: Pair<int, Pair10Type > Pair11Type;
 *  Pair0Type p0(0,Dune::RB::Tuple::Nil() );
 *  Pair1Type p1(1,p0);

 *  std::cout << "tuple size: " << Dune::RB::Tuple :: Size<Pair11Type> :: value << std::endl;

 *  for_each(coeffs.begin(), coeffs.end(), vector_print<double>);
 *  std::cout << std::endl;
 *  for_each(coeffsAll.begin(), coeffsAll.end(), vector_print<double>);
 *  std::cout << std::endl;

 *  } catch(Dune::Exception e) {
 *    std::cerr << e << std::endl;
 *  } catch(...) {
 *    std::cerr << "unknown exception!" << std::endl;
 *  }
 *}*/

using boost::test_tools::output_test_stream;

BOOST_AUTO_TEST_SUITE( LocalDecomposedOperatorTest )

BOOST_FIXTURE_TEST_CASE( component_test, DecomposedTest<true> )
{
  refineGrid(1);
  init();

  LopType1 lop1;
  LopType2 lop2;

  {
    output_test_stream oslop("reference/component1", true);
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple1);

    const OperatorComponentImp& opComp1 = ddo.component(0);

    std::cout << "evaluating component 1..." << std::endl;

    opComp1(*argument,*destination);

    BOOST_CHECK(oslop.match_pattern());
    BOOST_CHECK_EQUAL(*(destination->dbegin()), 1.0);
  }

  {
    output_test_stream oslop("reference/component2", true);
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple1);

    const OperatorComponentImp& opComp2 = ddo.component(1);

    std::cout << "evaluating component 2..." << std::endl;

    opComp2(*argument,*destination);

    BOOST_CHECK(oslop.match_pattern());
    BOOST_CHECK_EQUAL(*(destination->dbegin()), 1.0);
  }

  {
    output_test_stream oslop("reference/component3", true);
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple1);

    const OperatorComponentImp& opComp3 = ddo.component(2);

    std::cout << "evaluating component 3..." << std::endl;

    opComp3(*argument,*destination);

    BOOST_CHECK(oslop.match_pattern());
    BOOST_CHECK_EQUAL(*(destination->dbegin()), 1.0);
  }
}

BOOST_FIXTURE_TEST_CASE( complete_test, DecomposedTest<true> )
{
  refineGrid(1);
  init();

  LopType1 lop1;
  LopType2 lop2;

  {
    output_test_stream oslop("reference/complete", true);
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple1);

    const FullOperatorType & fullOp = ddo.complete();

    std::cout << "evaluating complete operator..." << std::endl;

    fullOp(*argument,*destination);

    BOOST_CHECK(oslop.match_pattern());
    BOOST_CHECK_EQUAL(*(destination->dbegin()), 8.0);
  }

  {
    output_test_stream oslop("reference/dumbcomplete", true);
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo(discreteFunctionSpace, lopTuple1);

    const DumbFullOperatorType & fullOp = ddo.dumbComplete();

    std::cout << "evaluating dumb complete operator..." << std::endl;

    fullOp(*argument,*destination);

    BOOST_CHECK(oslop.match_pattern());
    BOOST_CHECK_EQUAL(*(destination->dbegin()), 8.0);
  }

  {
    std::stringstream oslop;
    lop1.setOstream(oslop);
    lop2.setOstream(oslop);

    LocalOperatorTuple lopTuple1(lop1, lop2, lop1);
    LocalOperatorTuple lopTuple2(lop1, lop2, lop1);
    LocalOperatorTuple lopTuple3(lop1, lop2, lop1);

    DiscreteDecomposedOperatorType ddo1(discreteFunctionSpace, lopTuple1);
    DiscreteDecomposedOperatorType ddo2(discreteFunctionSpace, lopTuple2);
    DiscreteDecomposedOperatorType ddo3(discreteFunctionSpace, lopTuple3);

    DecomposedMappingType & addo = ddo1 + ddo2 + ddo3;

    const AddFullOperatorType & fullOp = addo.complete();

    std::cout << "evaluating sum of complete operators..." << std::endl;

    fullOp(*argument,*destination);
    std::cout << oslop.str();

    BOOST_CHECK_EQUAL(*(destination->dbegin()), 24.0);
  }

}
BOOST_AUTO_TEST_SUITE_END( )

