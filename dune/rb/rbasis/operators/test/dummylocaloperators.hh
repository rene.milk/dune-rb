#ifndef DUMMYLOCALOPERATORS_PTRD73CH

#define DUMMYLOCALOPERATORS_PTRD73CH

#include <iostream>
#include <string>

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Operator
{
namespace Test
{

/** first dummy LocalOperator class
 */
template< class DiscreteFunctionType, bool printMsg = false >
class Local1: public ILocalParametrized< DiscreteFunctionType,
    Local1< DiscreteFunctionType, printMsg > >
{
public:
  typedef ILocalParametrized< DiscreteFunctionType,
      Local1< DiscreteFunctionType, printMsg > > BaseType;
  typedef typename BaseType::ScalarType ScalarType;
  typedef double RangeFieldType;
  typedef double DomainFieldType;
private:
  using BaseType::scalar_;

public:
  Local1 ()
      : os_( 0 ), arg_( 0 ), dest_( 0 )
  {
  }

  void setOstream (std::ostream & os)
  {
    os_ = &os;
  }

  void prepareGlobal (const DiscreteFunctionType & arg,
                      DiscreteFunctionType & dest)
  {
    arg_ = &arg;
    dest_ = &dest;
    if (printMsg) *os_ << "pG in Lop1" << std::endl;
  }

  //! prepare for grid walktrough
  void prepareGlobal ()
  {
  }

  //! finalize the walktrough
  void finalizeGlobal ()
  {
    if (printMsg) *os_ << "fG in Lop1" << std::endl;
  }

  //! one entity
  template< class EntityType >
  inline void prepareLocal (EntityType & en)
  {
    if (printMsg) *os_ << " pL in Lop1" << std::endl;
  }

  //! \todo Please doc me!
  template< class EntityType >
  inline void finalizeLocal (EntityType & en)
  {
    if (printMsg) *os_ << " fL in Lop1" << std::endl;
  }

  template< class EntityType >
  inline void prepareLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void finalizeLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void applyLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void applyLocal (EntityType & en)
  {
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
    LocalFunctionType alf = arg_->localFunction( en );
    LocalFunctionType dlf = dest_->localFunction( en );

    for (int i = 0; i < alf.numDofs(); i++)
    {
      dlf[i] += alf[i] + scalar_;
    }
    if (printMsg) *os_ << "  apply in Lop1" << std::endl;
  }

  ScalarType coefficient () const
  {
    return 3.0;
  }

  bool isTimeDependent () const
  {
    return false;
  }

  bool isMuDependent () const
  {
    return false;
  }

  std::string symbolicCoefficient () const
  {
    return "3.0";
  }

  void setTime (double time)
  {
  }

protected:
  /*  ScalarType scalar_;*/
  std::ostream * os_;

  const DiscreteFunctionType * arg_;
  DiscreteFunctionType * dest_;
};

/** second dummy local operator class
 */
template< class DiscreteFunctionType, bool printMsg = false >
class Local2: public ILocalParametrized< DiscreteFunctionType,
    Local2< DiscreteFunctionType, printMsg > >
{
public:
  typedef ILocalParametrized< DiscreteFunctionType,
      Local2< DiscreteFunctionType, printMsg > > BaseType;
  typedef typename BaseType::ScalarType ScalarType;

  using BaseType::scalar_;
public:
  Local2 ()
      : os_( 0 ), arg_( 0 ), dest_( 0 )
  {
  }

  void setOstream (std::ostream & os)
  {
    os_ = &os;
  }

  typedef double RangeFieldType;
  typedef double DomainFieldType;
  void prepareGlobal (const DiscreteFunctionType & arg,
                      DiscreteFunctionType & dest)
  {
    arg_ = &arg;
    dest_ = &dest;
    if (printMsg) *os_ << "pG in Lop2" << std::endl;
  }

  //! prepare for grid walktrough 
  void prepareGlobal ()
  {
  }

  //! finalize the walktrough 
  void finalizeGlobal ()
  {
    if (printMsg) *os_ << "fG in Lop2" << std::endl;
  }

  //! one entity
  template< class EntityType >
  inline void prepareLocal (EntityType & en)
  {
    if (printMsg) *os_ << " pL in Lop2" << std::endl;
  }

  //! \todo Please doc me!
  template< class EntityType >
  inline void finalizeLocal (EntityType & en)
  {
    if (printMsg) *os_ << " fL in Lop2" << std::endl;
  }

  template< class EntityType >
  inline void prepareLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void finalizeLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void applyLocal (EntityType & en1, EntityType & en2)
  {
  }

  template< class EntityType >
  inline void applyLocal (EntityType & en)
  {
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
    LocalFunctionType alf = arg_->localFunction( en );
    LocalFunctionType dlf = dest_->localFunction( en );

    for (int i = 0; i < alf.numDofs(); i++)
    {
      dlf[i] += alf[i] + scalar_;
    }
    if (printMsg) *os_ << "  apply in Lop2" << std::endl;
  }

  ScalarType coefficient () const
  {
    return 2.0;
  }

  std::string symbolicCoefficient () const
  {
    return "2.0";
  }

  bool isTimeDependent () const
  {
    return false;
  }

  bool isMuDependent () const
  {
    return false;
  }

  void setTime (double time)
  {
  }
protected:
  /*  ScalarType scalar_;*/
  std::ostream * os_;

  const DiscreteFunctionType * arg_;
  DiscreteFunctionType * dest_;
};

}  // namespace Test
}  // namespace Operator
}  // namespace Lib
} // end of namespace Dune::RB
} // end of namespace Dune

#endif /* end of include guard: DUMMYLOCALOPERATORS_PTRD73CH */
