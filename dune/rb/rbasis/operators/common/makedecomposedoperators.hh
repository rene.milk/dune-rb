#ifndef  __MAKEDECOMPOSEDOPERATORS_HH__
#define  __MAKEDECOMPOSEDOPERATORS_HH__

#include  "../../../misc/tuples.hh"
#include  "../decomposedoperator.hh"
#include  "../discretedecomposedoperator.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Operator
{

/** @addtogroup decomposed
 * @{ */

/** @brief Traits class for the MakeDecomposedOperator class for LocalOperator's
 * dependent on a Flux implementations with zero integer arguments.
 *
 *
 * @tparam FluxTypeImp    flux implementation taking ModelParams as
 *                        first and only template parameter.
 * @tparam SpaceOpImp     LocalOperator implementation taking FluxTypeImp and
 *                        DestinationImp as template parameters. @sa e.g.
 *                        FinVol
 * @tparam ModelImp       Model Parameter
 * @tparam DestinationImp discrete function type
 */
template< template< class > class FluxTypeImp,
    template< class, class > class SpaceOpImp,
    class ModelImp, class DestinationImp >
struct FromFluxFactoryParams0
{
  typedef ModelImp ModelType;
  template< int i >
  struct Chooser
  {
    typedef FluxTypeImp< ModelType > FluxType;
    typedef SpaceOpImp< FluxType, DestinationImp > SpaceOperatorType;
  };
  typedef DestinationImp DFDomainType;
  typedef DestinationImp DFRangeType;
};

/** @brief Traits class for the MakeDecomposedOperator class for LocalOperator's
 * dependent on a Flux implementations with one integer argument.
 *
 * @tparam FluxTypeImp    flux implementation taking one integer argument and
 *                        ModelParams as template parameters.
 * @tparam SpaceOpImp     LocalOperator implementation taking FluxTypeImp and
 *                        DestinationImp as template parameters. @sa e.g.
 *                        FinVol.
 * @tparam ModelImp       Model Parameter
 * @tparam DestinationImp discrete function type
 */
template< template< int, class > class FluxTypeImp,
template< class, class > class SpaceOpImp,
class ModelImp, class DestinationImp >
struct FromFluxFactoryParams1
{
  typedef ModelImp ModelType;
  template< int i >
  struct Chooser
  {
    typedef FluxTypeImp< i, ModelType > FluxType;
    typedef SpaceOpImp< FluxType, DestinationImp > SpaceOperatorType;
  };
  typedef DestinationImp DFDomainType;
  typedef DestinationImp DFRangeType;
};

template< int start, int end, typename ParamsImp >
class Factory
{
public:
  typedef typename ParamsImp:: template Chooser< start >::FluxType FluxType;
  typedef typename ParamsImp:: template Chooser< start >::SpaceOperatorType SpaceOperatorType;
  typedef typename ParamsImp::ModelType ModelType;

  typedef Factory< start + 1, end, ParamsImp > PrevType;
  typedef typename PrevType::TupleType PrevTupleType;

  typedef typename RB::Tuple::Concat< SpaceOperatorType&, PrevTupleType > ConcatType;
  typedef typename ConcatType::Type TupleType;
public:

  Factory (ModelType & model)
      : flux_( FluxType( model ) ), spaceOp_( (flux_) ), prev_( model ),
        prevTuple_( prev_.getTuple() ),
        tuple_( ConcatType::concat( spaceOp_, prevTuple_ ) )
  {
  }
  ;

  ~Factory ()
  {
  }

  TupleType & getTuple ()
  {
    return tuple_;
  }

private:
  FluxType flux_;
  SpaceOperatorType spaceOp_;
  PrevType prev_;
  PrevTupleType &prevTuple_;
  TupleType tuple_;
};

/** @brief makes a decomposed operator around a recursively constructed tuple
 * of @link LocalParametrizedOperatorInterface parametrized operators @endlink.
 *
 * This class defines
 *   -# a tuple of discrete operators from local operators with 0 to 1 integer
 *   template arguments by iterating over these template parameters. The local
 *   operators must first be wrapped into a parameter traits class like
 *   e.g. MakeDecomposedOperatorFromFluxParams1, and then
 *   -# wraps the tuple inside a DiscreteDecomposedOperator
 *
 * @tparam end                   length of tuple to be generated.
 * @tparam ParamsImp             traits class describing the local operators,
 *                               see e.g. MakeDecomposedOperatorFromFluxParams1
 */
template< int end, typename ParamsImp >
class Factory< 0, end, ParamsImp >
{
public:
  //! flux type
  typedef typename ParamsImp:: template Chooser< 0 >::FluxType FluxType;
  //! space operator type
  typedef typename ParamsImp:: template Chooser< 0 >::SpaceOperatorType SpaceOperatorType;
  //! model type
  typedef typename ParamsImp::ModelType ModelType;

  typedef Factory< 1, end, ParamsImp > PrevType;
  typedef typename PrevType::TupleType PrevTupleType;
  //! tuple type
  typedef typename RB::Tuple::Concat< SpaceOperatorType&, PrevTupleType > ConcatType;
  typedef typename ConcatType::Type TupleType;
  //! decomposed operator type
  typedef Decomposed::Operator::Discrete< TupleType,
      typename ParamsImp::DFDomainType, typename ParamsImp::DFRangeType > DecomposedOperatorType;
  typedef typename ParamsImp::DFDomainType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
public:
  //! constructor
  Factory(const DiscreteFunctionSpaceType & space, ModelType & model)
  : flux_(FluxType(model)),
  spaceOp_((flux_)),
  prev_(model),
  prevTuple_(prev_.getTuple()),
  tuple_(ConcatType :: concat(spaceOp_, prevTuple_) ),
  space_(space),
  decOp_(space_, tuple_)
  {};

  //! returns tuple of parametrized operators
  TupleType & getTuple ()
  {
    return tuple_;
  }

  //! destructor
  ~Factory ()
  {
  }

  //! returns the decomposed operator
  DecomposedOperatorType & getOperator ()
  {
    return decOp_;
  }

private:
  FluxType flux_;
  SpaceOperatorType spaceOp_;
  PrevType prev_;
  PrevTupleType &prevTuple_;
  TupleType tuple_;
  const DiscreteFunctionSpaceType &space_;
  DecomposedOperatorType decOp_;
};

template< int end, typename ParamsImp >
class Factory< end, end, ParamsImp >
{
public:
  typedef typename ParamsImp::ModelType ModelType;
  typedef RB::Tuple::Nil TupleType;
public:

  Factory(ModelType & model)
  {};

  TupleType & getTuple ()
  {
    return null_;
  }
private:
  TupleType null_;
};

/* @} */

}  // namespace Operator
}  // namespace Decomposed
} // end namespace Dune::RBFem
} // end namespace Dune

#endif  /*__MAKEDECOMPOSEDOPERATORS_HH__*/
/* vim: set et sw=2: */
