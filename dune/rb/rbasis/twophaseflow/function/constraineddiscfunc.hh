#ifndef __CONSTRAINEDDISCFUNC_HH__
#define __CONSTRAINEDDISCFUNC_HH__

#include <vector>
#include <string>
#include <assert.h>

namespace Dune {

  template<class DiscFuncImp, class MDGridPartImp>
  class ConstrainedDiscreteFunction 
  : public DiscFuncImp {

  public:    
    typedef DiscFuncImp                                       BaseType;
    typedef typename BaseType::GridType::LeafIndexSet         IndexSetType;
    //! type of the macro elements, this should be set to int for the moment
    typedef int                                               MacroElement;
    //! type of the grid partition, here we use a Dune::multidomaingrid
    typedef MDGridPartImp                                     MDGridPart;
    //! this type, is named DiscreteFunctionType to be compatible to Dune::FEM::DiscreteFunction
    typedef ConstrainedDiscreteFunction<BaseType, MDGridPart> DiscreteFunctionType;
    //! type of the underlying discrete function space 
    typedef typename BaseType::DiscreteFunctionSpaceType      DiscreteFunctionSpaceType;
    //! type of local functions to this discrete function 
    typedef typename  BaseType::LocalFunctionType             LocalFunctionType;

  /**@brief Constructor
   *
   * @param[in] base The discrete function that is to be restricted
   * @param[in] gridPartition A partition of the grid indicating the coarse cells
   * @param[in] support The index of the coarse cell that this function should be restricted to
   *
   *@remark Choose support=-1 to get a normal discrete function that has support on the whole domain.
   */
  ConstrainedDiscreteFunction(BaseType &base, 
                              const MDGridPart& mdGridPart,
                              const MacroElement support)
    :BaseType(base),
     mdGridPart_(&mdGridPart),
     support_(support),
     zeroFunction_("zero", BaseType::space())
  {}

    /** @brief Constructor
     *
     * @param[in] name the name for the function
     * @param[in] discFuncSpace The discrete function space
     * @param[in] gridPartition The multi domain grid
     * @param[in] support Integer indicating the coarse element 
     *                    where this function should have its support
     */
    ConstrainedDiscreteFunction(const std::string &name,
                                const DiscreteFunctionSpaceType& discFuncSpace,
                                const MDGridPart& mdGridPart,
                                const MacroElement& support)
      : BaseType(name, discFuncSpace),
        mdGridPart_(&mdGridPart),
        support_(support),
        zeroFunction_("zero", discFuncSpace)
    {}
                                
  /** @brief Constructor
   *
   *  This is a constructor, that ensures compatibility to usual
   *  discrete functions, here the support is the whole domain.
   *
   * @param[in] name The desired name for the discrete function
   * @param[in] discFuncSpace The discrete function space 
   */ 
    ConstrainedDiscreteFunction(const std::string &name, 
                                 const DiscreteFunctionSpaceType& discFuncSpace)
    :BaseType(name, discFuncSpace),
     mdGridPart_(&discFuncSpace.gridPart()),
     support_(-1),
     zeroFunction_("zero", BaseType::space())
  {}

  /** @brief Constructor
   *
   *  This is a constructor, that ensures compatibility to usual
   *  discrete functions, here the support is the whole domain.
   *
   * @param[in] name The desired name for the discrete function
   * @param[in] discFuncSpace The discrete function space 
   */ 
  ConstrainedDiscreteFunction(const std::string &name, 
                              const DiscreteFunctionSpaceType& discFuncSpace,
                              const double* &ptr)
    :BaseType(name, discFuncSpace, ptr),
     mdGridPart_(NULL),
     support_(-1),
     zeroFunction_("zero", BaseType::space())
  {}

  /** @brief Constructor
   *
   *  This is a constructor, that ensures compatibility to usual
   *  discrete functions, here the support is the whole domain.
   *
   * @param[in] name The desired name for the discrete function
   * @param[in] discFuncSpace The discrete function space 
   */ 
  ConstrainedDiscreteFunction(const std::string &name, 
                              const DiscreteFunctionSpaceType& discFuncSpace,
                              double* &ptr)
    :BaseType(name, discFuncSpace, ptr),
     mdGridPart_(NULL),
     support_(-1),
     zeroFunction_("zero", BaseType::space())
  {}

  template< class EntityType >
  const LocalFunctionType localFunction(const EntityType &entity) const
  {
    assert(mdGridPart_!=NULL);
    // if the function has support on the given entity
    const bool onSupport = mdGridPart_->subDomainContains(support_, entity);
    if (support_==-1 || onSupport ) {
      return BaseType::localFunction(entity);
    }
    // else return a zero local function
    LocalFunctionType zero = zeroFunction_.localFunction(entity);
    // clear local dofs
    const unsigned int numLocalDofs = zero.numDofs();
    for (unsigned int i=0; i!=numLocalDofs; ++i)
      zero[i]=0.0;
    return zero;
  }
     
  template< class EntityType >
  LocalFunctionType localFunction(const EntityType &entity)
  {
    assert(mdGridPart_!=NULL);
    // if the function has support on the given entity
    const bool onSupport = mdGridPart_->subDomainContains(support_, entity);

    if ( support_==-1 || onSupport) {
      return BaseType::localFunction(entity);
    }
    // else return a zero local function
    LocalFunctionType zero = zeroFunction_.localFunction(entity);
    // clear local dofs
    const unsigned int numLocalDofs = zero.numDofs();
    for (unsigned int i=0; i!=numLocalDofs; ++i)
      zero[i]=0.0;
    return zero;
  }

  const MacroElement getSupport() const {
    return support_;
  }

  void setSupport(const MacroElement &supp) {
    support_ = supp;
    return;
  }

  void setGridPartition(const MDGridPart& gP) {
    mdGridPart_ = &gP;
    return;
  }

  const MDGridPart& gridPartition() const {
    assert(mdGridPart_!=NULL);
    return *mdGridPart_;
  }

private:
  const MDGridPart* mdGridPart_;
  MacroElement      support_;
  mutable BaseType  zeroFunction_;
};

}

#endif /* __CONSTRAINEDDISCFUNC_HH__ */
