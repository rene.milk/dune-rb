#include <config.h>

#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/fem/space/lagrangespace.hh>
#include  <dune/fem/space/fvspace.hh>


#include <dune/fem/function/adaptivefunction/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include L2-Projection
#include <dune/fem/operator/projection/l2projection.hh>

#include <dune/rb/rbasis/gramian/gramianpipeline.hh>
#include <dune/rb/misc/discfunclist/constraineddiscfunclist.hh>
#include <dune/rb/misc/discfunclist/discfunclist_xdr.hh>

// include matrix wrapper
#include <dune/rb/misc/cwrapper/cmatrixwrapper.hh>


#include "../constraineddiscfunc.hh"

using namespace Dune;
using namespace RB;

struct SineFunction{

  typedef double DomainFieldType;
  typedef double RangeFieldType;

  SineFunction(const unsigned int dim, const unsigned int &k) : dim_(dim), k_(k) {};

  template< typename DomainType, typename RangeType >
  void evaluate( const DomainType &x, RangeType &ret  ) const {
    ret[0] = 1.0;
    for (unsigned int i=0; i!=dim_; ++i)
      ret[0] *= sin(2*M_PI*k_*x[i]);
    return;
  }

private:
  const unsigned int dim_;
  const unsigned int k_;
};

int main(int argc, char *argv[])
{
  try{
  MPIManager::initialize(argc,argv);
  Parameter::append("cf.params");
  typedef Dune::FunctionSpace< double, double, 2, 1 > FunctionSpaceType;
  typedef AdaptiveLeafGridPart<GridType> GridPartType;

  const int polynomialOrder = 0;
  //Parameter::getValue<int>("constrainedfunc.test.polorder");

  typedef Dune::FiniteVolumeSpace
    < FunctionSpaceType, GridPartType, polynomialOrder>
    DiscreteSpaceType;
  // typedef Dune::LagrangeDiscreteFunctionSpace
  //   < FunctionSpaceType, GridPartType, polynomialOrder, Dune::CachingStorage >
  //   DiscreteSpaceType;
  typedef Dune::AdaptiveDiscreteFunction<DiscreteSpaceType>              DiscreteFunctionType;
  typedef ConstrainedDiscreteFunction<DiscreteFunctionType, int>         ConstDiscFunc;
  typedef DiscreteSpaceType::EntityType                                  EntityType;
  typedef DiscreteSpaceType::IteratorType                                IteratorType;
  typedef DiscreteFunctionType::LocalFunctionType                        LocalFunctionType;
  typedef LocalFunctionType::RangeType                                   RangeType;
  typedef GridType::LeafIndexSet                                         IndexSetType;

  typedef DiscreteSpaceType::DomainFieldType                                                DomainFieldType;
  typedef DiscreteSpaceType::RangeFieldType                                                 RangeFieldType;
  typedef L2Projection<DomainFieldType, RangeFieldType, SineFunction, DiscreteFunctionType> L2Projection;
  typedef Dune::Tuple<DiscreteFunctionType*>                                                IOTupleType;
  typedef DataWriter< GridType, IOTupleType > DataWriterType;
  typedef ConstrainedDiscreteFunctionList<ConstDiscFunc,DiscreteFunctionList_xdr>   DiscFuncList;
  typedef CMatrixWrapper                                                                MatrixType;

  GridPtr<GridType> gridPtr("unitcube2.dgf");
  GridType &grid = *gridPtr;
  Dune::gridinfo(grid);
  GridPartType gridPart(grid);
  DiscreteSpaceType discFuncSpace(gridPart);
  DiscreteFunctionType discFunc("discFunc", discFuncSpace);
  // fill the discrete function with data
  SineFunction sine(2, 1);
  L2Projection l2pro;
  l2pro(sine, discFunc);


  const int numElements = grid.size(0);
  assert(numElements!=0);
  const int numCoarseCells = numElements/4;
  std::vector<int> array;
  array.resize(numElements);

  // Grid run, mark coarse cells
  IteratorType end = gridPart.end<0>();
  int j=0;
  for (IteratorType it = gridPart.begin<0>(); it!=end; ++it, ++j) {
    array[j]=j/numCoarseCells;
  }

  int support1 = Parameter::getValue<int>("constrainedfunc.test.support1");
  int support2 = Parameter::getValue<int>("constrainedfunc.test.support2");

  ConstDiscFunc constrDiscFunc1(discFunc, array, support1);
  ConstDiscFunc constrDiscFunc2(discFunc, array, support2);

  // get a gramian pipeline
  DiscFuncList list(discFuncSpace, "all funcs");
  list.push_back(constrDiscFunc1);
  list.push_back(constrDiscFunc2);
  // now, get a "matrix" for the mass matrix and stiffness matrix
  const unsigned int listSize = list.size();
  unsigned int matrixSize = listSize * listSize;
  double * massMatrix = new double[matrixSize];
  MatrixType massMatrixWrapper(&massMatrix[0], listSize, listSize);

  typedef GramianPipeline<DiscFuncList, MatrixType> GramianPipelineType;
  typedef GramianPipelineType::OpHandle OpHandle;

  GramianPipelineType pipeline(list);

  // add the identity operator
  OpHandle hIdOp = pipeline.getIdentityHandle();
  pipeline.addGramMatrixComputation(hIdOp, hIdOp, massMatrixWrapper);
  // and run the pipeline
  pipeline.run();

  // print the result
  printf("The resulting mass matrix is:\n");
  for (unsigned int i=0; i<massMatrixWrapper.rows(); ++i) {
    for (unsigned int j=0; j<massMatrixWrapper.cols(); ++j) {
      double val = 0.0;
      massMatrixWrapper.get(i,j, val);
      std::cout.width(12);
      std::cout << val << " ";
    }
    std::cout << std::endl;
  }


  //=============== Visualization ===========================//
  IOTupleType uDataTup ( &discFunc );
  IOTupleType constrDataTup1 ( &constrDiscFunc1 );
  IOTupleType constrDataTup2 ( &constrDiscFunc2 );
  DataWriterType uWriter(grid, uDataTup);
  DataWriterType constrWriter1(grid, constrDataTup1);
  DataWriterType constrWriter2(grid, constrDataTup2);

  uWriter.write();
  constrWriter1.write();
  constrWriter2.write();
  //==========================================//



  } catch (Dune::Exception& e) {
    std::cout << e.what() << std::endl;
  }
  return 0;
}
