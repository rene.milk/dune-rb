#ifndef __MULTIDOMAINGRIDPART_HH__
#define __MULTIDOMAINGRIDPART_HH__

// stl stuff
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <assert.h>

// common
#include <dune/common/exceptions.hh>


namespace Dune {
  namespace RB {

template<class GridPartImp>
class MultiDomainGridPart : public GridPartImp
{

  typedef GridPartImp                                                 BaseType;
  typedef MultiDomainGridPart<BaseType>                               ThisType;
  typedef std::map<int, int>::iterator                                MapIterator;
  typedef std::map<int, int>::const_iterator                          ConstMapIterator;

public:
  typedef typename BaseType::GridType                                 GridType;
  typedef typename GridType::Traits::template Codim<0>::Entity        EntityType;
  typedef typename GridType::Traits::template Codim<0>::EntityPointer EntityPointer;
  typedef typename BaseType::IntersectionIteratorType                 IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection             IntersectionType;

public:
  MultiDomainGridPart(GridType& grid)
    : BaseType(grid),
      grid_(grid),
      subDomains_()
  {};


  const unsigned int numSubDomains() const {
    return subDomains_.size();
  }

  /** @remark Numbering starts at zero!*/  
  void addToSubDomain(const int subDomain, const EntityType& entity) {
      subDomains_.insert(subDomain);
      int index = BaseType::indexSet().index(entity);
      entityToSubDomainMap_[index] = subDomain;
      subdomainSizes_[subDomain]++;
  }

    const int enclosedFineCells(const int subdomain) const {
        ConstMapIterator it = subdomainSizes_.find(subdomain);
        assert(it!=subdomainSizes_.end());
        return it->second;
    }
    
  const bool subDomainContains(const int subDomain, 
                               const EntityType& entity) const { 
    int index = BaseType::indexSet().index(entity);
    ConstMapIterator it = entityToSubDomainMap_.find(index);
    assert(it!=entityToSubDomainMap_.end());
    //      DUNE_THROW(InvalidStateException, "The given entity was not marked to belong to any subdomain!");
    const int enSubDomain = it->second;
    bool contains = (enSubDomain==subDomain);
    return contains;
  }

  int getSubDomain(const EntityType& entity) const {
    int index = BaseType::indexSet().index(entity);
    ConstMapIterator it = entityToSubDomainMap_.find(index);
    assert(it!=entityToSubDomainMap_.end());
    //      DUNE_THROW(InvalidStateException, "The given entity was not marked to belong to any subdomain!");
    return it->second;
  }

  bool onCoarseCellIntersection(const IntersectionType& intersection) const {
    if (intersection.boundary())
      return false;
    EntityPointer insidePtr = intersection.inside();
    const EntityType &insideEn = *insidePtr;
    EntityPointer outsidePtr = intersection.outside();
    const EntityType &outsideEn = *outsidePtr;
    return (getSubDomain(insideEn)!=getSubDomain(outsideEn));
  }

private:
    const GridType&    grid_;
    std::map<int, int> entityToSubDomainMap_; 
    std::map<int, int> subdomainSizes_;
    std::set<int>      subDomains_;
  //  IntersectionVector coarseCellIntersections_;
};



  } // namespace RB
} // namespace Dune

#endif /* __MULTIDOMAINGRIDPART_HH__ */


