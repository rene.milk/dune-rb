#ifndef _DEVIDEGRID_H_
#define _DEVIDEGRID_H_

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/geometry/genericgeometry/referenceelements.hh>

namespace Dune {
  namespace  RB {
    class Subdomain
    {
    public:
      typedef FieldVector<double, GRIDDIM> DomainType;

    public:
      Subdomain(const DomainType& begin, const DomainType& end, const unsigned int index)
        : begin_(begin),
          end_(end),
          index_(index)
      {}

      bool contains(const DomainType& coord) const {
        DomainType leftDiff = coord-begin_;
        DomainType rightDiff = end_-coord;
        bool allGreaterZero=true;
        for (int i=0; (i!=GRIDDIM && allGreaterZero); ++i) {
          allGreaterZero = (leftDiff[i]>=0 && rightDiff[i]>0);
        }
        return allGreaterZero;
      }

      unsigned int getIndex() { return index_; }

    private:
      DomainType      begin_;
      DomainType      end_;
      unsigned int    index_;
    };

    class SubdomainMarker
    {
      typedef FieldVector<double, GRIDDIM> DomainType;
      typedef Subdomain SubdomainType;
      typedef GridSelector::GridType GridType; 

    public:
      SubdomainMarker(const std::string& subdomainPartitionFile
                      = Dune::Parameter::getValue<std::string>("grid.subdomainPartitionFile").c_str())
      : subdomainPartitionFile_(subdomainPartitionFile)
      {}
      
      template<class MDGridPartType>
      SubdomainMarker(MDGridPartType& mdGridPart, const std::string& subdomainPartitionFile
                      = Dune::Parameter::getValue<std::string>("grid.subdomainPartitionFile").c_str())
      : subdomainPartitionFile_(subdomainPartitionFile)
      {
        readSubdomains();
        markSubdomains(mdGridPart);
      }


//      template<class MDGridPartType>
//      void visualizeSubdomains(MDGridPartType& mdGridPart) {
//        typedef typename MDGridPartType::GridType		GridType;
//        typedef FunctionSpace<double, double, GridType::dimension, 1>	FunctionSpaceType;
//        typedef FiniteVolumeSpace<FunctionSpaceType, MDGridPartType, 0>	DiscFuncSpaceType;
//        typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>		DiscreteFunctionType;
//        typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType> ConstrainedDiscreteFunctionType;
//
//        // I know this is crap. If you have a better idea, tell me!
//        DiscFuncSpaceType discFuncSpace(mdGridPart);
//        // create a function that is 1 everywhere
//        DiscreteFunctionType one("Visualization of Subdomains", discFuncSpace);
//        double* dof = one.leakPointer();
//        int numDofs = one.size();
//        for (int i=0; i!=numDofs; ++i) {
//          dof[i]=1.0;
//        }
//        ConstrainedDiscreteFunctionType oneConstrained(one, mdGridPart, -1);
//        const int numSubDomains = mdGridPart.numSubDomains();
//        // restrict one to each subdomain in turns and plot
//        for (int sd=0; sd!=numSubDomains; ++sd) {
//          std::stringstream name;
//          name << "subdomain_" << sd;
//          ConstrainedDiscreteFunctionType vis(name.str().c_str(), discFuncSpace, mdGridPart, sd);
//          vis.clear();
//          oneConstrained.setSupport(sd);
//          restrictFunction(oneConstrained, vis);
//          plotFunction(vis);
//        }
//      }

      void readSubdomains() {
        //open inward file stream to read subdomain definitions from file
        std::ifstream in(subdomainPartitionFile_.c_str());
        if (!in) {
          //...and throw an exeption if the file doesn't exist
          DUNE_THROW(IOError, "Could not open subdomain definition file for reading!");
        }
        std::string row;
        // read first line to check wether subdomains are given in intervall form
        getline(in, row);
        if (row=="INTERVAL") {
          std::vector<std::vector<float> > data;
          while (getline(in, row)) {
            // ignore blank lines
            if (strlen(row.c_str())==0)
              continue;
            // ignore comments
            if (row[0] == '#')
              continue;
            // read coordinates
            std::vector<float> vec( GridType::dimension, 0.0);
#if GRIDDIM==2
            int items = sscanf(row.c_str(), "%f %f", &vec[0], &vec[1]);
#elif GRIDDIM==3
            int items = sscanf(row.c_str(), "%f %f %f", &vec[0], &vec[1], &vec[2]);
#else 
            DUNE_THROW(NotImplemented, "Subdomain partinioning is only implemented for dimension 2 and 3!");
#endif
            if(items != GridType::dimension)
              DUNE_THROW(IOError, "Subdomain definition file doesn't use correct syntax!");
            data.push_back(vec);
          }
          assert(data.size()==3);
          DomainType stepsizes;
          for (int i=0; i!=GRIDDIM; ++i)
            stepsizes[i]=(data[1][i]-data[0][i])/data[2][i];
#if GRIDDIM==3
          int index=0;
          for (double xPos=data[0][0]; xPos<(data[1][0]-0.5*stepsizes[0]); xPos+=stepsizes[0]) {
            for (double yPos=data[0][1]; yPos<(data[1][1]-0.5*stepsizes[1]); yPos+=stepsizes[1]) {
              for (double zPos=data[0][2]; zPos<(data[1][2]-0.5*stepsizes[2]); zPos+=stepsizes[2]) {
                DomainType begin(0.0), end(0.0);
                begin[0]=xPos; begin[1]=yPos; begin[2]=zPos;
                end[0]=xPos+stepsizes[0]; end[1]=yPos+stepsizes[1]; end[2]=zPos+stepsizes[2];
                for (int i=0; i!=GRIDDIM; ++i)
                  if (end[i]==data[1][i])
                    end[i]+=1e-10;
                subdomains_.push_back(SubdomainType(begin, end, index));
                ++index;
              }
            }
          }
#elif GRIDDIM==2
          int index=0;
          for (double xPos=data[0][0]; xPos<(data[1][0]-0.5*stepsizes[0]); xPos+=stepsizes[0]) {
            for (double yPos=data[0][1]; yPos<(data[1][1]-0.5*stepsizes[1]); yPos+=stepsizes[1]) {
              DomainType begin(0.0), end(0.0);
              begin[0]=xPos; begin[1]=yPos;
              end[0]=xPos+stepsizes[0]; end[1]=yPos+stepsizes[1];
              for (int i=0; i!=GRIDDIM; ++i)
                if (end[i]==data[1][i])
                  end[i]+=1e-10;
              subdomains_.push_back(SubdomainType(begin, end, index));
              ++index;
            }
          }
#endif
        } else {
          //read given file
          do {
            // ignore blank lines
            if (strlen(row.c_str())==0)
              continue;
            // ignore comments
            if (row[0] == '#')
              continue;
            // read coordinates
            std::vector<float> begin( GridType::dimension, 0.0), end( GridType::dimension, 0.0);
            unsigned int index;
#if GRIDDIM==2
            int items = sscanf(row.c_str(), "%u %f %f %f %f", &index, &begin[0], &begin[1], &end[0], &end[1]);
#elif GRIDDIM==3
            int items = sscanf(row.c_str(), "%u %f %f %f %f %f %f", &index, &begin[0], &begin[1], &begin[2], &end[0], &end[1], &end[2]);
#else 
            DUNE_THROW(NotImplemented, "Subdomain partinioning is only implemented for dimension 2 and 3!");
#endif
            if(items != ( GridType::dimension)*2+1)
              DUNE_THROW(IOError, "Subdomain definition file doesn't use correct syntax!");

            DomainType beginCopy, endCopy;      
            for (int i=0; i!= GridType::dimension; ++i) {
              beginCopy[i]=begin[i];
              endCopy[i]=end[i];
            }
            subdomains_.push_back(SubdomainType(beginCopy, endCopy, index));
          } while (getline(in, row));
        }
      }

      template<class MDGridPartType>
      void markSubdomains(MDGridPartType& mdGridPart) {
        typedef typename MDGridPartType::GridType		GridType;
        typedef typename MDGridPartType::template Codim<0>::IteratorType	IteratorType;
        typedef typename IteratorType::Entity		EntityType;

        for (IteratorType it = mdGridPart.template begin<0>(); it != mdGridPart.template end<0>(); ++it) {
          const EntityType& e = *it;
          // get the barycenter
          DomainType local = GenericReferenceElements<typename  GridType::ctype,
                                                             GridType::dimension>::general(e.type()).position(0,0);
          DomainType global = e.geometry().global(local);
          typedef typename std::vector<SubdomainType>::iterator MyIteratorType;
          int entityIndex = mdGridPart.indexSet().index(e);
          MyIteratorType sdVecEnd = subdomains_.end();
          for (MyIteratorType sdVecIt=subdomains_.begin(); sdVecIt!=sdVecEnd; ++sdVecIt) {
            if (sdVecIt->contains(global)) {
              mdGridPart.addToSubdomain(sdVecIt->getIndex(), e);
              break;
            }
          }
        }
//          std::cout << "Seed vector size: " << sizeof(mdGridPart.entitySeeds_) << "\n";
  //        std::cout << "Index vector size: " << sizeof(mdGridPart.entityToSubdomainMap_) << "\n";
      };

      const SubdomainType& getSubdomain(const int n) const {
        return subdomains_[n];
      }

      int numSubdomains() const {
        return subdomains_.size();
      }

    private:
      std::vector<SubdomainType> subdomains_;
      const std::string subdomainPartitionFile_;
    };

  } // namespace RB
} // namespace Dune


#endif /* _DEVIDEGRID_H_ */

