#ifndef __PLOTFUNCTION_HH__
#define __PLOTFUNCTION_HH__

#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/solver/timeprovider.hh>

#include <Eigen/Core>

namespace Dune {

  /** Helper struct to set the data path correctly for the function plotter*/
  struct MyParameters : public DataWriterParameters {

    MyParameters(const std::string& path) : path_(path)
    {};

    std::string prefix () const {
      return path_;
    }
      private:
      std::string path_;
  };

  namespace RB {

    /**@brief plot an arbitrary function using Fem::DataWriter.
     *
     * @param[in] function The function to plot
     * @tparam DiscreteFunctionType The type of the function.
     */
    template<class DiscreteFunctionImp>
    class FunctionPlotter {

      typedef  DiscreteFunctionImp                                     DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::GridPartType         GridPartType;
      typedef typename GridPartType::GridType                          GridType;
      typedef tuple<DiscreteFunctionType*>                             IOTupleType;
      typedef DataWriter<GridType, IOTupleType>                        DataWriterType;
      typedef TimeProvider<>                                           TimeProviderType;
      typedef MyParameters                                             WriterParameter;

    public:
      FunctionPlotter(DiscreteFunctionType& function, const std::string& name, const double& startTime=0.0)
        :function_(function),
         writerParams_(name),
         grid_(function.space().gridPart().grid()),
         dataTup_(&function_),
         writer_(grid_, dataTup_, writerParams_),
         tp_(startTime)
      {
          tp_.provideTimeStepEstimate(1.0);
          tp_.provideTimeStepUpperBound(1.0);
          tp_.init();
      }      

      void plot() const {
        // visualize the function
        writer_.write(function_.name());
        return;
      }
      
      void plotTimed() const {
        writer_.write(tp_, function_.name());
        tp_.next(1.0);
      }

    private:
      DiscreteFunctionType&    function_;
      MyParameters             writerParams_;
      const GridType&          grid_;
      IOTupleType              dataTup_;
      DataWriterType           writer_;
      mutable TimeProviderType tp_;

    };
      
      template<class DiscreteFunctionType>
      void plotFunction(DiscreteFunctionType& func) {
          FunctionPlotter<DiscreteFunctionType> plotter(func, func.name());
          plotter.plot();
          return;
      }

    template< class DiscreteFunctionImp >
    class MyVTKFunction : public VTKFunction< typename DiscreteFunctionImp::GridType > {
      typedef DiscreteFunctionImp DiscreteFunctionType;
      typedef typename DiscreteFunctionType::GridType::LeafGridView GridViewType;
      typedef typename GridViewType::ctype ctype;
      enum { dim = GridViewType::dimension };
      typedef typename GridViewType::template Codim< 0 >::Entity Entity;
    public:
      MyVTKFunction(const DiscreteFunctionType& func)
      : func_(func)
      {};

      int ncomps() const {
        return DiscreteFunctionType::RangeType::size;
      }

      double evaluate(int comp, const Entity &e, const Dune::FieldVector< ctype, dim > &xi) const {
        typename DiscreteFunctionType::LocalFunctionType localFunction = func_.localFunction(e);
        typename DiscreteFunctionType::RangeType res(0.0);
        localFunction.evaluate(xi, res);
        return res[comp];
      }

      std::string name () const {
        return func_.name();
      }

    private:
      const DiscreteFunctionType& func_;


    };

      template<class DiscreteFunctionType>
      void plotVectorFunction(DiscreteFunctionType& func) {
        MyVTKFunction<DiscreteFunctionType> vectorFunction(func);
        VTKWriter<typename DiscreteFunctionType::GridType::LeafGridView> vtkWriter(func.space().grid().leafView());
        shared_ptr<MyVTKFunction<DiscreteFunctionType> > vectorFunctionPtr(&vectorFunction);
        vtkWriter.addCellData(vectorFunctionPtr);
        vtkWriter.write(func.name());
        return;
      }

      
      template<class DiscreteFunctionSpaceType>
      void plotFunction(const Eigen::VectorXd& vec, 
                        const DiscreteFunctionSpaceType& space, 
                        const std::string& name) {
        typedef AdaptiveDiscreteFunction<DiscreteFunctionSpaceType> DiscreteFunctionType;
          DiscreteFunctionType func(name, space, vec.data());
          FunctionPlotter<DiscreteFunctionType> plotter(func, name);
          plotter.plot();
          return;
      }
    
    template< class GridType >
    void plotCellData(const Eigen::VectorXd& vec, 
                      const GridType& grid,
                      const std::string& name,
                      const std::string& path="./") {
      VTKWriter<typename GridType::LeafGridView> vtkwriter(grid.leafView());
      vtkwriter.addCellData(vec, name);
      vtkwriter.write(path+"/"+name);
      return;
    }

    template< class GridType >
    void plotVertexData(const Eigen::VectorXd& vec, 
                      const GridType& grid,
                      const std::string& name,
                      const std::string& path="./") {
      VTKWriter<typename GridType::LeafGridView> vtkwriter(grid.leafView());
      vtkwriter.addVertexData(vec, name);
      vtkwriter.write(path+"/"+name);
      return;
    }

    
      
      template<class DiscreteFunctionType>
      void plotFunction(DiscreteFunctionType& func, const std::string name) {
          FunctionPlotter<DiscreteFunctionType> plotter(func, name);
          plotter.plot();
          return;
      }

  } // namespace RB 
} // namespace Dune 


#endif /* __PLOTFUNCTION_HH__ */
