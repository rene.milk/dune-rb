#ifndef __VECTORIO_HH__
#define __VECTORIO_HH__

#include <ostream>


namespace Dune {
  
  namespace RB {

    template < class T >
    inline std::ostream& operator << (std::ostream& os, const std::vector<T>& v) {
      os << "[";
      for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
        {
          os << " " << *ii;
        }
      os << " ]";
      return os;
    }

    
  } // namespace RB 
  
} // namespace Dune 

#endif /* __VECTORIO_HH__ */
