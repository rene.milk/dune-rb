#include <config.h>

#include <dune/common/exceptions.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

using namespace Dune;
using namespace RB;

#ifndef POLORDER
const int polynomialOrder = 1;
#else
const int polynomialOrder = POLORDER;
#endif /* ifndef POLORDER */

int main(int argc, char *argv[])
{
    try {
        
        std::cout << "Usage: './" << argv[0] << " grid.macroGridFile:foo.dgf <additional fem parameters> nameOfYourFile.xdr'!\n";
        
        typedef GridSelector::GridType                                                       GridType;
        typedef LeafGridPart<GridType>                                                       GridPartType;
        typedef FunctionSpace<double, double, GridType::dimension, 1>                        FunctionSpaceType;
        typedef DiscontinuousGalerkinSpace<FunctionSpaceType, GridPartType, polynomialOrder> DiscFuncSpaceType;
        typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                  DiscreteFunctionType;
        
        typedef FunctionSpace<float, float, GridType::dimension, 1>                        FunctionSpaceTypeFloat;
        typedef DiscontinuousGalerkinSpace<FunctionSpaceTypeFloat, GridPartType, polynomialOrder> DiscFuncSpaceTypeFloat;
        typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                  DiscreteFunctionTypeFloat;

        
        MPIManager::initialize(argc, argv);
        Parameter::append(argc, argv);
        
        GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));
        GridType &grid = *gridPtr;
        
        // print some information about the grid
        GridPartType gridPart(grid);
        DiscFuncSpaceType discFuncSpace(gridPart);
        
        // all dune-fem parameters should have been stripped from argv by now
        std::string filename = argv[1];
        // read the function from file
        DiscreteFunctionType function("ReadFrom:"+filename, discFuncSpace);
        function.read_xdr(filename);
        
        // plot the function
        plotFunction(function);
    } catch (Dune::Exception e) {
        std::cout << e.what();
    }    
    return 0;
}
