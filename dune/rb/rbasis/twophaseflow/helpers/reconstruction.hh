#ifndef __RECONSTRUCTION_HH__
#define __RECONSTRUCTION_HH__

#include <vector>

#include <dune/grid/common/genericreferenceelements.hh>
#include <dune/common/exceptions.hh>

#include <dune/rb/rbasis/twophaseflow/helpers/generic2dp2lagrangeinterpolation.hh>


namespace Dune {
namespace RB {
  
  template<class DomainFunctionImpl, class RangeFunctionImpl>
  class Reconstruction
  {
    typedef DomainFunctionImpl                                          DomainFunctionType;
    typedef RangeFunctionImpl                                           RangeFunctionType;
    typedef typename DomainFunctionType::DiscreteFunctionSpaceType
    DomainFunctionSpaceType;
    typedef typename DomainFunctionType::LocalFunctionType              DomainLocalFunctionType;
    typedef typename RangeFunctionType::DiscreteFunctionSpaceType
    RangeFunctionSpaceType;
    typedef typename RangeFunctionType::LocalFunctionType               RangeLocalFunctionType;
    typedef typename RangeFunctionSpaceType::LagrangePointSetType       LagrangePointSetType;
    typedef typename DomainFunctionSpaceType::GridPartType              GridPartType;
    typedef typename GridPartType::GridType                             GridType;
    typedef FieldVector<double, 2>                                      CoordinateType;
    typedef typename GridType::Traits::template Codim<0>::EntityPointer EntityPointerType;
    typedef typename EntityPointerType::Entity                          EntityType;
    typedef typename RangeFunctionSpaceType::IteratorType               EntityIteratorType;
    typedef typename DomainFunctionType::RangeType                      DomainRangeType;
    typedef typename GridPartType::IntersectionIteratorType             IntersectionIteratorType;
    typedef typename EntityType::Geometry                               GeometryType;
    typedef Generic2DP2LagrangeInterpolation<GridType>                  GenericLagrangeInterpolation;

  public:
    Reconstruction(const DomainFunctionType& domainFunction,
                   RangeFunctionType& reconstruction,
                   const int subDomain)
      : domainFunction_(domainFunction),
        gridPart_(domainFunction.space().gridPart()),
        reconstruction_(reconstruction),
        subDomain_(subDomain)
    {};



    void reconstructOnEntity(const EntityType& entity) {
      if (subDomain_==-1 || gridPart_.getSubDomain(entity)==subDomain_)
        doReconstruction(entity);
    }


  private:
    void doReconstruction(const EntityType& entity) {
      typedef std::vector<CoordinateType>::const_iterator CoordinateVectorIterator;
      std::vector<CoordinateType> verticesGlobal;
      std::vector<DomainRangeType> evals;
      const DomainLocalFunctionType domainLocalFunc = domainFunction_.localFunction(entity);
      const GeometryType& enGeo = entity.geometry();

      // add all vertices of this cell
      for (int i=0; i!=enGeo.corners(); ++i) {
        const CoordinateType cornerGlobal = enGeo.corner(i);
        const CoordinateType cornerLocal = enGeo.local(cornerGlobal);
        verticesGlobal.push_back(cornerGlobal);
        DomainRangeType lfEval(0.0);
        domainLocalFunc.evaluate(cornerLocal, lfEval);
        evals.push_back(lfEval);
      }
      assert(verticesGlobal.size()==3);

      // Determine the type of the cell
      int cellType;
      // get the barycenter
      CoordinateType localBarycenter = GenericReferenceElements<typename GridType::ctype,2>
        ::general(entity.type()).position(0,0);
      CoordinateType globalBarycenter = enGeo.global(localBarycenter);
      const CoordinateVectorIterator vecEnd = verticesGlobal.end();
      int above = 0;
      for (CoordinateVectorIterator vecIt = verticesGlobal.begin(); vecIt!=vecEnd; ++vecIt) {
        if ((*vecIt)[1]>globalBarycenter[1])
          ++above;
      }
      if (above==1)
        cellType=1;
      else if (above==2)
        cellType=2;
      else
        //! \todo A manual would be nice
        DUNE_THROW(NotImplemented, 
                   "You have choosen a wrong grid type, please refer to the manual for valid grid types!");
      
      std::vector<CoordinateType> p(3);
      std::vector<DomainRangeType> f(3);

      int pointWithMaxYValue = 0;
      int pointWithMinYValue = 0;
      int k=0;
      for (CoordinateVectorIterator vecIt = verticesGlobal.begin(); vecIt!=vecEnd; ++vecIt, ++k) {
        if ((*vecIt)[1]>verticesGlobal[pointWithMaxYValue][1])
          pointWithMaxYValue=k;
        if ((*vecIt)[1]<verticesGlobal[pointWithMaxYValue][1])
          pointWithMinYValue=k;
      }
      // Depending on the cell type, define p_0-p_2
      if (cellType==1) {
        // we already found p_2:
        p[2]=verticesGlobal[pointWithMaxYValue];
        f[2]=evals[pointWithMaxYValue];
        verticesGlobal.erase(verticesGlobal.begin()+pointWithMaxYValue);
        evals.erase(evals.begin()+pointWithMaxYValue);
        if (verticesGlobal[0][0]<verticesGlobal[1][0]) {
          p[0]=verticesGlobal[0];
          f[0]=evals[0];
          p[1]=verticesGlobal[1];
          f[1]=evals[1];
        } else {
          p[0]=verticesGlobal[1];
          f[0]=evals[1];
          p[1]=verticesGlobal[0];
          f[1]=evals[0];
        }
      } else {
        // we already found p_2:
        p[2]=verticesGlobal[pointWithMinYValue];
        f[2]=evals[pointWithMinYValue];
        verticesGlobal.erase(verticesGlobal.begin()+pointWithMinYValue);
        evals.erase(evals.begin()+pointWithMinYValue);
        if (verticesGlobal[0][0]<verticesGlobal[1][0]) {
          p[1]=verticesGlobal[0];
          f[1]=evals[0];
          p[0]=verticesGlobal[1];
          f[0]=evals[1];
        } else {
          p[1]=verticesGlobal[1];
          f[1]=evals[1];
          p[0]=verticesGlobal[0];
          f[0]=evals[0];
        }
      }

      // define corners of the neighbor cells
      std::vector<CoordinateType> q;
      q.push_back(p[0]+(p[1]-p[2]));
      q.push_back(p[2]+(p[1]-p[0]));
      q.push_back(p[2]+(p[0]-p[1]));
      std::vector<DomainRangeType> g(3);
      std::vector<bool> foundQ(3, false);

      IntersectionIteratorType iend = gridPart_.iend(entity);
      for (IntersectionIteratorType iit=gridPart_.ibegin(entity);
           iit!=iend; ++iit) {
        if (iit->neighbor()) {// && (!gridPart_.onCoarseCellIntersection(*iit))) {
          const EntityPointerType outsidePtr = iit->outside();
          const EntityType& outsideEn = *outsidePtr;
          const GeometryType& outsideGeo = outsideEn.geometry();
          // Iterate over all outside vertices 
          for (int i=0; i!=outsideGeo.corners(); ++i) {
            const CoordinateType cornerGlobal = outsideGeo.corner(i);
            const CoordinateType cornerLocal = outsideGeo.local(cornerGlobal);
            for (int i=0; i!=3; ++i) {
              if (cornerGlobal==q[i]) {
                foundQ[i]=true;
                //                DomainRangeType lfEval(0.0);
                domainLocalFunc.evaluate(cornerLocal, g[i]);
              }
            }
          }
        }
      } // loop over all intersections of this cell

      // now, finally, extrapolate to the points, that were not found
      // q_1 should always be found
      if (foundQ[2]==false)
        DUNE_THROW(NotImplemented, "You have either choosen a wrong fine or coarse triangulation!");
      if (foundQ[0]==false)
        g[0]=f[0]+((g[2]-f[0])/(q[2][1]-p[0][1]))*(q[0][1]-p[0][1]);
      if (foundQ[1]==false)
        g[1]=f[2]+((g[2]-f[2])/(q[2][0]-p[2][0]))*(q[1][0]-p[2][0]);

      // now we have all points and evals together and can start the lagrange interpolation
      std::vector<CoordinateType> quadPoints(p);
      quadPoints.insert(quadPoints.end(), q.begin(), q.end());
      std::vector<DomainRangeType> quadWeights(f);
      quadWeights.insert(quadWeights.end(), g.begin(), g.end());

      // get a generic lagrange interpolation
      GenericLagrangeInterpolation lagrangeInterpolation(quadPoints, quadWeights);

      RangeLocalFunctionType reconsLocalFunc = reconstruction_.localFunction(entity);
      const LagrangePointSetType& lagrangePtSet = reconstruction_.space().lagrangePointSet(entity);
          
      // set dofs of reconstruction on this entity
      const int nop = lagrangePtSet.nop();
      for (int qP=0; qP!=nop; ++qP) {
        CoordinateType quadPointLocal = lagrangePtSet[qP].
          quadrature().point(lagrangePtSet[qP].point());
        CoordinateType quadPointGlobal = enGeo.global(quadPointLocal);
        DomainRangeType interpolVal(0.0);
        lagrangeInterpolation.evaluate(quadPointGlobal, interpolVal);
        bool quadPointOnBoundary = (quadPointGlobal[0]==1.0 || quadPointGlobal[1]==1.0 ||
                                    quadPointGlobal[0]==0.0 || quadPointGlobal[1]==0.0);
        if (!quadPointOnBoundary && (quadPointLocal[0]==0.5 || quadPointLocal[1]==0.5)) {
          reconsLocalFunc[qP] += 0.5*interpolVal;
        } else {
          reconsLocalFunc[qP] = interpolVal;
        }
      }
      return;
    }

  public:
    void reconstruct() {
      EntityIteratorType end = gridPart_.template end<0>();
      for (EntityIteratorType it = gridPart_.template begin<0>(); it!=end; ++it) {
        reconstructOnEntity(*it);
      }
      return;
    }

  private:
    const DomainFunctionType& domainFunction_;
    const GridPartType&       gridPart_;
    RangeFunctionType&        reconstruction_;
    const int                 subDomain_;
  };
  
} // namespace RB   
} // namespace Dune 

#endif /* __RECONSTRUCTION_HH__ */
