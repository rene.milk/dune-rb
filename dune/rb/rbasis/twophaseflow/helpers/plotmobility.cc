//
//  File.cpp
//  dune-rb
//
//  Created by Sven Kaulmann on 12.03.12.
//  Copyright (c) 2012 Universität Stuttgart. All rights reserved.
//
#include <config.h>

#include <iostream>

#include <dune/common/exceptions.hh>

// grid stuff
#include <dune/grid/common/gridinfo.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/groundwaterflow.hh>

#include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

// boost stuff
#include <boost/exception/all.hpp>

using namespace Dune;
using namespace RB;

#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/fem/operator/projection/l2projection.hh>

#ifndef POLORDER
const int polynomialOrder = 1;
#else
const int polynomialOrder = POLORDER;
#endif /* ifndef POLORDER */




template < class GridPartType, class ProblemType >
void visualizeMobility(GridPartType& gridPart, ProblemType& problem) {
    
    typedef FunctionSpace<double, double, GridPartType::dimension, 1> FunctionSpaceType;
    typedef FiniteVolumeSpace<FunctionSpaceType, GridPartType, 0>                           FiniteVolumeSpaceType;
    typedef AdaptiveDiscreteFunction<FiniteVolumeSpaceType>                                 FiniteVolumeFunctionType;
    
    // prepare output function
    FiniteVolumeSpaceType fvSpace(gridPart);
    FiniteVolumeFunctionType mobilityFunction("mobility", fvSpace);
    FunctionPlotter<FiniteVolumeFunctionType> plotter(mobilityFunction, "mobility");
    
    const int size = 6;
    for (int i=0; i!=size; ++i) {
        mobilityFunction.clear();
        typename ProblemType::ParameterDomainType param = ProblemType::ParameterDomainType::Unit(6, i)*0.95 
        + (ProblemType::ParameterDomainType::Ones(6)-ProblemType::ParameterDomainType::Unit(6, i))*0.01;
        problem.mobility().setParameter(param);
        
        // project the mobility to the finite volume space
        L2Projection<double, double, typename ProblemType::GroundwaterflowMobilityFunctionType, FiniteVolumeFunctionType> l2Projection;
        l2Projection(problem.mobility(), mobilityFunction);
    
        // plot the mobility function
        plotter.plotTimed();
    }
    return;
} 

int main(int argc, char *argv[])
{
    try {
        
        typedef GridSelector::GridType                                                         GridType;
        typedef LeafGridPart<GridType>                                                         GridPartType;
        typedef MultiDomainGridPart<GridPartType>                                              MDGridPartType;      
        typedef Groundwaterflow<1>                            ProblemType;
        
        MPIManager::initialize(argc, argv);
        Parameter::append(argc, argv);
        Parameter::append("model.nx", "60");
        Parameter::append("model.ny", "110");
        Parameter::append("model.nz", "85"); 
        Parameter::append("model.muLambda", "0.01,0.01,0.01,0.95,0.01,0.01"); 
        Parameter::append("fem.io.macroGridFile_3d", "../../../grid/groundwaterflowgrid.dgf");
        Parameter::append("fem.io.outputformat", "1");
        Parameter::append("fem.io.savestep", "1");
        Parameter::append("fem.io.savecount", "1");
        
        GridPtr<GridType> gridPtr("/Users/svenkaulmann/dune_2_1/dune-rb/dune/rb/rbasis/twophaseflow/grid/groundwaterflowgrid.dgf");
               
        GridType &grid = *gridPtr;
        
        // print some information about the grid
        gridinfo(grid);
        MDGridPartType mdGridPart(grid);
        // create problem
        ProblemType problem;
        
        /* visualize the permeability as finite volume function */
        visualizeMobility(mdGridPart, problem);
        
    }
    catch (const Exception& e) {
        std::cout << e.what();
        return 1;
    }
    catch (const std::exception& e) {
        std::cout << e.what();
        return 1;
    }
    catch (...) {
        std::cout << "Don't know what the problem is!\n";
        return 1;
    }
    return 0;
} /* main */

