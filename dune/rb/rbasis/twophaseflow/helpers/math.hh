#ifndef __MATH_HH__
#define __MATH_HH__

#include <algorithm>
#include <functional>
#include <cmath>
#include <numeric>
#include <vector>

#include <boost/bind.hpp>

namespace Dune {
  
  namespace RB {
    
    template<class VecType>
    double mean(const VecType& vec) {

      const double mean = accumulate(vec.begin(), vec.end(), 0.0)/vec.size();
      return mean;
    }

    template<class VecType>
    double stddeviation(const VecType& vec) {
      std::vector<double> zero_mean(vec.begin(), vec.end());
      transform( zero_mean.begin(), zero_mean.end(), zero_mean.begin(),bind2nd( std::minus<double>(), mean(vec) ) );

      double deviation = inner_product( zero_mean.begin(),zero_mean.end(), zero_mean.begin(), 0.0 );
      deviation = sqrt( deviation / ( vec.size() - 1 ) );
      return deviation;
    }

    /** mean of values of a std::map*/
    template<typename MapType>
    double meanOfValues(const MapType& map) {
      std::vector<typename MapType::value_type::second_type> vec;
      std::transform( map.begin(), map.end(),
                      std::back_inserter(vec),
                      boost::bind(&MapType::value_type::second,_1) );
      return mean(vec);
    }

    /** standard deviation of values of a std::map*/
    template<typename MapType>
    double stddeviationOfValues(const MapType& map) {
      std::vector<typename MapType::value_type::second_type> vec;
      std::transform( map.begin(), map.end(),
                      std::back_inserter(vec),
                      boost::bind(&MapType::value_type::second,_1) );
      return stddeviation(vec);
    }
    /** mean of keys of a std::map*/
    template<typename MapType>
    double meanOfKeys(const MapType& map) {
      typedef typename MapType::value_type::first_type MyType;
      std::vector<double> vec;
      std::transform( map.begin(), map.end(),
                      std::back_inserter(vec),
                      boost::bind(&MapType::value_type::first,_1) );
      return mean(vec);
    }

    /** standard deviation of keys of a std::map*/
    template<typename MapType>
    double stddeviationOfKeys(const MapType& map) {
      typedef typename MapType::value_type::first_type MyType;
      std::vector<double> vec;
      std::transform( map.begin(), map.end(),
                      std::back_inserter(vec),
                      boost::bind(&MapType::value_type::first,_1) );
      return stddeviation(vec);
    }
   
  } // namespace RB 
  
} // namespace Dune 
#endif /* __MATH_HH__ */
