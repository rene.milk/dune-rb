#ifndef _PARAMETERSPACE_H_
#define _PARAMETERSPACE_H_

#include <vector>
#include <iostream>
#include <numeric>

#include <dune/common/fvector.hh>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

namespace Dune {
  namespace RB {

    template<class CoordinateType, class DimensionVec>
    class UniformParameterSpace
    {
      typedef CoordinateType		ParameterDomainType;
      typedef DimensionVec		IntVector;

    public:
      UniformParameterSpace(const ParameterDomainType& begin,
                            const ParameterDomainType& end,
                            const IntVector& numParams)
        :begin_(begin), end_(end), numParams_(numParams),
         ParamDim(begin.size()),
         dimensionBase_(ParameterDomainType::Ones(ParamDim))
      {
        dimensionBase_.setConstant(1.0);
        // compute size of this parameter space
        size_=1.0;
        for (int i = 0; i!=ParamDim; ++i) {
          size_*=numParams_[i];
        }

        for (int i=1; i!=ParamDim; ++i) {
          for (int j=0; j!=i; ++j) {
            dimensionBase_(i)*=numParams_[j];
          }
        }
      };

      /** Constructor creating a space with the same
       *  amount of parameters in each direction.
       *  
       *  @param[in] begin std::vector defining the 
       *                   begin of the space
       *  @param[in] end   std::vector defining the 
       *                   end of the space
       *  @param[in] size  The number of parameters in each 
       *                   direction
       */
      UniformParameterSpace(const ParameterDomainType& begin,
                            const ParameterDomainType& end,
                            const unsigned int size)
        :begin_(begin),
         end_(end), 
         numParams_(begin.size(), size),
         ParamDim(begin.size()),
         dimensionBase_(ParameterDomainType::Ones(ParamDim))
      {
        // compute size of this parameter space
        size_=1.0;
        for (int i = 0; i!=ParamDim; ++i) {
          size_*=numParams_[i];
        }

        for (int i=1; i!=ParamDim; ++i) {
          for (int j=0; j!=i; ++j) {
            dimensionBase_(i)*=numParams_[j];
          }
        }
      };


      const int size() const {
        return size_;
      }

      void getParameter(const int i, ParameterDomainType& ret) const {
        assert(i>=0 && i<size());
        ret = ParameterDomainType::Zero(ParamDim);
        for (int comp=0; comp!=ParamDim; ++comp) {
          int numberInDim = 
            int(std::ceil(i/dimensionBase_(comp)))%numParams_[comp];
          assert(numberInDim<numParams_[comp]);
          ret(comp) = (end_(comp)-begin_(comp))*numberInDim/numParams_[comp]+begin_(comp);
        }
      } 

    protected:
      const ParameterDomainType begin_;
      const ParameterDomainType end_;
      const IntVector numParams_;
      const int ParamDim;
      ParameterDomainType dimensionBase_;
      int size_;

    };
    
    template<class CoordinateType, class DimensionVec>
    class TriangularParameterSpace : public UniformParameterSpace<CoordinateType, DimensionVec>
    {
      typedef UniformParameterSpace<CoordinateType, DimensionVec> BaseType;
      typedef CoordinateType		ParameterDomainType;
      typedef DimensionVec		IntVector;
      
      
    public:
      TriangularParameterSpace(const ParameterDomainType& begin,
                            const ParameterDomainType& end,
                            const IntVector& numParams)
      :BaseType(begin, end, numParams)
      {
        fillParameterVector();
      }
      
      /** Constructor creating a space with the same
       *  amount of parameters in each direction.
       *  
       *  @param[in] begin std::vector defining the 
       *                   begin of the space
       *  @param[in] end   std::vector defining the 
       *                   end of the space
       *  @param[in] size  The number of parameters in each 
       *                   direction
       */
      TriangularParameterSpace(const ParameterDomainType& begin,
                            const ParameterDomainType& end,
                            const unsigned int size)
      :BaseType(begin, end, size)
      {
        fillParameterVector();
      }

      const int size() const {
        return parameters_.size();
      }
      
      void getParameter(const int i, ParameterDomainType& ret) const {
        assert(i<this->size());
        ret = parameters_[i];
        return;
      }
      
    protected:
      void fillParameterVector() {
        for (int i=0; i!=BaseType::size(); ++i) {
          ParameterDomainType param;
          BaseType::getParameter(i, param);
          if (param.sum()<=1.0)
            parameters_.push_back(param);
        }
      }
      
//      const ParameterDomainType begin_;
//      const ParameterDomainType end_;
//      const IntVector numParams_;
//      const int ParamDim;
//      ParameterDomainType dimensionBase_;
//      int size_;
      std::vector<ParameterDomainType> parameters_;
      
    };

      template<class CoordinateType>
      class RandomParameterSpace : public UniformParameterSpace<CoordinateType, std::vector<int> >
      {
          typedef UniformParameterSpace<CoordinateType, std::vector<int> > BaseType;
          typedef CoordinateType		ParameterDomainType;
          
          
      public:     
          /** Constructor creating a space with a given overall size for a given random seed.
           *  
           *  @param[in] begin std::vector defining the 
           *                   begin of the space
           *  @param[in] end   std::vector defining the 
           *                   end of the space
           *  @param[in] size  The desired size of the space
           *  @param[in] seed  The random seed for the parameters
           */
          RandomParameterSpace(const ParameterDomainType& begin,
                               const ParameterDomainType& end,
                               const unsigned int size,
                               const uint64_t seed)
          :BaseType(begin, end, size),
          parameters_(size, ParameterDomainType::Zero(begin.rows()))
          {
              assert(begin.rows()==end.rows());
              
              const int parSize = begin.size();
              for (int parEntry=0; parEntry!=parSize; ++parEntry) {
                  
                  // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
                  // Define a base random number generator and initialize it with a seed.
                  boost::minstd_rand baseGen((parEntry+1)*seed);
                  // Define distribution U[begin(parEntry), end(parEntry)) [double values]
                  boost::uniform_real<> uniDblUnit(begin(parEntry), end(parEntry));
                  // Define a random variate generator using our base generator and distribution
                  boost::variate_generator<boost::minstd_rand&, boost::uniform_real<> > uniDblGen(baseGen, uniDblUnit);
                  
                  for (int i=0; i!=size; ++i) {
                      parameters_[i](parEntry) = uniDblGen();
                  }
              }
          }
          
          /** Constructor creating a space with a given overall size.
           *  
           *  @param[in] begin std::vector defining the 
           *                   begin of the space
           *  @param[in] end   std::vector defining the 
           *                   end of the space
           *  @param[in] size  The desired size of the space
           */
          RandomParameterSpace(const ParameterDomainType& begin,
                               const ParameterDomainType& end,
                               const unsigned int size)
          :BaseType(begin, end, size),
          parameters_(size, ParameterDomainType::Zero(begin.rows()))
          {
              assert(begin.rows()==end.rows());
              
              const int parSize = begin.size();
              for (int parEntry=0; parEntry!=parSize; ++parEntry) {
                  
                  // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
                  // Define a base random number generator and initialize it with a seed.
                  
                  uint64_t seed;
                  std::ifstream urandom;
                  urandom.open("/dev/urandom");
                  urandom.read(reinterpret_cast<char*> (&seed), sizeof (seed));
                  urandom.close();
                  boost::minstd_rand baseGen(seed);
                  // Define distribution U[begin(parEntry), end(parEntry)) [double values]
                  boost::uniform_real<> uniDblUnit(begin(parEntry), end(parEntry));
                  // Define a random variate generator using our base generator and distribution
                  boost::variate_generator<boost::minstd_rand&, boost::uniform_real<> > uniDblGen(baseGen, uniDblUnit);
                  
                  for (int i=0; i!=size; ++i) {
                      parameters_[i](parEntry) = uniDblGen();
                  }
              }
          }
          
          
          const int size() const {
              return parameters_.size();
          }
          
          void getParameter(const int i, ParameterDomainType& ret) const {
              assert(i<this->size());
              ret = parameters_[i];
              return;
          }
          
      protected:    
          
          
          std::vector<ParameterDomainType> parameters_;
          
      };
      
    template<class CoordinateType>
    class TriangularRandomParameterSpace : public UniformParameterSpace<CoordinateType, std::vector<int> >
    {
      typedef UniformParameterSpace<CoordinateType, std::vector<int> > BaseType;
      typedef CoordinateType		ParameterDomainType;
    
      
    public:     
      /** Constructor creating a space with a given overall size.
       *  
       *  @param[in] begin std::vector defining the 
       *                   begin of the space
       *  @param[in] end   std::vector defining the 
       *                   end of the space
       *  @param[in] size  The desired size of the space
       */
      TriangularRandomParameterSpace(const ParameterDomainType& begin,
                            const ParameterDomainType& end,
                            const unsigned int size)
        :BaseType(begin, end, size),
         parameters_(size, ParameterDomainType::Zero(begin.rows()))
      {
        assert(begin.rows()==end.rows());

        const int parSize = begin.size();
        for (int parEntry=0; parEntry!=parSize; ++parEntry) {

          // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
          // Define a base random number generator and initialize it with a seed.

          uint64_t seed;
          std::ifstream urandom;
          urandom.open("/dev/urandom");
          urandom.read(reinterpret_cast<char*> (&seed), sizeof (seed));
          urandom.close();
          boost::minstd_rand baseGen(seed);
          // Define distribution U[begin(parEntry), end(parEntry)) [double values]
          boost::uniform_real<> uniDblUnit(begin(parEntry), end(parEntry));
          // Define a random variate generator using our base generator and distribution
          boost::variate_generator<boost::minstd_rand&, boost::uniform_real<> > uniDblGen(baseGen, uniDblUnit);
          
          for (int i=0; i!=size; ++i) {
            parameters_[i](parEntry) = uniDblGen();
          }
        }
        // scale all parameters down to have one-norm 1.
        for (int i=0; i!=size; ++i) {
          parameters_[i] /= parameters_[i].sum();
        }
      }


      TriangularRandomParameterSpace(const ParameterDomainType& begin,
                                     const ParameterDomainType& end,
                                     const unsigned int size,
                                     const uint64_t seed)
        :BaseType(begin, end, size),
         parameters_(size, ParameterDomainType::Zero(begin.rows()))
      {
        assert(begin.rows()==end.rows());
              
        const int parSize = begin.size();
        for (int parEntry=0; parEntry!=parSize; ++parEntry) {
                  
          // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
          // Define a base random number generator and initialize it with a seed.
          boost::minstd_rand baseGen((parEntry+1)*seed);
          // Define distribution U[begin(parEntry), end(parEntry)) [double values]
          boost::uniform_real<> uniDblUnit(begin(parEntry), end(parEntry));
          // Define a random variate generator using our base generator and distribution
          boost::variate_generator<boost::minstd_rand&, boost::uniform_real<> > uniDblGen(baseGen, uniDblUnit);
                  
          for (int i=0; i!=size; ++i) {
            parameters_[i](parEntry) = uniDblGen();
          }
        }
        // scale all parameters down to have one-norm 1.
        for (int i=0; i!=size; ++i) {
          parameters_[i] /= parameters_[i].sum();
        }
      }
      




      const int size() const {
        return parameters_.size();
      }
      
      void getParameter(const int i, ParameterDomainType& ret) const {
        assert(i<this->size());
        ret = parameters_[i];
        return;
      }
      
    protected:    


      std::vector<ParameterDomainType> parameters_;
      
    };
    
  } // namespace Dune
} // namespace RB
  
#endif /* _PARAMETERSPACE_H_ */
