#ifndef __INTERPOLATEDISCRETEFUNCTION_HH__
#define __INTERPOLATEDISCRETEFUNCTION_HH__

namespace Dune {
namespace RB {
  
  template<class FunctionType, class DiscreteFunctionType>
  void interpolateDiscreteFunction ( const FunctionType &function,
                                     DiscreteFunctionType &discreteFunction,
                                     const int subDomain )
   {

     typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType; 
     typedef typename DiscreteFunctionSpaceType::IteratorType         IteratorType;
     typedef typename FunctionType::LocalFunctionType                 FunctionLocalFunctionType;
     typedef typename DiscreteFunctionType::LocalFunctionType         LocalFunctionType;
     typedef typename FunctionType::LocalFunctionType::RangeType      RangeType;
     typedef typename DiscreteFunctionSpaceType::GridPartType         GridPartType;
     typedef typename DiscreteFunctionSpaceType::LagrangePointSetType LagrangePointSetType;

     static const int dimRange = DiscreteFunctionSpaceType::dimRange;
     const DiscreteFunctionSpaceType &dfSpace = discreteFunction.space();
     const GridPartType& gridPart = discreteFunction.space().gridPart();

     IteratorType endit = dfSpace.end();
     for( IteratorType it = dfSpace.begin(); it != endit; ++it )
       {
         if (gridPart.getSubDomain(*it)==subDomain) {
           const LagrangePointSetType &lagrangePointSet
             = dfSpace.lagrangePointSet( *it );

           FunctionLocalFunctionType f_local = function.localFunction( *it );
           LocalFunctionType df_local = discreteFunction.localFunction( *it );
      
           // assume point based local dofs 
           const int nop = lagrangePointSet.nop();
           int k = 0;
           for( int qp = 0; qp < nop; ++qp )
             {
               RangeType phi;
               f_local.evaluate( lagrangePointSet[ qp ], phi );
               for( int i = 0; i < dimRange; ++i, ++k )
                 df_local[ k ] = phi[ i ];
             }
         }
       }
  }

  
} // namespace RB   
} // namespace Dune 

#endif /* __INTERPOLATEDISCRETEFUNCTION_HH__ */
