#include <config.h>

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>



// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>

// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/lagrangeinterpolation.hh>
// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// rb stuff
#include "../reconstruction.hh"

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

using namespace Dune;
using namespace RB;

struct MyFunction {
  typedef FunctionSpace< double, double, 2, 1 > FunctionSpaceType;

  static const int dimDomain = FunctionSpaceType::dimDomain;

  typedef FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef FunctionSpaceType::DomainType DomainType;
  typedef FunctionSpaceType::RangeType RangeType;

  typedef FunctionSpaceType::JacobianRangeType JacobianRangeType;
  typedef FunctionSpaceType::HessianRangeType HessianRangeType;

  void evaluate ( const DomainType &x, RangeType &ret ) const {
    ret = x*x;
    return;
  }
};

/** evaluate the gradient of a given discrete function*/
template<class DFImpl>
struct MyGradientFunction {
  typedef DFImpl DFType;
  typedef FunctionSpace< double, double, 2, 1 > FunctionSpaceType;

  static const int dimDomain = FunctionSpaceType::dimDomain;

  typedef FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef FunctionSpaceType::DomainType DomainType;
  typedef FunctionSpaceType::RangeType RangeType;

  typedef FunctionSpaceType::JacobianRangeType JacobianRangeType;
  typedef FunctionSpaceType::HessianRangeType HessianRangeType;

  MyGradientFunction(const DFType& df): df_(df) {};

  template<typename T>
  void evaluate ( const DomainType &x, T &ret ) const {
    JacobianRangeType jac(0.0);
    df_.jacobian(x, jac);
    ret = jac[0];
    return;
  }

private:
  const DFType& df_;
};

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                            GridType;
    typedef LeafGridPart<GridType>                                            GridPart;
    typedef MultiDomainGridPart<GridPart>                                     MDGridPart;
    typedef FunctionSpace<double, double, GridType::dimension, 1>             DomainFunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<DomainFunctionSpace, MDGridPart, 1> DomainDiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<DomainDiscFuncSpaceType>                 DomainDiscreteFunctionType;

    typedef FunctionSpace<double, double, GridType::dimension, 1>             ReconsFunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<ReconsFunctionSpace, MDGridPart, 2> ReconsDiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<ReconsDiscFuncSpaceType>                 ReconsDiscreteFunctionType;

    typedef FunctionSpace<double, double, GridType::dimension, 2>               GradientFunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<GradientFunctionSpace, MDGridPart, 1> GradientDiscreteFunctionSpace;
    typedef AdaptiveDiscreteFunction<GradientDiscreteFunctionSpace>             GradientDiscreteFunctionType;

    typedef ReconsDiscFuncSpaceType::IteratorType                             EntityIteratorType;
    typedef ReconsDiscFuncSpaceType::EntityType                               EntityType;
    typedef Reconstruction<DomainDiscreteFunctionType, ReconsDiscreteFunctionType> ReconstructionType;


    MPIManager::initialize(argc, argv);
    Parameter::append("reconstructiontest.params");

    GridPtr<GridType> gridPtr("../../grid/unitcube2.dgf");
    GridType &grid = *gridPtr;
    // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("fem.globalRefine", 0));
    // print some information about the grid
    Dune::gridinfo(grid);
    MDGridPart gridPart(grid);
    Dune::VTKWriter<GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.write("grid");
    DomainDiscFuncSpaceType domainDiscFuncSpace(gridPart);
    DomainDiscreteFunctionType domainFunction("orignal", domainDiscFuncSpace);
    ReconsDiscFuncSpaceType reconsDiscFuncSpace(gridPart);
    ReconsDiscreteFunctionType reconsFunction("reconstruction", reconsDiscFuncSpace);
    domainFunction.clear();
    // fill the original function with quadratic data
    LagrangeInterpolation<DomainDiscreteFunctionType> interpol;
    MyFunction quadratic;
    interpol.interpolateFunction(quadratic, domainFunction);

    GrapeDataDisplay<GridType> grapeDisplay(grid);
    grapeDisplay.addData(domainFunction);
    ReconstructionType reconstruction(domainFunction, reconsFunction, -1);
    reconstruction.reconstruct();
    FunctionPlotter<ReconsDiscreteFunctionType> plotter(reconsFunction);

    MyGradientFunction<ReconsDiscreteFunctionType> gradientFunction(reconsFunction);
    GradientDiscreteFunctionSpace gradientDiscFuncSpace(gridPart);
    GradientDiscreteFunctionType gradientDiscreteFunction("gradient", gradientDiscFuncSpace);
    LagrangeInterpolation<GradientDiscreteFunctionType> gradientInterpol;
    //gradientInterpol.interpolateFunction(gradientFunction, gradientDiscreteFunction);

    plotter.plot();
    grapeDisplay.addData(reconsFunction);
    grapeDisplay.addData(gradientDiscreteFunction);
    grapeDisplay.display();

    // compute errors
    // create L2 - Norm 
    Dune::L2Norm<MDGridPart> l2norm(gridPart);
    // calculate L2 - Norm 
    const double l2error = l2norm.distance( domainFunction, reconsFunction );      /*@LST0E@*/
    // create H1 - Norm 
    Dune::H1Norm<MDGridPart> h1norm(gridPart);
    const double h1error = h1norm.distance( domainFunction, reconsFunction );
    std::cout << "L2 Error: " << l2error << "\tH1Error: " << h1error << "\n";
    



    // for (EntityIteratorType it = gridPart.begin<0>(); it != gridPart.end<0>(); ++it) {
    //   const EntityType& e = *it;
 
    // }


  } catch (Dune::Exception e) {
    std::cerr << e.what();
    return 1;
  }  
  return 0;
}
