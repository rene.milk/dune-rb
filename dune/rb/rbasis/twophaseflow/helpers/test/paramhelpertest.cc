#include <config.h>

#include <iostream>


#include <dune/common/fvector.hh>

#include "../parameterspace.hh"


int main(int argc, char *argv[])
{
  typedef std::vector<double> VecType;
  typedef std::vector<int> IntVecType;
  VecType begin(4, 0.0);
  VecType end(4, 1.0);  
  IntVecType numParams(4, 4);
  Dune::RB::UniformParameterSpace<VecType, IntVecType> paramSpace(begin, end, numParams);
  std::cout << "Size: " << paramSpace.size() << std::endl;
  std::vector<double> paramVec;
  for (int i = 0; i<paramSpace.size(); ++i) {
    paramVec.clear();
    paramSpace.getParameter(i, paramVec);
    std::cout << "Parameter " << i << ": ";
    for (int j=0; j!=4; ++j)
      std::cout << paramVec[j] << " ";
    std::cout << std::endl;
  }
  return 0;
}

