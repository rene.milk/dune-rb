#ifndef __ADDSPARSEMATRICES_HH__
#define __ADDSPARSEMATRICES_HH__


#include <vector>
// for fabs
#include <math.h>
#include <cassert>


#include <dune/fem/operator/matrix/istlmatrix.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>

namespace Dune {
namespace RB {
  
  
  template <class LinearOperatorImp> 
  class AddMatrixObject
  {
    typedef LinearOperatorImp LinearOperator;
    typedef typename LinearOperator::MatrixType Matrix;
  public:
    AddMatrixObject()
    {}
    
    ~AddMatrixObject() {};
    
    
    virtual void addMatrix(const Matrix& mPtr);
    
    
    virtual void addScaledMatrix(const Matrix& mPtr, const double& factor);
    
    virtual LinearOperator& sumUp(const double eps=1e-14);
    
    virtual LinearOperator& getSum();
    
    virtual void clearSum();
    
    virtual void clearAll();
  };
  

/** @class AddSparseMatrices
 *
 * This is just a helper class that adds two matrices of type FEM::SparseRowMatrix
 */
  template <class RowSpaceImp, class ColSpaceImp, class TraitsImp> 
  class AddMatrixObject<SparseRowMatrixObject<RowSpaceImp, ColSpaceImp, TraitsImp> >
  {
    typedef SparseRowMatrixObject<RowSpaceImp, ColSpaceImp, TraitsImp> LinearOperator;
    typedef typename LinearOperator::MatrixType Matrix;
  public:
    template<class DFSpace>
    AddMatrixObject(const DFSpace& dfSpace):sum_(dfSpace, dfSpace)
    {
      sum_.reserve();
      sum_.clear();
    };
    
    ~AddMatrixObject() {};
    
    
    void addMatrix(const Matrix& mPtr) {
      addScaledMatrix(&mPtr, 1.0);
      return;
    }
    
    void addScaledMatrix(const Matrix& mPtr, const double& factor) {
      // assert(mPtr.rows() == sum_.matrix().rows());
      // assert(mPtr.cols() == sum_.matrix().cols());
      matrices_.push_back(&mPtr);
      scaleFactors_.push_back(factor);
      return;
    }
    
    LinearOperator& sumUp(const double eps=1e-14) {
      const unsigned int size = matrices_.size();
      assert(size==scaleFactors_.size());
      Matrix &sumMatrix = sum_.matrix();
      for (unsigned int k=0; k!=size; ++k) {
        const double& scale = scaleFactors_[k];
        const int rows = matrices_[k]->rows();
        for (int i = 0; i < rows; ++i) {
          const int cols = matrices_[k]->numNonZeros(i);
          for (int j = 0; j < cols; ++j) {
            const int col = matrices_[k]->realCol(i,j);
            assert(matrices_[k]->find(i,col));
            double value = (*matrices_[k])(i,col);
            value *= scale;
            if (fabs( value ) > eps)
              sumMatrix.add(i, col, value);
          }
        }
      }
      return sum_;
    }
    
    LinearOperator& getSum() {
      return sum_;
    }
    
    void clearSum() {
      sum_.clear();
      return;
      
    }
    
    void clearAll() {
      sum_.clear();
      matrices_.clear();
      scaleFactors_.clear();
      return;
    } 
    
  protected:
    std::vector<const Matrix*> matrices_;
    std::vector<double>        scaleFactors_;
    LinearOperator             sum_;
    
  };

  template <class RowSpaceImp, class ColSpaceImp, class TraitsImp> 
  class AddMatrixObject<ISTLMatrixObject<RowSpaceImp, ColSpaceImp, TraitsImp> > {

  typedef ISTLMatrixObject<RowSpaceImp, ColSpaceImp, TraitsImp> LinearOperatorType;
  typedef typename LinearOperatorType::MatrixType MatrixType;

public:

  template<class DFSpace>
  AddMatrixObject(const DFSpace& dfSpace):sum_(dfSpace, dfSpace)
  {
    sum_.reserve();
    sum_.clear();
  };
 
  ~AddMatrixObject() {};


  void addMatrix(const MatrixType& mPtr) {
    addScaledMatrix(&mPtr, 1.0);
    return;
  }

  void addScaledMatrix(const MatrixType& mPtr, const double& factor) {
    // assert(mPtr.rows() == sum_.matrix().rows());
    // assert(mPtr.cols() == sum_.matrix().cols());
    matrices_.push_back(&mPtr);
    scaleFactors_.push_back(factor);
    return;
  }

  LinearOperatorType& sumUp(const double eps=1e-14) {
    typedef typename MatrixType::RowIterator      RowIteratorType;
    typedef typename MatrixType::ConstRowIterator ConstRowIteratorType;
    typedef typename MatrixType::ColIterator      ColIteratorType;
    typedef typename MatrixType::ConstColIterator ConstColIteratorType;

    const unsigned int size = this->matrices_.size();
    assert(size==this->scaleFactors_.size());
    MatrixType& sumMatrix = this->sum_.matrix();
    for (unsigned int k=0; k!=size; ++k) {
      assert(this->matrices_[k]->N()==sumMatrix.N() 
             && this->matrices_[k]->M()==sumMatrix.M());
      RowIteratorType sumEnd=sumMatrix.end();
      ConstRowIteratorType summandRow=this->matrices_[k]->begin();
      for (RowIteratorType sumRow=sumMatrix.begin(); sumRow!=sumEnd;
           ++sumRow, ++summandRow){
        sumRow->axpy(this->scaleFactors_[k], *summandRow);
      }
    }
    return this->sum_;
  }

  LinearOperatorType& getSum() {
    return sum_;
  }

  void clearSum() {
    sum_.clear();
    return;

  }

  void clearAll() {
    sum_.clear();
    matrices_.clear();
    scaleFactors_.clear();
    return;
  } 

protected:
  std::vector<const MatrixType*> matrices_;
  std::vector<double>        scaleFactors_;
  LinearOperatorType             sum_;


};

} // namespace RB
} // namespace Dune


#endif /* __ADDSPARSEMATRICES_HH__ */
