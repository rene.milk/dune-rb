#ifndef __EXECUTIONTIMER_HH__
#define __EXECUTIONTIMER_HH__

#include <boost/date_time/posix_time/posix_time.hpp>

namespace Dune {
  
  namespace RB {
    
    class MyTimer {
    public:
      static void startTimer() {
        startt_ = boost::posix_time::microsec_clock::local_time();
        return;
      }

      static double stopTimerSeconds() {
        stopt_ = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration duration = stopt_-startt_;
        return duration.total_seconds();
      }

      static double stopTimerMilliseconds() {
        stopt_ = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration duration = stopt_-startt_;
        return duration.total_milliseconds();
      }

      static double stopTimerMicroseconds() {
        stopt_ = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration duration = stopt_-startt_;
        return duration.total_microseconds();
      }


    private:
      static boost::posix_time::ptime startt_;
      static boost::posix_time::ptime stopt_;
    };

    boost::posix_time::ptime MyTimer::startt_;
    boost::posix_time::ptime MyTimer::stopt_;


#define EXECUTETIMEDSECONDS(command) \
    (MyTimer::startTimer(),command,MyTimer::stopTimerSeconds())

#define EXECUTETIMEDMILLISECONDS(command) \
    (MyTimer::startTimer(),command,MyTimer::stopTimerMilliseconds())

#define EXECUTETIMEDMICROSECONDS(command) \
    (MyTimer::startTimer(),command,MyTimer::stopTimerMicroseconds())

    
  } // namespace RB 
  
} // namespace Dune 

#endif /* __EXECUTIONTIMER_HH__ */
