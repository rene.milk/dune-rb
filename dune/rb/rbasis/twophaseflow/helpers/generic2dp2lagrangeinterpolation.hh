#ifndef __GENERIC2DP2LAGRANGEINTERPOLATION_HH__
#define __GENERIC2DP2LAGRANGEINTERPOLATION_HH__

// stl
#include <vector>

// dune-common
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>



namespace Dune {
namespace RB {
  
  template<class GridImpl>
  class Generic2DP2LagrangeInterpolation
  {
    typedef GridImpl                  GridType;
    typedef FieldVector<double, 2>    CoordinateType;
    typedef FieldVector<double, 1>    RangeType;
    typedef FieldVector<double, 6>    CoefficientType;
    typedef FieldMatrix<double, 6, 6> MatrixType;


  public:
    Generic2DP2LagrangeInterpolation(const std::vector<CoordinateType> coords, 
                                     const std::vector<RangeType> evals)
      : evals_(evals),
        coeffs_(6),
        matrix_(0.0)
    {
      assert(evals.size()==6);
      assert(coords.size()==6);

      // prepare matrix
      for (int r=0; r!=6; ++r) {
        const CoordinateType& point = coords[r];
        typename MatrixType::row_type& row = matrix_[r];
        row[0] = point[0]*point[0];
        row[1] = point[1]*point[1];
        row[2] = point[0]*point[1];
        row[3] = point[0];
        row[4] = point[1];
        row[5] = 1.0;
      }
      // invert matrix
      matrix_.invert();
      // compute coefficients for base functions
      CoefficientType rhs(0.0);
      rhs[0]=1.0;
      for (int i=0; i!=6; ++i) {
        matrix_.mv(rhs, coeffs_[i]);
        if (i!=5) {
          rhs[i]=0.0;
          rhs[i+1]=1.0;
        }
      }
    }

    void evaluate(const CoordinateType& evalPt, RangeType& ret) const {
      const double x = evalPt[0];
      const double y = evalPt[1];
      ret[0] = RangeType(0.0);
      // loop over all baseFunctions, evaluate them in evalPt
      for (int i=0; i!=6; ++i) {
        double baseFuncValue(0.0);
        const CoefficientType& baseFuncCoeffs = coeffs_[i];
        baseFuncValue = baseFuncCoeffs[0]*x*x;
        baseFuncValue += baseFuncCoeffs[1]*y*y;
        baseFuncValue += baseFuncCoeffs[2]*x*y;
        baseFuncValue += baseFuncCoeffs[3]*x;
        baseFuncValue += baseFuncCoeffs[4]*y;
        baseFuncValue += baseFuncCoeffs[5];
        baseFuncValue *= evals_[i];
        ret[0] += baseFuncValue;
      }
      return;
    } 

  private:
    const std::vector<RangeType> evals_;
    std::vector<CoefficientType> coeffs_;
    MatrixType                   matrix_;
  };
  
} // namespace RB   
} // namespace Dune 

#endif /* __GENERIC2DP2LAGRANGEINTERPOLATION_HH__ */
