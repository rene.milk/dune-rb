//
// seperableparametricfunction.h
// dune-rb
//
// Created by Sven Kaulmann on 10.01.12.
// Copyright 2012. All rights reserved.
//

#ifndef DUNE_RB_SEPERABLEPARAMETRICFUNCTION
#define DUNE_RB_SEPERABLEPARAMETRICFUNCTION


namespace Dune {
namespace RB {


/** Interface class for all seperable parametric functions.
 *
 *  A seperable parametric function is a function
 *  @f$f:X\times\mathcal{P}\rightarrow\mathbb{R}^k@f$
 *  with a Hilbert space @f$X@f$ (spatial domain) and
 *  a open bounded set @f$\mathcal{P}@f$ (parameter domain) that can be written as
 *  @f{align}{
 *    f(x,\mu)=\sum_{i=1}^N \theta_i(\mu)f_i(x)
 *  @f}
 *  with parameter dependent functions ("coefficients") @f$\theta_i@f$ and parameter independent functions
 *  ("components") @f$ f_i@f$.
 *
 *  @remark The coefficient is assumed to be double-valued!
 */
template <class DomainImpl, class RangeImpl, class ParameterDomainImpl>
class SeperableParametricFunctionInterface {

public:
    //! The type of the spatial domain.
    typedef DomainImpl DomainType;
    //! The type of the spatial range.
    typedef RangeImpl RangeType;
    //! The type of the parameter domain.
    typedef ParameterDomainImpl ParameterDomainType;
    //! The field type in the range
    typedef typename RangeType::field_type RangeFieldType;
    
    /** Return the number of summands
     *
     *  @returns The number of summands in this function.
     */
    virtual const int size() const = 0;

    /** Evaluate a coefficient in a given point in the parameter domain.
     *
     *  @param[in] i  The number of the coefficient.
     *  @param[in] mu The point in parameter space where the coefficient shall be evaluated.
     *  @returns The evaluation of the i'th coefficient.
     */
    virtual const double coefficient(const int i, const ParameterDomainType& mu) const = 0;

    /** Evaluate a coefficient in the point in parameter domain that was set using the
     *  SeperableParametricFunctionInterface::setParameter function.
     *
     *  @warning If the parameter was not set before, the parameter set by ParameterDomainType() will be used!
     *
     *  @param[in] i  The number of the coefficient.
     *  @param[in] mu The point in parameter space where the coefficient shall be evaluated.
     *  @returns The evaluation of the i'th coefficient.
     */
    const double coefficient(const int i) const {
        return coefficient(i, mu_);
    }

    /** Evaluate a component in a given point in the spatial domain.
     *
     *  @param[in] i The number of the component.
     *  @param[in] x The point where the component shall be evaluated.
     *  @returns The evaluation of the desired component.
     */
    virtual const RangeType component(const int i, const DomainType& x) const = 0;

    /** Set the parameter for the next runs.
     *
     *  This sets the parameter @f$\mu@f$ to a constant value. After the call to this
     *  function, the "coefficient" function can be called without the parameter @f$\mu@f$.
     *
     *  @param[in] mu The parameter that shall be used from now on.
     */
    void setParameter(const ParameterDomainType& mu) {
        mu_=mu;
        return;
    }

    /** Get the parameter stored in this class.
     *
     *  This returns the parameter @f$\mu@f$ that was stored using the setParameter method.
     *
     *  @returns The parameter @f$\mu@f$.
     */
    const ParameterDomainType& getParameter() const {
        return mu_;
    }

    /** Evaluate the whole function in a given spatial and parameter point.
     *
     *  This evaluates the sum
     *  @f{align}{
     *  f(x,\mu)=\sum_{i=1}^N \theta_i(\mu)f_i(x)
     *  @f}
     *
     *  @param[in] x  The point in the spatial domain where the evaluation is desired.
     *  @param[in] mu The point in the parameter domain where the evaluation is desired.
     *  @returns Returns the evaluation of this function.
     */
    const RangeType evaluate(const DomainType& x, const ParameterDomainType& mu) const {
        RangeType ret(0.0);
        for (int i=0; i!=size(); ++i) {
            ret+=component(i, x)*coefficient(i, mu);
        }
        return ret;
    }

    /** Evaluate the whole function in a given spatial point.
     *
     *  This evaluates the sum
     *  @f{align}{
     *  f(x,\bar\mu)=\sum_{i=1}^N \theta_i(\bar\mu)f_i(x)
     *  @f}
     *  where @$f\bar\mu@f$ is the parameter saved in this function beforehand using the
     *  SeperableParametricFunctionInterface::setParameter function.
     *
     *  @param[in] x  The point in the spatial domain where the evaluation is desired.
     *  @returns Returns the evaluation of this function.
     */
    const RangeType evaluate(const DomainType& x) const {
        RangeType ret(0.0);
        for (int i=0; i!=size(); ++i) {
            ret+=component(i, x)*coefficient(i, mu_);
        }
        return ret;
    }
    
    /** Evaluate the whole function in a given spatial point.
     *
     *  This evaluates the sum
     *  @f{align}{
     *  f(x,\bar\mu)=\sum_{i=1}^N \theta_i(\bar\mu)f_i(x)
     *  @f}
     *  where @$f\bar\mu@f$ is the parameter saved in this function beforehand using the
     *  SeperableParametricFunctionInterface::setParameter function.
     *
     *  @param[in]  x   The point in the spatial domain where the evaluation is desired.
     *  @param[out] ret The evaluation of this function.
     */
    void evaluate(const DomainType& x, RangeType& ret) const {
        ret = RangeType(0.0);
        for (int i=0; i!=size(); ++i) {
            ret+=component(i, x)*coefficient(i, mu_);
        }
        return;
    }


protected:
    ParameterDomainType mu_;
};

} // namespace Dune
} // namespace RB

#endif // ifndef DUNE_RB_SEPERABLEPARAMETRICFUNCTION