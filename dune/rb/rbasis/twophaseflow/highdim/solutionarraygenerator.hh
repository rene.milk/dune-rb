#ifndef __SOLUTIONARRAYGENERATOR_HH__
#define __SOLUTIONARRAYGENERATOR_HH__

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>


namespace Dune {
  namespace RB {

    class SolutionArrayGenerator {

      typedef LeafGridPart<OneDGrid>               GridPartType;
      typedef GridPartType::Codim<0>::IteratorType IteratorType;
      typedef IteratorType::Entity                 EntityType;
      typedef EntityType::Geometry                 GeometryType;
      typedef GeometryType::LocalCoordinate        LocalCoordinateType;
      typedef GeometryType::GlobalCoordinate       GlobalCoordinateType;
      typedef CachingQuadrature<GridPartType, 0>   QuadratureType;

    public:
      SolutionArrayGenerator(const double eps,
                             const int quadGridSize,
                             const double gridLeftBorder,
                             const double gridRightBorder)
        : eps_(eps),
          quadGridSize_(quadGridSize),
          quadGrid_(quadGridSize, gridLeftBorder, gridRightBorder),
          quadGridPart_(quadGrid_),
          ceps_(calcCeps())
      {
        exactVals_ = new double[quadGridSize_];
        calcFuncValues();
      }

      ~SolutionArrayGenerator() {
        delete exactVals_;
        exactVals_ = 0;
      }

      const double ceps() const {
        return ceps_;
      }
      
      const double* funcValueArray() const {
        return exactVals_;
      }

      const int arraySize() const {
        return quadGridSize_;
      }

      /** @param[in] epsName optional: give the constants a name*/
      void write(const std::string& filename, 
                 const std::string& epsName="") const;

    private:
      double calcCeps();
      void calcFuncValues();
      const double a0(const double& x) const;
      const double a1(const double& x) const;

      double       eps_;
      const int    quadGridSize_;
      OneDGrid     quadGrid_;
      GridPartType quadGridPart_;
      double*      exactVals_;
      const double ceps_;


  }; // class SolutionArrayGenerator

    double SolutionArrayGenerator::calcCeps() {
      double numerator = 0.0;
      double denominator = 0.0;

      IteratorType end = quadGridPart_.end<0>();
      for (IteratorType it = quadGridPart_.begin<0>(); it!=end; ++it) {
        const EntityType& en = *it;
        const GeometryType& geometry = en.geometry();

        QuadratureType quadrature(en, 0);
        const unsigned int numQuadPoints = quadrature.nop();
        for (unsigned int qP=0; qP!=numQuadPoints; ++qP) {
          LocalCoordinateType quadPointLocal = quadrature.point(qP);
          GlobalCoordinateType quadPointGlobal = geometry.global(quadPointLocal);
          const double factor = 
            quadrature.weight(qP)*geometry.integrationElement(quadPointLocal);
          numerator += factor*(-quadPointGlobal/(a0(quadPointGlobal)*a1(quadPointGlobal/eps_)));
          denominator += factor*(1.0/(a0(quadPointGlobal)*a1(quadPointGlobal/eps_)));
        } 
      }
      return numerator/denominator;
    }

    void SolutionArrayGenerator::calcFuncValues() {
      double val=0.0;
      int i=0;
      IteratorType end = quadGridPart_.end<0>();
      for (IteratorType it = quadGridPart_.begin<0>(); it!=end; ++it, ++i) {
        const EntityType& en = *it;
        const GeometryType& geometry = en.geometry();

        QuadratureType quadrature(en, 0);
        const unsigned int numQuadPoints = quadrature.nop();
        for (unsigned int qP=0; qP!=numQuadPoints; ++qP) {
          LocalCoordinateType quadPointLocal = quadrature.point(qP);
          GlobalCoordinateType quadPointGlobal = geometry.global(quadPointLocal);
          const double factor = 
            quadrature.weight(qP)*geometry.integrationElement(quadPointLocal);
          val += factor*(quadPointGlobal/(a0(quadPointGlobal)*a1(quadPointGlobal/eps_)));
          val += ceps_ * factor*(1.0/(a0(quadPointGlobal)*a1(quadPointGlobal/eps_)));
        } 
        assert(i<quadGridSize_);
        exactVals_[i] = val;
      }
      return;
    }

    const double SolutionArrayGenerator::a0(const double& x) const {
      return (1.0+x);
    }

    const double SolutionArrayGenerator::a1(const double& x) const {
      double cosVal = cos(2.0*M_PI*x);
      return ((2.0/3.0)*(1+(cosVal*cosVal)));
    }

    void SolutionArrayGenerator::write(const std::string& filename, 
                                       const std::string& epsName) const {
      std::ofstream file(filename.c_str());
      file << "#ifndef __EXACTSOLVALS" << epsName << "__\n"
           << "#define __EXACTSOLVALS" << epsName << "__\n"
           << "const int exactSol" << epsName
           << "ValsSize = " << quadGridSize_
           << ";\n"
           << "const double exactSol" << epsName
           << "Ceps = ";
      file.precision(12);
      file.setf(std::ios::scientific);
      file << ceps_
           << ";\n"
           << "const double exactSol" << epsName 
           << "Vals[" << quadGridSize_ << "] = {";
      file << exactVals_[0];
      for (int i=1; i!= quadGridSize_; ++i) {
        file << ",";
        file.precision(12);
        file.setf(std::ios::scientific);
        file << exactVals_[i];
      }
      file << "};\n"
           << "#endif //#ifndef __EXACTSOLVALS" << epsName << "__";
      return;
    }


} // namespace RB 
} // namespace Dune

#endif /* __SOLUTIONARRAYGENERATOR_HH__ */
