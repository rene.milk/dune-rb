//
// newprobleminterface.h
// dune-rb
//
// Created by Sven Kaulmann on 10.01.12.
// Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef DUNE_RB_PROBLEMINTERFACE_HH
#define DUNE_RB_PROBLEMINTERFACE_HH


#include <dune/rb/rbasis/twophaseflow/highdim/seperableparametricfunction.hh>

/* The following are needed for the visualize* functions */
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/fem/operator/projection/l2projection.hh>

/* Definition of the CHECK_AND_CALL... macro*/
#include <dune/common/bartonnackmanifcheck.hh>

#include <Eigen/Core>

namespace Dune {
namespace RB {



template <int dimRange, class Implementation>
class ProblemInterface {

public:
    typedef FieldVector<double, GridSelector::dimgrid>
    DomainType;
    typedef FieldVector<double, dimRange>
    RangeType;
    typedef Eigen::VectorXd
    ParameterDomainType;
    typedef SeperableParametricFunctionInterface<DomainType, RangeType, ParameterDomainType>
    SeperableParametricFunctionType;
    enum BoundaryType {Dirichlet, Neumann};
    typedef FieldMatrix<double, DomainType::size, DomainType::size>
    DiffusionMatrixType;


    /** Evaluate the right hand side function in given point.
     *
     *  @param[in]   x   The point where the right hand side shall be evaluated
     *  @returns The evaluation of the right hand side
     */
    RangeType rightHandSide(const DomainType& x) const {
        return implementation()->rightHandSide(x);
    }

    /** Evaluate the diffusion matrix in given point.
     *
     *  This is a general template function. This implementation assumes the given
     *  result matrix to implement an operator(int i, int j) for element access.
     *  A version using Dune::FieldMatrix is also available as template specialization.
     *
     *  @param[in]  x The point where the permeability shall be evaluated in local coordinates.
     *  @param[in]  e The entity where x resides.
     *  @param[ret] k The permeability.
     *
     */
    template <class EntityType, class MatrixType>
    void permeability(const DomainType& x, const EntityType& e, const MatrixType& k) const {
        return implementation()->permeability(x, e, k);
    }

    /** Get the mobility as seperable parametric function.
     *
     * @returns Returns the mobility as class derived from the Dune::RB::SeperableParametricFunctionInterface.
     */
    SeperableParametricFunctionType& mobility() {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->mobility());
        return implementation()->mobility();
    }

    /** Get the mobility as seperable parametric function, constant version.
     *
     * @returns Returns the mobility as constant class derived from the Dune::RB::SeperableParametricFunctionInterface.
     */
    const SeperableParametricFunctionType& mobility() const {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->mobility());
        return implementation()->mobility();
    }

    /** Get the dirichlet boundary values as seperable parametric function
     *
     * @returns Returns the dirichlet boundary values as class derived from the
     *  Dune::RB::SeperableParametricFunctionInterface.
     */
    SeperableParametricFunctionType& dirichletValues() {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->dirichletValues());
        return implementation()->dirichletValues();
    }

    /** Get the dirichlet boundary values as seperable parametric function, constant version.
     *
     * @returns Returns the dirichlet boundary values as constant class derived from the
     *  Dune::RB::SeperableParametricFunctionInterface.
     */
    const SeperableParametricFunctionType& dirichletValues() const {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->dirichletValues());
        return implementation()->dirichletValues();
    }

    /** Get the neumann boundary values as seperable parametric function
     *
     * @returns Returns the neumann boundary values as class derived from the
     *  Dune::RB::SeperableParametricFunctionInterface.
     */
    SeperableParametricFunctionType& neumannValues() {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->neumannValues());
        return implementation()->neumannValues();
    }


    /** Get the neumann boundary values as seperable parametric function, constant version.
     *
     * @returns Returns the neumann boundary values as constant class derived from the
     *  Dune::RB::SeperableParametricFunctionInterface.
     */
    const SeperableParametricFunctionType& neumannValues() const {
        CHECK_INTERFACE_IMPLEMENTATION(implementation()->neumannValues());
        return implementation()->neumannValues();
    }

    /** Obtain the boundary type of a given boundary intersection.
     *
     *  @param[in] e Intersection whose boundary type shall be returned.
     *  @returns Returns the boundary type of the given intersection.
     */
    template <class IntersectionType>
    BoundaryType boundaryType (const IntersectionType &intersection) const {
        const int boundaryID = intersection.boundaryId();
        if (boundaryID==1 || boundaryID==5) {
            return Dirichlet;
        } else {
            return Neumann;
        }
        DUNE_THROW(NotImplemented, "The boundary ID you provided does not exist!");

    }

    /** Visualize the permeability field.
     *
     *  The function will plot the permeability as finite volume function @f$f:\mathbb{R}^n\rightarrow\mathbb{R}^n@f$
     *  (where n is the dimension of the grid). Every dimension in the range of @f$f@f$ represents one entry on the
     *  diagonal of the permeability.
     *  @note We assume the permeability to be constant in each cell of the given grid part.
     *
     *  @tparam GridPartType The type of the grid part.
     *  @param[in] gridPart  The grid part.
     */
    template < class GridImp >
    void visualizePermeability(GridImp& grid) {
      CHECK_INTERFACE_IMPLEMENTATION(implementation()->visualizePermeability());
      return implementation()->visualizePermeability(grid);
    } /* visualizePermeability */
    
    

    /** Visualize the mobility field.
     *
     *  The function will plot the mobility as finite volume function @f$f:\mathbb{R}^n\rightarrow\mathbb{R}@f$
     *  (where n is the dimension of the grid). 
     *  @note This will @f$L^2@f$-project the mobility to a constant value on each grid entity.
     *
     *  @tparam GridPartType The type of the grid part.
     *  @param[in] gridPart  The grid part.
     */
    template < class GridPartType >
    void visualizeMobility(GridPartType& gridPart) {
        
        typedef FunctionSpace<double, double, GridPartType::dimension, 1> FunctionSpaceType;
        typedef FiniteVolumeSpace<FunctionSpaceType, GridPartType, 0>                           FiniteVolumeSpaceType;
        typedef AdaptiveDiscreteFunction<FiniteVolumeSpaceType>                                 FiniteVolumeFunctionType;
        
        // prepare output function
        FiniteVolumeSpaceType fvSpace(gridPart);
        FiniteVolumeFunctionType mobilityFunction("mobility", fvSpace);
        mobilityFunction.clear();
        
        // project the mobility to the finite volume space
        L2Projection<double, double, SeperableParametricFunctionType, FiniteVolumeFunctionType> l2Projection;
        l2Projection(this->mobility(), mobilityFunction);
        
        // plot the mobility function
        plotFunction(mobilityFunction);
        
        return;
    } 
    
    
protected:
        /** Barton-Nackman trick*/
    Implementation* implementation() {
        return static_cast<Implementation*>(this);
    }
        /** Barton-Nackman trick*/
    const Implementation* implementation() const {
        return static_cast<const Implementation*>(this);
    }

};

} // namespace RB

} // namespace Dune

#endif // #ifndef DUNE_RB_PROBLEMINTERFACE_HH
