#ifndef _CONSTANTPROBLEM_H_
#define _CONSTANTPROBLEM_H_


  template< class GridImp, class FunctionSpaceImp >
  class ConstantProblem 
    : public QuadraticProblem< GridImp, FunctionSpaceImp >
  {
    typedef QuadraticProblem< GridImp, FunctionSpaceImp > BaseType;

  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit ConstantProblem()
      : sigma_(Parameter::getValue<double>("rb.sigma"))
    {}

    ~ConstantProblem() {}

    /** exact solution: return 0.5 x^2*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      ret[0] = 1.0;
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = 0.0;
      return;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      grad[0][0] = 0.0;
      grad[0][1] = 0.0;
      return;
    }
  private:
    double sigma_;
  };


#endif /* _CONSTANTPROBLEM_H_ */
