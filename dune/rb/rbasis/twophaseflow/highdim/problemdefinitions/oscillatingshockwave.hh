#ifndef _OSCILLATINGSHOCKWAVE_H_
#define _OSCILLATINGSHOCKWAVE_H_


  template< class GridImp, class FunctionSpaceImp>
  class OscillatingShockwave
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    typedef Shockwave<GridImp, FunctionSpaceImp> ThisType;
    typedef GridImp GridType;
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit OscillatingShockwave()
      : eps_(Parameter::getValue<double>("problem.eps")),
        u_(0, 10),
        muWater_(0.1),
        muOil_(0.9)
    {
      this->replaceParameter(Parameter::getValue<std::vector<double> >("model.muLambda"));
      Ns_ = this->muLambda_.size();
      Nlambda_= Ns_*Ns_+Ns_+1;
    }

    ~OscillatingShockwave() {}

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = 0.0;
    }

    void sigma(RangeType& ret) const {
      ret[0]=Parameter::getValue<double>("rb.sigma");
    }

    //! the exact solution 
    void u ( const DomainType &x, RangeType &ret ) const
    {
      assert(0);
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      assert(0);
    }
    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      // set @f$K=\diag((1+x)\cdot\frac{2}{3}(1+\cos^2(2\pi \frac{x}{\varepsilon}))@f$
      //      double val = (2.0*(1.0+x[0]))/(3.0);
      double val = (2.0)/(3.0)*(1.0+x[0]);
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      val *= 1.0+(cosVal*cosVal);
      m[0][0] = val;
      m[1][1] = val;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      double sinVal = sin(2.0*M_PI*x[0]/eps_);
      double val = 2.0/3.0;
      val += (2.0/3.0)*cosVal*cosVal;
      double foo = (8.0/3.0)*M_PI*(1+x[0])*cosVal*sinVal;
      foo /= eps_;
      val -= foo;
      m[0][0] = val+(2.0/3.0*(1+(cosVal*cosVal)));
      m[1][1] = val+(2.0/3.0*(1+(cosVal*cosVal)));
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
    }

    bool constantK () const
    {
      return false;
    }

    double l(const int i) const {
      assert(i<Nlambda_);
      assert(i>=0);
      double val;
      if (i==0)
        val = 1.0;
      else if (i<=Ns_) {
        val = this->muLambda_[i-1];
      } else {
        int m = (i-Ns_-1)%Ns_;
        int n = int(std::ceil(double(i-Ns_-1.0)/double(Ns_)))%Ns_;
        val = (this->muLambda_[m]*this->muLambda_[n]);
      }
      return val;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      int N = Nlambda();
      lm[0]=0.0;
      for (int i=0; i!=N; ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    // This is done for Omega=[0,10] !!!!
    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm=0.0;
      if (n==0) {
        lm[0]=1.0/(muOil_);
        return;
      }
      else if (n<=Ns_) {
        SPart(n-1, x, lm);
        lm[0]*= -2.0/muOil_;
      }
      else {
        const int i = (n-Ns_-1)%Ns_;
        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
        RangeType valI(0.0);
        RangeType valJ(0.0);
        SPart(i, x, valI);
        SPart(j, x, valJ);
        lm[0] = (muOil_+muWater_*muWater_)/(muWater_*muOil_)*valI[0]*valJ[0];
      }
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      if (n==0) {
        jac[0]=0.0;
        return;
      }
      else if (n<=Ns_) {
        SPartJacobian(n, x, jac);
        jac[0]*= -2.0/muOil_;
      }
      else {
        const int i = (n-Ns_-1)%Ns_;
        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
        JacobianRangeType jacI(0.0);
        JacobianRangeType jacJ(0.0);
        RangeType valI(0.0);
        RangeType valJ(0.0);
        SPart(i, x, valI);
        SPart(j, x, valJ);
        SPartJacobian(i, x, jacI);
        SPartJacobian(j, x, jacJ);
        jacI[0] *= valJ[0];
        jacJ[0] *= valI[0];
        jac[0] = jacI[0] + jacJ[0];
        jac[0] *= (muOil_+muWater_*muWater_)/(muWater_*muOil_);
      }
      return;
    }

  protected:
    // This is done for Omega=[0,10] !!!!
    void SPart(const int n, const DomainType &x, RangeType &lm) const {
      double gridXWidth = 10.0;
      double gridYWidth = 10.0;

      double foo = std::pow((double(2.0*(n+1))-Ns_*x[0]/(0.4*gridXWidth)),6);
      double val 
        = atan(-1.0*foo-10000.0/std::pow(gridYWidth,6)*std::pow((x[1]-0.5*gridYWidth),6))+(5.0/6.0)*M_PI;
      lm[0] = val*val*val;
      lm[0]*=0.1;
      if (GridType::dimension==3)
        lm[0]*=1-x[2];
      return;
    } 

    void SPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      assert(0);
      double gridXWidth = 10.0;
      double gridYWidth = 10.0;
      double term1 = double(2*(n+1))-double(Ns_)*x[0]*2.5/gridXWidth;
      double denominator = 1.0+std::pow((std::pow(term1,6)+10000.0*std::pow((x[1]-0.5*gridYWidth),6)/std::pow(gridYWidth,6)),2);
      jac[0][0]=5.0*Ns_*std::pow(term1,5)/(denominator*gridXWidth);
      jac[0][1]=-20000.0*std::pow((x[1]-0.5*gridYWidth),5)/(denominator*std::pow(gridYWidth,6));
      return;
    } 

  public:
    //! the n'th parameter-independent part of the boundary values
    virtual void gPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      ret[0]=11.0-1.0*x[0];
      return;
    }
    
    
    //! the number of parts of the dirichlet values
    virtual unsigned int Ngd() const { 
      return Nlambda_;
    };

    //! the i'th parameter for the boundary values
    virtual double mugd(const unsigned int nu) const {
      return l(nu);
    }

  private:
      double		eps_;
      OscillatingCoefficient	u_;
      const double	muWater_;
      const double	muOil_;
      int		Ns_;
      int		Nlambda_;


  };



#endif /* _OSCILLATINGSHOCKWAVE_H_ */
