#ifndef _OSCILLATINGPROBLEM_H_
#define _OSCILLATINGPROBLEM_H_


  template< class GridImp >
  class OscillatingProblem 
  : public ProblemInterface< typename OscillatingCoefficient::FunctionSpaceType >
  {
    typedef ProblemInterface< typename OscillatingCoefficient::FunctionSpaceType > BaseType;

  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit OscillatingProblem() 
      : sigma_(Parameter::getValue<double>("rb.sigma")),
        eps_(Parameter::getValue<double>("problem.eps")),
        u_(0, 1)
    {}

    ~OscillatingProblem() {}

    void u( const DomainType &x, RangeType &ret ) const
    {
      u_.evaluate(x, ret);
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1;
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      u(x, ret);
    }

    void sigma(RangeType& ret) const {
      ret[0]=sigma_;
    }

    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      // set @f$K=\diag((1+x)\cdot\frac{2}{3}(1+\cos^2(2\pi \frac{x}{\varepsilon}))@f$
      double val = (2.0*(1.0+x[0]))/(3.0);
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      val *= 1.0+(cosVal*cosVal);
      m[0][0] = val;
      m[1][1] = val;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      double sinVal = sin(2.0*M_PI*x[0]/eps_);
      double val = 2.0/3.0;
      val += (2.0/3.0)*cosVal*cosVal;
      double foo = (8.0/3.0)*M_PI*(1+x[0])*cosVal*sinVal;
      foo /= eps_;
      val -= foo;
      m[0][0] = val;
      m[1][1] = val;
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
    }

    bool constantK () const
    {
      return false;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      u_.jacobian(x,grad);
      return;
    }

  private:
    double  sigma_;
    double  eps_;
    OscillatingCoefficient u_;
  };


#endif /* _OSCILLATINGPROBLEM_H_ */
