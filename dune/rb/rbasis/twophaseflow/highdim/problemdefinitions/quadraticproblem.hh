#ifndef _QUADRATICPROBLEM_H_
#define _QUADRATICPROBLEM_H_


  template< class GridImp, class FunctionSpaceImp >
  class QuadraticProblem 
  : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;

  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit QuadraticProblem()
      : sigma_(Parameter::getValue<double>("rb.sigma"))
    {}

    ~QuadraticProblem() {}

    /** exact solution: return 0.5 x^2*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      ret[0] = 0.5*x[0]*x[0];
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1;
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      u(x, ret);
    }

    double sigma() const {
      return sigma_;
    }

    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      m[0][0] = 1.0;
      m[1][1] = 1.0;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
    }


    bool constantK () const
    {
      return true;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      grad[0][0] = x[0];
      grad[0][1] = 0.0;
      return;
    }

    double l(const int i) const {
      return 1.0;
    }

    int Nlambda() const {
      return 1;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      lm[0] = 1.0;
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm[0]=1.0;
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      jac=0;
      return;
    } 

  private:
    double sigma_;
  };


#endif /* _QUADRATICPROBLEM_H_ */
