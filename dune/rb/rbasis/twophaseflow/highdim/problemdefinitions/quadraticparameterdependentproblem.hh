#ifndef _QUADRATICPARAMETERDEPENDENT_H_
#define _QUADRATICPARAMETERDEPENDENT_H_

  template< class GridImp, class FunctionSpaceImp>
  class QuadraticParameterDependentProblem 
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit QuadraticParameterDependentProblem()
      : sigma_(Parameter::getValue<double>("rb.sigma")),
        Nlambda_(Parameter::getValue<int>("test.num_params"))
    {}

    ~QuadraticParameterDependentProblem() {}

    /** exact solution: return 0.5 x^2*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      DUNE_THROW(NotImplemented, 
                 "Exact solution is not implemented in this model!");
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1;
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      ret[0]=0.0;
      return;
    }

    void sigma(RangeType& ret) const {
      ret[0]=sigma_;
    }

    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      m[0][0] = 1.0;
      m[1][1] = 1.0;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      return;
    }


    bool constantK () const
    {
      return true;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      DUNE_THROW(NotImplemented, 
                 "Exact solution is not implemented in this model!");
      return;
    }

    double l(const int i) const {
      double param=0.0;
      std::stringstream sstream;
      sstream << "test.param" << i;
      Parameter::get(sstream.str(), param);
      return param;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      lm[0]=0.0;
      for (int i=0; i!=Nlambda_; ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm[0]=atan(-60*x[0]+(n*15))+0.5*M_PI;
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      double denominator = 225*(n-4*x[0])*(n-4*x[0]);
      denominator += 1.0;
      jac[0] = -60.0/denominator;
      jac[1] = 0.0;
      return;
    } 
  private:
    double    sigma_;
    const int Nlambda_;
  };


#endif /* _QUADRATICPARAMETERDEPENDENT_H_ */
