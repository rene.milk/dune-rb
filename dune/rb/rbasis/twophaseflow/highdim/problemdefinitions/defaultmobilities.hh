//
// defaultmobilities.hh
// dune-rb
//
// Created by Sven Kaulmann on 09.05.12.
// Copyright 2012 Universität Stuttgart. All rights reserved.
//
#ifndef __DEFAULTMOBILITIES_HH__
#define __DEFAULTMOBILITIES_HH__

namespace Dune {
namespace RB {
template< class DomainType, class RangeType, class ParameterDomainType >
class IdentityMobilityFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;

  IdentityMobilityFunction()
  {
    setParameter(ParameterDomainType(1.0));
  }

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    return 1.0;
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType lm(1.0);

    return lm;
  }
};
}
}


#endif // #ifndef __DEFAULTMOBILITIES_HH__