//
// groundwaterflow.h
// dune-rb
//
// Created by Sven Kaulmann on 30.11.11.
// Copyright 2011. All rights reserved.
//

#ifndef __GROUNDWATERFLOW_H__
#define __GROUNDWATERFLOW_H__

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/probleminterface.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/seperableparametricfunction.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/defaultmobilities.hh>

namespace Dune {
namespace RB {

template< class DomainType, class RangeType, class ParameterDomainType >
class GroundwaterflowMobilityFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;

  GroundwaterflowMobilityFunction(double dx, double dy, double dz,
                                  int nx, int ny, int nz)
    : x0_(0.0),
      epsilon_(0.0),
      alphaVec_(0.0)
  {
    setParameter(Parameter::getValue< ParameterDomainType >("model.muLambda"));
    assert(DomainType::size == 3);

    // x0_ is the point in the domain from which the distance for lambda is measured
    x0_[2]   = nz * dz;
    epsilon_ = (ny * dy * 0.75 / size());
    DomainType endOfDomain(0.0);
    endOfDomain[0] = nx * dx;
    endOfDomain[1] = ny * dy;
    endOfDomain   -= x0_;
    const double diagonalLengthOfDomain = endOfDomain.two_norm();
    for (int i = 0; i != size(); ++i) {
      alphaVec_.push_back(diagonalLengthOfDomain * i / (size()));
    }
  }

  const int size() const
  {
    return this->mu_.rows();
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    assert(i < size() && i >= 0);
    assert(size() == mu.rows());

    return mu[i];
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType  lm(0.0);
    DomainType difference = x;

    difference -= x0_;
    double distanceToX0 = difference.two_norm();
    if ((distanceToX0 >= alphaVec_[i] - epsilon_) && (distanceToX0 <= alphaVec_[i] + epsilon_)) {
      double sinVal = sin(0.5 * M_PI * (distanceToX0 + epsilon_ - alphaVec_[i]) / epsilon_);
      lm[0] = 1 - sinVal * sinVal;
    } else {
      lm[0] = 1.0;
    }
    return lm;
  }   /* component */

private:
  DomainType            x0_;
  double                epsilon_;
  std::vector< double > alphaVec_;
};

template< class DomainType, class RangeType, class ParameterDomainType >
class GroundwaterflowDirichletValueFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  GroundwaterflowDirichletValueFunction()
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    return 1.0;
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    return RangeType(0.0);
  }   /* component */
};

template< class DomainType, class RangeType, class ParameterDomainType >
class GroundwaterflowNeumannValueFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  GroundwaterflowNeumannValueFunction()
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    return 1.0;
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType ret(0.0);

    if (abs(x[1]) < 1e-6)
      ret[0] = 1;
    return ret;
  } // component
};

template< int dimRange >
class Groundwaterflow
  : public ProblemInterface< dimRange, Groundwaterflow< dimRange > >
{
  typedef ProblemInterface< dimRange, Groundwaterflow< dimRange > > BaseType;

  // this is needed in order to use the const version of those functions from the interface
  using BaseType::mobility;
  using BaseType::dirichletValues;
  using BaseType::neumannValues;
  using BaseType::BoundaryType;
  // using BaseType::implementation;

public:
  typedef FieldVector< double, GRIDDIM >  DomainType;
  typedef FieldVector< double, dimRange >  RangeType;
  typedef typename BaseType::ParameterDomainType  ParameterDomainType;
  typedef FieldMatrix< double, DomainType::size, DomainType::size >  DiffusionMatrixType;

  
  typedef GroundwaterflowMobilityFunction<DomainType, RangeType, ParameterDomainType>
  GroundwaterflowMobilityFunctionType;
  typedef GroundwaterflowDirichletValueFunction< DomainType, RangeType, ParameterDomainType >
  GroundwaterflowDirichletValueFunctionType;
  typedef GroundwaterflowNeumannValueFunction< DomainType, RangeType, ParameterDomainType >
  GroundwaterflowNeumannValueFunctionType;

  Groundwaterflow()
    : dx_(6.096),
      dy_(3.048),
      dz_(0.6096),
  mobilityFunction_(dx_, dy_, dz_, Parameter::getValue<int>("model.nx"), Parameter::getValue<int>("model.ny"),
                    Parameter::getValue<int>("model.nz")),
      obstacleValue_(Parameter::getValue< double >("model.obstacle", -1.0))
  {
    readPermeability();
  }

  ~Groundwaterflow() {
    delete[] permeability_;
  }

  RangeType rightHandSide(const DomainType& x) const
  {
    return RangeType(0.0);
  }

  template< class EntityType, class MatrixType >
  void permeability(const DomainType& x, const EntityType& e, MatrixType& k) const
  {
    k = MatrixType::Zero(3, 3);

    // get the barycenter
    DomainType local
      = Dune::GenericReferenceElements< typename EntityType::ctype, GRIDDIM >::general(e.type()).position(0, 0);
    DomainType global = e.geometry().global(local);

    int xIntervall = std::floor(global[0] / dx_);
    int yIntervall = std::floor(global[1] / dy_);
    int zIntervall = std::floor(global[2] / dz_);

    const int offset = xIntervall + yIntervall * 60 + zIntervall * 220 * 60;
    if ((obstacleValue_ > 0) && (abs(global[1] - 300) < 4) && (global[0] < 150)) {
      k(0, 0) = obstacleValue_;
      k(1, 1) = obstacleValue_;
      k(2, 2) = obstacleValue_;
    } else {
      k(0, 0) = permeability_[offset];
      k(1, 1) = permeability_[offset+1122000];
      k(2, 2) = permeability_[offset+2244000];
    }
    return;
  }   /* permeability */

  template< class EntityType >
  void permeability(const DomainType& x, const EntityType& e, DiffusionMatrixType& k) const
  {
    k = 0.0;

    // get the barycenter
    DomainType local
      = Dune::GenericReferenceElements< typename EntityType::ctype, GRIDDIM >::general(e.type()).position(0, 0);
    DomainType global = e.geometry().global(local);

    int xIntervall = std::floor(global[0] / dx_);
    int yIntervall = std::floor(global[1] / dy_);
    int zIntervall = std::floor(global[2] / dz_);

    const int offset = xIntervall + yIntervall * 60 + zIntervall * 220 * 60;
    if ((obstacleValue_ > 0) && (abs(global[1] - 300) < 4) && (global[0] < 150)) {
      k[0][0] = obstacleValue_;
      k[1][1] = obstacleValue_;
      k[2][2] = obstacleValue_;
    } else {
      k[0][0] = permeability_[offset];
      k[1][1] = permeability_[offset+1122000];
      k[2][2] = permeability_[offset+2244000];
    }
    return;
  }   /* permeability */


  GroundwaterflowMobilityFunctionType& mobility()
  {
    return mobilityFunction_;
  }

  const GroundwaterflowMobilityFunctionType& mobility() const
  {
    return mobilityFunction_;
  }

  GroundwaterflowDirichletValueFunctionType& dirichletValues()
  {
    return dirichletValueFunction_;
  }

  const GroundwaterflowDirichletValueFunctionType& dirichletValues() const
  {
    return dirichletValueFunction_;
  }

  GroundwaterflowNeumannValueFunctionType& neumannValues()
  {
    return neumannValueFunction_;
  }

  const GroundwaterflowNeumannValueFunctionType& neumannValues() const
  {
    return neumannValueFunction_;
  }
  
  template < class GridImp >
  void visualizePermeability(GridImp& grid) {
    typedef GridImp                                                 GridType;
    typedef typename GridType::LeafGridView::template Codim<0>::Iterator IteratorType;
    typedef typename GridType::LeafGridView::template Codim<0>::Entity   EntityType;
    typedef FieldVector<double, GridType::dimension> DomainType;
     
    Eigen::VectorXd perm_x(grid.size(0));
    Eigen::VectorXd perm_y(grid.size(0));
    Eigen::VectorXd perm_z(grid.size(0));
    
    const IteratorType gridEnd = grid.leafView().template end<0>();
    for (IteratorType it = grid.leafView().template begin<0>(); it != gridEnd; ++it) {
      const EntityType& entity = *it;
      // get the barycenter
      DomainType local = Dune::GenericReferenceElements<typename GridType::ctype, GridType::dimension>::general(entity.type()).position(0,0);
      
      Eigen::MatrixXd permeabilityMatrix;
      permeability(local, entity, permeabilityMatrix);
      int index = grid.leafView().indexSet().index(entity);
      perm_x(index) = permeabilityMatrix(0, 0);
      perm_y(index) = permeabilityMatrix(1, 1);
      perm_z(index) = permeabilityMatrix(2, 2);
    }
    
    VTKWriter<typename GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.addCellData(perm_x, "perm_x");
    vtkwriter.addCellData(perm_y, "perm_y");
    vtkwriter.addCellData(perm_z, "perm_z");
    vtkwriter.write("vis/Permeability");
    
    
    return;
  }

  
private:
  /** Read the SPE10 permeability field from file.
   *
   *  @remark Data is supposed to be in file
   *  "../../../highdim/problemdefinitions/groundwaterflow_perm.dat".
   */
  void readPermeability()
  {
    permeability_ = new double[3366000];
    std::string filename
      = Parameter::getValue< std::string >("model.permeabilityFile",
                                           std::string("../../../highdim/problemdefinitions/groundwaterflow_perm.dat"));
    std::ifstream file(filename.c_str());
    double        val;
    if (!file)                        // file couldn't be opened
      DUNE_THROW(IOError, "Data file for Groundwaterflow permeability could not be opened!");
    file >> val;
    int counter = 0;
    while (!file.eof()) {
      // keep reading until end-of-file
      permeability_[counter++] = val;
      file >> val; // sets EOF flag if no value found
    }
    file.close();
    return;
  }   /* readPermeability */

  const double dx_;
  const double dy_;
  const double dz_;
  const double obstacleValue_;

  GroundwaterflowMobilityFunctionType       mobilityFunction_;
  GroundwaterflowDirichletValueFunctionType dirichletValueFunction_;
  GroundwaterflowNeumannValueFunctionType   neumannValueFunction_;

  double* permeability_;
};
} // namespace RB
} // namespace Dune

#endif /* ifndef __GROUNDWATERFLOW_H__ */