#ifndef _SHOCKWAVE_H_
#define _SHOCKWAVE_H_



  template< class GridImp, class FunctionSpaceImp>
  class Shockwave
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    typedef Shockwave<GridImp, FunctionSpaceImp> ThisType;
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit Shockwave()
      : muWater_(0.5),
        muOil_(0.5)
    {
      this->replaceParameter(Parameter::getValue<std::vector<double> >("model.muLambda"));
      Ns_ = this->muLambda_.size();
      Nlambda_= Ns_;//Ns_*Ns_+Ns_+1;
    }

    ~Shockwave() {}

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1.0;
    }

    //! the exact solution 
    void u ( const DomainType &x, RangeType &ret ) const
    {
      assert(0);
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      assert(0);
    }

    double l(const int i) const {
      assert(i<Nlambda_);
      assert(i>=0);
      double val;
//      if (i==0)
//        val = 1.0;
//      else if (i<=Ns_) {
        val = this->muLambda_[i];
        //val = this->muLambda_[i-1];
        //      } else {
//        int m = (i-Ns_-1)%Ns_;
//        int n = int(std::ceil(double(i-Ns_-1.0)/double(Ns_)))%Ns_;
//        val = (this->muLambda_[m]*this->muLambda_[n]);
//      }
      return val;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      int N = Nlambda();
      lm[0]=0.0;
      for (int i=0; i!=N; ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    // This is done for Omega=[0,10] !!!!
    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm=0.0;
      
//      if (n==0) {
//        lm[0]=1.0/muOil_;
//      }
//      else if (n<=Ns_) {
//        SPart(n-1, x, lm);
//        lm[0]*= -2.0/muOil_;
//      }
//      else {
//        const int i = (n-Ns_-1)%Ns_;
//        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
//        RangeType valI(0.0);
//        RangeType valJ(0.0);
//        SPart(i, x, valI);
//        SPart(j, x, valJ);
//        lm[0] = (muOil_+muWater_*muWater_)/(muWater_*muOil_)*valI[0]*valJ[0];
//      }
      
      // SPart(n, x, lm);
      // lm[0]*= -2.0/muOil_;
      lm[0]=1.0;
      
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      
      assert(0);
      
      if (n==0) {
        jac[0]=0.0;
        return;
      }
      else if (n<=Ns_) {
        SPartJacobian(n, x, jac);
        jac[0]*= -2.0/muOil_;
      }
      else {
        const int i = (n-Ns_-1)%Ns_;
        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
        JacobianRangeType jacI(0.0);
        JacobianRangeType jacJ(0.0);
        RangeType valI(0.0);
        RangeType valJ(0.0);
        SPart(i, x, valI);
        SPart(j, x, valJ);
        SPartJacobian(i, x, jacI);
        SPartJacobian(j, x, jacJ);
        jacI[0] *= valJ[0];
        jacJ[0] *= valI[0];
        jac[0] = jacI[0] + jacJ[0];
        jac[0] *= (muOil_+muWater_*muWater_)/(muWater_*muOil_);
      }
      return;
    }

  protected:
    // This is done for Omega=[0,10] !!!!
    void SPart(const int n, const DomainType &x, RangeType &lm) const {
      double gridXWidth = 10.0;
      double gridYWidth = 10.0;

      double foo = std::pow((double(2.0*(n+1))-Ns_*x[0]/(0.4*gridXWidth)),6);
      lm[0] = (1.0/3.0)*atan(-1.0*foo-10000.0/std::pow(gridYWidth,6)*std::pow((x[1]-0.5*gridYWidth),6))+(M_PI/2.0);
      return;
    } 

    void SPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      double gridXWidth = 10.0;
      double gridYWidth = 10.0;
      double term1 = double(2*(n+1))-double(Ns_)*x[0]*2.5/gridXWidth;
      double denominator = 1.0+std::pow((std::pow(term1,6)+10000.0*std::pow((x[1]-0.5*gridYWidth),6)/std::pow(gridYWidth,6)),2);
      jac[0][0]=5.0*Ns_*std::pow(term1,5)/(denominator*gridXWidth);
      jac[0][1]=-20000.0*std::pow((x[1]-0.5*gridYWidth),5)/(denominator*std::pow(gridYWidth,6));
      return;
    } 

  public:
    //! the n'th parameter-independent part of the boundary values
    virtual void gDPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      ret[0]= 0.0;//0.5*x[0];
      return;
    }
    
    
    //! the number of parts of the dirichlet values
    virtual unsigned int Ngd() const { 
      return 1;
    };

    //! the i'th parameter for the boundary values
    virtual double mugd(const unsigned int nu) const {
      return 1.0;
    }

      //! the n'th parameter-independent part of the boundary values
    void gNPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      ret[0] = 0.0;   
      return;
    }

      //! the number of parts of the dirichlet values
    virtual unsigned int Ngn() const { 
      return 1;
    };
    
      //! the i'th parameter for the neumann boundary values
    virtual double mugn(const unsigned int nu) const {
      return 1.0;
    }
    
    
  private:
    const double muWater_;
    const double muOil_;
    int          Ns_;
    int          Nlambda_;

  };


#endif /* _SHOCKWAVE_H_ */
