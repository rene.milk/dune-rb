//
// johanson.h
// dune-rb
//
// Created by Sven Kaulmann on 30.11.11.
// Copyright 2011. All rights reserved.
//

#ifndef __JOHANSON_HH__
#define __JOHANSON_HH__

// include own parameter class (copy from fem)
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/probleminterface.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/seperableparametricfunction.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/defaultmobilities.hh>

namespace Dune {
namespace RB {
template< class DomainType, class RangeType, class ParameterDomainType >
class JohansonDirichletValueFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  JohansonDirichletValueFunction()
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    return 1.0;
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    return RangeType(200.0);
  }   /* component */
};

template< class DomainType, class RangeType, class ParameterDomainType >
class JohansonNeumannValueFunction
  : public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
{
  typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
  BaseType;

public:
  /* the following is needed if we want to use the BaseType::coefficient(int i) method
   * (is shadowed otherwise)*/
  using BaseType::coefficient;
  using BaseType::component;


  JohansonNeumannValueFunction()
  {}

  const int size() const
  {
    return 1;
  }

  const double coefficient(const int i, const ParameterDomainType& mu) const
  {
    return 1.0;
  }


  const RangeType component(const int i, const DomainType& x) const
  {
    RangeType ret(1.0);

    return ret;
  } // component
};

template< class GridImp, int dimRange >
class Johanson
  : public ProblemInterface< dimRange, Johanson< GridImp, dimRange > >
{
  typedef ProblemInterface< dimRange, Johanson< GridImp, dimRange > > BaseType;
  typedef GridImp                                                     GridType;
  // this is needed in order to use the const version of those functions from the interface
  using BaseType::mobility;
  using BaseType::dirichletValues;
  using BaseType::neumannValues;
  using BaseType::BoundaryType;
  // using BaseType::implementation;

public:
  typedef FieldVector< double, GRIDDIM >                            DomainType;
  typedef FieldVector< double, dimRange >                           RangeType;
  typedef typename BaseType::ParameterDomainType                    ParameterDomainType;
  typedef FieldMatrix< double, DomainType::size, DomainType::size > DiffusionMatrixType;

  typedef IdentityMobilityFunction< DomainType, RangeType,
                                    ParameterDomainType > MobilityFunctionType;
  typedef JohansonDirichletValueFunction< DomainType, RangeType,
                                          ParameterDomainType > DirichletValueFunctionType;
  typedef JohansonNeumannValueFunction< DomainType, RangeType,
                                        ParameterDomainType > NeumannValueFunctionType;

  Johanson(const GridType& grid)
    : grid_(grid)
  {
    readPermeability();
  }


  RangeType rightHandSide(const DomainType& x) const
  {
    return RangeType(0.0);
  }

  template< class EntityType, class MatrixType >
  void permeability(const DomainType& x, const EntityType& e, MatrixType& k) const
  {
    assert(k.rows() == 3 && k.cols() == 3);

    const int index = grid_.leafIndexSet().index(e);

    double treshold = 0.001;
    k       = MatrixType::Zero(3, 3);
    k(0, 0) = treshold + permeability_(index, 0);
    k(1, 1) = treshold + permeability_(index, 1);
    k(2, 2) = treshold + permeability_(index, 2);

    return;
  }   /* permeability */

  template< class EntityType >
  void permeability(const DomainType& x, const EntityType& e, DiffusionMatrixType& k) const
  {
    const int index = grid_.leafIndexSet().index(e);

    double treshold = 0.001;
    k       = 0.0;
    k[0][0] = treshold + permeability_(index, 0);
    k[1][1] = treshold + permeability_(index, 1);
    k[2][2] = treshold + permeability_(index, 2);

    return;
  }   /* permeability */


  MobilityFunctionType& mobility()
  {
    return mobilityFunction_;
  }

  const MobilityFunctionType& mobility() const
  {
    return mobilityFunction_;
  }

  DirichletValueFunctionType& dirichletValues()
  {
    return dirichletValueFunction_;
  }

  const DirichletValueFunctionType& dirichletValues() const
  {
    return dirichletValueFunction_;
  }

  NeumannValueFunctionType& neumannValues()
  {
    return neumannValueFunction_;
  }

  const NeumannValueFunctionType& neumannValues() const
  {
    return neumannValueFunction_;
  }
  
  template < class MyGridImp >
  void visualizePermeability(MyGridImp& grid) {  
    typedef GridImp GridType;
    VTKWriter<typename GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.addCellData(permeability_.col(0), "perm_x");
    vtkwriter.addCellData(permeability_.col(1), "perm_y");
    vtkwriter.addCellData(permeability_.col(2), "perm_z");
    vtkwriter.write("vis/Permeability");
    
    return;
  }

protected:
  /** Read the Johanson permeability field from file.
   *
   *  @remark Data is supposed to be in file
   *  "../../../highdim/problemdefinitions/johanson_perm.dat".
   */
  void readPermeability()
  {
    double treshold = Parameter::getValue<double>("model.permeabilityTreshold", 1e-4);
    
    Eigen::VectorXd permtemp(450576);

    std::string permfilename
      = Parameter::getValue< std::string >("model.permeabilityFile",
                                           std::string("../../../highdim/problemdefinitions/johanson_perm.dat"));
    std::ifstream permfile(permfilename.c_str());

    
    double val;

    if (!permfile)
      DUNE_THROW(IOError, "Data file for Johanson permeability could not be opened!");

    permfile >> val; 
    int counter = 0;
    
    while (!permfile.eof()) {
      // keep reading until end-of-file
      permtemp(counter) = std::max(val, treshold);
      ++counter;
      permfile >> val;                           // sets EOF flag if no value found
    }
    permfile.close();

    
    // Now get the permeability for the non-fault cells
    
    permeability_ = Eigen::MatrixXd(244521, 3);
    for (int i=0; i!=grid_.globalCell().size(); ++i) {
      int index = grid_.globalCell()[i];
      permeability_(i, 0) = permtemp(index);
    }
    permeability_.col(1)=permeability_.col(0);
    permeability_.col(2)=0.1*permeability_.col(0);
    
    return;
  }   /* readPermeability */

  const GridType& grid_;

  MobilityFunctionType       mobilityFunction_;
  DirichletValueFunctionType dirichletValueFunction_;
  NeumannValueFunctionType   neumannValueFunction_;

  Eigen::MatrixXd permeability_;
};
} // namespace RB
} // namespace Dune

#endif /* ifndef __JOHANSON_HH__ */