#ifndef _LINEARILYPARAMTERDEPENDENTPROBLEM_H_
#define _LINEARILYPARAMTERDEPENDENTPROBLEM_H_


 template< class GridImp, class FunctionSpaceImp>
  class LinearilyParamterDependentProblem 
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit LinearilyParamterDependentProblem()
      : sigma_(Parameter::getValue<double>("rb.sigma")),
        Nlambda_(Parameter::getValue<int>("test.num_params"))
    {}

    ~LinearilyParamterDependentProblem() {}

    /** exact solution*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      ret[0]=10*(2*x[0]*x[0]-2*log(x[0]*x[0]+1))/(l(0));
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -10*12*x[0]*x[0];
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      ret=0.0;
      for (int i=0; i!=int(Ngd()); ++i) {
        RangeType val(0.0);
        gPart(i, x, val);
        val*=mugd(i);
        ret[0]+=val;      
      }
      return;
    }

    unsigned int Ngd() const {
      return 1;
    }

    double mugd(const unsigned int nu) const {
      return 1.0/l(nu);
    }

    void gPart(const unsigned int nu, const DomainType& x, 
               RangeType& ret) const {
      ret[0]=10*(2*x[0]*x[0]-2*log(x[0]*x[0]+1));
      return;
    }
    

    void sigma(RangeType& ret) const {
      ret[0]=sigma_;
    }



    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      DUNE_THROW(NotImplemented, 
                 "Exact solution is not implemented in this model!");
      return;
    }

    double l(const int i) const {
      double param=0.0;
      std::stringstream sstream;
      sstream << "test.param" << i;
      Parameter::get(sstream.str(), param);
      return param;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      lm[0]=0.0;
      for (int i=0; i!=int(Nlambda_); ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm[0] = 1+x[0]*x[0];
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      jac[0]=2*x[0];
      jac[1]=0.0;
      return;
    } 
  private:
    double             sigma_;
    const unsigned int Nlambda_;
  };


#endif /* _LINEARILYPARAMTERDEPENDENTPROBLEM_H_ */
