#ifndef _POLYNOMIALPARAMETERDEPENDENTPROBLEM_H_
#define _POLYNOMIALPARAMETERDEPENDENTPROBLEM_H_


 template< class GridImp, class FunctionSpaceImp>
  class PolynomialParameterDependentProblem 
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit PolynomialParameterDependentProblem()
      : sigma_(Parameter::getValue<double>("rb.sigma")),
        Nlambda_(Parameter::getValue<int>("test.num_params"))
    {}

    ~PolynomialParameterDependentProblem() {}

    /** exact solution: return 0.5 x^2*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      DUNE_THROW(NotImplemented, 
                 "Exact solution is not implemented in this model!");
      return;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      DUNE_THROW(NotImplemented, 
                 "Exact solution is not implemented in this model!");
      return;
    }


    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1;
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      ret[0]=0.0;
      return;
    }

    void sigma(RangeType& ret) const {
      ret[0]=sigma_;
    }

    double l(const int i) const {
      double param=0.0;
      std::stringstream sstream;
      sstream << "test.param" << i;
      Parameter::get(sstream.str(), param);
      return param;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      lm[0]=0.0;
      for (int i=0; i!=int(Nlambda_); ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm[0]= std::pow(x[0],n);
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      if (n>1) {
        jac[0][0] = std::pow(x[0],n-1);
        jac[0][0] *= double(n);
      }
      else
        jac[0][0] = 0.0;
      jac[0][1] = 0.0;
      return;
    } 

    unsigned int Ngd() const {
      return 1;
    }

    double mugd(const unsigned int nu) const {
      return 1.0;
    }

    void gPart(const unsigned int nu, const DomainType& x, 
               RangeType& ret) const {
      ret[0]=0.0;
      return;
    }


  private:
    double             sigma_;
    const unsigned int Nlambda_;
  };



#endif /* _POLYNOMIALPARAMETERDEPENDENTPROBLEM_H_ */
