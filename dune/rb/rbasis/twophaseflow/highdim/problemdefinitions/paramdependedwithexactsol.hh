#ifndef _PARAMDEPENDEDWITHEXACTSOL_H_
#define _PARAMDEPENDEDWITHEXACTSOL_H_


  template< class GridImp, class FunctionSpaceImp>
  class ParamDependedWithExactSol 
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit ParamDependedWithExactSol()
      : sigma_(Parameter::getValue<double>("rb.sigma")),
        Nlambda_(2)
    {}

    ~ParamDependedWithExactSol() {}

    /** exact solution: return 0.5 x^2*/
    void u ( const DomainType &x, RangeType &ret ) const
    {
      const double a = l(0); 
      const double b = l(1); 
      const double c = l(2); 
      ret[0]=a*cos(x[0])*cos(x[0])+b*cos(2*M_PI*x[0])+c*sin(2*M_PI*x[0])+(a+b+c)*M_PI+2*M_PI*x[0];
      return;
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      const double a = l(0); 
      const double b = l(1); 
      const double c = l(2); 
      
      grad[0][0] = -2*a*sin(x[0])*cos(x[0])-2*b*M_PI*sin(2*M_PI*x[0])+2*M_PI*c*cos(2*M_PI*x[0])+2*M_PI;
      grad[0][1]=0.0;
      return;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -1.0;
      return;
    }

    //! the Dirichlet boundary data function
    void g(const DomainType& x, RangeType& ret) const {
      u(x, ret);
      return;
    }

    void sigma(RangeType& ret) const {
      ret[0]=sigma_;
    }

    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      m[0][0] = 1.0;
      m[1][1] = 1.0;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      return;
    }


    bool constantK () const
    {
      return true;
    }

    double l(const int i) const {
      double param=0.0;
      std::stringstream sstream;
      sstream << "test.param" << i;
      Parameter::get(sstream.str(), param);
      return param;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      lm[0]=0.0;
      for (int i=0; i!=int(Nlambda_); ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      if (n==0)
        lm[0]=x[0]*x[0];
      else if (n==1)
        lm[0]=-x[0]*x[0]*x[0];
      else
        DUNE_THROW(NotImplemented, "This part of lambda does not exist!");
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      if (n==0)
        jac[0][0]=2*x[0];
      else if (n==1)
        jac[0][0]=-3*x[0]*x[0];
      else
        DUNE_THROW(NotImplemented, "This part of lambda does not exist!");
      jac[0][1]=0.0;
      return;
    } 
  private:
    double             sigma_;
    const unsigned int Nlambda_;
  };


#endif /* _PARAMDEPENDEDWITHEXACTSOL_H_ */
