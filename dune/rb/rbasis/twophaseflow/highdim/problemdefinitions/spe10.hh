#ifndef _SPE10_H_
#define _SPE10_H_

template< class GridImp, class FunctionSpaceImp>
  class SPE10
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    typedef SPE10<GridImp, FunctionSpaceImp> ThisType;
    typedef GridImp GridType;
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;
    typedef typename BaseType::BoundaryType BoundaryType;

    explicit SPE10()
    : muWater_(0.01),
      muOil_(1.0)
    {
      this->replaceParameter(Parameter::getValue<std::vector<double> >("model.muLambda"));
      Ns_ = this->muLambda_.size();
      Nlambda_= Ns_*Ns_+Ns_+1;
      readPermeability();
    }

    ~SPE10() {
      delete[] permeability_;
    }

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = 1.0;
    }

    //! the exact solution 
    void u ( const DomainType &x, RangeType &ret ) const {
      assert(0);
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const {
      assert(0);
    }
    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      if (DomainType::size!=3)
        DUNE_THROW(InvalidStateException, "SPE10 test case was implemented for dim=3!");
      m=0;
      int xIntervall = std::floor(x[0]/7.62);
      int zIntervall = std::floor(x[2]/0.762);
      m[0][0] = permeability_[xIntervall*20+zIntervall];
      m[1][1] = permeability_[xIntervall*20+zIntervall+2000];
      m[2][2] = permeability_[xIntervall*20+zIntervall+4000];

//      m[0][0] = 1.0;
//      m[1][1] = 1.0;
//      m[2][2] = 1.0;
      
      
      return;
    }
    
    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      DUNE_THROW(NotImplemented, "x derivative for permeability is not implemented for SPE10 test case!");
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      DUNE_THROW(NotImplemented, "y derivative for permeability is not implemented for SPE10 test case!");
      return;
    }

    bool constantK () const
    {
      return false;
    }

    double l(const int i) const {
      assert(i<Nlambda_);
      assert(i>=0);
      double val;
      if (i==0)
        val = 1.0;
      else if (i<=Ns_) {
        val = this->muLambda_[i-1];
      } else {
        int m = (i-Ns_-1)%Ns_;
        int n = int(std::ceil(double(i-Ns_-1.0)/double(Ns_)))%Ns_;
        val = (this->muLambda_[m]*this->muLambda_[n]);
      }
      return val;
    }

    int Nlambda() const {
      return Nlambda_;
    }

    virtual void lambda(const DomainType &x, RangeType &lm) const {
      int N = Nlambda();
      lm[0]=0.0;
      for (int i=0; i!=N; ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    // This is done for Omega=[0,10] !!!!
    virtual void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      lm=0.0;//
//      if (n==0) {
//        lm[0]=1.0/(muOil_);
//        return;
//      }
//      else if (n<=Ns_) {
//        SPart(n-1, x, lm);
//        lm[0]*= -2.0/muOil_;
//      }
//      else {
//        const int i = (n-Ns_-1)%Ns_;
//        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
//        RangeType valI(0.0);
//        RangeType valJ(0.0);
//        SPart(i, x, valI);
//        SPart(j, x, valJ);
//        lm[0] = (muOil_+muWater_*muWater_)/(muWater_*muOil_)*valI[0]*valJ[0];
//      }
      lm[0]=1.0;
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      assert(0);
//      if (n==0) {
//        jac[0]=0.0;
//        return;
//      }
//      else if (n<=Ns_) {
//        SPartJacobian(n, x, jac);
//        jac[0]*= -2.0/muOil_;
//      }
//      else {
//        const int i = (n-Ns_-1)%Ns_;
//        const int j = int(std::ceil(double(n-Ns_-1.0)/double(Ns_)))%Ns_;
//        JacobianRangeType jacI(0.0);
//        JacobianRangeType jacJ(0.0);
//        RangeType valI(0.0);
//        RangeType valJ(0.0);
//        SPart(i, x, valI);
//        SPart(j, x, valJ);
//        SPartJacobian(i, x, jacI);
//        SPartJacobian(j, x, jacJ);
//        jacI[0] *= valJ[0];
//        jacJ[0] *= valI[0];
//        jac[0] = jacI[0] + jacJ[0];
//        jac[0] *= (muOil_+muWater_*muWater_)/(muWater_*muOil_);
//      }
      return;
    }

  protected:
    void SPart(const int n, const DomainType &x, RangeType &lm) const {
      double gridXWidth = 762.0;
      double gridYWidth = 7.62;

      double foo = std::pow((double(2.0*(n+1))-Ns_*x[0]/(0.4*gridXWidth)),6);
      double val 
        = atan(-1.0*foo-10000.0/std::pow(gridYWidth,6)*std::pow((x[1]-0.5*gridYWidth),6))+(5.0/6.0)*M_PI;
      lm[0] = val*val*val;
      lm[0]*=0.1;
      if (GridType::dimension==3)
        lm[0]*=1-x[2];
      return;
    } 

    void SPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      assert(0);
      double gridXWidth = 762.0;
      double gridYWidth = 7.62;
      double term1 = double(2*(n+1))-double(Ns_)*x[0]*2.5/gridXWidth;
      double denominator = 1.0+std::pow((std::pow(term1,6)+10000.0*std::pow((x[1]-0.5*gridYWidth),6)/std::pow(gridYWidth,6)),2);
      jac[0][0]=5.0*Ns_*std::pow(term1,5)/(denominator*gridXWidth);
      jac[0][1]=-20000.0*std::pow((x[1]-0.5*gridYWidth),5)/(denominator*std::pow(gridYWidth,6));
      return;
    } 

  public:
    //! the n'th parameter-independent part of the boundary values
    void gDPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      if (x[2]>15.23) {
        ret[0]=300.0;
      } else
        ret[0]=160.0-0.2*x[0];
        //              ret[0]=330.0-0.39*x[0];
      return;
    }

    //! the n'th parameter-independent part of the boundary values
    void gNPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      if (x[0]<1e-4) {
        ret[0]= 1000.0;
      } else {
        ret[0] = 0.0;   
      }
      return;
    }
   
    //! the number of parts of the dirichlet values
    virtual unsigned int Ngd() const { 
      return Nlambda_;
    };

    //! the i'th parameter for the dirichlet boundary values
    virtual double mugd(const unsigned int nu) const {
      return l(nu);
    }
    
      //! the number of parts of the dirichlet values
    virtual unsigned int Ngn() const { 
      return Nlambda_;
    };
    
      //! the i'th parameter for the neumann boundary values
    virtual double mugn(const unsigned int nu) const {
      return l(nu);
    }

  private:
    void readPermeability() {
      permeability_ = new double[6000];
      std::ifstream file("/Users/svenkaulmann//Documents/diploma/dune/dune-rb/dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/perm.dat");
      double val;
      if(!file) // file couldn't be opened
        DUNE_THROW(IOError, "Data file for SPE10 permeability could not be opened!");
      file >> val;
      int counter=0;
      while ( !file.eof() ) { // keep reading until end-of-file
        permeability_[counter++]=val;
        file >> val; // sets EOF flag if no value found
      }
      file.close();
      return;
    }

    const double  muWater_;
    const double  muOil_;
    int	          Ns_;
    int	          Nlambda_;
    double*       permeability_;
  };



#endif /* _SPE10_H_ */
