#ifndef _OSCILLATINGINCLUSIONS_H_
#define _OSCILLATINGINCLUSIONS_H_

  template< class GridImp, class FunctionSpaceImp>
  class OscillatingInclusions
    : public ProblemInterface< FunctionSpaceImp >
  {
    typedef ProblemInterface< FunctionSpaceImp > BaseType;
    typedef Shockwave<GridImp, FunctionSpaceImp> ThisType;
  public:
    typedef typename BaseType::FunctionSpaceType FunctionSpaceType;

    static const int dimRange = FunctionSpaceType::dimRange;
    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType  RangeType;
    typedef typename BaseType::JacobianRangeType JacobianRangeType;
    typedef typename BaseType::DomainFieldType DomainFieldType;
    typedef typename BaseType::RangeFieldType  RangeFieldType;

    typedef typename FunctionSpaceType::HessianRangeType HessianRangeType;

    typedef typename BaseType::DiffusionMatrixType DiffusionMatrixType;

    explicit OscillatingInclusions()
      : eps_(Parameter::getValue<double>("problem.eps"))
    {
      this->replaceParameter(Parameter::getValue<std::vector<double> >("model.muLambda"));
      Nlambda_ = Parameter::getValue<int>("model.Nlambda");
      for (int i=0; i!=Nlambda_; ++i) {
        std::stringstream name;
        name << "model.lambdaNode" << i;
        std::vector<double> val = Parameter::getValue<std::vector<double> >(name.str());
        DomainType node(0.0);
        for (int j=0; j!=int(val.size()); ++j) {
          node[j]=val[j];
        }
        lambdaNodes_.push_back(node);
      }
    }

    ~OscillatingInclusions() {}

    //! the right hand side
    void f ( const DomainType &x, RangeType &ret ) const {
      ret[0] = -2.0;
    }

    void sigma(RangeType& ret) const {
      ret[0]=Parameter::getValue<double>("rb.sigma");
    }

    //! the exact solution 
    void u ( const DomainType &x, RangeType &ret ) const
    {
      assert(0);
    }

    //! the gradient of the exact solution 
    virtual void gradient ( const DomainType &x, JacobianRangeType &grad ) const
    {
      assert(0);
    }
    //! the diffusion matrix 
    void K ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      // set @f$K=\diag((1+x)\cdot\frac{2}{3}(1+\cos^2(2\pi \frac{x}{\varepsilon}))@f$
      //      double val = (2.0*(1.0+x[0]))/(3.0);
      double val = (2.0)/(3.0);
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      val *= 1.0+(cosVal*cosVal);
      m[0][0] = val;
      m[1][1] = val;
      return;
    }

    //! the x derivative of the diffusion matrix 
    void dxK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
      double cosVal = cos(2.0*M_PI*x[0]/eps_);
      double sinVal = sin(2.0*M_PI*x[0]/eps_);
      double val = 2.0/3.0;
      val += (2.0/3.0)*cosVal*cosVal;
      double foo = (8.0/3.0)*M_PI*(1+x[0])*cosVal*sinVal;
      foo /= eps_;
      val -= foo;
      m[0][0] = val;
      m[1][1] = val;
      return;
    }

    //! the y derivative of the diffusion matrix 
    void dyK ( const DomainType &x, DiffusionMatrixType &m ) const {
      m=0;
    }

    bool constantK () const
    {
      return false;
    }

    double l(const int i) const {
      assert(i<Nlambda_);
      assert(i>=0);
      return (this->muLambda_[i]);
    }

    int Nlambda() const {
      return Nlambda_;
    }

    void lambda(const DomainType &x, RangeType &lm) const {
      int N = Nlambda();
      lm[0]=0.0;
      for (int i=0; i!=N; ++i) {
        RangeType partValue(0.0);
        lambdaPart(i, x, partValue);
        partValue *= l(i);
        lm += partValue;
      }
      return;
    } 

    void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
      assert(n>=0&&n<lambdaNodes_.size());
      DomainType difference = x-lambdaNodes_[n];
      lm[0]=exp(-1000*(difference.two_norm()));
      return;
    } 

    void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
      DomainType difference=x-lambdaNodes_[n];
      double norm = difference.two_norm();
      double commonPart = -1000/norm;
      RangeType oldVal(0.0);
      lambdaPart(n,x, oldVal);
      commonPart *= oldVal[0];
      jac[0][0] = -(x[0]-lambdaNodes_[n][0])*commonPart;
      jac[0][1] = -(x[1]-lambdaNodes_[n][1])*commonPart;
      return;
    }

  public:
    //! the n'th parameter-independent part of the boundary values
    virtual void gPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
      ret[0]=0.0;//3.0*x[0]*x[0]/M_PI;
      return;
    }
    
    
    //! the number of parts of the dirichlet values
    virtual unsigned int Ngd() const { 
      return Nlambda_;
    };

    //! the i'th parameter for the boundary values
    virtual double mugd(const unsigned int nu) const {
      return l(nu);
    }

  private:
    int		Nlambda_;
    double		eps_;
    std::vector<DomainType>	lambdaNodes_;
  };

#endif /* _OSCILLATINGINCLUSIONS_H_ */
