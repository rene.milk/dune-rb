#ifndef _THERMALBLOCK_H_
#define _THERMALBLOCK_H_

#include <dune/rb/rbasis/twophaseflow/highdim/probleminterface.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/seperableparametricfunction.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>

namespace Dune {
namespace RB {

template < class DomainType, class RangeType, class ParameterDomainType >
class ThermalblockMobilityFunction
: public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType > {

    typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
    BaseType;

public:
    /* the following is needed if we want to use the BaseType::coefficient(int i) method
     * (is shadowed otherwise)*/
    using BaseType::coefficient;
    using BaseType::component;

    ThermalblockMobilityFunction()
         : subdomainMarker_(Parameter::getValue<std::string>("thermalblock.parameterSubdomainPartitionFile").c_str())
    {
        setParameter(Parameter::getValue< ParameterDomainType >("model.muLambda"));
        subdomainMarker_.readSubdomains();
        if (size()!=subdomainMarker_.numSubdomains())
            DUNE_THROW(IOError, "Parameter does not have right size for given thermalblock block file!");
    }

    const int size() const {
        return this->mu_.size();
    }

    const double coefficient(const int i, const ParameterDomainType& mu) const {
        assert(i<size() && i>=0);
        assert(size()==mu.size());

        return mu[i];
    }


    const RangeType component(const int i, const DomainType& x) const {
        assert(i<subdomainMarker_.numSubdomains());

        RangeType lm(0.0);
        if (subdomainMarker_.getSubdomain(i).contains(x))
            lm[0]=1.0;
        else
            lm[0]=0.0;
        return lm;
    }
protected:
    Dune::RB::SubdomainMarker subdomainMarker_;

};

template < class DomainType, class RangeType, class ParameterDomainType >
class ThermalblockDirichletValueFunction
: public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType > {

    typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
    BaseType;
public:

    /* the following is needed if we want to use the BaseType::coefficient(int i) method
     * (is shadowed otherwise)*/
    using BaseType::coefficient;
    using BaseType::component;


    ThermalblockDirichletValueFunction()
    {}

    const int size() const {
        return 1;
    }

    const double coefficient(const int i, const ParameterDomainType& mu) const {
        return 1.0;
    }


    const RangeType component(const int i, const DomainType& x) const {
        RangeType ret(0.0);
        return ret;
    }
};

template < class DomainType, class RangeType, class ParameterDomainType >
class ThermalblockNeumannValueFunction
: public SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType > {

    typedef SeperableParametricFunctionInterface< DomainType, RangeType, ParameterDomainType >
    BaseType;
public:

    /* the following is needed if we want to use the BaseType::coefficient(int i) method
     * (is shadowed otherwise)*/
    using BaseType::coefficient;
    using BaseType::component;


    ThermalblockNeumannValueFunction()
    {}

    const int size() const {
        return 1;
    }

    const double coefficient(const int i, const ParameterDomainType& mu) const {
        return 1.0;
    }


    const RangeType component(const int i, const DomainType& x) const {
        RangeType ret(0.0);
        if (fabs(x[1])<1e-18) {
            ret[0] = 1.0;
        } else
            ret[0] = 0.0;
        return ret;
    }
};

template < int dimRange >
class Thermalblock
: public ProblemInterface<dimRange, Thermalblock<dimRange> > {

    typedef ProblemInterface<dimRange, Thermalblock<dimRange> > BaseType;

        // this is needed in order to use the const version of those functions from the interface
    using BaseType::mobility;
    using BaseType::dirichletValues;
    using BaseType::neumannValues;
    // using BaseType::implementation;

public:
    typedef FieldVector<double, GRIDDIM>
    DomainType;
    typedef FieldVector<double, dimRange>
    RangeType;
    typedef typename BaseType::ParameterDomainType
    ParameterDomainType;
    typedef FieldMatrix<double, DomainType::size, DomainType::size>
    DiffusionMatrixType;

    typedef ThermalblockMobilityFunction<DomainType, RangeType, ParameterDomainType>
    ThermalblockMobilityFunctionType;
    typedef ThermalblockDirichletValueFunction<DomainType, RangeType, ParameterDomainType>
    ThermalblockDirichletValueFunctionType;
    typedef ThermalblockNeumannValueFunction<DomainType, RangeType, ParameterDomainType>
    ThermalblockNeumannValueFunctionType;
    enum BoundaryType {Dirichlet, Neumann};

    Thermalblock()
    {}


    RangeType rightHandSide( const DomainType &x ) const {
        return RangeType(1.0);
    }

    template< class EntityType, class MatrixType >
    void permeability(const DomainType &x, const EntityType& e, MatrixType &k ) const {
        assert(k.rows()==2 && k.cols()==2);

        k << 1, 0, 0, 1;
        return;
    }

    template < class EntityType >
    void permeability( const DomainType &x, const EntityType& e, DiffusionMatrixType &k ) const {

        k=0.0;
        k[0][0] = 1.0;
        k[1][1] = 1.0;

        return;
    }


    ThermalblockMobilityFunctionType& mobility() {
        return mobilityFunction_;
    }

    const ThermalblockMobilityFunctionType& mobility() const {
        return mobilityFunction_;
    }

    ThermalblockDirichletValueFunctionType& dirichletValues() {
        return dirichletValueFunction_;
    }

    const ThermalblockDirichletValueFunctionType& dirichletValues() const {
        return dirichletValueFunction_;
    }

    ThermalblockNeumannValueFunctionType& neumannValues() {
        return neumannValueFunction_;
    }

    const ThermalblockNeumannValueFunctionType& neumannValues() const {
        return neumannValueFunction_;
    }
    
    
    // The following is necessary because YASP grid confuses my boundaries ids.
    /** Obtain the boundary type of a given boundary intersection.
     *
     *  @param[in] e Intersection whose boundary type shall be returned.
     *  @returns Returns the boundary type of the given intersection.
     */
    template <class IntersectionType>
    BoundaryType boundaryType (const IntersectionType &intersection) const {
        DomainType center = intersection.geometry().center();
        if (center[1]<1e-6 || center[1]>0.99999) {
            return Dirichlet;
        } else {
            return Neumann;
        }
        // DUNE_THROW(NotImplemented, "The boundary ID you provided does not exist!");
    }
  
  template < class GridImp >
  void visualizePermeability(GridImp& grid) {}
    

private:
    ThermalblockMobilityFunctionType mobilityFunction_;
    ThermalblockDirichletValueFunctionType dirichletValueFunction_;
    ThermalblockNeumannValueFunctionType neumannValueFunction_;
};

}     // namespace RB
}    // namespace Dune

#endif    /*  _THERMALBLOCK_H_ */