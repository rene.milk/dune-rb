#ifndef FEMHOWTO_BASE_HH
#define FEMHOWTO_BASE_HH

#include <sstream>

#include <dune/common/misc.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>

#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/misc/utility.hh>
#include <dune/fem/misc/l2error.hh>
#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/misc/femtimer.hh>
#include <dune/fem/misc/gridwidth.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/io/file/dataoutput.hh>

namespace Dune {

GridPtr<GridSelector::GridType> initialize(const std::string problemDescription) { /*@LST0S@*/
  // ----- read in runtime parameters ------
  std::string filename; /*@\label{fv:param0}@*/
  Parameter::get("highdim.gridFile", filename);
  int startLevel = Parameter::getValue<int>("highdim.startLevel", 0);

  // output of error and eoc information
  std::string eocOutPath = Parameter::getValue<std::string>("highdim.eocOutputPath",  /*@\label{fv:param1}@*/
                                                            std::string("."));
  std::string eocFile = eocOutPath + std::string("/eoc.tex");
  FemEoc::initialize(eocOutPath, "eoc", problemDescription); /*@\label{fv:femeocInit}@*/

  // initialize grid
  GridPtr<GridSelector::GridType> gridptr(filename);
  // and refine the grid until the startLevel is reached
  for(int level=0; level < startLevel ; ++level)
    GlobalRefine::apply(*gridptr, 1 ); /*@\label{fv:globalRefine1}@*/
  return gridptr;
} /*@LST0E@*/


template <class Algorithm>
void compute(Algorithm& algorithm) {
  typedef typename Algorithm::DiscreteFunctionType DiscreteFunctionType;
  const typename Algorithm::DiscreteSpaceType& space = algorithm.space();
  // the solution function
  DiscreteFunctionType u("solution", space);
  compute(algorithm, u);
}

template <class Algorithm, class DiscreteFunctionType>
void compute(Algorithm& algorithm, DiscreteFunctionType &u)
{
  const typename Algorithm::DiscreteSpaceType& space = algorithm.space();
  typename Algorithm::GridPartType& gridPart = space.gridPart();
  GridSelector::GridType& grid = gridPart.grid();

  // solution function
  u.clear();

  // get some parameters
  const int eocSteps   = Parameter::getValue<int>("highdim.eocSteps", 1);

  // Initialize the DataOutput that writes the solution on the harddisk in a
  // format readable by e.g. Paraview
  // in each loop for the eoc computation the results at
  // the final time is stored
  typedef Tuple<DiscreteFunctionType*> IOTupleType;
  typedef DataOutput<GridSelector::GridType, IOTupleType> DataOutputType;
  IOTupleType dataTup ( &u );
  DataOutputType dataOutput( grid, dataTup );

  const unsigned int femTimerId = FemTimer::addTo("timestep");
  for(int eocloop=0; eocloop < eocSteps; ++eocloop)
  {
    FemTimer :: start(femTimerId);

    // do one step
    algorithm(u);

    double runTime = FemTimer::stop(femTimerId);

    FemTimer::printFile("./timer.out");
    FemTimer::reset(femTimerId);

    // Write solution to hd
    dataOutput.writeData(eocloop);

    // finalize algorithm (compute errors)
    algorithm.finalize(u);

    // calculate grid width
    const double h = GridWidth::calcGridWidth(gridPart);

    if( Parameter :: verbose() )
      FemEoc::write(h,grid.size(0),runTime,0, std::cout);
    else
      FemEoc::write(h,grid.size(0),runTime,0);

    // Refine the grid for the next EOC Step. If the scheme uses adaptation,
    // the refinement level needs to be set in the algorithms' initialize method.
    if(eocloop < eocSteps-1)
    {
      GlobalRefine::apply(grid,DGFGridInfo<GridSelector::GridType>::refineStepsForHalf());
      grid.loadBalance();
    }
  } /***** END of EOC Loop *****/
  FemTimer::removeAll();
}

} // namespace Dune

#endif
