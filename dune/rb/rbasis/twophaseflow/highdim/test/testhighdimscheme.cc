#include <config.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>

// grid stuff
//#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/lagrangeinterpolation.hh>
//#include <dune/fem/space/fvspace.hh>
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithmneu.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/problem.hh>

#include <dune/rb/rbasis/twophaseflow/rb/highdimoperator.hh>

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

// local includes
#include <dune/rb/misc/parameter/parameter.hh>

using namespace Dune;
using namespace RB;

const int polynomialOrder = POLORDER;

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                              GridType;
    typedef LeafGridPart<GridType>                                              GridPart;
    typedef MultiDomainGridPart<GridPart>                                       MDGridPartType;
    typedef FunctionSpace<double, double, GridType::dimension, 1>               FunctionSpaceType;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpaceType, MDGridPartType, polynomialOrder> 
    DiscFuncSpaceType;
    typedef DiscontinuousGalerkinSpace<FunctionSpaceType, MDGridPartType, polynomialOrder> 
    DGSpaceType;
    typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                         DiscreteFunctionType;
    typedef AdaptiveDiscreteFunction<DGSpaceType>                               DGFunctionType;
    typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType>   ConstrainedDF;
    typedef ProblemInterface<FunctionSpaceType>                                 ProblemType;

    MPIManager::initialize(argc, argv);
    Parameter::append("testhighdimscheme.params");

    GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("fem.io.macroGridFile_2d"));
    GridType& grid = *gridPtr;
    // prepare the convergence table
    std::ofstream file("errors.dat");
    std::string headline = "{Grid size}\t{Grid Width}\t{L2 Error}\t{EOC (L2)}\t{H1 Error}\t{EOC (H1)}\t{Used Time}\n";
    file << headline;
    std::cout << headline;

    std::vector<double> l2errors;
    std::vector<double> l2errorsDG;
    std::vector<double> h1errors;
    std::vector<double> h1errorsDG;

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    const unsigned int maxSize = Parameter::getValue<int>("test.maxGridRefinement");
    for (unsigned int i=0; i<=maxSize; ++i) {

      // refine the grid globally
      grid.globalRefine(1);

      // get a gridpart
      MDGridPartType mdGridPart(grid);
      // get a discrete function space
      DiscFuncSpaceType discFuncSpace(mdGridPart);

      DGSpaceType dgSpace(mdGridPart);

      //! define the type of the system matrix object
      typedef Dune::SparseRowMatrixTraits<DiscFuncSpaceType, DiscFuncSpaceType>
        MatrixObjectTraits;
      typedef Dune::SparseRowMatrixTraits<DGSpaceType, DGSpaceType>
        DGMatrixObjectTraits;      
      //! define the discrete fe laplace operator, see ./laplaceoperator.hh 
      typedef Dune::LaplaceFEOp< DiscreteFunctionType, MatrixObjectTraits >
        LaplaceOperatorType; 
      // define the discrete DG operator
      typedef HighdimOperator<DGFunctionType, DGMatrixObjectTraits>           
        DGOperatorType;

      // get high dimensional algorithm
      Algorithm<LaplaceOperatorType> highDimAlgorithm(discFuncSpace, *problem);
      Algorithm<DGOperatorType> dgAlgorithm(dgSpace, *problem);    
      // compute the high dim solution + visualize
      DiscreteFunctionType highDimSolution("highdimsol", discFuncSpace);

      DGFunctionType dgSolution("dgsolution", dgSpace);

      highDimSolution.clear();

      double usedTime = EXECUTETIMEDMILLISECONDS(highDimAlgorithm(highDimSolution));

      double dgTime = EXECUTETIMEDMILLISECONDS(dgAlgorithm(dgSolution));

      DiscreteFunctionAdapter< ProblemType::ExactSolutionType, MDGridPartType >
        exactSolutionDiscrete("exact solution", problem->exactSolution(), mdGridPart, polynomialOrder+1);

      // // visualize
      plotFunction(dgSolution);
      plotFunction(highDimSolution);

      // compute errors
      // create L2 - Norm 
      Dune::L2Norm<MDGridPartType> l2norm(mdGridPart);
      // calculate L2 - Norm 
      const double l2error = l2norm.distance(exactSolutionDiscrete, highDimSolution);      /*@LST0E@*/
      const double l2errorDG = l2norm.distance(exactSolutionDiscrete, dgSolution);      /*@LST0E@*/
      l2errors.push_back(l2error);
      l2errorsDG.push_back(l2errorDG);
      // create H1 - Norm 
      Dune::H1Norm<MDGridPartType> h1norm(mdGridPart);
      // calculate H1 - Norm 
      const double h1error = h1norm.distance(exactSolutionDiscrete, highDimSolution);
      const double h1errorDG = h1norm.distance(exactSolutionDiscrete, dgSolution);
      h1errors.push_back(h1error);
      h1errorsDG.push_back(h1errorDG);

      // calc grid width
      double gridWidth = sqrt(2.0)*pow(0.5,i+1);
      // calculate the eoc
      double eocl2 = std::numeric_limits<double>::signaling_NaN();
      double eoch1 = std::numeric_limits<double>::signaling_NaN();

      double eocl2DG = std::numeric_limits<double>::signaling_NaN();
      double eoch1DG = std::numeric_limits<double>::signaling_NaN();

      if (i>0) {
        eocl2 = (log(l2errors[i-1]/l2errors[i])/log(2.0));
        eoch1 = (log(h1errors[i-1]/h1errors[i])/log(2.0));

        eocl2DG = (log(l2errorsDG[i-1]/l2errorsDG[i])/log(2.0));
        eoch1DG = (log(h1errorsDG[i-1]/h1errorsDG[i])/log(2.0));

      }

      std::stringstream line;
      line << grid.size(0) << "\t" << gridWidth << "\t" 
           << l2error << "\t" << eocl2 << "\t"
           << h1error << "\t" << eoch1 << "\t"
           << usedTime << "\t"
           << l2errorDG << "\t" << eocl2DG << "\t"
           << h1errorDG << "\t" << eoch1DG << "\t"
           << dgTime
           <<"\n";

      // output to std::cout 
      std::cout << line.str();
      // output to data file
      file << line.str();

   } // loop over grid refinements
    
    delete problem;

    file.close();

  } catch (Dune::Exception e) {
    std::cerr << e.what();
    return 1;
  }  
  return 0;
}
