#ifndef DUNE_LAPLACE_HH
#define DUNE_LAPLACE_HH

//- Dune includes
#include <dune/common/fmatrix.hh>
#include <dune/common/timer.hh>

#include <dune/fem/storage/array.hh>
#include <dune/fem/quadrature/quadrature.hh>
#include <dune/fem/function/common/scalarproducts.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/2order/lagrangematrixsetup.hh>

#include "probleminterface.hh"

namespace Dune
{

  //! \brief The Laplace operator
  template< class DiscreteFunction, class MatrixTraits >             /*@LST0S@*/
  class LaplaceFEOp
  : public Operator< typename DiscreteFunction::RangeFieldType,
                     typename DiscreteFunction::RangeFieldType,
                     DiscreteFunction,
                     DiscreteFunction >,
    public OEMSolver::PreconditionInterface                         /*@\label{poi:precif}@*/
  {                                                                 /*@LST0E@*/
    typedef LaplaceFEOp< DiscreteFunction, MatrixTraits > ThisType;
    typedef Operator< typename DiscreteFunction::RangeFieldType, typename DiscreteFunction::RangeFieldType,
                      DiscreteFunction, DiscreteFunction > BaseType;

    // needs to be friend for conversion check 
    friend class Conversion<ThisType,OEMSolver::PreconditionInterface>;
    
  public:
    //! type of discrete functions
    typedef DiscreteFunction DiscreteFunctionType;

    //! type of discrete function space
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteFunctionSpaceType;

    //! type of problem 
    typedef ProblemInterface< typename DiscreteFunctionSpaceType :: FunctionSpaceType, typename DiscreteFunctionSpaceType::GridPartType > 
        ProblemType; 

    //! field type of range
    typedef typename DiscreteFunctionSpaceType :: RangeFieldType
      RangeFieldType;

    //! type of range
    typedef typename DiscreteFunctionSpaceType :: RangeType
      RangeType;
       
  protected:
    //! type of jacobian
    typedef typename DiscreteFunctionSpaceType :: JacobianRangeType
      JacobianRangeType;
    //! type of the base function set
    typedef typename DiscreteFunctionSpaceType :: BaseFunctionSetType
      BaseFunctionSetType;

  public:
    //! type of grid partition
    typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;
    //! type of grid
    typedef typename DiscreteFunctionSpaceType :: GridType GridType;

    //! polynomial order of base functions
    enum { polynomialOrder = DiscreteFunctionSpaceType :: polynomialOrder };

    //! The grid's dimension
    enum { dimension = GridType :: dimension };
        
    //! type of quadrature to be used
    typedef CachingQuadrature< GridPartType, 0 > QuadratureType;

    typedef typename MatrixTraits
      :: template  MatrixObject< LagrangeMatrixTraits< MatrixTraits > >
      :: MatrixObjectType LinearOperatorType;

    //! get important types from the MatrixObject 
    typedef typename LinearOperatorType :: LocalMatrixType LocalMatrixType;
    typedef typename LinearOperatorType :: PreconditionMatrixType PreconditionMatrixType;
    typedef typename LinearOperatorType :: MatrixType MatrixType;

  public:
    // types for boundary treatment
    // ----------------------------
    typedef typename DiscreteFunctionSpaceType :: MapperType MapperType;
    typedef SlaveDofs< DiscreteFunctionSpaceType, MapperType > SlaveDofsType;
    typedef typename SlaveDofsType :: SingletonKey SlaveDofsKeyType;    /*@\label{poi:singleton}@*/
    typedef SingletonList< SlaveDofsKeyType, SlaveDofsType >
      SlaveDofsProviderType;
    
  protected:
    // type of DofManager
    typedef DofManager< GridType > DofManagerType;

   
  public:
    //! constructor 
    LaplaceFEOp ( const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem )
    : discreteFunctionSpace_( dfSpace ),
      dofManager_( DofManagerType::instance( dfSpace.grid() ) ),
      linearOperator_( discreteFunctionSpace_, discreteFunctionSpace_ ),
      sequence_( -1 ),
      problem_( problem ),
      slaveDofs_( getSlaveDofs( discreteFunctionSpace_ ) ),
      gradCache_( discreteFunctionSpace_.mapper().maxNumDofs() ),
      gradDiffusion_( discreteFunctionSpace_.mapper().maxNumDofs() )
    {}
        
  private:
    // prohibit copying
    LaplaceFEOp ( const ThisType & );

  public:                                                           /*@LST0S@*/
    //! apply the operator
    virtual void operator() ( const DiscreteFunctionType &u, 
                              DiscreteFunctionType &w ) const 
    {
      systemMatrix().apply( u, w );                                 /*@\label{poi:matrixEval}@*/
    }                                                               /*@LST0E@*/
  
    //! return reference to preconditioning matrix, used by OEM-Solver
    const PreconditionMatrixType &preconditionMatrix () const
    {
      return systemMatrix().preconditionMatrix();
    }

    //! return true if preconditioning is enabled
    bool hasPreconditionMatrix () const
    {
      return linearOperator_.hasPreconditionMatrix();
    }

    //! print the system matrix into a stream
    void print ( std :: ostream & out = std :: cout ) const 
    {
      systemMatrix().matrix().print( out );
    }

    //! return reference to discreteFunctionSpace
    const DiscreteFunctionSpaceType &discreteFunctionSpace () const
    {
      return discreteFunctionSpace_;
    }

    //! return reference to problem
    const ProblemType& problem() const
    {
      return problem_;
    } 

    /*! \brief obtain a reference to the system matrix
     *
     *  The assembled matrix is returned. If the system matrix has not been
     *  assembled, yet, the assembly is performed.
     *
     *  \returns a reference to the system matrix
     */
    LinearOperatorType &systemMatrix () const                        /*@LST0S@*/
    {
      // if stored sequence number it not equal to the one of the
      // dofManager (or space) then the grid has been changed
      // and matrix has to be assembled new
      if( sequence_ != dofManager_.sequence() )                     /*@\label{poi:sequence}@*/
        assemble();

      return linearOperator_;
    }

    /** \brief perform a grid walkthrough and assemble the global matrix */
    void assemble () const 
    {
      const DiscreteFunctionSpaceType &space = discreteFunctionSpace();

      // reserve memory for matrix 
      linearOperator_.reserve();                                   /*@LST0E@*/

      // create timer (also stops time)
      Timer timer;

      // clear matrix                                             /*@LST0S@*/ 
      linearOperator_.clear();

      // apply local matrix assembler on each element
      typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
      IteratorType end = space.end();
      for(IteratorType it = space.begin(); it != end; ++it)
      {
        assembleLocal( *it );                                       /*@\label{poi:applAss}@*/
      }

      // correct matrix due to boundary conditions 
      boundaryCorrectOnGrid();              /*@\label{poi:bdCorr}@*/  /*@LST0E@*/
      
      // get elapsed time 
      const double assemblyTime = timer.elapsed();
      // in verbose mode print times 
      if ( Parameter :: verbose () )
        std :: cout << "Time to assemble FE matrix: " << assemblyTime << "s" << std :: endl;

      // get grid sequence number from space (for adaptive runs)    /*@LST0S@*/
      sequence_ = dofManager_.sequence();
    }

  protected:
    //! assemble local matrix for given entity
    template< class EntityType >
    void assembleLocal( const EntityType &entity ) const
    {
      typedef typename EntityType :: Geometry Geometry;

      // assert that matrix is not build on ghost elements 
      assert( entity.partitionType() != GhostEntity );

      // cache geometry of entity 
      const Geometry &geometry = entity.geometry();

      // get local matrix from matrix object
      LocalMatrixType localMatrix                                   /*@\label{poi:localMInit}@*/
        = linearOperator_.localMatrix( entity, entity );
      
      // get base function set 
      const BaseFunctionSetType &baseSet = localMatrix.domainBaseFunctionSet();/*@\label{poi:baseSetInit}@*/

      // get number of local base functions 
      const size_t numBaseFunctions = baseSet.numBaseFunctions();
            
      // create quadrature of appropriate order 
      QuadratureType quadrature( entity, 2*polynomialOrder+1 );/*@\label{poi:quadraInit}@*/

      // loop over all quadrature points
      const size_t numQuadraturePoints = quadrature.nop();
      for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
      {
        // get local coordinate of quadrature point 
        const typename QuadratureType :: CoordinateType &x
          = quadrature.point( pt );

        // get jacobian inverse transposed 
        const typename Geometry :: Jacobian& inv
          = geometry.jacobianInverseTransposed( x );

        typedef typename ProblemType :: DiffusionMatrixType DiffusionMatrixType;
        DiffusionMatrixType K;
        // evaluate diffusion matrix 
        problem().K( geometry.global( x ), K );

        // for all base functions evaluate the gradient
        // on quadrature point pt and apply jacobian inverse 
        baseSet.jacobianAll( quadrature[ pt ], inv, gradCache_ );

        RangeType lambda;
        problem().lambda( geometry.global(x), lambda);
        // apply diffusion tensor  
        for( size_t i = 0; i < numBaseFunctions; ++i ) {
          K.mv( gradCache_[ i ][ 0 ], gradDiffusion_[ i ][ 0 ] );
          gradDiffusion_[ i ][ 0 ] *= lambda[0];
          
        }
        // evaluate integration weight 
        weight_ = quadrature.weight( pt ) * geometry.integrationElement( x );
        
        // add scalar product of gradients to local matrix 
        updateLocalMatrix( localMatrix );
      }
    }
  
    //! add scalar product of cached gradients to local matrix
    void updateLocalMatrix ( LocalMatrixType &localMatrix ) const
    {
      const size_t rows    = localMatrix.rows();
      const size_t columns = localMatrix.columns();
      for( size_t i = 0; i < rows; ++i )
      {
        for ( size_t j = 0; j < columns; ++j )
        {
          const RangeFieldType value
            = weight_ * (gradCache_[ i ][ 0 ] * gradDiffusion_[ j ][ 0 ]);
          localMatrix.add( j, i, value );
        }
      }
    }
  
    //! adjust matrix due to boundary conditions
    void boundaryCorrectOnGrid () const
    {                                                                /*@LST0E@*/
      typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
      typedef typename IteratorType :: Entity EntityType;

      const DiscreteFunctionSpaceType &dfSpace = discreteFunctionSpace();

      const IteratorType end = dfSpace.end();
      for( IteratorType it = dfSpace.begin(); it != end; ++it )
      {
        const EntityType &entity = *it;
        // if entity has boundary intersections 
        if( entity.hasBoundaryIntersections() )
          boundaryCorrectOnEntity( entity );
      }
    }                                                      /*@LST0S@*//*@LST0E@*/

    /*! treatment of Dirichlet-DoFs for one entity
     *
     *   delete rows for dirichlet-DoFs, setting diagonal element to 1.
     *
     *   \note A LagrangeDiscreteFunctionSpace is implicitly assumed.
     *
     *   \param[in]  entity  entity to perform Dirichlet treatment on
     */
    template< class EntityType >
    void boundaryCorrectOnEntity ( const EntityType &entity ) const /*@LST0S@*/
    {                                                               /*@LST0E@*/
      typedef typename DiscreteFunctionSpaceType :: LagrangePointSetType
        LagrangePointSetType;

      const int faceCodim = 1;
      typedef typename GridPartType :: IntersectionIteratorType
        IntersectionIteratorType;
      typedef typename LagrangePointSetType
        :: template Codim< faceCodim > :: SubEntityIteratorType
        FaceDofIteratorType;

      const DiscreteFunctionSpaceType &dfSpace = discreteFunctionSpace();
      const GridPartType &gridPart = dfSpace.gridPart();

      const LagrangePointSetType &lagrangePointSet                  /*@\label{poi:lagPoSet}@*/
        = dfSpace.lagrangePointSet( entity );

      SlaveDofsType &slaveDofs = this->slaveDofs();
      const int numSlaveDofs = slaveDofs.size();
 
      // get local matrix from matrix object 
      LocalMatrixType localMatrix = linearOperator_.localMatrix( entity, entity );

      // loop over all intersections and find dirichlet 
      // boundaries 
      const IntersectionIteratorType endit = gridPart.iend( entity );
      for( IntersectionIteratorType it = gridPart.ibegin( entity );
           it != endit ; ++it )
      {
        // get intersection 
        typedef typename IntersectionIteratorType :: Intersection IntersectionType;
        const IntersectionType& intersection = *it;
    
        // skip non-boundary elements and neumann-boundaries
        if( ! intersection.boundary() )    /*@\label{poi:skipNonBd}@*/
          continue;
        if (intersection.boundaryId()==2)
          continue;

        // get local face number of boundary intersection 
        const int face = intersection.indexInInside();

        // get iterator over all local dofs on this face 
        FaceDofIteratorType faceIt
          = lagrangePointSet.template beginSubEntity< faceCodim >( face );
        const FaceDofIteratorType faceEndIt
          = lagrangePointSet.template endSubEntity< faceCodim >( face );

        // iterate over face dofs and set unit row 
        for( ; faceIt != faceEndIt; ++faceIt )
        {
          const int localDof = *faceIt;
          // clear the whole row
          localMatrix.clearRow( localDof );

          // set diagonal to 1 
          localMatrix.set( localDof, localDof, 1 );

          // clear all other columns  
          const int globalDof = dfSpace.mapToGlobal( entity, localDof ); /*@\label{poi:clslv0}@*/ 

          for( int i = 0; i < numSlaveDofs; ++i )
          {
            // slave dofs are canceled 
            if( globalDof == slaveDofs[ i ] )
              localMatrix.set( localDof, localDof, 0 );
          }                                                         /*@\label{poi:clslv1}@*/ 
        }
      }
    }                                                                 /*@LST0S@*//*@LST0E@*/

    // return slave dofs                          /*@\label{poi:slavedof0}@*/
    static SlaveDofsType *getSlaveDofs ( const DiscreteFunctionSpaceType &space )
    {
      SlaveDofsKeyType key( space, space.mapper() );
      return &(SlaveDofsProviderType :: getObject( key ));
    }

    // return reference to slave dofs 
    SlaveDofsType &slaveDofs () const
    {
      slaveDofs_->rebuild();
      return *slaveDofs_;
    }                                                    /*@\label{poi:slavedof1}@*/

  protected:
    const DiscreteFunctionSpaceType &discreteFunctionSpace_;
    const DofManagerType &dofManager_;

    //! pointer to the system matrix
    mutable LinearOperatorType linearOperator_;
 
    //! flag indicating whether the system matrix has been assembled
    mutable int sequence_;
      
    //! the diffusion tensor 
    const ProblemType& problem_;

    //! pointer to slave dofs 
    SlaveDofsType *const slaveDofs_;

    mutable DynamicArray< JacobianRangeType > gradCache_;
    mutable DynamicArray< JacobianRangeType > gradDiffusion_;
    mutable RangeFieldType weight_;    
  };                                                                  /*@LST0S@*//*@LST0E@*/



  // Assembler for right hand side
  // note: this is not an L2-Projection
  template< class DiscreteFunction >
  class RightHandSideAssembler
  {
  public:
    typedef DiscreteFunction DiscreteFunctionType;
    
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteFunctionSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType
      LocalFunctionType;
  
    typedef typename DiscreteFunctionSpaceType :: BaseFunctionSetType
      BaseFunctionSetType;
    typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
    typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;
    typedef typename GridPartType :: GridType GridType;

    enum { dimension = GridType :: dimension };
    //! polynomial order of base functions
    enum { polynomialOrder = DiscreteFunctionSpaceType :: polynomialOrder };


  public:
    //! discreteFunction is an output parameter (kind of return value)
    template< class FunctionType >
    static void assemble( const FunctionType &function,
                          DiscreteFunctionType &discreteFunction,
                          const int polOrd)
    {
      typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;
      typedef typename IteratorType :: Entity EntityType;
      typedef typename EntityType :: Geometry GeometryType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType; 
      typedef typename IntersectionIteratorType::Intersection IntersectionType;

      // We use a caching quadrature for codimension 0 entities
      typedef CachingQuadrature< GridPartType, 0 > QuadratureType;
      typedef CachingQuadrature<GridPartType, 1> FaceQuadratureType;
      
      const DiscreteFunctionSpaceType &discreteFunctionSpace
        = discreteFunction.space();
  
      // set discreteFunction to zero
      discreteFunction.clear(); 

      std::vector< RangeType > phis;

      // start grid traversal 
      const IteratorType endit = discreteFunctionSpace.end();
      for( IteratorType it = discreteFunctionSpace.begin(); 
           it != endit; ++it )
      {
        // *it gives a reference to the current entity
        const EntityType &entity = *it;

        // obtain a reference to the entity's geometry
        const GeometryType &geometry = entity.geometry();
      
        // obtain BaseFunctionSet for the entity
        // note that base functions are always defined on the reference geometry
        LocalFunctionType localFunction = discreteFunction.localFunction( entity ); 

        QuadratureType quadrature( entity, 2*polynomialOrder+1  );
        const size_t numQuadraturePoints = quadrature.nop();
        phis.resize( numQuadraturePoints );
        for( size_t qP = 0; qP < numQuadraturePoints; ++qP )
        {
          // get integration element multiplied with quadrature weight
          const double factor = 
            geometry.integrationElement( quadrature.point( qP ) ) * 
            quadrature.weight( qP );

          // evaluate right hand side function
          function.evaluate( geometry.global( quadrature.point( qP ) ), phis[ qP ] );

          // apply factor 
          phis[ qP ] *= factor;
        }
        // add to local function (evaluates each base function)
        localFunction.axpyQuadrature( quadrature, phis );
        
        // treat neumann - boundary values
        const IntersectionIteratorType iend = discreteFunctionSpace.gridPart().iend(entity);
        for (IntersectionIteratorType iit = discreteFunctionSpace.gridPart().ibegin(entity);
             iit!=iend; ++iit) {
          const IntersectionType& intersection = *iit;
          if (intersection.boundary() &&
              function.boundaryType(intersection)==FunctionType::Neumann) {
            FaceQuadratureType faceQuad(discreteFunctionSpace.gridPart(),
                                        intersection, 4*polynomialOrder+1,
                                        FaceQuadratureType::INSIDE);
            const size_t numIntQuadPoints = faceQuad.nop();
            std::vector<RangeType> bndValues;
            bndValues.resize(numIntQuadPoints);
            // loop over all quad points
            for (unsigned int iqP = 0; iqP!=numIntQuadPoints; ++iqP) {
              // get local coordinate of quadrature point 
              const typename IntersectionType::Geometry &inGeo = intersection.geometry();
              const typename FaceQuadratureType::LocalCoordinateType &xLocal=faceQuad.localPoint(iqP);
              typedef typename FaceQuadratureType::CoordinateType CoordinateType;
              const CoordinateType& xGlobal=inGeo.global(xLocal);
              const double factor = inGeo.integrationElement(xLocal)*faceQuad.weight(iqP);
              function.gN(xGlobal, bndValues[iqP]);
              bndValues[iqP] *= factor;
            }
            localFunction.axpyQuadrature( faceQuad, bndValues );
          }
        }
      }

      // communicate data (for parallel runs)
      discreteFunction.communicate();
    }
  };

} // end namespace 
#endif
