#ifndef __ALGORITHM__
#define __ALGORITHM__


// boost 
#include <boost/timer/timer.hpp>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/rb/highdimoperator.hh>

#include <Eigen/Sparse>

namespace Dune {
namespace RB {



template<class DiscreteOperatorImpl, class DofVectorImp>
class Algorithm {
public:
    typedef DiscreteOperatorImpl DiscreteOperatorType;
    typedef DofVectorImp DofVectorType;
    typedef typename DiscreteOperatorType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename DiscreteOperatorType::ProblemType ProblemType;
    typedef typename DiscreteOperatorType::MatrixType MatrixType;
//    typedef Eigen::SuperLU<MatrixType>             SolverType;
//    typedef Eigen::BiCGSTAB<MatrixType>           SolverType;
      typedef Eigen::BiCGSTAB<MatrixType, Eigen::IncompleteLUT<double> >           SolverType;
//    typedef Eigen::BiCGSTAB<MatrixType, Eigen::IdentityPreconditioner>           SolverType;
//    typedef Eigen::ConjugateGradient<MatrixType>   SolverType;
//    typedef Eigen::SimplicialCholesky<MatrixType>  SolverType;
    typedef std::multimap<std::string, DofVectorType> SolutionsMapType;
    typedef std::pair<std::string, DofVectorType> PairType;

public:
    Algorithm(const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem)
        : dfSpace_(dfSpace),
          problem_(problem),
          discreteOperator_(dfSpace_, problem_)
    {
        
        // read operator components if desired
        if (Parameter::getValue<bool>("highdimOp.readData")) {
                boost::timer::auto_cpu_timer timer("Read detailed operator from file: %w seconds!\n");
                getOperator().readData();
        } else {        
            // assemble the operator
            {
                boost::timer::auto_cpu_timer timer("Assemble detailed operator: %w seconds!\n");
                discreteOperator_.assemble();
            }
            // save operator components if desired
            if (Parameter::getValue<bool>("highdimOp.saveData")) {
                boost::timer::auto_cpu_timer timer("Save detailed operator to file: %w seconds!\n");
                getOperator().saveData();
                
            }
        }        
    }

    void operator() (DofVectorType& solution) {

        // lookup solution: if computed already, just load it
        std::string symbolicParam = boost::lexical_cast<std::string>(problem_.mobility().getParameter());
        typename SolutionsMapType::const_iterator found = solutionsMap_.find(symbolicParam);
        if (found!=solutionsMap_.end()) {
            solution=found->second;
            return;
        }
        
      // configure solver
        double solverEps
          = Parameter::getValue<double>("algorithm.solver.eps", 1.0e-8);
        const int solverVerbose 
          = Parameter::getValue<int>("algorithm.solver.verbose", 0);
        const int maxIterations = 
            Parameter::getValue<int>("algorithm.solver.maxIterations", dfSpace_.size());
        double dropTol 
          = Parameter::getValue<double>("algorithm.solver.preconditioner.dropTol", Eigen::NumTraits<double>::dummy_precision());
        int fillFactor 
          = Parameter::getValue<int>("algorithm.solver.preconditioner.fillFactor", 10);
        solver_.setMaxIterations(maxIterations);
        solver_.setTolerance(solverEps);        
        solver_.preconditioner().setDroptol(dropTol);
        solver_.preconditioner().setFillfactor(fillFactor);        
  
    
        // get system matrix
        boost::timer::cpu_timer timer;
        Eigen::SparseMatrix<double>& systemMatrixEigen = discreteOperator_.systemMatrixEigen();
        if (solverVerbose)
            std::cout << "Get eigen system matrix: " << timer.elapsed().wall/1e9 << "s\n";
              
        // initialize solver
        timer.start();
        solver_.compute(systemMatrixEigen);
        if (solverVerbose) {
            std::cout << "Initialize high dim solver: " << timer.elapsed().wall/1e9 << "s\n";
        }

        // solve 
        timer.start();
        solution = solver_.solve(discreteOperator_.rightHandSideEigen());

        // print some information about solver convergence        
        if (solverVerbose) {
            std::cout << "Solve highdim with preconditioning:" << timer.elapsed().wall/1e9 << "s\n";
            std::cout << "#iterations:     " << solver_.iterations() << std::endl;
            std::cout << "estimated error: " << solver_.error()      << std::endl;
        }
        
        if (solver_.info()!=Eigen::Success)
            std::cout << "High dim solve was not successfull!\n";
        
        // save the solution in stash
        solutionsMap_.insert(PairType(symbolicParam, solution));
        
        return;
    }

    DiscreteOperatorType& getOperator() {
        return discreteOperator_;
    }

    const DiscreteOperatorType& getOperator() const {
        return discreteOperator_;
    }
    
    const DiscreteFunctionSpaceType& space() const {
        return dfSpace_;
    }
    
private:
    
    const DiscreteFunctionSpaceType& dfSpace_;
    const ProblemType& problem_;
    DiscreteOperatorType discreteOperator_;
    SolverType solver_;
    SolutionsMapType solutionsMap_;

};


}
}

#define USE_SUPERLU

#endif //__ALGORITHM__
