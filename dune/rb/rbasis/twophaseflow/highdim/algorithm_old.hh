#ifndef POISSON_SERIAL_HH
#define POISSON_SERIAL_HH

//***********************************************************************
/*! Poisson problem: 

  This is an example solving the poisson problem
  \f{displaymath}
  \begin{array}{rcll}
  -\triangle u &=& f & \quad \mbox{in}\ \Omega\\
             u &=& 0 & \quad \mbox{on}\ \partial\Omega
  \end{array}
  \f}
  with the finite element method using Lagrangian elements. The polynomial
  order is given by POLORDER.
  
  In this example, $\Omega = ]0,1[^{dimworld}$ and
  \f[
  f( x, y, z ) = 4 dimworld \pi^2 u(x,y,z) 
  \f]

  The exact solution to the poisson problem is then given by
  \f[
  u( x, y, z ) = \prod_{i=1}^{dimworld} sin( 2 \pi x_i).
  \f]
*/
//***********************************************************************

// this define enables the use of higher (>2) order Lagrange basis functions
#define USE_TWISTFREE_MAPPER

// System Includes
// ---------------
#include <iostream>
#include <sstream>


// DUNE Core Includes
// ------------------
#include <dune/common/version.hh>

// if this value is not defined, then we have version 1.1.1
#ifndef DUNE_VERSION_HH
#define OLD_DUNE_GRID_VERSION
#endif

#include <dune/common/stdstreams.hh>
#include <dune/common/timer.hh>

// DUNE-FEM includes
// -----------------

// grid parts 
#include <dune/fem/gridpart/gridpart.hh>

// adaptation classes 
#include <dune/fem/space/common/adaptmanager.hh>
// lagrange space 
#include <dune/fem/space/lagrangespace.hh>

// discrete functions 
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/function/vectorfunction.hh>

#include <dune/fem/operator/discreteoperatorimp.hh>

// matrix implementations 
#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
#include <dune/fem/operator/matrix/ontheflymatrix.hh>
//#include <dune/fem/operator/matrix/istlmatrix.hh>

// linear solvers 
#include <dune/fem/solver/inverseoperators.hh>
#include <dune/fem/solver/oemsolver/oemsolver.hh>
#include <dune/fem/solver/istlsolver.hh>

// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// parameter reading facilities 
#include <dune/fem/io/parameter.hh>

// header for data output for grape and VTK 
#include <dune/fem/io/file/datawriter.hh>

#include <dune/fem/misc/femeoc.hh>

// Local Includes
// --------------

// matrix assembler 
#include "fem.hh"


// Algorithm
// ---------

namespace Dune {
namespace RB {

template<class OperatorImp>
class Algorithm {



};

template <>
template<class DiscreteFunction, class MatrixTraits>   
class Algorithm<LaplaceFEOp<DiscreteFunction, MatrixTraits> >
{
public:              
  typedef LaplaceFEOp<DiscreteFunction, MatrixTraits> LaplaceOperatorType;
  typedef typename LaplaceOperatorType::GridType GridType;
  enum { polynomialOrder = LaplaceOperatorType::polynomialOrder };


  /** choose the grid partition (and hence the index set) to use
   *
   *  \note Not all index sets are continuous. The LeafIndexSet for AlbertaGrid,
   *        for example, is not. If you want to use OEM solvers, the index set
   *        must be continuous. In such a case use AdaptiveLeafGridPart.
   */
  //---- GridParts -----------------------------------------------------------
  typedef typename LaplaceOperatorType::GridPartType  GridPartType;
                                                                    
  //---- FunctionSpace -------------------------------------------------------
  //! define the function space, \f[ \R^n \rightarrow \R \f]
  // see dune/common/functionspace.hh
  typedef Dune::FunctionSpace< double, double, GridType::dimensionworld, 1 > FunctionSpaceType;

  //! to be revised 
  typedef Dune::ProblemInterface< FunctionSpaceType, GridPartType > ProblemType;

  // The data functions (as defined in problem.hh)
  //---- Right Hand Side, Exact Solution, and Stiffness tensor ---------------
  typedef typename ProblemType ::  ExactSolutionType   ExactSolutionType;

  //---- Adapter for exact solution ------------------------------------------
  typedef Dune::DiscreteFunctionAdapter< ExactSolutionType, GridPartType >
    GridExactSolutionType;

  //---- DiscreteFunctionSpace -----------------------------------------------
  //! define the discrete function space our unkown belongs to
  typedef Dune::LagrangeDiscreteFunctionSpace
    < FunctionSpaceType, GridPartType, polynomialOrder, Dune::CachingStorage >
    DiscreteSpaceType;

  //---- DiscreteFunction ----------------------------------------------------
  //---- good choice for adaptive simulations using OEM solver ---------------
  //! define the type of discrete function we are using
  typedef Dune::AdaptiveDiscreteFunction< DiscreteSpaceType > DiscreteFunctionType;

  //---- InverseOperator ----------------------------------------------------
  //---- good choice for build in CG solver ---------------------------------
  //! define the inverse operator we are using to solve the system 
  // typedef Dune::CGInverseOp< DiscreteFunctionType, LaplaceOperatorType >

  //typedef Dune::ISTLBICGSTABOp< DiscreteFunctionType, LaplaceOperatorType >
  //typedef ISTLCGOp< DiscreteFunctionType, LaplaceOperatorType >
  //typedef OEMCGOp<DiscreteFunctionType,LaplaceOperatorType>
  typedef Dune::OEMBICGSTABOp<DiscreteFunctionType,LaplaceOperatorType>
  //typedef Dune::OEMBICGSQOp<DiscreteFunctionType,LaplaceOperatorType>
  //typedef Dune::OEMGMRESOp<DiscreteFunctionType,LaplaceOperatorType>
  InverseOperatorType;


public:
  //! constructor                                                  /*@LST0S@*/
  Algorithm(const DiscreteSpaceType &space, const ProblemType& problem)
    : space_(space),
      gridPart_( space_.gridPart() ),
      problem_( problem )
  {
    // add entries to eoc calculation 
    std::vector<std::string> femEocHeaders;
    // we want to calculate L2 and H1 error (and EOC)
    femEocHeaders.push_back("$L^2$-error");
    femEocHeaders.push_back("$H^1$-error");                        /*@LST0E@*/

    // get eoc id 
    eocId_ = Dune::FemEoc::addEntry(femEocHeaders);

  }                                                                /*@LST0S@*/

  //! setup and solve the linear system
  void operator()(DiscreteFunctionType & solution)
  {                                                                /*@LST0E@*/
    // type of grid iterator 
    typedef typename DiscreteSpaceType :: IteratorType IteratorType; 
    // type of entity 
    typedef typename IteratorType :: Entity EntityType;

    // in verbose mode some info 
    if( Dune::Parameter :: verbose () ) 
    {
      std::cout << std :: endl << "Solving for " << space_.size()
        << " unkowns and polynomial order "
        << DiscreteSpaceType :: polynomialOrder << "." 
        << std :: endl << std :: endl;
    }

    // create adapter (for visualization with grape)
    GridExactSolutionType ugrid( "exact solution", problem_.exactSolution(), gridPart_,
                                 DiscreteSpaceType :: polynomialOrder + 1 );

    // initialize solution with zero                                 /*@LST0S@*/
    solution.clear();

    // create laplace assembler (is assembled on call of systemMatrix by solver)
    LaplaceOperatorType laplace( space_ , problem_ );               /*@\label{poi:laplaceDef}@*/

    // create right hand side
    DiscreteFunctionType rhs( "rhs", space_ );                      /*@\label{poi:rhsInit0}@*/ 

    // initialize as zero 
    rhs.clear();

    // setup right hand side 
    Dune::RightHandSideAssembler< DiscreteFunctionType >
      ::assemble ( problem_ , rhs , 2*polynomialOrder+1 ); 

    // set Dirichlet Boundary to exact solution
    bool hasDirichletBoundary = false;
    const IteratorType endit = space_.end();
    for( IteratorType it = space_.begin(); it != endit; ++it )
    {
      const EntityType &entity = *it;
      // in entity has intersections with the boundary adjust dirichlet nodes 
      if( entity.hasBoundaryIntersections() )
        hasDirichletBoundary |= boundaryTreatment( entity, problem_, rhs, solution );
    }                                                               /*@\label{poi:rhsInit1}@*/

    if( !hasDirichletBoundary )
    {
      clearMeanValue( rhs );
      setExactMeanValue( ugrid, solution );
    }

    // solve the linear system
    solve( laplace, rhs, solution );                                /*@\label{poi:solve}@*/ 
  }

  //! finalize computation by calculating errors and EOCs 
  void finalize(DiscreteFunctionType & solution)
  {
    // create exact solution 
    ExactSolutionType uexact( problem_ ); 

    // create grid function adapter 
    GridExactSolutionType ugrid( "exact solution", uexact, gridPart_,
                                 DiscreteSpaceType :: polynomialOrder + 1 );

    // create L2 - Norm 
    Dune::L2Norm< GridPartType > l2norm( gridPart_ );
    // calculate L2 - Norm 
    const double l2error = l2norm.distance( ugrid, solution );      /*@LST0E@*/
    std::cout << "L2-Error: " << l2error << std::endl;
    // create H1 - Norm 
    Dune::H1Norm< GridPartType > h1norm( gridPart_ );
    // calculate H1 - Norm 
    const double h1error = h1norm.distance( ugrid, solution );

    // store values 
    std::vector<double> errors;
    errors.push_back( l2error );
    errors.push_back( h1error );
    error_vec_.push_back(l2error);
    // submit error to the FEM EOC calculator 
    Dune::FemEoc :: setErrors(eocId_, errors);
  }                                                        /*@LST0S@*//*@LST0E@*/

  //! return reference to discrete space 
  const DiscreteSpaceType & space() {
    return space_;
  }

  const std::vector<double>& getErrors() {
    return error_vec_;
  }


private:                                                            /*@LST0S@*/
  //! set the dirichlet points to exact values
  template< class EntityType, class DiscreteFunctionType >
  bool boundaryTreatment( const EntityType &entity,
                          const ProblemType& problem,  
                          DiscreteFunctionType &rhs,
                          DiscreteFunctionType &solution )
  {                                                                 /*@LST0E@*/
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteSpaceType;
    typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;

    typedef typename DiscreteSpaceType :: LagrangePointSetType
      LagrangePointSetType;
    typedef typename DiscreteSpaceType :: GridPartType GridPartType;

    const int faceCodim = 1;
    typedef typename GridPartType :: IntersectionIteratorType
      IntersectionIteratorType;
    typedef typename LagrangePointSetType
      :: template Codim< faceCodim > :: SubEntityIteratorType
      FaceDofIteratorType;

    const DiscreteSpaceType &dfSpace = rhs.space();
    const GridPartType &gridPart = dfSpace.gridPart();

    // get local functions of data 
    LocalFunctionType rhsLocal = rhs.localFunction( entity );
    LocalFunctionType solutionLocal = solution.localFunction( entity );

    bool hasDirichletBoundary = false;

    typedef typename EntityType :: Geometry Geometry; 
    const Geometry& geo = entity.geometry();

    const LagrangePointSetType &lagrangePointSet = dfSpace.lagrangePointSet( entity );

    IntersectionIteratorType it = gridPart.ibegin( entity );
    const IntersectionIteratorType endit = gridPart.iend( entity );
    for( ; it != endit; ++it )
    {
      typedef typename IntersectionIteratorType :: Intersection IntersectionType;
      const IntersectionType& intersection = *it;

      // if intersection is with boundary, adjust data  
      if( intersection.boundary() && problem_.boundaryType(intersection)==ProblemType::Dirichlet )
      {
        hasDirichletBoundary = true;
        //typedef typename IntersectionType :: Geometry GlobalGeometryType;
        //const GlobalGeometryType& interGeometry = intersection.intersectionGlobal();

        // get face number of boundary intersection 
        const int face = intersection.indexInInside();
        // get dof iterators 
        FaceDofIteratorType faceIt
          = lagrangePointSet.template beginSubEntity< faceCodim >( face );
        const FaceDofIteratorType faceEndIt
          = lagrangePointSet.template endSubEntity< faceCodim >( face );
        for( ; faceIt != faceEndIt; ++faceIt )
        {
          // get local dof number 
          const int localDof = *faceIt;

          typedef typename FunctionSpaceType :: DomainType DomainType;
          // get global coordinate of point on boundary 
          const DomainType global = geo.global( lagrangePointSet.point( localDof ) );

          // evaluate boundary data
          typedef typename FunctionSpaceType :: RangeType RangeType;
          RangeType phi;
          problem.gD( global, phi );

          // adjust right hand side and solution data 
          rhsLocal[ localDof ] = phi[ 0 ];
          solutionLocal[ localDof ] = phi[ 0 ];
        }
      }
    }

    return hasDirichletBoundary;
  }                                                                 /*@LST0S@*/

  void clearMeanValue ( DiscreteFunctionType &rhs ) const
  {
    typedef typename DiscreteFunctionType::DofIteratorType DofIteratorType;

    typename FunctionSpaceType::RangeFieldType meanValue( 0 );

    const DofIteratorType dend = rhs.dend();
    for( DofIteratorType dit = rhs.dbegin(); dit != dend; ++dit )
      meanValue += *dit;
    meanValue /= rhs.size();

    std::cout << "Substracting mean value from right hand side: " << meanValue << std::endl;
    for( DofIteratorType dit = rhs.dbegin(); dit != dend; ++dit )
      *dit -= meanValue;
  }

  template< class GridFunctionType >
  void setExactMeanValue ( const GridFunctionType &ugrid, DiscreteFunctionType &solution ) const
  {
    typedef typename DiscreteSpaceType::IteratorType IteratorType;
    typedef typename DiscreteFunctionType::DofIteratorType DofIteratorType;

    typename FunctionSpaceType::RangeType meanValue( 0 );
    typename FunctionSpaceType::RangeFieldType volume( 0 );

    const IteratorType end = space_.end();
    for( IteratorType it = space_.begin(); it != end; ++it )
    {
      const typename IteratorType::Entity &entity = *it;
      const typename IteratorType::Entity::Geometry &geometry = entity.geometry();
      const typename GridFunctionType::LocalFunctionType ulocal = ugrid.localFunction( entity );

      typename LaplaceOperatorType::QuadratureType quadrature( entity, 2*polynomialOrder+1 );
      const int numQuadraturePoints = quadrature.nop();
      for( int qp = 0; qp < numQuadraturePoints; ++qp )
      {
        const typename FunctionSpaceType::RangeFieldType factor
          = quadrature.weight( qp ) * geometry.integrationElement( quadrature.point( qp ) );
        volume += factor;

        typename FunctionSpaceType::RangeType value;
        ulocal.evaluate( quadrature[ qp ], value );
        meanValue.axpy( factor, value );
      }
    }
    meanValue /= volume;

    std::cout << "Setting exact mean value: " << meanValue[ 0 ] << std::endl;
    const DofIteratorType dend = solution.dend();
    for( DofIteratorType dit = solution.dbegin(); dit != dend; ++dit )
      *dit = meanValue[ 0 ];
  }

  //! solve the resulting linear system
  void solve ( LaplaceOperatorType &laplace,
               const DiscreteFunctionType &rhs,
               DiscreteFunctionType &solution )
  {                                                                /*@LST0E@*/
    // create timer (also stops time) 
    Dune::Timer timer;

    // solve the linear system (with CG)
    const double dummy = 12345.67890;
    double solverEps = 1e-8;
    solverEps = Dune::Parameter :: getValue( "femhowto.solvereps", solverEps );

    // get verbosity information from Parameter class 
    const bool verbose = Dune::Parameter :: verbose();
    const bool solverVerbose = Dune::Parameter::getValue<bool>("fem.solver.verbose");
    // create inverse operator                                     /*@LST0S@*/
    InverseOperatorType cg(laplace, dummy, solverEps, std::numeric_limits<int>::max(), solverVerbose);

    // solve the system 
    cg( rhs, solution );                                            /*@\label{poi:cgExec}@*//*@LST0E@*/

    // get elapsed time 
    const double solveTime = timer.elapsed();

    // output in only in verbose mode (parameter fem.verbose)
    if( verbose ) 
      std :: cout << "Time needed by solver: " << solveTime << "s" << std :: endl;
  }                                                                /*@LST0S@*/

protected:
  const DiscreteSpaceType &space_;
  const GridPartType &gridPart_;
  const ProblemType& problem_;
  int eocId_;

private:
  std::vector<double> error_vec_;
};                                                                /*@LST0E@*/

} // namespace RB
} // namespace Dune

#endif // POISSONSERIAL_HH
