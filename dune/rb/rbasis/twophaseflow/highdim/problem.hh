#ifndef PROBLEM_HH
#define PROBLEM_HH

#include <cassert>
#include <cmath>
#include <sstream>

#include <dune/common/array.hh>

#include "probleminterface.hh"
#include <dune/rb/misc/parameter/parameter.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/solutionarraygenerator.hh>

//#include <dune/fem/misc/femtimer.hh>

namespace Dune
{

  //! exact solution for oscillating coefficient
  struct OscillatingCoefficient
  {
    typedef FunctionSpace< double, double, GRIDDIM, 1 > FunctionSpaceType;

    static const int dimDomain = FunctionSpaceType::dimDomain;

    typedef FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef FunctionSpaceType::RangeFieldType RangeFieldType;

    typedef FunctionSpaceType::DomainType DomainType;
    typedef FunctionSpaceType::RangeType RangeType;

    typedef FunctionSpaceType::JacobianRangeType JacobianRangeType;
    typedef FunctionSpaceType::HessianRangeType  HessianRangeType;

  public:
    OscillatingCoefficient(const double gridLeftBorder,
                           const double gridRightBorder)
      : eps_(Parameter::getValue<double>("problem.eps")),
        solutionArray_(eps_, Parameter::getValue<double>("problem.quadGridSize"),
                       gridLeftBorder, gridRightBorder),
        exactValues_(solutionArray_.funcValueArray()),
        ceps_(solutionArray_.ceps()),
        intervallLength_((gridRightBorder-gridLeftBorder)/solutionArray_.arraySize())
    {}

    void evaluate ( const DomainType &x, RangeType &ret ) const {
      const int index = std::floor(x[0]/intervallLength_);
      ret[0] = exactValues_[index];
    }

    void jacobian ( const DomainType &x, JacobianRangeType &grad ) const {
      grad[0][0] = (x[0]+ceps_)/(a0(x[0])*a1(x[0]/eps_));
      grad[0][1] = 0.0;
      return;
    }

  private:
    const double a0(double x) const {
      return (1.0+x);
    }

    const double a1(double x) const {
      double cosVal = cos(2.0*M_PI*x);
      return ((2.0/3.0)*(1+(cosVal*cosVal)));
    }

    const double                     eps_;
    const RB::SolutionArrayGenerator solutionArray_;
    const double*                    exactValues_;
    double                           ceps_;
    double                           intervallLength_;
  };


//#include "problemdefinitions/quadraticproblem.hh"
//#include "problemdefinitions/quarticproblem.hh"
//#include "problemdefinitions/cubicproblem.hh"
//#include "problemdefinitions/constantproblem.hh"
//#include "problemdefinitions/linearproblem.hh"
//#include "problemdefinitions/quadraticparameterdependentproblem.hh"
//#include "problemdefinitions/paramdependedwithexactsol.hh"
//#include "problemdefinitions/linearilyparamterdependentproblem.hh" 
//#include "problemdefinitions/polynomialparameterdependentproblem.hh"
//#include "problemdefinitions/oscillatingproblem.hh"
//#include "problemdefinitions/shockwave.hh"
//#include "problemdefinitions/oscillatingshockwave.hh"
//#include "problemdefinitions/oscillatinginclusions.hh"
//#include "problemdefinitions/spe10.hh"

  
} // namespace Dune
#include "problemdefinitions/groundwaterflow.h"
//#include "problemdefinitions/thermalblock.hh"


namespace Dune {

  template < class GridImp, class GridPartImp >
  static ProblemInterface < FunctionSpace < double, double, GridImp::dimensionworld, 1 > , GridPartImp >  *
  createProblemFromFunction( const int problemType, GridPartImp* gridPartPtr )
  {
    typedef FunctionSpace < double, double, GridImp::dimensionworld, 1 > FunctionSpaceType;
    switch( problemType )
    {
//    case 0: return new OscillatingProblem< GridImp > ();
//    case 1: return new QuadraticProblem< GridImp, FunctionSpaceType > ();
//    case 2: return new CubicProblem< GridImp, FunctionSpaceType > ();
//    case 3: return new QuarticProblem< GridImp, FunctionSpaceType > ();
//    case 4: return new ConstantProblem< GridImp, FunctionSpaceType > ();
//    case 5: return new LinearProblem< GridImp, FunctionSpaceType > ();
//    case 6: return new QuadraticParameterDependentProblem< GridImp, FunctionSpaceType > ();
//    case 7: return new PolynomialParameterDependentProblem< GridImp, FunctionSpaceType > ();
//    case 8: return new ParamDependedWithExactSol< GridImp, FunctionSpaceType > ();
//    case 9: return new LinearilyParamterDependentProblem< GridImp, FunctionSpaceType > ();
//    case 10: return new Shockwave< GridImp, FunctionSpaceType > ();
//    case 11: return new OscillatingShockwave< GridImp, FunctionSpaceType > ();
//    case 12: return new OscillatingInclusions< GridImp, FunctionSpaceType > ();
//    case 13: return new SPE10< GridImp, FunctionSpaceType > ();
//    case 14: return new ThermalBlock< GridImp, FunctionSpaceType > ();
    case 15: return new Groundwaterflow< GridImp, FunctionSpaceType, GridPartImp > (gridPartPtr);
    default: std::cerr << "Wrong value for problem type, bye bye!" << std::endl;
    }
    return 0;
  }


  template< class GridImp, class GridPartImp >
  static ProblemInterface<FunctionSpace< double, double, GridImp::dimensionworld, 1 >, GridPartImp>* 
  createProblem (GridPartImp* gridPartPtr = 0)
  {
    int problemFunction = 0; // default value 
    const std::string problemFunctionTable[] = { "sin", "cos", "alberta", "corner", "heter" };
    // read Function from parameter file 
    problemFunction =  Parameter::getEnum( "highdim.problemsolution", problemFunctionTable, problemFunction );

    // default value
    int problemType = 0;
    const std::string problemTypeTable[] = {  "oscillating", "quadratic",
                                              "cubic", "quartic", "constant", "linear",
                                              "quadraticparameterdependent",
                                              "polynomialparamterdependent",
                                              "parameterdependentwithexactsolution",
                                              "linearilyparameterdependent",
                                              "shockwave", "oscillatingshockwave",
      "oscillatinginclusions", "spe10", "thermalblock", "groundwaterflow"};
    // read problem type from parameter file 
    problemType = Parameter::getEnum( "highdim.problemtype", problemTypeTable, problemType );

    // see problem.hh for available problems 
    switch( problemFunction )
    {
      case 0: return createProblemFromFunction<GridImp> ( problemType, gridPartPtr );
      default: std::cerr << "Wrong problem value, bye, bye!" << std::endl;
               abort();
    }
    return 0;
  }

}

#endif // #ifndef PROBLEM_HH


