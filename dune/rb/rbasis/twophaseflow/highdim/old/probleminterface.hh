#ifndef DUNE_PROBLEMINTERFACE_HH
#define DUNE_PROBLEMINTERFACE_HH

#include <dune/fem/function/common/function.hh>

namespace Dune {

template <class FunctionSpaceImp, class GridPartImp >
class ProblemInterface
{
public:
  typedef FunctionSpaceImp                                           FunctionSpaceType;
  typedef ProblemInterface< FunctionSpaceType, GridPartImp >         ThisType;

  enum { dimDomain = FunctionSpaceType :: dimDomain };

  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  typedef typename FunctionSpaceType :: JacobianRangeType            JacobianRangeType;
  typedef typename FunctionSpaceType :: DomainFieldType              DomainFieldType;
  typedef typename FunctionSpaceType :: RangeFieldType               RangeFieldType;

  typedef FieldMatrix< RangeFieldType, dimDomain, dimDomain >        DiffusionMatrixType;
  enum BoundaryType {Dirichlet, Neumann};
  typedef typename GridPartImp::EntityType EntityType;
  
public:

  //! destructor
  virtual ~ProblemInterface() {}

  //! evaluates the right hand side (Function like bahavior)
  inline void evaluate(const DomainType& x, RangeType& ret) const
  {
    f(x , ret);
  }

  //! the right hand side
  virtual void f(const DomainType& x, RangeType& ret) const = 0;

  //! the exact solution
  virtual void u(const DomainType& x, RangeType& ret) const = 0;

  //! the diffusion matrix
  virtual void K(const DomainType& x, const EntityType& entity, DiffusionMatrixType& m) const =0;

  //! the diffusion matrix
  virtual void dxK(const DomainType& x, DiffusionMatrixType& m) const {
    m=0.0;
    return;
  }

  //! the diffusion matrix
  virtual void dyK(const DomainType& x, DiffusionMatrixType& m) const {
    m=0.0;
    return;
  }

  //! the mobility
  virtual void lambda(const DomainType &x, RangeType &lm) const {
    lm=1.0;
  }

  //! the n'th parameter-independent part of the mobility
  virtual void lambdaPart(const int n, const DomainType &x, RangeType &lm) const {
    lm[0] = 1.0;
  }
 
  //! the n'th parameter-independent part of the gradient of the mobility
  virtual void lambdaPartJacobian(const int n, const DomainType &x, JacobianRangeType &jac) const {
    jac[0]=0.0;
    return;
  }

  //! the i'th parameter
  virtual double l(const int i) const {
    return 1.0;
  }
  
  virtual const std::vector<double>& muLambda() const {
    return muLambda_;
  }

  virtual void muLambda(std::vector<double>& mu) const {
    mu = muLambda_;
    return; 
  }

  //! the number of parts of the mobility
  virtual int Nlambda() const { return 1; };

  //! the number of parts of the dirichlet values
  virtual unsigned int Ngd() const { 
    return 1;
  };

    //! the number of parts of the neumann values
  virtual unsigned int Ngn() const { 
    return 1;
  };
  
  //! the i'th parameter for the boundary values
  virtual double mugd(const unsigned int nu) const {
    return 1.0;
  }
  
  //! the i'th parameter for the boundary values
  virtual double mugn(const unsigned int nu) const {
    return 1.0;
  }

 //! the n'th parameter-independent part of the dirichlet boundary values
  virtual void gDPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
    u(x, ret);
    return;
  }
 
    //! the n'th parameter-independent part of the neumann boundary values
  virtual void gNPart(const unsigned int nu, const DomainType &x, RangeType &ret) const {
    u(x, ret);
    return;
  }
 
  //! returns true if diffusion coefficient is constant
  virtual bool constantK() const {
    return true;
  }
  
  virtual void visualizePermeability() const = 0;


  virtual double sigma() const { return Parameter::getValue<double>("rb.sigma"); }

  //! the Dirichlet boundary data function
  void gD(const DomainType& x, RangeType& ret) const
  {
    ret=0.0;
    for (int i=0; i!=int(Ngd()); ++i) {
      RangeType val(0.0);
      gDPart(i, x, val);
      val*=mugd(i);
      ret[0]+=val;      
    }
    return;
  }

  //! the Neumann boundary data function
  void gN(const DomainType& x,
                  RangeType& dn) const {
    dn[0]=0;
    for (int i=0; i!=int(Ngn()); ++i) {
      RangeType val(0.0);
      gNPart(i, x, val);
      val*=mugn(i);
      dn[0]+=val;
    }
    return;
  }
  
  /** \brief obtain type of boundary intersection
   *
   *  \param[in] intersection intersection whose boundary type shall be returned
   *  \returns boundary type of the intersection
   */
  template <class IntersectionType>
  BoundaryType boundaryType (const IntersectionType &intersection) const {
    const int boundaryID = intersection.boundaryId();
    if (boundaryID==1) {
      return Dirichlet;
    }
    else if (boundaryID==2) {
      return Neumann;
    }
    DUNE_THROW(NotImplemented, "The boundary ID you provided does not exist!");
  }

  //! the gradient of the exact solution
  virtual void gradient(const DomainType& x,
                        JacobianRangeType& grad) const = 0;

  //! return whether boundary is Dirichlet (true) or Neumann (false)
  virtual bool dirichletBoundary(const int bndId, const DomainType& x) const
  {
    return (bndId==1);
  }

  /** replace a parameter*/
  virtual void replaceParameter(const std::vector<double>& mu) {
    //    assert(mu.size()==muLambda_.size());
    muLambda_ = mu;
  }
  
  template<class FiniteVolumeFunctionImpl>
  void visualizePermeability(FiniteVolumeFunctionImpl& kx,
                             FiniteVolumeFunctionImpl& ky,
                             FiniteVolumeFunctionImpl& kz) {
    DUNE_THROW(NotImplemented, "Visualizing the permeability field is not implemented for this test case!");
  }

protected:
  std::vector<double> muLambda_;

protected:
  ProblemInterface() : exactSolution_( *this ) {}

  //! the exact solution to the problem for EOC calculation
  class ExactSolution
    : public Fem::Function< FunctionSpaceType, ExactSolution >
  {
  private:
    typedef Fem :: Function< FunctionSpaceType, ExactSolution >             BaseType;

  protected:
    const ThisType    &data_;

  public:
    inline ExactSolution ( const ThisType& data )
    : data_( data )
    {
    }

    inline void evaluate ( const DomainType &x, RangeType &ret ) const
    {
      data_.u( x, ret );
    }

    inline void jacobian ( const DomainType &x, JacobianRangeType &ret ) const
    {
      data_.gradient( x, ret );
    }

    inline void evaluate (const DomainType &x,
                          const double time, RangeType &phi ) const
    {
      evaluate( x, phi );
    }
  }; // end class ExactSolution

public:
  //! type of function converter for exact solution and gradient
  typedef ExactSolution ExactSolutionType;

protected:
  ExactSolutionType exactSolution_;

public:
  const ExactSolutionType& exactSolution() const { return exactSolution_; }
};

}
#endif  /*DUNE_PROBLEMINTERFACE_HH*/
