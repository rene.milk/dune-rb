#include <config.h>

// stl stuff
#include <vector>


// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>

// multidomaingrid
#include <dune/grid/multidomaingrid.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include "../rbgeneration.hh"
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>


using namespace Dune;
using namespace RB;

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                         GridType;
    typedef Dune::mdgrid::FewSubDomainsTraits<GridType::dimension,4>       MDGridTraits;
    typedef MultiDomainGrid<GridType, MDGridTraits>                        MDGridType;
    typedef LeafGridPart<MDGridType>                                       GridPart;
    typedef FunctionSpace<double, double, 2, 1>                            FunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpace, GridPart, 1>      DiscFuncSpace;
    typedef AdaptiveDiscreteFunction<DiscFuncSpace>                        DiscreteFunction;
    typedef ConstrainedDiscreteFunction<DiscreteFunction, MDGridType>      ConstrainedDF;
    typedef ConstrainedRBSpace<ConstrainedDF>                              RBSpace;
    typedef AdaptiveDiscreteFunction<RBSpace>                              ReducedFunction;
    typedef DiscFuncSpace::IteratorType                                    IteratorType;
    typedef MDGridType::Codim<0>::Entity                                   Entity;
    typedef RBGenerationTraits<RBSpace>                                    RBGenerationTraits;
    typedef RBGeneration<RBGenerationTraits>                               RBGeneration;

    // data output
    typedef Dune::Tuple<ConstrainedDF*>      IOTupleType;
    typedef DataWriter<MDGridType, IOTupleType> DataWriterType;

    MPIManager::initialize(argc, argv);
    Parameter::append("parameter");

    GridPtr<GridType> gridPtr("unitcube2.dgf");
    GridType &grid = *gridPtr;
    MDGridType mdGrid(grid);
    GridPart gridPart(mdGrid);
    // get some subdomains
    mdGrid.startSubDomainMarking();
    for (IteratorType it = gridPart.begin<0>(); it != gridPart.end<0>(); ++it) {
      const Entity& e = *it;
      typedef Dune::FieldVector<MDGridType::ctype,2> Vec;
      Vec local = Dune::GenericReferenceElements<MDGridType::ctype,2>::general(e.type()).position(0,0);
      Vec c = e.geometry().global(local);
      double x = c[0];
      double y = c[1];
      if (x<0.5) {
        if (y<0.5) {
          mdGrid.addToSubDomain(0,e);
        }
        else
          mdGrid.addToSubDomain(3,e);
      }
      else {
        if (y<0.5)
          mdGrid.addToSubDomain(1,e);
        else
          mdGrid.addToSubDomain(2,e);
      }
    }
    mdGrid.preUpdateSubDomains();
    mdGrid.updateSubDomains();
    mdGrid.postUpdateSubDomains();
    DiscFuncSpace discFuncSpace(gridPart);

    // now, get a reduced space
    RBSpace rbSpace(discFuncSpace);

    // RB generator
    RBGeneration rbGenerator(rbSpace);

    // add a initial base function to the space
    rbGenerator.init();
    std::cout << "RB Space size after init (should be 4): " << rbSpace.size() 
              << std::endl;
    // get a reduced function  
    ReducedFunction reduced("red", rbSpace);

    //= Visualize the results ===========================================
    /* One after another, we set the dofs of "reduced" to one (and the others
       zero), project the reduced function to the high dimensional function
       space and visualize the result. */

    ConstrainedDF temp("temp", discFuncSpace, mdGrid, -1);
    ConstrainedDF highDim("highDim", discFuncSpace, mdGrid, -1);
    IOTupleType highDimTuple(&highDim);
    DataWriterType highDimWriter(mdGrid, highDimTuple);

    for (int i = 0; i<4; ++i) {
      int j=0;
      for (ReducedFunction::DofIteratorType it = reduced.dbegin();
           it != reduced.dend(); ++it, ++j) {
        if (i==j)
          *it = 1.0;
        else
          *it = 0.0;
      }
      highDim.clear();
      temp.clear();
      temp.setSupport(i);
      // Project the reduced function to the high dimensional space
      rbSpace.project(reduced, temp);
      std::cout << temp;
      // we have to delete all non-support dofs in the high dim function
      restrictFunction(temp, highDim);
      std::cout << highDim;
      highDimWriter.write();
    }

  } catch (Dune::Exception e) {
    std::cerr << e.what();
  }  
  return 0;
}

