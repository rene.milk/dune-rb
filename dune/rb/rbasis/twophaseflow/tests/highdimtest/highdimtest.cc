#include <config.h>

// #define DUNE_RB_HIGHDIMISTL
#define USE_EIGEN
#define USE_SUPERLU

#include <dune/common/exceptions.hh>

// grid stuff
#include <dune/grid/common/gridinfo.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/groundwaterflow.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/parameterspace.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>


// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/rb/highdimoperator.hh>

using namespace Dune;
using namespace RB;

template <class DiscreteFunctionImpl>
struct StringTraits {
    typedef DiscreteFunctionImpl DiscreteFunctionType;
    typedef std::string          AttributeType;
};

#ifndef POLORDER
const int polynomialOrder = 1;
#else
const int polynomialOrder = POLORDER;
#endif /* ifndef POLORDER */

int main(int argc, char *argv[])
{
    try {

        typedef GridSelector::GridType                                                       GridType;
        typedef LeafGridPart<GridType>                                                       GridPartType;
        typedef FunctionSpace<double, double, GridType::dimension, 1>                        FunctionSpaceType;
        typedef DiscontinuousGalerkinSpace<FunctionSpaceType, GridPartType, polynomialOrder> DiscFuncSpaceType;
#ifdef DUNE_RB_HIGHDIMISTL
        typedef BlockVectorDiscreteFunction<DiscFuncSpaceType>                               DiscreteFunctionType;
        typedef ISTLMatrixTraits<DiscFuncSpaceType, DiscFuncSpaceType>                       HighDimMatrixTraitsType;
#else /* ifdef DUNE_RB_HIGHDIMISTL */
        typedef SparseRowMatrixTraits<DiscFuncSpaceType, DiscFuncSpaceType>                  HighDimMatrixTraitsType;
        typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                  DiscreteFunctionType;
#endif /* ifdef DUNE_RB_HIGHDIMISTL */
        typedef Groundwaterflow<1>                                                           ProblemType;
        typedef HighdimOperator<DiscreteFunctionType, HighDimMatrixTraitsType, ProblemType>  HighdimOperatorType;
        typedef Algorithm<HighdimOperatorType>                                               HighdimAlgorithm;

        MPIManager::initialize(argc, argv);
        Parameter::append("../highdimtest.params");

        GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));
        GridType &grid = *gridPtr;

            // print some information about the grid
        gridinfo(grid);
        GridPartType gridPart(grid);

        DiscFuncSpaceType discFuncSpace(gridPart);

            // create problem
        ProblemType problem;

            /* visualize the permeability as finite volume function */
        problem.visualizePermeability(gridPart);
        problem.visualizeMobility(gridPart);


            // if a full test is wanted
        if (Parameter::exists("paramspace.size")) {
            // get algorithm (assemble or load matrix)
            HighdimAlgorithm algorithm(discFuncSpace, problem);
            
                // get a parameter space
            std::vector<double> begin;
            std::vector<double> end;
            const int paramDimensionSize = Parameter::getValue<int>("paramspace.size");
            Parameter::get("paramspace.begin", begin);
            Parameter::get("paramspace.end", end);
            std::vector<int> numParamsVec(begin.size(), paramDimensionSize);
            TriangularParameterSpace<std::vector<double>, std::vector<int> >
            paramSpace(begin, end, numParamsVec);
            const int paramSpaceSize = paramSpace.size();

                // get func list to store detailed solutions
            typedef StringTraits<DiscreteFunctionType>         StringTraitsType;
            typedef DiscreteFunctionList_xdr<StringTraitsType> HighDimSolutionsListType;
            HighDimSolutionsListType highDimSolutions(discFuncSpace, "snapshots", "discFuncs");

                // compute and store high dimensional solution for all parameters in param space
            for (int k=0; k<paramSpaceSize; ++k) {
                std::vector<double> mu;
                paramSpace.getParameter(k, mu);
                problem.mobility().setParameter(mu);
                std::cout << "Set parameter to mu = [ ";
                for (unsigned int j=0; j!=mu.size(); ++j)
                    std::cout << mu[j] << " ";
                std::cout << "]\n";

                DiscreteFunctionType solution("solution", discFuncSpace);
                algorithm(solution);

                std::stringstream muAsString;
                for (int i=0; i!=int(mu.size()); ++i)
                    muAsString << mu[i];
                highDimSolutions.push_back(solution, muAsString.str());
            }
        } else {
            DiscreteFunctionType solution("solution", discFuncSpace);
            // test for different sigma values
            for (double sigma=30; sigma<35; sigma+=10) {
         //       Parameter::replaceKey("highdimOp.sigma", sigma);
                std::cout << "Sigma: " << Parameter::getValue<double>("highdimOp.sigma") << std::endl;

                // get algorithm (assemble or load matrix)
                HighdimAlgorithm algorithm(discFuncSpace, problem);
                
                algorithm(solution);
                plotFunction(solution);
            }
        }
    }
    catch (Exception& e) {
        std::cout << e.what();
        return 1;
    }
    catch (std::exception& e) {
        std::cout << e.what();
        return 1;
    }
    catch (...) {
        std::cout << "Don't know what the problem is!\n";
    }
    return 0;
} /* main */