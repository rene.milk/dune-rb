#include <config.h>
#include "../reducedoperator.hh"

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>


// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>

// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/solver/inverseoperators.hh>
// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include "../rbgeneration.hh"
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>
#include <dune/rb/rbasis/twophaseflow/rb/errorestimator/errorestimator.hh>



// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

using namespace Dune;
using namespace RB;

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                              GridType;
    typedef LeafGridPart<GridType>                                              GridPart;
    typedef MultiDomainGridPart<GridPart>                                       MDGridPart;
    typedef FunctionSpace<double, double, GridType::dimension, 1>               FunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpace, MDGridPart, 1>         DiscFuncSpace;
    typedef AdaptiveDiscreteFunction<DiscFuncSpace>                             DiscreteFunction;
    typedef ConstrainedDiscreteFunction<DiscreteFunction, MDGridPart>           ConstrainedDF;
    typedef ProblemInterface<FunctionSpace>                                     ProblemType;
    typedef ConstrainedRBSpace<ConstrainedDF>                                   RBSpace;
    typedef AdaptiveDiscreteFunction<RBSpace>                                   ReducedFunction;
    typedef DiscFuncSpace::IteratorType                                         IteratorType;
    typedef DiscFuncSpace::EntityType                                           Entity;
    typedef Dune::SparseRowMatrixTraits<RBSpace, RBSpace>                       MatrixObjectTraits;
    typedef ReducedOperator<ReducedFunction, MatrixObjectTraits>                LinearOperatorType;
    typedef ErrorEstimator<ReducedFunction, ProblemType>                        ErrorEstimatorType;
    typedef RBGenerationTraits<RBSpace, LinearOperatorType, ErrorEstimatorType> RBGenerationTraits;
    typedef RBGeneration<RBGenerationTraits>                                    RBGeneration;
    //---- InverseOperator ----------------------------------------------------
    //---- good choice for build in CG solver ---------------------------------
    //! define the inverse operator we are using to solve the system 
    typedef Dune::OEMBICGSTABOp<ReducedFunction, LinearOperatorType>            InverseOperatorType;

    MPIManager::initialize(argc, argv);
    Parameter::append("rboperatortest.params");

    GridPtr<GridType> gridPtr("unitcube2.dgf");
    GridType &grid = *gridPtr;
    // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("fem.globalRefine", 0));
    // print some information about the grid
    Dune::gridinfo(grid);
    MDGridPart mdGridPart(grid);
    Dune::VTKWriter<GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.write("grid");

    // get some subdomains
    const bool fourSubDomains = Parameter::getValue<bool>("grid.fourSubdomains");
    for (IteratorType it = mdGridPart.begin<0>(); it != mdGridPart.end<0>(); ++it) {
      const Entity& e = *it;
      typedef Dune::FieldVector<GridType::ctype,2> Vec;
      // get the barycenter
      Vec local = Dune::GenericReferenceElements<GridType::ctype,2>::general(e.type()).position(0,0);
      Vec c = e.geometry().global(local);
      double x = c[0];
      double y = c[1];
      if (fourSubDomains) {
        if (x<0.5) {
          if (y<0.5)
            mdGridPart.addToSubDomain(0,e);
          else
            mdGridPart.addToSubDomain(1,e);
        }
        else {
          if (y<0.5) {
            mdGridPart.addToSubDomain(2,e);
          }
          else {
            mdGridPart.addToSubDomain(3,e);
          }
        }
      }
      else {
        //        if (x<0.5) {
          mdGridPart.addToSubDomain(0,e);
        // }
        // else {
        //   mdGridPart.addToSubDomain(1,e);
        // }
      }
    }

    DiscFuncSpace discFuncSpace(mdGridPart);

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );
    
    // std::vector<MuRangeType> mu_ranges;
    // Dune::Parameter::get( "rb.mu_ranges", mu_ranges );

    // prepare the convergence table
    // std::ofstream file("errors.dat");
    // file << "{RB Size} {Mean L2 Error} {Mean H1 Error} {STDDeviation L2} "
    //      << "{STDDeviation H1} {Max L2 Error} {Min L2 Error}\n";

    // const unsigned int startSize = Parameter::getValue<int>("test.startRBSize");
    // const unsigned int maxSize = Parameter::getValue<int>("test.finalRBSize");
    // const unsigned int stepSize = Parameter::getValue<int>("test.rbStepSize");
    // for (unsigned int finalRBSize=startSize; finalRBSize<=maxSize; finalRBSize+=stepSize) {

    std::vector<double> l2errors;
    std::vector<double> h1errors;

    //Parameter::replaceKey("rb.size", finalRBSize);

    // now, get a reduced space
    RBSpace* rbSpace;
    // if (Parameter::getValue<bool>("rb.readBaseFuncsFromDisk")) {
    //   std::vector<std::string> listNames;
    //   Parameter::get("rb.listNames", listNames);
    //   rbSpace = new RBSpace(discFuncSpace, listNames);
    // } else {
    rbSpace = new RBSpace(discFuncSpace);
        
    // RB generator
    RBGeneration rbGenerator(*rbSpace, *problem);
        
    // add a initial base function to the space
    int rbinitTime = FemTimer::addTo("Initialization of RB");
    FemTimer::start(rbinitTime);
    rbGenerator.init();
    FemTimer::stop(rbinitTime);
                

    // if (Parameter::getValue<bool>("rb.plotBaseFunctions")) {
    //   ConstrainedDF baseFunction("baseFunction", discFuncSpace);
    //   GrapeDataDisplay<GridType> baseDisplay(grid);
    //   for (int i=0; i!=4; ++i) {
    //     const RBSpace::ConstrainedList &list = rbSpace->getFuncList(i);
    //     for (unsigned int j=0; j!=list.size(); ++j) {
    //       list.getFunc(j, baseFunction);
    //       baseDisplay.addData(baseFunction);            
    //     }
    //   }
    //   baseDisplay.display();
    // }

    // get a reduced operator
    int assemblyTime = FemTimer::addTo("Assemblation of reduced matrix");
    FemTimer::start(assemblyTime);
    LinearOperatorType* rbOp = new LinearOperatorType(*rbSpace, *problem);
    rbOp->assemble();
    FemTimer::stop(assemblyTime);
    if (Parameter::getValue<bool>("rb.verbose"))
      rbOp->print(std::cout);
   
    // create inverse operator
    const double dummy = 12345.67890;
    double solverEps = 1e-8;
    const bool solverVerbose = Parameter::getValue<bool>("fem.solver.verbose");
    InverseOperatorType cg(*rbOp, dummy, solverEps, std::numeric_limits<int>::max(), solverVerbose);

    ReducedFunction solution("solution", *rbSpace);
    solution.clear();
    std::cout << "Perfoming reduced solution for the base function itsself!\n";
    cg(rbOp->rightHandSide(), solution);
    std::cout << "The following should be one!\n";
    solution.print(std::cout);

    Parameter::replaceKey("test.param0", 1.0);
    Parameter::replaceKey("test.param1", 0.0);
    Parameter::replaceKey("test.param2", 1.0);
    rbGenerator.extend();

    delete rbOp;
    rbOp = new LinearOperatorType(*rbSpace, *problem);
    rbOp->assemble();
    InverseOperatorType cg2(*rbOp, dummy, solverEps, std::numeric_limits<int>::max(), solverVerbose);


    std::vector<double> simParams;
    Parameter::get("simulation.parameters", simParams);
    std::cout << "Set parameters for simulation: mu=[";
    for (unsigned int i=0; i!=simParams.size(); ++i)
      std::cout << simParams[i] << " ";
    std::cout << "]\n";
    for (unsigned int param = 0; param!=problem->Nlambda(); ++param) {
      std::stringstream paramName;
      paramName << "test.param" << param;
      Parameter::replaceKey(paramName.str(), simParams[param]);
    }

    // solve the reduced system
    ReducedFunction solutionLarge("solution", *rbSpace);
    solutionLarge.clear();
    int reducedSolutionTime = FemTimer::addTo("Reduced Solution");
    //        std::cout << "Starting reduced simulation!\n";
    FemTimer::start(reducedSolutionTime);
    cg2(rbOp->rightHandSide(), solutionLarge);
    FemTimer::stop(reducedSolutionTime);

    solutionLarge.print(std::cout);

    // reconstruct the reduced solution + visualize
    int reconstructionTime = FemTimer::addTo("Reconstruction");
    std::stringstream redSolName;
    redSolName << "reduced_baseFuncs";
    ConstrainedDF recons(redSolName.str(), discFuncSpace, mdGridPart, -1);
    FemTimer::start(reconstructionTime);
    rbSpace->project(solutionLarge, recons);
    FemTimer::stop(reconstructionTime);
    // grapeDisp.addData(recons);

    // compute the matching high dim solution + visualize
    std::stringstream highDimName;
    highDimName << "HighDimSol_baseFuncs";
    DiscreteFunction highDimSolution(highDimName.str(), discFuncSpace);
    rbGenerator.detailedSimulation(highDimSolution);
    // grapeDisp.addData(highDimSolution);

    // // compute the difference function and plot it
    // DiscreteFunction difference(highDimSolution);
    // difference -=recons;
    // grapeDisp.addData(difference, "difference", 0.0);

    // if (Parameter::getValue<bool>("fem.io.grapedisplay"))
    //   grapeDisp.display();


    // compute errors
    // create L2 - Norm 
    Dune::L2Norm<MDGridPart> l2norm(mdGridPart);
    // calculate L2 - Norm 
    double l2error = l2norm.distance( highDimSolution, recons );      /*@LST0E@*/
    l2error /= l2norm.norm(highDimSolution);
    // create H1 - Norm 
    Dune::H1Norm<MDGridPart> h1norm(mdGridPart);
    // calculate H1 - Norm 
    double h1error = h1norm.distance( highDimSolution, recons );
    h1error /= l2norm.norm(highDimSolution);

    std::cout << "L2Error: " << l2error << " H1Error: " << h1error
              << std::endl;

    // l2errors.push_back(l2error);
    // h1errors.push_back(h1error);
    //        FemTimer::print(std::cout);
    //} // loop over all test mus
    // file << finalRBSize << " " << mean(l2errors) << " " << mean(h1errors)
    //      << " " << stddeviation(l2errors) << " " << stddeviation(h1errors)
    //      << " " << *std::max_element(l2errors.begin(), l2errors.end())
    //      << " " << *std::min_element(l2errors.begin(), l2errors.end())
    //      << std::endl;

    //    } // loop over rbSizes
    //    file.close();

    delete rbOp;

  } catch (Dune::Exception e) {
    std::cerr << e.what();
    return 1;
  }  
  return 0;
}
