#include <config.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>


// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>
// index mapper
#include <dune/grid/common/universalmapper.hh>


// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>
#include <dune/fem/misc/femtimer.hh>
#include <dune/fem/misc/gridwidth.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/problem.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/parameterspace.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>

// stuff stuff
#include <dune/stuff/profiler.hh>

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

// local includes
#include "../errorestimator/errorestimator.hh"
#include "../rbgeneration.hh"


using namespace Dune; 
using namespace RB;

const int polynomialOrder = POLORDER;

int main(int argc, char *argv[])
{
  
  try {
    typedef GridSelector::GridType                                                        GridType;
    typedef LeafGridPart<GridType>                                                        GridPartType;
    typedef Dune::RB::MultiDomainGridPart<GridPartType>                                   MDGridPartType;
    typedef FunctionSpace<double, double, GridType::dimension, 1>                         FunctionSpaceType;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpaceType, MDGridPartType, polynomialOrder> DiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                   DiscreteFunctionType;
    typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType>             ConstrainedDFType;
    typedef ConstrainedRBSpace<ConstrainedDFType>                                         RBSpaceType;
    typedef AdaptiveDiscreteFunction<RBSpaceType>                                         ReducedFunctionType;
    typedef DiscreteFunctionList_xdr<ReducedFunctionType>                                 ReducedListType;
    typedef DiscFuncSpaceType::IteratorType                                               IteratorType;
    typedef GridType::Codim<0>::EntityPointer                                             EntityPointerType;
    typedef DiscFuncSpaceType::EntityType                                                 EntityType;
    typedef Dune::SparseRowMatrixTraits<RBSpaceType, RBSpaceType>                         MatrixObjectTraitsType;
    typedef ReducedOperator<ReducedFunctionType, MatrixObjectTraitsType>                  ReducedOperatorType;
    typedef ProblemInterface<FunctionSpaceType>                                           ProblemType;
    typedef ErrorEstimator<ReducedFunctionType, ProblemType>                              ErrorEstimatorType;
    typedef RBGenerationTraits<RBSpaceType, ReducedOperatorType, ErrorEstimatorType>      RBGenerationTraitsType;
    typedef RBGeneration<RBGenerationTraitsType>                                          RBGeneratorType;



    MPIManager::initialize(argc, argv);
    Parameter::append("testerrorestimator2.params");

    GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));
    GridType &grid = *gridPtr;

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    // prepare file for output
    std::stringstream filename;
    boost::uuids::uuid uid = boost::uuids::random_generator()();
    filename << "errorestimatortest_" << uid;
    std::ofstream file((filename.str()+".log").c_str());
    assert(file);
    file << "Errorestimator Test\n"
         << "-------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "All errors are absolute errors!\n\n";

    file << "{Grid size}\t{Grid Width}\t{L2 Error Ex}\t{EOC Ex (L2)}\t{H1 Error Ex}\t{EOC Ex(H1)}"
         << "\t{Error Est}\t{EOC Est}\n";

    Parameter::write((filename.str()+".paramlog").c_str());

    std::vector<double> errors;

    std::cout << "Performing reduced simulation and evaluating error"
              << " estimator for all parameters in the test set!";

    std::vector<double> l2errorsExact;
    std::vector<double> h1errorsExact;
    std::vector<double> errorsEstimated;

    // refine the grid once, such that four subdomains are
    // possible right from the beginning
    grid.globalRefine(1);


    // grid refinement loop
    for (int gridRefine=0; gridRefine!=8; ++gridRefine) {

      grid.globalRefine(1);
      MDGridPartType mdGridPart(grid);

      // get some subdomains
      const bool fourSubDomains = Parameter::getValue<bool>("grid.fourSubdomains");
      for (IteratorType it = mdGridPart.begin<0>(); it != mdGridPart.end<0>(); ++it) {
        const EntityType& e = *it;
        typedef Dune::FieldVector<GridType::ctype,2> Vec;
        // get the barycenter
        Vec local = Dune::GenericReferenceElements<GridType::ctype,2>::general(e.type()).position(0,0);
        Vec c = e.geometry().global(local);
        double x = c[0];
        double y = c[1];
        if (fourSubDomains) {
          if (x<0.25) 
            mdGridPart.addToSubDomain(0,e);
          else { 
            if (x<0.5)
              mdGridPart.addToSubDomain(1,e);
            else {
              if (x<0.75)
                mdGridPart.addToSubDomain(2,e);
              else 
                mdGridPart.addToSubDomain(3,e);
            }
          }
        }
        else {
          if (x<0.5)
            mdGridPart.addToSubDomain(0,e);
          else
            mdGridPart.addToSubDomain(1,e);
        }
      }

      DiscFuncSpaceType discFuncSpace(mdGridPart);
      RBSpaceType rbSpace(discFuncSpace);

      RBGeneratorType rbGenerator(rbSpace, *problem);

      DiscreteFunctionType highDimSol("highDimSol", discFuncSpace);
      highDimSol.clear();
      rbGenerator.detailedSimulation(highDimSol);

      // set the high dim solution as base function
      rbGenerator.init(highDimSol);

      // set all dofs of reduced function to one
      ReducedFunctionType reducedSol("reducedSol", rbSpace);
      for (int i=0; i!=rbSpace.size(); ++i)
        reducedSol.dof(i)=1.0;

      rbGenerator.updateErrorEstimator();
      double estimatedError = rbGenerator.evaluateErrorEstimator(reducedSol);
      errorsEstimated.push_back(estimatedError);

      DiscreteFunctionAdapter< ProblemType::ExactSolutionType, MDGridPartType >
        exactSolutionDiscrete("exact solution", problem->exactSolution(), mdGridPart, polynomialOrder+1);

      // visualize
      // GrapeDataDisplay<GridType> grapeDisp(grid);
      // // grapeDisp.addData(highDimSolution);
      // // grapeDisp.addData(exactSolutionDiscrete);
      // grapeDisp.display();

      // compute errors
      // create L2 - norm 
      Dune::L2Norm<MDGridPartType> l2norm(mdGridPart);
      // calculate L2 - error 
      const double l2errorExact = l2norm.distance(exactSolutionDiscrete, highDimSol);
      l2errorsExact.push_back(l2errorExact);
      // create H1 - norm 
      Dune::H1Norm<MDGridPartType> h1norm(mdGridPart);
      // calculate H1 - error
      const double h1errorExact = h1norm.distance(exactSolutionDiscrete, highDimSol);
      h1errorsExact.push_back(h1errorExact);

      // calc grid width
      double gridWidth = sqrt(2.0)*std::pow(0.5,gridRefine+1);
      // calculate the eoc
      double eocl2 = std::numeric_limits<double>::signaling_NaN();
      double eoch1 = std::numeric_limits<double>::signaling_NaN();
      double eocEstimated = std::numeric_limits<double>::signaling_NaN();
      if (gridRefine>0) {
        eocl2 = (log(l2errorsExact[gridRefine-1]/l2errorsExact[gridRefine])/log(2.0));
        eoch1 = (log(h1errorsExact[gridRefine-1]/h1errorsExact[gridRefine])/log(2.0));
        eocEstimated = (log(errorsEstimated[gridRefine-1]/errorsEstimated[gridRefine])/log(2.0));
      }

      // output to std::cout 
      // std::cout << "Grid Size: " << grid.size(0) << "\tGrid Width: " << gridWidth 
      //           << "\tL2 Error: " << l2error << "\tEOC (L2): " << eocl2
      //           << "\tH1Error: " << h1error <<  "\tEOC (H1): " << eoch1 
      //           << "\tTime: " << usedTime << "\n";

      // output to data file
      file << grid.size(0) << "\t" << gridWidth << "\t" 
           << l2errorExact << "\t" << eocl2 << "\t"
           << h1errorExact << "\t" << eoch1 << "\t"
           << estimatedError << "\t" << eocEstimated << "\n";
      file.flush();
    }


    delete problem;
    file.close();  
  } catch (Dune::Exception e) {
  std::cerr << e.what() << std::endl;
  return 1;
 }  
  
return 0;
}
