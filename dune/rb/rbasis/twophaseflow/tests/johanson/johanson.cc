#include <config.h>

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

#include <dune/common/exceptions.hh>

// grid stuff
// #include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/CpGrid.hpp>
// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/highdim/problemdefinitions/johanson.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/rb/rbgeneration.hh>

#include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>
#include <dune/rb/grid/coarse/multidomain.hh>

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

// boost stuff
#include <boost/exception/all.hpp>

namespace Dune {
namespace Capabilities {
template< class Grid >
struct hasHierarchicIndexSet;

template< >
struct hasHierarchicIndexSet< CpGrid >
{
  static const bool v = false;
};
}
}


using namespace Dune;
using namespace RB;

template< class VectorType, class OutputStream >
void computeProfileData(VectorType& data, OutputStream& out, const std::string& name)
{
  std::sort(data.begin(), data.end());
  double meanVal           = mean(data);
  double stdDeviationOfVal = stddeviation(data);
  double maxVal            = *(data.rbegin());
  double minVal            = *(data.begin());
  if (!(out.is_open()))
    throw std::runtime_error("Could not write profile data to output stream!\n");
  out << "Max " << name << ": " << maxVal
      << "\nMin " << name << ": " << minVal
      << "\nMean " << name << ": " << meanVal
      << "\nStandard deviation of " << name << ": " << stdDeviationOfVal;
  return;
} /* computeProfileData */

#ifndef POLORDER
const int polynomialOrder = 1;
#else
const int polynomialOrder = POLORDER;
#endif /* ifndef POLORDER */

int main(int argc, char* argv[])
{
  try {
    // Py_Initialize();
    // PySys_SetArgv(argc, argv);

    typedef CpGrid                                                                           GridType;
    // typedef GridSelector::GridType                                               GridType;
    typedef LeafGridPart< GridType >                                                         GridPartType;
    typedef Dune::RB::Grid::Coarse::Multidomain< GridPartType >                              MDGridPartType;
    typedef FunctionSpace< double, double, GridType::dimension, 1 >                          FunctionSpaceType;
    typedef DiscontinuousGalerkinSpace< FunctionSpaceType, MDGridPartType, polynomialOrder > DiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction< DiscFuncSpaceType >                                    DiscreteFunctionType;
    typedef ConstrainedRBSpace< DiscFuncSpaceType >                                          RBSpaceType;

    typedef Johanson< GridType, 1 >                                                          ProblemType;
    typedef ProblemType::ParameterDomainType                                                 ParameterDomainType;
    typedef RBGeneration< RBSpaceType, DiscreteFunctionType, ProblemType >                   RBGeneratorType;

    MPIManager::initialize(argc, argv);
    Parameter::append("../johanson.params");

    CpGrid grid;
    grid.readEclipseFormat("/Users/svenkaulmann/dune_2_1/dune-rb/dune/rb/rbasis/twophaseflow/grid/johanson.grdecl",
                           0.0,
                           false,
                           false);

    // const array<int, 3> dims = {10, 10, 10};
    // const array<double, 3> cellsize = {1, 1, 1};
    // grid.createCartesian(dims, cellsize);

    std::cout << grid.leafIndexSet().size(0) << "\n";std::cout.flush();
    
    VTKWriter< CpGrid::LeafGridView > vtkwriter(grid.leafView());
    vtkwriter.write("grid");

    // print some information about the grid
    gridinfo(grid);
    MDGridPartType mdGridPart(grid);

    SubdomainMarker sdMarker;
    sdMarker.readSubdomains();
    sdMarker.markSubdomains(mdGridPart);

    DiscFuncSpaceType discFuncSpace(mdGridPart);

    // create problem
    ProblemType problem(grid);

    /* visualize the permeability as finite volume function */
    problem.visualizePermeability(grid);
//    problem.visualizeMobility(grid);

    // now, get a reduced space
    RBSpaceType rbSpace(discFuncSpace);
    bool        readFromDisk = Parameter::getValue< bool >("reducedspace.read", false);
    if (readFromDisk) {
      rbSpace.readBaseFunctions();
      std::cout << "Base functions read!\n";
    }

    // instantiate RB generator
    RBGeneratorType rbGenerator(rbSpace, problem, mdGridPart);

    if (readFromDisk && Parameter::getValue< bool >("podgreedy.applyFinalPOD", false)) {
      rbGenerator.readSnapshots();
      rbGenerator.compressInformation();
      // rbGenerator.updateErrorEstimatorFull();
    }

    if (!readFromDisk) {
      // extend the reduced basis
      rbGenerator.init();
      rbGenerator.greedyExtension();
      rbGenerator.saveSnapshots();
    }
    // rbGenerator.plotBaseFunctions();


    if (Parameter::getValue< bool >("reducedspace.save", false))
      rbSpace.saveBaseFunctions();

    // assemble reduced operator
    rbGenerator.updateReducedOperator();
    std::cout << "Reduced operator assembled!\n";

    // get a parameter space
    ParameterDomainType begin;
    ParameterDomainType end;
    const int           paramDimensionSize = Parameter::getValue< int >("testing.numParams");
    Parameter::get("podgreedy.paramspace.begin", begin);
    Parameter::get("podgreedy.paramspace.end", end);

    TriangularRandomParameterSpace< ParameterDomainType > paramSpace(begin, end, paramDimensionSize, 29072011);


    const int paramSpaceSize = paramSpace.size();

    // prepare file for output
    std::stringstream  filename;
    boost::uuids::uuid uid = boost::uuids::random_generator() ();
    filename << "lowdimschemetest_" << uid;
    std::ofstream file((filename.str() + ".log").c_str());
    assert(file);
    file << "Low dim scheme test\n"
         << "-------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "All errors are relative errors!\n";

    std::vector< double > errors;
    std::vector< double > highdimRuntimes(paramSpaceSize);
    std::vector< double > lowdimRuntimes(paramSpaceSize);
    std::vector< double > reconsTimes(paramSpaceSize);
    std::vector< double > trueErrors(paramSpaceSize);
    std::vector< double > upperBounds(paramSpaceSize);
    std::vector< double > lowerBounds(paramSpaceSize);

    Parameter::write((filename.str() + ".paramlog").c_str());

    // lets see if the user wants to test the basis
    bool testWanted = true;

    // std::cout << "Would you like to test the basis? (0/1) ";
    // std::cin >> testWanted;

    std::stringstream errorFile;
    errorFile << "errorBounds_" << uid;
    std::ofstream errorBoundsFile((errorFile.str() + ".log").c_str());
    assert(errorBoundsFile);


    if (testWanted) {
      std::cout << "Performing reduced simulation and computing error to highdim solution"
                << " for all test parameters!\n";

      Eigen::MatrixXd detailedSolutions(discFuncSpace.size(), paramSpaceSize);
      if (Parameter::getValue< bool >("testing.readHighDim"))
        detailedSolutions.loadFromBinaryFile("highdimsolutionsForTestSet.dat");

      boost::progress_display testingBar(paramSpaceSize);
      for (int k = 0; k < paramSpaceSize; ++k, ++testingBar) {
        ParameterDomainType mu;
        paramSpace.getParameter(k, mu);
        std::cout << "Testing for mu=[" << mu.transpose() << "]\n";

        Eigen::VectorXd solution;
        if (Parameter::getValue< bool >("testing.readHighDim"))
          solution = detailedSolutions.col(k);
        else {
          problem.mobility().setParameter(mu);
          rbGenerator.detailedSimulation(solution);
          if (Parameter::getValue< bool >("testing.saveHighDim"))
            detailedSolutions.col(k) = solution;
        }

        // rbGenerator.getErrorHighdim(mu, highdimRuntimes[k], lowdimRuntimes[k], reconsTimes[k],
        // trueErrors[k], upperBounds[k], lowerBounds[k]);
        rbGenerator.getErrorHighdim(mu, highdimRuntimes[k], lowdimRuntimes[k], reconsTimes[k],
                                    trueErrors[k], upperBounds[k], lowerBounds[k], solution);

        errorBoundsFile << lowerBounds[k] << "," << trueErrors[k] << "," << upperBounds[k] << "\n";
      }

      // plot the error bounds
      // try {
      // boost::python::class_<std::vector<double> >("PyVec")
      // .def(boost::python::vector_indexing_suite<std::vector<double> >());
      // boost::python::object mymodule = boost::python::import("mymodule");
      // boost::python::object dict = mymodule.attr("__dict__");
      // boost::python::object plot = dict["plotErrorBars"];

      // plot(lowerBounds, trueErrors, upperBounds, std::string(filename.str()+".png"));
      // } catch (boost::python::error_already_set&) {
      // PyErr_Print();
      // boost::python::object sys(boost::python::handle<>(PyImport_ImportModule("sys")));
      // boost::python::object err = sys.attr("stderr");
      // std::string err_text = boost::python::extract<std::string>(err.attr("getvalue")());
      // std::cout << "PythonError: " << err_text << endl;
      // std::cerr << "Plotting the error with bounds did not work!\n";
      // }

      if (Parameter::getValue< bool >("testing.saveHighDim"))
        detailedSolutions.saveToBinaryFile("highdimsolutionsForTestSet.dat");

      computeProfileData(trueErrors, file, "true errors");
      computeProfileData(lowerBounds, file, "lower bounds");
      computeProfileData(upperBounds, file, "upper bounds");
      file << "\nSize of test param set: " << paramSpaceSize;
      for (int i = 0; i != int(mdGridPart.numSubdomains()); ++i)
        file << "\nSize of RB in SD " << i << ": "
             << rbSpace.getSubdomainBasis(i).cols();

      computeProfileData(highdimRuntimes, file, "highdimruntimes (ms)");
      file << std::endl;
      computeProfileData(lowdimRuntimes, file, "lowdimruntimes (mus)");
      file << std::endl;
      computeProfileData(reconsTimes, file, "reconstimes (ms)");
    }     // if test wanted
          // lets see if the user wants to plot
    // bool plotWanted = false;
    // //        std::cout << "Would you like to plot a function? (0/1) ";
    // //        std::cin >> plotWanted;
    // while (plotWanted) {
    // ParameterDomainType mu(begin.rows());
    // for (int i=0; i!=int(begin.rows()); ++i) {
    // std::cout << std::endl;
    // std::cout << "Please enter component " << i << " of parameter! ";
    // double val;
    // std::cin >> val;
    // mu(i)=val;
    // }
    // problem.mobility().setParameter(mu);
    // rbGenerator.plotError(false);
    // std::cout << "Would you like to plot another function? (0/1) ";
    // std::cin >> plotWanted;
    // }

    file.close();

    // Py_Finalize();
  } catch (const Exception& e) {
    std::cout << e.what();
    return 1;
  } catch (const std::exception& e) {
    std::cout << e.what();
    return 1;
  }
  // catch (boost::python::error_already_set&) {
  // PyErr_Print();
  // boost::python::object sys(boost::python::handle<>(PyImport_ImportModule("sys")));
  // boost::python::object err = sys.attr("stderr");
  // std::string err_text = boost::python::extract<std::string>(err.attr("getvalue")());
  // std::cout << "PythonError: " << err_text << endl;
  // return 1;
  // }
  catch (...) {
    std::cout << "Don't know what the problem is!\n";
    return 1;
  }
  return 0;
} /* main */