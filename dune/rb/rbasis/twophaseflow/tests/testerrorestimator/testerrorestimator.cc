#include <config.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>


// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>
// index mapper
#include <dune/grid/common/universalmapper.hh>


// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>
#include <dune/fem/misc/femtimer.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/problem.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/parameterspace.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>

// stuff stuff
#include <dune/stuff/profiler.hh>

// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

// local includes
#include "../errorestimator/errorestimator.hh"
#include "../rbgeneration.hh"


using namespace Dune; 
using namespace RB;

const int polynomialOrder = POLORDER;

int main(int argc, char *argv[])
{
  
  try {
    typedef GridSelector::GridType                                                        GridType;
    typedef LeafGridPart<GridType>                                                        GridPartType;
    typedef Dune::RB::MultiDomainGridPart<GridPartType>                                   MDGridPartType;
    typedef FunctionSpace<double, double, GridType::dimension, 1>                         FunctionSpaceType;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpaceType, MDGridPartType, polynomialOrder> DiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                   DiscreteFunctionType;
    typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType>             ConstrainedDFType;
    typedef ConstrainedRBSpace<ConstrainedDFType>                                         RBSpaceType;
    typedef AdaptiveDiscreteFunction<RBSpaceType>                                         ReducedFunctionType;
    typedef DiscreteFunctionList_xdr<ReducedFunctionType>                                 ReducedListType;
    typedef DiscFuncSpaceType::IteratorType                                               IteratorType;
    typedef GridType::Codim<0>::EntityPointer                                             EntityPointerType;
    typedef DiscFuncSpaceType::EntityType                                                 EntityType;
    typedef Dune::SparseRowMatrixTraits<RBSpaceType, RBSpaceType>                         MatrixObjectTraitsType;
    typedef ReducedOperator<ReducedFunctionType, MatrixObjectTraitsType>                  ReducedOperatorType;
    typedef ProblemInterface<FunctionSpaceType>                                           ProblemType;
    typedef ErrorEstimator<ReducedFunctionType, ProblemType>                              ErrorEstimatorType;
    typedef RBGenerationTraits<RBSpaceType, ReducedOperatorType, ErrorEstimatorType>      RBGenerationTraitsType;
    typedef RBGeneration<RBGenerationTraitsType>                                          RBGeneratorType;



    MPIManager::initialize(argc, argv);
    if (argc!=2)
      DUNE_THROW(IOError, "Please give the name of the parameter file used for building the reduced basis as argument!");
    // argv should contain all the important info like number of subdomains,
    // storage location of the rb, etc
    Parameter::append(argv[1]);
    // error estimator params contain the size of the test set
    Parameter::append("testerrorestimator.params");

    GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));
    GridType &grid = *gridPtr;

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    grid.globalRefine(Parameter::getValue<int>("fem.globalRefine", 0));
      
    MDGridPartType mdGridPart(grid);

    Dune::VTKWriter<GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.write("grid");

    // get some subdomains
    const bool fourSubDomains = Parameter::getValue<bool>("grid.fourSubdomains");
    for (IteratorType it = mdGridPart.begin<0>(); it != mdGridPart.end<0>(); ++it) {
      const EntityType& e = *it;
      typedef Dune::FieldVector<GridType::ctype,2> Vec;
      // get the barycenter
      Vec local = Dune::GenericReferenceElements<GridType::ctype,2>::general(e.type()).position(0,0);
      Vec c = e.geometry().global(local);
      double x = c[0];
      double y = c[1];
      if (fourSubDomains) {
        if (x<2.5) 
          mdGridPart.addToSubDomain(0,e);
        else { 
          if (x<5)
            mdGridPart.addToSubDomain(1,e);
          else {
            if (x<7.5)
              mdGridPart.addToSubDomain(2,e);
            else 
              mdGridPart.addToSubDomain(3,e);
          }
        }
      }
      else {
        if (x<5)
          mdGridPart.addToSubDomain(0,e);
        else
          mdGridPart.addToSubDomain(1,e);
      }
    }

    // always read base funcs from disk      
    DiscFuncSpaceType discFuncSpace(mdGridPart);
    std::vector<std::string> dataPathes;
    std::vector<std::string> headerNames;
    Parameter::get("rb.dataPathes", dataPathes);
    Parameter::get("rb.headerNames", headerNames);
    assert(headerNames.size()==dataPathes.size());
    assert(headerNames.size()==mdGridPart.numSubDomains());
    RBSpaceType rbSpace(discFuncSpace, dataPathes, headerNames);

    RBGeneratorType rbGenerator(rbSpace, *problem);
    // assemble reduced operator and error estimator for this rb space
    rbGenerator.updateOperators();

    // get a parameter space
    std::vector<double> begin;
    std::vector<double> end;
    const int paramDimensionSize = Parameter::getValue<int>("paramspace.paramDimensionSize");
    Parameter::get("paramspace.begin", begin);
    Parameter::get("paramspace.end", end);
    std::vector<int> numParamsVec(begin.size(), paramDimensionSize);
    
    UniformParameterSpace<std::vector<double>, std::vector<int> >
      paramSpace(begin, end, numParamsVec);
    
    assert(problem->Nlambda()== begin.size());
    const int paramSpaceSize = paramSpace.size();

    // prepare file for output
    std::stringstream filename;
    boost::uuids::uuid uid = boost::uuids::random_generator()();
    filename << "errorestimatortest_" << uid;
    std::ofstream file((filename.str()+".log").c_str());
    assert(file);
    file << "Errorestimator Test\n"
         << "-------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "All errors are relative errors!\n";

    Parameter::write((filename.str()+".paramlog").c_str());

    std::vector<double> errors;

    std::cout << "Performing reduced simulation and evaluating error"
              << " estimator for all parameters in the test set!";
    Stuff::SimpleProgressBar<> testingBar(paramSpaceSize);
    for (int k=0; k<paramSpaceSize; ++k, ++testingBar) {
      std::vector<double> mu;
      paramSpace.getParameter(k, mu);
      double relativeError = rbGenerator.getError(mu);
      errors.push_back(relativeError);
    }

    std::sort(errors.begin(), errors.end());
    double meanError = mean(errors);
    double stdDeviationOfError = stddeviation(errors);
    double maxError = *(errors.rbegin());
    double minError = *(errors.begin());

    file << "Max Error: " << maxError
         << "\nMin Error: " << minError
         << "\nMean: " << meanError
         << "\nStandard deviation: " << stdDeviationOfError
         << "\nSize of test param set: " << paramSpaceSize;

    delete problem;
    file.close();  
  } catch (Dune::Exception e) {
  std::cerr << e.what() << std::endl;
  return 1;
 }  
  
return 0;
}
