import matplotlib
from pylab import *
from numpy import *


def plotScatterData(xData, yData):
    print matplotlib.get_backend()
    print matplotlib.matplotlib_fname()
    subplot(211)
    bar(xData,yData)
    subplot(212)
    bar(yData,xData)
    savefig('test.png')
    result = 0
    
def mean(xData, yData):
    result = mean(xData)

def plotCompressibility(yData,numFuncsPerSubdomain):
    counter = 0
    numSubDomains = len(numFuncsPerSubdomain)
    for subdomain in range(0,numSubDomains):
        rows=4.0;
        columns = ceil(len(numFuncsPerSubdomain)/rows)
        subplot(rows, columns, subdomain)
        size = numFuncsPerSubdomain[subdomain]
        xData = range(0, size)
        bar(xData, [yData[i] for i in range(counter, counter+size)], log=True)
        counter = counter+size
        gca().axes.get_xaxis().set_visible(False)    
    savefig('compressibility.png')

def plotErrorBars(lowerBounds, trueErrors, upperBounds, outputName):
    size=len(lowerBounds)
    x=range(1,size+1)
    xlim(0,size+1)
    for i in range(0,len(lowerBounds)):
        lowerBounds[i]=trueErrors[i]-lowerBounds[i]
    for i in range(0,len(upperBounds)):
        upperBounds[i]-=trueErrors[i]
    errorbar(x,trueErrors,yerr=[lowerBounds,upperBounds],fmt='ro')
    savefig(outputName)
    # show()

