#include <config.h>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>



// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>

// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/solver/inverseoperators.hh>
#include <dune/fem/space/fvspace.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/rb/rbgeneration.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>
#include <dune/rb/rbasis/twophaseflow/rb/errorestimator/errorestimator.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>


// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>



using namespace Dune;
using namespace RB;

template< class VectorType, class OutputStream>
void computeProfileData(VectorType& data, OutputStream& out, const std::string& name) {
  std::sort(data.begin(), data.end());
    double meanVal = mean(data);
    double stdDeviationOfVal = stddeviation(data);
    double maxVal = *(data.rbegin());
    double minVal = *(data.begin());
    assert(out);
    out << "Max " << name << ": " << maxVal
        << "\nMin " << name << ": " << minVal
        << "\nMean " << name << ": " << meanVal
        << "\nStandard deviation of " << name << ": " << stdDeviationOfVal;
  return;
}

#ifndef POLORDER 
const int polynomialOrder = 2;
#else 
const int polynomialOrder = POLORDER;
#endif

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                                   GridType;
    typedef LeafGridPart<GridType>                                                   GridPartType;
    typedef Dune::RB::MultiDomainGridPart<GridPartType>                              MDGridPartType;
    typedef FunctionSpace<double, double, GridType::dimension, 1>                    FunctionSpaceType;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpaceType, MDGridPartType, polynomialOrder>      DiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                              DiscreteFunctionType;
    typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType>        ConstrainedDFType;
    typedef ConstrainedRBSpace<ConstrainedDFType>                                    RBSpaceType;
    typedef AdaptiveDiscreteFunction<RBSpaceType>                                    ReducedFunctionType;
    typedef DiscreteFunctionList_xdr<ReducedFunctionType>                            ReducedListType;
    typedef DiscFuncSpaceType::IteratorType                                          IteratorType;
    typedef GridType::Codim<0>::EntityPointer                                        EntityPointerType;
    typedef DiscFuncSpaceType::EntityType                                            EntityType;
    typedef Dune::SparseRowMatrixTraits<RBSpaceType, RBSpaceType>                    MatrixObjectTraitsType;
    typedef ReducedOperator<ReducedFunctionType, MatrixObjectTraitsType>             ReducedOperatorType;
    typedef ProblemInterface<FunctionSpaceType>                                      ProblemType;
    typedef ErrorEstimator<ReducedFunctionType, ProblemType>                         ErrorEstimatorType;
    typedef RBGenerationTraits<RBSpaceType, ReducedOperatorType, ErrorEstimatorType> RBGenerationTraitsType;
    typedef RBGeneration<RBGenerationTraitsType>                                     RBGeneratorType;

    //---- InverseOperator ----------------------------------------------------
    typedef Dune::OEMBICGSTABOp<ReducedFunctionType, ReducedOperatorType>    InverseOperatorType;

    MPIManager::initialize(argc, argv);
    Parameter::append("../spe10.params");

    GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));

    GridType &grid = *gridPtr;
    // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("grid.globalRefine", 0));
    // print some information about the grid
    Dune::gridinfo(grid);
    MDGridPartType mdGridPart(grid);
    // Dune::VTKWriter<GridType::LeafGridView> vtkwriter(grid.leafView());
    // vtkwriter.write("grid");

    SubdomainMarker<MDGridPartType> sdMarker(mdGridPart);
      //    sdMarker.visualizeSubdomains();

    // typedef LevelGridPart<GridType>			LevelGridPartType;
    // typedef FiniteVolumeSpace<FunctionSpaceType, LevelGridPart<GridType>, 0>    FVSpaceType;
    // typedef AdaptiveDiscreteFunction<FVSpaceType>		FVFunction;
    // LevelGridPartType levelGridPart(grid, 0);
    // FVSpaceType fvSpace(levelGridPart);
    // FVFunction marker("sdMarker", fvSpace);
    // double* markerDofs = marker.leakPointer();
    // int numDofs = marker.size();
    // for (int i=0; i!=numDofs; ++i) {
    //   markerDofs[i]=i;
    // }
    // FunctionPlotter<FVFunction> plotMarker(marker);
    // plotMarker.plot();
    // pressAnyKey();

    DiscFuncSpaceType discFuncSpace(mdGridPart);

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    // visualize the permability
    typedef FiniteVolumeSpace<FunctionSpaceType, MDGridPartType, 0>     FVSpaceType;
    typedef AdaptiveDiscreteFunction<FVSpaceType>	            FVFunction;
    FVSpaceType fvSpace(mdGridPart);
    FVFunction permeability_x("perm_x", fvSpace);
    FVFunction permeability_y("perm_y", fvSpace);
    FVFunction permeability_z("perm_z", fvSpace);
    permeability_x.clear();
    permeability_y.clear();
    permeability_z.clear();
    for (IteratorType it = mdGridPart.begin<0>(); it != mdGridPart.end<0>(); ++it) {
      const EntityType& e = *it;
      // get the barycenter
      typedef FieldVector<double, 3> DomainType;
      DomainType local = Dune::GenericReferenceElements<GridType::ctype,
		                GridType::dimension>::general(e.type()).position(0,0);
      DomainType global = e.geometry().global(local);
      ProblemType::DiffusionMatrixType m;
      problem->K(global, m);
      assert(m[0][0]>1e-6);
      assert(m[1][1]>1e-6);
      assert(m[2][2]>1e-6);
      permeability_x.localFunction(e)[0]=m[0][0];
      permeability_y.localFunction(e)[0]=m[1][1];
      permeability_z.localFunction(e)[0]=m[2][2];
    }
    FunctionPlotter<FVFunction> plotterX(permeability_x), plotterY(permeability_y), plotterZ(permeability_z);
    plotterX.plot();
    plotterY.plot();
    plotterZ.plot();

    // now, get a reduced space
    RBSpaceType* rbSpace;
    bool readFromDisk = Parameter::getValue<bool>("rb.readBaseFuncsFromDisk", false);
    if (readFromDisk) {
      std::vector<std::string> dataPathes;
      std::vector<std::string> headerNames;
      Parameter::get("rb.dataPathes", dataPathes);
      Parameter::get("rb.headerNames", headerNames);
      assert(headerNames.size()==dataPathes.size());
      assert(headerNames.size()==mdGridPart.numSubDomains());
      rbSpace = new RBSpaceType(discFuncSpace, dataPathes, headerNames);
      std::cout << "Base functions read!\n";
    } else {
      rbSpace = new RBSpaceType(discFuncSpace);
    }

    // RB generator
    RBGeneratorType rbGenerator(*rbSpace, *problem, mdGridPart);

    if (readFromDisk) {
      rbGenerator.generateBasisFromSnapshots();
        //      rbGenerator.plotBaseFunctions();
    }

    if (!readFromDisk) {
      // extend the reduced basis
      rbGenerator.init();
        //      rbGenerator.plotBaseFunctions();
      rbGenerator.greedyExtension();
    }
    //rbGenerator.plotBaseFunctions();


    // assemble reduced operator
    rbGenerator.updateReducedOperator();
    std::cout << "Reduced operator assembled!\n";

    // get a parameter space
    std::vector<double> begin;
    std::vector<double> end;
    const int paramDimensionSize = Parameter::getValue<int>("testParamspace.paramDimensionSize");
    Parameter::get("testParamspace.begin", begin);
    Parameter::get("testParamspace.end", end);
    std::vector<int> numParamsVec(begin.size(), paramDimensionSize);
    
    UniformParameterSpace<std::vector<double>, std::vector<int> >
      paramSpace(begin, end, numParamsVec);
    
    //    assert(problem->Nlambda()== begin.size());
    const int paramSpaceSize = paramSpace.size();

    // prepare file for output
    std::stringstream filename;
    boost::uuids::uuid uid = boost::uuids::random_generator()();
    filename << "lowdimschemetest_" << uid;
    std::ofstream file((filename.str()+".log").c_str());
    assert(file);
    file << "Low dim scheme test\n"
         << "-------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "All errors are relative errors!\n";

    std::vector<double> errors;
    std::vector<double> highdimRuntimes(paramSpaceSize);
    std::vector<double> lowdimRuntimes(paramSpaceSize);
    std::vector<double> reconsTimes(paramSpaceSize);

    Parameter::write((filename.str()+".paramlog").c_str());

    // lets see if the user wants to test the basis
    bool testWanted = false;
    std::cout << "Would you like to test the basis? (0/1) ";
    std::cin >> testWanted;
    if (testWanted) {
      std::cout << "Performing reduced simulation and computing error to highdim solution"
                << " for all test parameters!\n";
      Stuff::SimpleProgressBar<> testingBar(paramSpaceSize);
      for (int k=0; k<paramSpaceSize; ++k, ++testingBar) {
        std::vector<double> mu;
        paramSpace.getParameter(k, mu);
        double relativeError 
          = rbGenerator.getErrorHighdim(mu, highdimRuntimes[k], lowdimRuntimes[k], reconsTimes[k]);
        errors.push_back(relativeError);
      }
      ++testingBar;

      computeProfileData(errors, file, "errors");
      file << "\nSize of test param set: " << paramSpaceSize;
      for (int i=0; i!=int(mdGridPart.numSubDomains()); ++i)
        file << "\nSize of RB in SD " << i << ": "
             << rbSpace->getFuncList(i).size();

      computeProfileData(highdimRuntimes, file, "highdimruntimes (ms)");
      file << std::endl;
      computeProfileData(lowdimRuntimes, file, "lowdimruntimes (mus)");
      file << std::endl;
      computeProfileData(reconsTimes, file, "reconstimes (ms)");
    } // if test wanted
    // lets see if the user wants to plot
    bool plotWanted = false;
    std::cout << "Would you like to plot a function? (0/1) ";
    std::cin >> plotWanted;
    while (plotWanted) {
      std::vector<double> mu;
      for (int i=0; i!=int(begin.size()); ++i) {
        std::cout << std::endl;
        std::cout << "Please enter component " << i << " of parameter! ";
        double val;
        std::cin >> val;
        mu.push_back(val);
      }
      problem->replaceParameter(mu);
      rbGenerator.plotError(false);
      std::cout << "Would you like to plot another function? (0/1) ";
      std::cin >> plotWanted;
    }

    //    delete problem;
    file.close();  
    delete rbSpace;

  } catch (Dune::Exception e) {
    std::cerr << e.what();
    return 1;
  }  
  return 0;
}

