#include <config.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>
// index mapper
#include <dune/grid/common/universalmapper.hh>


// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>
#include <dune/rb/rbasis/twophaseflow/rb/rbgeneration.hh>
#include <dune/rb/rbasis/twophaseflow/rb/errorestimator/errorestimator.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>


// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

using namespace Dune; 
using namespace RB;

#ifndef POLORDER 
const int polynomialOrder = 2;
#else 
const int polynomialOrder = POLORDER;
#endif



int main(int argc, char *argv[])
{
  
  try {
    typedef GridSelector::GridType                                                      GridType;
    typedef LeafGridPart<GridType>                                                      GridPartType;
    typedef Dune::RB::MultiDomainGridPart<GridPartType>                                 MDGridPartType;
    typedef FunctionSpace<double, double, GridType::dimension, 1>                       FunctionSpaceType;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpaceType, MDGridPartType, polynomialOrder>
      DiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<DiscFuncSpaceType>                                 DiscreteFunctionType;
    typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridPartType>           ConstrainedDFType;
    typedef ConstrainedRBSpace<ConstrainedDFType>                                       RBSpaceType;
    typedef AdaptiveDiscreteFunction<RBSpaceType>                                       ReducedFunctionType;
    typedef DiscreteFunctionList_xdr<ReducedFunctionType>                               ReducedListType;
    typedef Dune::SparseRowMatrixTraits<RBSpaceType, RBSpaceType>                       MatrixObjectTraitsType;
    typedef ReducedOperator<ReducedFunctionType, MatrixObjectTraitsType>                ReducedOperatorType;
    typedef ProblemInterface<FunctionSpaceType>                                         ProblemType;
    typedef ErrorEstimator<ReducedFunctionType, ProblemType>                            ErrorEstimatorType;
    typedef RBGenerationTraits<RBSpaceType, ReducedOperatorType, ErrorEstimatorType>    RBGenerationTraitsType;
    typedef RBGeneration<RBGenerationTraitsType>						    RBGeneratorType;

    MPIManager::initialize(argc, argv);
    Parameter::append("testgreedy.params");

    GridPtr<GridType> gridPtr(Parameter::getValue<std::string>("grid.macroGridFile"));
    GridType &grid = *gridPtr;
    // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("grid.globalRefine", 0));
    // print some information about the grid
    Dune::gridinfo(grid);

    MDGridPartType mdGridPart(grid);

    devideGrid<DiscFuncSpaceType>(mdGridPart);

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    DiscFuncSpaceType discFuncSpace(mdGridPart);
    RBSpaceType* rbSpace;
    bool readFromDisk = Parameter::getValue<bool>("rb.readBaseFuncsFromDisk", false);
    if (readFromDisk) {
      std::vector<std::string> dataPathes;
      std::vector<std::string> headerNames;
      Parameter::get("rb.dataPathes", dataPathes);
      Parameter::get("rb.headerNames", headerNames);
      assert(headerNames.size()==dataPathes.size());
      assert(headerNames.size()==mdGridPart.numSubDomains());
      rbSpace = new RBSpaceType(discFuncSpace, dataPathes, headerNames);
    } else {
      rbSpace = new RBSpaceType(discFuncSpace);
    }
    RBGeneratorType rbGenerator(*rbSpace, *problem);

    rbGenerator.plotBaseFunctions();

    if (!readFromDisk)
      rbGenerator.init();

    rbGenerator.greedyExtension();

    delete rbSpace;
    FemTimer::print(std::cout);
  } catch (Dune::Exception e) {
    std::cerr << std::endl << e.what() << std::endl;
    return 1;
  }  

  return 0;
}
