#ifndef __SCALAR_PRODUCT_HH__
#define __SCALAR_PRODUCT_HH__

#include <dune/fem/quadrature/cachingquadrature.hh>

template <class DiscFuncType1, class DiscFuncType2>
double scalar_product(const DiscFuncType1 &func1,
                      const DiscFuncType2 &func2)
{

  typedef typename DiscFuncType1::DiscreteFunctionSpaceType       DiscreteFunctionSpaceType;
  typedef typename DiscFuncType1::RangeType                       RangeType;
  typedef typename DiscFuncType1::DomainType                      DomainType;
  typedef typename DiscreteFunctionSpaceType::IteratorType        IteratorType;
  typedef typename DiscreteFunctionSpaceType::GridPartType        GridPartType;
  typedef typename DiscreteFunctionSpaceType::BaseFunctionSetType BaseFunctionSetType;

  enum { polynomialOrder = DiscreteFunctionSpaceType::polynomialOrder };

  const DiscreteFunctionSpaceType &discFuncSpace = func1.space();
  std::vector<double> squareBaseFuncNorms(discFuncSpace.size(), 0.0);
  GridPartType gridPart = discFuncSpace.gridPart();

  //============================  compute the square base functions norms ==================
  //loop over grid
  IteratorType end = gridPart.template end<0>();
  for (IteratorType it = gridPart.template begin<0>();
       it != end; ++it) {
    //get baseFunctionSet for this entity
    BaseFunctionSetType baseFuncSet = discFuncSpace.baseFunctionSet(*it);
    //loop over all base functions
    unsigned int numBaseFuncs = baseFuncSet.numBaseFunctions();
    assert(numBaseFuncs < squareBaseFuncNorms.size());
    for (unsigned int i=0; i!=numBaseFuncs; ++i) {
      double entityIntegral=0.0;
      //get a quadrature for this entity
      Dune::CachingQuadrature<GridPartType, 0> quadrature(*it, 2*polynomialOrder+2);

      //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint) {
        double integrationElement = it->geometry().integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);
        RangeType baseFunctionValue;
        baseFuncSet.evaluate(i, quadrature.point(quadPoint), baseFunctionValue);
        entityIntegral += integrationElement * weight * baseFunctionValue * baseFunctionValue;
      } //loop over all quadrature points
        //accumulate integral for this base function
      squareBaseFuncNorms[discFuncSpace.mapToGlobal(*it, i)] += entityIntegral;
    } // loop over all base functions
  } //loop over grid
  //==========================================================================================


  assert(func1.size()==func2.size());
  //================================ perform scalar product ==================================
  double result = 0.0;

  typedef typename DiscFuncType1 :: ConstDofIteratorType             ConstDofIteratorType1;
  typedef typename DiscFuncType2 :: ConstDofIteratorType             ConstDofIteratorType2;

  ConstDofIteratorType2 func2it = func2.dbegin();
  int i=0;
  for (ConstDofIteratorType1 func1it = func1.dbegin();
       func1it!= func1.dend(); ++func1it, ++func2it, ++i) {
    result += (*func1it) * (*func2it) * squareBaseFuncNorms[i];
  }//end of sum loop

  return result;
}

template <class DiscFuncType, class MatrixType>
double scalar_product(const DiscFuncType &func1,
                      const DiscFuncType &func2,
                      const MatrixType& matrix) {
  DiscFuncType buffer("buffer", func1.space());
  matrix.apply(func1, buffer);
  typename DiscFuncType::RangeType val = buffer.scalarProductDofs(func2);
  return val[0];
}

/** Calculate the scalar product of two Dune::AdaptiveDiscreteFunction functions using an Eigen matrix.
 *
 *  @tparam DiscreteFunctionSpaceType The type that specializes the Dune::AdaptiveDiscreteFunction template class.
 *  @tparam EigenDerived The type that specializes the Eigen::Matrix.
 *  @param[in] discreteFunctionOne The first function @f$\phi@f$ for the scalar product.
 *  @param[in] discreteFunctionTwo The second function @f$\psi@f$ for the scalar product. 
 *  @param[in] matrix The matrix @f$A@f$ for the scalar product.
 *  @return Returns the scalar product @f$\phi^\tau\cdot A\cdot\psi@f$.
 */
template<class DiscreteFunctionSpaceType, class EigenMatrixType >
double scalarProduct(const Dune::AdaptiveDiscreteFunction<DiscreteFunctionSpaceType>& discreteFunctionOne,
                     const Dune::AdaptiveDiscreteFunction<DiscreteFunctionSpaceType>& discreteFunctionTwo,
                     const EigenMatrixType& matrix) {
    const Eigen::Map<const Eigen::VectorXd> one(discreteFunctionOne.leakPointer(), discreteFunctionOne.size());
    const Eigen::Map<const Eigen::VectorXd> two(discreteFunctionTwo.leakPointer(), discreteFunctionTwo.size());
    
    return one.transpose()*matrix*two;
}

template <class DiscFuncType1, class DiscFuncType2>
double scalarProductH1(const DiscFuncType1 &func1,
                       const DiscFuncType2 &func2)
{
  
  typedef typename DiscFuncType1::DiscreteFunctionSpaceType       DiscreteFunctionSpaceType;
  typedef typename DiscFuncType1::RangeType                       RangeType;
  typedef typename DiscFuncType1::JacobianRangeType               JacobianRangeType;
  typedef typename DiscFuncType1::DomainType                      DomainType;
  typedef typename DiscreteFunctionSpaceType::IteratorType        IteratorType;
  typedef typename DiscreteFunctionSpaceType::GridPartType        GridPartType;
  typedef typename DiscreteFunctionSpaceType::BaseFunctionSetType BaseFunctionSetType;
  
  enum { polynomialOrder = DiscreteFunctionSpaceType::polynomialOrder };
  
  const DiscreteFunctionSpaceType &discFuncSpace = func1.space();
  std::vector<double> squareBaseFuncNorms(discFuncSpace.size(), 0.0);
  GridPartType gridPart = discFuncSpace.gridPart();
  
    //============================  compute the square base functions norms ==================
    //loop over grid
  IteratorType end = gridPart.template end<0>();
  for (IteratorType it = gridPart.template begin<0>();
       it != end; ++it) {
      //get baseFunctionSet for this entity
    BaseFunctionSetType baseFuncSet = discFuncSpace.baseFunctionSet(*it);
      //loop over all base functions
    unsigned int numBaseFuncs = baseFuncSet.numBaseFunctions();
    assert(numBaseFuncs < squareBaseFuncNorms.size());
    for (unsigned int i=0; i!=numBaseFuncs; ++i) {
      double entityIntegral=0.0;
        //get a quadrature for this entity
      Dune::CachingQuadrature<GridPartType, 0> quadrature(*it, 2*polynomialOrder+2);
      
        //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint) {
        double integrationElement = it->geometry().integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);
        JacobianRangeType baseFunctionJacobianValue;
        baseFuncSet.jacobian(i, quadrature.point(quadPoint), baseFunctionJacobianValue);
        entityIntegral += integrationElement * weight * (baseFunctionJacobianValue[0] * baseFunctionJacobianValue[0]);
      } //loop over all quadrature points
        //accumulate integral for this base function
      squareBaseFuncNorms[discFuncSpace.mapToGlobal(*it, i)] += entityIntegral;
    } // loop over all base functions
  } //loop over grid
    //==========================================================================================
  
  
  assert(func1.size()==func2.size());
    //================================ perform scalar product ==================================
  double result = 0.0;
  
  typedef typename DiscFuncType1 :: ConstDofIteratorType             ConstDofIteratorType1;
  typedef typename DiscFuncType2 :: ConstDofIteratorType             ConstDofIteratorType2;
  
  ConstDofIteratorType2 func2it = func2.dbegin();
  int i=0;
  for (ConstDofIteratorType1 func1it = func1.dbegin();
       func1it!= func1.dend(); ++func1it, ++func2it, ++i) {
    result += (*func1it) * (*func2it) * squareBaseFuncNorms[i];
  }//end of sum loop
  
  return result;
}


#endif /* __SCALAR_PRODUCT_HH__ */
