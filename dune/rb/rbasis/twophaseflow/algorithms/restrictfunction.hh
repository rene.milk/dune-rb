#ifndef __RESTRICTFUNCTION_HH__
#define __RESTRICTFUNCTION_HH__

#include <set>

#include <Eigen/Core>

namespace Dune {
  namespace RB {
  
      template<class DiscreteFunctionSpaceImp>
      class Localizer {
          typedef DiscreteFunctionSpaceImp DiscreteFunctionSpaceType;
          
      public:
          
          Localizer(const DiscreteFunctionSpaceType& discFuncSpace) {
              init(discFuncSpace);
          }
          
          typedef Eigen::MatrixXi
          RestrictionMatrixType;
          
          typedef Eigen::MatrixXd
          MatrixType;
          
          typedef Eigen::VectorXd
          VectorType;

          template<class DiscreteFunctionSpaceType>
          void init(const DiscreteFunctionSpaceType& discFuncSpace) {
              computeRestrictionMatrix(discFuncSpace, restrictionMatrix_);
              return;
          }
          
          // const MatrixType& localizeMatrix(const MatrixType& matrix, const int element) const {
          //     assert(element<restrictionMatrix_.cols() 
          //            && element>=0
          //            && "You tried to restrict a matrix to an element that does not exist!");              
          //     assert(matrix.rows()==restrictionMatrix_.rows()
          //            && matrix.cols()==restrictionMatrix_.rows()
          //            && "You tried to restrict a matrix that does not have the right dimension!");
          //     const int newRows = restrictionMatrix_.col(element).sum();
          //     localizedMatrix_ = MatrixType::Zero(newRows, newRows);
          //     int i=0;
          //     int j=0;
          //     for (int row=0; row!=matrix.rows(); ++row) {
          //         if (restrictionMatrix_(row, element)==1) {
          //             for (int col=0; col!=matrix.cols(); ++col) {
          //                 if (restrictionMatrix_(col, element)==1) {
          //                     localizedMatrix_(i, j) = matrix(row, col);   
          //                     ++j;
          //                 }
          //             }
          //             ++i; j=0;
          //         }
          //     }
          //     return localizedMatrix_;
          // }

          const MatrixType& localizeMatrixRows(const MatrixType& matrix, const int element) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to restrict a matrix to an element that does not exist!");              
              assert(matrix.rows()==restrictionMatrix_.rows()
                     && "You tried to restrict a matrix that does not have the right dimension!");
              const int newRows = restrictionMatrix_.col(element).sum();
              localizedMatrix_ = MatrixType::Zero(newRows, matrix.cols());
              int i=0;
              for (int row=0; row!=matrix.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      for (int col=0; col!=matrix.cols(); ++col) {
                              localizedMatrix_(i, col) = matrix(row, col);   
                      }
                      ++i;
                  }
              }
              return localizedMatrix_;
          }

          
          const MatrixType& localizeMatrixColumns(const MatrixType& matrix, const int element) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to restrict a matrix to an element that does not exist!");              
              assert(matrix.rows()==restrictionMatrix_.rows()
                     && matrix.cols()==restrictionMatrix_.rows()
                     && "You tried to restrict a matrix that does not have the right dimension!");
              const int newCols = restrictionMatrix_.col(element).sum();
              localizedMatrix_ = MatrixType::Zero(matrix.rows(), newCols);
              int i=0;
              int j=0;
              for (int row=0; row!=matrix.rows(); ++row) {
                      for (int col=0; col!=matrix.cols(); ++col) {
                          if (restrictionMatrix_(col, element)==1) {
                              localizedMatrix_(i, j) = matrix(row, col);   
                              ++j;
                          }
                      }
                      ++i; j=0;
              }
              return localizedMatrix_;
          }

          const VectorType& localizeVector(const VectorType& vector, const int element) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to restrict a vector to an element that does not exist!");              
              assert(vector.rows()==restrictionMatrix_.rows()
                     && "You tried to restrict a vector that does not have the right dimension!");
              const int newRows = restrictionMatrix_.col(element).sum();
              localizedVector_ = VectorType::Zero(newRows);
              int i=0;
              for (int row=0; row!=vector.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      localizedVector_(i) = vector(row);   
                      ++i;
                  }
              }
              return localizedVector_;
          }

          const VectorType& getFullVector(const VectorType& vector, const int element) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to restrict a vector to an element that does not exist!");              
              assert(vector.rows()==restrictionMatrix_.col(element).sum()
                     && "You tried to restrict a vector that does not have the right dimension!");
              localizedVector_ = VectorType::Zero(restrictionMatrix_.rows());
              int i=0;
              for (int row=0; row!=localizedVector_.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      localizedVector_(row) = vector(i);   
                      ++i;
                  } else {
                      localizedVector_(row)=0.0;                      
                  }
              }
              return localizedVector_;
          }
          
          void getFullVector(const VectorType& vector, const int element, VectorType& ret) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to restrict a vector to an element that does not exist!");              
              assert(vector.rows()==restrictionMatrix_.col(element).sum()
                     && "You tried to restrict a vector that does not have the right dimension!");
              ret = VectorType::Zero(restrictionMatrix_.rows());
              int i=0;
              for (int row=0; row!=ret.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      ret(row) = vector(i);   
                      ++i;
                  } else {
                      ret(row)=0.0;                      
                  }
              }
              return;
          }
          
          const MatrixType& getFullMatrixRows(const MatrixType& matrix, const int element) const {
              assert(element<restrictionMatrix_.cols() 
                     && element>=0
                     && "You tried to get the full version of a matrix to an element that does not exist!");              
              assert(matrix.rows()==restrictionMatrix_.col(element).sum()
                     && "You tried to rget the full version of a matrix that does not have the right dimension!");
              localizedMatrix_ = MatrixType::Zero(restrictionMatrix_.rows(), matrix.cols());
              int i=0;
              for (int row=0; row!=localizedMatrix_.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      localizedMatrix_.row(row) = matrix.row(i);   
                      ++i;
                  }
              }
              return localizedMatrix_;
          }
          
          template<class TripletsVectorType>
          void addTriplets(const MatrixType& matrix, const int element,
                           const int start, TripletsVectorType& triplets) const {
              int i=0;
              for (int row=0; row!=restrictionMatrix_.rows(); ++row) {
                  if (restrictionMatrix_(row, element)==1) {
                      for (int j=0; j!=matrix.cols(); ++j)
                          triplets.push_back(typename TripletsVectorType::value_type(row, start+j, matrix(i,j)));
                      ++i;
                  }                
              }
              return;
          }
          
      private:
          template<class DiscreteFunctionSpaceType>
          void computeRestrictionMatrix(const DiscreteFunctionSpaceType& discFuncSpace,
                                        RestrictionMatrixType& restrictionMatrix) const {
              typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
              typedef typename IteratorType::Entity EntityType;
              typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
              const GridPartType& gridPart = discFuncSpace.gridPart();
              
              const typename DiscreteFunctionSpaceType::MapperType& mapper = discFuncSpace.mapper();
              restrictionMatrix = Eigen::MatrixXi::Zero(discFuncSpace.size(), gridPart.numSubdomains());
              const int numSubdomains = gridPart.numSubdomains();
              IteratorType gEnd = discFuncSpace.end();
              for (IteratorType it=discFuncSpace.begin(); it!=gEnd; ++it) {
                  const EntityType &en = *it;
                  const int subdomain = gridPart.getSubdomain(en);
                  int numDofs = mapper.numDofs(en);
                  for (int i=0; i!=numDofs; ++i) {
                      const int globalDof = mapper.mapToGlobal(en, i);
                      restrictionMatrix.row(globalDof)=Eigen::RowVectorXi::Unit(numSubdomains, subdomain);
                  }
              } // grid run
          }
          
          RestrictionMatrixType restrictionMatrix_;
          mutable MatrixType localizedMatrix_;
          mutable VectorType localizedVector_;          
      };


    template<class InputFunctionType, class OutputFunctionType>
    void restrictFunction(const InputFunctionType &p, OutputFunctionType &ret) {

      typedef typename InputFunctionType::LocalFunctionType                             InputLocalFunctionType;
      typedef typename OutputFunctionType::LocalFunctionType                            OutputLocalFunctionType;
      typedef typename InputFunctionType::DiscreteFunctionSpaceType::IteratorType       IteratorType;
      typedef typename IteratorType::Entity		        EntityType;
      typedef typename InputFunctionType::MacroElement                                  MacroElement;

      MacroElement support = p.getSupport();
      ret.clear();

      IteratorType gEnd = p.space().end();
      for (IteratorType it=p.space().begin(); it!=gEnd; ++it) {
        const EntityType &en = *it;
        // if given entity is in the support-macro element of p
        if (p.gridPartition().subDomainContains(support, en)) {
          // get local functions for p and ret
          InputLocalFunctionType pLocal = p.localFunction(en);
          OutputLocalFunctionType retLocal = ret.localFunction(en);

          int numDofs = pLocal.numDofs();
          // make shure, both functions at least have the same polynomial degree
          assert(numDofs==retLocal.numDofs());

          for (int i=0; i!=numDofs; ++i)
            retLocal[i] = pLocal[i];
 
        } // if p has support in this element
      } // end of grid run
    }

    template<class ConstrainedDFImpl>
    class FunctionRescaler {

      typedef ConstrainedDFImpl                                                         ConstrainedDF;
      typedef typename ConstrainedDF::DiscreteFunctionSpaceType                         DiscreteSpace;
      typedef typename ConstrainedDF::DofIteratorType                                   DofIterator;
      typedef typename DiscreteSpace::GridPartType                                      GridPartType;
      typedef typename ConstrainedDF::LocalFunctionType                                 LocalFunctionType;
      typedef typename DiscreteSpace::IteratorType                                      IteratorType;
      typedef typename ConstrainedDF::MacroElement                                      MacroElement;
      typedef typename GridPartType::GridType::template Codim<0>::Entity                Entity;
      typedef typename GridPartType::GridType::Traits::template Codim<0>::EntityPointer EntityPointer;
      typedef typename GridPartType::IntersectionIteratorType                           IntIteratorType;
      typedef typename IntIteratorType::Intersection                                    IntersectionType;

    public:

      FunctionRescaler():initialized_(false) {};
      

      void rescaleFunctionOnIntersection(ConstrainedDFImpl &p) {
        if (!initialized_)
          init(p);

        for (std::map<int, int>::const_iterator it = globalDofs_.begin();
             it!=globalDofs_.end(); ++it) {
          const std::size_t dofIndex = it->first;
          if (it->second<=4)
            p.leakPointer()[dofIndex] *= 0.5;
          else
            p.leakPointer()[dofIndex] *= 0.25;
        }
      }

    private:
      void init(const ConstrainedDF& p) {
        initialized_=true;

        const DiscreteSpace&  discFuncSpace = p.space();
        const GridPartType&   gridPart      = discFuncSpace.gridPart();

        IteratorType gEnd = gridPart.template end<0>();
        for (IteratorType it=gridPart.template begin<0>(); it!=gEnd; ++it) {
          const Entity &en = *it;
          // get local function for p
          LocalFunctionType pLocal = p.localFunction(en);

          int numDofs = pLocal.numDofs();

          /*check, if one of the current dofs belongs to an
            intersection that is shared by different coarse cells.
            If that is the case, we assign only the half value.*/
          IntIteratorType iend = gridPart.iend(en);
          for (IntIteratorType iit = gridPart.ibegin(en); iit!=iend; ++iit) {
            const IntersectionType &intersection = *iit;
            if (gridPart.onCoarseCellIntersection(intersection)) {
              EntityPointer outsidePtr = intersection.outside(); 
              const Entity& outsideEn = *outsidePtr;
              LocalFunctionType outsideLF = p.localFunction(outsideEn);
     
              std::set<int> outsideGlobalDofs;
              for (int i = 0; i < outsideLF.numDofs(); ++i) {
                outsideGlobalDofs.insert(discFuncSpace.mapToGlobal(outsideEn, i));
              }
              for (int i = 0; i < numDofs; ++i) {
                const unsigned int globalDof = discFuncSpace.mapToGlobal(en, i);
                std::set<int>::iterator setIt = outsideGlobalDofs.find(globalDof);
                if (setIt!=outsideGlobalDofs.end()) {
                  std::map<int, int>::iterator globalDofsIt = globalDofs_.find(globalDof);
                  if (globalDofsIt==globalDofs_.end())
                    globalDofs_[globalDof]=1;
                  else
                    ++(globalDofsIt->second);
                }
              }
            }
          }
        } // end of grid run
      } // void init

      std::map<int, int> globalDofs_;
      bool initialized_;
    };
  } // namespace RB
} // namespace Dune


#endif /* __RESTRICTFUNCTION_HH__ */

