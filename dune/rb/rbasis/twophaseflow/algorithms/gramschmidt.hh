#ifndef __GRAMSCHMIDT_HH__
#define __GRAMSCHMIDT_HH__

#include "scalar_product.hh"
#include <dune/fem/misc/l2norm.hh>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>

namespace Dune {
  
  namespace RB {

    template<class ReducedSpaceType>
    int getStartOfSubDomain(const ReducedSpaceType& reducedSpace, const int subDomain) {
      int start=0;
      for (int i=0; i!=subDomain; ++i) {
        start+=reducedSpace.getFuncList(i).size(); 
      }
      return start;      
    }

//      // Gram-Schmidt using the energy scalar product. This assumes the
//      // new function to be the last base function in reducedSpace.
//    template<class ReducedSpaceType, class MatrixType>
//    void gramSchmidtEnergy(ReducedSpaceType& reducedSpace, const int subDomain, const MatrixType& matrix) {
//
//      typename ReducedSpaceType::ConstrainedList& baseFunctions = reducedSpace.getFuncList(subDomain);
//      baseFunctions.stashFunctions();
//      const int numBaseFunctions = baseFunctions.size();
//        // save new function in buffer
//      typename ReducedSpaceType::BaseFunctionType& newBaseFunction = baseFunctions.getFunc(numBaseFunctions-1);
//
//        // calculate the row where the matching values of the bilinear form are to be found in matrix
//      const int row = getStartOfSubDomain(reducedSpace, subDomain);
//      //perfom the projection algorithm
//      for (int j=row; j!=row+numBaseFunctions-1; ++j) {
//        double numerator = matrix(row+numBaseFunctions-1, j);
//        double denominator = matrix(j,j);
//        double factor = numerator/denominator;
//        newBaseFunction.addScaled(baseFunctions.getFunc(j), -1.0*factor);
//      }//loop over baseFuncs
//       // save changes in new base function to disk
//      baseFunctions.setFunc(numBaseFunctions-1, newBaseFunction);
//      return;
//    }
//    
    
    // Gram-Schmidt using arbitrary matrix for scalar product
    template<class DiscreteFunctionListType, class MatrixType>
    void gramSchmidt(typename DiscreteFunctionListType::DiscreteFunctionType& u, const DiscreteFunctionListType& baseSet, 
                     const MatrixType& matrix) {
      
      typedef typename DiscreteFunctionListType::DiscreteFunctionType  DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      
      const DiscreteFunctionSpaceType& discFuncSpace = u.space();
      
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old",  discFuncSpace);
      
      int baseSetSize = baseSet.size();
      
      buffer_old.clear();
      buffer_old.assign(u);
      //perfom the projection algorithm
      for (int j=0; j!=baseSetSize; ++j) {
        buffer_ortho.clear();
        baseSet.getFunc(j, buffer_ortho);
        double prod = scalar_product(buffer_old, buffer_ortho, matrix);
        double squarenorm = scalar_product(buffer_ortho, buffer_ortho, matrix);
        buffer_ortho *= prod/squarenorm;
        u -= buffer_ortho;
      }//loop over baseFuncs
        
//        double uSquareNorm = scalar_product(u, u, matrix);
//        u /= uSquareNorm;
    }

      /** Perform the Gram-Schmidt algorithm using a given matrix for the scalar products.
       *
       *  @param[in,out] u    The function that is to be projected on the orthorgonal complement of the space spanned by 
       *                      the vectors in phi.
       *  @param[in]     phi  The function that span the space to which u shall be orthorgonal.
       *  @param[in]     A    The matrix that defines the scalar product.
       */
      template<class DiscreteFunctionType, class BasisType, class MatrixType>
      void gramSchmidt(DiscreteFunctionType& u, const BasisType& phi, const MatrixType& A) {
          Eigen::Map<Eigen::VectorXd> uVec(u.leakPointer(), u.size());
          Eigen::VectorXd uVecCopy(uVec);
          
          int baseSetSize = phi.cols();
          
          //perfom the projection algorithm
          for (int j=0; j!=baseSetSize; ++j) {
              double prod = uVecCopy.transpose()*A*phi.col(j);
              double squarenorm = phi.col(j).transpose()*A*phi.col(j);
              uVec -= phi.col(j)*prod/squarenorm;
          }
          
          return;
      }

      /** Perform the Gram-Schmidt algorithm using a given matrix for the scalar products.
       *
       *  @param[in,out] u    The function that is to be projected on the orthorgonal complement of the space spanned by 
       *                      the vectors in phi.
       *  @param[in]     phi  The function that span the space to which u shall be orthorgonal.
       *  @param[in]     A    The matrix that defines the scalar product.
       */
      template<class BasisType, class MatrixType>
      void gramSchmidt(Eigen::VectorXd& u, const BasisType& phi, const MatrixType& A) {

          Eigen::VectorXd uCopy(u);
          
          int baseSetSize = phi.cols();
          
          //perfom the projection algorithm
          for (int j=0; j!=baseSetSize; ++j) {
              double prod = uCopy.transpose()*A*phi.col(j);
              double squarenorm = phi.col(j).transpose()*A*phi.col(j);
              u -= phi.col(j)*prod/squarenorm;
          }
          
          return;
      }
      
    template<class DiscreteFunctionListType>
    void gramSchmidt(typename DiscreteFunctionListType::DiscreteFunctionType& u, const DiscreteFunctionListType& baseSet) {
      
      typedef typename DiscreteFunctionListType::DiscreteFunctionType  DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      
      const DiscreteFunctionSpaceType& discFuncSpace = u.space();
      
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old",  discFuncSpace);
      
      int baseSetSize = baseSet.size();
      
      buffer_old.clear();
      buffer_old.assign(u);
        //perfom the projection algorithm
      for (int j=0; j!=baseSetSize; ++j) {
        buffer_ortho.clear();
        baseSet.getFunc(j, buffer_ortho);
        double prod = scalar_product(buffer_old, buffer_ortho);
        double squarenorm = scalar_product(buffer_ortho, buffer_ortho);
        buffer_ortho *= prod/squarenorm;
        u -= buffer_ortho;
      }//loop over baseFuncs
#ifdef RB_DEBUG
        // printf("After gram schmidt, u is:\n");
        // u.print(std::cout);
#endif
    }
    
    template<class DiscreteFunctionListType>
    void gramSchmidtH1(typename DiscreteFunctionListType::DiscreteFunctionType& u, const DiscreteFunctionListType& baseSet) {
      
      typedef typename DiscreteFunctionListType::DiscreteFunctionType  DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      
      const DiscreteFunctionSpaceType& discFuncSpace = u.space();
      
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old",  discFuncSpace);
      
      int baseSetSize = baseSet.size();
      
      buffer_old.clear();
      buffer_old.assign(u);
        //perfom the projection algorithm
      for (int j=0; j!=baseSetSize; ++j) {
        buffer_ortho.clear();
        baseSet.getFunc(j, buffer_ortho);
        double prod = scalarProductH1(buffer_old, buffer_ortho);
        double squarenorm = scalar_product(buffer_ortho, buffer_ortho);
        buffer_ortho *= prod/squarenorm;
        u -= buffer_ortho;
      }//loop over baseFuncs
#ifdef RB_DEBUG
        // printf("After gram schmidt, u is:\n");
        // u.print(std::cout);
#endif
    }

    template<class DiscreteFunctionListType>
    void gramSchmidt(const DiscreteFunctionListType& argumentFunctions, DiscreteFunctionListType& destinationFunctions ) {

      typedef typename DiscreteFunctionListType::DiscreteFunctionType        DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType       DiscreteFunctionSpaceType;
      typedef Dune::L2Norm<typename DiscreteFunctionSpaceType::GridPartType> L2NormType;    

      L2NormType l2norm(argumentFunctions.space().gridPart());

      const int argumentSize = argumentFunctions.size();
      if (argumentSize<1)
        DUNE_THROW(InvalidStateException, "Function list given to Gram Schmidt algorithm is empty!");

      // get first function
      DiscreteFunctionType buffer("orthonormalized function", argumentFunctions.space());
      argumentFunctions.getFunc(0, buffer);
      // normalize it
      double norm = l2norm.norm(buffer);
      buffer/=norm;
      // save it as first base function
      destinationFunctions.push_back(buffer);
      for (int i=1; i!=int(argumentFunctions.size()); ++i) {
        buffer.clear();
        argumentFunctions.getFunc(i, buffer);
        gramSchmidt(buffer, destinationFunctions);
        buffer /= l2norm.norm(buffer);
        destinationFunctions.push_back(buffer);
      }
      return;
    }
    
    template<class DiscreteFunctionListType>
    void gramSchmidtH1(const DiscreteFunctionListType& argumentFunctions, DiscreteFunctionListType& destinationFunctions ) {
      
      typedef typename DiscreteFunctionListType::DiscreteFunctionType        DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType       DiscreteFunctionSpaceType;
      typedef Dune::H1Norm<typename DiscreteFunctionSpaceType::GridPartType> H1NormType;
      
      H1NormType h1norm(argumentFunctions.space().gridPart());
      
      const int argumentSize = argumentFunctions.size();
      if (argumentSize<1)
        DUNE_THROW(InvalidStateException, "Function list given to Gram Schmidt algorithm is empty!");
      
        // get first function
      DiscreteFunctionType buffer("orthonormalized function", argumentFunctions.space());
      argumentFunctions.getFunc(0, buffer);
        // normalize it
      double norm = h1norm.norm(buffer);
      buffer/=norm;
        // save it as first base function
      destinationFunctions.push_back(buffer);
      for (int i=1; i!=int(argumentFunctions.size()); ++i) {
        buffer.clear();
        argumentFunctions.getFunc(i, buffer);
        gramSchmidtH1(buffer, destinationFunctions);
        buffer /= h1norm.norm(buffer);
        destinationFunctions.push_back(buffer);
      }
      return;
    }

  } // namespace RB 
  
} // namespace Dune 

#endif /* __GRAMSCHMIDT_HH__ */
