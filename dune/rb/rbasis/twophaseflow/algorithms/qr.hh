#ifndef __QR_HH__
#define __QR_HH__

// we use std::accumulate and std::for_each
#include <numeric>
#include <algorithm>

#define USE_ALGLIB

#ifdef USE_ALGLIB
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/ap.h>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/ap.cpp>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/alglibinternal.h>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/alglibinternal.cpp>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/linalg.h>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/linalg.cpp>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/alglibmisc.h>
#include <dune/rb/rbasis/twophaseflow/thirdparty/alglib/alglibmisc.cpp>
#endif

#include <flens/flens.h>

namespace Dune {
namespace RB {

  template<class ArgumentFunctionListImp>
  class QRDecomposition {
    typedef ArgumentFunctionListImp ArgumentFunctionListType;
    typedef typename ArgumentFunctionListType::DiscreteFunctionType::ConstDofIteratorType
    ConstDofIteratorType;
    
#ifdef USE_ALGLIB
    typedef alglib::real_2d_array MatrixType;
#endif

  public:
#ifdef USE_ALGLIB
    QRDecomposition(const ArgumentFunctionListType& r) 
    : r_(r),
    matrixRows_(r_.space().size()),
    matrixCols_(r_.size())
    {
      rMatrix_.setlength(matrixRows_, matrixCols_);

      for ( int j = 0; j < matrixCols_; j++ )
      {
        ConstDofIteratorType dofIt = r_.getFunc(j).dbegin();
        assert(matrixRows_==r_.getFunc(j).size());
        for ( int i = 0; i < matrixRows_; i++, ++dofIt )
        {
          rMatrix_(i,j) = *dofIt;
        }
      }
    }
#else
    QRDecomposition(const ArgumentFunctionListType& r) 
      : r_(r),
        matrixRows_(r_.space().size()),
        matrixCols_(r_.size()),
        rMatrix_(matrixRows_, matrixCols_)
    {
      for ( int j = 0; j < matrixCols_; j++ )
      {
        ConstDofIteratorType dofIt = r_.getFunc(j).dbegin();
        assert(matrixRows_==r_.getFunc(j).size());
        for ( int i = 0; i < matrixRows_; i++, ++dofIt )
        {
          rMatrix_(i,j) = *dofIt;
        }
      }
    }
#endif
    

#ifdef USE_ALGLIB
    void compute() {
      alglib::rmatrixqr(rMatrix_, matrixRows_, matrixCols_, tau_);
      return;
    }
#else
    void compute(MatrixType &Q, MatrixType& R) {
      flens::qr(rMatrix_, Q, R);
      return;
    }
#endif

    template< class MatrixImpl>
    void getQ(MatrixImpl& Q) const {
#ifdef USE_ALGLIB
      assert(Q.numRows()==matrixRows_);
      assert(Q.numCols()==matrixRows_);
      alglib::real_2d_array qREAL;
      alglib::rmatrixqrunpackq(rMatrix_, matrixRows_, matrixCols_, tau_, matrixRows_, qREAL);
      for (int i=0; i!=matrixRows_; ++i) {
        for (int j=0; j!=matrixRows_; ++j) {
          Q(i+1,j+1)=qREAL(i,j);
        }
      }
#else
      assert(0);
#endif
      return;
    }
    
    template< class MatrixImpl>
    void getR(MatrixImpl& R) const {
#ifdef USE_ALGLIB
      assert(R.numRows()==matrixRows_);
      assert(R.numCols()==matrixCols_);
      alglib::real_2d_array rREAL;
      alglib::rmatrixqrunpackr(rMatrix_, matrixRows_, matrixCols_, rREAL);
      for (int i=0; i!=matrixRows_; ++i) {
        for (int j=0; j!=matrixCols_; ++j) {
          R(i+1,j+1)=rREAL(i,j);
        }
      }
#else
      assert(0);
#endif
      return;
    }
    
  private:
    const ArgumentFunctionListType& r_;
    const int matrixRows_;
    const int matrixCols_;
    MatrixType rMatrix_;
#ifdef USE_ALGLIB
    alglib::real_1d_array tau_;
#endif
  };
  
} // namespace RB
} // namespace Dune

#endif /* __QR_HH__ */

