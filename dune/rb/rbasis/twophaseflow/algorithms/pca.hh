#ifndef __PCA_HH__
#define __PCA_HH__

#include <stdexcept>
#include <Eigen/Core>
#include <Eigen/Eigen>

namespace Dune {
namespace RB {



template<class EigenMatrixInputImpl>
class PCAEigen {
    typedef EigenMatrixInputImpl EigenMatrixInputType;
    typedef Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> EigenvalueSolverType;
    typedef typename Eigen::VectorXd EigenvalueVectorType;
public:
    PCAEigen(const EigenMatrixInputType& matrix)
    : matrix_(matrix)
    {}
        
  void subtractMean(const Eigen::MatrixXi& restrictionInfo, const int subdomain) {
        // Calculate and substract the deviations from the mean
      matrix_ -= matrix_.colwise().mean().transpose().replicate(matrix_.rows(),1);
      // restrict basis again, this is a hack that should be made superfluous
      matrix_=(restrictionInfo.col(subdomain).replicate(1,matrix_.cols()).array()>0).select(matrix_, 0);
      return;
  }
    
    const Eigen::MatrixXd& computeCovarianceMatrix() {
      covarianceMatrix_ = (matrix_.transpose()*matrix_);
//      covarianceMatrix_ /= matrix_.cols();
      return covarianceMatrix_;
    }

  template<class ProductMatrixType>
    const Eigen::MatrixXd& computeCovarianceMatrix(const ProductMatrixType& productMatrix) {
      covarianceMatrix_ = (matrix_.transpose()*productMatrix*matrix_);

      return covarianceMatrix_;
    }
    
    
    void computeEigenvalues() {
        if (covarianceMatrix_.size()==0)
            DUNE_THROW(InvalidStateException, "You need to call computeGramMatrix() before computing the eigenvalues!");
        eigenvalueSolver_.compute(covarianceMatrix_);
        if (eigenvalueSolver_.info() != Eigen::Success) {
          std::cout << "\nCovariance matrix:\n" << covarianceMatrix_ << std::endl;
          throw std::runtime_error("Solver for eigenvalues failed to converge!");
        }
        return;
    }
    
    double getTotalVariation() const {
        return eigenvalueSolver_.eigenvalues().sum();
    }
    
    const int computeNeededNumberByPercentage(const int desiredPercentage,
                                              const int minimumSize=-1) {
        const EigenvalueVectorType& eigenvalues = eigenvalueSolver_.eigenvalues();
        neededComponents_ = 1;
        double totalVariation = getTotalVariation();

        // compute the percentage of the total variation that the first pc covers
        double currentPercentage = eigenvalues(eigenvalues.rows()-1)/totalVariation;
        // while the desired percentage is not reached, enlarge the set of pc's that
        // will be returned
        while (currentPercentage<desiredPercentage && neededComponents_<matrix_.cols()) {
            ++neededComponents_;
            currentPercentage += eigenvalues(eigenvalues.rows()-neededComponents_)/totalVariation;
        }
        
#ifdef RBDEBUG
        std::cout << "We need " << neededComponents_
        << " principal components to cover the desired total variation of "
        << desiredPercentage << " percent.\n";
#endif
        
        if (minimumSize!=-1)
            // make shure a minimum amount of information is incorporated into the basis
            neededComponents_=std::max(int(neededComponents_),minimumSize);
        
        return neededComponents_;
    }
    
    /** This computes the number of needed principal components to
     *  ensure the mean quadratic projection error to be below a given tolerance.
     */
    const int computeNeededNumberByError(const double tolerance) {
        const EigenvalueVectorType& eigenvalues = eigenvalueSolver_.eigenvalues();
        neededComponents_ = matrix_.cols();
        double errorMade = eigenvalues(0);
        int i=1;
        while (errorMade < tolerance && i<eigenvalues.rows()) {
            --neededComponents_;
            errorMade += eigenvalues(i);
            ++i;
        }
        return neededComponents_;
    }
    
    template<class EigenMatrixOutputType>
    void computePrincipalComponents(EigenMatrixOutputType& principalComponents) {

         if (eigenvalueSolver_.eigenvalues().rows()==0)
             DUNE_THROW(InvalidStateException, "You need to call computeEigenvalues() before computing the principal components!");
         if (neededComponents_==0)
             DUNE_THROW(InvalidStateException, "You need to call computeNeededPrincipalComponents() before computing the principal components!");
        
        // "Convert the source data to z-scores"
        matrix_ /= covarianceMatrix_.diagonal().cwiseSqrt().sum();

        principalComponents = eigenvalueSolver_.eigenvectors().rightCols(neededComponents_);
        principalComponents = matrix_ * principalComponents;

        /* ATTENTION: Please do not change the above two lines to the ones below, as it raises the
        * static error
        * YOU_ARE_TRYING_TO_USE_AN_INDEX_BASED_ACCESSOR_ON_AN_EXPRESSION_THAT_DOES_NOT_SUPPORT_THAT
        * during compilation of my laplaceccfvserver programme. (Martin)
        *
        * TODO: Find out, why the above error is raised by the line below.
        * */
        // "Project the z-scores of the data onto the new basis"
        //principalComponents = matrix_*eigenvalueSolver_.eigenvectors().rightCols(neededComponents_);
        return;
    }

private:
    EigenMatrixInputType matrix_;
    Eigen::MatrixXd covarianceMatrix_;
    int neededComponents_;
    EigenvalueSolverType eigenvalueSolver_;
};

} // namespace RB
} // namespace Dune




// we use std::accumulate and std::for_each
//#include <numeric>
//#include <algorithm>

// #include <dune/rb/misc/discfunclist/discfunclistsubsetwrapper.hh>
// #include <dune/rb/misc/cwrapper/cmatrixwrapper.hh>
// #include <dune/rb/rbasis/gramian/gramianpipeline.hh>
// #include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
// #include <dune/rb/rbasis/twophaseflow/helpers/pressanykey.hh>


//#include "scalar_product.hh"

// include lapack
//#include <dune/rb/misc/clapack.h>






//  template<class ArgumentFunctionListImp>
//  class PCA {
//    typedef ArgumentFunctionListImp                                 ArgumentFunctionListType;
//    typedef ArgumentFunctionListType                                DestinationFunctionListType;
//    typedef typename ArgumentFunctionListType::DiscreteFunctionType DiscreteFunctionType;
//    typedef DiscFuncListSubSetWrapper<ArgumentFunctionListType>     SubSetWrapperType;
//    typedef CMatrixWrapper                                          MatrixWrapperType;
//    typedef GramianPipeline<SubSetWrapperType, MatrixWrapperType>   GramianPipelineType;
//    typedef typename GramianPipelineType::OpHandle                  OpHandleType;
//    typedef typename DiscreteFunctionType::DofIteratorType          DofIteratorType;
//    typedef typename DiscreteFunctionType::ConstDofIteratorType     ConstDofIteratorType;
//
//
//  public:
//    PCA(ArgumentFunctionListType& argumentList,
//        const int support=-1)
//      : argumentList_(argumentList),
//        support_(support),
//        subSetWrapper_(argumentList_, Parameter::getValue<unsigned int>("pca.subsetwrapperstepsize", 1)),
//        argumentListSize_(subSetWrapper_.size()),
//        eigenvalues_(0),
//        gramArray_(0),
//        gramMatrix_(0)
//    {
//      assert(subSetWrapper_.size()>0);
//    }
//
//    ~PCA() {
//      delete gramArray_;
//      gramArray_ = 0;
//      delete gramMatrix_;
//      gramMatrix_ = 0;
//      delete[] eigenvalues_;
//      eigenvalues_ = 0;
//    }
//
//    void subtractMean() {
//      // Calculate the deviations from the mean
//      DiscreteFunctionType temp("temp", argumentList_.space());
//      DiscreteFunctionType temp2("temp2", argumentList_.space());
//      const int subSetWrapperSize = subSetWrapper_.size();
//
//      for (int i=0; i!=subSetWrapperSize; ++i) {
//        temp.clear();
//        subSetWrapper_.getFunc(i, temp);
//        if (support_!=-1) {
//          temp2.setGridPartition(temp.gridPartition());
//          temp2.setSupport(support_);
//        }
//        // calc the mean
//        double mean = std::accumulate(temp.dbegin(), temp.dend(), 0.0);
//        mean /= temp.size();
//        Subtract subtract(mean);
//        std::for_each(temp.dbegin(), temp.dend(), subtract);
//        if (support_!=-1) {
//          temp2.clear();
//          restrictFunction(temp, temp2);
//          subSetWrapper_.setFunc(i, temp2);
//        }
//        else
//          subSetWrapper_.setFunc(i, temp);
//      }
//    }
//
//    const MatrixWrapperType& computeGramMatrix() {
//      //! \todo change this to reasonable value
//      const unsigned int blockSize = Parameter::getValue<int>("gramianpipeline.blocksize", 10);
//      GramianPipelineType pipeline(subSetWrapper_, blockSize);
//
//      OpHandleType hIdOp = pipeline.getIdentityHandle();
//
//      //Compute Gram-Matrix and save it in vec
//      unsigned int gramMatrixSize = argumentListSize_ * argumentListSize_;
//      gramArray_ = new double[gramMatrixSize];
//      gramMatrix_ = new MatrixWrapperType(&gramArray_[0], argumentListSize_, argumentListSize_);
//
//      pipeline.addGramMatrixComputation(hIdOp, hIdOp, *gramMatrix_);
//      pipeline.run();
//
//      //save some matrix entries, will be needed later
//      for (int i=0; i!=argumentListSize_; ++i) {
//        assert(gramArray_[i*argumentListSize_+i]>0);
//        stdDeviations_.push_back(sqrt(gramArray_[i*argumentListSize_+i]));
//      }
//      return *gramMatrix_;
//    }
//
//
//    /**
//
//     *  @remark This uses Lapack! 
//     *  lapack_ptrdiff_t is set in clapack.c to the correct argument type by the
//     *  used lapack implementation (either distributed lapack or Matlab's
//     *  lapack)
//     */
//    const double* computeEigenvalues() {
//      if (gramArray_==0)
//        DUNE_THROW(InvalidStateException, "You need to call computeGramMatrix() before computing the eigenvalues!");
//      // prepare work space
//      eigenvalues_ = new double[argumentListSize_];
//
//      /*size for the workspace used by dsyev, set to
//        -1 first so dsyev will guess the right size*/
//      typedef Lapack :: lapack_ptrdiff_t ptrdiffType;
//      ptrdiffType lwork = -1;
//      ptrdiffType n = argumentListSize_; //order of the gram matrix
//      char jobz = 'v'; //calculate eigenvalues and eigenvectors
//      char uplo = 'l'; //use upper triangular matrix
//
//      //allocate memory for lapack, first with size 1
//      double* workHere = new double[1];
//      //return value for lapack
//      ptrdiffType info; //info = 0 means everthing worked out
//
//      //call Lapack
//      Lapack :: dsyev_(&jobz, &uplo, &n, &gramArray_[0], &n,
//                       &eigenvalues_[0], &workHere[0], &lwork, &info);
//
//      /*after calling dsyev with lwork=-1 workHere[0] contains the
//        right size for workHere*/
//      lwork = static_cast<ptrdiffType>(workHere[0]);
//      assert(lwork>0);
//      delete workHere;
//      workHere = new double[lwork];
//      //finally call lapack and compute the eigenvalues
//      Lapack :: dsyev_(&jobz, &uplo, &n, &gramArray_[0], &n,
//                       &eigenvalues_[0], &workHere[0], &lwork, &info);
//      delete[] workHere;
//
//      // error handling
//      if (info<0)
//        DUNE_THROW(InvalidStateException, "Gram matrix had illegal value!");
//      if (info>0)
//        DUNE_THROW(InvalidStateException, "Eigenvalue algorithm failed to converge!");
//
//      return eigenvalues_;
//    }
//
//    double getTotalVariation() const {
//      return std::accumulate(eigenvalues_, eigenvalues_+argumentListSize_, 0.0);
//    }
//
//    const int computeNeededNumberByPercentage(const int desiredPercentage,
//                                               const int minimumSize=-1) {
//      neededComponents_ = 1;
//      double totalVariation = getTotalVariation();
//      // compute the percentage of the total variation that the first pc covers
//      double currentPercentage = eigenvalues_[argumentListSize_-1]/totalVariation;
//      // while the desired percentage is not reached, enlarge the set of pc's that
//      // will be returned
//      while (currentPercentage<desiredPercentage && neededComponents_<argumentListSize_) {
//        currentPercentage += eigenvalues_[argumentListSize_-1-neededComponents_]/totalVariation;
//        ++neededComponents_;
//      }
//
//#ifdef RBDEBUG
//      std::cout << "We need " << neededComponents_
//                << " principal components to cover the desired total variation of "
//                << desiredPercentage << " percent.\n";
//#endif
//      
//      if (minimumSize!=-1)
//        // make shure a minimum amount of information is incorporated into the basis
//        neededComponents_=std::max(int(neededComponents_),minimumSize);
//
//      return neededComponents_;
//    }
//
//    /** This computes the number of needed principal components to
//     *  ensure the mean quadratic projection error to be below a given tolerance.
//     */
//    const int computeNeededNumberByError(const double tolerance) {
//      neededComponents_ = argumentListSize_;
//      double errorMade = eigenvalues_[0];
//      int i=1;
//      while (errorMade < tolerance) {
//        --neededComponents_;
//        errorMade += eigenvalues_[i];
//        ++i;
//      }
//      return neededComponents_;
//    }
//
//    void computePrincipalComponents(DestinationFunctionListType& destinationList) const {
//      if (eigenvalues_==0)
//        DUNE_THROW(InvalidStateException, "You need to call computeEigenvalues() before computing the principal components!");
//      if (neededComponents_==0)
//        DUNE_THROW(InvalidStateException, "You need to call computeNeededPrincipalComponents() before computing the principal components!");
//      
//      DiscreteFunctionType buffer("buffer", argumentList_.space());
//      DiscreteFunctionType newBaseFunc("Principal_component", argumentList_.space());
//
//      for (int l=0; l!=neededComponents_; ++l) {
//        buffer.clear();
//        //get eigenvector to (uSize-l)-largest eigenvalue
//        double dof[argumentListSize_];
//        /*copy from array of double to DiscreteFunction,
//          remember: last vector is the vector to the largest eigenvalue!*/
//        int j=0;
//        for (int i=argumentListSize_*(argumentListSize_-l-1);
//             i!=argumentListSize_*(argumentListSize_-l); ++i, ++j)
//          dof[j] = gramArray_[i];
//
//        newBaseFunc.clear();
//        double *eigenvecDof = dof;
//
//        // project the eigenvector back to the span of subSetWrapper
//        for (int i=0; i!=argumentListSize_; ++i, ++eigenvecDof) {
//          buffer.clear();
//          subSetWrapper_.getFunc(i, buffer);
//          // "normalize the data set with respect to its variance"
//          *eigenvecDof/=stdDeviations_[i];
//          //          printf("Standart deviation is: %E \n", stdDeviation[i]);
//          DofIteratorType newBaseFuncDof = newBaseFunc.dbegin();
//          ConstDofIteratorType uDof = buffer.dbegin();
//
//          for (size_t j=0; j!=(unsigned)buffer.size(); ++j, ++newBaseFuncDof, ++uDof) {
//            *newBaseFuncDof += (*eigenvecDof)*(*uDof);
//          }
//        }
//
//      if (support_!=-1)
//        newBaseFunc.setSupport(support_);
//      destinationList.push_back(newBaseFunc);
//    }
//      return;
//    }
//
//    void plotEigenvalues(Gnuplot& plotter) const {
//      std::vector<double> eigenvaluesVector(eigenvalues_, eigenvalues_+argumentListSize_);
//      std::reverse(eigenvaluesVector.begin(),eigenvaluesVector.end());
//      std::vector<double> xVals;
//      for (double i=1.0; i<=double(eigenvaluesVector.size());  i+=1.0)
//        xVals.push_back(i);
//      std::stringstream name;
//      name << "Subdomain " << support_;
//      plotter.set_ylogscale();
//      plotter.set_style("points").plot_xy(xVals, eigenvaluesVector, name.str().c_str());
//    }
//
//  private:
//
//    struct Subtract {
//      Subtract(double sub):sub_(sub){};
//      void operator() (double& x) {
//        x -= sub_;
//      }
//    private:
//      double sub_;
//    };
//
//
//    ArgumentFunctionListType& argumentList_;
//    const int                 support_;
//    SubSetWrapperType         subSetWrapper_;
//    const int                 argumentListSize_;
//    double*                   eigenvalues_;
//    double*                   gramArray_;
//    MatrixWrapperType*        gramMatrix_;
//    int                       neededComponents_;
//    std::vector<double>       stdDeviations_;
//  };
//
//
//
//  template<class DiscreteFunctionListType>
//  void easyPCA(const DiscreteFunctionListType &argumentList_, DiscreteFunctionListType& destinationList_,
//               const int support=-1, const int minimumSize=-1) {
//    PCA<DiscreteFunctionListType> pca(argumentList_, support);
//    pca.computeGramMatrix();
//    pca.computeEigenvalues();
//    const double desiredPercentage = Parameter::getValue<double>("pca.desiredPercentage", 1.0);
//    pca.computeNeededNumberByPercentage(desiredPercentage, minimumSize);
//    pca.computePrincipalComponents(destinationList_);
//    return;
//  }































//  namespace PCANamespace {
//    struct Subtract {
//      Subtract(double sub):sub_(sub){};
//      void operator() (double& x) {
//        x -= sub_;
//      }
//    private:
//      double sub_;
//   };
//  }
//
//
///** @brief pca fixspace algorithm
// *
// *  @note This uses Lapacks dsyev.
// *
// *  @todo can we maybe put his method inside a linear algebra class?
// *
// *  @param[in]        U         a list of functions, a function in span(U)
// *                              will be used to expand the space specified by oldSpace
// *  @param[out]       princComp the principal components, ordered by their significance                  
// *  @param[in]        support   the support of the given functions
// *
// *  @tparam DiscreteFunctionListType The type of the function lists for input and output.
// */
//template< class DiscreteFunctionListType>
//void pca(DiscreteFunctionListType &U, DiscreteFunctionListType &princComp, const int support=-1,
//         const int minSize=-1)
//{
//  const double desiredPercentage = Parameter::getValue<double>("pca.desiredPercentage", 1.0);
//  //assert(desiredPercentage<=1.0 && desiredPercentage>=0);
//  typedef typename DiscreteFunctionListType::DiscreteFunctionType DiscreteFunctionType;
//  // Wrap U to run PCA only on a subset
//  unsigned int subSetStepSize = 
//    Parameter::getValue<unsigned int>("pca.subsetwrapperstepsize", 1);
//  typedef DiscFuncListSubSetWrapper<DiscreteFunctionListType> SubSetWrapper;
//  SubSetWrapper subSetWrapper(U, subSetStepSize);
//  int subSetWrapperSize = subSetWrapper.size();
//  assert(subSetWrapperSize>0);
//  // Calculate the deviations from the mean
//  DiscreteFunctionType temp("temp", U.space());
//  DiscreteFunctionType temp2("temp2", U.space());
//
//  if (Parameter::getValue<bool>("pca.substractMean", false)) {
//    for (int i=0; i!=subSetWrapperSize; ++i) {
//      temp.clear();
//      subSetWrapper.getFunc(i, temp);
//      if (support!=-1) {
//        temp2.setGridPartition(temp.gridPartition());
//        temp2.setSupport(support);
//      }
//      // calc the mean
//      double mean = std::accumulate(temp.dbegin(), temp.dend(), 0.0);
//      mean /= temp.size();
//      PCANamespace::Subtract subtract(mean);
//      std::for_each(temp.dbegin(), temp.dend(), subtract);
//      if (support!=-1) {
//        temp2.clear();
//        restrictFunction(temp, temp2);
//        subSetWrapper.setFunc(i, temp2);
//      }
//      else
//        subSetWrapper.setFunc(i, temp);
//    }
//  }
//
//  unsigned int uSize = subSetWrapper.size();
//
//  DiscreteFunctionType buffer1("buffer1", U.space());
//
//  typedef GramianPipeline< SubSetWrapper, CMatrixWrapper >  GramianPipelineType;
//  typedef typename GramianPipelineType :: OpHandle          OpHandle;
//  //! \todo change this to reasonable value
//  const unsigned int blockSize = Parameter::getValue<int>("gramianpipeline.blocksize", 10);
//  GramianPipelineType pipeline(subSetWrapper, blockSize);
//
//  OpHandle hIdOp = pipeline.getIdentityHandle();
//
//  //Compute Gram-Matrix and save it in vec
//  unsigned int gramMatrixSize = uSize * uSize;
//  double * gramMatrix = new double[gramMatrixSize];
//  CMatrixWrapper gramMatrixWrapper(&gramMatrix[0], uSize, uSize);
//  pipeline.addGramMatrixComputation(hIdOp, hIdOp, gramMatrixWrapper);
//  pipeline.run();
//
//  //save some matrix entries, will be needed later
//  std::vector<double> stdDeviation;
//  for (unsigned int i=0; i!=uSize; ++i) {
//    assert(gramMatrix[i*uSize+i]>0);
//    stdDeviation.push_back(sqrt(gramMatrix[i*uSize+i]));
//  }
//
//
//
//  // #if defined(HAVE_LAPACK) && HAVE_LAPACK
//  double eigenvalues[uSize];
//  //calculate eigenvalues
//
//  /*  lapack_ptrdiff_t is set in clapack.c to the correct argument type by the
//   *  used lapack implementation (either distributed lapack or Matlab's
//   *  lapack)*/
//
//  /*size for the workspace used by dsyev, set to
//    -1 first so dsyev will guess the right size*/
//  typedef Lapack :: lapack_ptrdiff_t ptrdiffType;
//  ptrdiffType lwork = -1;
//  ptrdiffType n = uSize; //order of the gram matrix
//  char jobz = 'v'; //calculate eigenvalues and eigenvectors
//  char uplo = 'l'; //use upper triangular matrix
//
//  //allocate memory for lapack, first with size 1
//  double* workHere = new double[1];
//  //return value for lapack
//  ptrdiffType info; //info = 0 means everthing worked out
//
//  //call Lapack
//  Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
//                   &eigenvalues[0], &workHere[0], &lwork, &info);
//
//  /*after calling dsyev with lwork=-1 workHere[0] contains the
//    right size for workHere*/
//  lwork = static_cast<ptrdiffType>(workHere[0]);
//  assert(lwork>0);
//  delete workHere;
//  workHere = new double[lwork];
//  //finally call lapack and compute the eigenvalues
//  Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
//                   &eigenvalues[0], &workHere[0], &lwork, &info);
//  if (info == 0) {
//    // plot the eigenvalues
//    std::stringstream name;
//    name << "eigenvalues_SD" << support << ".dat";
//    std::ofstream eigenvalFile(name.str().c_str());
//    for (int e=0; e!=uSize; ++e)
//      eigenvalFile << eigenvalues[e] << std::endl;
//    eigenvalFile.flush();
//    eigenvalFile.close();
//
//
//    /* Decide, how many functions are needed to achieve the desired percentage 
//       of total variation */
//    // the number of functions to be added
//    unsigned int L=1;
//    // compute the total variation covered by ALL principal components
//    double totalVariation = 0.0;
//    for (unsigned int i=0; i!=uSize; ++i)
//      totalVariation += eigenvalues[i];
//    // compute the percentage of the total variation that the first pc covers
//    double currentPercentage = eigenvalues[uSize-1]/totalVariation;
//    // while the desired percentage is not reached, enlarge the set of pc's that
//    // will be returned
//    while (currentPercentage<desiredPercentage && L<uSize) {
//        currentPercentage += eigenvalues[uSize-1-L]/totalVariation;
//        ++L;
//#ifdef RBDEBUG
//        if (L==uSize) {
//          std::cout << "Reached maximum number of principal components!\n";
//        }
//#endif
//    }
//#ifdef RBDEBUG
//    std::cout << "We need " << L
//              << " principal components to cover the desired total variation of "
//              << desiredPercentage << " percent.\n";
//#endif
//
//    if (minSize!=-1)
//      // make shure a minimum amount of information is incorporated into the basis
//      L=std::max(int(L),minSize);
//
//
//    for (unsigned int l=0; l!=L; ++l) {
//      buffer1.clear();
//      typedef typename DiscreteFunctionType :: DofIteratorType       DofIterator;
//      typedef typename DiscreteFunctionType :: ConstDofIteratorType  ConstDofIterator;
//      //get eigenvector to (uSize-l)-largest eigenvalue
//      double dof[uSize];
//      /*copy from array of double to DiscreteFunction,
//        remember: last vector is the vector to the largest eigenvalue!*/
//      int j=0;
//      for (unsigned int i=uSize*(uSize-l-1); i!=uSize*(uSize-l); ++i, ++j)
//        dof[j] = gramMatrix[i];
//
//      DiscreteFunctionType newBaseFunc("newBaseFunc", U.space());
//      newBaseFunc.clear();
//      double *eigenvecDof = dof;
//
//      //project the eigenvector back to the span of subSetWrapper
//      for (unsigned int i=0; i!=uSize; ++i, ++eigenvecDof) {
//        buffer1.clear();
//        subSetWrapper.getFunc(i, buffer1);
//        //"normalize the data set with respect to its variance"
//        *eigenvecDof/=stdDeviation[i];
//        //          printf("Standart deviation is: %E \n", stdDeviation[i]);
//        DofIterator newBaseFuncDof = newBaseFunc.dbegin();
//        ConstDofIterator uDof = buffer1.dbegin();
//
//        for (size_t j=0; j!=(unsigned)buffer1.size(); ++j, ++newBaseFuncDof, ++uDof) {
//         *newBaseFuncDof += (*eigenvecDof)*(*uDof);
//
//        }
//      }
//
//      if (support!=-1)
//        newBaseFunc.setSupport(support);
//      princComp.push_back(newBaseFunc);
//    }
//  } // if (info == 0)
//  else if (info<0) {
//    std::cerr << "Argument " << info << " of gram matrix had illegal value!";
//    assert(false);
//  }
//  else if (info>0) {
//    std::cerr << "Eigenvalue algorithm failed to converge!";
//    assert(false);
//  }
//// #else // if HAVE_LAPACK is not defined
//
//// #error "You need Lapack and the macro \"HAVE_LAPACK\" defined to use this!"
//
//// #endif //ifdef LAPACK
//
//  delete gramMatrix;
//
//} //pca
//#endif

#endif /* __PCA_HH__ */

