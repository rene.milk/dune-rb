#include <config.h>


  // grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>


  // fem stuff
  //#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

  // rb stuff
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
using namespace Dune;

#ifndef POLORDER 
const int polynomialOrder = 2;
#else 
const int polynomialOrder = POLORDER;
#endif

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType          GridType;
    typedef LevelGridPart<GridType>         LevelGridPartType;
    typedef LeafGridPart<GridType>          LeafGridPartType;
    
    typedef FunctionSpace<double, double, GridType::dimension, 1>                                    FunctionSpaceType;
    typedef FiniteVolumeSpace<FunctionSpaceType, LeafGridPartType, polynomialOrder>         LeafDiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<LeafDiscFuncSpaceType>                                          LeafDiscreteFunctionType;
    typedef FiniteVolumeSpace<FunctionSpaceType, LevelGridPartType, polynomialOrder>        LevelDiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<LevelDiscFuncSpaceType>                                         LevelDiscreteFunctionType;
    
    typedef LevelDiscFuncSpaceType::EntityType EntityType;

    
    MPIManager::initialize(argc, argv);
   
    GridPtr<GridType> gridPtr("/Users/svenkaulmann/Documents/diploma/dune/dune-rb/dune/rb/rbasis/twophaseflow/grid/unitcube2.dgf");
    Parameter::append("/Users/svenkaulmann/Documents/diploma/dune/dune-rb/dune/rb/rbasis/twophaseflow/tests/shockwave/shockwave.params");
    GridType &grid = *gridPtr;
      // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("grid.globalRefine", 0));

    LevelGridPartType levelGridPart(grid, 0);
    LeafGridPartType leafGridPart(grid);
    
    LeafDiscFuncSpaceType fineSpace(leafGridPart);
    LevelDiscFuncSpaceType coarseSpace(levelGridPart);
    
    LeafDiscreteFunctionType fineFunction("fine", fineSpace);
    LevelDiscreteFunctionType coarseFunction("coarse", coarseSpace);
    
    int foo=1;
    LeafDiscreteFunctionType::DofIteratorType end = fineFunction.dend();
    for (LeafDiscreteFunctionType::DofIteratorType dofit = fineFunction.dbegin(); dofit!=end; ++dofit) {
      *dofit = foo++;
    }
    foo = 1;
    LevelDiscreteFunctionType::DofIteratorType lend = coarseFunction.dend();
    for (LevelDiscreteFunctionType::DofIteratorType dofit2 = coarseFunction.dbegin(); dofit2!=lend; ++dofit2) {
      *dofit2 = foo++;
    }
    
    LevelDiscFuncSpaceType::IteratorType firstCellIterator = coarseSpace.begin();
      // iterate over children of first element
    EntityType& firstCell = *firstCellIterator;
    EntityType::HierarchicIterator childEnd = firstCell.hend(grid.maxLevel());
    for (EntityType::HierarchicIterator it = firstCell.hbegin(grid.maxLevel()); it!=childEnd; ++it) {
      EntityType& entity = *it;
      if (entity.level()==grid.maxLevel()) {
        LeafDiscreteFunctionType::LocalFunctionType fineLocal = fineFunction.localFunction(entity);
        for (int i=0; i!=fineLocal.numDofs(); ++i) {
          fineLocal[i]=0;
        }
      }
    }
    
      // iterate over leaf intersections of first element
    EntityType::LeafIntersectionIterator leafIntersectionsEnd = firstCell.ileafend();
    for (EntityType::LeafIntersectionIterator iit=firstCell.ileafbegin(); iit!=leafIntersectionsEnd; ++iit) {
      std::cout << "Auf intersection!\n";
    }
    
    
    RB::FunctionPlotter<LeafDiscreteFunctionType> finePlotter(fineFunction);
    RB::FunctionPlotter<LevelDiscreteFunctionType> coarsePlotter(coarseFunction);
    finePlotter.plot();
    coarsePlotter.plot();
        
  } catch (Dune::Exception e) {
    std::cerr << e.what();
    return 1;
  }  
  return 0;
}

