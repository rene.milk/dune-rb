#include <config.h>

#include <dune/common/mpihelper.hh>
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/misc/mpimanager.hh>
// rb stuff
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>

#include <dune/grid/multidomaingrid.hh>
using namespace Dune;

int main(int argc, char *argv[]) {
  Dune::MPIManager::initialize(argc, argv);
  try {
    typedef GridSelector::GridType GridType;

    GridPtr<GridType> gridPtr("tentimesten_3D.dgf");
    Parameter::append("mdgridparttest.params");
    GridType &grid = *gridPtr;

    typedef MultiDomainGrid<GridType,mdgrid::FewSubDomainsTraits<GridType::dimension,4> > MDGridType;
    MDGridType mdGrid(grid);

    typedef MDGridType::LeafGridView GridView;
    GridView gv = mdGrid.leafView();
    typedef GridView::Codim<0>::Iterator Iterator;
    typedef GridView::Codim<2>::Iterator VIterator;
    typedef GridView::Codim<0>::Entity Entity;
    typedef GridView::Codim<0>::Geometry Geometry;
    mdGrid.startSubDomainMarking();
    for (Iterator it = gv.begin<0>(); it != gv.end<0>(); ++it) {
      const Entity& e = *it;
      //IndexSet::SubDomainSet& sds = is.subDomainSet(e);
      Dune::FieldVector<GridType::ctype,3> c 
        = e.geometry().global(Dune::GenericReferenceElements<GridType::ctype,3>::general(e.type()).position(0,0));
      double x = c[0];
      double y = c[1];
      if (x < 5) {
        if (y < 5)
          mdGrid.addToSubDomain(0,e);
        else // if (y >= 5)
          mdGrid.addToSubDomain(1,e);
      } else {
        if (y<5)
          mdGrid.addToSubDomain(2,e);
        else 
          mdGrid.addToSubDomain(3,e);
      }
    }
  
    mdGrid.preUpdateSubDomains();
    mdGrid.updateSubDomains();
    mdGrid.postUpdateSubDomains();
  
    MDGridType::SubDomainGrid& subDomainOne = mdGrid.subDomain(1);
    typedef AdaptiveLeafGridPart<MDGridType::SubDomainGrid> SubDomainOneGPType;
    SubDomainOneGPType mdGridPart(subDomainOne);

    typedef FunctionSpace<double, double, GridType::dimension, 1>                FunctionSpaceType;
    typedef FiniteVolumeSpace<FunctionSpaceType, SubDomainOneGPType, 0>          LeafDiscFuncSpaceType;
    typedef AdaptiveDiscreteFunction<LeafDiscFuncSpaceType>                      LeafDiscreteFunctionType;

    LeafDiscFuncSpaceType sd1funcSpace(mdGridPart);
    LeafDiscreteFunctionType sd1func("SubDomain 1", sd1funcSpace);
    RB::FunctionPlotter<LeafDiscreteFunctionType> plotSDOne(sd1func);
    plotSDOne.plot();
    MDGridType::SubDomainGrid& subDomainTwo = mdGrid.subDomain(2);

    typedef MDGridType::Traits::Codim<1>::LeafIterator MyLeafIntersectionIteratorType;
    MyLeafIntersectionIteratorType end = mdGrid.leafend<1>();
    for (MyLeafIntersectionIteratorType it=mdGrid.leafbegin<1>(); it!=end; ++it) {
      std::cout << it->geometry().center();
    }
    std::cout << "\n\n\n";
    typedef MDGridType::LeafAllSubDomainInterfacesIterator LeafSubIteratorType;
    LeafSubIteratorType subEnd = mdGrid.leafAllSub  DomainInterfacesEnd();
    for (LeafSubIteratorType it = mdGrid.leafAllSubDomainInterfacesBegin(); it!=subEnd; ++it) {
      std::cout << it->geometry().center();
    }
    
      //LeafDiscreteFunctionType::LocalFunctionType oneLocal = sd1func.localFunction(*(subDomainTwo.leafbegin<0>()));

  } catch (Dune::Exception e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
