#ifndef __OPERATOR_BASE__
#define __OPERATOR_BASE__

// std stuff
#include <sstream>

//- Dune includes
#include <dune/common/fmatrix.hh>
#include <dune/istl/io.hh>
#include <dune/common/fmatrixev.hh>

// fem includes
#include <dune/fem/storage/array.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/function/common/scalarproducts.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/2order/lagrangematrixsetup.hh>
#include <dune/fem/operator/2order/dgmatrixsetup.hh>
#include <dune/fem/solver/oemsolver/oemsolver.hh>

// dune rb stuff
#include <dune/rb/rbasis/twophaseflow/helpers/addsparsematrices.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/executiontimer.hh>
#include <dune/rb/misc/discfunclist/discfunclist_xdr.hh>
#include <dune/rb/misc/discfunclist/discfunclist_mem.hh>
//#include <dune/stuff/matrix.hh>

// boost stuff
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/operations.hpp>

// stuff stuff
//#include <dune/stuff/printing.hh>

// eigen stuff
#include <Eigen/Dense>
#include <Eigen/Eigen>
#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#include <Eigen/Sparse>
#include <Eigen/SuperLUSupport>

namespace Dune
{
namespace RB
{


template< class DiscreteFunctionSpaceImpl, class ProblemImpl, class Implementation >
class OperatorBase {
    typedef OperatorBase< DiscreteFunctionSpaceImpl, ProblemImpl, Implementation > ThisType;

public:
    typedef ProblemImpl ProblemType;
    typedef Eigen::MatrixXd DiscreteFunctionListType;    
    
    //! type of discrete function space
    typedef DiscreteFunctionSpaceImpl DiscreteFunctionSpaceType;
    //! type of grid part
    typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
    //! type of grid
    typedef typename GridPartType::GridType GridType;
    typedef typename GridType::Traits::template Codim<0>::EntityPointer EntityPointer;
    typedef typename EntityPointer::Entity EntityType;
    typedef typename EntityType::Geometry GeometryType;
    typedef typename DiscreteFunctionSpaceType::IteratorType EntityIteratorType;
    typedef typename GridPartType::IntersectionIteratorType IntIteratorType;
    typedef typename IntIteratorType::Intersection IntersectionType;
    //! type of diffusion matrix
    typedef typename ProblemType::DiffusionMatrixType DiffusionMatrixType;
    //! field type of range
    typedef typename DiscreteFunctionSpaceType::RangeFieldType RangeFieldType;
    //! type of range
    typedef typename DiscreteFunctionSpaceType::RangeType RangeType;
    //! type of domain
    typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
    //! polynomial order of base functions
    enum { polynomialOrder = DiscreteFunctionSpaceType :: polynomialOrder };
    //! The grid's dimension
    enum { dimension       = GridType :: dimension };
    // We use a caching quadrature for codimension 0 entities
    typedef CachingQuadrature<GridPartType, 0>                          QuadratureType;
    // ... and also for codimension 1 entities
    typedef CachingQuadrature<GridPartType, 1>                          FaceQuadratureType;
    // The eigen::matrix type
    typedef Eigen::SparseMatrix<double> EigenMatrixType;
    typedef EigenMatrixType MatrixType;
    // The eigen::vector type
    typedef Eigen::VectorXd EigenVectorType;
    //! type of jacobian
    typedef typename DiscreteFunctionSpaceType::JacobianRangeType JacobianRangeType;

public:
    // types for boundary treatment
    // ----------------------------
    typedef typename DiscreteFunctionSpaceType :: MapperType MapperType;
    typedef SlaveDofs< DiscreteFunctionSpaceType, MapperType > SlaveDofsType;
    typedef typename SlaveDofsType :: SingletonKey SlaveDofsKeyType;      /*@\label{poi:singleton}@*/
    typedef SingletonList< SlaveDofsKeyType, SlaveDofsType >
    SlaveDofsProviderType;

public:
    //! constructor
    OperatorBase(const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem,
                 const std::string& dataPath,
                 const double& sigma)
        : discFuncSpace_( dfSpace ),
          gridPart_(discFuncSpace_.gridPart()),
          dataPath_(dataPath),
          rhsList_(DiscreteFunctionListType::
                   Zero(dfSpace.size(), 
                        1+problem.mobility().size()*problem.dirichletValues().size()+problem.neumannValues().size())),
          problem_( problem ),
          gradCache_( dfSpace.mapper().maxNumDofs() ),
          gradDiffusion_( dfSpace.mapper().maxNumDofs() ),
          sigma_(sigma),
          verbose_(Parameter::getValue<bool>("reducedoperator.verbose", false)),
          saveData_(false)
    {

        const int numLambdaComps = problem_.mobility().size();        
        const int cols = discFuncSpace_.size();
        for (int i = 0; i<numLambdaComps; ++i) {
            // add zero operators and rhs functions, will be filled in assemble()
            opComponentsEigen_.push_back(EigenMatrixType(cols,cols));
        }

        // reserve memory for matrices
        typedef typename std::vector<EigenMatrixType>::iterator OpVecIt;
        OpVecIt opEnd = opComponentsEigen_.end();
        for (OpVecIt it = opComponentsEigen_.begin(); it!=opEnd; ++it) {
            it->reserve(Eigen::VectorXi::Constant(cols,(GRIDDIM+1)*((2*GRIDDIM)+1)));
        }

        fineJumpTerm_ = EigenMatrixType(cols,cols);
        fineJumpTerm_.reserve(Eigen::VectorXi::Constant(cols,(GRIDDIM+1)*((2*GRIDDIM)+1)));
        coarseJumpTerm_ = EigenMatrixType(cols,cols);
        coarseJumpTerm_.reserve(Eigen::VectorXi::Constant(cols,(GRIDDIM+1)*((2*GRIDDIM)+1)));
      
        // create path
        boost::filesystem::create_directory(dataPath_);
    }

    ~OperatorBase () {}

protected:
    // prohibit copying
    OperatorBase( const ThisType & );

public:       
    EigenVectorType getLhComponentEigen(const unsigned int i) const {     
        return rhsList_.col(i);
    }

  const EigenMatrixType& getBhComponentEigen(const unsigned int i) const {
    assert(i>=0 && i<opComponentsEigen_.size());
    return opComponentsEigen_[i];
  }

    EigenMatrixType getBhComponentEigenHigh(const unsigned int i) const {
        assert(i>=0 && i<opComponentsEigen_.size());
        return opComponentsEigen_[i]+((sigma_)*coarseJumpTerm_)+((sigma_)*fineJumpTerm_);
    }

    
    const EigenMatrixType& getFineJumpComponent() const {
        return fineJumpTerm_;
    }

    const EigenMatrixType& getCoarseJumpComponent() const {
        return coarseJumpTerm_;
    }

    //! return reference to discFuncSpace
    const DiscreteFunctionSpaceType &discreteFunctionSpace () const
    {
        return discFuncSpace_;
    }

private:
    //! return reference to problem
    const ProblemType& problem() const
    {
        return problem_;
    }

public:
    EigenMatrixType& systemMatrixEigen() const {
        const int rows = discFuncSpace_.size();
        const int cols = rows;

        operatorSumEigen_.resize(rows,cols);
        operatorSumEigen_.reserve(Eigen::VectorXi::Constant(cols,(GRIDDIM+1)*((2*GRIDDIM)+1)));
        double sum = 0.0;
        const int numOpComps = problem_.mobility().size();
        for (int opComp=0; opComp!=numOpComps; ++opComp) {
            // add the component to the sum, scaled with the proper parameter
            const double parameter = problem_.mobility().coefficient(opComp);
            sum += parameter;
            operatorSumEigen_ += parameter*opComponentsEigen_[opComp];
        }
        operatorSumEigen_ += fineJumpTerm_*sigma_*sum;
        operatorSumEigen_ += coarseJumpTerm_*sigma_*sum;
        operatorSumEigen_.makeCompressed();
        return operatorSumEigen_;
    }

    EigenVectorType& rightHandSideEigen() const {
        rhsSumEigen_ = EigenVectorType::Zero(rhsList_.rows());
        
        rhsSumEigen_ += rhsList_.col(0);
        const int numGdComps = problem_.dirichletValues().size();
        const int numOpComps = problem_.mobility().size();
        for (int i=0; i!=numGdComps; ++i) {
            const double muForGd = problem_.dirichletValues().coefficient(i);
            for (int opComp=0; opComp!=numOpComps; ++opComp) {
                const int index = 1+i*numOpComps+opComp;
                assert(index<rhsList_.cols());
                rhsSumEigen_ += problem_.mobility().coefficient(opComp)*muForGd*rhsList_.col(index);
            }
        }
        for (int i=0; i!=problem_.neumannValues().size(); ++i) {
            const int index = 1+numGdComps*numOpComps+i;
            assert(index<rhsList_.cols());
            const double muForGn = problem_.neumannValues().coefficient(i);
            rhsSumEigen_ += muForGn*rhsList_.col(index);
        }

        return rhsSumEigen_;
    }

    /** \brief perform a grid walkthrough and assemble the global matrices
     *  and the right hand side.  */
    void assemble() const {
        DUNE_THROW(NotImplemented, "Call assemble only on the derived classes!");
        return;
    }

    const int numOperatorComponents() const {
        return problem_.mobility().size();
    }
    
    const int numRightHandSideComponents() const {
        return rhsList_.cols();
    }

public:
    void saveData() const {
        saveData_=true;
        saveMatrices();
        saveRightHandSide();
        return;
    }
    
    void readData() {
        readMatrices();
        readRightHandSide();
    }

protected:
    /**@brief save all matrices to disk.
     *
     */
    void saveMatrices() const {
        const int numOpComps = problem_.mobility().size();
        for (int opComp=0; opComp!=numOpComps; ++opComp) {
            std::string filename = dataPath_+"/opComp"
                                     +boost::lexical_cast<std::string>(opComp)+".bin";
            opComponentsEigen_[opComp].saveToBinaryFile(filename);
        }
        std::string filename = dataPath_+"/coarseJumps.bin";
        coarseJumpTerm_.saveToBinaryFile(filename);        
        filename = dataPath_+"/fineJumps.bin";
        fineJumpTerm_.saveToBinaryFile(filename);          
        return;
    }

    /**@brief read all matrices from disk.
     *
     */
    void readMatrices() {
        const int numOpComps = problem_.mobility().size();
        for (int opComp=0; opComp!=numOpComps; ++opComp) {
            std::string filename = dataPath_+"/opComp"
                                      +boost::lexical_cast<std::string>(opComp)+".bin";
            // (GRIDDIM+1)*((2*GRIDDIM)+1)
            opComponentsEigen_[opComp].loadFromBinaryFile(filename);
        }
        std::string filename = dataPath_+"/coarseJumps.bin";
        coarseJumpTerm_.loadFromBinaryFile(filename);        
        filename = dataPath_+"/fineJumps.bin";
        fineJumpTerm_.loadFromBinaryFile(filename);   
        return;
    }


    /**@brief save all right hand side functions to disk.
     */
    void saveRightHandSide() const {
        std::string filename = dataPath_ + "/rhs.bin";
        rhsList_.saveToBinaryFile(filename);
        return;

    }

    /**@brief read all right hand side functions from disk.
     *
     * @param[in] dataPath_ Name for the folder where the
     *            functions are saved.
     */
    void readRightHandSide() {
        std::string filename = dataPath_ + "/rhs.bin";
        rhsList_.loadFromBinaryFile(filename);
        return;
    }

    template<class JacobianRangeTypeVec>
    void jacobianAll(const EntityType& entity, const DomainType& quadPoint,
                     JacobianRangeTypeVec& jac) const {
        implementation()->jacobianAll(entity, quadPoint, jac);
        return;
    }

    template<class RangeTypeVec>
    void evaluateAll(const EntityType& entity, const DomainType& quadPoint,
                     RangeTypeVec& evals) const {
        implementation()->evaluateAll(entity, quadPoint, evals);
        return;
    }

    template<class RangeTypeVec>
    void evaluateJumpAll(const EntityType& inside, const EntityType& outside,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         RangeTypeVec& evals) const {
        implementation()->evaluateJumpAll(inside, outside, quadPointInInside, quadPointInOutside, evals);
        return;
    }

    template<class RangeTypeVec>
    void evaluateMeanAll(const EntityType& inside, const EntityType& outside,
                         const IntersectionType& intersection,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         const int opComp,
                         const DomainType& normal,
                         RangeTypeVec& evals) const {
        implementation()->evaluateMeanAll(inside, outside, intersection, quadPointInInside, quadPointInOutside, opComp, normal, evals);
        return;
    }

    RangeType sigma(const DomainType& xLocalInside,
                    const DomainType& xLocalOutside,
                    const IntersectionType& intersection,
                    const DomainType& normal) const {
        RangeType ret(0.0);
        EntityPointer insidePtr = intersection.inside();
        const EntityType &insideEn = *insidePtr;
        // Get the inside diffusion matrix
        typedef Eigen::Matrix<double, GRIDDIM, GRIDDIM> EigenMatrixType;
        EigenMatrixType Kinside;
        problem_.permeability(xLocalInside, insideEn, Kinside);
        
        Eigen::Matrix<double, GRIDDIM, 1> intersectionNormal;
        for (int i=0; i!=DomainType::size; ++i)
            intersectionNormal(i) = normal[i];
        // for definition of deltaMinus/deltaPlus see Ern:2009uq, page 5
        double deltaMinus = intersectionNormal.transpose()*Kinside*intersectionNormal;
        
        if (intersection.boundary()) {
            
//#if 0
//            Eigen::SelfAdjointEigenSolver<EigenMatrixType> insideEigensolver(Kinside);
//            Eigen::SelfAdjointEigenSolver<EigenMatrixType>::RealVectorType insideEigenvalues = insideEigensolver.eigenvalues();
//
//            // just for the moment, I'm not sure how eigen stores vectors
//            assert(insideEigenvalues.rows()==GRIDDIM);
//
//            // assert eigenvalues to be real
//            assert(fabs(std::imag(insideEigenvalues(0)))<1e-20 && fabs(std::imag(insideEigenvalues(dimension-1)))<1e-20);
//            double lambdaMinInside = std::real(insideEigenvalues(0));
//            double lambdaMaxInside = std::real(insideEigenvalues(dimension-1));
//
//            ret[0] = lambdaMaxInside*lambdaMaxInside/(lambdaMinInside);
//#endif
            ret[0]=deltaMinus;
            
        } else {

            EntityPointer outsidePtr = intersection.outside();
            const EntityType &outsideEn = *outsidePtr;
            // Get the outside diffusion matrix
            EigenMatrixType Koutside;
            problem_.permeability(xLocalOutside, outsideEn, Koutside);
            
//#if 0
//            // Get the mean of K
//            Koutside += Kinside;
//            Koutside *= 0.5;
//            // get the eigenvalues of Kmean
//            Eigen::SelfAdjointEigenSolver<EigenMatrixType> meanEigensolver(Koutside);
//            Eigen::SelfAdjointEigenSolver<EigenMatrixType>::RealVectorType meanEigenvalues = meanEigensolver.eigenvalues();
//            // assert eigenvalues to be real
//            assert(fabs(std::imag(meanEigenvalues(0)))<1e-20 && fabs(std::imag(meanEigenvalues(dimension-1)))<1e-20);
//            // just for the moment, I'm not sure how eigen stores vectors
//            assert(meanEigenvalues.rows()==GRIDDIM);
//
//            double lambdaMinMean = std::real(meanEigenvalues(0));
//            double lambdaMaxMean = std::real(meanEigenvalues(dimension-1));
//
//
//            ret[0] = lambdaMaxMean*lambdaMaxMean/(lambdaMinMean);
//#endif    
            double deltaPlus  = intersectionNormal.transpose()*Koutside*intersectionNormal;
            ret[0] = deltaPlus*deltaMinus/(deltaPlus+deltaMinus);
            
        }

//        ret[0] *= sigma_;// = std::max(ret[0], sigma_);
        ret[0]/=computeIntersectionDiameter(intersection);
        return ret;
    }


    double computeIntersectionDiameter(const IntersectionType& intersection) const {
      double diameter=0.0;
// todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
        DomainType firstCorner = intersection.geometry().corner(0);
        for (int i=1; i!=GRIDDIM; ++i) {
            DomainType corner = intersection.geometry().corner(i);
            corner -= firstCorner;
            diameter = std::max(diameter, corner.two_norm());
        }
#else
      diameter = sqrt(intersection.geometry().volume());      
#endif
      
        return diameter;       
    }

    Implementation* implementation() {
        return static_cast<Implementation*>(this);
    }

    const Implementation* implementation() const {
        return static_cast<const Implementation*>(this);
    }

protected:
    //! assemble local matrix for given entity
    void assembleOp( const EntityType &entity ) 
    {
        typedef typename EntityType::Geometry Geometry;

        // assert that matrix is not build on ghost elements
        assert( entity.partitionType() != GhostEntity );

        // cache geometry of entity
        const Geometry &geometry = entity.geometry();

        // get number of local base functions
        // create quadrature of appropriate order
        const int orderOfIntegrand = (polynomialOrder-1)*2+polynomialOrder+1;
        const int quadOrder = std::ceil((orderOfIntegrand+1)/2);
        QuadratureType quadrature(entity, quadOrder);

        // loop over all quadrature points
        const size_t numQuadraturePoints = quadrature.nop();
        for( size_t pt = 0; pt < numQuadraturePoints; ++pt ) {
            // get local coordinate of quadrature point
            const typename QuadratureType :: CoordinateType &x
                = quadrature.point( pt );

            DiffusionMatrixType K;
            // evaluate diffusion matrix
            problem().permeability(x, entity, K );

            this->jacobianAll(entity, quadrature.point(pt), gradCache_);
            const size_t numBaseFunctions = gradCache_.size();

            const int numOpComps = problem_.mobility().size();
            for (int opComp=0; opComp!=numOpComps; ++opComp) {

//                LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(entity, entity);

                // evaluate the current parameter-independent part of lambda
                RangeType lambda = problem().mobility().component(opComp, geometry.global(x));

                // apply diffusion tensor
                for( size_t i = 0; i < numBaseFunctions; ++i ) {
                    K.mv( gradCache_[ i ][ 0 ], gradDiffusion_[ i ][ 0 ] );
                    gradDiffusion_[ i ][ 0 ] *= lambda[0];
                }
                // evaluate integration weight
                weight_ = quadrature.weight( pt ) * geometry.integrationElement( x );

                for (unsigned int i=0; i!=numBaseFunctions; ++i)
                    for (unsigned int j=0; j!=numBaseFunctions; ++j) {
                        const int row = discFuncSpace_.mapper().mapToGlobal(entity, i);
                        const int col = discFuncSpace_.mapper().mapToGlobal(entity, j);
                        const RangeFieldType value = weight_ * (gradCache_[i][0]*gradDiffusion_[j][0]);
//                        localMatrix.add(i, j, value);
                        
                        opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                    }
            } // iteration over all operator components
        } // iteration over all quad points

        // assemble intersections parts
        const int insideIndex = gridPart_.grid().leafIndexSet().index(entity);
        IntIteratorType iend = gridPart_.iend(entity);
        for (IntIteratorType iit = gridPart_.ibegin(entity); iit!=iend; ++iit) {
            const IntersectionType &intersection = *iit;
            // dirichlet boundary parts
            if (intersection.boundary()) {
                if (problem_.boundaryType(intersection)==ProblemType::Dirichlet)
                  assembleOpOnBoundary(intersection);
            }
            // inner intersections
            else {
                EntityPointer outsidePtr = intersection.outside();
                const EntityType &outsideEn = *outsidePtr;
                // visit the intersection only once
                if (insideIndex>gridPart_.grid().leafIndexSet().index(outsideEn)) {
                    assembleOpOnIntersections(intersection);
                }
            }
        }
        return;
    }

    void assembleRHS(const EntityType& entity) const {

        const typename DiscreteFunctionSpaceType::MapperType &mapper = discFuncSpace_.mapper();
        
        // obtain a reference to the entity's geometry
        const GeometryType &geometry = entity.geometry();
        const int orderOfIntegrand = (polynomialOrder+1)+polynomialOrder;
        const int quadOrder = std::ceil((orderOfIntegrand+1)/2);
        QuadratureType quadrature(entity, quadOrder);
        const size_t numQuadraturePoints = quadrature.nop();
        // entity part
        for( size_t qP = 0; qP < numQuadraturePoints; ++qP ) {
            // get integration element multiplied with quadrature weight
            const double factor =
                geometry.integrationElement(quadrature.point(qP))*quadrature.weight(qP);
            // evaluate right hand side function
            DomainType xGlobal = geometry.global(quadrature.point(qP));
            RangeType phis = problem_.rightHandSide(xGlobal);
            // apply factor
            phis *= factor;

            // evaluate base functions
            std::vector<RangeType> evals;
            this->evaluateAll(entity, quadrature.point(qP), evals);
            for (unsigned int i=0; i!=evals.size(); ++i) {
                double entityIntegral = evals[i]*phis;
                
                // get global index
                const int row = mapper.mapToGlobal(entity, i);
//                nuIndieLF[i] += entityIntegral;
                rhsList_(row, 0) +=entityIntegral;
            }
        }
        // intersection, dirichlet-boundary part
        int numGdComps=problem_.dirichletValues().size();
        const int numOpComps = problem_.mobility().size();
        for (int gdComp=0; gdComp!=numGdComps; ++gdComp) {
            for (int opComp=0; opComp!=numOpComps; ++opComp) {
//                DiscreteFunctionType& temp2 =
//                rhsc_.getFunc(gdComp*numOpComps+opComp);
                // now: iterate over intersections
                IntIteratorType iend = gridPart_.iend(entity);
                for (IntIteratorType iit = gridPart_.ibegin(entity); iit!=iend; ++iit) {
                    const IntersectionType &intersection = *iit;

                    // get the inside entity, its index and the enclosing coarse cell
                    EntityPointer insidePtr = intersection.inside();
                    const EntityType &insideEn = *insidePtr;
                    const unsigned int insideIndex = gridPart_.grid().leafIndexSet().index(insideEn);

                    // if we are on a dirichlet boundary intersection
                    if (intersection.boundary() && problem_.boundaryType(intersection)==ProblemType::Dirichlet) {
                        
                        const int orderOfIntegrand = (polynomialOrder-1)+2*(polynomialOrder+1);
                        const int quadOrder = std::ceil((orderOfIntegrand+1)/2);
                        FaceQuadratureType faceQuad(gridPart_, intersection,
                                                    quadOrder, FaceQuadratureType::INSIDE );
                        const size_t numIntQuadPoints = faceQuad.nop();
                        // loop over all quadrature points
                        for (unsigned int iqP = 0; iqP!=numIntQuadPoints; ++iqP) {
                            // get local coordinate of quadrature point
                            const typename FaceQuadratureType::LocalCoordinateType &xLocal=faceQuad.localPoint(iqP);
                            const typename IntersectionType::Geometry &inGeo = intersection.geometry();
                          
                          
// todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
                          const typename IntersectionType::Geometry &insideGeometry = intersection.geometryInInside();
                          const typename FaceQuadratureType::CoordinateType &xInInside=insideGeometry.global(xLocal);
                          const typename FaceQuadratureType::CoordinateType &xGlobal=inGeo.global(xLocal);
# else
//                          const int faceIndexInInside = intersection.indexInInside();
//                          const int insideEntityIndex = gridPart_.grid().leafIndexSet().index(insideEn);
//                          int globalFaceIndex = gridPart_.grid().cellFace(insideEntityIndex, faceIndexInInside);
                          
                          typedef typename FaceQuadratureType::CoordinateType CoordinateType;
                          const CoordinateType xGlobal=intersection.geometry().center();
                          const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(0,0);
#endif
           
                            const double factor = inGeo.integrationElement(xLocal)*faceQuad.weight(iqP);

                            std::vector<RangeType> baseFuncEvals;
                            this->evaluateAll(insideEn, xInInside, baseFuncEvals);
                            // evaluate gradients of the inside base function set
                            std::vector<JacobianRangeType> tempGradPhii;
                            tempGradPhii.resize(baseFuncEvals.size());
                            this->jacobianAll(insideEn, xInInside, tempGradPhii);
                            RangeType gDPart = problem_.dirichletValues().component(gdComp, xGlobal);

                            //======================== Assemble all c's for the rhs ======================
                            // evaluate the boundary value function
                            RangeType value = gDPart;
                            // apply the factor
                            value *= factor;
                            value *= sigma(xInInside, xInInside, intersection, intersection.unitOuterNormal(xLocal));

//                            LocalFunctionType cLF = temp2.localFunction(entity);
                            for (unsigned int i=0; i!=baseFuncEvals.size(); ++i) {
                                // get global index
                                const int row = mapper.mapToGlobal(entity, i);
                                rhsList_(row, 1+gdComp*numOpComps+opComp) += value*baseFuncEvals[i];
//                                cLF[i]+=value*baseFuncEvals[i];
                            }
                            //========================  c's for the rhs done ======================

                            //=========================Assemble all d's for the rhs =====================
                            DiffusionMatrixType K;
                            // evaluate permeability matrix
                            problem_.permeability(xInInside, insideEn, K);
                            std::vector<JacobianRangeType> gradients(tempGradPhii.size(), JacobianRangeType(0.0));
                            // evaluate the current parameter-independent part of lambda
                            RangeType lambda = problem_.mobility().component(opComp, xGlobal);
                            // apply diffusion tensor
                            for(unsigned int i = 0; i < tempGradPhii.size(); ++i ) {
                                K.mv( tempGradPhii[ i ][ 0 ], gradients[ i ][ 0 ] );
                                gradients[ i ][ 0 ] *= lambda[0];
                                // get global index
                                const int row = mapper.mapToGlobal(entity, i);
                                rhsList_(row, 1+gdComp*numOpComps+opComp) -= 
                                factor*gDPart[0] * (gradients[i][0]*intersection.unitOuterNormal(xLocal));
//                                cLF[i] -= factor*gDPart[0] * (gradients[i][0]*intersection.unitOuterNormal(xLocal));
                            }
                            //========================== d parts for the rhs done ================================
                            //                  else if (problem_.boundaryType(intersection)==ProblemType::Neumann) {
                        } // loop over all intersection quadpoints
                    } // if dirichlet boundary
                } // loop over intersection
//#ifndef FUNCLIST_MEM
//                rhsc_.setFunc(gdComp*numOpComps+opComp, temp2);
//#endif
            } // iteration over mu all components
        } // loop over all g_d comps

        // assemble Neumann-part of rhs function
        int numGnComps=problem_.neumannValues().size();
        for (int gnComp=0; gnComp!=numGnComps; ++gnComp) {
//            DiscreteFunctionType& temp = rhse_.getFunc(gnComp);
            // now: iterate over intersections
            IntIteratorType iend = gridPart_.iend(entity);
            for (IntIteratorType iit = gridPart_.ibegin(entity); iit!=iend; ++iit) {
                const IntersectionType &intersection = *iit;
                // make shure we are on a neumann boundary intersection
                if (intersection.neighbor() || problem_.boundaryType(intersection)!=ProblemType::Neumann)
                    continue;

                const int orderOfIntegrand = (polynomialOrder+1)+polynomialOrder;
                const int quadOrder = std::ceil((orderOfIntegrand+1)/2);
                FaceQuadratureType faceQuad(gridPart_, intersection,
                                            quadOrder, FaceQuadratureType::INSIDE );
                const size_t numIntQuadPoints = faceQuad.nop();
                // loop over all quadrature points
                for (unsigned int iqP = 0; iqP!=numIntQuadPoints; ++iqP) {
                    // get local coordinate of quadrature point
                    const typename FaceQuadratureType::LocalCoordinateType &xLocal=faceQuad.localPoint(iqP);
                    const typename IntersectionType::Geometry &inGeo = intersection.geometry();
                  
                  
                  // todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
                  const typename IntersectionType::Geometry &insideGeometry = intersection.geometryInInside();
                  const typename FaceQuadratureType::CoordinateType &xInInside=insideGeometry.global(xLocal);
                  const typename FaceQuadratureType::CoordinateType &xGlobal=inGeo.global(xLocal);
# else
//                  const int faceIndexInInside = intersection.indexInInside();
//                  const int insideEntityIndex = gridPart_.grid().leafIndexSet().index(entity);
//                  int globalFaceIndex = gridPart_.grid().cellFace(insideEntityIndex, faceIndexInInside);
                  
                  typedef typename FaceQuadratureType::CoordinateType CoordinateType;
                  const CoordinateType xGlobal=intersection.geometry().center();
                  const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(entity.type()).position(0,0);
#endif
      
                    const double factor = inGeo.integrationElement(xLocal)*faceQuad.weight(iqP);

                    std::vector<RangeType> baseFuncEvals;
                    this->evaluateAll(entity, xInInside, baseFuncEvals);
                    RangeType gNPart = problem_.neumannValues().component(gnComp, xGlobal);

                    //======================== Assemble all c's for the rhs ======================
                    // evaluate the boundary value function
                    RangeType value = gNPart;
                    // apply the factor
                    value *= factor;
//                    LocalFunctionType eLF = temp.localFunction(entity);
                    for (unsigned int i=0; i!=baseFuncEvals.size(); ++i) {
                        // get global index
                        const int row = mapper.mapToGlobal(entity, i);
                        rhsList_(row, 1+numGdComps*numOpComps+gnComp)+=value*baseFuncEvals[i];
//                        eLF[i]+=value*baseFuncEvals[i];
                    }
                    //========================  e's for the rhs done ======================
                } // loop over all intersection quadpoints

            } // loop over intersections
//#ifndef FUNCLIST_MEM
//            rhse_.setFunc(gnComp, temp);
//#endif
        } // loop over all g_n comps
    }   // assembleRHS

    void assembleOpOnIntersections(const IntersectionType &intersection) {

        typedef typename IntersectionType::GlobalCoordinate NormalVector;

        const typename DiscreteFunctionSpaceType::MapperType &mapper = discFuncSpace_.mapper();

        EntityPointer insidePtr = intersection.inside();
        const EntityType &insideEn = *insidePtr;
        EntityPointer outsidePtr = intersection.outside();
        const EntityType &outsideEn = *outsidePtr;

        const int orderOfIntegrand = 3*polynomialOrder;
        const int quadOrder = std::ceil((orderOfIntegrand+1)/2);
        FaceQuadratureType faceQuad(gridPart_, intersection,
                                    quadOrder, FaceQuadratureType::INSIDE );
        const size_t numIntQuadPoints = faceQuad.nop();
        for (unsigned int iqP = 0; iqP!=numIntQuadPoints; ++iqP) {

            // get local coordinate of quadrature point
            const typename IntersectionType::Geometry &inGeo = intersection.geometry();
            const double intersectionVolume = inGeo.volume();
            const typename FaceQuadratureType::LocalCoordinateType &xLocal=faceQuad.localPoint(iqP);
          
// todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
            //! \todo check if it makes a difference to use faceQuad.point(iqP) here
            typedef typename FaceQuadratureType::CoordinateType CoordinateType;
            const CoordinateType xGlobal=inGeo.global(xLocal);
            const CoordinateType xInInside=intersection.geometryInInside().global(xLocal);
            const CoordinateType xInOutside=intersection.geometryInOutside().global(xLocal);
# else
//          const int faceIndexInInside = intersection.indexInInside();
//          const int insideEntityIndex = gridPart_.grid().leafIndexSet().index(insideEn);
//          int globalFaceIndex = gridPart_.grid().cellFace(insideEntityIndex, faceIndexInInside);
          
          //! \todo check if it makes a difference to use faceQuad.point(iqP) here
          typedef typename FaceQuadratureType::CoordinateType CoordinateType;
          const CoordinateType xGlobal=intersection.geometry().center();
          const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(0,0);
          const CoordinateType xInOutside = GenericReferenceElements< double, 3 >::general(outsideEn.type()).position(0,0);
#endif
            const NormalVector normal = intersection.unitOuterNormal(xLocal);
            const double factor = inGeo.integrationElement(xLocal)*faceQuad.weight(iqP);

            std::vector<RangeType> jumpPhii;
            std::vector<RangeType> jumpPhij;

            this->evaluateJumpAll(insideEn, outsideEn, xInInside, xInOutside, jumpPhii);
            this->evaluateJumpAll(outsideEn, insideEn, xInOutside, xInInside, jumpPhij);

            const int jumpPhiiSize = jumpPhii.size();
            const int jumpPhijSize = jumpPhij.size();

            // iterate over all components
            const int numOpComps = problem_.mobility().size();
            for (int opComp=0; opComp!=numOpComps; ++opComp) {

                std::vector<RangeType> meanPhii;
                std::vector<RangeType> meanPhij;
                this->evaluateMeanAll(insideEn, outsideEn, intersection,xInInside, xInOutside, opComp, normal, meanPhii);
                this->evaluateMeanAll(outsideEn, insideEn, intersection,
                                      xInOutside, xInInside, opComp, normal, meanPhij);

                // update the parameter dependent operator
                double value = 0.0;

                // first: terms in B^\nu that concern functions of both coarse elements
//                LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(insideEn, outsideEn);
//                LocalMatrixType localMatrixTransposed = opComponents_[opComp]->localMatrix(outsideEn, insideEn);
                for (int i=0; i!=jumpPhiiSize; ++i) {
                    const int row = mapper.mapToGlobal(insideEn, i);
                    for (int j=0; j!=jumpPhijSize; ++j) {
                        const int col = mapper.mapToGlobal(outsideEn, j);
                        // negative because of the jump, together with minus in operator sum we
                        // have a plus
                        value = factor*jumpPhij[j][0]*meanPhii[i];
//                        localMatrix.add(i, j, value);
//                        // the transposed part (B^\nu)^\top
//                        localMatrixTransposed.add(j, i, value);
                        
                        opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                        opComponentsEigen_[opComp].coeffRef(col,row)+=value;                        
                        
                        
                        if (dataPath_!="rbOpData") {
                            // this time, the jump brings positive sign, thus we get a minus
                            value = -1.0*factor*jumpPhii[i][0]*meanPhij[j];
//                            localMatrixTransposed.add(j, i, value);
//                            // the transposed part (B^\nu)^\top
//                            localMatrix.add(i, j, value);
                            
                            opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                            opComponentsEigen_[opComp].coeffRef(col,row)+=value;                        
                            
                        }
                    }
                }

                if (dataPath_!="rbOpData") {
                    // second: terms that concern only functions on the inside coarse element
//                    LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(insideEn, insideEn);
                    for (int i=0; i!=jumpPhiiSize; ++i) {
                        const int row = mapper.mapToGlobal(insideEn, i);
                        for (int ii=0; ii!=jumpPhiiSize; ++ii) {
                            const int col = mapper.mapToGlobal(insideEn, ii);
                            // minus because the jump gives a plus, together with the minus in the
                            // operator sum we have a minus
                            value = -1.0*factor*jumpPhii[ii][0]*meanPhii[i];
//                            localMatrix.add(i, ii, value);
//                            // the transposed part (B^\nu)^\top
//                            localMatrix.add(ii, i, value);
                            
                            
                            opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                            opComponentsEigen_[opComp].coeffRef(col,row)+=value;     
                        }
                    }
                    // third: terms that concern only functions on the outside coarse element
//                    LocalMatrixType localMatrixOutside
//                        = opComponents_[opComp]->localMatrix(outsideEn, outsideEn);
                    for (int j=0; j!=jumpPhijSize; ++j) {
                        const int row = mapper.mapToGlobal(outsideEn, j);
                        for (int jj=0; jj!=jumpPhijSize; ++jj) {
                            const int col = mapper.mapToGlobal(outsideEn, jj);
                            // plus because the jump gives a minus, together with the minus in the
                            // operator sum we have a plus
                            value = factor*jumpPhij[jj][0]*meanPhij[j];
//                            localMatrixOutside.add(j, jj, value);
//                            // the transposed part (B^\nu)^\top
//                            localMatrixOutside.add(jj, j, value);
                            
                            
                            opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                            opComponentsEigen_[opComp].coeffRef(col,row)+=value;     
                        }
                    }
                }
            } // loop over all components
            // assemble C
            double value = 0.0;
            EigenMatrixType* jumpComponent;
            if (gridPart_.onCoarseCellIntersection(intersection))
                jumpComponent = &coarseJumpTerm_;
            else
                jumpComponent = &fineJumpTerm_;
            
            for (int i=0; i!=jumpPhiiSize; ++i) {
                const int row = mapper.mapToGlobal(insideEn, i);
                // first: all terms that concern only functions with support in
                // the inside coarse element
                //                    LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(insideEn, insideEn);
                for (int ii=0; ii<=i; ++ii) {
                    const int col = mapper.mapToGlobal(insideEn, ii);
                    value = factor*sigma(xInInside, xInOutside, intersection, normal)*jumpPhii[i][0]*jumpPhii[ii][0];
                    //                        localMatrix.add(i, ii, value);
                    
                    jumpComponent->coeffRef(row,col)+=value;
                    
                    // C is symmetric
                    if (i!=ii) {
                        //                            localMatrix.add(ii, i, value);
                        
                        jumpComponent->coeffRef(col,row)+=value;     
                    }
                }
                if (dataPath_!="rbOpData") {
                    // second: mixed terms
                    //                        LocalMatrixType localMatrixMixed
                    //                            = opComponents_[opComp]->localMatrix(insideEn, outsideEn);
                    //                        LocalMatrixType localMatrixMixedTransposed
                    //                            = opComponents_[opComp]->localMatrix(outsideEn, insideEn);
                    for (int j=0; j!=jumpPhijSize; ++j) {
                        const int col = mapper.mapToGlobal(outsideEn, j);
                        value = -1.0*factor*sigma(xInInside, xInOutside, intersection, normal)*jumpPhii[i][0]*jumpPhij[j][0];
                        //                            localMatrixMixed.add(i, j, value);
                        
                        jumpComponent->coeffRef(row,col)+=value;
                        
                        // C is symmetric
                        if (row!=col) {
                            //                                localMatrixMixedTransposed.add(j, i, value);
                            
                            jumpComponent->coeffRef(col,row)+=value;     
                        }
                    }
                }
            }
            if (dataPath_!="rbOpData") {
                // third: terms that concern only functions with support in
                // the outside coarse element
                //                    LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(outsideEn, outsideEn);
                for (int j=0; j!=jumpPhijSize; ++j) {
                    const int row = mapper.mapToGlobal(outsideEn, j);
                    for (int jj=0; jj<=j; ++jj) {
                        const int col = mapper.mapToGlobal(outsideEn, jj);
                        value = factor*sigma(xInInside, xInOutside, intersection, normal)*jumpPhij[j][0]*jumpPhij[jj][0];
                        //                            localMatrix.add(j, jj, value);
                        
                        jumpComponent->coeffRef(row,col)+=value;
                        
                        // C is symmetric
                        if (row!=col) {
                            //                                localMatrix.add(jj, j, value);
                            
                            jumpComponent->coeffRef(col,row)+=value;     
                        }
                    }
                }
            }
            //= C done ====================================
            
        } // loop over all quad points
    }   // void assembleOnIntersections
    
    void assembleOpOnBoundary(const IntersectionType &intersection) {

        typedef typename IntersectionType::GlobalCoordinate NormalVector;

        const typename DiscreteFunctionSpaceType::MapperType &mapper = discFuncSpace_.mapper();

        EntityPointer insidePtr = intersection.inside();
        const EntityType &insideEn = *insidePtr;

        const int orderOfIntegrand = 3*polynomialOrder;
        const int quadOrder = std::ceil((orderOfIntegrand+1)/2);        
        FaceQuadratureType faceQuad(gridPart_, intersection,
                                    quadOrder, FaceQuadratureType::INSIDE );
        const size_t numIntQuadPoints = faceQuad.nop();
        for (unsigned int iqP = 0; iqP!=numIntQuadPoints; ++iqP) {

            // get local coordinate of quadrature point
            const typename IntersectionType::Geometry &inGeo = intersection.geometry();
            const double intersectionVolume = inGeo.volume();
            const typename FaceQuadratureType::LocalCoordinateType &xLocal=faceQuad.localPoint(iqP);
            //! \todo check if it makes a difference to use faceQuad.point(iqP) here
            typedef typename FaceQuadratureType::CoordinateType CoordinateType;

// todo: this should be something like #if GRIDTYPE!=CPGRID
#ifndef USE_CPGRID
            const CoordinateType xGlobal=inGeo.global(xLocal);
            const CoordinateType xInInside=intersection.geometryInInside().global(xLocal);
#else
//          const int faceIndexInInside = intersection.indexInInside();
//          const int insideEntityIndex = gridPart_.grid().leafIndexSet().index(insideEn);
//          int globalFaceIndex = gridPart_.grid().cellFace(insideEntityIndex, faceIndexInInside);
          
          //! \todo check if it makes a difference to use faceQuad.point(iqP) here
          typedef typename FaceQuadratureType::CoordinateType CoordinateType;
          const CoordinateType xGlobal=intersection.geometry().center();
          const CoordinateType xInInside = GenericReferenceElements< double, 3 >::general(insideEn.type()).position(0,0);
#endif
          
            const NormalVector normal = intersection.unitOuterNormal(xLocal);
            const double factor = inGeo.integrationElement(xLocal)*faceQuad.weight(iqP);

            DiffusionMatrixType K;
            // evaluate diffusion matrix
            problem_.permeability(xInInside, insideEn, K);

            std::vector<RangeType> phii;
            std::vector<JacobianRangeType> gradPhii;
            std::vector<JacobianRangeType> tempGradPhii;

            this->evaluateAll(insideEn, xInInside, phii);
            this->jacobianAll(insideEn, xInInside, tempGradPhii);

            gradPhii.resize(tempGradPhii.size());

            const int phiiSize = phii.size();

            // iterate over all components
            const int numOpComps = problem_.mobility().size();
            for (int opComp=0; opComp!=numOpComps; ++opComp) {
                // evaluate the current parameter-independent part of lambda
                RangeType lambda = problem_.mobility().component( opComp, xGlobal );
                // apply diffusion tensor to gradPhii...
                for(int i = 0; i < phiiSize; ++i) {
                    K.mv(tempGradPhii[i][0], gradPhii[i][0]);
                    gradPhii[i][0] *= lambda[0];
                }

                // update the parameter dependent operator
                double value = 0.0;
//                LocalMatrixType localMatrix = opComponents_[opComp]->localMatrix(insideEn, insideEn);
                for (int i=0; i!=phiiSize; ++i) {
                    const int row = mapper.mapToGlobal(insideEn, i);
                    for (int ii=0; ii!=phiiSize; ++ii) {
                        const int col = mapper.mapToGlobal(insideEn, ii);
                        // minus because the jump gives a plus, together with the minus in the
                        // operator sum we have a minus
                        value = -1.0*factor*phii[ii][0]*(gradPhii[i][0]*normal);

//                        localMatrix.add(i, ii, value);
//                        // the transposed part (B^\nu)^\top
//                        localMatrix.add(ii, i, value);
                        
                        opComponentsEigen_[opComp].coeffRef(row,col)+=value;
                        opComponentsEigen_[opComp].coeffRef(col,row)+=value;     
                    }
                }
            } // loop over all components
            // assemble C
            double value = 0.0;
            EigenMatrixType* jumpComponent;
            if (gridPart_.onCoarseCellIntersection(intersection))
                jumpComponent = &coarseJumpTerm_;
            else
                jumpComponent = &fineJumpTerm_;
            for (int i=0; i!=phiiSize; ++i) {
                const int row = mapper.mapToGlobal(insideEn, i);
                for (int ii=0; ii<=i; ++ii) {
                    const int col = mapper.mapToGlobal(insideEn, ii);
                    value = factor*sigma(xInInside, xInInside, intersection, normal)*phii[i][0]*phii[ii][0];
                    //                        localMatrix.add(i, ii, value);
                    jumpComponent->coeffRef(row,col)+=value;
                    
                    // C is symmetric
                    if (row!=col) {
                        //                            localMatrix.add(ii, i, value);
                        
                        jumpComponent->coeffRef(col,row)+=value;     
                    }
                }
            }
            //= C done ====================================
        } // loop over all quad points
    }   // void assembleOpOnBoundary


protected:
    const DiscreteFunctionSpaceType &discFuncSpace_;
    const GridPartType& gridPart_;
    const std::string dataPath_;
    /** the components @f$\boldsymbol{A}^\nu-\boldsymbol{B}^\nu-(\boldsymbol{B}^\nu)^\top+\boldsymbol{C}^\nu@f$
     *  /* of the DG operator */
    mutable std::vector<EigenMatrixType> opComponentsEigen_;
    EigenMatrixType fineJumpTerm_;
    EigenMatrixType coarseJumpTerm_;
    //! the sum of all operators as eigen::Matrix
    mutable EigenMatrixType operatorSumEigen_;

    mutable DiscreteFunctionListType rhsList_;
    //! the sum of all right hand side functions, scaled by parameters as eigen::vector
    mutable EigenVectorType rhsSumEigen_;
    //! the problem data
    const ProblemType& problem_;

    mutable std::vector< JacobianRangeType > gradCache_;
    mutable std::vector< JacobianRangeType > gradDiffusion_;
    mutable RangeFieldType weight_;

    double sigma_;

    const bool verbose_;
    mutable bool saveData_;

};

}   // end namespace RB
} // end namespace Dune

#endif /* __OPERATOR_BASE__ */
