#ifndef __ENERGYSCALARPRODUCT_HH_
#define __ENERGYSCALARPRODUCT_HH__

#include "operator_base.hh"

namespace Dune
{
  namespace RB
  {
    
    
    template< class DiscreteFunctionListImpl, class MatrixTraits >
    class EnergyScalarProduct : public OperatorBase<typename DiscreteFunctionListImpl::DiscreteFunctionType, MatrixTraits, EnergyScalarProduct<DiscreteFunctionListImpl, MatrixTraits> >
    {
      typedef typename DiscreteFunctionListImpl::DiscreteFunctionType DiscreteFunctionType;
      typedef EnergyScalarProduct< DiscreteFunctionListImpl, MatrixTraits > ThisType;
      typedef OperatorBase<DiscreteFunctionType, MatrixTraits, ThisType> BaseType;
      // needs to be friend for conversion check 
      friend class Conversion<ThisType,OEMSolver::PreconditionInterface>;
      
      friend class OperatorBase<DiscreteFunctionType, MatrixTraits, ThisType>;

    public:
        // typedefs from base
      typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef DiscreteFunctionListImpl DiscreteFunctionListType;
      typedef typename BaseType::LinearOperatorType LinearOperatorType;
      typedef typename BaseType::SparseMatrixSum SparseMatrixSum;
      typedef typename BaseType::ProblemType ProblemType;
      typedef typename BaseType::JacobianRangeType JacobianRangeType;
      typedef typename BaseType::RangeFieldType RangeFieldType;
      typedef typename BaseType::EntityType EntityType;
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::EntityIteratorType EntityIteratorType;
      typedef typename BaseType::GridPartType GridPartType;
      
        //! type of base function list
      typedef DiscreteFunctionListType ConstrainedFuncListType;

      
    public:
      //! constructor 
      EnergyScalarProduct(const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem,
                      const ConstrainedFuncListType& baseFunctionList,
                      const int subDomain,
                      const bool readData=false)
      : BaseType(dfSpace, problem,
                 Parameter::getValue<std::string>("energyscalarproduct.dataPath",Parameter::getValue<std::string>("reducedOp.dataPath")),
                 Parameter::getValue<double>("energyscalarproduct.sigma", Parameter::getValue<double>("reducedOp.sigma")),
                 readData),
      baseFunctionList_(baseFunctionList),
      subDomain_(subDomain)
      {
        this->rhsNuIndependent_.setGridPartition(this->gridPart_);
        this->rhsNuIndependent_.setSupport(-1);
        
        DiscreteFunctionType zero("rhsNuDependent", this->discFuncSpace_, this->gridPart_, -1);
        zero.clear();
        const int numLambdaComps = this->problem_.Nlambda();
        const int numGDComps = this->problem_.Ngd();
        const int numGNComps = this->problem_.Ngn();
        bool enlargeFuncList = (this->rhsc_.size()==0);
        if (enlargeFuncList) {        
          for (int i = 0; i<numGDComps; ++i) {
            this->rhsc_.push_back(zero);
            for (int j = 0; j<numLambdaComps; ++j) {
              this->rhsd_.push_back(zero);
            }
          }
          for (int i = 0; i<numGNComps; ++i) {
            this->rhse_.push_back(zero);
          }
        }
        for (int i = 0; i<numLambdaComps; ++i) {
          // add zero operators and rhs functions, will be filled in assemble()
          LinearOperatorType* comp = new LinearOperatorType(this->discFuncSpace_, this->discFuncSpace_);
          this->opComponents_.push_back(comp);
        }
        
        // reserve memory for matrices
        typedef typename std::vector<LinearOperatorType*>::iterator OpVecIt;
        OpVecIt opEnd = this->opComponents_.end();
        for (OpVecIt it = this->opComponents_.begin(); it!=opEnd; ++it) {
          (*it)->reserve();
        }
        this->opJumpPenaltyComp_.reserve();
        
        if (this->readData_) {
          this->readMatrices();
          this->readRightHandSide();
        }
        baseFunctionList_.stashFunctions();
        this->assemble();
        baseFunctionList_.dropStash();
      }
      
      ~EnergyScalarProduct() {}
      
    protected:
      // prohibit copying
      EnergyScalarProduct ( const ThisType & );
      
    public:
      
      /** \brief perform a grid walkthrough and assemble the global matrices 
       and the right hand side.  */
      void assemble() const 
      {
        typedef typename std::vector<LinearOperatorType*>::iterator OpVecIt;
        OpVecIt opEnd = this->opComponents_.end();
        // clear matrices and right hand side
        for (OpVecIt it = this->opComponents_.begin(); it!=opEnd; ++it) {
          (*it)->clear();
        }
        this->opJumpPenaltyComp_.clear();
        this->rhsNuIndependent_.clear(); 
        
        /* apply local matrix assembler on each element, also computes
         boundary and intersection integrals*/
        EntityIteratorType gridEnd = this->discFuncSpace_.end();
        for(EntityIteratorType it = this->discFuncSpace_.begin(); it != gridEnd; ++it) {
          assembleOp(*it);

          // assemble right hand side and intersection parts of operator
          assembleRHS(*it);
        }
      }
      
    protected:
      template<class JacobianRangeTypeVec>
      void jacobianAll(const EntityType& entity, const DomainType& quadPoint,
                       JacobianRangeTypeVec& jac) const {
        const int subDomain = this->gridPart_.getSubDomain(entity);
        const int listSize = baseFunctionList_.size();
        jac.resize(listSize);
        if (subDomain!=subDomain_) {
          for (int i=0; i!=listSize; ++i) {
            jac[i]=0.0;
          }
          return;
        }
        for (int i=0; i!=listSize; ++i) {
          const DiscreteFunctionType& func = baseFunctionList_.getFunc(i);
          const typename DiscreteFunctionType::LocalFunctionType
          localFunc = func.localFunction(entity);
          localFunc.jacobian(quadPoint, jac[i]);
        }
      }
      
      template<class RangeTypeVec>
      void evaluateAll(const EntityType& entity, const DomainType& quadPoint,
                       RangeTypeVec& evals) const {
        const int subDomain = this->gridPart_.getSubDomain(entity);
        const int listSize = baseFunctionList_.size();
        evals.resize(listSize);
        if (subDomain!=subDomain_) {
          for (int i=0; i!=listSize; ++i) {
            evals[i]=0.0;
          }
          return;
        }
        for (int i=0; i!=listSize; ++i) {
          const DiscreteFunctionType& func = baseFunctionList_.getFunc(i);
          const typename DiscreteFunctionType::LocalFunctionType
          localFunc = func.localFunction(entity);
          localFunc.evaluate(quadPoint, evals[i]);
        }
      }
      
      template<class RangeTypeVec>
      void evaluateJumpAll(const EntityType& inside, const EntityType& outside,
                           const DomainType& quadPointInInside,
                           const DomainType& quadPointInOutside,
                           RangeTypeVec& evals) const {
        this->evaluateAll(inside, quadPointInInside, evals);
          // on inside intersections we need to substract to value on the neighbor cell
        if (this->gridPart_.getSubDomain(inside)==this->gridPart_.getSubDomain(outside)) {  
          const int evalsSize = evals.size();
          RangeTypeVec outsideEvals(evalsSize);
          this->evaluateAll(outside, quadPointInOutside, outsideEvals);
          for (int i=0; i!=evalsSize; ++i) {
            evals[i]-=outsideEvals[i];
          }
          if (fabs(evals[0])<1e-5)
            int i=0;
        } 
        return;
      }
      
    protected:
      //! references to all subdomain base function lists
      const ConstrainedFuncListType& baseFunctionList_;
      const int subDomain_;
    };
    
  } // end namespace RB
} // end namespace Dune

#endif /* __ENERGYSCALARPRODUCT_HH__ */
