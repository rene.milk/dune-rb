#ifndef __RBGENERATION_HH__
#define __RBGENERATION_HH__

// stl, etc stuff
#include <sstream>

// boost
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/timer/timer.hpp>

// for plotting vecs
#include "Python.h"
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

// fem stuff
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>
#include <dune/fem/space/dgspace.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/algorithms/pca.hh>
#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/gramschmidt.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/parameterspace.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/vectorio.hh>
#include <dune/rb/misc/parameter/parameter.hh>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>
#include <dune/rb/rbasis/twophaseflow/rb/highdimoperator.hh>
#include <dune/rb/rbasis/twophaseflow/rb/errorestimator/residualestimator.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/progressbar.hh>

#include <dune/rb/rbasis/twophaseflow/helpers/customprojection.hh>

namespace Dune {
namespace RB {





/**@class RBGeneration
 * @ingroup RB
 * @brief This class generates a reduced basis by extending a given
 *        reduced basis.
 */
template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
class RBGeneration
{

    typedef ReducedSpaceImpl                                     ReducedSpaceType;
    typedef DetailedFunctionImp                                  DetailedFunctionType;
    typedef ProblemImpl                                          ProblemType;
    typedef typename ProblemType::ParameterDomainType            ParameterDomainType;
    typedef typename ReducedSpaceType::DiscreteFunctionSpaceType DiscFuncSpaceType;
    typedef typename DiscFuncSpaceType::FunctionSpaceType        FunctionSpaceType;
        // ! type of the grid partition, this shall be a multi domain gridpart
    typedef typename ReducedSpaceType::GridPartition             GridPartitionType;
    enum { polynomialOrder = ReducedSpaceType::polynomialOrder };


    typedef Localizer<DiscFuncSpaceType>                                                         LocalizerType;
    typedef ReducedOperator<ReducedSpaceType, ProblemType, LocalizerType>                        ReducedOperatorType;
    typedef HighdimOperator<DiscFuncSpaceType, ProblemType>                                      HighdimOperatorType;
    typedef Eigen::VectorXd                                                                      DofVectorType;
    typedef Algorithm<HighdimOperatorType, DofVectorType>                                        HighdimAlgorithmType;
    typedef ResidualEstimator<ReducedSpaceType, HighdimOperatorType, ProblemType, LocalizerType> ResidualEstimatorType;



    typedef L2Norm<GridPartitionType>    L2NormType;
    typedef H1Norm<GridPartitionType>    H1NormType;

    typedef Eigen::MatrixXd              EigenMatrixType;
    typedef std::vector<EigenMatrixType> EigenMatrixListType;




public:


    RBGeneration(ReducedSpaceType &rbSpace, ProblemType& problem, GridPartitionType& gridPartition)
        : rbSpace_(rbSpace),
          localizer_(rbSpace.baseFunctionSpace()),
          gridPartition_(gridPartition),
          problem_(problem),
          rbOp_(rbSpace_, problem, localizer_),
          algorithm_(rbSpace_.baseFunctionSpace(), problem_),
          verbose_(Parameter::getValue<bool>("rbgeneration.verbose")),
          residualEstimator_(rbSpace_, algorithm_.getOperator(), problem, localizer_, problem.mobility().getParameter()),
          muBar_(problem.mobility().getParameter()),
          numParams_(problem_.mobility().size()),
          numSubdomains_(gridPartition_.numSubdomains()),
          l2norm_(gridPartition_),
          h1norm_(gridPartition_),
          reducedAlgorithmVerbose_(Parameter::getValue<bool>("rbgeneration.reducedSolver.verbose", false)){
        if (Parameter::getValue<bool>("residualestimator.readData", false))
            residualEstimator_.load();

        int numDofsPerFineCell = rbSpace.baseFunctionSpace().mapper().maxNumDofs();
        for (unsigned int element=0; element!=gridPartition.numSubdomains(); ++element) {
            snapshots_.push_back(Eigen::MatrixXd(gridPartition.enclosedFineCells(element)*numDofsPerFineCell,0));
        }
    };

    ~RBGeneration () {}

    /** Initialize the RB space with a function
     *
     * @param[in] p The initial function
     */
    inline void init(DofVectorType &p);

    /** Initialize the RB space with a function
     *
     * The inital base function will be the solution to the high
     * dimensional problem to the current parameters.
     */
    inline void init();

  inline void printFlux(const DofVectorType& dofVector) const;


    /** Perfom one high dimensional solution
     *
     */
    inline void detailedSimulation(DofVectorType& p) {
        algorithm_(p);
        return;
    }


    inline void reducedSimulation(DofVectorType& p, const bool printAll=false) const;

    /** Extend the reduced basis space with a new solution to the
     *  high dimensional problem using the current parameters.
     *
     *  @param[in] percentage The desired accuracy for the pca
     */
    bool extend();

    /** Extend the reduced basis space with the function p using the
     *  algorithm discribed in my diploma thesis
     *
     * @todo Discribe the algorithm here
     *
     * @param[in] p The function to be added
     * @param[in] percentage The desired accuracy for the pca
     */
    bool extend(DofVectorType& p);


    /** Print information regarding the compressibility on each subdomain.
     *
     * This prints the Gram matrix @f$ \phi^\tau\cdot A\cdot\phi@f$ for each
     * subdomain basis @f$\phi@f$ and its eigenvalues. Here, @f$A@f$ denotes the
     * high-dimensional system matrix.
     */
    void printCompressibility();


    /** Call a python script using two containers.
     *
     *  This calls the python script located in "mymodule.py" and hands over the two containers.
     *  @note I guess the container should be of an stl container, otherwise boost will not provide converters for it.
     *
     *  @param[in] xData A container containing the x data.
     *  @param[in] xData A container containing the y data.
     *  @param[in] desiredFunction A string containing the name of the function that is to be called in the python
     *                             script.
     */
    template<class ContainerType1, class ContainerType2>
    void callPython(const ContainerType1& xData, const ContainerType2& yData, const std::string& desiredFunction);


    /** Save the computed snapshots to disk.
     *
     *  This uses the Eigen::MatrixBase::saveToBinaryFile memberfunction to save the snapshots in matrix-binary format.
     */
    void saveSnapshots() const;

    /** Read the computed snapshots from disk
     *
     *  This assumes the snapshots to be written in binary files in the directory "snapshots".
     *
     */
    void readSnapshots();


// void generateBasisFromSnapshots();

    /** Compress the information
     *
     */
    void compressInformation();


    /** Extend the reduced basis to a desired size using uniformly
     *  distributed parameters.
     *
     *  @note This uses the parameters "rbgeneration.uniform.begin"
     *  and "rbgeneration.uniform.end" defining the corners of the
     *  parameter space.
     *
     *  @param[in] size The number of parameters in each parameter
     *                  dimension.
     */
    void uniformExtension();

    /**@brief The greedy algorithm.
     *
     * @param[in] size The desired size of the space
     */
    void greedyExtension(const boost::uuids::uuid& uid = boost::uuids::random_generator() ());

public:
    /** perform reduced simulation and compute error using current
     *   reduced operator and error estimator*/
    double getError(const ParameterDomainType& mu) const;


    /** perform reduced simulation and compute error using current
     *   reduced operator and error estimator*/
    void getErrorHighdim(const ParameterDomainType& mu, double& highdimRuntime,
                         double& lowdimRuntime, double& reconsTime, double& trueError,
                         double& upperBound, double& lowerBound);
    
    /** perform reduced simulation and compute error using current
     *   reduced operator and error estimator*/
    void getErrorHighdim(const ParameterDomainType& mu, double& highdimRuntime,
                         double& lowdimRuntime, double& reconsTime, double& trueError,
                         double& upperBound, double& lowerBound, const DofVectorType& p);

        /** Update operators */
    void updateOperators() {
        updateReducedOperator();
    }

    double updateErrorEstimator();

    double updateErrorEstimatorFull();

        /** Update reduced operators */
    double updateReducedOperator();

    /** print the reduced solution, the high dim solution and the difference of the
     *   two for the current parameter using grape*/
    void plotError(const bool printError=false);

private:
        /** print information about the extension just done*/
    void printExtensionInfo(const ParameterDomainType& mu) const;


private:
    ReducedSpaceType& rbSpace_;
    LocalizerType localizer_;
    GridPartitionType&      gridPartition_;
    ProblemType&        problem_;
    ReducedOperatorType rbOp_;
    HighdimAlgorithmType algorithm_;
    const bool verbose_;
    ResidualEstimatorType residualEstimator_;
    const ParameterDomainType muBar_;
    const unsigned int numParams_;
    std::vector<double> squareBaseFuncNorms_;
    const unsigned int numSubdomains_;
    const L2NormType l2norm_;
    const H1NormType h1norm_;
    std::vector<ParameterDomainType> usedMus_;
    EigenMatrixListType snapshots_;
    const bool reducedAlgorithmVerbose_;
    double gramSchmidtTime_;
};

} // namespace Dune
} // namespace RB

#include "rbgeneration.cc"

#endif /* __RBGENERATION_HH__ */
