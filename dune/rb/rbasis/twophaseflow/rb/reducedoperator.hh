#ifndef __REDUCEDOPERATOR_HH__
#define __REDUCEDOPERATOR_HH__

#include "operator_base.hh"
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>

namespace Dune
{
namespace RB
{

#if 0
template< class DiscreteFunctionImpl, class MatrixTraits, class ProblemImpl >
class ReducedOperator : public OperatorBase<DiscreteFunctionImpl, typename MatrixTraits::template MatrixObject<LagrangeMatrixTraits<MatrixTraits> >
                                            :: MatrixObjectType, ProblemImpl, ReducedOperator<DiscreteFunctionImpl, MatrixTraits, ProblemImpl> >
{
    typedef typename MatrixTraits::template MatrixObject<LagrangeMatrixTraits<MatrixTraits> >
    :: MatrixObjectType MatrixObjectType;
    typedef ReducedOperator< DiscreteFunctionImpl, MatrixTraits, ProblemImpl >          ThisType;
    typedef OperatorBase<DiscreteFunctionImpl, MatrixObjectType, ProblemImpl, ThisType> BaseType;
        // needs to be friend for conversion check
    friend class Conversion<ThisType,OEMSolver::PreconditionInterface>;

    friend class OperatorBase<DiscreteFunctionImpl, MatrixObjectType, ProblemImpl, ThisType>;

public:
        // typedefs from base
    typedef typename BaseType::DiscreteFunctionSpaceType        DiscreteFunctionSpaceType;
    typedef typename BaseType::DiscreteFunctionType             DiscreteFunctionType;
    typedef typename BaseType::DiscreteFunctionListType         DiscreteFunctionListType;
    typedef typename BaseType::LinearOperatorType               LinearOperatorType;
    typedef typename BaseType::MatrixType                       MatrixType;
    typedef typename BaseType::SparseMatrixSum                  SparseMatrixSum;
    typedef typename BaseType::ProblemType                      ProblemType;
    typedef typename BaseType::JacobianRangeType                JacobianRangeType;
    typedef typename BaseType::RangeFieldType                   RangeFieldType;
    typedef typename BaseType::EntityType                       EntityType;
    typedef typename BaseType::DomainType                       DomainType;
    typedef typename BaseType::RangeType                        RangeType;
    typedef typename BaseType::EntityIteratorType               EntityIteratorType;
    typedef typename BaseType::GridPartType                     GridPartType;
    typedef typename BaseType::IntersectionType                 IntersectionType;

        // ! type of base function list
    typedef typename DiscreteFunctionSpaceType::ConstrainedList ConstrainedFuncListType;


public:
        // ! constructor
    ReducedOperator(const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem,
                    const bool readData=false)
        : BaseType(dfSpace, problem,
                   Parameter::getValue<std::string>("reducedOp.dataPath"),
                   Parameter::getValue<double>("reducedOp.sigma"),
                   readData)
    {}

    ~ReducedOperator () {}

protected:
        // prohibit copying
    ReducedOperator ( const ThisType & );

public:

    /** \brief perform a grid walkthrough and assemble the global matrices
     *  and the right hand side.  */
    void assemble() const
    {
        typedef typename std::vector<LinearOperatorType*>::iterator OpVecIt;
        OpVecIt opEnd = this->opComponents_.end();
            // clear matrices and right hand side
        for (OpVecIt it = this->opComponents_.begin(); it!=opEnd; ++it) {
            (*it)->clear();
        }
        this->rhsNuIndependent_.clear();

            // get all current base function lists and stash them
        this->baseFunctionLists_.clear();
        for (unsigned int i=0; i!=this->gridPart_.numSubDomains(); ++i) {
            const ConstrainedFuncListType& list = this->discFuncSpace_.getFuncList(i);
            const int listSize = list.size();
            list.stashFunctions(0, listSize);
            this->baseFunctionLists_.push_back(this->discFuncSpace_.getFuncListPtr(i));
        }

        /* apply local matrix assembler on each element, also computes
         *  boundary and intersection integrals*/
        EntityIteratorType gridEnd = this->discFuncSpace_.end();
        for(EntityIteratorType it = this->discFuncSpace_.begin(); it != gridEnd; ++it) {
            assembleOp(*it);

                // assemble right hand side and intersection parts of operator
            assembleRHS(*it);
        }

            // clear the space needed by the stash
        for (unsigned int i=0; i!=this->gridPart_.numSubDomains(); ++i) {
            this->discFuncSpace_.getFuncList(i).dropStash();
        }
    } /* assemble */

protected:
    template<class JacobianRangeTypeVec>
    void jacobianAll(const EntityType& entity, const DomainType& quadPoint,
                     JacobianRangeTypeVec& jac) const {
        const int subDomain = this->gridPart_.getSubDomain(entity);
        assert(this->baseFunctionLists_[subDomain]!=0);
        const ConstrainedFuncListType& list = *(this->baseFunctionLists_[subDomain]);
        const int listSize = list.size();
        jac.resize(listSize);
        for (int i=0; i!=listSize; ++i) {
            const typename DiscreteFunctionSpaceType::BaseFunctionType& func = list.getFunc(i);
            const typename DiscreteFunctionSpaceType::BaseFunctionType::LocalFunctionType
                localFunc = func.localFunction(entity);
            localFunc.jacobian(quadPoint, jac[i]);
        }
    } /* jacobianAll */

    template<class RangeTypeVec>
    void evaluateAll(const EntityType& entity, const DomainType& quadPoint,
                     RangeTypeVec& evals) const {
        const int subDomain = this->gridPart_.getSubDomain(entity);
        assert(this->baseFunctionLists_[subDomain]!=0);
        const ConstrainedFuncListType& list = *(this->baseFunctionLists_[subDomain]);
        const int listSize = list.size();
        evals.resize(listSize);
        for (int i=0; i!=listSize; ++i) {
            const typename DiscreteFunctionSpaceType::BaseFunctionType& func = list.getFunc(i);
            const typename DiscreteFunctionSpaceType::BaseFunctionType::LocalFunctionType
                localFunc = func.localFunction(entity);
            localFunc.evaluate(quadPoint, evals[i]);
        }
    } /* evaluateAll */

    template<class RangeTypeVec>
    void evaluateJumpAll(const EntityType& inside, const EntityType& outside,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         RangeTypeVec& evals) const {
        this->evaluateAll(inside, quadPointInInside, evals);
            // on inside intersections we need to substract to value on the neighbor cell
        if (this->gridPart_.getSubDomain(inside)==this->gridPart_.getSubDomain(outside)) {
            const int evalsSize = evals.size();
            RangeTypeVec outsideEvals(evalsSize);
            this->evaluateAll(outside, quadPointInOutside, outsideEvals);
            for (int i=0; i!=evalsSize; ++i) {
                evals[i]-=outsideEvals[i];
            }
        }
        return;
    } /* evaluateJumpAll */


    template<class RangeTypeVec>
    void evaluateMeanAll(const EntityType& inside, const EntityType& outside,
                         const IntersectionType& intersection,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         const int opComp,
                         const DomainType& normal,
                         RangeTypeVec& evals) const {
        DomainType xGlobal = inside.geometry().global(quadPointInInside);
        typedef typename ProblemType::DiffusionMatrixType DiffusionMatrixType;
        DiffusionMatrixType Kinside;
            // evaluate diffusion matrix
        this->problem_.permeability(quadPointInInside, inside, Kinside);
        RangeType lambdaInside = this->problem_.mobility().component(opComp, xGlobal);
        DynamicArray<JacobianRangeType> insideGradientsTemp;
        this->jacobianAll(inside, quadPointInInside, insideGradientsTemp);
        DynamicArray<JacobianRangeType> insideGradients(insideGradientsTemp.size());
        evals.resize(insideGradientsTemp.size());
        for (int i=0; i!=insideGradientsTemp.size(); ++i) {
            Kinside.mv(insideGradientsTemp[i][0], insideGradients[i][0]);
            evals[i][0] = insideGradients[i][0]*normal;
            evals[i][0] *= 0.5*lambdaInside[0];
        }

        if (this->gridPart_.getSubDomain(inside)==this->gridPart_.getSubDomain(outside)) {
            DiffusionMatrixType Koutside;
                // evaluate diffusion matrix
            this->problem_.permeability(quadPointInOutside, outside, Koutside);
            RangeType lambdaOutside = this->problem_.mobility().component(opComp, xGlobal);
            DynamicArray<JacobianRangeType> outsideGradientsTemp;
            this->jacobianAll(outside, quadPointInOutside, outsideGradientsTemp);
            DynamicArray<JacobianRangeType> outsideGradients(outsideGradientsTemp.size());
            for (int i=0; i!=outsideGradientsTemp.size(); ++i) {
                Koutside.mv(outsideGradientsTemp[i][0], outsideGradients[i][0]);
                outsideGradients[i][0] *= 0.5*lambdaOutside[0];
                evals[i][0] += outsideGradients[i][0]*normal;
            }
        }

        return;
    } /* evaluateMeanAll */

    template<class OutStreamType>
    void printMatrix(const MatrixType& matrix, const std::string& name,
                     OutStreamType& out) const {
        Stuff::printSparseRowMatrixMatlabStyle(matrix, name, out, 1e-14);
        return;
    }

    template<class OutStreamType>
    void printDiscreteFunction(const DiscreteFunctionType& func, const std::string& name,
                               OutStreamType& out) const {
        Stuff::printDiscreteFunctionMatlabStyle(func, name, out);
    }

protected:
        // ! references to all subdomain base function lists
    mutable std::vector<const ConstrainedFuncListType*> baseFunctionLists_;
};
#endif

template <class ReducedSpaceImpl, class ProblemImpl, class LocalizerImp>
class ReducedOperator {

public:
    typedef ReducedSpaceImpl ReducedSpaceType;
    typedef ProblemImpl      ProblemType;
    typedef Eigen::MatrixXd  MatrixType;
    typedef Eigen::SparseMatrix<double> SparseMatrixType;
    typedef Eigen::VectorXd  VectorType;
    typedef LocalizerImp LocalizerType;
    typedef Eigen::Matrix<SparseMatrixType, Eigen::Dynamic, 1> ComponentsVectorType;
    typedef Eigen::Matrix<VectorType, Eigen::Dynamic, 1> RightHandSideVectorType;
    
    
    ReducedOperator(const ReducedSpaceType& rbSpace, 
                    const ProblemType& problem,
                    const LocalizerType& localizer) 
        : rbSpace_(rbSpace),
          problem_(problem),
          localizer_(localizer)
    {}
    
    
    /** Update this operator
     *
     *  This projects a given high dimensional operator and the right hand side therein to the reduced space. 
     *  This is needed every time that the reduced space has changed (when base functions where added, for example). 
     *  
     *  @note This assumes that the whole reduced basis has changed. A more efficient version that only updates
     *        some columns and rows of the reduced matrices is still to be implemented.
     *
     *  @tparam HighDimensionalOperatorType The type of the high dimensional operator that is to be projected.
     *  @param[in] highDimOp The high dimensional operator that is to be projected.
     */
    template<class HighDimensionalOperatorType>
    void update(const HighDimensionalOperatorType& highDimOp) {
        
        const int numHighDimDofs = highDimOp.discreteFunctionSpace().size(); 
        const int rbSpaceSize = rbSpace_.size();
        
//        // put all subdomain bases into one big matrix
//        Eigen::MatrixXd fullBasis(numHighDimDofs, rbSpace_.size());
//        int start=0;        
//        for (int subdomain=0; subdomain!=rbSpace_.gridPart().numSubDomains(); ++subdomain) {
//            const int subdomainBasisSize = rbSpace_.getSubdomainBasis(subdomain).cols();
//            fullBasis.block(0, start, numHighDimDofs, subdomainBasisSize) 
//            =rbSpace_.getSubdomainBasis(subdomain);
//            start += subdomainBasisSize;
//        }
        
        // Write all subdomain bases to one big sparse matrix
        Eigen::SparseMatrix<double> fullBasis(numHighDimDofs, rbSpaceSize);
        
        typedef Eigen::Triplet<double> TripletType;
        std::vector<TripletType> triplets;
        triplets.reserve(rbSpace_.getSubdomainBasis(0).cols()*numHighDimDofs);
        int start = 0;
        for (int subdomain=0; subdomain!=rbSpace_.gridPart().numSubdomains(); ++subdomain) {
            const MatrixType& subdomainBasis = rbSpace_.getSubdomainBasis(subdomain);
            localizer_.addTriplets(subdomainBasis, subdomain, start, triplets);
            start += subdomainBasis.cols();
        }
        fullBasis.setFromTriplets(triplets.begin(), triplets.end());
                
        // project all components of the high dimensional operator and right hand side to the new reduced basis      
        const int numOpComps = highDimOp.numOperatorComponents();
        opComponents_.resize(numOpComps);
                                                      
        const int numRHSComps = highDimOp.numRightHandSideComponents();
        rhsComponents_.resize(numRHSComps);
              
        // operator components
        for (int opComp=0; opComp!=numOpComps; ++opComp) {
            opComponents_(opComp)=fullBasis.transpose()
            *highDimOp.getBhComponentEigen(opComp)*fullBasis;
        }
        // right hand side components
        for (int rhsComp=0; rhsComp!=numRHSComps; ++rhsComp) {
            rhsComponents_(rhsComp) = fullBasis.transpose()*highDimOp.getLhComponentEigen(rhsComp);
        }
        
        // jump terms
        double fineSigma = Parameter::getValue<double>("reducedOp.fineSigma");
        double coarseSigma = Parameter::getValue<double>("reducedOp.coarseSigma");
        
        // fine
        jumpComponent_ = fineSigma*(fullBasis.transpose()*highDimOp.getFineJumpComponent()*fullBasis);
        
        // coarse
        jumpComponent_ 
        += coarseSigma*(fullBasis.transpose()*highDimOp.getCoarseJumpComponent()*fullBasis);        

        return;
    }
    
    /** Return the reduced system matrix.
     *
     * @return Returns the reduced system matrix for the parameters currently set in the problem class.
     */
    const SparseMatrixType& systemMatrix() const {
        assert(opComponents_.rows()!=0 && "You called systemMatrix() on an empty operator!");
        
        const int rows = opComponents_(0).rows();
        const int cols = opComponents_(0).cols();
        
        // reset matrix to all zero with the new size
        operatorSum_.resize(rows, cols);
        
        double sum = 0.0;
        const int numOpComps = problem_.mobility().size();
        for (int opComp=0; opComp!=numOpComps; ++opComp) {
            // add the component to the sum, scaled with the proper parameter
            const double parameter = problem_.mobility().coefficient(opComp);
            sum += parameter;
            operatorSum_ += parameter*opComponents_(opComp);
        }
        
        operatorSum_ += sum*jumpComponent_;      
//        operatorSumDense_ = operatorSum_.toDense();
        
        return operatorSum_;
    }
    
    /** Return the reduced right hand side vector.
     *
     * @return Returns the reduced right hand side vector for the parameters currently set in the problem class.
     */
    const VectorType& rightHandSide() const {
        assert(rhsComponents_.rows()>0);
        
        rightHandSideSum_=rhsComponents_(0);
        const int numOpComps = opComponents_.rows();
        
        for (int i=0; i!=problem_.dirichletValues().size(); ++i) {
            const double dirichletValueCoefficient = problem_.dirichletValues().coefficient(i);
            for (int opComp=0; opComp!=numOpComps; ++opComp) {
                const int index = 1+i*numOpComps+opComp;
                assert(index<rhsComponents_.rows());
                
                const double mobilityCoefficient = problem_.mobility().coefficient(opComp);
                rightHandSideSum_ += dirichletValueCoefficient*mobilityCoefficient*rhsComponents_(index);
            }
        }
        for (int i=0; i!=problem_.neumannValues().size(); ++i) {
            int index = 1+problem_.dirichletValues().size()*numOpComps+i;
            const double neumannValueCoefficient = problem_.neumannValues().coefficient(i);
            rightHandSideSum_ += neumannValueCoefficient*rhsComponents_(index);
        }
        return rightHandSideSum_;
    }
    
private:
    const ReducedSpaceType& rbSpace_;
    const ProblemType&      problem_;
    ComponentsVectorType opComponents_;
    RightHandSideVectorType rhsComponents_;
    mutable SparseMatrixType      operatorSum_;
    mutable MatrixType      operatorSumDense_;
    mutable SparseMatrixType jumpComponent_;
    mutable VectorType      rightHandSideSum_;
    const LocalizerType     localizer_;
};
    
}   // end namespace RB
} // end namespace Dune


#endif /* __REDUCEDOPERATOR_HH__ */
