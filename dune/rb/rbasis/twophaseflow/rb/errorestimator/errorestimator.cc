namespace Dune {
  namespace RB {
    
    template<class ReducedFunctionImpl, class ProblemImpl>
    void ErrorEstimator<ReducedFunctionImpl, ProblemImpl>::init() {
        // for the time being, copy the base function instead of reconstructing them
        for (unsigned int i=0; i!=gridPart_.numSubDomains(); ++i) {
          const RBBaseFunctionListType& baseFuncList = rbSpace_.getFuncList(i);
          baseFuncList.stashFunctions(0, baseFuncList.size());
          for (unsigned int j=0; j!=baseFuncList.size(); ++j) {
            reconstructedBaseFunctions_[i].push_back(baseFuncList.getFunc(j));
          }
          baseFuncList.dropStash();
         
        }
        int numCoarseCellIntersections=0;
        const EntityIteratorType end = gridPart_.template end<0>();
        for (EntityIteratorType it = gridPart_.template begin<0>(); it!=end; ++it) {
          IntersectionIteratorType intEnd = gridPart_.iend(*it);
          for (IntersectionIteratorType intIt = gridPart_.ibegin(*it); intIt!=intEnd; 
               ++intIt) {
            const IntersectionType& intersection = *intIt;
            if (gridPart_.onCoarseCellIntersection(intersection)) {
              EntityPointerType insidePtr = intersection.inside();
              EntityPointerType outsidePtr = intersection.outside();
              const EntityType& insideEn = *insidePtr;
              const EntityType& outsideEn = *outsidePtr;
              unsigned int coarseCellInside = gridPart_.getSubDomain(insideEn);
              unsigned int coarseCellOutside = gridPart_.getSubDomain(outsideEn);
              assert(coarseCellInside<reconstructedBaseFunctions_.size());
              const unsigned int fineCellInside = gridPart_.indexSet().index(insideEn);
              const unsigned int fineCellOutside = gridPart_.indexSet().index(outsideEn);
              const int intersectionIndex = intersectionIndexMapper_.map(intersection);
               if (coarseCellInside<coarseCellOutside) {
                 equationStorage_.addInner(intersectionIndex,
                                           intersection, coarseCellInside,
                                           reconstructedBaseFunctions_[coarseCellInside], true);
                 equationStorage_.addInner(intersectionIndex,
                                           intersection, coarseCellOutside,
                                           reconstructedBaseFunctions_[coarseCellOutside], false);
                ++numCoarseCellIntersections;
                const unsigned int insideNumBaseFuncs = rbSpace_.getFuncList(coarseCellInside).size();
                const unsigned int outsideNumBaseFuncs = rbSpace_.getFuncList(coarseCellOutside).size();
                // create the online matrices @f$\boldsymbol{M}^{E_1,E_2}@f$,
                // @f$\boldsymbol{M}^{E_1,E_1}@f$ and @f$\boldsymbol{M}^{E_2,E_2}@f$
                // and matching @f$N@f$ matrices. 
                onlineMatrixStorage_.add(intersectionIndex,
                                         fineCellInside, insideNumBaseFuncs, coarseCellInside,
                                         fineCellOutside, outsideNumBaseFuncs, coarseCellOutside);

                // if not done yet, save this combination of fine cells, needed for evaluate method
                std::vector<int> thisComb(5);
                thisComb[0]=fineCellInside;
                thisComb[1]=fineCellOutside;
                thisComb[2]=coarseCellInside;
                thisComb[3]=coarseCellOutside;
                thisComb[4]=intersectionIndex;
                
                MyPredicate pred(thisComb);
                if (std::find_if(fineCellCombs_.begin(), fineCellCombs_.end(), pred)==fineCellCombs_.end())
                  fineCellCombs_.push_back(thisComb);
              }
            }
            else if (intersection.boundary()) {
              EntityPointerType insidePtr = intersection.inside();
              const EntityType& insideEn = *insidePtr;
              unsigned int coarseCellInside = gridPart_.getSubDomain(insideEn);
              unsigned int intersectionIndex = intersectionIndexMapper_.map(intersection);
              equationStorage_.addBoundary(intersectionIndex,
                                           intersection,
                                           reconstructedBaseFunctions_[coarseCellInside]);
              // save this boundary intersection together with enclosing coarse cell
              boundaryCells_.push_back(std::pair<int, int>(intersectionIndex, coarseCellInside));
            }
          }
        }
        if (verbose_)
          std::cout << "The grid has " << numCoarseCellIntersections
                    << " intersections of fine cells on the coarse cell intersections!\n";
    }

    template<class ReducedFunctionImpl, class ProblemImpl>
    class ErrorEstimator<ReducedFunctionImpl, ProblemImpl>::MyPredicate {
    public:
      MyPredicate(const std::vector<int>& myData):myData_(myData)
      {}

      const bool operator()(const std::vector<int>& data) const {
        bool equal;
        equal = (((data[0]==myData_[0]) && (data[1]==myData_[1]) && (data[4]==myData_[4]))
                 || ((data[0]==myData_[1]) && (data[1]==myData_[0]) && (data[4]==myData_[4]))) ;
        return equal;
      }

    private:
      std::vector<int> myData_;
    };

    template<class ReducedFunctionImpl, class ProblemImpl>
    void ErrorEstimator<ReducedFunctionImpl, ProblemImpl>::assembleOnlineMatrices() {
      std::cout << "Assemble online matrices:  ";
      onlineMatrixStorage_.assemble(reconstructedBaseFunctions_);
      // print the matrices to std::cout
      if (verbose_) {
        onlineMatrixStorage_.print();
        // print some information about the quantities assembled
        std::cout << "The grid has " << gridPart_.grid().size(0) << " entities, subdivided into "
                  << gridPart_.numSubDomains() << " subdomains.\n"
                  << "We have " << problem_.Nlambda() << " parameters.\n";
        onlineMatrixStorage_.printNumMatrices();
      }
      return;        
    }

    template<class ReducedFunctionImpl, class ProblemImpl>
    double ErrorEstimator<ReducedFunctionImpl, ProblemImpl>::evaluate(const ReducedFunctionType& func,
                                                                      const bool printAll) {
      const int numSubDomains = gridPart_.numSubDomains();
      double totalError=0.0;
      bool printAllErrors = printAll;//Parameter::getValue<bool>("errorestimator.printAllErrors", printAll);
        
      //===================== residual terms ===============================
      int dofsBegin = 0;
      int dofsEnd = 0;
      for (int coarseCell=0; coarseCell!=numSubDomains; ++coarseCell) {
        double residualErrorOnCoarseCell = onlineMatrixStorage_.getfNorm(coarseCell);

        // get the dof vector for this coarse cell
        dofsBegin = dofsEnd;
        dofsEnd += rbSpace_.getFuncList(coarseCell).size();
        int size = dofsEnd - dofsBegin;
        VectorType dofVec(size);
        int j=0;
        for (int i=dofsBegin; i!=dofsEnd; ++i, ++j) {
          dofVec[j] = func.leakPointer()[i];
        }

        for (int sigma=0; sigma!=Nlambda_; ++sigma) {
          double lSigma = problem_.l(sigma);
          const VectorType& k = onlineMatrixStorage_.getk(sigma, coarseCell);
          const int kSize = k.size();
          assert(kSize==size);
          residualErrorOnCoarseCell += lSigma*(k*dofVec);
          for (int nu=0; nu!=Nlambda_; ++nu) {
            double lNu = problem_.l(nu);
            VectorType temp(kSize);
            temp=0.0;
            const MatrixType& K = onlineMatrixStorage_.getK(sigma, nu, coarseCell);
            K.mv(dofVec, temp);
            double Kval = lSigma*lNu*(temp*dofVec);
            residualErrorOnCoarseCell += Kval;
          }
        }
        checkForNegativeError(residualErrorOnCoarseCell, "residual", dofVec, dofVec);

        totalError += sqrt(residualErrorOnCoarseCell);
      }
      if (printAllErrors)
        std::cout << "residual error: " << totalError << std::endl;

      //===================== jump terms ===============================
      double totalMError=0.0;
      double totalNError=0.0;

      typedef std::vector<std::vector<int> >::const_iterator MyIteratorType;
      MyIteratorType vecEnd = fineCellCombs_.end();
      for (MyIteratorType it = fineCellCombs_.begin(); it!=vecEnd; ++it) {

        double Merror = 0.0;
        double Nerror = 0.0;
          
        const int fineCellOne = (*it)[0];
        const int fineCellTwo = (*it)[1];
        const int coarseCellOne = (*it)[2];
        const int coarseCellTwo = (*it)[3];
        const int intersectionIndex = (*it)[4];
        // copy dofs
        int oneBegin = 0;
        for (int i=0; i!=coarseCellOne; ++i) {
          oneBegin+=rbSpace_.getFuncList(i).size();
        }
        const int oneEnd = oneBegin + rbSpace_.getFuncList(coarseCellOne).size();
        const int sizeOne = oneEnd - oneBegin;
        VectorType dofsOne(sizeOne);
        int j=0;
        for (int i=oneBegin; i!=oneEnd; ++i, ++j) {
          dofsOne[j] = func.dof(i);
          //                dofsOne[j] = func.leakPointer()[i];
        }
        // copy dofs              
        int twoBegin = 0;
        for (int i=0; i!=coarseCellTwo; ++i) {
          twoBegin+=rbSpace_.getFuncList(i).size();
        }
        const int twoEnd = twoBegin + rbSpace_.getFuncList(coarseCellTwo).size();
        const int sizeTwo = twoEnd - twoBegin;
        VectorType dofsTwo(sizeTwo);
        j=0;
        for (int i=twoBegin; i!=twoEnd; ++i, ++j) {
          dofsTwo[j] = func.dof(i);
          //                dofsTwo[j] = func.leakPointer()[i];
        }

        VectorType fooOne(rbSpace_.getFuncList(coarseCellOne).size());
        fooOne=0.0;
        VectorType fooTwo(rbSpace_.getFuncList(coarseCellTwo).size());
        fooTwo=0.0;
        VectorType fooThree(rbSpace_.getFuncList(coarseCellOne).size());
        fooThree=0.0;

        int rows = onlineMatrixStorage_.getM(intersectionIndex, fineCellOne, fineCellTwo).N();
        int cols = onlineMatrixStorage_.getM(intersectionIndex, fineCellOne, fineCellTwo).M();
        assert(cols==sizeTwo);
        assert(int(fooThree.N())==rows);
        assert(rows==sizeOne);

        onlineMatrixStorage_.getM(intersectionIndex, fineCellOne, fineCellOne).mv(dofsOne,fooOne);
        Merror += (fooOne*dofsOne);
        onlineMatrixStorage_.getM(intersectionIndex, fineCellTwo, fineCellTwo).mv(dofsTwo, fooTwo);
        Merror += (fooTwo*dofsTwo);
        onlineMatrixStorage_.getM(intersectionIndex, fineCellOne, fineCellTwo).mv(dofsTwo, fooThree);
        Merror -= 2*(fooThree*dofsOne); 

        checkForNegativeError(Merror, "M", dofsOne, dofsTwo);
        totalMError += sqrt(Merror);

        for (int sigma=0; sigma!=Nlambda_; ++sigma) {
          double lSigma = problem_.l(sigma);
          for (int nu=0; nu!=Nlambda_; ++nu) {
            double lNu = problem_.l(nu);
            fooOne=0.0;
            fooTwo=0.0;
            fooThree=0.0;
            onlineMatrixStorage_.getN(intersectionIndex, fineCellOne, sigma, fineCellOne, nu).mv(dofsOne, fooOne);
            Nerror += lSigma*lNu*(fooOne*dofsOne);
            onlineMatrixStorage_.getN(intersectionIndex, fineCellTwo, sigma, fineCellTwo, nu).mv(dofsTwo, fooTwo);
            Nerror += lSigma*lNu*(fooTwo*dofsTwo);
            onlineMatrixStorage_.getN(intersectionIndex, fineCellOne, sigma, fineCellTwo, nu).mv(dofsTwo, fooThree);
            Nerror -= 2.0*lSigma*lNu*(fooThree*dofsOne);
          } // loop over nu
        } // loop over sigma

        checkForNegativeError(Nerror, "N", dofsOne, dofsTwo);
        totalNError += sqrt(Nerror);
      } // loop over coarse cell combinations

      totalError+=totalMError;
      totalError+=totalNError;
      if (printAllErrors) {
        std::cout << "M error: " << totalMError << std::endl;
        std::cout << "N error: " << totalNError << std::endl;
      }

      //===================== Boundary terms ===============================
      double boundaryError=0.0;
      typedef std::vector<std::pair<int,int> >::const_iterator MyIterator;
      MyIterator setEnd = boundaryCells_.end();
      for (MyIterator it = boundaryCells_.begin(); it!=setEnd; ++it) {
        const int intersectionIndex = it->first;
        const int coarseCell = it->second;

        // get matching coarse cell dof vector
        int dofsBegin = 0;
        for (int i=0; i!=coarseCell; ++i) {
          dofsBegin+=rbSpace_.getFuncList(i).size();
        }
        const int dofsEnd = dofsBegin + rbSpace_.getFuncList(coarseCell).size();
        const int size = dofsEnd - dofsBegin;
        VectorType dofs(size);
        int j=0;
        for (int i=dofsBegin; i!=dofsEnd; ++i, ++j) {
          dofs[j] = func.dof(i);
        }

        double boundaryErrorElement = 0.0;
          
        for (int gdComp=0; gdComp!=Ngd_; ++gdComp) {
          double mugd1 = problem_.mugd(gdComp);
          boundaryErrorElement -= 2*mugd1*(onlineMatrixStorage_.getm(gdComp, intersectionIndex)*dofs);
          for (int gdComp2=0; gdComp2!=Ngd_; ++gdComp2) {
            double mugd2 = problem_.mugd(gdComp2);
            boundaryErrorElement += mugd1*mugd2*onlineMatrixStorage_.getGDNorm(gdComp, gdComp2, intersectionIndex);
          }
        }
        VectorType foo(size);
        foo=0.0;
        onlineMatrixStorage_.getQ(intersectionIndex).mv(dofs, foo);
        boundaryErrorElement += foo*dofs;

        checkForNegativeError(boundaryErrorElement, "boundary", dofs, dofs);
        boundaryError += sqrt(boundaryErrorElement);
      }
      if (printAllErrors)
        std::cout << "Boundary error: " << boundaryError << std::endl;
      totalError += boundaryError;

      assert(!std::isnan(totalError));
      assert(totalError>0.0);

      return totalError;
    }

    template<class ReducedFunctionImpl, class ProblemImpl>
    void ErrorEstimator<ReducedFunctionImpl, ProblemImpl>::checkForNegativeError(double& givenError, 
                                                                                 const std::string& partName,
                                                                                 const  VectorType& dofsOne, 
                                                                                 const VectorType& dofsTwo) {
      if (givenError<0.0) {
        if (fabs(givenError)<zeroTolerance_) {
          givenError=0.0;
        } else {
          char a;
          do {
            std::cout << "Error contribution for part " << partName
                      << " is negative: " << givenError << std::endl
                      << "Dofs one are: " << dofsOne
                      << "\nand Dofs two: " << dofsTwo << std::endl
                      << "\nSet to zero, abort or reset zero tolerance? (z/a/r) ";
            std::cin >> a;
          } while (a!='z' && a!='a' && a!='r');
          if (a=='z')
            givenError=0.0;
          else if (a=='a')
            DUNE_THROW(Dune::MathError, "Got negative value for square norm!");
          else if (a=='r') {
            std::cout << "\nPlease give new zero tolerance... ";
            std::cin >> zeroTolerance_;
            std::cout << "\nZero tolerance was replaced by " << zeroTolerance_ << std::endl;
            givenError = 0.0;
          }
        }
      }
      return;
    }


  } // namespace RB 
} // namespace Dune 
