#ifndef _NEWLIFTING_H_
#define _NEWLIFTING_H_

namespace Dune {
  namespace RB {
    template<class ErrorTraits, class ReducedFunctionImp>
    class NewInnerLifting
    {
      typedef NewInnerLifting<ErrorTraits, ReducedFunctionImp>                                  ThisType;
      typedef typename ErrorTraits::VectorFunctionType                    VectorFunctionType;
      typedef typename ErrorTraits::PhiListType                           PhiListType;
      typedef ReducedFunctionImp                   PhiType;
      typedef ReducedFunctionImp ReducedFunctionType;
      typedef typename ErrorTraits::VectorSpaceType                       VectorSpaceType;
      typedef typename PhiType::JacobianRangeType                         JacobianRangeType;
      typedef DiscreteFunctionList_mem<VectorFunctionType>                RightHandSideListType;
      typedef typename VectorSpaceType::BaseFunctionSetType               BaseFunctionSetType;
      typedef typename VectorSpaceType::RangeType                         RangeType;
      typedef typename VectorSpaceType::GridPartType                      GridPartType;
      typedef typename GridPartType::GridType                             GridType;
      typedef typename ErrorTraits::EntityType                            EntityType;
      typedef typename ErrorTraits::EntityPointerType                     EntityPointerType;
      typedef typename ErrorTraits::IntersectionType                      IntersectionType;
      typedef typename ErrorTraits::IntersectionIteratorType              IntersectionIteratorType;
      // ... and also for codimension 1 entities
      typedef CachingQuadrature<GridPartType, 1>                          FaceQuadratureType;
      typedef typename ErrorTraits::ElementQuadratureType                 ElementQuadratureType;
      typedef typename ErrorTraits::ProblemType                           ProblemType;
      typedef typename VectorFunctionType::DomainType                     DomainType;

      //! \todo This could for shure be done better 
      //(dynamic matrix or use polyniomal degree define and dim define (assert triangles))
      typedef FieldMatrix<double, 18, 18> MatrixType;
      typedef FieldVector<double, 18>     VectorType;

      //! polynomial order of base functions
      enum { polynomialOrder = VectorSpaceType::polynomialOrder };

    public:
      NewInnerLifting(const VectorSpaceType& vecSpace,
                   const ReducedFunctionType& func,
                   const IntersectionType& intersection,
                   const ProblemType& problem)
        : vecSpace_(vecSpace),
          gridPart_(vecSpace_.gridPart()),
          func_(func),
          intersection_(intersection),
          insidePtr_(intersection.inside()),
          outsidePtr_(intersection.outside()),
          insideEn_(*insidePtr_),
          outsideEn_(*outsidePtr_),
          problem_(problem),
          Nlambda_(problem_.Nlambda()),
          localIndex_(0),
          matrix_(0.0)
      {
        // assemble the matrix
        assembleMatrix(insideEn_);
        assembleMatrix(outsideEn_);
        matrix_.invert();

        // assemble the right hand side for all functions in phiList
        assembleRightHandSides();

      };

      ~NewInnerLifting() {}

    private:

      /** map a local dof number to an index, such that two dofs belonging
          to different cells but with the same global dofs will get the same
          index*/
      const unsigned int mapLocalToLocalIndex(const EntityType& entity, 
                                              const unsigned int i) {
        unsigned int ret;
        const unsigned int iGlobal = vecSpace_.mapToGlobal(entity, i);
        std::map<int,int>::const_iterator found = globalToLocalMap_.find(iGlobal);
        if (found==globalToLocalMap_.end()) {
          globalToLocalMap_[iGlobal]=localIndex_;
          localToGlobalMap_[localIndex_]=iGlobal;
          ret = localIndex_;
          ++localIndex_;
        } else
          ret = found->second;
        return ret;
      }

      void assembleMatrix(const EntityType& entity) {
        const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(entity);
        const int numBaseFuncs = baseFuncSet.numBaseFunctions();
        if (numBaseFuncs!=12)
          DUNE_THROW(NotImplemented, "For the moment we assume to have polorder 2 in 2D on triangles!");
        const typename EntityType::Geometry& geometry = entity.geometry();
        for (int i=0; i!=numBaseFuncs; ++i) {
          for (int j=0; j<=i; ++j) {
            // get the global indices, these give column and row in stiffness matrix
            const unsigned int row = mapLocalToLocalIndex(entity, i);
            const unsigned int col = mapLocalToLocalIndex(entity, j);
            
            ElementQuadratureType quadrature(entity, 2*(polynomialOrder+1));
            const unsigned int numQuadPoints = quadrature.nop();
            for (unsigned int qP=0; qP!=numQuadPoints; ++qP) {
              const double factor = 
                quadrature.weight(qP)*geometry.integrationElement(quadrature.point(qP));
              RangeType baseFuncVal1(0.0);
              RangeType baseFuncVal2(0.0);
              baseFuncSet.evaluate(i, quadrature.point(qP), baseFuncVal1);
              baseFuncSet.evaluate(j, quadrature.point(qP), baseFuncVal2);
              double value = baseFuncVal1*baseFuncVal2;
              value *= factor;
              matrix_[row][col] += value;
              if (i!=j)
                matrix_[col][row] += value;
            } 
          }
        }
      }

      void assembleRightHandSides() {
        // GrapeDataDisplay<GridType> bdisplay(phi.space().gridPart().grid());
        // bdisplay.addData(phi);  
        // bdisplay.display();          


        const typename PhiType::LocalFunctionType phiInsideLF = func_.localFunction(insideEn_);
        const typename PhiType::LocalFunctionType phiOutsideLF = func_.localFunction(outsideEn_);

        VectorType deltaRHS(0.0);
        VectorType psiRHS(0.0);

        const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(insideEn_);
        const BaseFunctionSetType& otherBaseFuncSet = vecSpace_.baseFunctionSet(outsideEn_);
        const int numBaseFuncs = baseFuncSet.numBaseFunctions();
        assert(numBaseFuncs==otherBaseFuncSet.numBaseFunctions());

        FaceQuadratureType faceQuad(vecSpace_.gridPart(), intersection_, 
                                    2*(polynomialOrder+1), FaceQuadratureType::INSIDE);
        const typename IntersectionType::Geometry &inGeo      = intersection_.geometry();
        const typename IntersectionType::Geometry &insideGeo  = intersection_.geometryInInside();
        const typename IntersectionType::Geometry &outsideGeo = intersection_.geometryInOutside();
        const unsigned int numQuadPoints = faceQuad.nop();

        for (int i=0; i!=numBaseFuncs; ++i) {
          int localIndexForInside = mapLocalToLocalIndex(insideEn_, i);
          int localIndexForOutside = mapLocalToLocalIndex(outsideEn_, i);

          // loop over all quadrature points
          for (unsigned int iqP = 0; iqP!=numQuadPoints; ++iqP) {
            typename FaceQuadratureType::LocalCoordinateType xLocal     = faceQuad.localPoint(iqP);
            typename FaceQuadratureType::CoordinateType      xInInside  = insideGeo.global(xLocal);
            typename FaceQuadratureType::CoordinateType      xInOutside = outsideGeo.global(xLocal);
            typename FaceQuadratureType::CoordinateType      xGlobal = inGeo.global(xLocal);

            const double factor = faceQuad.weight(iqP)*inGeo.integrationElement(xLocal);
            typename PhiType::RangeType phiInsideValue(0.0);
            typename PhiType::RangeType phiOutsideValue(0.0);
            phiInsideLF.evaluate(xInInside, phiInsideValue);
            phiOutsideLF.evaluate(xInOutside, phiOutsideValue);
            double deltaJump = phiInsideValue-phiOutsideValue;
            //              std::cout << "x: " << xGlobal << " phi inside: " << phiInsideValue << std::endl;
            typename BaseFunctionSetType::RangeType insideBaseFuncValue(0.0);
            baseFuncSet.evaluate(i, xInInside, insideBaseFuncValue);
            typename BaseFunctionSetType::RangeType outsideBaseFuncValue(0.0);
            otherBaseFuncSet.evaluate(i, xInOutside, outsideBaseFuncValue);          

            // Compute mean value of base functions of this entity on intersection
            double meanValue = 0.0;
            meanValue = (insideBaseFuncValue*intersection_.unitOuterNormal(xLocal));

            // update right hand side for delta computations
            deltaRHS[localIndexForInside]-=meanValue*factor*deltaJump;

            // update rhs for psi computations
            typename ProblemType::DiffusionMatrixType K;
            // evaluate diffusion matrix 
            problem_.K(xGlobal, K);
            JacobianRangeType phiInsideJac(0.0);
            JacobianRangeType phiOutsideJac(0.0);
            phiInsideLF.jacobian(xInInside, phiInsideJac);
            phiOutsideLF.jacobian(xInOutside, phiOutsideJac);
            //              std::cout << "x: " << xGlobal << " dPhi inside: " << phiInsideJac[0] << std::endl;
            typename ProblemType::RangeType lambda;
            problem_.lambda(xGlobal, lambda);
            // update right hand side for psi_sigma computation
            JacobianRangeType phiInsideJacTimesK(0.0);
            K.mv(phiInsideJac[0], phiInsideJacTimesK[0]);
            phiInsideJacTimesK *= lambda;
            double insideVal = phiInsideJacTimesK[0]*intersection_.unitOuterNormal(xLocal);
            JacobianRangeType phiOutsideJacTimesK(0.0);
            K.mv(phiOutsideJac[0], phiOutsideJacTimesK[0]);
            phiOutsideJacTimesK *= lambda;
            double outsideVal = phiOutsideJacTimesK[0]*intersection_.unitOuterNormal(xLocal);
            double psiJump = insideVal-outsideVal;
            psiRHS[localIndexForInside]-=meanValue*factor*psiJump;

            if (localIndexForInside!=localIndexForOutside) {
              // Compute mean value of base functions of outside entity on intersection
              meanValue = (outsideBaseFuncValue*intersection_.unitOuterNormal(xLocal));
              // update right hand side for psi_sigma computation
              psiRHS[localIndexForOutside]-=meanValue*factor*psiJump;
              deltaRHS[localIndexForOutside]-=meanValue*factor*deltaJump;
            }
          }
        }
        VectorType foo(0.0);
        // matrix is already inverted
        matrix_.mv(deltaRHS, foo);
        deltaVecs_.push_back(foo);
        foo=0.0;
        matrix_.mv(psiRHS, foo);
        psiVecs_.push_back(foo);
      }
      
    public:
      void evaluateDeltaLocal(const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = deltaVecs_[0];
        evaluateFuncLocal(vec, xLocal, ret);
        return;
      }

      void evaluatePsiLocal(const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = psiVecs_[0];
        evaluateFuncLocal(vec, xLocal, ret);
        return;
      }

      const unsigned int getNumEquations() const {
        return 1;
      }

      void replaceMap(const EntityPointerType& entityPtr) const {
        vecSpaceMapToGlobal_.clear();
        const EntityType& entity = *entityPtr;
        //        const BaseFunctionSetType&
          baseFuncSet_ = vecSpace_.baseFunctionSet(entity);
        int numBaseFuncs = baseFuncSet_.numBaseFunctions();
        for (int bF=0; bF!=numBaseFuncs; ++bF) {
          int globalIndex = vecSpace_.mapToGlobal(entity, bF);
          vecSpaceMapToGlobal_.push_back(globalIndex);
        }
        return;
      }


    private:
      void evaluateFuncLocal(const VectorType& vec, const DomainType& xLocal, RangeType& ret) const {
        const int mapSize = vecSpaceMapToGlobal_.size();
        assert(mapSize!=0);
        for (int bF=0; bF!=mapSize; ++bF) {
          int globalIndex = vecSpaceMapToGlobal_[bF];
          std::map<int, int>::const_iterator mapIt = globalToLocalMap_.find(globalIndex);
          if (mapIt!=globalToLocalMap_.end()) {
            RangeType baseFuncVal(0.0);
            baseFuncSet_.evaluate(bF, xLocal, baseFuncVal);
            baseFuncVal *= vec[mapIt->second];
            ret += baseFuncVal;
          }
        }
        return;
      }


    private:
      const VectorSpaceType&         vecSpace_;
      const GridPartType&            gridPart_;
      const ReducedFunctionType&     func_;
      const IntersectionType&        intersection_;
      EntityPointerType              insidePtr_;
      EntityPointerType              outsidePtr_;
      const EntityType&              insideEn_;
      const EntityType&              outsideEn_;
      const ProblemType&             problem_;
      const unsigned int             Nlambda_;
      int                            localIndex_;
      MatrixType                     matrix_;
      std::map<int, int>             globalToLocalMap_;
      std::map<int, int>             localToGlobalMap_;
      std::vector<VectorType>        deltaVecs_;
      std::vector<VectorType>        psiVecs_;
      std::vector<EntityPointerType> support_;
      static std::vector<int>       vecSpaceMapToGlobal_;
      static BaseFunctionSetType baseFuncSet_;
    };

    template<class ErrorTraits, class ReducedFunctionImp>
    std::vector<int> NewInnerLifting<ErrorTraits, ReducedFunctionImp>::vecSpaceMapToGlobal_;
    template<class ErrorTraits, class ReducedFunctionImp>
    typename ErrorTraits::VectorSpaceType::BaseFunctionSetType NewInnerLifting<ErrorTraits, ReducedFunctionImp>::baseFuncSet_;
  }
}

#endif /* _NEWLIFTING_H_ */
