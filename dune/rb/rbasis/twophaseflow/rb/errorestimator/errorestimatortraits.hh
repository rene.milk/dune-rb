#ifndef _ERRORESTIMATORTRAITS_H_
#define _ERRORESTIMATORTRAITS_H_

namespace Dune {
  namespace RB {
    template<class RBSpaceImpl, class ProblemImpl>
    struct ErrorEstimatorTraits {
      typedef RBSpaceImpl                                                           ReducedBasisSpace;
      typedef typename ReducedBasisSpace::GridPartType                              GridPartType;
      typedef typename GridPartType::GridType                                       GridType;
      // for the reconstructed functions
      typedef FunctionSpace<double, double, GridType::dimension, 1>                 ReconsFunctionSpace;
      enum { polynomialOrder = ReducedBasisSpace::polynomialOrder };

      typedef LagrangeDiscreteFunctionSpace<ReconsFunctionSpace, GridPartType, polynomialOrder> ReconstructedSpaceType;
      typedef AdaptiveDiscreteFunction<ReconstructedSpaceType>                                  FooType;
      typedef ConstrainedDiscreteFunction<FooType, GridPartType>                                ReconstructedFunctionType;
      // for the lifted functions
      typedef FunctionSpace<double, double, GridType::dimension, GridType::dimension>                             VectorFunctionSpace;
      typedef LagrangeDiscreteFunctionSpace<VectorFunctionSpace, GridPartType, polynomialOrder> VectorSpaceType;
      typedef AdaptiveDiscreteFunction<VectorSpaceType>                                         VectorFunctionType;
      typedef ConstrainedDiscreteFunctionList<ReconstructedFunctionType>                        PhiListType;
      typedef typename GridType::Traits::template Codim<0>::EntityPointer                       EntityPointerType;
      typedef typename EntityPointerType::Entity                                                EntityType;
      typedef typename EntityType::Geometry                                                     GeometryType;
      typedef typename VectorSpaceType::IteratorType                                            EntityIteratorType;
      typedef typename GridPartType::IntersectionIteratorType                                   IntersectionIteratorType;
      typedef typename IntersectionIteratorType::Intersection                                   IntersectionType;
      typedef typename VectorFunctionSpace::RangeFieldType                                      RangeFieldType;
      typedef Dune::SparseRowMatrixTraits<VectorSpaceType, VectorSpaceType>                     MatrixTraits;
      typedef typename MatrixTraits::template MatrixObject<LagrangeMatrixTraits<MatrixTraits> >
      ::MatrixObjectType                                                                        SystemMatrixType;
      typedef ProblemImpl                                                                       ProblemType;
      typedef CachingQuadrature<GridPartType, 0>                                                ElementQuadratureType;
      typedef Matrix<FieldMatrix<double,1,1> >                              MatrixType;
      typedef BlockVector<FieldVector<double,1> >                           VectorType;
      typedef typename GridPartType::GridType::LeafGridView                                     LeafGridView;
      typedef SingleCodimSingleGeomTypeMapper<LeafGridView, GridType::dimension-1>                                  IntersectionIndexSetType;

    };
  }
}


#endif /* _ERRORESTIMATORTRAITS_H_ */
