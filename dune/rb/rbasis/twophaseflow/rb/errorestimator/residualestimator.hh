#ifndef _RESIDUALESTIMATOR_H_
#define _RESIDUALESTIMATOR_H_

#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>


// include boost
#include <boost/filesystem/operations.hpp>
#include <boost/timer/timer.hpp>

#include <Eigen/Eigen>
#include <Eigen/Sparse>
#include <Eigen/SuperLUSupport>

namespace Dune {
namespace RB {
// MARK: Contructors
template< class ReducedSpaceType, class HighDimOperatorType, class ProblemType,
          class LocalizerImp >
class ResidualEstimator
{
  typedef typename ProblemType::ParameterDomainType                           ParameterDomainType;
  typedef typename HighDimOperatorType::EigenMatrixType                       SystemMatrixType;
  typedef LocalizerImp                                                        LocalizerType;
  // typedef Eigen::SuperLU<SystemMatrixType>                  SolverType;
  typedef Eigen::BiCGSTAB< SystemMatrixType, Eigen::IncompleteLUT< double > > SolverType;
  typedef Eigen::MatrixXd                                                     MatrixType;
  typedef Eigen::VectorXd                                                     VectorType;
  typedef MatrixType                                                          RieszRepresentantsListType;

public:
  ResidualEstimator(const ReducedSpaceType& rbSpace,
                    HighDimOperatorType& highDimOp,
                    ProblemType& problem,
                    const LocalizerType& localizer,
                    const ParameterDomainType& muBar)
    : rbSpace_(rbSpace),
      highDimOp_(highDimOp),
      problem_(problem),
      localizer_(localizer),
      muBar_(muBar),
      QLh_(1 + problem_.dirichletValues().size() * problem_.mobility().size() + problem_.neumannValues().size()),
      QBh_(problem_.mobility().size()),
      rieszRepresentants_(highDimOp.discreteFunctionSpace().size(), QLh_),
      initialized_(false),
      subdomainBasisSizes_(VectorType::Zero(rbSpace_.gridPart().numSubdomains()))
  {
    double dropTol    = Parameter::getValue< double >("residualestimator.solver.preconditioner.dropTol", 1e-4);
    int    fillFactor = Parameter::getValue< int >("residualestimator.solver.preconditioner.fillFactor", 20);

    solver_.preconditioner().setDroptol(dropTol);
    solver_.preconditioner().setFillfactor(fillFactor);

    /* set the number of threads used for computing the riesz representants */
    omp_set_num_threads(Parameter::getValue< int >("residualestimator.numThreads"));
  }


  ~ResidualEstimator()
  {
    if (Parameter::getValue< bool >("residualestimator.saveData", false))
      save();
  }


  /** Update the error estimator after new base functions have been added to the reduced space.
   *
   *  This computes the new riesz representants and updates the matrix @f$G@f$.
   */
  void update()
  {
    // if error estimator was not initialized so far, prepare the solver and compute the representants for Lh
    if (!initialized_) {
      // prepare the solver for solves in the future
      prepareSolver();

      /* solve for all riesz representants where the right hand sides are the right hand side parts of the
       *  high dimensional equation*/
      if (rieszRepresentants_.cols() == QLh_)
        solveLh();

      initialized_ = true;

      // compute the new riesz representants
      solveBhNew();
      // precompute G
      computeGFull();
    } else {
      // compute the new riesz representants
      solveBhNew();
      // precompute G
      computeGNew();
    }
    return;
  }   /* update */


  /** Update the error estimator: Recompute all riesz representants and depending quantities.
   *
   *  This computes the new riesz representants and updates the matrix @f$G@f$.
   */
  void updateFull()
  {
    /* run the update with as many threads as the corase grid has entities, if possible. Else, run it with the
     *  maximum number of threads*/
    omp_set_num_threads(std::min(omp_get_num_procs(), int(rbSpace_.gridPart().numSubdomains())));

    // if error estimator was not initialized so far, prepare the solver and compute the representants for Lh
    if (!initialized_) {
      // prepare the solver for solves in the future
      prepareSolver();

      /* solve for all riesz representants where the right hand sides are the right hand side parts of the
       *  high dimensional equation*/
      if (rieszRepresentants_.cols() == QLh_)
        solveLh();

      initialized_ = true;
    }
    // compute the new riesz representants
    solveBhFull();
    // precompute G
    computeGFull();

    return;
  }   /* update */

  /** Save the computed riesz representants to disk.
   *
   *  The representants will be saved in the directory ./residualEstimator/representants.bin.
   *  They are saved in as Eigen::Matrix in binary format.
   */
  void save() const
  {
    boost::filesystem::create_directory("residualEstimator");
    rieszRepresentants_.saveToBinaryFile("residualEstimator/representants.bin");
    return;
  }

  /** Load the riesz representants from disk.
   *
   *  The representants will be loaded from the file ./residualEstimator/representants.bin. The file is assumed to be
   *  in binary format. This also calls the computeG method such that the estimator is ready for evaluation once this
   *  method is finished.
   */
  void load()
  {
    // load the representants
    rieszRepresentants_.loadFromBinaryFile("residualEstimator/representants.bin");

    // compute G to make error estimator ready for evaluation.
    // computeGFull();
    return;
  } /* load */

  /** Evaluate the upper bound of the error for a given reduced function
   *
   * @tparam ReducedFunctionType The type of the reduced function.
   * @param[in] pH The reduced function.
   */
  template< class ReducedFunctionType >
  double evaluate(const ReducedFunctionType& pH) const
  {
    // compute the matching Theta for this reduced solution pH
    updateTheta(pH);

    if (G_.cols() == 0)
      DUNE_THROW(InvalidStateException, "You need to call ResidualEstimator::update() before calling evaluate!");

    // compute squared error
    double squaredResidualNorm = Theta_.transpose() * G_ * Theta_;
    if (squaredResidualNorm < 0.0) {
      if (squaredResidualNorm < -1.0)
        std::cout << "Set error " << squaredResidualNorm << " to zero!\n";
      squaredResidualNorm = 0.0;
    }

    double estimatedError = sqrt(squaredResidualNorm) * computeFactor();

    return estimatedError;
  }   /* evaluate */

  /** Evalute the lower bound of the error for a given reduced function
   *
   * @tparam ReducedFunctionType The type of the reduced function.
   * @param[in] pH The reduced function.
   */
  template< class ReducedFunctionType >
  double lowerBound(const ReducedFunctionType& pH) const
  {
    // compute the matching Theta for this reduced solution pH
    updateTheta(pH);

    if (G_.cols() == 0)
      DUNE_THROW(InvalidStateException, "You need to call ResidualEstimator::update() before calling evaluate!");

    // compute squared error
    double squaredResidualNorm = Theta_.transpose() * G_ * Theta_;
    if (squaredResidualNorm < 0.0) {
      if (squaredResidualNorm < -1.0)
        std::cout << "Set error " << squaredResidualNorm << " to zero!\n";
      squaredResidualNorm = 0.0;
    }

    return sqrt(squaredResidualNorm) * computeOtherFactor();
  }   /* lowerBound */

private:
  /** Get a reference to the system matrix.
   *
   *  This will copy the system matrix from the high-dimensional operator on first call.
   *
   * @returns Returns a reference to the system matrix for the parameter @f$\bar\mu@f$.
   */
  const SystemMatrixType& systemMatrix()
  {
    if ((systemMatrix_.cols() == 0) && (systemMatrix_.rows() == 0))
      // copy the system matrix. Sadly, this is neccessary if we want to compute the preconditioner only once.
      systemMatrix_ = highDimOp_.systemMatrixEigen();
    assert(systemMatrix_.cols() != 0 && systemMatrix_.rows() != 0 && "System matrix is not set correctly!");
    return systemMatrix_;
  } /* systemMatrix */

  /** Prepare the solver.
   *
   * If we are using SuperLU, this means that the LU decomposition is computed. If we are
   * using an iterative solver, this means that the preconditioner is initialized.
   */
  void prepareSolver()
  {
    boost::timer::auto_cpu_timer timer(2, "(Prepare solver: %w) ");
    ParameterDomainType          muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);

    // Call SolverType::compute. This does what was described above.
    solver_.compute(systemMatrix());

    problem_.mobility().setParameter(muBak);
    return;
  }   

  /** Solve the system one time for given right hand side.
   *
   *  @tparam RightHandSideType The type of the right hand side function
   *  @tparam SolutionType      The type of the solution function
   *  @param[in]  rightHandSide The right hand side for the equation
   *  @param[out] solution The solution.
   *
   *  @warning This expects the solver (LU, BiCG, ...) to be up to date.
   *
   */
  template< class RightHandSideType, class SolutionType >
  void solve(const RightHandSideType& rightHandSide, SolutionType& solution)
  {
    // solve
    solution = solver_.solve(rightHandSide);
    return;
  }


  /** Solve the equation @f$ (r^q,v)_{B_h(\bar\mu)}=L_h^i(v)@f$ for all right hand side parts @f$L_h^i@f$
   */
  void solveLh()
  {
    boost::timer::auto_cpu_timer timer(2, "(Solve for Lh: %w) ");
    ParameterDomainType          muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);
    double dropTol    = Parameter::getValue< double >("residualestimator.solver.preconditioner.dropTol", 1e-4);
    int    fillFactor = Parameter::getValue< int >("residualestimator.solver.preconditioner.fillFactor", 20);
    double solverEps
      = Parameter::getValue< double >("residualestimator.solver.eps", 1.0e-12);

    assert(rieszRepresentants_.cols() == QLh_);
#pragma omp parallel for
    for (int i = 0; i < QLh_; ++i) {
      const VectorType& rightHandSide = highDimOp_.getLhComponentEigen(i);

      VectorType solution;

      SolverType solver;
      solver.preconditioner().setDroptol(dropTol);
      solver.preconditioner().setFillfactor(fillFactor);
      solver.setTolerance(solverEps);
      SystemMatrixType sysCopy = systemMatrix();
      solver.compute(sysCopy);
      solution                   = solver.solve(rightHandSide);
      rieszRepresentants_.col(i) = solution;
    }

    problem_.mobility().setParameter(muBak);
    return;
  }   /* solveLh */


  /** Solve the equation @f$ (r^q,v)_{B_h(\bar\mu)}=B_h^i(\phi_j,v)@f$ for all operator parts @f$B_h^i@f$
   *   and all reduced basis base functions @f$\phi_j@f$.
   */
  void solveBhFull()
  {
    boost::timer::auto_cpu_timer timer(2, "(Solve for Bh: %w) ");
    ParameterDomainType          muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);

    const int sizeOfTh = rbSpace_.gridPart().numSubdomains();

    VectorType subdomainBasisSizes(sizeOfTh);

    for (int subDomain = 0; subDomain < sizeOfTh; ++subDomain)
      subdomainBasisSizes(subDomain) = rbSpace_.getSubdomainBasis(subDomain).cols();
    const int maximumSize = subdomainBasisSizes.maxCoeff();

    // enlarge the matrix holding the riesz representants and get a reference to the new column
    rieszRepresentants_.conservativeResize(Eigen::NoChange, QLh_ + subdomainBasisSizes.sum() * QBh_);


#pragma omp parallel for
    for (int k = 0; k < maximumSize * sizeOfTh; ++k) {
      int baseFunc  = std::floor(k / sizeOfTh);
      int subDomain = k - baseFunc * sizeOfTh;

      const MatrixType& subdomainBasis        = rbSpace_.getSubdomainBasis(subDomain);
      const int         numLocalBaseFunctions = subdomainBasis.cols();
      if (baseFunc < numLocalBaseFunctions) {
        int numElementsBefore = 0;
        for (int j = 0; j < baseFunc; ++j)
          for (int l = 0; l < sizeOfTh; ++l) {
            if (subdomainBasisSizes(l) > j)
              numElementsBefore += QBh_;
          }


        for (int l = 0; l < subDomain; ++l) {
          if (subdomainBasisSizes(l) > baseFunc)
            numElementsBefore += QBh_;
        }
        for (int i = 0; i < QBh_; ++i) {
          const VectorType baseFunctionLocalized = subdomainBasis.col(baseFunc);
          VectorType       baseFunctionFull;
          localizer_.getFullVector(baseFunctionLocalized, subDomain, baseFunctionFull);

          const VectorType rightHandSide = highDimOp_.getBhComponentEigenHigh(i) * baseFunctionFull;
          // solve
          VectorType solution;
          solution                                       = solver_.solve(rightHandSide);
          rieszRepresentants_.col(numElementsBefore + i) = solution;
        }
      }
      // }
    }

    problem_.mobility().setParameter(muBak);
    return;
  }   /* solveBhFull */

  /** Solve the equation @f$ (r^q,v)_{B_h(\bar\mu)}=B_h^i(\phi_j,v)@f$ for all operator parts @f$B_h^i@f$
   *   and all new reduced basis base functions @f$\phi_j@f$ (those that were added since the last call to this
   *   function).
   */
  void solveBhNew()
  {
    boost::timer::auto_cpu_timer timer;

    ParameterDomainType muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);

    const int sizeOfTh = rbSpace_.gridPart().numSubdomains();

    int oldSize         = rieszRepresentants_.cols();
    int numNewFunctions = 0;
    for (int element = 0; element < sizeOfTh; ++element) {
      numNewFunctions += rbSpace_.getSubdomainBasis(element).cols() - subdomainBasisSizes_(element);
    }

    // enlarge the matrix holding the riesz representants and get a reference to the new column
    rieszRepresentants_.conservativeResize(Eigen::NoChange, oldSize + numNewFunctions * QBh_);

    double dropTol    = Parameter::getValue< double >("residualestimator.solver.preconditioner.dropTol", 1e-4);
    int    fillFactor = Parameter::getValue< int >("residualestimator.solver.preconditioner.fillFactor", 20);
    double solverEps
      = Parameter::getValue< double >("residualestimator.solver.eps", 1.0e-12);

#pragma omp parallel for
    for (int k = 0; k < sizeOfTh * QBh_; ++k) {
      const int subDomain = std::floor(k / QBh_);
      const int i         = k - subDomain * QBh_;

      const MatrixType& subdomainBasis        = rbSpace_.getSubdomainBasis(subDomain);
      const int         numLocalBaseFunctions = subdomainBasis.cols();
      if (numLocalBaseFunctions > subdomainBasisSizes_(subDomain)) {
        int elementsUpdateBefore = 0;
        for (int j = 0; j < subDomain; ++j)
          if (rbSpace_.getSubdomainBasis(j).cols() > subdomainBasisSizes_(j))
            ++elementsUpdateBefore;

        // for the time being, we assume, that at most one base function is added in each step
        assert(numLocalBaseFunctions == subdomainBasisSizes_(subDomain) + 1);

        int baseFunc = numLocalBaseFunctions - 1;

        const VectorType baseFunctionLocalized = subdomainBasis.col(baseFunc);
        VectorType       baseFunctionFull;
        localizer_.getFullVector(baseFunctionLocalized, subDomain, baseFunctionFull);

        const VectorType rightHandSide = highDimOp_.getBhComponentEigenHigh(i) * baseFunctionFull;

        // solve
        SolverType solver;
        solver.preconditioner().setDroptol(dropTol);
        solver.preconditioner().setFillfactor(fillFactor);
        solver.setTolerance(solverEps);
        SystemMatrixType sysCopy = systemMatrix();
        solver.compute(sysCopy);
        VectorType solution = solver.solve(rightHandSide);

        rieszRepresentants_.col(oldSize + (elementsUpdateBefore * QBh_) + i) = solution;
      }
    }


    // update the basis sizes
    for (int element = 0; element < sizeOfTh; ++element) {
      subdomainBasisSizes_(element) = rbSpace_.getSubdomainBasis(element).cols();
    }

    problem_.mobility().setParameter(muBak);
    return;
  }   /* solveBh */


  /** Compute the system matrix @f$G=\sum_{T\in\Tau_H}G^T@f$, @f$(G^T)_{i,j}=B_h^T(r_i,r_j,\bar\mu)@f$ recomputing all
   *  entries.
   */
  void computeGFull()
  {
    boost::timer::auto_cpu_timer timer(2, "(Update G: %w) ");
    ParameterDomainType          muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);

    G_ = rieszRepresentants_.transpose() * systemMatrix() * rieszRepresentants_;

    problem_.mobility().setParameter(muBak);
    return;
  }   /* computeG */


  /** Compute the system matrix @f$G=\sum_{T\in\Tau_H}G^T@f$, @f$(G^T)_{i,j}=B_h^T(r_i,r_j,\bar\mu)@f$ only updating
   * the last rows and columns (since the last update).
   */
  void computeGNew()
  {
    boost::timer::auto_cpu_timer timer(2, "(Update G (new): %w) ");
    ParameterDomainType          muBak = problem_.mobility().getParameter();

    problem_.mobility().setParameter(muBar_);
    const int oldSize  = G_.rows();
    const int numOfNew = rieszRepresentants_.cols() - oldSize;

    G_.conservativeResize(oldSize + numOfNew, oldSize + numOfNew);
    G_.bottomRows(numOfNew) = rieszRepresentants_.rightCols(numOfNew).transpose() * systemMatrix()
                              * rieszRepresentants_;
    G_.rightCols(numOfNew) = rieszRepresentants_.transpose() * systemMatrix() * rieszRepresentants_.rightCols(numOfNew);

    problem_.mobility().setParameter(muBak);
    return;
  }   /* computeG */


  /** Update the coefficient vector @f$\Theta_r(\mu)@f$ for a given reduced function.
   *
   *  @tparam ReducedFunctionType The type of the function.
   *  @param[in] pH The reduced function.
   */
  template< class ReducedFunctionType >
  void updateTheta(const ReducedFunctionType& pH) const
  {
    Theta_ = VectorType::Zero(QLh_ + pH.rows() * QBh_);
    // write coefficients for Lh
    Theta_(0) = 1.0;
    const int Nlambda = problem_.mobility().size();
    for (int i = 0; i < int(problem_.dirichletValues().size()); ++i) {
      const double muForGd = problem_.dirichletValues().coefficient(i);
      for (int opComp = 0; opComp < Nlambda; ++opComp) {
        const int index = i * Nlambda + opComp;
        Theta_(index + 1) = problem_.mobility().coefficient(opComp) * muForGd;
      }
    }
    for (int i = 0; i < int(problem_.neumannValues().size()); ++i) {
      Theta_(1 + problem_.dirichletValues().size() * Nlambda + i) = problem_.neumannValues().coefficient(i);
    }

    // write Coefficients for Bh
    int                k = QLh_;
    std::vector< int > subdomainBasisSizes;
    const int          sizeOfTh = rbSpace_.gridPart().numSubdomains();
    for (int subDomain = 0; subDomain < sizeOfTh; ++subDomain)
      subdomainBasisSizes.push_back(rbSpace_.getSubdomainBasis(subDomain).cols());
    const int maximumSize = *(std::max_element(subdomainBasisSizes.begin(), subdomainBasisSizes.end()));
    for (int baseFunc = 0; baseFunc < maximumSize; ++baseFunc) {
      for (int subDomain = 0; subDomain != sizeOfTh; ++subDomain) {
        const int numLocalBaseFunctions = rbSpace_.getSubdomainBasis(subDomain).cols();
        if (baseFunc < numLocalBaseFunctions) {
          for (int i = 0; i != QBh_; ++i) {
            const int index = std::accumulate(subdomainBasisSizes.begin(),
                                              subdomainBasisSizes.begin() + subDomain,
                                              0.0) + baseFunc;
            assert(index < pH.rows());
            Theta_(k) = -1.0 * problem_.mobility().coefficient(i) * pH(index);
            k++;
          }
        }
      }
    }
    return;
  }   /* updateTheta */

  double computeFactor() const
  {
    std::vector< double > ThetaOfMu;
    const int             Nlambda = problem_.mobility().size();
    double                minimum = std::numeric_limits< double >::max();
    for (int i = 0; i != Nlambda; ++i) {
      ThetaOfMu.push_back(problem_.mobility().coefficient(i));
    }
    ParameterDomainType muBak = problem_.mobility().getParameter();
    problem_.mobility().setParameter(muBar_);
    for (int i = 0; i != Nlambda; ++i) {
      double quotient = ThetaOfMu[i] / problem_.mobility().coefficient(i);
      assert(quotient >= 0);
      if (quotient < minimum)
        minimum = quotient;
    }
    problem_.mobility().setParameter(muBak);

    assert(minimum > 0.0);

    return 1.0 / (minimum);
  }   /* computeFactor */


  double computeOtherFactor() const
  {
    std::vector< double > ThetaOfMu;
    const int             Nlambda = problem_.mobility().size();
    double                maximum = std::numeric_limits< double >::min();
    for (int i = 0; i != Nlambda; ++i) {
      ThetaOfMu.push_back(problem_.mobility().coefficient(i));
    }
    ParameterDomainType muBak = problem_.mobility().getParameter();
    problem_.mobility().setParameter(muBar_);
    for (int i = 0; i != Nlambda; ++i) {
      double quotient = ThetaOfMu[i] / problem_.mobility().coefficient(i);
      assert(quotient >= 0);
      if (quotient > maximum)
        maximum = quotient;
    }
    problem_.mobility().setParameter(muBak);

    assert(maximum > 0.0);

    return 1.0 / (maximum);
  }   /* computeFactor */

  const ReducedSpaceType&    rbSpace_;
  const HighDimOperatorType& highDimOp_;
  ProblemType&               problem_;
  const LocalizerType&       localizer_;
  const ParameterDomainType  muBar_;
  SolverType                 solver_;
  const int                  QLh_;
  const int                  QBh_;
  RieszRepresentantsListType rieszRepresentants_;
  MatrixType                 G_;
  mutable VectorType         Theta_;
  bool                       initialized_;
  SystemMatrixType           systemMatrix_;
  VectorType                 subdomainBasisSizes_;
};
} // namespace RB
} // namespace Dune


#endif /* _RESIDUALESTIMATOR_H_ */
