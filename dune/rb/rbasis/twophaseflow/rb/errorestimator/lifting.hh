#ifndef _LIFTING_H_
#define _LIFTING_H_

namespace Dune {
  namespace RB {
    template<class ErrorTraits>
    class InnerLifting
    {
      typedef InnerLifting<ErrorTraits>                                  ThisType;
      typedef typename ErrorTraits::VectorFunctionType                    VectorFunctionType;
      typedef typename ErrorTraits::PhiListType                           PhiListType;
      typedef typename PhiListType::DiscreteFunctionType                  PhiType;
      typedef typename ErrorTraits::VectorSpaceType                       VectorSpaceType;
      typedef typename PhiType::JacobianRangeType                         JacobianRangeType;
      typedef DiscreteFunctionList_mem<VectorFunctionType>                RightHandSideListType;
      typedef typename VectorSpaceType::BaseFunctionSetType               BaseFunctionSetType;
      typedef typename VectorSpaceType::RangeType                         RangeType;
      typedef typename VectorSpaceType::GridPartType                      GridPartType;
      typedef typename GridPartType::GridType                             GridType;
      typedef typename ErrorTraits::EntityType                            EntityType;
      typedef typename ErrorTraits::EntityPointerType                     EntityPointerType;
      typedef typename ErrorTraits::IntersectionType                      IntersectionType;
      typedef typename ErrorTraits::IntersectionIteratorType              IntersectionIteratorType;
      // ... and also for codimension 1 entities
      typedef CachingQuadrature<GridPartType, 1>                          FaceQuadratureType;
      typedef typename ErrorTraits::ElementQuadratureType                 ElementQuadratureType;
      typedef typename ErrorTraits::ProblemType                           ProblemType;
      typedef typename VectorFunctionType::DomainType                     DomainType;

      //! \todo This could for shure be done better 
      //(dynamic matrix or use polyniomal degree define and dim define (assert triangles))
      typedef FieldMatrix<double, 18, 18> MatrixType;
      typedef FieldVector<double, 18>     VectorType;

      //! polynomial order of base functions
      enum { polynomialOrder = VectorSpaceType::polynomialOrder };

    public:
      InnerLifting(const VectorSpaceType& vecSpace,
                   const PhiListType& phiList,
                   const IntersectionType& intersection,
                   const ProblemType& problem,
                   bool doInside)
        : vecSpace_(vecSpace),
          gridPart_(vecSpace_.gridPart()),
          phiList_(phiList),
          intersection_(intersection),
          insidePtr_(intersection.inside()),
          outsidePtr_(intersection.outside()),
          insideEn_(*insidePtr_),
          outsideEn_(*outsidePtr_),
          problem_(problem),
          Nlambda_(problem_.Nlambda()),
          localIndex_(0),
          matrix_(0.0),
          doInside_(doInside),
          verbose_(Parameter::getValue<bool>("lifting.verbose", false))
      {
        // assemble the matrix
        assembleMatrix(insideEn_);
        assembleMatrix(outsideEn_);
        matrix_.invert();

        // assemble the right hand side for all functions in phiList
        assembleRightHandSides();

      };

      ~InnerLifting() {}

    private:
      /** map a local dof number to an index, such that two dofs belonging
          to different cells but with the same global dofs will get the same
          index*/
      const unsigned int mapLocalToLocalIndex(const EntityType& entity, 
                                              const unsigned int i) {
        unsigned int ret;
        const unsigned int iGlobal = vecSpace_.mapToGlobal(entity, i);
        std::map<int,int>::const_iterator found = globalToLocalMap_.find(iGlobal);
        if (found==globalToLocalMap_.end()) {
          globalToLocalMap_[iGlobal]=localIndex_;
          localToGlobalMap_[localIndex_]=iGlobal;
          ret = localIndex_;
          ++localIndex_;
        } else
          ret = found->second;
        return ret;
      }

      void assembleMatrix(const EntityType& entity) {
        const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(entity);
        const int numBaseFuncs = baseFuncSet.numBaseFunctions();
        if (numBaseFuncs!=12)
          DUNE_THROW(NotImplemented, "For the moment we assume to have polorder 2 in 2D on triangles!");
        const typename EntityType::Geometry& geometry = entity.geometry();
        for (int i=0; i!=numBaseFuncs; ++i) {
          for (int j=0; j<=i; ++j) {
            // get the global indices, these give column and row in stiffness matrix
            const unsigned int row = mapLocalToLocalIndex(entity, i);
            const unsigned int col = mapLocalToLocalIndex(entity, j);
            
            ElementQuadratureType quadrature(entity, 2*(polynomialOrder+1));
            const unsigned int numQuadPoints = quadrature.nop();
            for (unsigned int qP=0; qP!=numQuadPoints; ++qP) {
              const double factor = 
                quadrature.weight(qP)*geometry.integrationElement(quadrature.point(qP));
              RangeType baseFuncVal1(0.0);
              RangeType baseFuncVal2(0.0);
              baseFuncSet.evaluate(i, quadrature.point(qP), baseFuncVal1);
              baseFuncSet.evaluate(j, quadrature.point(qP), baseFuncVal2);
              double value = baseFuncVal1*baseFuncVal2;
              value *= factor;
              matrix_[row][col] += value;
              if (i!=j)
                matrix_[col][row] += value;
            } 
          }
        }
      }

      void assembleRightHandSides() {
        PhiType phi("phiAssembleRHS", phiList_.space());
        const unsigned int listSize = phiList_.size();

        std::vector<VectorType> psiRHSVec(listSize*Nlambda_, VectorType(0.0));

        for (unsigned int l=0; l!=listSize; ++l) {
          phi.clear();
          phiList_.getFunc(l, phi);

          // GrapeDataDisplay<GridType> bdisplay(phi.space().gridPart().grid());
          // bdisplay.addData(phi);  
          // bdisplay.display();          


          const typename PhiType::LocalFunctionType phiInsideLF = phi.localFunction(insideEn_);
          const typename PhiType::LocalFunctionType phiOutsideLF = phi.localFunction(outsideEn_);

          VectorType deltaRHS(0.0);

          const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(insideEn_);
          const BaseFunctionSetType& otherBaseFuncSet = vecSpace_.baseFunctionSet(outsideEn_);
          const int numBaseFuncs = baseFuncSet.numBaseFunctions();
          assert(numBaseFuncs==otherBaseFuncSet.numBaseFunctions());

          FaceQuadratureType faceQuad(vecSpace_.gridPart(), intersection_, 
                                      2*(polynomialOrder+1), FaceQuadratureType::INSIDE);
          const typename IntersectionType::Geometry &inGeo      = intersection_.geometry();
          const typename IntersectionType::Geometry &insideGeo  = intersection_.geometryInInside();
          const typename IntersectionType::Geometry &outsideGeo = intersection_.geometryInOutside();
          const unsigned int numQuadPoints = faceQuad.nop();

          for (int i=0; i!=numBaseFuncs; ++i) {
            int localIndexForInside = mapLocalToLocalIndex(insideEn_, i);
            int localIndexForOutside = mapLocalToLocalIndex(outsideEn_, i);

            // loop over all quadrature points
            for (unsigned int iqP = 0; iqP!=numQuadPoints; ++iqP) {
              typename FaceQuadratureType::LocalCoordinateType xLocal     = faceQuad.localPoint(iqP);
              typename FaceQuadratureType::CoordinateType      xInInside  = insideGeo.global(xLocal);
              typename FaceQuadratureType::CoordinateType      xInOutside = outsideGeo.global(xLocal);
              typename FaceQuadratureType::CoordinateType      xGlobal = inGeo.global(xLocal);

              const double factor = faceQuad.weight(iqP)*inGeo.integrationElement(xLocal);
              typename PhiType::RangeType phiInsideValue(0.0);
              typename PhiType::RangeType phiOutsideValue(0.0);
              phiInsideLF.evaluate(xInInside, phiInsideValue);
              phiOutsideLF.evaluate(xInOutside, phiOutsideValue);
              //              double phiJump = phiInsideValue[0]-phiOutsideValue[0];
              //              std::cout << "x: " << xGlobal << " phi inside: " << phiInsideValue << std::endl;
              typename BaseFunctionSetType::RangeType insideBaseFuncValue(0.0);
              baseFuncSet.evaluate(i, xInInside, insideBaseFuncValue);
              typename BaseFunctionSetType::RangeType outsideBaseFuncValue(0.0);
              otherBaseFuncSet.evaluate(i, xInOutside, outsideBaseFuncValue);          

              // Compute mean value of base functions of this entity on intersection
              double insideMeanValue = (insideBaseFuncValue*intersection_.unitOuterNormal(xLocal));
              double outsideMeanValue = (outsideBaseFuncValue*intersection_.unitOuterNormal(xLocal));

              // update right hand side for delta computations
              if (doInside_) {
                deltaRHS[localIndexForInside]-=insideMeanValue*factor*phiInsideValue[0];
                if (localIndexForInside!=localIndexForOutside) 
                  deltaRHS[localIndexForOutside]-=outsideMeanValue*factor*phiInsideValue[0];
              }
              else {
                deltaRHS[localIndexForInside]-=insideMeanValue*factor*phiOutsideValue[0];
                if (localIndexForInside!=localIndexForOutside) 
                  deltaRHS[localIndexForOutside]-=outsideMeanValue*factor*phiOutsideValue[0];
              }
              // update rhs for psi computations
              typename ProblemType::DiffusionMatrixType K;
              // evaluate diffusion matrix 
              problem_.K(xGlobal, K);
              JacobianRangeType phiInsideJac(0.0);
              JacobianRangeType phiOutsideJac(0.0);
              phiInsideLF.jacobian(xInInside, phiInsideJac);
              phiOutsideLF.jacobian(xInOutside, phiOutsideJac);
              //              std::cout << "x: " << xGlobal << " dPhi inside: " << phiInsideJac[0] << std::endl;
              for (unsigned int sigma=0; sigma<Nlambda_; ++sigma) {
                typename ProblemType::RangeType lambda;
                problem_.lambdaPart(sigma, xGlobal, lambda);
                VectorType& psiRHS = psiRHSVec[l*Nlambda_+sigma];
                // update right hand side for psi_sigma computation
                JacobianRangeType phiInsideJacTimesK(0.0);
                K.mv(phiInsideJac[0], phiInsideJacTimesK[0]);
                phiInsideJacTimesK *= lambda;
                JacobianRangeType phiOutsideJacTimesK(0.0);
                K.mv(phiOutsideJac[0], phiOutsideJacTimesK[0]);
                phiOutsideJacTimesK *= lambda;
                double insideVal = phiInsideJacTimesK[0]*intersection_.unitOuterNormal(xLocal);
                double outsideVal = phiOutsideJacTimesK[0]*intersection_.unitOuterNormal(xLocal);
                if (doInside_) {
                  psiRHS[localIndexForInside]-=insideMeanValue*factor*insideVal;
                  if (localIndexForInside!=localIndexForOutside) 
                    psiRHS[localIndexForOutside]-=outsideMeanValue*factor*insideVal;
                }
                else {
                  psiRHS[localIndexForInside]-=insideMeanValue*factor*outsideVal;
                  if (localIndexForInside!=localIndexForOutside) 
                    psiRHS[localIndexForOutside]-=outsideMeanValue*factor*outsideVal;
                }
              }
            }
          }
          VectorType foo(0.0);
          // matrix is already inverted
          matrix_.mv(deltaRHS, foo);
          deltaVecs_.push_back(foo);
          for (unsigned int sigma=0; sigma<Nlambda_; ++sigma) {
            VectorType& psiRHS = psiRHSVec[l*Nlambda_+sigma];
            foo=0.0;
            matrix_.mv(psiRHS, foo);
            // if (l==1)
            //   std::cout << "foo: " << foo;
            psiVecs_.push_back(foo);
          }
        }// iteration over all phi in phiList
        if (verbose_) {
          int deltaSize = deltaVecs_.size();
          int psiSize = psiVecs_.size();
          std::cout << "DELTA: \n";
          for (int i=0; i!=deltaSize; ++i) 
            std::cout << deltaVecs_[i] << std::endl;
          std::cout << "PSI: (size " << psiSize << ") \n";
          for (int i=0; i!=psiSize; ++i) 
            std::cout << psiVecs_[i] << std::endl;
          std::cout << "Output done!\n";
        }
      } // assemble rhs

      // bool inSupport(const EntityPointerType& entityPtr) const {
      //   return (std::find(support_.begin(), support_.end(), entityPtr)!=support_.end());
      // }

    public:
      void evaluateDeltaLocal(const unsigned int i, const EntityPointerType& entityPtr,
                              const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = deltaVecs_[i];
        evaluateFuncLocal(vec, entityPtr, xLocal, ret);
        return;
      }

      void evaluatePsiLocal(const unsigned int i, const EntityPointerType& entityPtr,
                            const unsigned int sigma,
                            const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = psiVecs_[i*Nlambda_+sigma];
        evaluateFuncLocal(vec, entityPtr, xLocal, ret);
        return;
      }

      const unsigned int getNumEquations() const {
        return phiList_.size();
      }

      void replaceMap(const EntityPointerType& entityPtr) const {
        vecSpaceMapToGlobal_.clear();
        const EntityType& entity = *entityPtr;
        baseFuncSet_ = vecSpace_.baseFunctionSet(entity);
        int numBaseFuncs = baseFuncSet_.numBaseFunctions();
        for (int bF=0; bF!=numBaseFuncs; ++bF) {
          int globalIndex = vecSpace_.mapToGlobal(entity, bF);
          vecSpaceMapToGlobal_.push_back(globalIndex);
        }
        return;
      }

      void computeLocalIndices() const {
        const int baseFuncSetSize = baseFuncSet_.numBaseFunctions();
        indices_.clear();
        for (int bF=0; bF!=baseFuncSetSize; ++bF) {
          int globalIndex = vecSpaceMapToGlobal_[bF];
          std::map<int, int>::const_iterator mapIt = globalToLocalMap_.find(globalIndex);
          if (mapIt!=globalToLocalMap_.end())
            indices_.push_back(mapIt->second);
          else 
            indices_.push_back(-1);
        }
      }

    private:
      void evaluateFuncLocal(const VectorType& vec, const EntityPointerType& entityPtr,
                             const DomainType& xLocal, RangeType& ret) const {
        const int mapSize = vecSpaceMapToGlobal_.size();
        assert(mapSize!=0);
        for (int bF=0; bF!=mapSize; ++bF) {
          const int localIndex = indices_[bF];
          if (localIndex!=-1) {
            RangeType baseFuncVal(0.0);
            baseFuncSet_.evaluate(bF, xLocal, baseFuncVal);
            baseFuncVal *= vec[localIndex];
            ret += baseFuncVal;
          }
        }
        return;
      }


      const VectorSpaceType&     vecSpace_;
      const GridPartType&        gridPart_;
      const PhiListType&         phiList_;
      const IntersectionType&    intersection_;
      EntityPointerType          insidePtr_;
      EntityPointerType          outsidePtr_;
      const EntityType&          insideEn_;
      const EntityType&          outsideEn_;
      const ProblemType&         problem_;
      const unsigned int         Nlambda_;
      int                        localIndex_;
      MatrixType                 matrix_;
      std::map<int, int>         globalToLocalMap_;
      std::map<int, int>         localToGlobalMap_;
      std::vector<VectorType>    deltaVecs_;
      std::vector<VectorType>    psiVecs_;
      bool                       doInside_;
      bool                       verbose_;
      static std::vector<int>    vecSpaceMapToGlobal_;
      static BaseFunctionSetType baseFuncSet_;
      mutable std::vector<int>   indices_;
    };

    template<class ErrorTraits>
    std::vector<int> InnerLifting<ErrorTraits>::vecSpaceMapToGlobal_;
    template<class ErrorTraits>
    typename ErrorTraits::VectorSpaceType::BaseFunctionSetType InnerLifting<ErrorTraits>::baseFuncSet_;

    template<class ErrorTraits>
    class BoundaryLifting
    {
      typedef BoundaryLifting<ErrorTraits>                                ThisType;
      typedef typename ErrorTraits::VectorFunctionType                    VectorFunctionType;
      typedef typename VectorFunctionType::LocalFunctionType              VectorLocalFunctionType;
      typedef typename ErrorTraits::VectorSpaceType                       VectorSpaceType;
      typedef typename VectorSpaceType::BaseFunctionSetType               BaseFunctionSetType;
      typedef DiscreteFunctionList_mem<VectorFunctionType>                RightHandSideListType;
      typedef typename VectorSpaceType::GridPartType                      GridPartType;
      typedef typename GridPartType::GridType                             GridType;
      typedef typename ErrorTraits::EntityPointerType                     EntityPointerType;
      typedef typename ErrorTraits::EntityType                            EntityType;
      typedef typename ErrorTraits::IntersectionType                      IntersectionType;
      typedef typename ErrorTraits::IntersectionIteratorType              IntersectionIteratorType;
      typedef typename ErrorTraits::PhiListType                           PhiListType;
      // ... and also for codimension 1 entities
      typedef CachingQuadrature<GridPartType, 1>                          FaceQuadratureType;
      typedef typename ErrorTraits::ElementQuadratureType                 ElementQuadratureType;
      typedef typename VectorSpaceType::RangeType                         RangeType;
      typedef typename VectorSpaceType::DomainType                        DomainType;
      typedef typename ErrorTraits::ProblemType                           ProblemType;
      typedef typename ProblemType::RangeType                             GDRangeType;
      //! polynomial order of base functions
      enum { polynomialOrder = VectorSpaceType::polynomialOrder };

      //! \todo This could for shure be done better 
      //(dynamic matrix or use polyniomal degree define and dim define (assert triangles))
      typedef FieldMatrix<double, 12, 12> MatrixType;
      typedef FieldVector<double, 12>     VectorType;


    public:
      BoundaryLifting(const VectorSpaceType& vecSpace,
                      const PhiListType& phiList,
                      const IntersectionType& intersection,
                      const ProblemType& problem)
        : vecSpace_(vecSpace),
          gridPart_(vecSpace_.gridPart()),
          phiList_(phiList),
          intersection_(intersection),
          insidePtr_(intersection.inside()),
          insideEn_(*insidePtr_),
          coarseCell_(gridPart_.getSubDomain(insideEn_)),
          fineCell_(gridPart_.indexSet().index(insideEn_)),
          problem_(problem),
          localIndex_(0),
          matrix_(0.0)
      {
        assert(intersection.boundary());
        assembleMatrix(insideEn_);
        matrix_.invert();
        // assemble the right hand side
        assembleRightHandSide();
      };

      ~BoundaryLifting() {}

    private:
      /** map a local dof number to an index, such that two dofs belonging
          to different cells but with the same global dofs will get the same
          index*/
      const unsigned int mapLocalToLocalIndex(const EntityType& entity, 
                                              const unsigned int i) {
        unsigned int ret;
        const unsigned int iGlobal = vecSpace_.mapToGlobal(entity, i);
        std::map<int,int>::const_iterator found = globalToLocalMap_.find(iGlobal);
        if (found==globalToLocalMap_.end()) {
          globalToLocalMap_[iGlobal]=localIndex_;
          localToGlobalMap_[localIndex_]=iGlobal;
          ret = localIndex_;
          ++localIndex_;
        } else
          ret = found->second;
        return ret;
      }

      void assembleMatrix(const EntityType& entity) {
        const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(entity);
        const int numBaseFuncs = baseFuncSet.numBaseFunctions();
        if (numBaseFuncs!=12)
          DUNE_THROW(NotImplemented, "For the moment we assume to have polorder 2 in 2D on triangles!");
        const typename EntityType::Geometry& geometry = entity.geometry();
        for (int i=0; i!=numBaseFuncs; ++i) {
          for (int j=0; j<=i; ++j) {
            // get the global indices, these give column and row in stiffness matrix
            const unsigned int row = mapLocalToLocalIndex(entity, i);
            const unsigned int col = mapLocalToLocalIndex(entity, j);
            
            ElementQuadratureType quadrature(entity, (2*(polynomialOrder+1)));
            const unsigned int numQuadPoints = quadrature.nop();
            for (unsigned int qP=0; qP!=numQuadPoints; ++qP) {
              const double factor = 
                quadrature.weight(qP)*geometry.integrationElement(quadrature.point(qP));
              RangeType baseFuncVal1(0.0);
              RangeType baseFuncVal2(0.0);
              baseFuncSet.evaluate(i, quadrature.point(qP), baseFuncVal1);
              baseFuncSet.evaluate(j, quadrature.point(qP), baseFuncVal2);
              double value = baseFuncVal1*baseFuncVal2;
              value *= factor;
              matrix_[row][col] += value;
              if (i!=j)
                matrix_[col][row] += value;
            } 
          }
        }
      }

    
      void assembleRightHandSide() {
        VectorType rhsGD(0.0);

        const BaseFunctionSetType& baseFuncSet = vecSpace_.baseFunctionSet(insideEn_);
        const int numBaseFuncs = baseFuncSet.numBaseFunctions();

        FaceQuadratureType faceQuad(vecSpace_.gridPart(), intersection_, 
                                    2*(polynomialOrder+1), FaceQuadratureType::INSIDE);
        const typename IntersectionType::Geometry &inGeo      = intersection_.geometry();
        const typename IntersectionType::Geometry &insideGeo  = intersection_.geometryInInside();
        const unsigned int numQuadPoints = faceQuad.nop();

        std::vector<VectorType> gDRHS(problem_.Ngd(), VectorType(0.0));

        typename PhiListType::DiscreteFunctionType phi("phiAssembleRHS", phiList_.space());
        const unsigned int listSize = phiList_.size();
        for (unsigned int l=0; l!=listSize; ++l) {
          VectorType deltaRHS(0.0);
          phi.clear();
          phiList_.getFunc(l, phi);
          typename PhiListType::DiscreteFunctionType::LocalFunctionType
            phiLocal = phi.localFunction(insideEn_);

          for (int i=0; i!=numBaseFuncs; ++i) {
            const unsigned int localIndexMapped = mapLocalToLocalIndex(insideEn_, i);
            // loop over all quadrature points
            for (unsigned int iqP = 0; iqP!=numQuadPoints; ++iqP) {
              typename FaceQuadratureType::LocalCoordinateType xLocal    = faceQuad.localPoint(iqP);
              typename FaceQuadratureType::CoordinateType      xInInside = insideGeo.global(xLocal);
              typename FaceQuadratureType::CoordinateType      xGlobal   = inGeo.global(xLocal);
            
              const double factor = faceQuad.weight(iqP)*inGeo.integrationElement(xLocal);

              typename BaseFunctionSetType::RangeType insideBaseFuncValue(0.0);
              baseFuncSet.evaluate(i, xInInside, insideBaseFuncValue);
              // Compute mean value of base functions of this entity on intersection
              double meanValue = 0.0;
              meanValue = (insideBaseFuncValue*intersection_.unitOuterNormal(xLocal));

              if (l==0) {
                for (int gdComp=0; gdComp!=int(problem_.Ngd()); ++gdComp) {
                  // evaluate the matching parameter independent part of the dirichlet function
                  GDRangeType gDValue(0.0);
                  problem_.gDPart(gdComp, xGlobal, gDValue);
                  gDRHS[gdComp][localIndexMapped] -= meanValue*factor*gDValue;
                }
              }

              // evaluate the reconstructed base functions
              typename PhiListType::DiscreteFunctionType::RangeType phiVal(0.0);
              phiLocal.evaluate(xInInside, phiVal);
              deltaRHS[localIndexMapped] -= meanValue*factor*phiVal[0];
            }
          }
          // recons solution
          VectorType foo(0.0);
          // matrix is already inverted
          matrix_.mv(deltaRHS, foo);
          deltaVecs_.push_back(foo);
          //          reconstructSolution(foo, deltaSolutionList_.getFunc(l));
        }
        for (int gdComp=0; gdComp!=int(problem_.Ngd()); ++gdComp) {
          VectorType foo(0.0);
          // matrix is already inverted
          matrix_.mv(gDRHS[gdComp], foo);
          gdVecs_.push_back(foo);
        }
      } // assemble rhs


      void reconstructSolution(const VectorType& vec, VectorFunctionType& func) {
        const unsigned int vecSize = vec.N();
        assert(localToGlobalMap_.size()==vecSize);
        for (unsigned int i=0; i!=vecSize; ++i) {
          func.leakPointer()[localToGlobalMap_[i]] = vec[i];
        }
      }

    public:
      const int getCoarseCell() const {
        return coarseCell_;
      }

      const int getFineCell() const {
        return fineCell_;
      }

      void evaluateGDLocal(const unsigned int i, const EntityPointerType& entityPtr,
                           const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = gdVecs_[i];
        evaluateFuncLocal(vec, entityPtr, xLocal, ret);
        return;
      }


      void evaluateDeltaLocal(const unsigned int i, const EntityPointerType& entityPtr,
                              const DomainType& xLocal, RangeType& ret) const {
        ret=0.0;
        const VectorType& vec = deltaVecs_[i];
        evaluateFuncLocal(vec, entityPtr, xLocal, ret);
        return;
      }

      const unsigned int getNumEquations() {
        return phiList_.size();
      }

      void replaceMap(const EntityPointerType& entityPtr) const {
        vecSpaceMapToGlobal_.clear();
        const EntityType& entity = *entityPtr;
        baseFuncSet_ = vecSpace_.baseFunctionSet(entity);
        int numBaseFuncs = baseFuncSet_.numBaseFunctions();
        for (int bF=0; bF!=numBaseFuncs; ++bF) {
          int globalIndex = vecSpace_.mapToGlobal(entity, bF);
          vecSpaceMapToGlobal_.push_back(globalIndex);
        }
        return;
      }


      void computeLocalIndices() const {
        const int baseFuncSetSize = baseFuncSet_.numBaseFunctions();
        indices_.clear();
        for (int bF=0; bF!=baseFuncSetSize; ++bF) {
          int globalIndex = vecSpaceMapToGlobal_[bF];
          std::map<int, int>::const_iterator mapIt = globalToLocalMap_.find(globalIndex);
          if (mapIt!=globalToLocalMap_.end())
            indices_.push_back(mapIt->second);
          else 
            indices_.push_back(-1);
        }
      }

    private:
      void evaluateFuncLocal(const VectorType& vec, const EntityPointerType& entityPtr,
                             const DomainType& xLocal, RangeType& ret) const {
        const int mapSize = vecSpaceMapToGlobal_.size();
        assert(mapSize!=0);
        for (int bF=0; bF!=mapSize; ++bF) {
          const int localIndex = indices_[bF];
          if (localIndex!=-1) {
            RangeType baseFuncVal(0.0);
            baseFuncSet_.evaluate(bF, xLocal, baseFuncVal);
            baseFuncVal *= vec[localIndex];
            ret += baseFuncVal;
          }
        }
        return;
      }  

    private:
      const VectorSpaceType&         vecSpace_;
      const GridPartType&            gridPart_;
      const PhiListType&             phiList_;
      const IntersectionType&        intersection_;
      EntityPointerType              insidePtr_;
      const EntityType&              insideEn_;
      const int                      coarseCell_;
      const int                      fineCell_;
      const ProblemType&             problem_;
      int                            localIndex_;
      MatrixType                     matrix_;
      std::map<int, int>             globalToLocalMap_;
      std::map<int, int>             localToGlobalMap_;
      std::vector<VectorType>        deltaVecs_;
      std::vector<VectorType>        gdVecs_;
      static std::vector<int>        vecSpaceMapToGlobal_;
      static BaseFunctionSetType     baseFuncSet_;
      mutable std::vector<int>       indices_; 
    };

    template<class ErrorTraits>
    std::vector<int> BoundaryLifting<ErrorTraits>::vecSpaceMapToGlobal_;
    template<class ErrorTraits>
    typename ErrorTraits::VectorSpaceType::BaseFunctionSetType BoundaryLifting<ErrorTraits>::baseFuncSet_;
  }
}

#endif /* _LIFTING_H_ */
