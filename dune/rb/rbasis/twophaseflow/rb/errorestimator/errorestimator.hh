#ifndef __ERRORESTIMATOR_HH__
#define __ERRORESTIMATOR_HH__

// stl stuff
#include <iostream>
#include <fstream>
#include <time.h>

// istl stuff
#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>


// fem stuff
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/2order/lagrangematrixsetup.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/solver/inverseoperators.hh>
#include <dune/grid/common/scsgmapper.hh>
#include <dune/fem/io/matlab/matlabhelper.hh>
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/space/fvspace.hh>


// dune-rb
#include <dune/rb/misc/discfunclist/discfunclist_mem.hh>
#include <dune/rb/misc/discfunclist/discfunclist_xdr.hh>
#include <dune/rb/misc/discfunclist/constraineddiscfunclist.hh>
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/interpolatediscretefunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/reconstruction.hh>

// dune-stuff stuff
#include <dune/stuff/printing.hh>



// local includes
#include "errorestimatortraits.hh"
#include "intersectionindexmapper.hh"
#include "lifting.hh"
#include "newlifting.hh"
#include "equationstorage.hh"
#include "onlinematrixstorage.hh"

namespace Dune {
  namespace RB {
    template<class ReducedFunctionImpl, class ProblemImpl>
    class ErrorEstimator
    {
      typedef ReducedFunctionImpl                                      ReducedFunctionType;
      typedef ProblemImpl                                              ProblemType;
      typedef typename ReducedFunctionType::DiscreteFunctionSpaceType  ReducedBasisSpaceType;
      typedef ErrorEstimatorTraits<ReducedBasisSpaceType, ProblemType> ErrorTraits;
      typedef typename ErrorTraits::PhiListType                        PhiListType;
      typedef typename ErrorTraits::IntersectionIteratorType           IntersectionIterator;
      typedef typename ErrorTraits::EntityIteratorType                 EntityIteratorType;
      typedef typename ErrorTraits::IntersectionType                   IntersectionType;
      typedef typename ErrorTraits::IntersectionIteratorType           IntersectionIteratorType;
      typedef typename ErrorTraits::EntityPointerType                  EntityPointerType;
      typedef typename ErrorTraits::EntityType                         EntityType;
      typedef typename ErrorTraits::VectorSpaceType                    VectorSpaceType;
      typedef EquationStorage<ErrorTraits>                             EquationStorageType;
      typedef typename ErrorTraits::GridPartType                       GridPartType;
      typedef typename GridPartType::GridType                          GridType;
      typedef OnlineMatrices<ErrorTraits, EquationStorageType>         OnlineMatrixStorageType;
      typedef typename ReducedBasisSpaceType::ConstrainedList          RBBaseFunctionListType;
      typedef typename ErrorTraits::ReconstructedSpaceType             ReconstructedSpaceType;
      typedef typename ErrorTraits::ReconstructedFunctionType          ReconstructedFunctionType;
      typedef typename ErrorTraits::VectorType                         VectorType;
      typedef typename ErrorTraits::MatrixType                         MatrixType;
      typedef IntersectionIndexMapper<ErrorTraits>                     IntersectionIndexMapperType;
      enum { polynomialOrder = ErrorTraits::VectorSpaceType::polynomialOrder };

      typedef Reconstruction<typename ReducedBasisSpaceType::BaseFunctionType, ReconstructedFunctionType>
      ReconstructionOperatorType;

    public:
      ErrorEstimator(ReducedBasisSpaceType& rbSpace,
                     const ProblemType& problem)
        : rbSpace_(rbSpace), 
          gridPart_(rbSpace_.gridPart()),
          reconstructedSpace_(gridPart_),
          vecSpace_(gridPart_),
          problem_(problem),
          equationStorage_(vecSpace_, problem_),
          onlineMatrixStorage_(equationStorage_, problem_, gridPart_, rbSpace_),
          Nlambda_(problem_.Nlambda()),
          Ngd_(problem_.Ngd()),
          intersectionIndexMapper_(gridPart_),
          zeroTolerance_(Parameter::getValue<double>("errorestimator.zeroTolerance", 1.0e-15)),
          verbose_(Parameter::getValue<bool>("errorestimator.verbose", false))
      {
        PhiListType foo(reconstructedSpace_, "reconsBaseFuncs");
        for (unsigned int i=0; i!=gridPart_.numSubDomains(); ++i) {
          reconstructedBaseFunctions_.push_back(foo);
        }
        foo.clear();
        init();
      };

      ~ErrorEstimator() {
        // free hard disk space
        int size = reconstructedBaseFunctions_.size();
        for (int i=0; i!=size; ++i) 
          reconstructedBaseFunctions_[i].clear();
      }

      void assembleOnlineMatrices();

      //! \todo Constants C_1-C_4!!!
      double evaluate(const ReducedFunctionType& func, const bool printAll=false);

    private: 
      void init();

      class MyPredicate;

      void checkForNegativeError(double& givenError, const std::string& partName,
                                 const  VectorType& dofsOne, const VectorType& dofsTwo);

    private:
      ReducedBasisSpaceType&      rbSpace_;
      GridPartType&               gridPart_;
      ReconstructedSpaceType      reconstructedSpace_;
      std::vector<PhiListType>    reconstructedBaseFunctions_;
      VectorSpaceType             vecSpace_;
      const ProblemType&          problem_;
      EquationStorageType         equationStorage_;
      OnlineMatrixStorageType     onlineMatrixStorage_;
      const int                   Nlambda_;
      const int                   Ngd_;
      IntersectionIndexMapperType intersectionIndexMapper_;
      double                      zeroTolerance_;
      const bool                  verbose_;


      std::vector<std::pair<int,int> > boundaryCells_;
      std::vector<std::vector<int> >   fineCellCombs_;
    };

  } // namespace RB 
} // namespace Dune 

#include "errorestimator.cc"

#if 0
void evaluateLiftingOfJump(const ReducedFunctionType& func, 
                           const IntersectionType& intersection,
                           double &Mval,
                           double &Nval) const {
  VectorSpaceType vecSpace(gridPart_);
  NewInnerLifting<ErrorTraits, ReducedFunctionType> lifting(vecSpace, func, intersection, problem_);
  double Nvaltmp=0.0;
  double Mvaltmp=0.0;
  const EntityIteratorType end = gridPart_.template end<0>();
  for (EntityIteratorType it = gridPart_.template begin<0>(); it!=end; ++it) {
    const EntityType& entity = *it;
    lifting.replaceMap(entity);
    typename ErrorTraits::ElementQuadratureType quadrature(entity, 2*(polynomialOrder+1));
    const int nop = quadrature.nop();
    for (int qp=0; qp!=nop; ++qp) {
      typename VectorSpaceType::DomainType xLocal = quadrature.point(qp);
      const typename ErrorTraits::GeometryType& geometry = entity.geometry();
      const double factor = 
        quadrature.weight(qp)*geometry.integrationElement(xLocal);
      typename VectorSpaceType::RangeType psiVal(0.0);
      typename VectorSpaceType::RangeType deltaVal(0.0);
      lifting.evaluatePsiLocal(xLocal, psiVal);
      lifting.evaluateDeltaLocal(xLocal, deltaVal);
      Nvaltmp += factor*(psiVal*psiVal);
      Mvaltmp += factor*(deltaVal*deltaVal);
    }
  } // grid run
  assert(Nvaltmp>0.0);
  Mval = sqrt(Mvaltmp);
  Nval = sqrt(Nvaltmp);
}

void computeJumpOfJacobian(const ReducedFunctionType& func,
                           double& phiJump,
                           double& gradientJump) const {
  phiJump=0.0;
  gradientJump=0.0;
  const EntityIteratorType end = gridPart_.template end<0>();
  for (EntityIteratorType it = gridPart_.template begin<0>(); it!=end; ++it) {
    const IntersectionIteratorType intEnd = gridPart_.iend(*it);
    for (IntersectionIteratorType intIt = gridPart_.ibegin(*it); intIt!=intEnd; 
         ++intIt) {
      const IntersectionType& intersection = *intIt;
      if (gridPart_.onCoarseCellIntersection(intersection)) {
        EntityPointerType insidePtr = intersection.inside();
        EntityPointerType outsidePtr = intersection.outside();
        const EntityType& insideEn = *insidePtr;
        const EntityType& outsideEn = *outsidePtr;
        unsigned int coarseCellInside = gridPart_.getSubDomain(insideEn);
        unsigned int coarseCellOutside = gridPart_.getSubDomain(outsideEn);
        if (coarseCellInside>coarseCellOutside) {
          double phiElementJump = 0.0;
          double dphiElementJump = 0.0;
          evaluateLiftingOfJump(func, intersection, phiElementJump, dphiElementJump);
          phiJump+=phiElementJump;
          gradientJump+=dphiElementJump;
        }
      }
    }
  }
  return;
}
#endif

#endif /* __ERRORESTIMATOR_HH__ */

