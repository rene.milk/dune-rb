#ifndef _ONLINEMATRIXSTORAGE_H_
#define _ONLINEMATRIXSTORAGE_H_

namespace Dune {
  namespace RB {
    template<class ErrorTraits, class EquationStorageImpl>
    class OnlineMatrices
    {
      typedef EquationStorageImpl                                  EquationStorageType;
      typedef typename ErrorTraits::ProblemType                    ProblemType;
      typedef typename ErrorTraits::GridPartType                   GridPartType;
      typedef typename GridPartType::GridType                      GridType;
      typedef typename ErrorTraits::EntityType                     EntityType;
      typedef typename ErrorTraits::IntersectionType               IntersectionType;
      typedef typename ErrorTraits::EntityIteratorType             EntityIteratorType;
      typedef typename ErrorTraits::ReducedBasisSpace              ReducedBasisSpace;
      typedef typename ErrorTraits::PhiListType                    PhiListType;
      typedef typename ErrorTraits::ReconstructedFunctionType      ReconstructedFunctionType;
      typedef typename ErrorTraits::MatrixType                     MatrixType;
      typedef typename ErrorTraits::VectorType                     VectorType;
      typedef std::vector<MatrixType>                              MatrixStorage;
      typedef std::vector<VectorType>                              VectorStorage;
      typedef std::map<int, std::map<int, std::map<int, int> > >   Mapper;
      typedef Mapper::const_iterator                               ConstMapperIterator;
      typedef Mapper::iterator                                     MapperIterator;
      typedef typename ErrorTraits::ElementQuadratureType          ElementQuadratureType;
      typedef typename ErrorTraits::GeometryType                   GeometryType;
      typedef typename EquationStorageType::InnerLiftingType       InnerLiftingType;
      typedef typename ErrorTraits::VectorFunctionType             VectorFunctionType;
      typedef typename VectorFunctionType::RangeType               VectorRangeType;
      typedef typename EquationStorageType::BoundaryLiftingType    BoundaryLiftingType;
      typedef typename ReconstructedFunctionType::DomainType       DomainType;
      typedef typename ReconstructedFunctionType::RangeType        RangeType;
      enum { polynomialOrder = ErrorTraits::VectorSpaceType::polynomialOrder };


    public:
      OnlineMatrices(const EquationStorageType& equationStorage,
                     const ProblemType& problem,
                     GridPartType& gridPart,
                     const ReducedBasisSpace& rbSpace)
        : equationStorage_(equationStorage),
          problem_(problem),
          gridPart_(gridPart),
          rbSpace_(rbSpace),
          numSubDomains_(gridPart_.numSubDomains()),
          storageM_(),
          storageN_(),
          storageK_(),
          storagek_(),
          storagem_(),
          fNorm_(numSubDomains_, 0.0),
          liftedGDNorms_(),
          mapper_(),
          Nlambda_(problem_.Nlambda()),
          Ngd_(problem_.Ngd())
      {
        for (unsigned int subDomain=0; subDomain!=numSubDomains_; ++subDomain) {
          const unsigned int kSize = rbSpace_.getFuncList(subDomain).size();
          for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
            storagek_.push_back(VectorType(kSize));
            storagek_.back()=0;
            for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
              storageK_.push_back(MatrixType(kSize, kSize));
              storageK_.back()=0;
            }
          }
        }
      };

    private:
      const bool mapperContains(const unsigned int intersectionIndex,
                                const unsigned int fineCellOne,
                                const unsigned int fineCellTwo) const {
        return (findInMapper(intersectionIndex, fineCellOne, fineCellTwo)!=-1);
      }

      const int findInMapper(const unsigned int intersectionIndex,
                             const unsigned int fineCellOne,
                             const unsigned int fineCellTwo) const {
        ConstMapperIterator findOne = mapper_.find(fineCellOne);
        ConstMapperIterator findTwo = mapper_.find(fineCellTwo);
        if (findOne!=mapper_.end()) {
          std::map<int, std::map<int, int> >::const_iterator findFoo = findOne->second.find(fineCellTwo);
          std::map<int, std::map<int, int> >::const_iterator secEnd = findOne->second.end();
          if (findFoo!=secEnd) {
            std::map<int, int>::const_iterator findBar = findFoo->second.find(intersectionIndex);
            if (findBar!=findFoo->second.end())
              return findBar->second;
          }
        } 
        if (findTwo!=mapper_.end()) {
          std::map<int, std::map<int, int> >::const_iterator findFoo = findTwo->second.find(fineCellOne);
          std::map<int, std::map<int, int> >::const_iterator secEnd = findTwo->second.end();
          if (findFoo!=secEnd) {
            std::map<int, int>::const_iterator findBar = findFoo->second.find(intersectionIndex);
            if (findBar!=findFoo->second.end())
              return findBar->second;
          }
        }
        return -1;
      }

    public:
      void add(const unsigned int intersectionIndex,
               const unsigned int fineCellOne,
               const unsigned int sizeOne,
               const unsigned int coarseCellOne,
               const unsigned int fineCellTwo,
               const unsigned int sizeTwo,
               const unsigned int coarseCellTwo) {
        assert(coarseCellOne!=coarseCellTwo);
        bool exists = mapperContains(intersectionIndex, fineCellOne, fineCellTwo);
        if (exists==false) {
          // add matrix @f$\boldsymbol{M}^{E_1,E_2}@f$
          mapper_[fineCellOne][fineCellTwo][intersectionIndex]=storageM_.size();
          storageM_.push_back(MatrixType(sizeOne, sizeTwo));
          storageM_.back()=0.0;
          for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
            for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
              // add matrix @f$\boldsymbol{N}^{E_1,E_2,\sigma,\nu}@f$
              storageN_.push_back(MatrixType(sizeOne, sizeTwo));
              storageN_.back()=0.0;
            }
          }
        }
        if (mapperContains(intersectionIndex, fineCellOne, fineCellOne)==false) {
          // add matrix @f$\boldsymbol{M}^{E_1,E_1}@f$
          mapper_[fineCellOne][fineCellOne][intersectionIndex]=storageM_.size();
          storageM_.push_back(MatrixType(sizeOne, sizeOne));
          storageM_.back()=0.0;
          for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
            for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
              // add matrix @f$\boldsymbol{N}^{E_1,E_1,\sigma,\nu}@f$
              storageN_.push_back(MatrixType(sizeOne, sizeOne));
              storageN_.back()=0.0;
            }
          }
        }
        if (mapperContains(intersectionIndex, fineCellTwo, fineCellTwo)==false) {
          // add matrix @f$\boldsymbol{M}^{E_2,E_2}@f$
          mapper_[fineCellTwo][fineCellTwo][intersectionIndex]=storageM_.size();
          storageM_.push_back(MatrixType(sizeTwo, sizeTwo));
          storageM_.back()=0.0;
          for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
            for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
              // add matrix @f$\boldsymbol{N}^{E_2,E_2,\sigma,\nu}@f$
              storageN_.push_back(MatrixType(sizeTwo, sizeTwo));
              storageN_.back()=0.0;
            }
          }
        }
        cellCombinations_.push_back(coarseCellOne);
        cellCombinations_.push_back(coarseCellTwo);
        cellCombinations_.push_back(fineCellOne);
        cellCombinations_.push_back(fineCellTwo);
        cellCombinations_.push_back(intersectionIndex);

        cellCombinations_.push_back(coarseCellOne);
        cellCombinations_.push_back(coarseCellOne);
        cellCombinations_.push_back(fineCellOne);
        cellCombinations_.push_back(fineCellOne);
        cellCombinations_.push_back(intersectionIndex);

        cellCombinations_.push_back(coarseCellTwo);
        cellCombinations_.push_back(coarseCellTwo);
        cellCombinations_.push_back(fineCellTwo);
        cellCombinations_.push_back(fineCellTwo);
        cellCombinations_.push_back(intersectionIndex);
        return;
      }

      void printNumMatrices() {
        std::cout << "We have " << storageK_.size() << " K-matrices, "
                  << storageM_.size() << " M-matrices and " << storageN_.size()
                  << " N-matrices.\n";
        return;
      }

      MatrixType& getM(const unsigned int intersectionIndex,
                       const unsigned int fineCellOne,
                       const unsigned int fineCellTwo) {
        int index = findInMapper(intersectionIndex,
                                 fineCellOne, fineCellTwo);
        //        std::cout << " " << index << " ";
        assert(index!=-1);
        return storageM_[index];
      }

      MatrixType& getN(const unsigned int intersectionIndex,
                       const unsigned int fineCellOne,
                       const unsigned int sigma,
                       const unsigned int fineCellTwo,
                       const unsigned int nu) {
        int index = findInMapper(intersectionIndex,
                                 fineCellOne, fineCellTwo);
        assert(index!=-1);
        
        return storageN_[(Nlambda_*Nlambda_*index)+(sigma*Nlambda_)+nu];
      }

      MatrixType& getK(const unsigned int sigma,
                       const unsigned int nu,
                       const unsigned int coarseCell) {
        return storageK_[coarseCell*Nlambda_*Nlambda_+sigma*Nlambda_+nu];
      }

      VectorType& getk(const unsigned int sigma,
                       const unsigned int coarseCell) {
        return storagek_[coarseCell*Nlambda_+sigma];
      }

      double getfNorm(const unsigned int coarseCell) const {
        return fNorm_[coarseCell];
      };

      const double getGDNorm(const unsigned int i, const unsigned int j,
                       const int intersectionIndex) const {
        std::map<int, std::map<int, double> >::const_iterator found1 = liftedGDNorms_.find(intersectionIndex);
        assert(found1!=liftedGDNorms_.end());
        std::map<int, double>::const_iterator found2;
        if (i>=j) 
          found2 = found1->second.find(i*Ngd_+j);
        else 
          found2 = found1->second.find(j*Ngd_+i);
        return found2->second;
      }

      VectorType& getm(const unsigned int i, const int intersectionIndex) {
        assert(storagem_.find(intersectionIndex)!=storagem_.end());
        return storagem_[intersectionIndex][i];
      }

      MatrixType& getQ(const int intersectionIndex) {
        assert(storageQ_.find(intersectionIndex)!=storageQ_.end());
        return storageQ_[intersectionIndex];

      }

      void print() {
        // const int KSize = storageK_.size();
        // for (int i=0; i!=KSize; ++i) {
        //   const MatrixType& mat = storageK_[i];
        //   const int nu = i%Nlambda_;
        //   const int sigma = (i-nu)/Nlambda_;
        //   std::stringstream name;
        //   name << "K^{" << sigma << "," << nu << "}";
        //   Stuff::printFieldMatrix(mat, name.str(), std::cout);
        // }
        std::cout << std::endl;
        const int MSize = storageM_.size();
        for (int i=0; i!=MSize; ++i) {
          std::stringstream name;
          name << "M_" << i;
          Stuff::printFieldMatrix(storageM_[i], name.str(), std::cout);
        }
        std::cout << std::endl;
        const int NSize = storageN_.size();
        for (int i=0; i!=NSize; ++i) {
          std::stringstream name;
          name << "N_";
          Stuff::printFieldMatrix(storageN_[i], name.str(), std::cout);
        }        
        // std::cout << std::endl;
        // const int kSize = storagek_.size();
        // for (int i=0; i!=kSize; ++i) {
        //   std::stringstream name;
        //   name << "k^" << i;
        //   Stuff::printFieldVector(storagek_[i], name.str(), std::cout);
        // }
        // std::cout << std::endl;
        // typedef typename std::map<int, VectorType>::const_iterator ItType;
        // ItType mEnd = storagem_.end();
        // for (ItType mIt = storagem_.begin(); mIt!=mEnd; ++mIt) {
        //   Stuff::printFieldVector(mIt->second, "m", std::cout);
        // }
        // std::cout << std::endl;

        // typedef std::map<int, double>::const_iterator ItType2;
        // ItType2 gdEnd = liftedGDNorms_.end();
        // std::cout << "Norms of Lifted g_Ds: ";
        // for (ItType2 gdIt = liftedGDNorms_.begin(); gdIt!=gdEnd; ++gdIt) {
        //   std::cout << gdIt->second << " ";
        // }
        // std::cout << "\n\nSquare L2-Norms of f: ";
        // for (int i=0; i!=numSubDomains_; ++i) 
        //   std::cout << fNorm_[i] << " ";
        // std::cout << std::endl;

        // int i=0;
        // for (typename std::map<int,MatrixType>::const_iterator it = storageQ_.begin();
        //      it!=storageQ_.end(); ++it,++i) {
        //   std::stringstream name;
        //   name << "Q_" << i;
        //   Stuff::printFieldMatrix(it->second, name.str(), std::cout);
        // }        
        // std::cout << std::endl;
        // return;        
      }

      void assemble(const std::vector<PhiListType>& reconstructedBaseFuncs) {

        /*
          Run over grid {
          get entity
          get quadrature for entity
          loop over all quad points {
          get weight, quad point, integration element etc
          assemble K on entity
          // Assemble all matrices M^{i,j} and N^{i,j}:
          loop over cellCombinations_ {
          get inside and outside fine and coarse cell
          get insideEquation(insideFine, outsideFine, insideCoarse) 
          get outsideEquation(outsideFine, insideFine, outsideCoarse)
          loop over all insideInnerLiftings i
          loop over all outsideInnerLiftings j {
          add contribution of this entity to getM(insideFine, outsideFine)
          loop over sigma
          loop over nu
          add contribution to getN(insideFine, sigma, outsideFine, nu)
          }
          } // loop over all computations for M
          } // loop over quad points
          } // loop over grid
        */


        assert(reconstructedBaseFuncs.size()!=0);
        assert(reconstructedBaseFuncs.size()==gridPart_.numSubDomains());
  
        Stuff::SimpleProgressBar<> assemblationBar(gridPart_.grid().size(0));
        std::vector<double> allPsiVals;
        /* 
           ====================== Grid walk =========================
         */
        EntityIteratorType end = gridPart_.template end<0>();
        for (EntityIteratorType it = gridPart_.template begin<0>(); 
             it!=end; ++it, ++assemblationBar) {
          const EntityType& entity = *it;
          const GeometryType& geometry = entity.geometry();

          //profiler().StartTiming("k");
          const unsigned int coarseCell = gridPart_.getSubDomain(entity);
          const PhiListType& list = reconstructedBaseFuncs[coarseCell];
          const unsigned int numPhis = list.size();
          list.stashFunctions(0, numPhis);
          assert(numPhis!=0);

          const int quadOrderFork = 
            Parameter::getValue<int>("errorestimator.quadOrderFork1",(2*polynomialOrder+1));
          ElementQuadratureType quadratureFork(entity, quadOrderFork);
          const unsigned int numQuadPointsFork = quadratureFork.nop();

          for (unsigned int qP=0; qP!=numQuadPointsFork; ++qP) {
            DomainType xLocal = quadratureFork.point(qP);
            DomainType xGlobal = geometry.global(xLocal);
            const double factor = 
              quadratureFork.weight(qP)*geometry.integrationElement(xLocal);
            // update l2 norm of f for this entity
            typename ReconstructedFunctionType::RangeType fValue(0.0);
            problem_.f(xGlobal, fValue);
            // update l2 norm of f
            fNorm_[coarseCell]+=factor*(fValue*fValue);

            //============= assemble k on this entity ==========================
            //            FemTimer::start(assemblationOfK);
            /* get coarse cell (lets assume it is i), get the i'th PhiList from the
               vector created in (*)
               for each function m in PhiList {
               evaluate jacobian,
               sum up over diagonal to get divergence
               map m to global index using rbSpace_
               for each function n in PhiList {
               evaluate jacobian,
               sum up over diagonal to get divergence
               map n to global using rbSpace_
               update matrix entry (global(m),global(n))
               }
               }
            */

            typedef typename PhiListType::DiscreteFunctionType PhiType;
            typedef typename PhiType::LocalFunctionType        PhiLocalFunctionType;
            typedef typename PhiType::JacobianRangeType        JacobianRangeType;
            typedef typename PhiType::HessianRangeType         HessianRangeType;
            typedef typename ProblemType::DiffusionMatrixType  DiffusionMatrixType;
            typedef FieldVector<double, GridType::dimension> FieldVectorType;

            for (unsigned int m=0; m!=numPhis; ++m) {
              PhiLocalFunctionType phiLocal = list.getFunc(m).localFunction(entity);

              DiffusionMatrixType K;
              // evaluate diffusion matrix 
              problem_.K(xGlobal, K);
              DiffusionMatrixType dxK;
              // evaluate diffusion matrix derivative
              problem_.dxK(xGlobal, dxK);
              DiffusionMatrixType dyK;
              // evaluate diffusion matrix derivative
              problem_.dyK(xGlobal, dyK);

              // evaluate the jacobian of the solution phi
              JacobianRangeType phiJac(0.0);
              phiLocal.jacobian(xLocal, phiJac);

              HessianRangeType phiHess;
              phiLocal.hessian(xLocal, phiHess);
              // transpose phiHess
              FieldMatrix<double, GridType::dimension, GridType::dimension>
                phiHessTransposed(0.0);
              phiHessTransposed+=phiHess[0];
              double mem = phiHessTransposed[0][1];
              phiHessTransposed[0][1]=phiHessTransposed[1][0];
              phiHessTransposed[1][0]=mem;


              FieldVectorType KtimesPhiJac(0.0);
              K.mv(phiJac[0], KtimesPhiJac);

              FieldVectorType summand1(0.0);
              dxK.mv(phiJac[0], summand1);

              FieldVectorType summand2(0.0);
              dyK.mv(phiJac[0], summand2);

              DiffusionMatrixType summand3(K);
              summand3.rightmultiply(phiHessTransposed);

              for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
                double val(0.0);
                JacobianRangeType lambdaJac(0.0);
                problem_.lambdaPartJacobian(sigma, xGlobal, lambdaJac);
                RangeType lambdaVal(0.0);
                problem_.lambdaPart(sigma, xGlobal, lambdaVal);

                val += lambdaJac[0]*KtimesPhiJac;
                val += lambdaVal*(summand1[0]+summand2[1]+summand3[0][0]+summand3[1][1]);
                getk(sigma, coarseCell)[m]+= 2.0*factor*(val*fValue);
              }
            }
          } // loop over quadpoints for k
          //==================== k done ===============================
          //profiler().StopTiming("k");
          //profiler().StartTiming("K");

          //============= assemble K on this entity ==========================
          const int quadOrderForK = 
            Parameter::getValue<int>("errorestimator.quadOrderForK2",2*polynomialOrder+1);
          ElementQuadratureType quadratureForK(entity, quadOrderForK);
          const unsigned int numQuadPointsForK = quadratureForK.nop();
          for (unsigned int qP=0; qP!=numQuadPointsForK; ++qP) {
            std::vector<double> funcValues;
            DomainType xLocal = quadratureForK.point(qP);
            DomainType xGlobal = geometry.global(xLocal);
            const double factor = 
              quadratureForK.weight(qP)*geometry.integrationElement(xLocal);
            typedef typename PhiListType::DiscreteFunctionType PhiType;
            typedef typename PhiType::LocalFunctionType        PhiLocalFunctionType;
            typedef typename PhiType::JacobianRangeType        JacobianRangeType;
            typedef typename PhiType::HessianRangeType         HessianRangeType;
            typedef typename ProblemType::DiffusionMatrixType  DiffusionMatrixType;
            typedef FieldVector<double, GridType::dimension>   FieldVectorType;

            for (unsigned int m=0; m!=numPhis; ++m) {
              const PhiLocalFunctionType phiLocal = list.getFunc(m).localFunction(entity);

              DiffusionMatrixType K;
              // evaluate diffusion matrix 
              problem_.K(xGlobal, K);
              DiffusionMatrixType dxK;
              // evaluate diffusion matrix derivative
              problem_.dxK(xGlobal, dxK);
              DiffusionMatrixType dyK;
              // evaluate diffusion matrix derivative
              problem_.dyK(xGlobal, dyK);

              // evaluate the jacobian of the solution phi
              JacobianRangeType phiJac(0.0);
              phiLocal.jacobian(xLocal, phiJac);

              HessianRangeType phiHess;
              phiLocal.hessian(xLocal, phiHess);
              // transpose phiHess
              FieldMatrix<double, GridType::dimension, GridType::dimension> 
                phiHessTransposed(0.0);
              phiHessTransposed+=phiHess[0];
              double mem = phiHessTransposed[0][1];
              phiHessTransposed[0][1]=phiHessTransposed[1][0];
              phiHessTransposed[1][0]=mem;

              FieldVectorType KtimesPhiJac(0.0);
              K.mv(phiJac[0], KtimesPhiJac);

              FieldVectorType summand1(0.0);
              dxK.mv(phiJac[0], summand1);

              FieldVectorType summand2(0.0);
              dyK.mv(phiJac[0], summand2);

              DiffusionMatrixType summand3(K);
              summand3.rightmultiply(phiHessTransposed);

              for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
                double val(0.0);
                JacobianRangeType lambdaJac(0.0);
                problem_.lambdaPartJacobian(sigma, xGlobal, lambdaJac);
                RangeType lambdaVal(0.0);
                problem_.lambdaPart(sigma, xGlobal, lambdaVal);

                val += lambdaJac[0]*KtimesPhiJac;
                val += lambdaVal*(summand1[0]+summand2[1]+summand3[0][0]+summand3[1][1]);
                funcValues.push_back(val);
              }
            }

            const unsigned int numVals = funcValues.size();
            for (unsigned int m=0; m!=numVals; ++m) {

//              const unsigned int mGlobal = rbSpace_.mapToGlobal(entity, m/Nlambda_);
              const unsigned int sigma = m%Nlambda_;
              for (unsigned int n=0; n!=numVals; ++n) {
//                const unsigned int nGlobal = rbSpace_.mapToGlobal(entity, n/Nlambda_);
                const unsigned int nu = n%Nlambda_;
                getK(sigma, nu, coarseCell)[m/Nlambda_][n/Nlambda_]+=factor*funcValues[m]*funcValues[n];
              }
            }
          } // loop over quad points for K
          //=============================== K done ==================================
          list.dropStash();
          //profiler().StopTiming("K");
          //profiler().StartTiming("jump");
          //========================== Assemble M ==============================
          //profiler().StartTiming("Other");

          ElementQuadratureType quadratureForM(entity, 2*(polynomialOrder+1));
          const unsigned int numQuadPointsForM = quadratureForM.nop();
          //profiler().StopTiming("Other");

            const int cellCombinationsSize = cellCombinations_.size();
            //profiler().StopTiming("Other");

            for (int cellComb=0; cellComb<cellCombinationsSize; cellComb+=5) {
              //profiler().StartTiming("Evaluation for M,N");
              //profiler().StartTiming("Evaluation for M,N2");
              //profiler().StartTiming("get equation");
              const int coarseCellOne = cellCombinations_[cellComb];
              const int coarseCellTwo = cellCombinations_[cellComb+1];
              const int fineCellOneForComb = cellCombinations_[cellComb+2];
              const int fineCellTwoForComb = cellCombinations_[cellComb+3];
              const int intersectionIndex = cellCombinations_[cellComb+4];
              //              std::cout << "Eq storage indices: ";
              const InnerLiftingType& insideEquation = equationStorage_.get(intersectionIndex,
                                                                            coarseCellOne);
              const InnerLiftingType& outsideEquation = equationStorage_.get(intersectionIndex,
                                                                             coarseCellTwo);
              // replace map in lifting for this entity (remember: map is static)
              if (cellComb==0)
                insideEquation.replaceMap(it);

              insideEquation.computeLocalIndices();
              outsideEquation.computeLocalIndices();

              for (unsigned int qP=0; qP!=numQuadPointsForM; ++qP) {
                //profiler().StartTiming("Other");
                //FemTimer::start(assemblationOfM);
                DomainType xLocal = quadratureForM.point(qP);
                DomainType xGlobal = geometry.global(xLocal);
                const double factor = 
                  quadratureForM.weight(qP)*geometry.integrationElement(xLocal);
                
                //              std::cout << " done!\n";
                //profiler().StopTiming("get equation");
                //===== evaluate all functions from the inside equation in quad point =====
                //profiler().StartTiming("evaluate inside equation");
                const int numInsideEquations = insideEquation.getNumEquations();
                typedef std::vector<typename VectorFunctionType::RangeType> RangeTypeVector;
                RangeTypeVector insideDeltaValues;
                RangeTypeVector insidePsiValues;
                for (int i=0; i!=numInsideEquations; ++i) {
                  typename VectorFunctionType::RangeType deltaVal(0.0);

                  //profiler().StartTiming("evaluate delta equation");
                  insideEquation.evaluateDeltaLocal(i, it, xLocal, deltaVal);
                  //profiler().StopTiming("evaluate delta equation");

                  insideDeltaValues.push_back(deltaVal);
                  for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
                    VectorRangeType psiVal(0.0);
                    //profiler().StartTiming("evaluate psi equation");
                    insideEquation.evaluatePsiLocal(i, it, sigma, xLocal, psiVal);
                    //profiler().StopTiming("evaluate psi equation");
                    insidePsiValues.push_back(psiVal);
                    //allPsiVals.push_back(psiVal);
                  } // loop over sigma
                } // loop over inside equations
                //profiler().StopTiming("evaluate inside equation");
                //profiler().StartTiming("evaluate outside equation");

                //========================================================================== 
                //===== evaluate all functions from the outside equation in quad point =====
                RangeTypeVector* outsideDeltaValues;
                RangeTypeVector* outsidePsiValues;
                bool deleteOutsideValues = false;
                int numOutsideEquations;
                if (fineCellOneForComb!=fineCellTwoForComb) {
                  outsideDeltaValues = new RangeTypeVector();
                  outsidePsiValues = new RangeTypeVector();
                  deleteOutsideValues = true;
                  numOutsideEquations = outsideEquation.getNumEquations();
                  for (int j=0; j!=numOutsideEquations; ++j) {
                    VectorRangeType deltaVal(0.0);
                    outsideEquation.evaluateDeltaLocal(j, it, xLocal, deltaVal);

                    outsideDeltaValues->push_back(deltaVal);

                    for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
                      VectorRangeType psiVal(0.0);
                      outsideEquation.evaluatePsiLocal(j, it, nu, xLocal, psiVal);

                      outsidePsiValues->push_back(psiVal);
                    } // loop over nu
                  } // loop over outside equations
                  //==========================================================================
                } else {
                  outsideDeltaValues = &insideDeltaValues;
                  outsidePsiValues   = &insidePsiValues;
                  numOutsideEquations = insideEquation.getNumEquations();
                }
                // if (numOutsideEquations>1)
                //   std::cout << "numOutsideEquations: " << numOutsideEquations << std::endl;

                //profiler().StopTiming("evaluate outside equation");
                //profiler().StopTiming("Evaluation for M,N");
                //profiler().StopTiming("Evaluation for M,N2");
                //profiler().StartTiming("Matrix update for M,N");

                //======================== Update matrix entries ==================================
                for (int i=0; i!=numInsideEquations; ++i) {
                  for (int j=0; j!=numOutsideEquations; ++j) {
                    // update matrix entry for @f$\boldymbol{M}^{E_1,E_2}@f$
                    getM(intersectionIndex, fineCellOneForComb, fineCellTwoForComb)[i][j] += factor*(insideDeltaValues[i]*(*outsideDeltaValues)[j]);
                    for (unsigned int sigma=0; sigma!=Nlambda_; ++sigma) {
                      for (unsigned int nu=0; nu!=Nlambda_; ++nu) {
                        // update matrix entry for @f$\boldymbol{N}^{E_1,E_2,\sigma,\nu}@f$
                        VectorRangeType insideVal = insidePsiValues[i*Nlambda_+sigma];
                        VectorRangeType outsideVal = (*outsidePsiValues)[j*Nlambda_+nu];
                        double val = factor*(insideVal*outsideVal);
                        //                      std::cout << insideVal << " " << outsideVal << " ";
                        getN(intersectionIndex, fineCellOneForComb, sigma, fineCellTwoForComb, nu)[i][j] += val;
                        // if (i==1 && j==1) 
                        //   std::cout << "inside: " << insideVal << " outside: " << outsideVal;
                      }
                    }
                  }
                }
                //=================================================================================
                if (deleteOutsideValues) {
                  delete outsideDeltaValues;
                  outsideDeltaValues = 0;
                  delete outsidePsiValues;
                  outsidePsiValues = 0;
                }
                //profiler().StopTiming("Matrix update for M,N");
              } // loop over quad points for M
            } // loop over cell combinations

            //profiler().StartTiming("Boundary terms");
            
            // compute the l2 norm of @f$\boldsymbol{r}_e(g_D)@f$
            const std::map<int, BoundaryLiftingType*>& boundaryLiftings = 
              equationStorage_.getBoundaryLiftings();
            typedef typename std::map<int, BoundaryLiftingType*>::const_iterator ItType;
            ItType blEnd = boundaryLiftings.end();
            boundaryLiftings.begin()->second->replaceMap(it);
            for (ItType blIt = boundaryLiftings.begin(); blIt!=blEnd; ++blIt) {
              const unsigned int intersectionIndex = blIt->first;
              const unsigned int numBoundEq = blIt->second->getNumEquations();
              
              blIt->second->computeLocalIndices();
              
              //======== make sure all quantities that are to be written exist ===============
              // if the storage for m has not been used yet, initialize it
              if (storagem_.find(intersectionIndex)==storagem_.end()) {
                for (unsigned int gdComp=0; gdComp!=Ngd_; ++gdComp) {
                  storagem_[intersectionIndex][gdComp]=VectorType(numBoundEq);
                  // the following sets all entries of the vector to zero
                  storagem_[intersectionIndex][gdComp]=0;
                }
              }
              // if the storage for Q has not been used yet, initialize it
              if (storageQ_.find(intersectionIndex)==storageQ_.end()) {
                storageQ_[intersectionIndex]=MatrixType(numBoundEq, numBoundEq);
                // the following sets all entries of the vector to zero
                storageQ_[intersectionIndex]=0;
              }
              //==============================================================


              for (unsigned int qP=0; qP!=numQuadPointsForM; ++qP) {
                DomainType xLocal = quadratureForM.point(qP);
                DomainType xGlobal = geometry.global(xLocal);
                const double factor = 
                  quadratureForM.weight(qP)*geometry.integrationElement(xLocal);

                // evaluate delta
                std::vector<typename VectorFunctionType::RangeType> deltaValues;
                for (unsigned int i=0; i!=numBoundEq; ++i) {
                  typename VectorFunctionType::RangeType deltaVal(0.0);
                  blIt->second->evaluateDeltaLocal(i, it, xLocal, deltaVal);
                  deltaValues.push_back(deltaVal);
                }
                
                // evaluate g_d part
                std::vector<typename VectorFunctionType::RangeType> gdValues;
                for (unsigned int i=0; i!=problem_.Ngd(); ++i) {
                  typename VectorFunctionType::RangeType gdLiftedVal(0.0);
                  blIt->second->evaluateGDLocal(i, it, xLocal, gdLiftedVal);
                  gdValues.push_back(gdLiftedVal);
                }
                
                
                //================ set value of ||re(g_d,j)|| =============
                for (unsigned int i=0; i!=Ngd_; ++i) {
                  for (unsigned int j=0; j<=i; ++j) {
                    liftedGDNorms_[intersectionIndex][i*Ngd_+j]+=factor*(gdValues[i]*gdValues[j]);
                  }
                }
                

                for (unsigned int i=0; i!=numBoundEq; ++i) {

                  //============== set value of m^E_1 ========================
                  for (unsigned int gdComp=0; gdComp!=Ngd_; ++gdComp) 
                    storagem_[intersectionIndex][gdComp][i]+=factor*(gdValues[gdComp]*deltaValues[i]);

                  //====================== set value of Q^E_1 ====================
                  for (unsigned int j=0; j<=i; ++j) {
                    storageQ_[intersectionIndex][i][j]+=factor*(deltaValues[i]*deltaValues[j]);
                    if (i!=j)
                      storageQ_[intersectionIndex][j][i]+=factor*(deltaValues[i]*deltaValues[j]);
                  }

                }
              } // loop over quad points
            } // iteration over boundary liftings
            //profiler().StopTiming("Boundary terms");
            //std::cout << "QuadPoint done!\n";
          //profiler().StopTiming("jump");
          //          int klm=0;
          // std::cout << "One entity done!\n";
          // std::cin >> klm;
        } // grid walk
        ++assemblationBar;

        return;
      }

    private: 
      const EquationStorageType& equationStorage_;
      const ProblemType&         problem_;
      GridPartType&              gridPart_;
      const ReducedBasisSpace&   rbSpace_;
      const unsigned int         numSubDomains_;
      MatrixStorage              storageM_;
      std::vector<int>           cellCombinations_;
      MatrixStorage              storageN_;
      MatrixStorage              storageK_;
      VectorStorage              storagek_;

      std::map<int, std::map<int, VectorType> >  storagem_;
      std::vector<double>        fNorm_;
      std::map<int, std::map<int, double> >       liftedGDNorms_;

      Mapper                     mapper_;
      const unsigned int         Nlambda_;
      const unsigned int         Ngd_;
      std::map<int, MatrixType>  storageQ_;
    };
  }
}

#endif /* _ONLINEMATRIXSTORAGE_H_ */
