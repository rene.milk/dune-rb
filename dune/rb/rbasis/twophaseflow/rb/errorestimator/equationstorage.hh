#ifndef _EQUATIONSSTORAGE_H_
#define _EQUATIONSSTORAGE_H_

namespace Dune {
  namespace RB {
    /**@brief Map from intersection to index
     */
    template<class ErrorTraitsImpl>
    struct MyIntersectionIndexMapper {
      
      typedef ErrorTraitsImpl                         ErrorTraits;
      typedef typename ErrorTraits::IntersectionType  IntersectionType;
      typedef typename ErrorTraits::GridPartType      GridPartType;
      typedef typename GridPartType::IndexSetType     IndexSetType;
      typedef typename ErrorTraits::EntityType        EntityType;
      typedef typename ErrorTraits::EntityPointerType EntityPointerType;

    private:
      typedef std::map<unsigned int, std::map<unsigned int, int> > MapType;
      typedef MapType::const_iterator ConstMapIterator;
      //     typedef typename ErrorTraits::IntersectionIndexSetType IntersectionIndexSetType;
      
    public:
      MyIntersectionIndexMapper(const GridPartType& gridPart)
        : gridPart_(gridPart)//,
          //  intersectionIndexSet_(gridPart_.grid().leafView())
      {};
        
      void add(const unsigned int intersectionIndex,
               const unsigned int coarseCell, const int& index) {
        map_[coarseCell][intersectionIndex]=index;
        return;
      };

      int map(const unsigned int intersectionIndex,
              const unsigned int coarseCell) const {
        ConstMapIterator firstIt = map_.find(coarseCell);
        std::map<unsigned int, int>::const_iterator secondIt;
        if (firstIt!=map_.end()) {
          secondIt = firstIt->second.find(intersectionIndex);
          if (secondIt!=firstIt->second.end()) {
            return secondIt->second;
          }
        }
        // else, if intersection was not added before, return error
        return -1;
      }
        
    private:
      const GridPartType& gridPart_;
      MapType map_;
      //      IntersectionIndexSetType intersectionIndexSet_;
    };


    template<class ErrorTraits>
    class EquationStorage
    { 

      typedef typename ErrorTraits::IntersectionType         IntersectionType;
      typedef typename ErrorTraits::EntityPointerType        EntityPointerType;
      typedef typename ErrorTraits::EntityType               EntityType;
      typedef typename ErrorTraits::GridPartType             GridPartType;
      typedef typename ErrorTraits::PhiListType              PhiListType;
      typedef typename ErrorTraits::VectorSpaceType          VectorSpaceType;
      typedef typename ErrorTraits::VectorFunctionType       VectorFunctionType;
    public:
      typedef InnerLifting<ErrorTraits>                      InnerLiftingType;
      typedef BoundaryLifting<ErrorTraits>                   BoundaryLiftingType;
    private:
      typedef MyIntersectionIndexMapper<ErrorTraits>         MyMapper;
      typedef typename ErrorTraits::ProblemType              ProblemType;

    public:
      EquationStorage(const VectorSpaceType& vecSpace,
                      const ProblemType& problem) 
        : vecSpace_(vecSpace),
          mapper_(vecSpace_.gridPart()),
          problem_(problem)
      {};

      /** Destructor, free memory*/
      ~EquationStorage() {
        typedef typename std::vector<InnerLiftingType*>::iterator EqIterator;
        EqIterator end = innerLiftings_.end();
        for (EqIterator it = innerLiftings_.begin(); it!=end; ++it)
          delete *it;
        typedef typename std::map<int, BoundaryLiftingType*>::iterator BIterator;
        BIterator bend = boundaryLiftings_.end();
        for (BIterator bit = boundaryLiftings_.begin(); bit!=bend; ++bit)
          delete bit->second;
      }

      int addInner(const unsigned int intersectionIndex,
                   const IntersectionType& intersection,
                   const unsigned int coarseCell,
                   const PhiListType& phiList,
                   bool doInside) {
        const unsigned int index = innerLiftings_.size();
        InnerLiftingType* eq = new InnerLiftingType(vecSpace_, phiList, 
                                                    intersection, problem_, doInside);
        innerLiftings_.push_back(eq);
        mapper_.add(intersectionIndex, coarseCell, index);
        return index;
      }

      /** 
          @return returns the index of the intersection
      */
      void addBoundary(const unsigned int intersectionIndex,
                      const IntersectionType& intersection,
                      const PhiListType& phiList) {
        assert(boundaryLiftings_.find(intersectionIndex)==boundaryLiftings_.end());
        BoundaryLiftingType* foo = new BoundaryLiftingType(vecSpace_, phiList, intersection, problem_);
        boundaryLiftings_[intersectionIndex] = foo;
        return;
      }

      const InnerLiftingType& get(const unsigned int intersectionIndex,
                                  const unsigned int coarseCell) const {
        const int index = mapper_.map(intersectionIndex, coarseCell);
        assert(index!=-1);
        assert(index<int(innerLiftings_.size()));
        //        std::cout << " " << index << " ";
        return *innerLiftings_[index];
      }

      const std::map<int, BoundaryLiftingType*>& getBoundaryLiftings() const {
        return boundaryLiftings_;
      }

      void printAll() const {
        // print delta-equation system
        typedef typename std::vector<InnerLiftingType*>::const_iterator DeltaIterator;
        DeltaIterator deltaEnd = innerLiftings_.end();
        for (DeltaIterator deltaIt = innerLiftings_.begin(); 
             deltaIt!=deltaEnd; ++deltaIt) {
          (*deltaIt)->printMatrix();
        }
        return;
      }

    private:
      const VectorSpaceType&              vecSpace_;
      MyMapper                            mapper_; 
      std::vector<InnerLiftingType*>      innerLiftings_;
      std::map<int, BoundaryLiftingType*> boundaryLiftings_;
      const ProblemType&                  problem_;
    };
  }
}

#endif /* _EQUATIONSSTORAGE_H_ */
