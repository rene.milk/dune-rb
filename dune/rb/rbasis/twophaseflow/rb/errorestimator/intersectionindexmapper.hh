#ifndef __INTERSECTIONINDEXMAPPER_HH__
#define __INTERSECTIONINDEXMAPPER_HH__

namespace Dune {
  
  namespace RB {
    
    template< class ErrorTraits >
    class IntersectionIndexMapper
    {
      typedef typename ErrorTraits::GridPartType             GridPartType;
      typedef typename ErrorTraits::IntersectionIndexSetType IntersectionIndexSetType;
      typedef typename ErrorTraits::IntersectionType         IntersectionType;
      typedef typename ErrorTraits::EntityPointerType EntityPointerType;
      typedef typename ErrorTraits::EntityType EntityType;
      typedef typename ErrorTraits::GridType::LeafIndexSet EntityIndexSetType;
      typedef std::map<int, std::map<int, int> > MyMapType;
      typedef std::map<int, int> MyMapPartType;

    public:
      IntersectionIndexMapper(const GridPartType& gridPart)
        : gridPart_(gridPart),
          intersectionIndexSet_(gridPart_.grid().leafView()),
          entityIndexSet_(gridPart_.grid().leafIndexSet()),
          currentIndex_(0)
      {};
      
      ~IntersectionIndexMapper() {};

      const int map(const IntersectionType& intersection) {
        const EntityPointerType insidePtr = intersection.inside();
        const EntityType& insideEn = *insidePtr;
        int indexInside = intersectionIndexSet_.map(insideEn, intersection.indexInInside(), 1);
        int indexOutside = indexInside;
        if (intersection.neighbor()) {
        const EntityPointerType outsidePtr = intersection.outside();
        const EntityType& outsideEn = *outsidePtr;
        indexOutside = intersectionIndexSet_.map(outsideEn, intersection.indexInInside(), 1);
        }
        if (indexInside>indexOutside) {
          const int foo = indexInside;
          indexInside = indexOutside;
          indexOutside = foo;
        }
        MyMapType::iterator foundOne = map_.find(indexInside);
        MyMapPartType::iterator foundTwo = foundOne->second.end();
        if (foundOne!=map_.end())
          foundTwo = foundOne->second.find(indexOutside);
        if (foundOne!=map_.end() && foundTwo!=foundOne->second.end()) {
          return foundTwo->second;
        } else {
          map_[indexInside][indexOutside]=currentIndex_;
          ++currentIndex_;
        }
        return (currentIndex_-1);
      }      

    private:
      const GridPartType&            gridPart_;
      const IntersectionIndexSetType intersectionIndexSet_;
      const EntityIndexSetType&      entityIndexSet_;
      MyMapType                      map_;
      int                            currentIndex_;
    };
    
  } // namespace RB 
  
} // namespace Dune 

#endif /* __INTERSECTIONINDEXMAPPER_HH__ */
