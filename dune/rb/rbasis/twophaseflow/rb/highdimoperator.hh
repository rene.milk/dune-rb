#ifndef __HIGHDIMOPERATOR_HH__
#define __HIGHDIMOPERATOR_HH__

#define FUNCLIST_MEM

#include "operator_base.hh"
#include <dune/rb/rbasis/twophaseflow/helpers/progressbar.hh>

namespace Dune
{
namespace RB
{


template< class DiscreteFunctionSpaceImpl, class ProblemImpl >
class HighdimOperator : public OperatorBase<DiscreteFunctionSpaceImpl, 
                                            ProblemImpl, 
                                            HighdimOperator<DiscreteFunctionSpaceImpl, ProblemImpl> >
{
    typedef HighdimOperator< DiscreteFunctionSpaceImpl, ProblemImpl >          ThisType;
    typedef OperatorBase<DiscreteFunctionSpaceImpl, ProblemImpl, ThisType>     BaseType;
    friend class OperatorBase<DiscreteFunctionSpaceImpl, ProblemImpl, ThisType>;
    
public:
        // typedefs from base
    typedef typename BaseType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename BaseType::DiscreteFunctionListType  DiscreteFunctionListType;
    typedef typename BaseType::MatrixType                MatrixType;
    typedef typename BaseType::ProblemType               ProblemType;
    typedef typename BaseType::JacobianRangeType         JacobianRangeType;
    typedef typename BaseType::RangeFieldType            RangeFieldType;
    typedef typename BaseType::EntityType                EntityType;
    typedef typename BaseType::DomainType                DomainType;
    typedef typename BaseType::RangeType                 RangeType;
    typedef typename BaseType::EntityIteratorType        EntityIteratorType;
    typedef typename BaseType::GridPartType              GridPartType;
    typedef typename BaseType::IntersectionType          IntersectionType;



    typedef typename BaseType::EigenMatrixType     EigenMatrixType;
protected:
    typedef typename DiscreteFunctionSpaceType::BaseFunctionSetType BaseFunctionSetType;

public:
        // ! constructor
    HighdimOperator(const DiscreteFunctionSpaceType &dfSpace, const ProblemType &problem)
        : BaseType(dfSpace, problem,
                   Parameter::getValue<std::string>("highdimOp.dataPath"),
                   Parameter::getValue<double>("highdimOp.sigma"))
    {}

    ~HighdimOperator () {}

protected:
        // prohibit copying
    HighdimOperator ( const ThisType & );

public:
    /** \brief perform a grid walkthrough and assemble the global matrices
     *  and the right hand side.  */
    void assemble() 
    {
        /* apply local matrix assembler on each element, also computes
         *  boundary and intersection integrals*/
        boost::progress_display assemblyBar(this->discFuncSpace_.gridPart().grid().size(0));
        EntityIteratorType gridEnd = this->discFuncSpace_.end();
        for(EntityIteratorType it = this->discFuncSpace_.begin(); it != gridEnd; ++it, ++assemblyBar) {
            assembleOp(*it);

                // assemble right hand side and intersection parts of operator
            assembleRHS(*it);
        }
                   
        return;
    } /* assemble */

protected:
    template<class JacobianRangeTypeVec>
    void jacobianAll(const EntityType& entity, const DomainType& quadPoint,
                     JacobianRangeTypeVec& jac) const {
        const BaseFunctionSetType& baseFunctionSet = this->discFuncSpace_.baseFunctionSet(entity);
        jac.resize(baseFunctionSet.numBaseFunctions());
            // get jacobian inverse transposed
        const typename EntityType::Geometry::Jacobian& inv = entity.geometry().jacobianInverseTransposed(quadPoint);
        baseFunctionSet.jacobianAll(quadPoint, inv, jac);
        return;
    }

    template<class RangeTypeVec>
    void evaluateAll(const EntityType& entity, const DomainType& quadPoint,
                     RangeTypeVec& evals) const {
        const BaseFunctionSetType& baseFunctionSet = this->discFuncSpace_.baseFunctionSet(entity);
        evals.resize(baseFunctionSet.numBaseFunctions());
        baseFunctionSet.evaluateAll(quadPoint, evals);
        return;
    }

    template<class RangeTypeVec>
    void evaluateJumpAll(const EntityType& inside, const EntityType& outside,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         RangeTypeVec& evals) const {
        if (this->discFuncSpace_.continuous()) {
            const int size = this->discFuncSpace_.baseFunctionSet(inside).numBaseFunctions();
            RangeType zero(0.0);
            evals = RangeTypeVec(size, zero);
        } else
            this->evaluateAll(inside, quadPointInInside, evals);
        return;
    }

    template<class RangeTypeVec>
    void evaluateMeanAll(const EntityType& inside, const EntityType& outside,
                         const IntersectionType& intersection,
                         const DomainType& quadPointInInside,
                         const DomainType& quadPointInOutside,
                         const int opComp,
                         const DomainType& normal,
                         RangeTypeVec& evals) const {
        DomainType xGlobal = inside.geometry().global(quadPointInInside);
        typename ProblemType::DiffusionMatrixType Kinside;
            // evaluate diffusion matrix
        this->problem_.permeability(quadPointInInside, inside, Kinside);
        RangeType lambdaInside = this->problem_.mobility().component(opComp, xGlobal);
        DynamicArray<JacobianRangeType> insideGradientsTemp;
        this->jacobianAll(inside, quadPointInInside, insideGradientsTemp);
        DynamicArray<JacobianRangeType> insideGradients(insideGradientsTemp.size());
        evals.resize(insideGradientsTemp.size());

//        double weight = 0.5;
        
        // use weighted mean as supposed in Ern:2009uq
        // the inside and outside values of the permeability field
        Eigen::Matrix<double,GRIDDIM, GRIDDIM> Ki;
        Eigen::Matrix<double,GRIDDIM, GRIDDIM> Ko;
        this->problem_.permeability(quadPointInInside, inside, Ki);
        this->problem_.permeability(quadPointInOutside, outside, Ko);
        Eigen::Matrix<double, GRIDDIM, 1> intersectionNormal;
        for (int i=0; i!=DomainType::size; ++i)
            intersectionNormal(i) = normal[i];
        
        // for definition of deltaMinus/deltaPlus see Ern:2009uq, page 5
        double deltaMinus = intersectionNormal.transpose()*Ki*intersectionNormal;
        double deltaPlus  = intersectionNormal.transpose()*Ko*intersectionNormal;        
        
        double weight = deltaPlus/(deltaPlus+deltaMinus);
        // computation of weight done

        for (int i=0; i!=insideGradientsTemp.size(); ++i) {
            Kinside.mv(insideGradientsTemp[i][0], insideGradients[i][0]);
            evals[i][0] = insideGradients[i][0]*normal;
            evals[i][0] *= weight*lambdaInside[0];
        }
        return;
    } /* evaluateMeanAll */

//    template<class OutStreamType>
//    void printMatrix(const MatrixType& matrix, const std::string& name,
//                     OutStreamType& out) const {
//#ifdef DUNE_RB_HIGHDIMISTL
//        Stuff::printISTLMatrixMatlabStyle(matrix, name, out);
//#else
//        Stuff::printSparseRowMatrixMatlabStyle(matrix, name, out, 1e-14);
//#endif /* ifdef DUNE_RB_HIGHDIMISTL */
//        return;
//    }


};

}   // end namespace RB
} // end namespace Dune

#endif /* __HIGHDIMOPERATOR_HH__ */