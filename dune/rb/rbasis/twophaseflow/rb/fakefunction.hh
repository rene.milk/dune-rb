#ifndef __FAKEFUNCTION_HH__
#define __FAKEFUNCTION_HH__

#include <dune/common/fvector.hh>


namespace Dune {
  namespace RB {
 
    //template<typename ReconstructedFunctionImpl>
    // class FakeLocalFunction
    // {
    // public:
    //   typedef typename ReconstructedFunctionImpl::DiscreteFunctionSpaceType
    //   ::LagrangePointSetType::QuadraturePointWrapperType QuadraturePointWrapperType;
    //   typedef FieldVector<double, 2>                                VectorRangeType;

    //   FakeLocalFunction();
    //   virtual void evaluate(const QuadraturePointWrapperType& qpw,
    //                         VectorRangeType& ret) const;
    // };

    // template<typename ReconstructedFunctionImpl>
    // class ZeroLocalFunction : public FakeLocalFunction<ReconstructedFunctionImpl>
    // {
    //   typedef FakeLocalFunction<ReconstructedFunctionImpl>   BaseType;
    //   typedef typename BaseType::VectorRangeType             VectorRangeType;
    //   typedef typename BaseType::QuadraturePointWrapperType& QuadraturePointWrapperType;

    // public:
    //   ZeroLocalFunction()
    //   {};

    //   // The way we use this function at the moment, 
    //   // qpw will be a quadrature point wrapper
    //   void evaluate(const QuadraturePointWrapperType& qpw,
    //                 VectorRangeType& ret) const {
    //     ret[0]=0.0;
    //     ret[1]=0.0;
    //     return;
    //   }

    // };

    template<class EntityImpl, class ReconstructedFunctionImpl, class ProblemImpl, class DestFuncImpl>
    class NonZeroLocalFunction
    // : public FakeLocalFunction<ReconstructedFunctionImpl>
    {
      typedef EntityImpl                EntityType;
      typedef ReconstructedFunctionImpl ReconstructedFunctionType;
      typedef ProblemImpl               ProblemType;
      typedef DestFuncImpl              DestFuncType;

      typedef typename ReconstructedFunctionType::LocalFunctionType LocalFunctionType;
      typedef typename EntityType::Geometry                         Geometry;
      typedef typename ReconstructedFunctionType::DomainType        DomainType;
      typedef typename ReconstructedFunctionType::RangeType         MyRangeType;
      typedef typename ReconstructedFunctionType::JacobianRangeType JacobianRangeType;
      typedef typename ProblemType::DiffusionMatrixType             DiffusionMatrixType;
      //      typedef FakeLocalFunction<ReconstructedFunctionImpl>   BaseType;
      // typedef typename BaseType::VectorRangeType             VectorRangeType;
      // typedef typename BaseType::QuadraturePointWrapperType& QuadraturePointWrapperType;
    public:
      typedef FieldVector<double, 2>                                RangeType;
    private:
      typedef typename DestFuncType::DiscreteFunctionSpaceType
      ::LagrangePointSetType::QuadraturePointWrapperType QuadraturePointWrapperType;

      typedef typename ReconstructedFunctionType::DiscreteFunctionSpaceType::GridPartType
      GridPartType;

    public:
      NonZeroLocalFunction(const EntityType& entity,
                           const unsigned int sigma,
                           const ReconstructedFunctionType& phi,
                           const ProblemType& problem,
                           const int support)
        : entity_(entity),
          sigma_(sigma),
          phiLocal_(phi.localFunction(entity)),
          problem_(problem),
          geometry_(entity.geometry())
      {
        nonZero_ = (phi.space().gridPart().getSubDomain(entity)==support);
      };

      // The way we use this function at the moment, 
      // qpw will be a quadrature point wrapper
      void evaluate(const QuadraturePointWrapperType& qpw, RangeType& ret) const {
        //        if (nonZero_) {
          // extract the real coordinates from the quadrature point wrapper
          DomainType x = qpw.quadrature().point(qpw.point());
          ret[0] = 0.0;
          ret[1] = 0.0;
          DomainType xGlobal = geometry_.global(x);
          JacobianRangeType jac(0.0);
          phiLocal_.jacobian(x, jac);
          MyRangeType lambda;
          problem_.lambdaPart(sigma_, xGlobal, lambda);
          DiffusionMatrixType K;
          // evaluate diffusion matrix 
          problem_.K(xGlobal, K);
          K.mv(jac[0], ret);
          ret *= lambda[0];
        // }
        // else {
        //   ret[0] = 0.0;
        //   ret[1] = 0.0;
        // }
        return;
      }

    private:
      const EntityType&       entity_;
      const unsigned int      sigma_;
      const LocalFunctionType phiLocal_;
      const ProblemType&      problem_;
      const Geometry&         geometry_;
      bool nonZero_;
    };
   


    template<class ReconstructedFunctionImpl, class ProblemImpl, class DestFuncImpl>
    class FakeFunction
    {
      typedef ReconstructedFunctionImpl                    ReconstructedFunctionType;
      typedef ProblemImpl                                  ProblemType;
      typedef typename ReconstructedFunctionType::GridType::template Codim<0>::Entity
      EntityType;
      //    typedef typename ReconstructedFunctionType::DiscreteFunctionSpaceType::GridPartType
      //      GridPartType;
      // typedef ZeroLocalFunction<ReconstructedFunctionType>  ZeroLocalFunctionType;
      typedef DestFuncImpl DestFuncType;

      typedef NonZeroLocalFunction<EntityType, ReconstructedFunctionType, ProblemType, DestFuncType> 
      NonZeroLocalFunctionType;

    public:
      // typedef FakeLocalFunction<ReconstructedFunctionType>   LocalFunctionType;
      typedef NonZeroLocalFunctionType LocalFunctionType;

      FakeFunction(const unsigned int sigma,
                   const ReconstructedFunctionType& phi,
                   const ProblemType& problem,
                   const int support)
        : sigma_(sigma),
          phi_(phi),
          problem_(problem),
          support_(support)//,
          // gridPart_(phi.space().gridPart()),
          // zeroLocalFunc_()
      {};

      const LocalFunctionType localFunction(const EntityType& entity) const {
        // if (gridPart_.getSubDomain(entity)!=support_)
        //   return zeroLocalFunc_;
        return NonZeroLocalFunctionType(entity, sigma_, phi_, problem_, support_);
      }

      LocalFunctionType localFunction(const EntityType& entity) {
        // if (gridPart_.getSubDomain(entity)!=support_)
        //   return zeroLocalFunc_;
        return NonZeroLocalFunctionType(entity, sigma_, phi_, problem_, support_);
      }

    private:
      const unsigned int               sigma_;
      const ReconstructedFunctionType& phi_;
      const ProblemType&               problem_;
      const int                        support_;
      //      const GridPartType&              gridPart_;
      // ZeroLocalFunctionType            zeroLocalFunc_;
    };


} // namespace RB   
} // namespace Dune 

#endif /* __FAKEFUNCTION_HH__ */
