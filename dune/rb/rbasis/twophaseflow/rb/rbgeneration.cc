namespace Dune {
namespace RB {

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::init(DofVectorType& p) {
    
    ParameterDomainType muBak = problem_.mobility().getParameter();
    problem_.mobility().setParameter(muBar_);
    const typename HighdimAlgorithmType::DiscreteOperatorType::EigenMatrixType&
    systemMatrixEigen = algorithm_.getOperator().systemMatrixEigen();
    problem_.mobility().setParameter(muBak);
    
    for (int i = 0; i<int(gridPartition_.numSubdomains()); ++i) {
        
        DofVectorType localizedP = localizer_.localizeVector(p, i);
        
        // save the snapshot
        const int oldSize = snapshots_[i].cols();
        snapshots_[i].conservativeResize(Eigen::NoChange, oldSize+1);
        // set new column to new base function
        snapshots_[i].col(oldSize)=localizedP;
        
        const typename LocalizerType::VectorType fullP = localizer_.getFullVector(localizedP, i);
        double normSquared = fullP.transpose()*systemMatrixEigen*fullP;
        
        assert(normSquared>=0.0);
        double norm = sqrt(normSquared);
        
        
        // if norm of projected function is too small abort algorithm
        if (norm<1.0e-11) {
            DUNE_THROW(InvalidStateException, "Norm of initial function on element " + boost::lexical_cast<std::string>(i) 
                       + " (" + boost::lexical_cast<std::string>(norm) + ") is too close to zero, aborting!");
        }
        if (Parameter::getValue<bool>("rbgeneration.normOnElement"))
            localizedP /= norm;
        
        
        rbSpace_.addBaseFunction(localizedP, i);
    }      
    
    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::init() {
    DofVectorType p;
    algorithm_(p);  
   
//    plotCellData(p, gridPartition_.grid(), "Initial_solution", "vis");
  plotFunction<DetailedFunctionType>(p, algorithm_.space(), "Initial_solution");
  
    ParameterDomainType muBak = problem_.mobility().getParameter();
    problem_.mobility().setParameter(muBar_);
    double normSquared = p.transpose()*algorithm_.getOperator().systemMatrixEigen()*p;
    assert(normSquared>=0.0);
    double norm = sqrt(normSquared);
    problem_.mobility().setParameter(muBak);

    init(p);
    if (verbose_) {
      std::cout << "Initialized reduced basis using mu = [ " << problem_.mobility().getParameter().transpose() << " ]\n";
    }
    return;
}

  template< class ProblemImp, class DofVectorImp, class DiscreteFunctionImp >
  class Flux {
    typedef Flux<ProblemImp, DofVectorImp, DiscreteFunctionImp> ThisType;
    typedef ProblemImp ProblemType;
    typedef DofVectorImp DofVectorType;
    typedef DiscreteFunctionImp DiscreteFunctionType;
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
    typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

  public:
    typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
    typedef typename Dune::FieldVector<double, DomainType::size>  RangeType;

  public:
   
    Flux(const ProblemType& problem,
         const DofVectorType& dofVector,
         const DiscreteFunctionSpaceType& discFuncSpace)
      : problem_(problem),
        dofVector_(dofVector),
        sourceFunction_("Flux", discFuncSpace, dofVector_.data())
    {}

    template< class EntityType >
    void evaluate(const DomainType& global, const DomainType& local, const EntityType& en, RangeType& ret) const {
      Eigen::MatrixXd permeability(3,3);
      problem_.permeability(local, en, permeability);
      LocalFunctionType sourceLocalFunction = sourceFunction_.localFunction(en);
      typename DiscreteFunctionType::JacobianRangeType jac(0.0);
      sourceLocalFunction.jacobian(local, jac);
      Eigen::VectorXd jacEigen(3);
      static const int dimDomain = DomainType::size;
      for (int i=0; i!=dimDomain; ++i)
        jacEigen(i)=jac[0][i];

      Eigen::VectorXd retEigen = permeability*jacEigen;
      retEigen *= -1.0 * problem_.mobility().evaluate(global);
      for (int i=0; i!=dimDomain; ++i)
        ret[i] = retEigen(i);
      return;
    }

  private:
    const ProblemType& problem_;
    const DofVectorType& dofVector_;
    const DiscreteFunctionType sourceFunction_;
  };

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::printFlux(const DofVectorType& dofVector) const {

  typedef FunctionSpace<double, double, GridPartitionType::GridType::dimension, GridPartitionType::GridType::dimension>              JacFunctionSpaceType;
        typedef DiscontinuousGalerkinSpace<JacFunctionSpaceType, GridPartitionType, polynomialOrder> JacDiscFuncSpaceType;
        typedef AdaptiveDiscreteFunction<JacDiscFuncSpaceType>                                       JacDiscreteFunctionType;
        typedef Flux<ProblemType, DofVectorType, DetailedFunctionType> FluxType;

        JacDiscFuncSpaceType jacSpace(gridPartition_);
        JacDiscreteFunctionType fluxDiscrete("flux", jacSpace);
        FluxType flux(problem_, dofVector, algorithm_.space());

        Dune::RB::BetterL2Projection l2projection;
        l2projection.project(flux, fluxDiscrete);
        plotVectorFunction(fluxDiscrete);
        return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::reducedSimulation(DofVectorType &p, const bool printAll) const {

    typedef typename ReducedOperatorType::SparseMatrixType ReducedMatrixType;
    typedef typename ReducedOperatorType::VectorType ReducedVectorType;
    const ReducedMatrixType& reducedMatrix = rbOp_.systemMatrix();
    const ReducedVectorType& rightHandSide = rbOp_.rightHandSide();
    // solve
    
#if 0

    const int solverType = Parameter::getValue<int>("rbgeneration.reducedSolver.type", 0);
    switch (solverType) {
        case 0: {
            Eigen::SuperLU<ReducedMatrixType> solver(reducedMatrix);   
            p = solver.solve(rightHandSide);
        }
            break;
        case 1: {
            Eigen::SimplicialLDLt<ReducedMatrixType> solver(reducedMatrix);
            p = solver.solve(rightHandSide);
        }
            break;
        case 2: {
            Eigen::BiCGSTAB<ReducedMatrixType> solver(reducedMatrix);
            p = solver.solve(rightHandSide);
        }
            break;
        case 3: {
            Eigen::ConjugateGradient<ReducedMatrixType> solver(reducedMatrix);
            p = solver.solve(rightHandSide);   
        }
            break;
        case 4: {
            Eigen::BiCGSTAB<ReducedMatrixType, Eigen::IncompleteLUT<double> > solver(reducedMatrix); 
            double dropTol 
            = Parameter::getValue<double>("rbgeneration.reducedSolver.preconditioner.dropTol", 1e-4);
            int fillFactor 
            = Parameter::getValue<int>("rbgeneration.reducedSolver.preconditioner.fillFactor", 20);
            solver.preconditioner().setDroptol(dropTol);
            solver.preconditioner().setFillfactor(fillFactor);
            p = solver.solve(rightHandSide);
        }
            break;
        case 5: {
            Eigen::ConjugateGradient<ReducedMatrixType, Eigen::Lower, Eigen::IncompleteLUT<double> > solver(reducedMatrix);
            double dropTol 
            = Parameter::getValue<double>("rbgeneration.reducedSolver.preconditioner.dropTol", 1e-4);
            int fillFactor 
            = Parameter::getValue<int>("rbgeneration.reducedSolver.preconditioner.fillFactor", 20);
            solver.preconditioner().setDroptol(dropTol);
            solver.preconditioner().setFillfactor(fillFactor);
            p = solver.solve(rightHandSide);
        }
            break;   
        case 6: {
            Eigen::MatrixXd reducedMatrixDense = reducedMatrix.toDense();
            p = reducedMatrixDense.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(rightHandSide);
        }
            break;
        case 7: {
            Eigen::MatrixXd reducedMatrixDense = reducedMatrix.toDense();
            p = reducedMatrixDense.fullPivLu().solve(rightHandSide);
        }
            break;
        default: 
            DUNE_THROW(InvalidStateException, "Invalid solver ID for reduced solver given!");
    }
#endif

    if (Parameter::getValue<bool>("rbgeneration.reducedSolver.computeConditionNumber", false)) {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(reducedMatrix);
        std::cout << "Condition number of reduced matrix: " << eigensolver.eigenvalues()(reducedMatrix.rows()-1)/eigensolver.eigenvalues()(0)
        << "\n";
    }
    Eigen::SuperLU<ReducedMatrixType> solver(reducedMatrix);   
    p = solver.solve(rightHandSide);

    
    if (printAll) {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(reducedMatrix);
        std::cout << "Reduced matrix is:\n" << reducedMatrix << "\n";
        std::cout << "Right hand side is:\n" << rightHandSide << "\n";
        std::cout << "Solution with eigen:\n" << p << std::endl;
        std::cout <<  "for mu=[" << problem_.mobility().getParameter().transpose();
        std::cout << "]\nResidual: " << reducedMatrix*p-rightHandSide;
        if (eigensolver.info() != Eigen::Success) {
          std::cerr << "\nCould not compute eigenvalues of reduced matrix!\n";
          return;
        }
        std::cout << "\nThe eigenvalues of A are:\n" << eigensolver.eigenvalues() << std::endl;

    }
    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline bool
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::extend() {
    DofVectorType p;
    algorithm_(p);

    return extend(p);
}
    
    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    inline void
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::printCompressibility() {
        ParameterDomainType muBak = problem_.mobility().getParameter();
        problem_.mobility().setParameter(muBar_);
        typedef typename ReducedOperatorType::MatrixType ReducedMatrixType;
        std::vector<int> subdomainBasisSizes;
        std::vector<double> eigenvalues;
        for (int subdomain=0; subdomain!=gridPartition_.numSubdomains(); ++subdomain) {
            const ReducedMatrixType& subdomainBasis = rbSpace_.getSubdomainBasis(subdomain);
            const int subdomainBasisSize = subdomainBasis.cols();
            ReducedMatrixType subdomainMatrix = subdomainBasis.transpose()*subdomainBasis;
            
            // compute eigenvalues
            Eigen::SelfAdjointEigenSolver<ReducedMatrixType> eigensolver(subdomainMatrix);
            if (eigensolver.info() != Eigen::Success) {
              std::cerr << "Could not compute eigenvalues in printCompressibility!\n";
              return;
            }
            subdomainBasisSizes.push_back(subdomainBasisSize);
            
            for (int i=0; i!=eigensolver.eigenvalues().rows(); ++i) {
                eigenvalues.push_back(eigensolver.eigenvalues()(i));
            }
        }
      //  callPython(eigenvalues, subdomainBasisSizes, "plotCompressibility");
                                
        problem_.mobility().setParameter(muBak);        
        return;
    }

    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    template<class ContainerType1, class ContainerType2>
    inline void
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::callPython(const ContainerType1& xData,
                                                 const ContainerType2& yData,
                                                 const std::string& desiredFunction) {
        
        // boost::python::class_<ContainerType1 >("PyVec")
        // .def(boost::python::vector_indexing_suite<ContainerType1 >());
        // boost::python::class_<ContainerType2 >("PyVec")
        // .def(boost::python::vector_indexing_suite<ContainerType2 >());

        
        // boost::python::object mymodule = boost::python::import("mymodule");
        // boost::python::object dict = mymodule.attr("__dict__");
        
        // boost::python::object func = dict[desiredFunction];
        // func(xData, yData);
        
        return;
    }
    
    
    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    inline void
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::saveSnapshots() const {
        // make sure that the directory exists
        boost::filesystem::create_directory("snapshots");
        const int numSubdomains = snapshots_.size();
        for (int subdomain=0; subdomain!=numSubdomains; ++subdomain) {
            snapshots_[subdomain].saveToBinaryFile(std::string("snapshots")+std::string("/subdomain_")
                                                     +boost::lexical_cast<std::string>(subdomain)
                                                   +std::string("_data.bin"));
        }
        return;
    }
    
    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    inline void
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::readSnapshots()  {
        const int numSubdomains = gridPartition_.numSubdomains();
        snapshots_.resize(numSubdomains);
        for (int subdomain=0; subdomain!=numSubdomains; ++subdomain) {
            snapshots_[subdomain].loadFromBinaryFile(std::string("snapshots")+std::string("/subdomain_")
                                                                  +boost::lexical_cast<std::string>(subdomain)
                                                     +std::string("_data.bin"));
        }
        return;
    }
    
    
    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    inline bool
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::extend(DofVectorType& p) {
        
        const bool withGramSchmidt = Parameter::getValue<bool>("rbgeneration.withGramSchmidt");
        const bool withEnergyProduct = Parameter::getValue<bool>("rbgeneration.gramschmidt.useEnergyScalarProduct", false);
        const bool normOnElement = Parameter::getValue<bool>("rbgeneration.normOnElement");
        const double gramSchmidtDropTolerance = Parameter::getValue<double>("rbgeneration.gramSchmidtDropTolerance", 1e-11);
        bool extended = false;
        
        ParameterDomainType muBak = problem_.mobility().getParameter();
        problem_.mobility().setParameter(muBar_);
        // call gram-schmidt
        const typename HighdimAlgorithmType::DiscreteOperatorType::EigenMatrixType&
        systemMatrixEigen = algorithm_.getOperator().systemMatrixEigen();
        problem_.mobility().setParameter(muBak);
        
        // iterate over all subdomains
        for (unsigned int element = 0; element<gridPartition_.numSubdomains(); ++element) {
            const typename ReducedSpaceType::SubdomainBasisType& subdomainBasis = rbSpace_.getSubdomainBasis(element);
            
            DofVectorType localizedP = localizer_.localizeVector(p, element);
            DofVectorType fullP = localizer_.getFullVector(localizedP, element);
            // save the snapshot
            const int oldSize = snapshots_[element].cols();
            snapshots_[element].conservativeResize(Eigen::NoChange, oldSize+1);
            // set new column to new base function
            snapshots_[element].col(oldSize)= localizedP;
            
            // this is the l2 norm at the moment (yes, lowercase l)
            double localNorm = localizedP.norm();
            
            // shall we apply the Gram-Schmidt algorithm?
            if (withGramSchmidt) {
                // shall we use the energy scalar product in gram schmidt?
                if (withEnergyProduct) {
                    
                    const typename LocalizerType::MatrixType& fullSubdomainBasis
                    = localizer_.getFullMatrixRows(subdomainBasis, element);
                    
                    boost::timer::cpu_timer timer;
                    gramSchmidt(fullP, fullSubdomainBasis, systemMatrixEigen);
                    gramSchmidtTime_ += timer.elapsed().wall/1e6;
                    double localNormSquared = fullP.transpose()*systemMatrixEigen*fullP;
                    assert(localNormSquared>=0.0);
                    localNorm=sqrt(localNormSquared);
                    
                } else {
                    const typename LocalizerType::MatrixType& fullSubdomainBasis
                    = localizer_.getFullMatrixRows(subdomainBasis, element);
                    
                    boost::timer::cpu_timer timer;
                    Eigen::MatrixXd unit;
                    unit.setIdentity(systemMatrixEigen.rows(), systemMatrixEigen.cols());
                    gramSchmidt(fullP, fullSubdomainBasis, unit);
                    gramSchmidtTime_ += timer.elapsed().wall/1e6;
                    double localNormSquared = fullP.transpose()*systemMatrixEigen*fullP;
                    assert(localNormSquared>=0.0);
                    localNorm=sqrt(localNormSquared);
                }
            }
            
            // restrict the p-vector back to the subdomain
            localizedP = localizer_.localizeVector(fullP, element);
            
            // make shure the restricted function is something meaningfull
            assert(!std::isnan(localNorm));
            assert(localNorm>=0.0);
            // if norm of projected function is too small, we don't add it
            if (localNorm<gramSchmidtDropTolerance) {
                std::cout << "Did not extend the basis on Subdomain " 
                << element
                << " as the candidate already "
                << "belongs to the span of the current basis!\n";
            } else {
                // shall we norm the resulting function on subdomains?
                if (normOnElement)
                    localizedP /= localNorm;
                
                rbSpace_.addBaseFunction(localizedP, element);
                extended = true;
            }
        }
        return extended;
    }

//template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
//inline void
//RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::generateBasisFromSnapshots() {
//    DiscreteFunction p("snapshot", rbSpace_.baseFunctionSpace());
//    const int numSnapshots = snapshots_.size();
//    snapshots_.stashFunctions(0, numSnapshots);
//
//    //      Gnuplot plotter("Eigenvalues of Gram Matrix");
//    //      plotter.reset_all();
//
//    for (unsigned int i = 0; i<gridPartition_.numSubDomains(); ++i) {
//        std::stringstream name, namepre,nameppre;
//        name << "SD" << i << "_Data";
//        namepre << "SD" << i << "_Datapre";
//        nameppre << "SD" << i << "_Datappre";
//        ConstrainedDFList restrictedSnapshots(rbSpace_.baseFunctionSpace(), nameppre.str());
//        ConstrainedDFList newLocalBase(rbSpace_.baseFunctionSpace(), namepre.str());
//        for (int j=0; j!=numSnapshots; ++j) {
//            p.clear();
//            snapshots_.getFunc(j, p);
//            ConstrainedDF pConstrained(p, gridPartition_, i);
//            ConstrainedDF pRestricted(p, gridPartition_, i);
//            restrictFunction(pConstrained, pRestricted);
//            restrictedSnapshots.push_back(pRestricted);
//        }
//        PCA<ConstrainedDFList> pca(restrictedSnapshots, i);
//        pca.computeGramMatrix();
//        pca.computeEigenvalues();
//        //        pca.plotEigenvalues(plotter);
//        pca.computeNeededNumberByError(Parameter::getValue<double>("pca.eps"));
//        pca.computePrincipalComponents(newLocalBase);
//        ConstrainedDFList newLocalBaseOrthonormalized(rbSpace_.baseFunctionSpace(), name.str());
//        gramSchmidt(newLocalBase, newLocalBaseOrthonormalized);
//        //        pca(restrictedSnapshots, newLocalBase, i);
//        rbSpace_.setFuncList(i, newLocalBaseOrthonormalized);
//    }
//}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::compressInformation() {
  printCompressibility();

    for (unsigned int i = 0; i<gridPartition_.numSubdomains(); ++i) {
//        typedef typename ReducedOperatorType::MatrixType ReducedMatrixType;
//        const ReducedMatrixType subdomainBasis = rbSpace_.getSubdomainBasis(i);
//        PCAEigen<ReducedMatrixType> pca(subdomainBasis);
        const EigenMatrixType& subdomainBasis = snapshots_[i];
        const EigenMatrixType& subdomainBasisFull = localizer_.getFullMatrixRows(subdomainBasis, i);
        PCAEigen<EigenMatrixType> pca(subdomainBasisFull);
        std::cout << "Subdomain " << i << ". Size before: " << subdomainBasisFull.cols();
        std::cout.flush();
        //        pca.subtractMean(restrictionMatrix, i);
        if (Parameter::getValue<bool>("pca.useSystemMatrix", false)) {
            ParameterDomainType muBak = problem_.mobility().getParameter();
            problem_.mobility().setParameter(muBar_);
            pca.computeCovarianceMatrix(algorithm_.getOperator().systemMatrixEigen());
            problem_.mobility().setParameter(muBak);
        }
        else
            pca.computeCovarianceMatrix();
        pca.computeEigenvalues();
//        pca.computeNeededNumberByPercentage(Parameter::getValue<double>("pca.desiredPercentage"));
        pca.computeNeededNumberByError(Parameter::getValue<double>("pca.eps"));
        EigenMatrixType principalComponents;
        pca.computePrincipalComponents(principalComponents);
        std::cout << ". Size after: " << principalComponents.cols() << std::endl;        
        std::cout.flush();
        rbSpace_.getSubdomainBasis(i)=localizer_.localizeMatrixRows(principalComponents, i);
    }

    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::uniformExtension() {
    std::vector<double> begin;
    std::vector<double> end;
    const int paramDimensionSize = Parameter::getValue<int>("podgreedy.paramspace.size");
    Parameter::get("podgreedy.paramspace.begin", begin);
    Parameter::get("podgreedy.paramspace.end", end);

    std::vector<int> numParamsVec(begin.size(), paramDimensionSize);
    UniformParameterSpace<std::vector<double>, std::vector<int> >
    paramSpace(begin, end, numParamsVec);
    //      assert(numParams_ == begin.size());
    const int paramSpaceSize = paramSpace.size();

    std::cout << "Extending reduced basis using uniform algorithm";
    boost::progress_display generationBar(paramSpaceSize);

    boost::timer::cpu_timer extensionTimer;
    
    for (int k=0; k<paramSpaceSize; ++k, ++generationBar) {
        ParameterDomainType mu;
        paramSpace.getParameter(k, mu);
        problem_.mobility().setParameter(mu);
        bool wasExtended = extend();
        if (verbose_ && wasExtended)
            printExtensionInfo(mu);
    }

    if (Parameter::getValue<bool>("podgreedy.applyFinalPOD")) {
        compressInformation();
        std::cout << "Compressed basis to total size " << rbSpace_.size() << std::endl;
    }

    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::greedyExtension(const boost::uuids::uuid& uid) {
    ParameterDomainType begin;
    ParameterDomainType end;
    const int paramDimensionSize = Parameter::getValue<int>("podgreedy.paramspace.size");
    Parameter::get("podgreedy.paramspace.begin", begin);
    Parameter::get("podgreedy.paramspace.end", end);
    std::vector<int> numParamsVec(begin.size(), paramDimensionSize);

//    typedef UniformParameterSpace<ParameterDomainType, std::vector<int> > ParamSpaceType;
//    typedef TriangularParameterSpace<ParameterDomainType, std::vector<int> > ParamSpaceType;
#if GRIDDIM==2
    typedef RandomParameterSpace<ParameterDomainType> ParamSpaceType;
#else
    typedef TriangularRandomParameterSpace<ParameterDomainType> ParamSpaceType;
#endif

    ParamSpaceType* paramSpace = new ParamSpaceType(begin, end, paramDimensionSize, 42);
//        ParamSpaceType* paramSpace = new ParamSpaceType(begin, end, numParamsVec);

    const int paramSpaceSize = paramSpace->size();
    std::cout << "Running Greedy on training set with " << paramSpaceSize << " elements!\n";
    
    const double desiredError = Parameter::getValue<double>("podgreedy.desiredError");

    const double maxRuntime = Parameter::getValue<double>("podgreedy.maxRuntime");
    double runTime = 0.0;

    const int desiredSize = Parameter::getValue<double>("podgreedy.desiredSize");

    bool breakOnTime=false;
    bool breakOnError=false;
    bool breakOnSize=false;
    bool breakOnEmptyExtension=false;

    // prepare file for output
    std::stringstream filename;
    // this generates a unique filename
    filename << "rbgeneration_" << uid;
    std::ofstream file((filename.str()+".log").c_str());
    assert(file);
    file << "POD-Greedy Basis Generation\n"
         << "---------------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "ExtensionStep\tBasisSize\tBasisSizePerSD\tMaxError"
         << "\tMinError\tMeanError\tStdDeviation\tOpAssTime\tEstAssTime\tTrainingTime\n";
    // backup used parameters
    Parameter::write((filename.str()+".paramlog").c_str());

    std::multiset<double> operatorAssemblingTimes;
    std::multiset<double> estimatorAssemblingTimes;
    std::multiset<double> trainingTimes;
    int extensionStep = 0;
    boost::timer::cpu_timer extensionTimer;

    const bool useErrorEstimator = Parameter::getValue<bool>("rbgeneration.useErrorEstimator", true);

    while ((!breakOnTime) && (!breakOnError) && (!breakOnSize) && (!breakOnEmptyExtension)) {

        std::multimap<double, ParameterDomainType> errorVec;

        // assemble reduced operator, returns used time
        double opAssemblingTime = updateReducedOperator();
        operatorAssemblingTimes.insert(opAssemblingTime);

        // assemble error estimator, returns used time
        double estimatorAssemblingTime = 0.0;
        if (useErrorEstimator)
            estimatorAssemblingTime = updateErrorEstimator();
        estimatorAssemblingTimes.insert(estimatorAssemblingTime);

        std::cout << "Performing reduced simulation and evaluating error"
                  << " estimator for all parameters in the training set!\n";
        //        Stuff::SimpleProgressBar<> trainingBar(paramSpaceSize);
        boost::timer::cpu_timer trainingTimer;
        for (int k=0; k<paramSpaceSize; ++k) {                   //, ++trainingBar) {
            ParameterDomainType mu;
            paramSpace->getParameter(k, mu);
            double myError = 0.0;
            if (useErrorEstimator)
                myError = getError(mu);
            else {
                double lowerBound, upperBound, bar;
                getErrorHighdim(mu, bar, bar, bar, myError, upperBound, lowerBound);
            }
            errorVec.insert(std::pair<double,ParameterDomainType>(myError, mu));
        }
        //        ++trainingBar;
        std::cout.flush();
        // save time needed for training
        double trainingTime = trainingTimer.elapsed().wall/1e6;
        trainingTimes.insert(trainingTime);
        // if desired error was reached: break
        if (errorVec.rbegin()->first<desiredError) {
            breakOnError=true;
            if (verbose_)
                std::cout << "\nReached desired accuracy, will break greedy now!\n";
            if (Parameter::getValue<bool>("podgreedy.applyFinalPOD"))
                compressInformation();
        }
        // else extend the basis
        else {
            if (verbose_)
                std::cout << "\nMaximum error " << errorVec.rbegin()->first
                          << " was reached for mu="
                          << errorVec.rbegin()->second.transpose() << std::endl;
            problem_.mobility().setParameter(errorVec.rbegin()->second);
            if (Parameter::getValue<bool>("podgreedy.plot"))
                plotError(true);

            bool wasExtended = extend();
            if (wasExtended)
                usedMus_.push_back(errorVec.rbegin()->second);
            if (verbose_ && wasExtended)
                printExtensionInfo(errorVec.rbegin()->second);

            if (!wasExtended) {
                breakOnEmptyExtension = true;
                if (verbose_)
                    std::cout << "\nExtension was empty, will break greedy now!\n";
            }
        }
        // if the desired maximum size was reached: break
        if (rbSpace_.size()>=desiredSize) {
            if (verbose_)
                std::cout << "\nReached desired size of RB space, will break greedy now!\n";
            if (useErrorEstimator)
                estimatorAssemblingTimes.insert(updateErrorEstimator());
            breakOnSize = true;
        }
        // if the desired maximum runtime was reached: break
        if (extensionTimer.elapsed().wall/1e9>maxRuntime) {
            if (verbose_)
                std::cout << "\nTime limit for basis construction reached, will break greedy now!\n";
            breakOnTime = true;
        }
        // write sizes of bases on subdomains to file
        file << extensionStep++ << "\t" << rbSpace_.size() << "\t";
        for (int j=0; j!=int(gridPartition_.numSubdomains()); ++j) {
            file << rbSpace_.getSubdomainBasis(j).cols();
            if (j!=int(gridPartition_.numSubdomains()-1))
                file << "/";
        }
        // save computed errors and runtimes to file
        file << "\t" << errorVec.rbegin()->first
             << "\t" << errorVec.begin()->first
             << "\t" << meanOfKeys(errorVec) << "\t" << stddeviationOfKeys(errorVec)
             << "\t" << opAssemblingTime << "\t" << estimatorAssemblingTime << "\t" << trainingTime
             << "\n";
        file.flush();
    }

    // save the gathered information to file
    file << "\n\nMean assembling time for reduced operator (s): " << mean(operatorAssemblingTimes)
         << " (Std deviation: " << stddeviation(operatorAssemblingTimes) << ")\n"
         << "Mean update time for estimator (s): " << mean(estimatorAssemblingTimes)
         << " (Std deviation: " << stddeviation(estimatorAssemblingTimes) << ")\n"
         << "Mean training time (ms): " << mean(trainingTimes)
         << " (Std deviation: " << stddeviation(trainingTimes) << ")\n"
         << "Total time needed for gram-schmidt (ms): " << gramSchmidtTime_ << "\n"
         << "Break on error: " << breakOnError
         << "\nBreak on time: " << breakOnTime
         << "\nBreak on size: " << breakOnSize
         << "\nBreak on empty extension: " << breakOnEmptyExtension
         << "\nUsed parameters:\n";
    for (int j=0; j!=int(usedMus_.size()); ++j)
      file << usedMus_[j].transpose() << "\n";
    file.flush();
    file.close();

    // free memory taken by parameter space
    if (paramSpace!=0)
        delete paramSpace;

    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline double
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::getError(const ParameterDomainType& mu) const {

    problem_.mobility().setParameter(mu);
    // compute reduced solution
    DofVectorType reducedSolution;
    reducedSimulation(reducedSolution, reducedAlgorithmVerbose_);

    // return error
    return residualEstimator_.evaluate(reducedSolution);
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::getErrorHighdim(const ParameterDomainType& mu,
                                                  double & highdimRuntime,
                                                  double& lowdimRuntime,
                                                  double& reconsTime,
			  double& trueError,
			  double& upperBound,
			  double& lowerBound) {
    boost::timer::cpu_timer timer;
    
    problem_.mobility().setParameter(mu);

    // compute reduced solution
    DofVectorType reducedSolution;
    timer.start();
    reducedSimulation(reducedSolution, reducedAlgorithmVerbose_);
    lowdimRuntime = timer.elapsed().wall/1e3;
    // reconstruct
    DofVectorType recons;
    timer.start();
    rbSpace_.project(reducedSolution, recons, localizer_);
    reconsTime = timer.elapsed().wall/1e6;

    // compute the highdimensional solution
    DofVectorType p;
    timer.start();
    detailedSimulation(p);
    highdimRuntime = timer.elapsed().wall/1e9;
    
    // compute true error
    ParameterDomainType muBak = problem_.mobility().getParameter();
    problem_.mobility().setParameter(muBar_);
    // calc exact error
    typename HighdimAlgorithmType::DiscreteOperatorType::EigenMatrixType& highdimOperator
      = algorithm_.getOperator().systemMatrixEigen();      
    double pEnergyNormSquared = p.transpose()*highdimOperator*p;
    assert(pEnergyNormSquared>=0.0);
    problem_.mobility().setParameter(muBak);
       
    // compute diff and store it in recons
    recons -= p;
    double squaredError = recons.transpose()*highdimOperator*recons;       
    assert(squaredError>=0.0);
    trueError = sqrt(squaredError);
//    upperBound = residualEstimator_.evaluate(reducedSolution)/sqrt(pEnergyNormSquared);
//    lowerBound = residualEstimator_.lowerBound(reducedSolution)/sqrt(pEnergyNormSquared);
    
    // compute the relative l2 error
    double exactErrorL2 = recons.norm()/p.norm();

    //==========================================================================
    if (Parameter::getValue<bool>("rbgeneration.getErrorHighdim.verbose", false))
        std::cout << "Exact Error: (l2) " << exactErrorL2
        << "  Estimated: " << lowerBound  << "<=" << trueError << "<=" << upperBound
        << std::endl;
    return;
}
    
    template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
    inline void
    RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::getErrorHighdim(const ParameterDomainType& mu,
                                                                                      double & highdimRuntime,
                                                                                      double& lowdimRuntime,
                                                                                      double& reconsTime,
                                                                                      double& trueError,
                                                                                      double& upperBound,
                                                                                      double& lowerBound,
                                                                                      const DofVectorType& p) {
        boost::timer::cpu_timer timer;
        
        problem_.mobility().setParameter(mu);
        
        // compute reduced solution
        DofVectorType reducedSolution;
        timer.start();
        reducedSimulation(reducedSolution, reducedAlgorithmVerbose_);
        lowdimRuntime = timer.elapsed().wall/1e3;
        // reconstruct
        DofVectorType recons;
        timer.start();
        rbSpace_.project(reducedSolution, recons, localizer_);
        reconsTime = timer.elapsed().wall/1e6;
              
        // compute true error
        ParameterDomainType muBak = problem_.mobility().getParameter();
        problem_.mobility().setParameter(muBar_);
        // calc exact error
        typename HighdimAlgorithmType::DiscreteOperatorType::EigenMatrixType& highdimOperator
        = algorithm_.getOperator().systemMatrixEigen();      
        double pEnergyNormSquared = p.transpose()*highdimOperator*p;
        assert(pEnergyNormSquared>=0.0);
        problem_.mobility().setParameter(muBak);
        
        // compute diff and store it in recons
        recons -= p;
        double squaredError = recons.transpose()*highdimOperator*recons;       
        assert(squaredError>=0.0);
        trueError = sqrt(squaredError/pEnergyNormSquared);       
        // compute the relative l2 error
        double exactErrorL2 = recons.norm()/p.norm();
        
        //==========================================================================
        if (Parameter::getValue<bool>("rbgeneration.getErrorHighdim.verbose", false))
            std::cout << "Exact Error: (l2) " << exactErrorL2
            << "  Estimated: " << lowerBound  << "<=" << trueError << "<=" << upperBound
            << std::endl;
        return;
    }



template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline double
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::updateErrorEstimatorFull() {
    // set up error estimator
    std::cout << "Update of error estimator: ";
    boost::timer::cpu_timer timer;
    residualEstimator_.updateFull();
    double time = timer.elapsed().wall/1e9;
    std::cout << "(Total " << time << "s)\n";
    
    return time;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline double
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::updateErrorEstimator() {
    // set up error estimator
    std::cout << "Update of error estimator: ";
    boost::timer::cpu_timer timer;
    residualEstimator_.update();
    double time = timer.elapsed().wall/1e9;
    std::cout << "(Total " << time << "s)\n";
    
    return time;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline double
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::updateReducedOperator() {
    boost::timer::cpu_timer timer;
    rbOp_.update(algorithm_.getOperator());
    double time = timer.elapsed().wall/1e9;
    if (verbose_)
        std::cout << "Update of reduced operator: " << time << "s\n";
    
    return time;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::printExtensionInfo(const ParameterDomainType& mu) const {
    std::cout << "Extended reduced basis to size " << rbSpace_.size()
              << " using mu = [ " << mu.transpose() << "]\n";
    return;
}

template <class ReducedSpaceImpl, class DetailedFunctionImp, class ProblemImpl>
inline void
RBGeneration<ReducedSpaceImpl, DetailedFunctionImp, ProblemImpl>::plotError(const bool printError) {

    // compute reduced solution
    DofVectorType reducedSolution;
    reducedSimulation(reducedSolution, printError);

    // reconstruct
    DofVectorType recons;
    rbSpace_.project(reducedSolution, recons, localizer_);

    
    // compute the highdimensional solution
    DofVectorType p;
    detailedSimulation(p);
    
    // compute difference
    DofVectorType diff;
    diff = p - recons;

    if (printError) {
        double estimatedError = residualEstimator_.evaluate(reducedSolution);
        
        ParameterDomainType muBak = problem_.mobility().getParameter();
        problem_.mobility().setParameter(muBar_);
        // calc exact error
        typename HighdimAlgorithmType::DiscreteOperatorType::EigenMatrixType& highdimOperator
            = algorithm_.getOperator().systemMatrixEigen();      
        double reconstructionNormSquared = recons.transpose()*highdimOperator*recons;
        assert(reconstructionNormSquared>=0.0);
        std::cout << "Norm of reconstruction: " << sqrt(reconstructionNormSquared) << "\n";
        problem_.mobility().setParameter(muBak);

        double squaredError = diff.transpose()*highdimOperator*diff;       
        assert(squaredError>=0.0);
        double trueError = sqrt(squaredError);

        // done
        
        
        std::cout << "Estimated error is: " << estimatedError
                  << "\nTrue error is: " << trueError << std::endl;
    }


    // plot lowdim
    plotFunction<DetailedFunctionType>(recons, algorithm_.space(), "Reconstruction");
    // plot highdim
    plotFunction<DetailedFunctionType>(p, algorithm_.space(), "Solution");
    // plot diff
    plotFunction<DetailedFunctionType>(diff, algorithm_.space(), "Difference");
    // plot the mobility function
    problem_.visualizeMobility(gridPartition_);
    
    std::cout << "Just plotted high-, lowdim solution, difference of the two and the mobility!\n";
//    pressAnyKey();
    return;
}

}                     // namespace RB
}                   // namespace Dune
