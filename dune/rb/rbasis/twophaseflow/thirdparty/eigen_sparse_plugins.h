// -------------------------------------------------------------------------
// This file will be included within the body of Eigen::SparseMatrixBase
// -------------------------------------------------------------------------
public:
/** @brief Write sparse matrix to text file with given name.
 *
 *  @param[in] filename The name of the file where the matrix shall be written
 */

void saveToTextFile(const std::string& filename) const {
    FILE* f= fopen(filename.c_str(),"wt");
	if (!f)
		throw std::runtime_error(std::string("saveToTextFile: Error opening file ")+filename+std::string("' for writing a matrix as text."));
    for (int k=0; k<derived().outerSize(); ++k)
        for (typename Derived::InnerIterator it(derived(),k); it; ++it) {
            fprintf(f,"%i,%i,%.16e\n",it.row(),it.col(),static_cast<double>(it.value()));
        }
    return;
}

/** @brief Read sparse matrix from file with given filename
 *
 *  @param[in] filename        The name of the file that is to be read
 *  @param[in] nonZerosPerCol  A lower bound for the number of non-zero elements per column
 */
void loadFromTextFile(const std::string& filename, const int nonZerosPerCol) {
    // remove all non-zeros from this matrix
    derived().setZero();
    // reserve enough memory
    derived().reserve(VectorXi::Constant(cols(), nonZerosPerCol));
    std::ifstream f(filename.c_str());
	if (f.fail()) 
        throw std::runtime_error(std::string("loadFromTextFile: can't open file:") + filename);
    std::string row;
    while (std::getline(f, row)) {
        size_t last_found = 0;
        size_t found = row.find_first_of(",");
        double temp;
        std::vector<double> entryLine;
        do
        {
            if(found == std::string::npos)
                found = row.size();
            std::string subs = row.substr(last_found, found-last_found);
            last_found = found+1;
            std::istringstream in( subs );
            if (entryLine.size()==2) {
                in.precision(16);
                in.setf(std::ios::scientific);
            }
            else {
                in.precision(10);
                in.setf(std::ios::fixed);
            }
            in >> temp;
            entryLine.push_back(temp);
            found = row.find_first_of(",", last_found);
        } while(last_found != row.size()+1);
        assert(entryLine.size()==3);
        derived().insert(entryLine[0],entryLine[1])=entryLine[2];
    }
}

/** Interpret a std::vector<double> as a vector of Eigen::Triplets.
 */
class MyTripletVectorIterator {
    typedef std::vector<double> VectorType;
    typedef MyTripletVectorIterator ThisType;

public:
    typedef Eigen::Triplet<double> value_type;
    
    MyTripletVectorIterator(VectorType::iterator position)
    : position_(position)
    {}
    
    ThisType& operator++() {
        position_+=3;
        return *this;
    }
    
    value_type operator*() {
        return value_type(*position_, *(position_+1), *(position_+2));
    }
    
    value_type* operator->() {
        element_=value_type(*position_, *(position_+1), *(position_+2));
        return &element_;
    }
    
    bool operator==(const ThisType& other) const {
        return this->position_==other.position_;
    }

    bool operator!=(const ThisType& other) const {
        return this->position_!=other.position_;
    }

private:  
    VectorType::iterator position_;
    value_type element_;
};

/** Write a given double vector to a given outstream.
 *
 * @note This supports binary outstreams.
 *
 * @param[out] out The outstream where the vector shall be written.
 * @param[in] vec  The vector of type vector<double>.
 * @returns Returns true if vector was written without error.
 */ 
bool writeUnformattedDoubleVector(std::ostream& out , const std::vector<double>& vec) const
{
    size_t vecSize = vec.size();
    
    out.write( reinterpret_cast<char*>(&vecSize) , sizeof(vecSize) );
    
    for (int i =0; i<vecSize; ++i)
    {
        out.write(reinterpret_cast<const char*>(&vec[i]) , sizeof(vec[i]));
    }
    
    return !out.bad();
}

/** Read a double vector from a given instream.
 *
 * @note This supports binary streams.
 *
 * @param[out] in The instream from which the vector shall be read.
 * @param[in] vec  The vector of type vector<double>.
 * @returns Returns true if vector was read without error.
 */
bool readUnformattedDoubleVector(std::istream & in , std::vector<double> & vec)
{
    vec.clear();
    
    size_t vecSize;
    
    in.read( reinterpret_cast<char*>(&vecSize) , sizeof(vecSize) );
    
    if (vecSize > 0)
    {
        double keyValue;
        
        vec.reserve(vecSize);
        for (int i=0; i<vecSize; ++i)
        {
            in.read(reinterpret_cast<char*>(&keyValue) , sizeof(keyValue));
            vec.push_back(keyValue);
        }
    }
    
    return !in.bad();
}


/** @brief Write sparse matrix to binary file with given name.
 *
 *  @param[in] filename The name of the file where the matrix shall be written
 */

void saveToBinaryFile(const std::string& filename) const {

    std::vector<double> data;
    data.reserve(this->nonZeros()*3);
    for (int k=0; k<derived().outerSize(); ++k)
        for (typename Derived::InnerIterator it(derived(),k); it; ++it) {
            data.push_back(double(it.row()));
            data.push_back(double(it.col()));
            data.push_back(static_cast<double>(it.value()));
        }

    // perform output to a binary file
    std::ofstream output(filename.c_str(), std::ios::binary);
    if (!output.is_open()) {
      std::cerr << "saveToBinaryFile: Error opening file '" << filename << "'.\n";
      abort();
    }
    output << std::setprecision(16);
    
    writeUnformattedDoubleVector(output, data);
    output.close();    
    
    return;
}

/** @brief Read sparse matrix from file with given filename
 *
 *  @param[in] filename        The name of the file that is to be read
 *  @param[in] nonZerosPerCol  A lower bound for the number of non-zero elements per column
 */
void loadFromBinaryFile(const std::string& filename) {
    std::vector<double> data;
    std::ifstream input(filename.c_str(), std::ios::binary);
    if (!input.is_open()) {
		std::cerr << "loadFromBinaryFile: Error opening file '" << filename << "'.\n";
        abort();
    }
    bool readWithoutError = readUnformattedDoubleVector(input, data);
    input.close();

    derived().setFromTriplets(MyTripletVectorIterator(data.begin()),MyTripletVectorIterator(data.end()));
    
    return;
}
