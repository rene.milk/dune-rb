// -------------------------------------------------------------------------
// This file will be included within the body of Eigen::MatrixBase
// -------------------------------------------------------------------------

private:
/** Write the data array to a given outstream.
 *
 * @note This supports binary outstreams.
 *
 * @param[out] out The outstream where the array shall be written.
 * @returns Returns true if array was written without error.
 */
bool writeUnformattedDoubleVector(std::ostream& out) const
{
  size_t myRows = derived().rows();
  size_t myCols = derived().cols();

  out.write(reinterpret_cast< char* >(&myRows), sizeof(myRows));
  out.write(reinterpret_cast< char* >(&myCols), sizeof(myCols));

  size_t vecSize = myRows * myCols;

  for (size_t i = 0; i < vecSize; ++i) {
    out.write(reinterpret_cast< const char* >(&derived().data()[i]), sizeof(derived().data()[i]));
  }

  return !out.bad();
} // writeUnformattedDoubleVector

/** Read a data array from a given instream.
 *
 * @note This supports binary streams.
 *
 * @param[out] in The instream from which the vector shall be read.
 * @returns Returns true if array was read without error.
 */
bool readUnformattedDoubleVector(std::istream& in)
{
  size_t myRows;
  size_t myCols;

  in.read(reinterpret_cast< char* >(&myRows), sizeof(myRows));
  in.read(reinterpret_cast< char* >(&myCols), sizeof(myCols));

  derived().resize(myRows, myCols);

  size_t vecSize = myRows * myCols;

  if (vecSize > 0) {
    Scalar keyValue;
    for (size_t i = 0; i < vecSize; ++i) {
      in.read(reinterpret_cast< char* >(&keyValue), sizeof(keyValue));
      derived().data()[i] = keyValue;
    }
  }

  return !in.bad();
} // readUnformattedDoubleVector

public:
/** @brief Write matrix to binary file with given name.
 *
 *  @param[in] filename The name of the file where the matrix shall be written
 */
bool saveToBinaryFile(const std::string& filename) const
{
  // perform output to a binary file
  std::ofstream output(filename.c_str(), std::ios::binary);

  if (!output.is_open()) {
    std::cerr << "saveToBinaryFile: Error opening file '" << filename << "'.\n";
    abort();
  }

  output << std::setprecision(16);
  bool writtenWithoutError = writeUnformattedDoubleVector(output);
  output.close();

  return writtenWithoutError;
} // saveToBinaryFile

/** @brief Read matrix from file with given filename
 *
 *  @param[in] filename        The name of the file that is to be read
 *  @param[in] nonZerosPerCol  A lower bound for the number of non-zero elements per column
 */
bool loadFromBinaryFile(const std::string& filename)
{
  std::ifstream input(filename.c_str(), std::ios::binary);

  if (!input.is_open()) {
    std::cerr << "loadFromBinaryFile: Error opening file '" << filename << "'.\n";
    abort();
  }
  bool readWithoutErrors = readUnformattedDoubleVector(input);
  input.close();
  return readWithoutErrors;
} // loadFromBinaryFile


/** @brief Write matrix to text file with given name.
 *
 *  @param[in] filename The name of the file where the matrix shall be written
 */
bool saveToTextFile(const std::string& filename) const
{
  bool          saved = true;
  std::ofstream f(filename.c_str());

  if (!f) {
    std::cerr << "saveToTextFile: Error opening file " << filename << " for writing a matrix as text!\n";
    saved = false;
  } else {
    f << derived().rows() << "," << derived().cols() << std::endl;

    for (int k = 0; k < derived().rows(); ++k)
      for (int l = 0; l < derived().cols(); ++l) {
        f << k << "," << l << ",";
        if (boost::is_same< double, Scalar >::value) {
          f.precision(16);
          f.setf(std::ios::scientific);
        }
        f << derived().coeff(k, l) << std::endl;
      }

  }
  return saved;
} // saveToTextFile

/** @brief Read matrix from file with given filename
 *
 *  @param[in] filename        The name of the file that is to be read
 */
bool loadFromTextFile(const std::string& filename)
{
  bool          loaded = true;
  std::ifstream f(filename.c_str());

  if (f.fail()) {
    std::cerr << "loadFromTextFile: can't open file:" << filename << std::endl;
    loaded = false;
  } else {
    std::string row;
    // set correct matrix size
    {
      std::getline(f, row);
      size_t                last_found = 0;
      size_t                found      = row.find_first_of(",");
      int                   temp;
      std::vector< double > entryLine;
      do {
        if (found == std::string::npos)
          found = row.size();
        std::string subs = row.substr(last_found, found - last_found);
        last_found = found + 1;
        std::istringstream in(subs);
        in.precision(10);
        in.setf(std::ios::fixed);
        in >> temp;
        entryLine.push_back(temp);
        found = row.find_first_of(",", last_found);
      } while (last_found != row.size() + 1);
      loaded = (loaded && (entryLine.size() == 2));
      derived().resize(entryLine[0], entryLine[1]);
    }

    while (std::getline(f, row)) {
      size_t                last_found = 0;
      size_t                found      = row.find_first_of(",");
      int                rowcol;
      Scalar value;
      std::vector< int > entryLine;
      do {
        if (found == std::string::npos)
          found = row.size();
        std::string subs = row.substr(last_found, found - last_found);
        last_found = found + 1;
        std::istringstream in(subs);
        if (entryLine.size() == 2) {
          if (boost::is_same<double, Scalar>::value) {
            in.precision(16);
            in.setf(std::ios::scientific);
          }
          in >> value;
          entryLine.push_back(1);
        } else {
          in.precision(10);
          in.setf(std::ios::fixed);
          in >> rowcol;
          entryLine.push_back(rowcol);
        }
        found = row.find_first_of(",", last_found);
      } while (last_found != row.size() + 1);
      loaded                                 = (loaded && (entryLine.size() == 3));
      derived() (entryLine[0], entryLine[1]) = value;
    }
  }
  return loaded;
}   // loadFromTextFile