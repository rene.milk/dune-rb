#include <config.h>

#include "../reducedbasisspace.hh"
#include "../constrainedrbspace.hh"

// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>

// multidomaingrid
#include <dune/grid/multidomaingrid.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>

using namespace Dune;
using namespace RB;

#define RB_CONSTRAINED

int main(int argc, char *argv[])
{
  try {
    typedef GridSelector::GridType                                         GridType;
    typedef Dune::mdgrid::FewSubDomainsTraits<GridType::dimension,4>       MDGridTraits;
    typedef MultiDomainGrid<GridType, MDGridTraits>                        MDGridType;
#ifdef RB_CONSTRAINED
    typedef LeafGridPart<MDGridType>                                       GridPart;
#else
    typedef LeafGridPart<GridType>                                         GridPart;
#endif
    typedef FunctionSpace<double, double, 2, 1>                            FunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpace, GridPart, 1>      DiscFuncSpace;
    typedef AdaptiveDiscreteFunction<DiscFuncSpace>                        DiscreteFunction;
    typedef ConstrainedDiscreteFunction<DiscreteFunction, MDGridType, int> ConstrainedDF;
#ifdef RB_CONSTRAINED
    typedef ConstrainedRBSpace<ConstrainedDF>                              RBSpace;
#else
    typedef ReducedBasisSpace<DiscreteFunction>                            RBSpace;
#endif
    typedef AdaptiveDiscreteFunction<RBSpace>                              ReducedFunction;

    MPIManager::initialize(argc, argv);
    Parameter::append("constrainedtest.params");

    GridPtr<GridType> gridPtr("unitcube2.dgf");
    GridType &grid = *gridPtr;
#ifdef RB_CONSTRAINED
    MDGridType mdGrid(grid);
    GridPart gridPart(mdGrid);
#else
    GridPart gridPart(grid);
#endif
    DiscFuncSpace discFuncSpace(gridPart);

    RBSpace rbSpace(discFuncSpace);
    ReducedFunction reduced("red", rbSpace);


  } catch (Dune::Exception e) {
    std::cerr << e.what();
  }  
  return 0;
}
