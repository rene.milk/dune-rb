#ifndef __CONSTRAINEDRBSPACE_HH__
#define __CONSTRAINEDRBSPACE_HH__

#include "basefunctionset.hh"
#include "mapper.hh"
#include "commdatahandle.hh"


// rb stuff
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>

#include <Eigen/Core>

#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

namespace Dune
{
    
    namespace RB
    {
        
        template< class DetailedSpaceImp >
        class ConstrainedRBSpace;
        
        template< class DetailedSpaceImp >
        class ConstrainedRBSpaceTraits
        {           
        private:
            typedef ConstrainedRBSpaceTraits< DetailedSpaceImp > ThisType;
            
        public:
            typedef DetailedSpaceImp DiscreteFunctionSpaceType;
            typedef typename DiscreteFunctionSpaceType :: DomainType                   DomainType;
            typedef typename DiscreteFunctionSpaceType :: RangeType                    RangeType;
            typedef typename DiscreteFunctionSpaceType :: JacobianRangeType            JacobianRangeType;
            typedef typename DiscreteFunctionSpaceType :: DomainFieldType              DomainFieldType;
            typedef typename DiscreteFunctionSpaceType :: RangeFieldType               RangeFieldType;
            typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType            FunctionSpaceType;
            typedef typename DiscreteFunctionSpaceType :: GridPartType                 GridPartType;
            typedef typename DiscreteFunctionSpaceType :: GridType                     GridType;
            typedef GridPartType                                                   GridPartition;
            typedef int                                                            MacroElement;
            typedef typename DiscreteFunctionSpaceType :: IndexSetType                 IndexSetType;
            typedef typename DiscreteFunctionSpaceType :: IteratorType                 IteratorType;
            typedef Eigen::MatrixXd                                                EigenMatrixType;
            typedef Eigen::VectorXd                                                DofVectorType;
            typedef EigenMatrixType                                                SubdomainBasisType;
            typedef std::vector<EigenMatrixType>                                   EigenBaseFunctionListVectorType;

            typedef RBFem::Space::Mapper<GridPartType, EigenBaseFunctionListVectorType>       MapperType;
            typedef MapperType BlockMapperType;
            
            //      typedef GramianPipeline<ConstrainedList, MatrixType>                   GramianPipelineType;
            
            enum { localBlockSize = 1 };
            
            template< class DiscreteFunction,
            class Operation = DFCommunicationOperation :: Add >
            struct CommDataHandle
            {
                // This is just a phony data handle doing nothing
          typedef RBFem::Space::CommDataHandle< DiscreteFunction, Operation > Type;
                typedef Operation OperationType;
            };
        };
        
        
        
        /** \class   ConstrainedRBSpace
         *  \ingroup RBSpace
         *  \brief   provides the space for reduced basis simulations
         *
         *  The basis consists of discrete functions as basis functions. These
         *  discrete functions have an underlying arbitrary space. Consequently they
         *  inhert most of the structure from this space.
         *
         *  Initially the space is empty and by using the add function you can build this
         *  space and discrete functions.
         *
         *  \param  BaseFunction  type of discrete function used to represend the
         *                        base functions
         */
        template<class DiscreteSpaceImp>
        class ConstrainedRBSpace
        {
        public:
            //! type of the traits
            typedef ConstrainedRBSpaceTraits< DiscreteSpaceImp > Traits;
            
        private:
            typedef ConstrainedRBSpace< DiscreteSpaceImp > ThisType;
            
        public:
            //! discrete function space, the base functions belong to
            typedef typename Traits :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
            //! function space, the base functions belong to
            typedef typename Traits :: FunctionSpaceType      FunctionSpaceType;
            typedef typename Traits :: GridPartType           GridPartType;
            typedef typename Traits :: GridType               GridType;
            typedef typename Traits :: IndexSetType           IndexSetType;
            //! type of the space's iterator
            typedef typename Traits :: IteratorType           IteratorType;
            typedef typename Traits :: EigenBaseFunctionListVectorType EigenBaseFunctionListVectorType;
            typedef typename Traits::EigenMatrixType EigenMatrixType;
            typedef typename Traits::SubdomainBasisType SubdomainBasisType;
            //! Type of the grid partition
            typedef typename Traits :: GridPartition          GridPartition;
            //! Type of one macro element
            typedef typename Traits :: MacroElement           MacroElement;
            //! type of the DoF mapper
            typedef typename Traits :: MapperType             MapperType;
            enum { localBlockSize = Traits :: localBlockSize };
            
            //! type of the block mapper
            typedef typename Traits :: BlockMapperType        BlockMapperType;
            
            enum { polynomialOrder = DiscreteFunctionSpaceType :: polynomialOrder };
            
            typedef Eigen::VectorXd DofVectorType;
            
        protected:
            DiscreteFunctionSpaceType &baseFunctionSpace_;
            //    const GridPartType &gridPart_;
            EigenBaseFunctionListVectorType eigenBaseFunctionLists_;
            mutable MapperType mapper_;
            mutable BlockMapperType blockMapper_;
            
        public:
            //! default communication interface
            static const InterfaceType defaultInterface = InteriorBorder_All_Interface;
            
            //! default communication direction
            static const CommunicationDirection defaultDirection =  ForwardCommunication;
            
            /** \brief constructor
             *
             *  \param[in]  baseFunctionSpace  DiscreteFunctionSpace containing the
             *                                 base functions belong to
             *  \param[in]  commInterface      communication interface
             *  \param[in]  commDirection      communication direction
             */
            inline explicit
            ConstrainedRBSpace ( DiscreteFunctionSpaceType &baseFunctionSpace,
                                const InterfaceType commInterface = defaultInterface,
                                const CommunicationDirection commDirection = defaultDirection );
            
            /** \brief constructor
             *
             *  \param[in]  baseFunctionSpace  DiscreteFunctionSpace containing the
             *                                 base functions belong to
             *  \param[in]  listName           the name (and path) to a list containing
             *                                 precomputed base functions
             *  \param[in]  commInterface      communication interface
             *  \param[in]  commDirection      communication direction
             */
            inline explicit
            ConstrainedRBSpace ( DiscreteFunctionSpaceType &baseFunctionSpace,
                                const std::vector<std::string> &headerNames,
                                const std::vector<std::string> &dataPathes,
                                const InterfaceType commInterface = defaultInterface,
                                const CommunicationDirection commDirection = defaultDirection );
            
            //    /** \brief constructor reading the base functions from a stream
            //     *
            //     *  \param[in]  baseFunctionSpace  DiscreteFunctionSpace containing the
            //     *                                 base functions belong to
            //     *  \param[in]  in                 stream to read the base functions from
            //     *  \param[in]  commInterface      communication interface
            //     *  \param[in]  commDirection      communication direction
            //     */
            //    template< class StreamTraits >
            //    inline
            //    ConstrainedRBSpace ( DiscreteFunctionSpaceType &baseFunctionSpace,
            //                        InStreamInterface< StreamTraits > &in ,
            //                        const InterfaceType commInterface = defaultInterface ,
            //                        const CommunicationDirection commDirection = defaultDirection );
            
            inline ~ConstrainedRBSpace ();
            
            
            
            inline int size() const;
            
            inline const GridPartType& gridPart() const;
            
            // Implementation of DiscreteFunctionSpaceInterface
            // ------------------------------------------------
            
            //    /* \copydoc Dune::DiscreteFunctionSpaceInterface::continuous() const */
            //    inline bool continuous () const
            //    {
            //      std::cout << "Someone asks, if I am continuous!\n";
            //      return false;
            //      // return baseFunctionSpace_.continuous();
            //    }
            //
            //    /* \copydoc Dune::DiscreteFunctionSpaceInterface::order() const */
            //    inline int order () const
            //    {
            //      return baseFunctionSpace_.order();
            //    }
            //
            //    /* \copydoc Dune::DiscreteFunctionSpaceInterface::baseFunctionSet(const EntityType &entity) const */
            //    template< class EntityType >
            //    inline const BaseFunctionSetType baseFunctionSet( const EntityType &entity ) const
            //    {
            //      const int sD = BaseType::gridPart_.getSubDomain(entity);
            //      return BaseFunctionSetType( baseFunctionLists_[sD], entity );
            //    }
            
            //    //! get dimension of value
            //    inline int dimensionOfValue () const
            //    {
            //      return baseFunctionSpace_.dimensionOfValue;
            //    }
            
            /* \copydoc Dune::DiscreteFunctionSpaceInterface::mapper() const */
            inline MapperType &mapper () const
            {
                return mapper_;
            }
            
            /* \copydoc Dune::DiscreteFunctionSpaceInterface::blockMapper() const */
            inline BlockMapperType &blockMapper () const
            {
                return blockMapper_;
            }
            
            /* \copydoc Dune::DiscreteFunctionSpaceInterface::multipleBaseFunctionSets () const */
            inline bool multipleBaseFunctionSets () const
            {
                return true;
            }
            
            // ConstrainedRBSpace Specific Methods
            // ----------------------------------
            
            
            //    inline ConstrainedList& getFuncList(const MacroElement &i) {
            //      assert(i<int(BaseType::gridPart_.numSubDomains()));
            //      return baseFunctionLists_[i];
            //    }
            //
            //    inline const ConstrainedList& getFuncList(const MacroElement &i) const {
            //      assert(i<BaseType::gridPart_.numSubDomains());
            //      return baseFunctionLists_[i];
            //    }
            //
            //    inline const ConstrainedList* getFuncListPtr(const MacroElement &i) const {
            //      assert(i<BaseType::gridPart_.numSubDomains());
            //      return &(baseFunctionLists_[i]);
            //    }
            //    
            //
            //    inline void setFuncList(const MacroElement &i, const ConstrainedList& other) {
            //      assert(i<int(BaseType::gridPart_.numSubDomains()));
            //      //      delete baseFunctionLists_[i];
            //      //      baseFunctionLists_[i] = new ConstrainedList(other);
            //      baseFunctionLists_[i].clear();
            //      baseFunctionLists_[i] = other;
            //      return;
            //    }
            
            
            /** Get the basis for one subdomain as Eigen::Matrix, const version.
             *
             *  Each column of the returned matrix refers to one base function.
             *
             * @param[in] i The number of the subdomain
             * @return      Returns the basis of the given subdomain as Eigen::Matrix
             */
            const SubdomainBasisType& getSubdomainBasis(const MacroElement& i) const {
                assert(i>=0 && i<eigenBaseFunctionLists_.size() && "Invalid subdomain given!");
                
                return eigenBaseFunctionLists_[i];
            }
            
            /** Get the basis for one subdomain as Eigen::Matrix, non-const version.
             *
             *  Each column of the returned matrix refers to one base function.
             *
             * @param[in] i The number of the subdomain
             * @return      Returns the basis of the given subdomain as Eigen::Matrix
             */
            SubdomainBasisType& getSubdomainBasis(const MacroElement& i) {
                assert(i>=0 && i<eigenBaseFunctionLists_.size() && "Invalid subdomain given!");
                
                return eigenBaseFunctionLists_[i];
            }
            
            /** \brief add a base function to the reduced basis space
             *
             *  \note After adding the base function, you can safely remove or
             *        overwrite it. The ConstrainedRBSpace saves a copy.
             *
             *  \param[in]  baseFunction  base function to add to the reduced basis
             *                            space
             *  \param[in]  subDomain     the subDomain where the base function has its support
             */
            inline void addBaseFunction ( const DofVectorType &baseFunction, const int element);
            
            /** \brief access a base function within the reduced basis space
             *
             *  \param[in]  i    number of the base function to access
             *  \param[in]  subDomain     the subDomain where the base function has its support
             *  \param[out] ret  the returned base function
             *
             *  \returns a constant reference to the i-th base function
             */
            inline void baseFunction ( unsigned int i, const MacroElement &subDomain,
                                      DofVectorType &ret ) const;
            
            /** @brief overwrite the i'th base function
             *
             *  @param[in] i   integer indicating the base function that is to be overwritten
             *  @param[in] subDomain the subDomain where the base function has its support
             *  @param[in] arg the function that is to be stored
             */
            inline void setBaseFunction (unsigned int i, const MacroElement &subDomain,
                                         DofVectorType &arg);
            
            /** @brief return the underlying function space for the base functions*/
            inline const DiscreteFunctionSpaceType &baseFunctionSpace () const;
            
            /** \brief obtain number of base functions within the reduced basis space */
            inline unsigned int numBaseFunctions () const;
            
            inline void saveBaseFunctions(const std::string& directoryName) const;
            
            inline void readBaseFunctions(const std::string& directoryName);
            
            /** \brief project a discrete function over this space to the discrete
             *         function space of the base functions, i.e. projection from
             *         \f$\mathcal{W}^{N}\f$ to \f$\mathcal{W}^{H}\f$.
             *
             *  \note This method expects the source discrete function to implement the
             *        dof method (which is not part of the DiscreteFunctionInterface).
             *
             *  \param[in]   sourceFunction  discrete function to be projected
             *  \param[out]  destFunction    discrete function to receive the projected
             *                               function
             */
            template< class DestinationFunctionType >
            inline void project ( const Eigen::VectorXd& sourceFunction,
                                 DestinationFunctionType& destFunction ) const;
            
            
            template< class LocalizerType >
            inline void project (const Eigen::VectorXd& sourceFunction,
                                 Eigen::VectorXd& destFunction,
                                 const LocalizerType& localizer) const;
            
            /** \brief restrict a discrete function over the base space to the discrete
             *         function space of this base functions, i.e. projection from
             *         \f$\mathcal{W}^{H}\f$ to \f$\mathcal{W}^{N}\f$.
             *
             *
             *  \note This method expects the source discrete function to be a DiscreteFunction in the baseFunctionSpace
             *
             *  \param[in]   sourceFunction  discrete function to be restricted
             *  \param[out]  destFunction    discrete function to receive the restricted
             *                               function
             */
            //     template< class DiscreteFunctionType >
            //     inline void restrictFunction ( const BaseFunctionType &sourceFunction,
            //                                    DiscreteFunctionType &destFunction ) const;
            
            /**
             * \brief restrict a continuous function to the reduced space
             *
             * Let \f$ f \f$ be the (continuous) source function and \f$ g\f$ be the
             * (discrete) destination function.
             * If \f$ varphi_i\f$ then denotes the i'th base function, this algorithm performs
             * \f[
             *    g_i = \int_\Omega f(x) \cdot \varphi_i(x) \mathrm{d}x
             * \f]
             *
             * \param[in]  sourceFunction the function that is to be projected
             * \param[out] destFunction   the restricted function in the reduced space
             *
             */       
            template< class SourceFunctionType, class DiscreteFunctionType >
            inline void restrictFunction( const SourceFunctionType &sourceFunction,
                                         DiscreteFunctionType &destFunction ) const;
            
            /** \brief restrict a bilinearform which operates in the high dimensional space to a lower dimensional matrix which
             *         operates in the reducedbasisspace, i.e. projection from
             *         \f$\mbox{Lin}\left(\mathcal{W}^{H},\mathcal{W}^{H}\right)\f$ to
             *         \f$\mbox{Lin}\left(\mathcal{W}^{N},\mathcal{W}^{N}\right)\f$.
             *
             *
             *  \note This method expects the matrix of the FEM simulation
             *
             *  \param[in]   matrixOffline  matrix which corresponds the bilinearform
             *                              in the lagrange space
             *  \param[out]  matrixOnline   projected matrix to the lower dimensional
             *                              RB space
             */
            template< class MatrixOfflineType , class MatrixOnlineType >
            inline void restrictMatrix ( const MatrixOfflineType &matrixOffline,
                                        MatrixOnlineType &matrixOnline ) const;
            
            template< class OperatorComponentType , class MatrixOnlineType >
            inline void restrictOperator ( const OperatorComponentType &opOffline,
                                          MatrixOnlineType &matrixOnline ) const;
            
            
            template< class OperatorComponentType,
            class FuncComponentType,
            class DiscreteFunctionType >
            inline void restrictOperatorFunc ( const OperatorComponentType &opOffline,
                                              const FuncComponentType &func,
                                              DiscreteFunctionType &vectorOnline ) const;
            
            template< class OperatorComponentType1,
            class OperatorComponentType2,
            class MatrixOnlineType >
            inline void restrictTwoOperators ( const OperatorComponentType1 &opOffline1,
                                              const OperatorComponentType2 &opOffline2,
                                              MatrixOnlineType &matrixOnline ) const;
            
            
            template< class FuncComponentType1, class FuncComponentType2 >
            inline void l2ScalarProduct ( const FuncComponentType1 &func1,
                                         const FuncComponentType2 &func2,
                                         double &value ) const;
        };
        
    } // namespace RB
    
} // namespace Dune

#include "constrainedrbspace.cc"

#endif /* __CONSTRAINEDRBSPACE_HH__ */

