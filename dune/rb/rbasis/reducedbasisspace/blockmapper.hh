#ifndef  __RBSPACE_BLOCKMAPPER_HH__
#define  __RBSPACE_BLOCKMAPPER_HH__

#include <dune/fem/space/mapper/dofmapper.hh>
#include <algorithm>

namespace Dune
{

namespace RB
{

  template< class GridPartImp, class BaseFunctionListImp >
  class ReducedBasisBlockMapper;



  template< class GridPartImp, class BaseFunctionListImp >
  struct ReducedBasisBlockMapperTraits
  {
    typedef GridPartImp GridPartType;
    
    typedef typename GridPartType :: template Codim< 0 > :: IteratorType :: Entity
      EntityType;
    
    typedef BaseFunctionListImp BaseFunctionListType;

    typedef ReducedBasisBlockMapper< GridPartType, BaseFunctionListType >
      DofMapperType;
    
    typedef DefaultDofMapIterator< EntityType, DofMapperType >
      DofMapIteratorType;
  };



  /** \class ReducedBasisMapper
   *  \brief provides the mapper for the reduced basis space 
   *
   *  This mapper just performs the identity mapping.
   */
  template< class GridPartImp, class BaseFunctionListImp >
  class ReducedBasisBlockMapper
  : public DofMapperDefault
    < ReducedBasisBlockMapperTraits< GridPartImp, BaseFunctionListImp > >
  {
  public:
    typedef ReducedBasisBlockMapperTraits< GridPartImp, BaseFunctionListImp >
      Traits;
   
    typedef typename Traits :: BaseFunctionListType BaseFunctionListType;    

    typedef typename Traits :: GridPartType GridPartType;
    typedef typename Traits :: EntityType EntityType;
    typedef typename Traits :: DofMapIteratorType DofMapIteratorType;

  private:
    typedef ReducedBasisBlockMapper< GridPartType, BaseFunctionListType > ThisType;
    typedef DofMapperDefault< Traits > BaseType;

  protected:
    const BaseFunctionListType &baseFunctionLists_;
    const GridPartType         &gridPart_;

  public:
    inline explicit ReducedBasisBlockMapper(const BaseFunctionListType &baseFunctionLists,
                                       const GridPartType &gridPart)
      : baseFunctionLists_(baseFunctionLists),
        gridPart_(gridPart)
    {}

    /* \copydoc Dune::DofMapper::begin(const EntityType &entity) const */
    inline DofMapIteratorType begin ( const EntityType &entity ) const
    {
      return DofMapIteratorType
        ( DofMapIteratorType :: beginIterator, entity, *this );
    }
    
    /* \copydoc Dune::DofMapper::end(const EntityType &entity) const */
    inline DofMapIteratorType end ( const EntityType &entity ) const
    {
      return DofMapIteratorType
        ( DofMapIteratorType :: endIterator, entity, *this );
    }

    /* \copydoc Dune::DofMapper::mapToGlobal(const EntityType &entity,const int localDof) const */
    inline int mapToGlobal ( const EntityType &entity, const int localDof ) const
    {
      assert( localDof < maxNumDofs() );
      // find the coarse cell where this entity lives
      int sD = gridPart_.getSubDomain(entity);
      return sD;
    }

    /* \copydoc Dune::DofMapper::mapEntityDofToGlobal(const Entity &entity,const int localDof) const */
    template< class Entity >
    inline int mapEntityDofToGlobal ( const Entity &entity, const int localDof ) const
    {
      DUNE_THROW( NotImplemented, "ReducedBasisSpace cannot map entity DoFs." );
      return 0;
    }

    /* \copydoc Dune::DofMapper::maxNumDofs() const */
    inline int maxNumDofs () const
    {
      int maxSize = 0;
      for (int i=0; i!=baseFunctionLists_.size(); ++i) {
        maxSize = std::max(int(baseFunctionLists_[i].size()), maxSize);
      }
      return maxSize;
    }
    
//    inline int numDofs(const EntityType &entity) const {
//      // find the coarse cell where this entity lives
//      int sD = gridPart_.getSubDomain(entity);
//      return baseFunctionLists_[sD].size();
//    }

    /* \copydoc Dune::DofMapper::numEntityDofs(const Entity &entity) const */
    template< class Entity >
    inline int numEntityDofs ( const Entity &entity ) const
    {
      return 1;
    }
   
    /* \copydoc Dune::DofMapper::consecutive() const */
    bool consecutive () const
    {
      return false;
    }

    /* \copydoc Dune::DofMapper::newIndex(const int hole,const int block) const */
    int newIndex ( const int hole, const int block ) const
    {
      return -1;
    }

    /* \copydoc Dune::DofMapper::newSize() const */
    int newSize () const
    {
      return size();
    }

    /* \copydoc Dune::DofMapper::numberOfHoles(const int block) const */
    int numberOfHoles ( const int block ) const
    {
      return 0;
    }

    /* \copydoc Dune::DofMapper::oldIndex(const int hole,const int block) const */
    int oldIndex ( const int hole, const int block ) const
    {
      return -1;
    }

    /* \copydoc Dune::DofMapper::size() const */
    int size () const
    {
      return baseFunctionLists_.size();
    }
  };

} // end of namespace Dune::RB

} // end of namespace Dune

#endif //  __RBSPACE_BLOCKMAPPER_HH__
