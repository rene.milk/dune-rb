#ifndef DUNE_FEM_REDUCEDBASISSPACE_MAPPER_HH
#define DUNE_FEM_REDUCEDBASISSPACE_MAPPER_HH

#include <dune/fem/space/mapper/dofmapper.hh>

namespace Dune
{
namespace RBFem
{
namespace Space
{


  template< class GridPartImp, class BaseFunctionListImp >
  class Mapper;



  template< class GridPartImp, class BaseFunctionListImp >
  struct MapperTraits
  {
    typedef GridPartImp GridPartType;
    
    typedef typename GridPartType :: template Codim< 0 > :: IteratorType :: Entity
      EntityType;
    
    typedef BaseFunctionListImp BaseFunctionListType;

    typedef Mapper< GridPartType, BaseFunctionListType >
      DofMapperType;
    
    typedef Dune::DefaultDofMapIterator< EntityType, DofMapperType >
      DofMapIteratorType;
  };



  /** \class ReducedBasisMapper
   *  \brief provides the mapper for the reduced basis space 
   *
   *  This mapper just performs the identity mapping.
   */
  template< class GridPartImp, class BaseFunctionListImp >
  class Mapper
  : public Dune::DofMapperDefault
    < MapperTraits< GridPartImp, BaseFunctionListImp > >
  {
  public:
    typedef MapperTraits< GridPartImp, BaseFunctionListImp >
      Traits;
   
    typedef typename Traits :: BaseFunctionListType BaseFunctionListType;
    
    typedef typename Traits :: GridPartType GridPartType;
    typedef typename Traits :: EntityType EntityType;
    typedef typename Traits :: DofMapIteratorType DofMapIteratorType;

  private:
    typedef Mapper< GridPartType, BaseFunctionListType > ThisType;
    typedef DofMapperDefault< Traits > BaseType;

  protected:
      const BaseFunctionListType &baseFunctionLists_;
      const GridPartType gridPart_;

  public:
    inline explicit Mapper(const BaseFunctionListType &baseFunctionLists,
                                       const GridPartType &gridPart)
      : baseFunctionLists_(baseFunctionLists),
        gridPart_(gridPart)
    {}

    /* \copydoc Dune::DofMapper::begin(const EntityType &entity) const */
    inline DofMapIteratorType begin ( const EntityType &entity ) const
    {
      return DofMapIteratorType
        ( DofMapIteratorType :: beginIterator, entity, *this );
    }
    
    /* \copydoc Dune::DofMapper::end(const EntityType &entity) const */
    inline DofMapIteratorType end ( const EntityType &entity ) const
    {
      return DofMapIteratorType
        ( DofMapIteratorType :: endIterator, entity, *this );
    }

    /* \copydoc Dune::DofMapper::mapToGlobal(const EntityType &entity,const int localDof) const */
    inline int mapToGlobal ( const EntityType &entity, const int localDof ) const
    {
      // find the coarse cell where this entity lives
      int sD = gridPart_.getSubDomain(entity);
      int globalDof = 0;
      for (int i=0; i<sD; ++i)
        globalDof += baseFunctionLists_[i].cols();
      globalDof += localDof;
      return globalDof;
    }

    /* \copydoc Dune::DofMapper::mapEntityDofToGlobal(const Entity &entity,const int localDof) const */
    template< class Entity >
    inline int mapEntityDofToGlobal ( const Entity &entity, const int localDof ) const
    {
      DUNE_THROW( NotImplemented, "ReducedBasisSpace cannot map entity DoFs." );
      return 0;
    }

    /* \copydoc Dune::DofMapper::maxNumDofs() const */
    inline int maxNumDofs () const
    {
      return size();
    }

    inline int numDofs(const EntityType &entity) const {
      // find the coarse cell where this entity lives
      int sD = gridPart_.getSubDomain(entity);
      return baseFunctionLists_[sD].cols();
    }

    /* \copydoc Dune::DofMapper::numEntityDofs(const Entity &entity) const */
    template< class Entity >
    inline int numEntityDofs ( const Entity &entity ) const
    {
      DUNE_THROW( NotImplemented, "ReducedBasisSpace cannot map entity DoFs." );
      return 0;
    }
   
    /* \copydoc Dune::DofMapper::consecutive() const */
    bool consecutive () const
    {
      return false;
    }

    /* \copydoc Dune::DofMapper::newIndex(const int hole,const int block) const */
    int newIndex ( const int hole, const int block ) const
    {
      return -1;
    }

    /* \copydoc Dune::DofMapper::newSize() const */
    int newSize () const
    {
      return size();
    }

    /* \copydoc Dune::DofMapper::numberOfHoles(const int block) const */
    int numberOfHoles ( const int block ) const
    {
      return 0;
    }

    /* \copydoc Dune::DofMapper::oldIndex(const int hole,const int block) const */
    int oldIndex ( const int hole, const int block ) const
    {
      return -1;
    }

    /* \copydoc Dune::DofMapper::size() const */
    int size () const
    {
      int size=0;
      for (unsigned int i = 0; i<baseFunctionLists_.size(); ++i) {
        size+=baseFunctionLists_[i].cols(); 
      }
      return size;
    }
  };
}  // namespace Space
} // end of namespace RBFem
} // end of namespace Dune

#endif
