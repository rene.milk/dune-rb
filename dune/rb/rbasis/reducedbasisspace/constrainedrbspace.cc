#ifndef __CONSTRAINEDRBSPACE_INLINE_HH__
#define __CONSTRAINEDRBSPACE_INLINE_HH__

#include "constrainedrbspace.hh"


namespace Dune
{

namespace RB
{

    // ConstrainedRBSpace
    // -----------------
    
    template< class DiscreteSpaceImp >
    inline ConstrainedRBSpace< DiscreteSpaceImp >
    :: ConstrainedRBSpace ( ConstrainedRBSpace<DiscreteSpaceImp>::DiscreteFunctionSpaceType &baseFunctionSpace,
                           const InterfaceType commInterface,
                           const CommunicationDirection commDirection)
    : baseFunctionSpace_( baseFunctionSpace ),
    mapper_( eigenBaseFunctionLists_, baseFunctionSpace.gridPart() ),
    blockMapper_( eigenBaseFunctionLists_, baseFunctionSpace.gridPart() )
    {
        int numDofsPerFineCell = baseFunctionSpace.mapper().maxNumDofs();
        for (unsigned int i=0; i!=baseFunctionSpace_.gridPart().numSubdomains(); ++i) {
            eigenBaseFunctionLists_.push_back(Eigen::MatrixXd(baseFunctionSpace.gridPart().enclosedFineCells(i)*numDofsPerFineCell,0));
        }
    }
    
    // ConstrainedRBSpace
    // -----------------
    
    template< class DiscreteSpaceImp >
    inline ConstrainedRBSpace< DiscreteSpaceImp >
    :: ConstrainedRBSpace ( ConstrainedRBSpace<DiscreteSpaceImp>::DiscreteFunctionSpaceType &baseFunctionSpace,
                           const std::vector<std::string>& dataPathes,
                           const std::vector<std::string>& headerNames,
                           const InterfaceType commInterface,
                           const CommunicationDirection commDirection)
    : baseFunctionSpace_( baseFunctionSpace ),
    mapper_( eigenBaseFunctionLists_, baseFunctionSpace.gridPart() ),
    blockMapper_( eigenBaseFunctionLists_, baseFunctionSpace.gridPart() )
    {
        assert(headerNames.size()==dataPathes.size());
        assert(dataPathes.size()==gridPart().numSubDomains());
        
        int numDofsPerFineCell = baseFunctionSpace.mapper().maxNumDofs();
        
        for (unsigned int i=0; i!=baseFunctionSpace_.gridPart().numSubDomains(); ++i) {
            eigenBaseFunctionLists_.push_back(Eigen::MatrixXd(baseFunctionSpace.gridPart().enclosedFineCells(i)*numDofsPerFineCell,0));
        }
    }

    template< class DiscreteSpaceImp >
    inline ConstrainedRBSpace< DiscreteSpaceImp > :: ~ConstrainedRBSpace ()
    {}

    template< class DiscreteSpaceImp >
    inline int ConstrainedRBSpace< DiscreteSpaceImp >
    :: size() const {
        return mapper_.size();
    }
    

    
    
    template< class DiscreteSpaceImp >
    inline const typename ConstrainedRBSpace<DiscreteSpaceImp>::GridPartType& ConstrainedRBSpace< DiscreteSpaceImp >
    :: gridPart() const {
        return baseFunctionSpace_.gridPart();
    }
    
    template< class DiscreteSpaceImp >
    inline void ConstrainedRBSpace< DiscreteSpaceImp >
    :: addBaseFunction (const DofVectorType& baseFunction, const int element)
    {
        // resize the subdomain basis matrix
        const int oldSize = eigenBaseFunctionLists_[element].cols();
        eigenBaseFunctionLists_[element].conservativeResize(Eigen::NoChange, oldSize+1);
        // set new column to new base function
        eigenBaseFunctionLists_[element].col(oldSize)=baseFunction;
    }

    
    template<class DiscreteSpaceImp>
    inline void ConstrainedRBSpace<DiscreteSpaceImp>
    ::baseFunction (unsigned int i,
                    const MacroElement &subDomain,
                    DofVectorType &ret) const
    {
        assert(subDomain<gridPart().numSubDomains());
        ret = eigenBaseFunctionLists_[subDomain].col(i);
    }
    
    template< class DiscreteSpaceImp >
    inline void
    ConstrainedRBSpace< DiscreteSpaceImp > :: setBaseFunction (unsigned int i,
                                                           const MacroElement &subDomain,
                                                           DofVectorType &arg)
    {
        assert(subDomain<gridPart().numSubDomains());
        eigenBaseFunctionLists_[subDomain].col(i)=arg;
    }


  template< class DiscreteSpaceImp >
  inline const typename ConstrainedRBSpace< DiscreteSpaceImp > :: DiscreteFunctionSpaceType &
  ConstrainedRBSpace< DiscreteSpaceImp > :: baseFunctionSpace () const
  {
    return baseFunctionSpace_;
  }
    
    template< class DiscreteSpaceImp >
    inline void 
    ConstrainedRBSpace<DiscreteSpaceImp>::saveBaseFunctions(const std::string& directoryName="reducedSpaceBasis") const {
        const int numSubdomains = eigenBaseFunctionLists_.size();
        boost::filesystem::create_directory(directoryName);
        for (int subdomain=0; subdomain!=numSubdomains; ++subdomain) {
            eigenBaseFunctionLists_[subdomain].saveToBinaryFile(directoryName+"/subdomain_"
                                                                +boost::lexical_cast<std::string>(subdomain)
                                                                +"_data.bin");
        }
        return;
    }
    
    template< class DiscreteSpaceImp >
    inline void 
    ConstrainedRBSpace<DiscreteSpaceImp>::readBaseFunctions(const std::string& directoryName="reducedSpaceBasis")  {
        const int numSubdomains = baseFunctionSpace_.gridPart().numSubdomains();
        eigenBaseFunctionLists_.resize(numSubdomains);
        for (int subdomain=0; subdomain!=numSubdomains; ++subdomain) {
            eigenBaseFunctionLists_[subdomain].loadFromBinaryFile(directoryName+"/subdomain_"
                                                                +boost::lexical_cast<std::string>(subdomain)
                                                                +"_data.bin");
        }
        return;
    }


  template< class DiscreteSpaceImp >
  template< class DestinationFunctionType >
  inline void ConstrainedRBSpace< DiscreteSpaceImp >
    :: project ( const Eigen::VectorXd& sourceFunction,
               DestinationFunctionType& destFunction ) const  {

    Eigen::Map<Eigen::VectorXd> destEigen(destFunction.leakPointer(), destFunction.size());
    destEigen = Eigen::VectorXd::Zero(destEigen.rows());
    const unsigned int numSubDomains = baseFunctionSpace_.gridPart().numSubDomains(); 
    int start=0;
    for (unsigned int sD = 0; sD!=numSubDomains; ++sD) {
      const SubdomainBasisType& subdomainBasis = getSubdomainBasis(sD);
      const int subdomainBasisSize = subdomainBasis.cols();
      destEigen += subdomainBasis*sourceFunction.middleRows(start, subdomainBasisSize);
      start += subdomainBasisSize;
    }
    return;
  }

    template< class DiscreteSpaceImp >
    template< class LocalizerType>
    inline void ConstrainedRBSpace< DiscreteSpaceImp >
    :: project ( const Eigen::VectorXd& sourceFunction,
                Eigen::VectorXd& destFunction,
                const LocalizerType& localizer) const  {
        destFunction = Eigen::VectorXd::Zero(baseFunctionSpace_.size());
        
        const unsigned int numSubDomains = baseFunctionSpace_.gridPart().numSubdomains(); 
        int start=0;
        for (unsigned int sD = 0; sD!=numSubDomains; ++sD) {
            const SubdomainBasisType& subdomainBasis = getSubdomainBasis(sD);
            const int subdomainBasisSize = subdomainBasis.cols();
            const typename LocalizerType::MatrixType& fullSubdomainBasis = localizer.getFullMatrixRows(subdomainBasis, sD);
            destFunction += fullSubdomainBasis*sourceFunction.middleRows(start, subdomainBasisSize);
            start += subdomainBasisSize;
        }
        return;
    }
    

} //end of namespace Dune::RB

} //end of namespace Dune

#endif /* __CONSTRAINEDRBSPACE_INLINE_HH__ */

