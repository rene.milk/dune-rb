#ifndef LINEVOLEXPLICITDISCR_HH_
#define LINEVOLEXPLICITDISCR_HH_

#include  <dune/fem/solver/timeprovider.hh>
#include  <dune/fem/solver/rungekutta.hh>

#include "../operators/fv/convection.hh"
#include "../operators/fv/diffusion.hh"
#include "../operators/eye.hh"

#include "spaceoperatorwrapper.hh"

// for debugging purposes
#include "engoshop.hh"


namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

/** @ingroup linevol
 * @brief linear evolution scheme discrtization with finite
 * volume operators and explicit time discretization.
 */
template <class DiscrType>
class ExplicitDiscretization
{

public:
  //! grid type
  typedef typename DiscrType :: GridType                             GridType;
  //! grid part type
  typedef typename DiscrType :: GridPartType                         GridPartType;

  //! function space type
  typedef typename DiscrType :: FunctionSpaceType                    FunctionSpaceType;
  //! discrete function space type
  typedef typename DiscrType :: DiscFuncSpaceType                    SpaceType;
  //! discrete function type
  typedef typename DiscrType :: DiscreteFunctionType                 DestinationType;

  //! base type
  typedef SpaceOperatorInterface< DestinationType >                  BaseType;

  //! model type
  typedef typename DiscrType :: ModelType                            LinEvolModelType;

  //! time provider type
  typedef TimeProviderBase                                           TimeDiscParamType;

  typedef Lib::Operator::Fv::Convection::Composite< LinEvolModelType, DestinationType >   ConvFVCompType;
  //! decomposed operator for the convection contribution (non-constant parts)
  typedef typename ConvFVCompType :: CompositeType                   ConvFVDecompOpType;
#ifndef DUMB_TEST
  //! complete operator for the convection contribution (non-constant parts)
  typedef typename ConvFVDecompOpType :: CompleteType                ConvFVCompleteOpType;
#else
  typedef typename ConvFVDecompOpType :: DumbCompleteType            ConvFVCompleteOpType;
#endif

  typedef Lib::Operator::Fv::Convection::MuScaledFunctionComposite< LinEvolModelType,
                                         DestinationType >           ConvFVScalarCompType;
  //! decomposed function for the convection contribution (constant parts)
  typedef typename ConvFVScalarCompType :: CompositeType             ConvFVDecompFuncType;
  //! complete operator for the convection contribution (constant parts)
  typedef typename ConvFVDecompFuncType
            :: ConstantSpaceOperatorType                             ConvFVConstantOpType;

  typedef Lib::Operator::Fv::Diffusion::Composite< LinEvolModelType, DestinationType >   DiffFVCompType;
  //! decomposed operator for the diffusion contribution (non-constant parts)
  typedef typename DiffFVCompType :: CompositeType                   DiffFVDecompOpType;
#ifndef DUMB_TEST
  //! complete operator for the diffusion contribution (non-constant parts)
  typedef typename DiffFVDecompOpType :: CompleteType                DiffFVCompleteOpType;
#else
  typedef typename DiffFVDecompOpType :: DumbCompleteType            DiffFVCompleteOpType;
#endif

  typedef Lib::Operator::Fv::Diffusion::MuScaledFunctionComposite< LinEvolModelType,
                                         DestinationType >           DiffFVScalarCompType;
  //! decomposed function for the diffusion contribution (constant parts)
  typedef typename DiffFVScalarCompType :: CompositeType             DiffFVDecompFuncType;
  //! complete operator for the diffusion contribution (constant parts)
  typedef typename DiffFVDecompFuncType
            :: ConstantSpaceOperatorType                             DiffFVConstantOpType;



  //! decomposed operator of all non-constant contributions
  typedef typename ConvFVDecompOpType :: DecomposedMappingType       ExplicitDecompOperatorType;

  typedef Decomposed::Operator::ZeroFactory< DestinationType >                                MakeZeroType;
  //! decomposed operator for implicit discretization (= 0)
  typedef typename MakeZeroType :: OperatorType                      ImplicitDecompOperatorType;

  //! explicit space operator type
  typedef typename ConvFVDecompOpType :: SpaceMappingType            ExplicitOperatorType;

  //! eng-osher operator, for debugging purposes
  //typedef EngOshFiniteVolumeScheme<DestinationType, LinEvolModelType> 
  //ExplicitOperatorType;

  //! implicit operator type
  typedef typename ImplicitDecompOperatorType :: SpaceMappingType    ImplicitOperatorType;

  //! time solver type (explicit, runge kutta)
  typedef DuneODE :: ExplicitRungeKuttaSolver< DestinationType >     ODEType;
public:
  //! constructor
  ExplicitDiscretization (const SpaceType & space, LinEvolModelType &cdm) :
    linEvolModel_(cdm),
    space_(space),
    grid_(space_.grid()),
    gridPart_(space_.gridPart()),
    convFVComp_(space_, cdm),
    convFVDecompOp_(convFVComp_.getOperator()),
#ifndef DUMB_TEST
    op1_(convFVDecompOp_.complete()),
#else
    op1_(convFVDecompOp_.dumbComplete()),
#endif
    diffFVComp_(space_, cdm),
    diffFVDecompOp_(diffFVComp_.getOperator()),
#ifndef DUMB_TEST
    op2_(diffFVDecompOp_.complete()),
#else
    op2_(diffFVDecompOp_.dumbComplete()),
#endif
    convFVScalarComp_(cdm, space_),
    convFVDecompFunc_(convFVScalarComp_.getFunction()),
    func1Op_(convFVDecompFunc_.constantOperator()),
    diffFVScalarComp_(cdm, space_),
    diffFVDecompFunc_(diffFVScalarComp_.getFunction()),
    func2Op_(diffFVDecompFunc_.constantOperator()),
    explDecompOp_(convFVDecompOp_ + diffFVDecompOp_ ),
    makeZero_(space_),
    implDecompOp_(makeZero_.getOperator()),
    explComplOp_( op1_ + op2_ + func1Op_ + func2Op_ ),
    implComplOp_(implDecompOp_.complete())
  {
#ifdef DEBUG
    std::cout << "Using the explicit ode solver! In order to use a different"
      " discretization, change the 'DISCRETIZATION' make variable"
      << std::endl;
#endif
  };

  //! destructor
  ~ExplicitDiscretization ()
  {
  };

public: // interface methods to be used by the solvers

  //! return operator for explicit discretisation
  ExplicitOperatorType & explOp () {
    return explComplOp_;
  };

  //! return operator for implicit discretisation
  ImplicitOperatorType & implOp () {
    return implComplOp_;
  };

  //! return decomposed explicit operator (non-constant parts)
  ExplicitDecompOperatorType& explicitDecompOperator() {
    return explDecompOp_;
  }

  //! return decomposed implicit operator (non-constant parts)
  ImplicitDecompOperatorType& implicitDecompOperator() {
    return implDecompOp_;
  }

  //! return decomposed function with convection contributions (constant parts)
  ConvFVDecompFuncType& convFVDecompFunc() const {
    return convFVDecompFunc_;
  }

  //! return decomposed function with diffusion contributions (constant parts)
  DiffFVDecompFuncType& diffFVDecompFunc() const {
    return diffFVDecompFunc_;
  }

  //! return discrete function space
  const SpaceType & space() const
  {
    return space_;
  }

  //! creates an ode solver and returns a pointer to it
  //!
  //! \attention this must be deleted in user space afterwards.
  template<class TimeProvider>
  ODEType * newODESolver(TimeProvider & tp, const int rkSteps)
  {
    return new ODEType(explComplOp_, tp, rkSteps);
  }

protected:
  LinEvolModelType           &linEvolModel_;

  //! function space for discrete solution
  const SpaceType            &space_;

  const GridType             &grid_;
  const GridPartType         &gridPart_;

  ConvFVCompType              convFVComp_;
  ConvFVDecompOpType         &convFVDecompOp_;
  ConvFVCompleteOpType       &op1_;

  DiffFVCompType              diffFVComp_;
  DiffFVDecompOpType         &diffFVDecompOp_;
  DiffFVCompleteOpType       &op2_;

  ConvFVScalarCompType        convFVScalarComp_;
  ConvFVDecompFuncType       &convFVDecompFunc_;
  ConvFVConstantOpType       &func1Op_;

  DiffFVScalarCompType        diffFVScalarComp_;
  DiffFVDecompFuncType       &diffFVDecompFunc_;
  DiffFVConstantOpType       &func2Op_;


  ExplicitDecompOperatorType &explDecompOp_;
  MakeZeroType                makeZero_;
  ImplicitDecompOperatorType &implDecompOp_;

  // explicit Operator
  ExplicitOperatorType       &explComplOp_;
  ImplicitOperatorType       &implComplOp_;
};

} // namespace LinEvol
} // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif /* LINEVOLEXPLICITDISCR_HH_ */

