/* #include "linevolsolve.hh" */

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {


// constructor
template<class Stepper>
Solver<Stepper>::Solver (ModelType &linEvolModel, DiscretizationType &discr) :
  grid_(discr.space().grid()),
  stepper_(linEvolModel, discr)
{
  // ----- read in runtime parameters ------
}

// destructor
template< class Stepper >
Solver<Stepper>::~Solver (){}


//   void solve(DiscreteFunctionListType &funcList)
template< class Stepper>
template< class DiscreteFunctionListImp >
void Solver<Stepper>::solve(DiscreteFunctionListImp &funcList)
{
//   const typename Stepper::DiscreteSpaceType& space = stepper_.space();
//   DiscreteFunctionListType funclist(space, "NAME");
  evolve(funcList);

  Parameter::write("parameter.log");
}

template< class Stepper>
template< class DiscreteFunctionListImp >
void Solver< Stepper >::evolve(DiscreteFunctionListImp &funcList) {
  typedef typename Stepper :: DiscreteFunctionType                   DiscreteFunctionType;
  const typename Stepper::DiscreteSpaceType& space = stepper_.space();

  // solution function
  DiscreteFunctionType u("solution",space);

  // get some parameters
  std::string filename;
  Parameter::get("rb.gridFile", filename);
  int printCount           = Parameter::getValue<int>("rb.printCount", -1);
  int verboserank          = Parameter::getValue<int>("fem.verboserank", -1);
  const double maxTimeStep =
    Parameter::getValue("rb.maxTimeStep", std::numeric_limits<double>::max());
  double startTime         = Parameter::getValue<double>("rb.startTime");
  double endTime           = Parameter::getValue<double>("rb.endTime");

  // if (verboserank>1) {
  //   // print current mu values
  //   vector<std::string> mu_names;
  //   Dune::Parameter::get("rb.mu_names", mu_names);
  //   int noMu = mu_names.size();
  //   for (int i=0; i!=noMu; ++i) {
  //     std::cout << mu_names[i] << ": " << Dune::Parameter::getValue<double>(mu_names[i]) 
  //               << std::endl;
  //   }
  // }

  // Initialize Timer for CPU time measurements
  unsigned int timeStepTimer = FemTimer::addTo("max time/timestep");
  // Initialize TimeProvider
  GridTimeProvider< GridSelector :: GridType > tp(startTime, grid_);
  
  // Initialize the DataWriter that writes the solution on the harddisk in a
  // format readable by e.g. Paraview
  // in each loop for the eoc computation the results at
  // the final time is stored
  typedef tuple<DiscreteFunctionType*> IOTupleType;
  typedef DataOutput<typename GridSelector :: GridType, IOTupleType> DataWriterType;
  IOTupleType dataTup ( &u );
  DataWriterType dataWriter( grid_, dataTup, tp);

  // set initial data \todo use decomposed initial data
  stepper_.initialize(tp, u);
  funcList.push_back(u);

  // for statistics
  double maxdt     = 0.;
  double mindt     = 1.e10;
  double averagedt = 0.;

  dataWriter.write();

  /**********************************************
   * Time Loop                                  *
   *********************************************/
  FemTimer::start(timeStepTimer);
  for( tp.init() ; tp.time() < endTime ; tp.next() )
  {
    // keep the time step estimate bounded by maxTimeStep
    tp.provideTimeStepEstimate(maxTimeStep);
    const double tnow  = tp.time();
    const double ldt   = tp.deltaT();
//     std::cout << "ldt: " << ldt << std::endl;
    const int counter  = tp.timeStep();

    /************************************************
     * Compute an ODE timestep                      *
     ***********************************************/
    FemTimer::start(timeStepTimer, 1);
    stepper_.step(tp,u);

    FemTimer::stop(timeStepTimer, 1, FemTimer::max);

    // Check that no NANs have been generated
    if (!u.dofsValid()) {
      dataWriter.write();
      abort();
    }

    if(verboserank > 1 && printCount > 0 && counter % printCount == 0) {
      std::cout << "step: " << counter << "  time = " << tnow << ", dt = " << ldt
        << "\n";
    }

    dataWriter.write();

    //store solution in discFuncList
    funcList.push_back(u);

    // some statistics
    mindt = (ldt<mindt)?ldt:mindt;
    maxdt = (ldt>maxdt)?ldt:maxdt;
    averagedt += ldt;
  } /****** END of time loop *****/

/*  double runTime = FemTimer::stop();*/
  FemTimer::stop(timeStepTimer);

  FemTimer::printFile(tp,"./timer.out");
  FemTimer::reset(timeStepTimer);

  averagedt /= double(tp.timeStep());
  if(verboserank > 3)
  {
    std::cout << "Minimum dt: " << mindt
      << "\nMaximum dt: " << maxdt
      << "\nAverage dt: " << averagedt << std::endl;
  }

  // Write solution to hd
  stepper_.finalize(tp,u);
#if defined(DEBUG) && DEBUG > 6
  std::cout << "in Solver :: evolve(): solution at endtime =\n";
  u.print(std::cout);
#endif

  //std::cout << "==============================================\n"; std::cout.flush();
      //which function should be read from list?
      //int functionToGet=1;
      //      Parameter::get("discfuncwriter.id", functionToGet);

      //get function from list
      //assert(functionToGet < funcList.size());
      //      funcList.getFunc(functionToGet, u);
      //std::cout << "functionToGet: " << functionToGet << " size_XDR1: " << u.size() << " value_XDR1: " << u.dbegin()[5] << "\n";std::cout.flush();
  //     discFuncList_xdr3.getFunc(functionToGet, u);
  //     std::cout << "functionToGet: " << functionToGet << " size_XDR3: " << u.size() << " value_XDR3: " << u.dbegin()[5] << "\n";
  //     discFuncList_mem.getFunc(functionToGet, u);
  //     std::cout << "functionToGet: " << functionToGet << " size_mem: " << u.size() << " value_mem: " << u.dbegin()[5] << "\n";
}

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace RBFem
} // end of namespace Dune

/* vim:set et sw=2: */
