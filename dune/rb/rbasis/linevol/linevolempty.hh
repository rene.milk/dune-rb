#ifndef  __LINEVOL_EMPTY_HH__
#define  __LINEVOL_EMPTY_HH__

#include <string>

namespace Dune
{
namespace RBFem
{
namespace Example
{
namespace LinEvol
{


//#define RB_DEBUG

/** \ingroup linevol
 *  @brief   an empty implementation of a LinEvolInterface
 *
 * For testing purposes only.
 *
 */
template<class Traits>
class EmptyLibrary
  : public RB::ILinEvolLibrary <Traits>
{
  typedef EmptyLibrary ThisType;

  typedef RB::IRBServer< typename Traits::ServerType > ServerType;
  typedef RB::RBServerArgsTraits T;
  typedef RB::ISerializedArrayFactory< typename Traits::ArrayFactory > ArrayFactory;
public:
  //=================
  // Constructor
  //=================
  EmptyLibrary()
  {}

  //=================
  // Destructor
  //=================
  ~EmptyLibrary()
  {}

private:
  //Prohibit copying and assignment
  inline EmptyLibrary ( const ThisType & );
  inline ThisType & operator= ( const ThisType & );


  //===================================
  // all public methods, sorted by name
  //===================================
public:

  bool mexFunction ( const std::string & command, T::LArgType & alhs, T::RArgType const & arhs )
  {
    return false;
  }

  /**
   * @brief runs a detailed simulation of the numerical scheme for parameters
   * that need to be set beforehand via the set_mu method.
   * The solution is written to U, such that it is readable by
   * RBMatlab.
   *
   * @param[out] U      reference to the DoFs of the DiscreteFunction
   */
  inline void detailedSimulation(T::ArrayType * &U)
  {
  }


  /**
   * @brief expand current reduced basis with a given discrete function
   *
   * @param[in] dofVec a matlab dof vec, to be stored in basis as discrete function
   * @param[out] ret give back the new reduced basis
   */
  inline void expandRB(const T::ArrayType & dofVec, T::ArrayType * &ret)
  {
  }

  /** @brief generate model data
   *
   * This computes the scalar products \f$ \langle\phi_i | \phi_i\rangle\f$ where \f$\phi_i\f$
   * is the i'th basis function of the discrete function space. The values are saved internally 
   * and used e.g. for compunting the scalar product of two functions.
   *
   */
  inline void genModelData()
  {
  }

  /**
   * @brief returns a vector of parameter values corresponding to the values in
   * mu_names_
   *
   * @param[out] mu_values      vector of double with mu values
   */
  inline void getMu( T::ArrayType *& mu_values )
  {
  }

  /** @brief get the current size of the reduced basis space
   *
   *  @return returns the number of base functions in the reduced space
   */
  inline int getRBSize()
  {
    return 0;
  }

  /** @brief initialize the reduced basis with the data function \f$u_0\f$
   * 
   *  each component, that is the space-dependent part, of u0 is added as
   *  a function to the reduced basis space
   */
  inline void initDataBasis(T::ArrayType *& num)
  {
  }

  /**
   * @brief needs to be run at the beginning of the program in order to
   * initialize the grid and the Dune::Parameter singleton
   */
  inline void initModel( T::ArrayType *& ret )
  {
    ServerType :: printStatusMsg("in initModel\n");
    ServerType :: printWarnMsg("warning in initModel\n");
  }

  /**
   * @brief projects the inital values onto the reduced basis space
   *
   * @param[in] decomp_mode determines whether the complete function, only 
   *                        the components or only the coefficients are 
   *                        projected
   * @param[out] a0         an mxArray where the projected values will be written
   */
  inline void rbInitValues( const int decomp_mode, T::ArrayType * & a0 )
  {
  }

  /**
   * @brief returns offline operators that can be used by rb_simulation in RBmatlab.
   * This method can be seen to be the equivalent of gen_online_data in RBmatlab.
   *
   * @param[in]  decomp_mode integer indicating wheter the complete operator (0), only
   * @param[out] operators   online data (independent of H)
   *                         the operator components (1) or only the coefficient vector
   *                         shall be returned.
   */
  inline void rbOperators( const int decomp_mode,
                           T::ArrayType *& operators )
  {
  }

  /**
   * @brief reconstructs a reduced discrete function given by the coefficient
   * vector 'coefficients'.
   *
   * @param[in]  coefficients  coefficient vector \f$[\mathbf{a}^k]\f$
   * @param[out] reconsList    a list containing the reconstructed functions for each
   *                           timestep. \f$[U_N^k = \sum_{i=0}^N a_i^k \varphi_i]\f$.
   */
  template< class DiscreteFunctionListImp >
  inline void rbReconstruction( const T::ArrayType * coefficients,
                                DiscreteFunctionListImp & reconsList)
  {
  }

  /** @brief extend the reduced space
   *
   *  Given a vector of parameter values, this function computes the high dimensional
   *  solutions to these values and, using the pca algorithm, chooses one function
   *  which is then added to the reduced basis.
   *
   *  @param[in] mu an mxArray* of parameter values
   *  @param[in] k  optional parameter defining the number of functions to be added to 
   *                the space
   */
  inline void rbExtensionPCA ( const T::ArrayType & mu, const int k )
  {
  }

  /** @brief reconstructs a reduced simulation in the high dimensional function
   * space @f$W_H\f$.
   *
   * @param detailed_data       Matlab struct representing the current reduced
   *                            basis space.
   * @param coefficients        coefficient vectors @f$\mathbf{a}^k@f$ for some
   *                            time steps @f$k@f$, that ought to be
   *                            reconstructed.
   * @param ret                 some returnn value (TODO: define purpose)
   */
  inline void rbReconstruction( const T::ArrayType & detailed_data,
                                const T::ArrayType & coefficients,
                                T::ArrayType *& ret)
  {
  }

  /**
   * @brief replaces the values for parameter keys given through the vector
   *        in the private variable mu_names_ by values from vector mu_values
   *
   * @param[in] mu_values the parameter vector \f$\mu\f$.
   */
  inline void setMu(const T::ArrayType & mu_values)
  {
  }

  //=================
  // Member variables
  //=================

private:
}; // end of MatlabInterface

}  // namespace LinEvol
}  // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif  /*__LINEVOL_EMPTY_HH__ */

/* vim: set sw=2 et: */
