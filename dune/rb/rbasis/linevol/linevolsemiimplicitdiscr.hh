#ifndef LINEVOLSEMIIMPLICITDISCR_HH_
#define LINEVOLSEMIIMPLICITDISCR_HH_

#include  <dune/fem/solver/timeprovider.hh>
#include <dune/fem/solver/odesolver.hh>

#include "../operators/fv/convection.hh"
#include "../operators/fv/diffusion.hh"
#include "../operators/eye.hh"

#include "spaceoperatorwrapper.hh"

namespace Dune {
namespace RBFem {
namespace Example
{
namespace LinEvol
{


/** @ingroup linevol
 * @brief linear evolution scheme discretization with finite volume operators
 * and semi-implicit time discretization.
 */
template <class DiscrType>
class SemiImplicitDiscretization
{

public:
  //! grid type
  typedef typename DiscrType :: GridType                             GridType;
  //! grid part type
  typedef typename DiscrType :: GridPartType                         GridPartType;

  //! function space type
  typedef typename DiscrType :: FunctionSpaceType                    FunctionSpaceType;
  //! discrete function space type
  typedef typename DiscrType :: DiscFuncSpaceType                    SpaceType;
  //! discrete function type
  typedef typename DiscrType :: DiscreteFunctionType                 DestinationType;

  //! base type
  typedef SpaceOperatorInterface< DestinationType >                  BaseType;

  //! model type
  typedef typename DiscrType :: ModelType                            LinEvolModelType;

  //! time provider type
  typedef TimeProviderBase                                           TimeDiscParamType;

  typedef Lib::Operator::Fv::Convection::Composite< LinEvolModelType, DestinationType >   ConvFVCompType;
  //! decomposed operator for the convection contribution (non-constant parts)
  typedef typename ConvFVCompType :: CompositeType                   ConvFVDecompOpType;
  //! complete operator for the convection contribution (non-constant parts)
  typedef typename ConvFVDecompOpType :: CompleteType                ConvFVCompleteOpType;

  typedef Lib::Operator::Fv::Convection::MuScaledFunctionComposite< LinEvolModelType,
                                         DestinationType >           ConvFVScalarCompType;
  //! decomposed function for the convection contribution (constant parts)
  typedef typename ConvFVScalarCompType :: CompositeType             ConvFVDecompFuncType;
  //! complete operator for the convection contribution (constant parts)
  typedef typename ConvFVDecompFuncType
            :: ConstantSpaceOperatorType                             ConvFVConstantOpType;

  typedef Lib::Operator::Fv::Diffusion::Composite< LinEvolModelType, DestinationType >   DiffFVCompType;
  //! decomposed operator for the diffusion contribution (non-constant parts)
  typedef typename DiffFVCompType :: CompositeType                   DiffFVDecompOpType;
  //! complete operator for the diffusion contribution (non-constant parts)
  typedef typename DiffFVDecompOpType :: CompleteType                DiffFVCompleteOpType;

  typedef Lib::Operator::Fv::Diffusion::MuScaledFunctionComposite< LinEvolModelType,
                                         DestinationType >           DiffFVScalarCompType;
  //! decomposed function for the diffusion contribution (constant parts)
  typedef typename DiffFVScalarCompType :: CompositeType             DiffFVDecompFuncType;
  //! complete operator for the diffusion contribution (constant parts)
  typedef typename DiffFVDecompFuncType
            :: ConstantSpaceOperatorType                             DiffFVConstantOpType;

  typedef typename ConvFVDecompFuncType :: ConstantMappingType       ConstantMappingType;

  typedef ConstantMappingType                                        ConstantOpType;


  //! decomposed operator of all non-constant contributions
  typedef ConvFVDecompOpType                                         ExplicitDecompOperatorType;

  typedef Decomposed::Operator::ZeroFactory< DestinationType >                                MakeZeroType;
  //! decomposed operator for implicit discretization (= diffusion)
  typedef DiffFVDecompOpType                                         ImplicitDecompOperatorType;

  //! explicit operator type
  typedef typename ConvFVDecompOpType :: SpaceMappingType            ExplicitOperatorType;
  //! implicit operator type
  typedef typename DiffFVDecompOpType :: SpaceMappingType            ImplicitOperatorType;

  //! time solver type (semi-implicit)
  typedef DuneODE :: SemiImplicitOdeSolver< DestinationType >        ODEType;

public:
  //! constructor
  SemiImplicitDiscretization (const SpaceType & space, LinEvolModelType &cdm) :
    linEvolModel_(cdm),
    space_(space),
    grid_(space_.grid()),
    gridPart_(space_.gridPart()),
    convFVComp_(space_, cdm),
    convFVDecompOp_(convFVComp_.getOperator()),
    op1_(convFVDecompOp_.complete()),
    diffFVComp_(space_, cdm),
    diffFVDecompOp_(diffFVComp_.getOperator()),
    op2_(diffFVDecompOp_.complete()),
    convFVScalarComp_(cdm, space_),
    convFVDecompFunc_(convFVScalarComp_.getFunction()),
    func1Op_(convFVDecompFunc_.constantOperator()),
    diffFVScalarComp_(cdm, space_),
    diffFVDecompFunc_(diffFVScalarComp_.getFunction()),
    func2Op_(diffFVDecompFunc_.constantOperator()),
    constantOp_(func1Op_),
    explDecompOp_(convFVDecompOp_),
    makeZero_(space_),
    implDecompOp_(diffFVDecompOp_),
    explComplOp_(op1_ + func1Op_ + func2Op_ ),
    implComplOp_(op2_)
  {
#ifdef DEBUG
    std::cout << "Using the semi-implicit ode solver! In order to use a different"
      "discretization, change the 'DISCRETIZATION' make variable"
      << std::endl;
#endif
  };

  //! destructor
  ~SemiImplicitDiscretization ()
  {
  };

public: // interface methods to be used by the solvers

  //! return operator for explicit discretisation
  ExplicitOperatorType & explOp () {
    return explComplOp_;
  };

  //! return operator for implicit discretisation
  ImplicitOperatorType & implOp () {
    return implComplOp_;
  };

  //! return decomposed explicit operator (non-constant parts)
  ExplicitDecompOperatorType& explicitDecompOperator() {
    return explDecompOp_;
  }

  //! return decomposed implicit operator (non-constant parts)
  ImplicitDecompOperatorType& implicitDecompOperator() {
    return implDecompOp_;
  }

  //! return decomposed function with convection contributions (constant parts)
  ConvFVDecompFuncType& convFVDecompFunc() const {
    return convFVDecompFunc_;
  }

  //! return decomposed function with diffusion contributions (constant parts)
  DiffFVDecompFuncType& diffFVDecompFunc() const {
    return diffFVDecompFunc_;
  }

  //! return discrete function space
  const SpaceType & space() const
  {
    return space_;
  }

  //! creates an ode solver and returns a pointer to it
  //!
  //! \attention this must be deleted in user space afterwards.
  template<class TimeProvider>
  ODEType * newODESolver(TimeProvider & tp, const int rkSteps)
  {
    return new ODEType(explOp(), implOp(), tp, rkSteps);
  }

protected:
  LinEvolModelType           &linEvolModel_;

  //! function space for discrete solution
  const SpaceType            &space_;

  const GridType             &grid_;
  const GridPartType         &gridPart_;

  ConvFVCompType              convFVComp_;
  ConvFVDecompOpType         &convFVDecompOp_;
  ConvFVCompleteOpType       &op1_;

  DiffFVCompType              diffFVComp_;
  DiffFVDecompOpType         &diffFVDecompOp_;
  DiffFVCompleteOpType       &op2_;

  ConvFVScalarCompType        convFVScalarComp_;
  ConvFVDecompFuncType       &convFVDecompFunc_;
  ConvFVConstantOpType       &func1Op_;

  DiffFVScalarCompType        diffFVScalarComp_;
  DiffFVDecompFuncType       &diffFVDecompFunc_;
  DiffFVConstantOpType       &func2Op_;

  ConstantOpType             &constantOp_;

  ExplicitDecompOperatorType &explDecompOp_;
  MakeZeroType                makeZero_;
  ImplicitDecompOperatorType &implDecompOp_;

  // explicit Operator
  ExplicitOperatorType       &explComplOp_;
  ImplicitOperatorType       &implComplOp_;
};

}  // namespace LinEvol
}  // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif /* LINEVOLSEMIIMPLICITDISCR_HH_ */

