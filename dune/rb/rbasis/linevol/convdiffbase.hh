#ifndef __CONVDIFFBASE_HH__
#define __CONVDIFFBASE_HH__

#include <dune/common/fmatrix.hh>
#include "../../misc/parameter/parameter.hh"
#include "../../datafunc/dirichlet.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

/** @ingroup models
 * @brief model class for convection diffusion problems.
 *
 * This model describes convection-diffusion problems of following type
 * @f{eqnarray}{
 *   D_t R(x,t,u) + \nabla \cdot ( F(x,t,u) - D(x,t) \nabla ( A (u) ) = & Q(x,t,u)  & \mbox{in } \Omega \\
 *                                                         u(\cdot,0) = & u_0(x)    & \mbox{in } \Omega \\
 *                                                             u(x,t) = & u_{\mbox{dir}}(x,t) & \mbox{in }\Gamma_{\mbox{dir}} \\
 *                          F(x,t,u) - D(x,u) \nabla ( A (u)) \cdot n = & u_{\mbox{neu}}(x,t) &\mbox{in }\Gamma_{\mbox{neu}} \\
 *                                  - D(x,t) \nabla ( A (u)) \cdot  n = & 0                 & \mbox{in }\Gamma_{\mbox{out}}
 * @f}
 *
 * where - by now - @f$A(u) = u@f$, @f$R(x,t,u)=u@f$, @f$Q=0@f$ and
 * @f$\Gamma_{\mbox{neu}} = \emptyset@f$.
 *
 * All other data functions are exchangeable via template parameters.
 *
 * @tparam InitDataFactory   factory for decomposed initial data function
 *                           @f$u_0(x)@f$.
 * @tparam VelocityFactory   factory for decomposed convective flux function
 *                           @f$F(x,t,u)@f$.
 * @tparam DiffusionFactory  factory for decomposed diffusion-dispersion matrix
 *                           @f$D(x,t)@f$.
 * @tparam DirichletFactory  factory for values on Dirichlet boundary
 *                           @f$u_{\mbox{dir}}@f$
 * @tparam BoundaryType      class defining the boundary types
 *                           @f$\Gamma_{\mbox{out}}, \Gamma_{\mbox{neu}}@f$ and
 *                           @f$\Gamma_{\mbox{dir}}@f$.
 *
 * @rbparam{rb.lambda, lambda in lax-friedrichs flux, 0.5}
 */
template <class ModelParamType,
          class InitDataFactory,
          class VelocityFactory,
          class DiffusionFactory,
          class DirichletFactory,
          class BoundaryType,
          class ExactLocalFunctionImp = Lib::Data::DirichletConstant<ModelParamType> >
class ConvectionDiffusionModel
{
public:

  //! model parameter type
  typedef ModelParamType                                             ParamType;

  //! this type
  typedef ConvectionDiffusionModel< ModelParamType, InitDataFactory,
                        VelocityFactory, DiffusionFactory,
                        DirichletFactory, BoundaryType >             ThisType;

  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! domain vector space type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;

  //! dimension of domain vector space
  static const int dimDomain = ModelParamType :: dimDomain;
  //! field type
  typedef typename ModelParamType :: FieldType                       FieldType;
  //! diffusion matrix type
  typedef FieldMatrix< FieldType, dimDomain, dimDomain >             DiffusionMatrixType;
  //! jacobian matrix type
  typedef DomainType                                                 JacobianMatrixType;

  //! decomposed function for initial data function
  typedef typename InitDataFactory :: DecomposedType                 DecomposedInitDataType;
  //! decomposed function for velocity function
  typedef typename VelocityFactory :: DecomposedType                 DecomposedVelocityType;
  //! decomposed function for diffusion function
  typedef typename DiffusionFactory :: DecomposedType                DecomposedDiffusionType;
  //! decomposed function for Dirichlet boundary function
  typedef typename DirichletFactory :: DecomposedType                DecomposedDirichletType;

  //! exact local function for eoc calculations
  typedef ExactLocalFunctionImp                                      ExactLocalFunction;


public:
  //! constructor
  ConvectionDiffusionModel  (const GridPartType &gridPart)
    : funcSpace_(),
      gridPart_( gridPart ),
      lambda_( Parameter::getValue<double>("rb.lambda", 0.5) ),
      //! \todo it is not so beautiful to init the Maker class with a model
      //! this pointer inside the model constructor. However, this works, but
      //! we need to keep an eye on the right order of initialization.
      initDataFactory_( *this, gridPart_ ),
      velFactory_( *this, gridPart_ ),
      diffFactory_( *this, gridPart_ ),
      dirFactory_( *this, gridPart_ )
  {
  };

  //! destructor
  ~ConvectionDiffusionModel ()
  {
  };

public: // interface methods

  //! returns the decomposed function for @f$F(x,t,u)@f$
  DecomposedVelocityType & velocity ()
  {
    return velFactory_.getFunction();
  };

  //! returns the decomposed function for @f$u_{\mbox{dir}}(x,t)@f$
  DecomposedDirichletType & dirichlet()
  {
    return dirFactory_.getFunction();
  }

  //! sets the lambda for the lax-friedrichs flux
  //!
  //! \todo get rid of this function in the model.
  void lambdaScalar(DomainType & x, const double time,
                    double & lambda) const
  {
    lambda = lambda_;
  }

  //! returns the decomposed function for @f$u_0(x)@f$
  const DecomposedInitDataType & initialData () const
  {
    return initDataFactory_.getFunction();
  }

  //! returns the decomposed function for @f$D(x,t)@f$
  DecomposedDiffusionType & diffusion ()
  {
    return diffFactory_.getFunction();
  }

  //! returns the boundary type for an intersection and time value
  template <class IntersectionIterator, class FaceDomainType>
  inline int boundaryType(const IntersectionIterator & it, const double time,
                          const FaceDomainType & localPoint,
                          const DomainType & point ) const
  {
    return BoundaryType :: boundaryType(it, time, localPoint, point);
  }

protected:
  //! the problem data for this problem
  FunctionSpaceType   funcSpace_;

  const GridPartType &gridPart_;
  double              lambda_;
  InitDataFactory     initDataFactory_;


private:   // for internal use only
  VelocityFactory  velFactory_;
  DiffusionFactory diffFactory_;
  DirichletFactory dirFactory_;


}; // end class ConvDiffBase

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif

/* vim: set sw=2 et: */
