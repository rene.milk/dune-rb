#ifndef SPACEOPERATORWRAPPER_HH_

#define SPACEOPERATORWRAPPER_HH_

#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/operator/discreteoperatorimp.hh>
#include "../../misc/parameter/parameter.hh"
#include "../../misc/utility.hh"
#include <algorithm>

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Operator {

// forward declaration
template< class Mapping, class DiscFuncType >
class CombinedTimeSpaceMapping;

/**
 * @brief adds time dependency methods to the SpaceOperatorInterface.
 */
template< class Mapping, class DiscFuncType >
class TimeSpaceMapping
  : public Dune::SpaceOperatorInterface< DiscFuncType >
{
private:
  typedef DiscFuncType                                               DiscreteFunctionType;
  typedef TimeSpaceMapping< Mapping, DiscreteFunctionType >      ThisType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
public:

  TimeSpaceMapping(const DiscreteFunctionSpaceType & space, Mapping & wrapee)
    : space_(space), wrapee_(wrapee)
  { }

  virtual ~TimeSpaceMapping() {};

  //! applies the operator
  void operator()(const DiscreteFunctionType & arg, DiscreteFunctionType & dest) const
  {
    wrapee_(arg, dest);
  }

  //! returns time step estimate
  virtual double timeStepEstimate() const = 0;

  //! sets the new time in the  wrapped operator
  virtual void setTime(double time) = 0;

  //! returns discrete function space
  const DiscreteFunctionSpaceType & space() const
  {
    return space_;
  }

  const Mapping& operator*() const
  {
    return wrapee_;
  }

/*private:

 *  const ThisType & operator+(const ThisType & op2) const
 *  {
 *    // You should not use constant references!
 *  }*/
protected:
  Mapping & getWrapee()
  {
    return wrapee_;
  }

private:
  const DiscreteFunctionSpaceType &space_;
  Mapping                         &wrapee_;
};

// forward declarations
template< class WrapperOne, class WrapperTwo >
class CombinedTimeSpaceWrapper;

template <class OpWrapee, class MappingType, class DiscFuncType>
class TimeSpaceWrapper;

/**
 * @brief overloads the plus operator for SpaceOperatorWrapper
 * objects with the purpose to allow efficient sums of such operators.
 *
 * Indeed the plus operator is overloaded for these objects such that
 * only the local operators are added if existent. This reduces the number
 * of necessary grid walk when the summed operator is evaluated.
 */
template <class MappingType, class DiscFuncType, class ImpType>
class TimeSpaceWrapperDefault
  : public TimeSpaceMapping< MappingType, DiscFuncType >,
    public Dune::ObjPointerStorage
{
public:
  typedef DiscFuncType                                               DiscreteFunctionType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef TimeSpaceMapping< MappingType, DiscFuncType >          BaseType;
public:
  TimeSpaceWrapperDefault(const DiscreteFunctionSpaceType & space, MappingType & wrapee)
    : BaseType(space, wrapee)
  {
  }

  template<class OpWrapeeAddend>
  CombinedTimeSpaceWrapper<ImpType,
                               TimeSpaceWrapper<OpWrapeeAddend,
                                  MappingType, DiscreteFunctionType> > &
  operator+( TimeSpaceWrapper<OpWrapeeAddend,
                                  MappingType, DiscreteFunctionType> & op2 )
  {
    typedef CombinedTimeSpaceWrapper< ImpType,
                                          TimeSpaceWrapper
                                            < OpWrapeeAddend,
                                              MappingType,
                                              DiscreteFunctionType > > SumType;

    SumType * sum = new SumType(asImp(), op2);

    this->saveObjPointer(sum);

    return (*sum);
  }

  template<class OpWrapeeAddend>
  CombinedTimeSpaceWrapper<ImpType,
                               TimeSpaceWrapper<OpWrapeeAddend,
                                  MappingType, DiscreteFunctionType> > &
  add( TimeSpaceWrapper<OpWrapeeAddend,
                                  MappingType, DiscreteFunctionType> & op2 )
  {
    typedef CombinedTimeSpaceWrapper< ImpType,
                                          TimeSpaceWrapper
                                            < OpWrapeeAddend,
                                              MappingType,
                                              DiscreteFunctionType > > SumType;

    SumType * sum = new SumType(asImp(), op2);

    this->saveObjPointer(sum);

    return (*sum);
  }

private:
  ImpType & asImp()
  {
    return static_cast<ImpType &>(*this);
  }

  const ImpType & asImp() const
  {
    return static_cast<const ImpType &>(*this);
  }
};

/**
 * @brief is automatically created when two SpaceOperatorWrapperDefault
 * objects are added together.
 */
template< class WrapperOne, class WrapperTwo >
class CombinedTimeSpaceWrapper
  : public TimeSpaceWrapperDefault< typename WrapperOne :: MappingType,
                                        typename WrapperOne :: DiscreteFunctionType,
                                        CombinedTimeSpaceWrapper
                                          <WrapperOne, WrapperTwo> >
{
private:
  typedef typename WrapperOne :: OpWrapeeType                        OpWrapeeOne;
  typedef typename WrapperTwo :: OpWrapeeType                        OpWrapeeTwo;


  template<class FullOp1, class FullOp2>
  struct FullOperatorAddType
  {
    //! combined type
    typedef Empty                                                    OperatorType;
//    typedef SpaceOperatorMapping< MappingType, DomainType >          type;
  };

  /**
   * @brief contains the type of the complete operator after adding FullOp1 and
   * FullOp2
   *
   * @todo I think, we do not need this any more, because CompleteType is always a SpaceOperatorMapping
   *
   * @tparam LocalOp1  local operator of first addend
   * @tparam LocalOp2  local operator of second addend
   */
  template<class LocalOp1, class LocalOp2, class DFDomainType, class DFRangeType>
  struct FullOperatorAddType<
           DiscreteOperator<
             LocalOp1, DFDomainType, DFRangeType >,
           DiscreteOperator<
             LocalOp2, DFDomainType, DFRangeType > >
  {
    //! combined type
    typedef DiscreteOperator< CombinedLocalOperator< LocalOp1,
                                                     LocalOp2 >,
                              DFDomainType, DFRangeType >            OperatorType;
  };

public:
//  typedef __typeof__(makeT<OpWrapeeOne> () + makeT<OpWrapeeTwo>() )  OpWrapeeType;
  typedef typename FullOperatorAddType< OpWrapeeOne, OpWrapeeTwo >
            :: OperatorType                                          OpWrapeeType;

  typedef TimeSpaceWrapperDefault
                    < typename WrapperOne :: MappingType,
                      typename WrapperOne :: DiscreteFunctionType,
                      CombinedTimeSpaceWrapper< WrapperOne,
                                                    WrapperTwo > >   BaseType;

public:
  typedef typename WrapperOne :: MappingType                         MappingType;
  typedef typename WrapperOne :: DiscreteFunctionType                DiscreteFunctionType;
public:

  CombinedTimeSpaceWrapper(WrapperOne & op1, WrapperTwo & op2)
    : BaseType(op1.space(), (*op1 + *op2)), op1_(op1), op2_(op2)
  {}

  double timeStepEstimate() const
  {
    double dtEst = std::min(op1_.timeStepEstimate(), op2_.timeStepEstimate());
    return dtEst;
  }

  void setTime(double time)
  {
    op1_.setTime(time);
    op2_.setTime(time);
  }

  OpWrapeeType & operator*()
  {
    return static_cast<OpWrapeeType &>((*this).getWrapee());
  }
private:
  WrapperOne   &op1_;
  WrapperTwo   &op2_;
};

/**
 * @brief wraps an arbitrary discrete space operator and adds method for time control.
 *
 * It also implements the plus operator for efficient summation of localized operators.
 */
template <class OpWrapee, class Mapping, class DiscFuncType>
class TimeSpaceWrapper
  : public TimeSpaceWrapperDefault<Mapping, DiscFuncType,
                                       TimeSpaceWrapper<OpWrapee, Mapping, DiscFuncType> >
{
public:
  typedef OpWrapee                                                   OpWrapeeType;
  typedef Mapping                                                    MappingType;
  typedef DiscFuncType                                               DiscreteFunctionType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef TimeSpaceWrapper< OpWrapeeType, MappingType,
                                DiscreteFunctionType >               ThisType;
  typedef TimeSpaceWrapperDefault< MappingType,
                                       DiscreteFunctionType,
                                       ThisType >                    BaseType;
public:
  //! constructor
  TimeSpaceWrapper( const DiscreteFunctionSpaceType & space, OpWrapeeType & wrapee )
    : BaseType(space, wrapee),
      wrapee_(wrapee),
      dtEst_(Parameter::getValue<double>("rb.dtEst"))
  {};

  double timeStepEstimate() const
  {
    //! @todo this should be somehow delegated to the Wrappee
    double minEst = dtEst_;
    return minEst;
  }

  void setTime(double time)
  {
    wrapee_.getLocalOp().setTime(time);
  }

  OpWrapeeType & operator*()
  {
    return wrapee_;
  }

private:
  OpWrapeeType &wrapee_;
  double        dtEst_;
};

/**
 * @brief wraps an arbitrary discrete space operator and adds empty implementations for
 * time control methods.
 *
 * This class implements the plus operator for efficient summation of localized operators.
 */
template <class Mapping, class DiscFuncType>
class SpaceOperatorWrapper
  : public TimeSpaceMapping<Mapping, DiscFuncType>,
    public Dune::ObjPointerStorage
{

public:
  typedef Mapping                                                    OpWrapeeType;
  typedef Mapping                                                    MappingType;
  typedef DiscFuncType                                               DiscreteFunctionType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef TimeSpaceMapping< MappingType, DiscreteFunctionType >  BaseType;
  typedef SpaceOperatorWrapper< Mapping, DiscFuncType >          ThisType;

public:
  SpaceOperatorWrapper(const DiscreteFunctionSpaceType & space, MappingType & mapping)
    : BaseType(space, mapping),
      dtEst_(Parameter::getValue<double>("rb.dtEst"))
  {}

  virtual ~SpaceOperatorWrapper() {}

  double timeStepEstimate() const
  {
    return dtEst_;
  }


  template<typename AddType>
  ThisType &
  operator+( AddType & op2 )
  {
    MappingType * msum = new MappingType(**this + *op2);
    ThisType * sum = new ThisType(this->space(), *msum );

    this->saveObjPointer(sum);

    return (*sum);
  }

  virtual void setTime(double time)
  {
  }

private:
  double dtEst_;
};

} // namespace Operator
} // namespace Lib
} // end of namespace RBFem
} // end of namespace Dune


#endif /* SPACEOPERATORWRAPPER_HH_ */

