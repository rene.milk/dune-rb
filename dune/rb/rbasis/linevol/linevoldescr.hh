#ifndef __LINEVOLDESCR_HH__
#define __LINEVOLDESCR_HH__

#ifdef MATLAB_MEX_FILE
#undef HAVE_BLAS
#define HAVE_BLAS 0
#endif

#include <iostream>

#include <dune/common/tuples.hh>
#include <dune/fem/gridpart/gridpart.hh>

// include Parameter singleton
#include "../../misc/parameter/parameter.hh"
using Dune::MuRangeType;
using Dune::Parameter;

namespace Dune
{
namespace RBFem
{
namespace Example
{
namespace LinEvol
{

#ifdef GRIDDIM
const int dim = GRIDDIM;
#else
const int dim = 2;
#endif

/*const double fcweighD = 0.25;*/

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace RBFem
} // end of namespace Dune

// include convection diffusion model
//#include "linevoldata.hh"
#include "linevolmodel.hh"
#include "linevoldiscr.hh"
#include "linevolsolve.hh"

namespace Dune
{
namespace RBFem
{
namespace Example
{
namespace LinEvol
{

/** @ingroup linevol
 * @brief defines all types that are needed for the discretization of the model
 *
 * The user can choose between different models and time discretizations via the
 * template parameters @c me and @c tde.
 *
 * For acceptable values see the definition of the enums TimeDiscretizationEnum
 * and ModelEnum, respectively the associated specializations of
 * LinEvolDiscretization and LinEvolModel
 */
template< typename Field, int dimR, int polOrd, enum TimeDiscretizationEnum tde,
    enum ModelEnum me >
struct Description
{
  typedef GridSelector::GridType GridType;
  enum
  {
    dim = GridType::dimension
  };
  enum
  {
    dimworld = GridType::dimensionworld
  };
  enum
  {
    polOrder = polOrd
  };

  enum
  {
    dimRange = dimR
  };

  typedef Field FieldType;
  // GridPartType
  typedef AdaptiveLeafGridPart< GridType > GridPartType;

  typedef FunctionSpace< FieldType, FieldType, dim, dimRange > FunctionSpaceType;

  // from the data functions we build the model parameters, with the help of
  // the class LinEvolModel
  typedef ModelTraits< 1, FunctionSpaceType, GridPartType > ModelParamType;
  typedef typename Model< ModelParamType, me >::Type ModelType;

  // and from the model we can build the discretization FV operators for the
  // reaction, the convection and the diffusion term.
  typedef DiscretizationTraits< ModelType, polOrd > DiscrParamType;
  typedef typename Discretization< DiscrParamType, tde >::Type DiscrType;

  //we need the discrete function space type for discrete function list
  typedef typename DiscrParamType::DiscFuncSpaceType DiscFuncSpaceType;

  // Furthermore, we need to define the ODE solver.
  typedef typename DiscrType::DestinationType DestinationType;
  typedef typename DiscrType::ODEType ODEType;

  typedef tuple< DestinationType * > IOTupleType;

  typedef Description< Field, dimR, polOrd, tde, me > ThisType;
  typedef Stepper< ThisType > StepperType;
  typedef Solver< StepperType > SolverType;
};

} // end namespace LinEvol
} // end namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif
