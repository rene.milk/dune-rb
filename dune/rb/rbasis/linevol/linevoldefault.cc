//#include "linevoldefault.hh"

#include <dune/fem/operator/projection/l2projection.hh>
#include <dune/fem/function/adaptivefunction/adaptivefunction.hh>
#include <dune/fem/io/file/dataoutput.hh>

#include <dune/rb/misc/parameter/parameter.hh>
#include <dune/rb/matlabcomm/serializedarray/interface.hh>
#include "../functions/decomposedfunction.hh"

#include <unistd.h>

#include <dune/common/timer.hh>

namespace Dune
{
namespace RBFem
{
namespace Example
{
namespace LinEvol
{

//============
// constructor
//============
template< class ST, class PT >
inline Library< ST, PT >::Library ()
    : discFuncSpace_( 0 ), redBaseSpace_( 0 ), model_( 0 ), discretization_( 0 )
{
  char * dunerb_path_cstr = getenv( "DUNERBHOME" );
  if (dunerb_path_cstr == NULL)
  {
    ServerType::printErrMsg(
        "Environment variable DUNERBHOME needs to be set. Model will not be usable!" );
    return;
  }
  std::string dunerbPath( dunerb_path_cstr );
  std::string parameterfile = dunerbPath + "/dunerbconvdiff.params";
  Dune::Parameter::append( parameterfile );
  /*  Dune::Parameter :: append(argc_mpi, (char**)argv_mpi);
   Dune::Parameter :: append(std::string("mexconvdiff.params"));*/

  mu_names_ = Dune::Parameter::getValue< std::vector< std::string > >(
      "rb.mu_names" );
  Dune::Parameter::get( "rb.mu_ranges", mu_ranges_ );
  assert( mu_ranges_.size() == mu_names_.size() );

  std::string gridFilename = Dune::Parameter::getValue< std::string >(
      "rb.gridFile" );
  {
    //! \todo add check if gridFilename exists, because MATLAB dies if something goes
    // wrong. An exit call in dune, alberta or ALUgrid destroys the entire matlab
    // session!
    gridPtr_ = new GridPtr< GridType >( gridFilename );
  }

  GridType & grid( *(*gridPtr_) );
  int startLevel = Dune::Parameter::getValue< int >( "rb.startLevel", 0 );
  // and refine the grid until the startLevel is reached
  for (int level = 0; level < startLevel; ++level)
  {
    //      gridPtr_->globalRefine(DGFGridInfo<GridType>::refineStepsForHalf());
    GlobalRefine::apply( grid, DGFGridInfo< GridType >::refineStepsForHalf() );
  }
  gridPart_ = new GridPartType( grid );
  discFuncSpace_ = new DiscFuncSpaceType( *gridPart_ );
  linAlgLib_ = new LinAlgLib( *discFuncSpace_ );
}

template< class ST, class PT >
Library< ST, PT >::~Library ()
{
  if (discretization_ != 0)
  {
    delete discretization_;
    discretization_ = 0;
  }
  if (redBaseSpace_ != 0)
  {
    delete redBaseSpace_;
    redBaseSpace_ = 0;
  }
  if (model_ != 0)
  {
    delete model_;
    model_ = 0;
  }
  if (linAlgLib_ != 0)
  {
    delete linAlgLib_;
    linAlgLib_ = 0;
  }
  if (discFuncSpace_ != 0)
  {
    delete discFuncSpace_;
    discFuncSpace_ = 0;
  }
  if (gridPart_ != 0)
  {
    delete gridPart_;
    gridPart_ = 0;
  }
  if (gridPtr_ != 0)
  {
    delete gridPtr_;
    gridPtr_ = 0;
  }
#ifdef SHARED_PTR_NAMESPACE
  shGridPtr_.reset();
#endif
}

//====================================
// all private methods, sorted by name
//====================================

template< class ST, class PT >
template< class ContainerT >
inline bool Library< ST, PT >::checkInsideMuRange (
    const ContainerT & muvalues) const
{
  bool insideRange = true;
  for (unsigned int i = 0; i < mu_ranges_.size(); i++)
  {
    if (!mu_ranges_[i].isValid( muvalues[i] ))
    {
#ifndef NDEBUG
      std::stringstream oss;
      oss << "Parameter " << i << " is out of range: " << muvalues[i]
          << " not in " << mu_ranges_[i] << std::endl;
      ServerType::printErrMsg( oss.str() );
#endif
      insideRange = false;
    }
  }
  return insideRange;
}

template< class ST, class PT >
inline void Library< ST, PT >::initReducedBasisSpace (
    const std::string rbPath)
{
  if (rbPath != "")
    redBaseSpace_ = new ReducedBasisSpace( discFuncSpace(), rbPath );
  else
    redBaseSpace_ = new ReducedBasisSpace( discFuncSpace() );
}

template< class ST, class PT >
inline void Library< ST, PT >::trajectoryToMatlab (
    const DiscreteFunctionList_XDR & trajectory, Args::ArrayType * &U) const
{
  typedef typename DiscreteFunctionType::DofIteratorType DofIteratorType;

  const DiscFuncSpaceType &discFuncSpace = this->discFuncSpace();

  int ndofs = discFuncSpace.size();
  int ntimesteps = trajectory.size();
  U = ArrayFactory::createMatrix( ntimesteps, ndofs );

  DiscreteFunctionType buffer( "buffer", discFuncSpace );

  double * u_ptr = U->getPr();

  int trajSize = trajectory.size();

  for (int i = 0; i < trajSize; ++i)
  {
    trajectory.getFunc( i, buffer );
    int j = 0;
    DofIteratorType end = buffer.dend();
    for (DofIteratorType it = buffer.dbegin(); it != end; ++it, ++j)
    //Assume all functions have the same size!
    //write function for timestep j in row j
    {
      u_ptr[i + trajSize * j] = *it;
    }
  }
}

//===================================
// all public methods, sorted by name
//===================================

template< class ST, class PT >
inline void Library< ST, PT >::clearReducedSpaceFiles ()
{
  if (redBaseSpace_)
  {
    redBaseSpace_->clear();
    redBaseSpace_->getFuncList().clear();
    delete redBaseSpace_;
  }

  initReducedBasisSpace( rbListPath_ );
}

template< class ST, class PT >
inline void Library< ST, PT >::detailedSimulation (Args::ArrayType * &U)
{
  solveDetailed( U );
  //     solver.solve();
}

template< class ST, class PT >
inline double Library< ST, PT >::errorToExact (DiscreteFunctionType & u,
                                                      double time)
{
  typedef typename ModelType::ExactLocalFunction ExactLocalFunction;
  typedef LocalFunctionAdapter< ExactLocalFunction > ExactSolution;
  typedef typename ExactSolution::LocalFunctionType LocalFunctionType1;
  typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType2;

  typedef typename DiscFuncSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;
  typedef typename EntityType::Geometry GeometryType;

  ExactLocalFunction exactLF( model(), gridPart() );
  exactLF.setTime( time );

  ExactSolution exactSol( "exact", exactLF, gridPart() );

  const DiscFuncSpaceType & space = discFuncSpace();

  typedef typename GridPartType::GridType::Traits::CollectiveCommunication CommunicatorType;

  const CommunicatorType & comm = gridPart().grid().comm();

  double ret = 0;

  RangeType lv1, lv2;

  int quadOrd = 2 * ProblemType::polOrder + 1;

  // iterate over all elements defining the function
  IteratorType eit = space.end();
  for (IteratorType it = space.begin(); it != eit; ++it)
  {
    const EntityType& en = *it;

    CachingQuadrature< GridPartType, 0 > quad( en, quadOrd );
    // get local functions on current element
    LocalFunctionType1 lf1 = exactSol.localFunction( en );
    LocalFunctionType2 lf2 = u.localFunction( en );

    // get geoemetry of entity
    const GeometryType& geo = en.geometry();

    const int quadNop = quad.nop();
    for (int qp = 0; qp < quadNop; ++qp)
    {
      const double det = geo.integrationElement( quad.point( qp ) );

      // evaluate local functions
      lf1.evaluate( quad[qp], lv1 );
      lf2.evaluate( quad[qp], lv2 );
      // substract
      lv1 -= lv2;

      ret += det * quad.weight( qp ) * lv1.one_norm();
    } // end qp iteration

  } // end element iteration

  ret = comm.sum( ret );
  return ret;
}

template< class ST, class PT >
inline void Library< ST, PT >::expandRB (const Args::ArrayType &dofVec,
                                                Args::ArrayType * &ret)
{
  typedef typename DiscreteFunctionType::DofIteratorType DofIteratorType;

  //get a reference to the reduced basis space
  ReducedBasisSpace &redBaseSpace = *(redBaseSpace_);
  //create a buffer function
  DiscreteFunctionType buffer( "buffer", *(discFuncSpace_) );

  //get a pointer to the dof vector
  const double *dofVec_ptr = dofVec.getPr();

  int i = 0;
  DofIteratorType end = buffer.dend();
  //copy dofs to buffer
  for (DofIteratorType it = buffer.dbegin(); it != end; ++it, ++i)
    *it = dofVec_ptr[i];

  /*store buffer as base function in reduced basis space. Function is
   saved on disc, so we don't need to care about buffer beeing freed
   at the end of this function*/
  redBaseSpace.addBaseFunction( buffer );

  int spaceSize = redBaseSpace.numBaseFunctions();
  int ndofs = 32;
  int nfuncs = spaceSize;
  ret = ArrayFactory::createMatrix( nfuncs, ndofs );
  double * ret_ptr = ret->getPr();

  for (int ii = 0; ii < spaceSize; ++i)
  {
    redBaseSpace.baseFunction( ii, buffer );
    int j = 0;
    typename DiscreteFunctionType::DofIteratorType dend = buffer.dend();
    for (typename DiscreteFunctionType::DofIteratorType it = buffer.dbegin();
        it != dend; ++it, ++j)
        //Assume all functions have the same size!
        //write function for timestep j in row j
    {
      ret_ptr[ii + spaceSize * j] = *it;
    }
  }
}

template< class ST, class PT >
inline void Library< ST, PT >::genModelData (Args::ArrayType *& W)
{
  W = ArrayFactory::createMatrix( 1, 1 );
  const std::vector< double > & sqbfn = linAlgLib_->squareBaseFuncNorms();
  (W->getPr())[0] = sqbfn[0];

} //void gen_model_data

template< class ST, class PT >
template< class DecomposedOperatorType1, class DecomposedOperatorType2 >
inline void Library< ST, PT >::getCoefficientErrorEstimator_matrix (
    DecomposedOperatorType1 &op1, DecomposedOperatorType2 &op2,
    Args::ArrayType & returnArray)
{
  const int numComps1 = op1.numComponents();
  const int numComps2 = op2.numComponents();

  double * return_ptr = returnArray.getPr();
  std::vector< double > coefficients1;
  op1.coefficients( coefficients1 );
  std::vector< double > coefficients2;
  op2.coefficients( coefficients2 );

  for (int comp1 = 0; comp1 != numComps1; ++comp1)
    for (int comp2 = 0; comp2 != numComps2; ++comp2)
      return_ptr[(comp1 * numComps2) + comp2] = coefficients1[comp1]
        * coefficients2[comp2];
}

template< class ST, class PT >
inline void Library< ST, PT >::getEndtimeAndTimesteps (Args::ArrayType* &T,
                                                              Args::ArrayType* &nt)
{
  // set T
  assert( T!=NULL );
  double * T_ptr = T->getPr();
  double endTime = Parameter::getValue< double >( "rb.endTime" );
  *T_ptr = endTime;
  // set nt
  assert( nt!=NULL );
  double * nt_ptr = nt->getPr();
  *nt_ptr = getNoTimeSteps();
}

template< class ST, class PT >
inline double Library< ST, PT >::getDt () const
{
  double dtEst = Parameter::getValue< double >( "rb.dtEst" );
  return dtEst;
}
;

template< class ST, class PT >
template< class ContainerT >
inline void Library< ST, PT >::getMu (ContainerT & muValues)
{
  size_t noMu = mu_names_.size();
  assert( muValues.size() == noMu );
  for (size_t i = 0; i < noMu; ++i)
  {
    double val;
    Dune::Parameter::get( mu_names_[i], val );
    muValues[i] = val;
  }
}

template< class ST, class PT >
inline void Library< ST, PT >::getMu (Args::ArrayType * & muValues)
{
  size_t noMu = mu_names_.size();
  muValues = ArrayFactory::createMatrix( noMu, 1 );
  double * prMu = muValues->getPr();
  for (size_t i = 0; i < noMu; ++i)
  {
    double val;
    Dune::Parameter::get( mu_names_[i], val );
    prMu[i] = val;
  }
}

template< class ST, class PT >
inline int Library< ST, PT >::getNoTimeSteps () const
{
  double endTime = Parameter::getValue< double >( "rb.endTime" );
  double dt = getDt();
  double factor = Parameter::getValue< double >( "fem.timeprovider.factor",
                                                 1.0 );
  int noTimeSteps = static_cast< int >( floor( endTime / (dt * factor) ) );
  noTimeSteps += 1;
  return noTimeSteps;
}
;

template< class ST, class PT >
template< class DecomposedOperatorType >
inline void Library< ST, PT >::getOperatorCoefficients (
    const DecomposedOperatorType& op, Args::ArrayType &returnArray)
{
  int numComps = op.numComponents();
  assert( numComps != 0 );
  double * return_ptr = returnArray.getPr();
  std::vector< double > coefficients;
  op.coefficients( coefficients );
  // now, restrict the operator
  for (int i = 0; i != numComps; ++i)
  {
    return_ptr[i] = coefficients[i];
  }
}

template< class ST, class PT >
template< class DecomposedOperatorType >
inline void Library< ST, PT >::getSymbolicOperatorCoefficients (
    const DecomposedOperatorType& op, Args::ArrayType &returnArray)
{
  int numComps = op.numComponents();
  assert( numComps != 0 );
  std::vector< std::string > coefficients;
  op.symbolicCoefficients( coefficients );
  // now, restrict the operator
  for (int i = 0; i < numComps; ++i)
  {
    returnArray.setCell( i, ArrayFactory::createString( coefficients[i] ) );
  }
}

template< class ST, class PT >
inline void Library< ST, PT >::getRBSize (Args::ArrayType*& rbSize)
{
  ReducedBasisSpace &redBaseSpace = *(redBaseSpace_);

  rbSize = ArrayFactory::createMatrix( 1, 1 );
  double * rbSize_ptr = rbSize->getPr();
  *rbSize_ptr = redBaseSpace.size();

  return;
}

template< class ST, class PT >
inline void Library< ST, PT >::initDataBasis (Args::ArrayType *& num)
{
  typedef typename ModelType::DecomposedInitDataType DecompInitType;
  typedef typename DecompInitType::ComponentType ComponentType;
  typedef double DomainFieldType;
  typedef double RangeFieldType;

  L2Projection< DomainFieldType, RangeFieldType, ComponentType,
      DiscreteFunctionType > l2pro;
  const ModelType & model = this->model();
  const DecompInitType &u0 = model.initialData();
  const int numComponents = u0.numComponents();
  GridPartType gridPart = *gridPart_;
  // write all components of u0 as initial basis functions to reduced basis space
  int num_i = 0;
  for (int i = 0; i != numComponents; ++i)
  {
    DiscreteFunctionType buffer( "init_data_basis", discFuncSpace() );
    l2pro( u0.component( i ), buffer );
    double l2n = linAlgLib_->l2norm( buffer );
    if (l2n != 0.0)
    {
      buffer *= 1.0 / l2n;

      redBaseSpace().addBaseFunction( buffer );
      num_i++;
    }
    else
    {
      std::cerr
          << "Warning: initial data basis candidate turned out to be a null function!"
          << std::endl;
    }
  }
  std::cout << "Added " << num_i
            << " base functions.! Doing now an orthonormalization:\n";
  linAlgLib_->gram_schmidt( redBaseSpace().getFuncList() );
  // if initial data is zero add the constant function as initial basis
  // TODO: change below into a real projection algorithm
  if (num_i == 0)
  {
    DiscreteFunctionType buffer( "init_data_basis", discFuncSpace() );
    typedef typename DiscreteFunctionType::DofIteratorType DofItType;
    DofItType end = buffer.dend();
    for (DofItType dofIt = buffer.dbegin(); dofIt != end; ++dofIt)
    {
      *dofIt = 1.0;
    }
    buffer *= 1.0 / linAlgLib_->l2norm( buffer );
    redBaseSpace().addBaseFunction( buffer );
  }
  num = ArrayFactory::createMatrix( 1, 1 );
  (num->getPr())[0] = num_i;
} // init_data_basis

template< class ST, class PT >
inline void Library< ST, PT >::initModel (Args::ArrayType *& retArray,
                                                 const std::string& rbPath = "")
{

  rbListPath_ = rbPath;

  if (discretization_ != 0) delete discretization_;

  if (redBaseSpace_ != 0) delete redBaseSpace_;

  if (model_ != 0) delete model_;

  /*    Dune :: Parameter :: blockKeys(mu_names_);*/
  model_ = new ModelType( gridPart() );
  initReducedBasisSpace( rbListPath_ );

  discretization_ = new ConvDiffDiscretizationType( discFuncSpace(),
                                                    this->model() );
  /*    Dune :: Parameter :: unblockKeys();*/

  const char * fieldnames[] =
    { "mu_ranges", "mu_names", "T", "nt" };
  const int numfields = sizeof(fieldnames) / sizeof(*fieldnames);
  retArray = ArrayFactory::createStruct( numfields, fieldnames );

  retArray->setField( 0, ArrayFactory::createCell( 1, mu_ranges_.size() ) );
  retArray->setField( 1, ArrayFactory::createCell( 1, mu_ranges_.size() ) );
  Args::ArrayType & ranges_cell = retArray->getField( 0, 0 );
  Args::ArrayType & names_cell = retArray->getField( 0, 1 );

  for (size_t i = 0; i < mu_ranges_.size(); ++i)
  {
    ranges_cell.setCell( i, ArrayFactory::createMatrix( 2, 1 ) );
    double * range_pr = ranges_cell.getCell( i ).getPr();
    range_pr[0] = mu_ranges_[i].from;
    range_pr[1] = mu_ranges_[i].to;

    names_cell.setCell( i, ArrayFactory::createString( mu_names_[i] ) );
  }

  retArray->setField( 2, ArrayFactory::createMatrix( 1, 1 ) );
  retArray->setField( 3, ArrayFactory::createMatrix( 1, 1 ) );

  double * T_ptr = retArray->getField( 0, 2 ).getPr();
  T_ptr[0] = Parameter::getValue< double >( "rb.endTime" );
  double * nt_ptr = retArray->getField( 0, 3 ).getPr();

  nt_ptr[0] = getNoTimeSteps();
}
;

template< class ST, class PT >
inline void Library< ST, PT >::isValidRBSpace (Args::ArrayType *& retArray)
{
  retArray = ArrayFactory::createMatrix( 1, 1 );
  double * answer = retArray->getPr();
  answer[0] = (redBaseSpace_ != 0 && redBaseSpace_->size() > 0);
}

template< class ST, class PT >
inline void Library< ST, PT >::rbInitValues (const int decomp_mode,
                                                    Args::ArrayType * & a0)
{
  assert( decomp_mode == 0 || decomp_mode == 1 || decomp_mode == 2 );
  typedef typename ModelType::DecomposedInitDataType DecompInitType;
  typedef typename DecompInitType::ComponentType ComponentType;
  typedef typename DecompInitType::CompleteType CompleteType;

  typedef typename ReducedBasisSpace::BaseFunctionType BaseFunctionType;
  //     typedef ReducedBasisSpace::BaseFunctionSpaceType                 ReducedFunctionSpace;
  typedef AdaptiveDiscreteFunction< ReducedBasisSpace > ReducedFunction;
  //    typedef ReducedBasisSpace::DiscreteFunctionType                  ReducedFunction;

  typedef typename ReducedFunction::ConstDofIteratorType ConstDofIteratorType;

  const ModelType &model = this->model();
  const DecompInitType &u0 = model.initialData();
  //     const ReducedFunctionSpace &redFuncSpace = (*(redBaseSpace_)).baseFunctionSpace();

  switch (decomp_mode)
  {
  // 0 means complete or not specified
  case 0:
  {
    DiscreteFunctionType complete( "complete", *discFuncSpace_ );
    u0.projectComplete( complete );
    ReducedFunction dest( "reduced_function", (*redBaseSpace_) );
    // restrict the full initial function to the reduced space
    (*(redBaseSpace_)).restrictFunction( complete, dest );

    const unsigned int ndofs = dest.size();
    // now write the result to an array to give it back to matlab
    a0 = ArrayFactory::createCell( 1, 1 );
    a0->setCell( 0, ArrayFactory::createMatrix( 1, ndofs ) );
    Args::ArrayType & cell = a0->getCell( 0 );
    double * cell_ptr = cell.getPr();
    unsigned int i = 0;
    ConstDofIteratorType end = dest.dend();
    for (ConstDofIteratorType it = dest.dbegin(); it != end; ++it, ++i)
    {
      cell_ptr[i] = *it;
    }
  }
    break;

    // 1 means components
  case 1:
  {
    typedef double DomainFieldType;
    typedef double RangeFieldType;
    L2Projection< DomainFieldType, RangeFieldType, ComponentType,
        DiscreteFunctionType > l2pro;
    size_t ncomponents = u0.numComponents();
    ReducedFunction dest( "reduced_function", (*redBaseSpace_) );
    const unsigned int ndofs = dest.size();
    a0 = ArrayFactory::createCell( ncomponents, 1 );

    // iterate over all components
    for (size_t i = 0; i != ncomponents; ++i)
    {
      dest.clear();
      ComponentType component = u0.component( i );

      (*redBaseSpace_).restrictFunction( component, dest );

      a0->setCell( i, ArrayFactory::createMatrix( 1, ndofs ) );
      Args::ArrayType & cell = a0->getCell( i );
      double * cell_ptr = cell.getPr();
      //iterate over all dofs
      int j = 0;
      ConstDofIteratorType end = dest.dend();
      for (ConstDofIteratorType it = dest.dbegin(); it != end; ++it, ++j)
      {
        cell_ptr[j] = *it;
      }
    }
  }
    break;

    // 2 means coefficients
  case 2:
    int ncoefficients = u0.numComponents();
    a0 = ArrayFactory::createMatrix( 1, ncoefficients );
    double* a0_ptr = a0->getPr();
    for (int i = 0; i != ncoefficients; ++i)
    {
      a0_ptr[i] = u0.coefficient( i );
    }
    break;
  } //switch
} // rb_init_values

template< class ST, class PT >
inline void Library< ST, PT >::rbSymbolicCoefficients (
    Args::ArrayType *& symCoeffs)
{
  typedef typename ConvDiffDiscretizationType::ImplicitDecompOperatorType ImplicitOpType;
  typedef typename ConvDiffDiscretizationType::ExplicitDecompOperatorType ExplicitOpType;
  typedef typename ConvDiffDiscretizationType::ConvFVDecompFuncType ConvFVDecompFuncType;
  typedef typename ConvDiffDiscretizationType::DiffFVDecompFuncType DiffFVDecompFuncType;

  typedef typename ModelType::DecomposedInitDataType DecompInitType;

  // get an instance of the discretization
  ConvDiffDiscretizationType &discretization = *discretization_;

  // reserve memory for the return argument
  size_t noOperators = 4;
  symCoeffs = ArrayFactory::createCell( 1, noOperators );

  // get the 'implicit' operator, belonging to diffusion (LL_I)
  ImplicitOpType &implicitOp = discretization.implicitDecompOperator();

  //  get the 'explicit' operator, belonging to convection (LL_E)
  ExplicitOpType &explicitOp = discretization.explicitDecompOperator();

  // get the right hand side vector (b)
  typedef Decomposed::Function::Combined< DiffFVDecompFuncType, ConvFVDecompFuncType > CombinedType;
  ConvDiffDiscretizationType &discretization_nonConst = *discretization_;
  DiffFVDecompFuncType &diffFVDecompFunc = discretization_nonConst
    .diffFVDecompFunc();
  ConvFVDecompFuncType &convFVDecompFunc = discretization_nonConst
    .convFVDecompFunc();
  CombinedType bCombined( diffFVDecompFunc, convFVDecompFunc );

  const ModelType &model = this->model();
  const DecompInitType &u0 = model.initialData();

  /* ============== Implicit Operator ==============*/

  symCoeffs->setCell(
      0, ArrayFactory::createCell( 1, implicitOp.numComponents() ) );
  Args::ArrayType & LL_I = symCoeffs->getCell( 0 );
  getSymbolicOperatorCoefficients( implicitOp, LL_I );

  /* ============ End of implicit Operator =========*/

  /* ============ Explicit Operator ================*/

  symCoeffs->setCell(
      1, ArrayFactory::createCell( 1, explicitOp.numComponents() ) );
  Args::ArrayType & LL_E = symCoeffs->getCell( 1 );
  getSymbolicOperatorCoefficients( explicitOp, LL_E );

  /* ============ End of explicit Operator =========*/

  /*============= b - vector =======================*/

  symCoeffs->setCell(
      2, ArrayFactory::createCell( 1, bCombined.numComponents() ) );
  Args::ArrayType & b = symCoeffs->getCell( 2 );
  getSymbolicOperatorCoefficients( bCombined, b );

  /* ============ End of b - vector ================*/

  /* ============ initial data =====================*/
  symCoeffs->setCell( 3, ArrayFactory::createCell( 1, u0.numComponents() ) );
  Args::ArrayType & uu0 = symCoeffs->getCell( 3 );
  getSymbolicOperatorCoefficients( u0, uu0 );
  /* ============ End of initial data ==============*/

}

template< class ST, class PT >
inline void Library< ST, PT >::rbOperators (const int decomp_mode,
                                                   Args::ArrayType* &operators)
{
  typedef typename StepperType::DiscretizationType DiscretizationType;

  typedef typename ConvDiffDiscretizationType::ImplicitDecompOperatorType ImplicitOpType;

  typedef typename ConvDiffDiscretizationType::ExplicitDecompOperatorType ExplicitOpType;

  typedef typename ConvDiffDiscretizationType::ConvFVDecompFuncType ConvFVDecompFuncType;

  typedef typename ConvDiffDiscretizationType::DiffFVDecompFuncType DiffFVDecompFuncType;

  typedef typename ReducedBasisSpace::GramianPipelineType GramianPipelineType;

  typedef typename GramianPipelineType::OpHandle OpHandle;
  typedef typename GramianPipelineType::FuncHandle FuncHandle;
  // get an instance of the discretization
  //! \todo make discretization and a child variables const again
  ConvDiffDiscretizationType &discretization = *discretization_;

  // reserve memory for the return argument
  size_t noOperators = 9;
  operators = ArrayFactory::createCell( noOperators, 1 );

  const unsigned int rbSize = (*(redBaseSpace_)).size();

  // get the 'implicit' operator, belonging to diffusion (LL_I)
  ImplicitOpType &implicitOp = discretization.implicitDecompOperator();

  //  get the 'explicit' operator, belonging to convection (LL_E)
  ExplicitOpType &explicitOp = discretization.explicitDecompOperator();

  // get the right hand side vector (b)
  typedef Decomposed::Function::Combined< DiffFVDecompFuncType, ConvFVDecompFuncType > CombinedType;
  ConvDiffDiscretizationType &discretization_nonConst = *discretization_;
  DiffFVDecompFuncType &diffFVDecompFunc = discretization_nonConst
    .diffFVDecompFunc();
  ConvFVDecompFuncType &convFVDecompFunc = discretization_nonConst
    .convFVDecompFunc();
  CombinedType bCombined( diffFVDecompFunc, convFVDecompFunc );

  if (decomp_mode == 2)
  {
    /* ============== Implicit Operator ==============*/

    operators->setCell(
        0, ArrayFactory::createMatrix( 1, implicitOp.numComponents() ) );
    Args::ArrayType & LL_I = operators->getCell( 0 );
    getOperatorCoefficients( implicitOp, LL_I );

    /* ============ End of implicit Operator =========*/

    /* ============ Explicit Operator ================*/

    operators->setCell(
        1, ArrayFactory::createMatrix( 1, explicitOp.numComponents() ) );
    Args::ArrayType & LL_E = operators->getCell( 1 );
    getOperatorCoefficients( explicitOp, LL_E );

    /* ============ End of explicit Operator =========*/

    /*============= b - vector =======================*/

    operators->setCell(
        2, ArrayFactory::createMatrix( 1, bCombined.numComponents() ) );
    Args::ArrayType & b = operators->getCell( 2 );
    getOperatorCoefficients( bCombined, b );

    /*============= end of b - vector =================*/

    /*====== A posteriori error estimator matrices, vectors and scalars ========*/

    // K_II
    operators->setCell(
        3,
        ArrayFactory::createMatrix(
            1, implicitOp.numComponents() * implicitOp.numComponents() ) );
    Args::ArrayType & K_II = operators->getCell( 3 );
    getCoefficientErrorEstimator_matrix( implicitOp, implicitOp, K_II );

    // K_EE
    operators->setCell(
        4,
        ArrayFactory::createMatrix(
            1, explicitOp.numComponents() * explicitOp.numComponents() ) );
    Args::ArrayType & K_EE = operators->getCell( 4 );
    getCoefficientErrorEstimator_matrix( explicitOp, explicitOp, K_EE );

    // K_IE
    operators->setCell(
        5,
        ArrayFactory::createMatrix(
            1, implicitOp.numComponents() * explicitOp.numComponents() ) );
    Args::ArrayType & K_IE = operators->getCell( 5 );
    getCoefficientErrorEstimator_matrix( implicitOp, explicitOp, K_IE );

    // m_I
    operators->setCell(
        6,
        ArrayFactory::createMatrix(
            1, implicitOp.numComponents() * bCombined.numComponents() ) );
    Args::ArrayType & m_I = operators->getCell( 6 );
    getCoefficientErrorEstimator_matrix( implicitOp, bCombined, m_I );

    // m_E
    operators->setCell(
        7,
        ArrayFactory::createMatrix(
            1, explicitOp.numComponents() * bCombined.numComponents() ) );
    Args::ArrayType & m_E = operators->getCell( 7 );
    getCoefficientErrorEstimator_matrix( explicitOp, bCombined, m_E );

    // m
    operators->setCell(
        8,
        ArrayFactory::createMatrix(
            1, bCombined.numComponents() * bCombined.numComponents() ) );
    Args::ArrayType & m = operators->getCell( 8 );
    getCoefficientErrorEstimator_matrix( bCombined, bCombined, m );
  }
  else if (decomp_mode == 1)
  {
    GramianPipelineType & pipeline = redBaseSpace().getPipeline();
    OpHandle hIdOp = pipeline.getIdentityHandle();

    // register implicitOp
    std::vector< OpHandle > hIOps;
    for (int i = 0; i < implicitOp.numComponents(); ++i)
    {
      hIOps.push_back(
          pipeline.registerDiscreteOperator( implicitOp.component( i ) ) );
    }
    // register explicitOp
    std::vector< OpHandle > hEOps;
    for (int i = 0; i < explicitOp.numComponents(); ++i)
    {
      hEOps.push_back(
          pipeline.registerDiscreteOperator( explicitOp.component( i ) ) );
    }
    // register RHS b
    std::vector< FuncHandle > hbFuncs;
    for (int i = 0; i < bCombined.numComponents(); ++i)
    {
      hbFuncs.push_back(
          pipeline.registerDiscreteFunction( bCombined.component( i ) ) );
    }

    // LL_I
    size_t cellsize = hIOps.size();
    operators->setCell( 0, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop = operators->getCell( 0 );
    for (unsigned int i = 0; i < hIOps.size(); ++i)
    {
      cellop.setCell( i, ArrayFactory::createMatrix( rbSize, rbSize ) );
      Args::ArrayType & temp = cellop.getCell( i );
      RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, rbSize, 0.0 );
      pipeline.addGramMatrixComputation( hIdOp, hIOps[i], mxmatrix );
    }
    // LL_E
    cellsize = hEOps.size();
    operators->setCell( 1, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_E = operators->getCell( 1 );
    for (unsigned int i = 0; i < hEOps.size(); ++i)
    {
      cellop_E.setCell( i, ArrayFactory::createMatrix( rbSize, rbSize ) );
      Args::ArrayType & temp = cellop_E.getCell( i );
      RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, rbSize, 0.0 );
      pipeline.addGramMatrixComputation( hIdOp, hEOps[i], mxmatrix );
    }
    // b vector
    cellsize = hbFuncs.size();
    operators->setCell( 2, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_b = operators->getCell( 2 );
    for (unsigned int i = 0; i < hbFuncs.size(); ++i)
    {
      cellop_b.setCell( i, ArrayFactory::createMatrix( rbSize, 1 ) );
      Args::ArrayType & temp = cellop_b.getCell( i );
      RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, 1, 0.0 );
      pipeline.addVectorComputation( hIdOp, hbFuncs[i], mxmatrix );
    }
    /*====== A posteriori error estimator matrices, vectors and scalars ========*/

    // K_II
    cellsize = hIOps.size() * hIOps.size();
    operators->setCell( 3, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_II = operators->getCell( 3 );
    for (unsigned int i = 0; i < hIOps.size(); ++i)
    {
      for (unsigned int j = 0; j < hIOps.size(); ++j)
      {
        cellop_II.setCell( i * hIOps.size() + j,
                           ArrayFactory::createMatrix( rbSize, rbSize ) );
        Args::ArrayType & temp = cellop_II.getCell( i * hIOps.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, rbSize, 0.0 );
        pipeline.addGramMatrixComputation( hIOps[i], hIOps[j], mxmatrix );
      }
    }
    // K_EE
    cellsize = hEOps.size() * hEOps.size();
    operators->setCell( 4, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_EE = operators->getCell( 4 );
    for (unsigned int i = 0; i < hEOps.size(); ++i)
    {
      for (unsigned int j = 0; j < hEOps.size(); ++j)
      {
        cellop_EE.setCell( i * hEOps.size() + j,
                           ArrayFactory::createMatrix( rbSize, rbSize ) );
        Args::ArrayType & temp = cellop_EE.getCell( i * hEOps.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, rbSize, 0.0 );
        pipeline.addGramMatrixComputation( hEOps[i], hEOps[j], mxmatrix );
      }
    }
    // K_IE
    cellsize = hIOps.size() * hEOps.size();
    operators->setCell( 5, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_IE = operators->getCell( 5 );
    for (unsigned int i = 0; i < hIOps.size(); ++i)
    {
      for (unsigned int j = 0; j < hEOps.size(); ++j)
      {
        cellop_IE.setCell( i * hEOps.size() + j,
                           ArrayFactory::createMatrix( rbSize, rbSize ) );
        Args::ArrayType & temp = cellop_IE.getCell( i * hEOps.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, rbSize, 0.0 );
        pipeline.addGramMatrixComputation( hIOps[i], hEOps[j], mxmatrix );
      }
    }
    // m_I
    cellsize = hIOps.size() * hbFuncs.size();
    operators->setCell( 6, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_mI = operators->getCell( 6 );
    for (unsigned int i = 0; i < hIOps.size(); ++i)
    {
      for (unsigned int j = 0; j < hbFuncs.size(); ++j)
      {
        cellop_mI.setCell( i * hbFuncs.size() + j,
                           ArrayFactory::createMatrix( rbSize, 1 ) );
        Args::ArrayType & temp = cellop_mI.getCell( i * hbFuncs.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, 1, 0.0 );
        pipeline.addVectorComputation( hIOps[i], hbFuncs[j], mxmatrix );
      }
    }
    // m_E
    cellsize = hEOps.size() * hbFuncs.size();
    operators->setCell( 7, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_mE = operators->getCell( 7 );
    for (unsigned int i = 0; i < hEOps.size(); ++i)
    {
      for (unsigned int j = 0; j < hbFuncs.size(); ++j)
      {
        cellop_mE.setCell( i * hbFuncs.size() + j,
                           ArrayFactory::createMatrix( rbSize, 1 ) );
        Args::ArrayType & temp = cellop_mE.getCell( i * hbFuncs.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), rbSize, 1, 0.0 );
        pipeline.addVectorComputation( hEOps[i], hbFuncs[j], mxmatrix );
      }
    }
    // m
    cellsize = hbFuncs.size() * hbFuncs.size();
    operators->setCell( 8, ArrayFactory::createCell( cellsize, 1 ) );
    Args::ArrayType & cellop_m = operators->getCell( 8 );
    for (unsigned int i = 0; i < hbFuncs.size(); ++i)
    {
      for (unsigned int j = 0; j < hbFuncs.size(); ++j)
      {
        cellop_m.setCell( i * hbFuncs.size() + j,
                          ArrayFactory::createMatrix( 1, 1 ) );
        Args::ArrayType & temp = cellop_m.getCell( i * hbFuncs.size() + j );
        RB::CMatrixWrapper mxmatrix( temp.getPr(), 1, 1, 0.0 );
        /*          std::cout << mxmatrix << std::endl;*/
        pipeline.addScalarComputation( hbFuncs[i], hbFuncs[j], mxmatrix );
      }
    }
    redBaseSpace().runPipeline();

  }
  else if (decomp_mode == 0)
  {
    GramianPipelineType & pipeline = redBaseSpace().getPipeline();
    OpHandle hIdOp = pipeline.getIdentityHandle();

    // register implicitOp
    OpHandle hIOp = pipeline.registerDiscreteOperator(
        *(implicitOp.complete()) );

    // register explicitOp
    OpHandle hEOp = pipeline.registerDiscreteOperator(
        *(explicitOp.complete()) );

    // register RHS b
    FuncHandle hbFunc = pipeline.registerDiscreteFunction(
        bCombined.complete() );

    // LL_I
    operators->setCell( 0, ArrayFactory::createMatrix( rbSize, rbSize ) );
    Args::ArrayType & temp_I = operators->getCell( 0 );
    RB::CMatrixWrapper mxmatrix_I( temp_I.getPr(), rbSize, rbSize, 0.0 );
    pipeline.addGramMatrixComputation( hIdOp, hIOp, mxmatrix_I );
    // LL_E
    operators->setCell( 1, ArrayFactory::createMatrix( rbSize, rbSize ) );
    Args::ArrayType & temp_E = operators->getCell( 1 );
    RB::CMatrixWrapper mxmatrix_E( temp_E.getPr(), rbSize, rbSize, 0.0 );
    pipeline.addGramMatrixComputation( hIdOp, hEOp, mxmatrix_E );
    // b vector
    operators->setCell( 2, ArrayFactory::createMatrix( rbSize, 1 ) );
    Args::ArrayType & temp_b = operators->getCell( 2 );
    RB::CMatrixWrapper mxmatrix_b( temp_b.getPr(), rbSize, 1, 0.0 );
    pipeline.addVectorComputation( hIdOp, hbFunc, mxmatrix_b );
    /*====== A posteriori error estimator matrices, vectors and scalars ========*/

    // K_II
    operators->setCell( 3, ArrayFactory::createMatrix( rbSize, rbSize ) );
    Args::ArrayType & temp_II = operators->getCell( 3 );
    RB::CMatrixWrapper mxmatrix_II( temp_II.getPr(), rbSize, rbSize, 0.0 );
    pipeline.addGramMatrixComputation( hIOp, hIOp, mxmatrix_II );
    // K_EE
    operators->setCell( 4, ArrayFactory::createMatrix( rbSize, rbSize ) );
    Args::ArrayType & temp_EE = operators->getCell( 4 );
    RB::CMatrixWrapper mxmatrix_EE( temp_EE.getPr(), rbSize, rbSize, 0.0 );
    pipeline.addGramMatrixComputation( hEOp, hEOp, mxmatrix_EE );
    // K_IE
    operators->setCell( 5, ArrayFactory::createMatrix( rbSize, rbSize ) );
    Args::ArrayType & temp_IE = operators->getCell( 5 );
    RB::CMatrixWrapper mxmatrix_IE( temp_IE.getPr(), rbSize, rbSize, 0.0 );
    pipeline.addGramMatrixComputation( hIOp, hEOp, mxmatrix_IE );
    // m_I
    operators->setCell( 6, ArrayFactory::createMatrix( rbSize, 1 ) );
    Args::ArrayType & temp_mI = operators->getCell( 6 );
    RB::CMatrixWrapper mxmatrix_mI( temp_mI.getPr(), rbSize, 1, 0.0 );
    pipeline.addVectorComputation( hIOp, hbFunc, mxmatrix_mI );
    // m_E
    operators->setCell( 7, ArrayFactory::createMatrix( rbSize, 1 ) );
    Args::ArrayType & temp_mE = operators->getCell( 7 );
    RB::CMatrixWrapper mxmatrix_mE( temp_mE.getPr(), rbSize, 1, 0.0 );
    pipeline.addVectorComputation( hEOp, hbFunc, mxmatrix_mE );
    // m
    operators->setCell( 8, ArrayFactory::createMatrix( 1, 1 ) );
    Args::ArrayType & temp_m = operators->getCell( 8 );
    RB::CMatrixWrapper mxmatrix_m( temp_m.getPr(), 1, 1, 0.0 );
    /*          std::cout << mxmatrix << std::endl;*/
    pipeline.addScalarComputation( hbFunc, hbFunc, mxmatrix_m );

    // run the pipeline
    redBaseSpace().runPipeline();
  }
  else
  {
    ServerType::printErrMsg( "decomp_mode unknown (must be 0,1 or 2)" );
  }
}

template< class ST, class PT >
template< class DiscreteFunctionListImp >
inline void Library< ST, PT >::rbReconstruction (
    const Args::ArrayType & coefficients, DiscreteFunctionListImp & reconsList)
{
  ReducedBasisSpace &redBaseSpace = *(redBaseSpace_);
  DiscFuncSpaceType &discFuncSpace = *(discFuncSpace_);

  unsigned int noTimeSteps = coefficients.getN(); // mxGetN = number of cols
  unsigned int noBaseFunctions = coefficients.getM(); // mxGetM = number of rows
  // make sure we have got one coefficient for each base function
  assert( noBaseFunctions == redBaseSpace.numBaseFunctions() );
  const double * coeff = coefficients.getPr();
  for (unsigned int col = 0; col != noTimeSteps; ++col)
  {
    DiscreteFunctionType reconstruction( "reconstruction", discFuncSpace );
    reconstruction.clear();
    for (unsigned int row = 0; row != noBaseFunctions; ++row, ++coeff)
    {
      DiscreteFunctionType baseFunc( "baseFunc", discFuncSpace );
      baseFunc.clear();
      redBaseSpace.baseFunction( row, baseFunc );
      baseFunc *= *coeff;
      reconstruction += baseFunc;
    }
    reconsList.push_back( reconstruction );
  }
}

/*template<class ST, class PT>
 *inline void LinEvolDefault<ST, PT>::rbReductionError( const T::ArrayType & coefficients, T::ArrayType & ret )
 *{
 *  DiscreteFunctionListType reconsList;
 *  rbReconstruction(coefficients, reconsList);

 *}*/

template< class ST, class PT >
inline int Library< ST, PT >::rbExtensionPCA (const Args::ArrayType & mu,
                                                     const int k,
                                                     double percentage)
{
  //create a function list with candidates for the rb extension
  // for a memory function list, get number of time steps as right size for list
  int noTimeSteps = getNoTimeSteps();
  DiscreteFunctionList_MEM U( *(discFuncSpace_), noTimeSteps );
  //    DiscreteFunctionList_XDR U( *(discFuncSpace_), "Candidates" );
  //now, start a detailed simulation for the given mu-vector
  std::vector< double > oldMu( mu_names_.size() );
  getMu( oldMu );
  assert( mu.getN()*mu.getM() == mu_names_.size() );
  setMu( mu.getPr() );
  solveDetailed( U );
  setMu( oldMu );

  return linAlgLib_->pca( U, redBaseSpace_->getFuncList(), k, percentage );
}

template< class ST, class PT >
void Library< ST, PT >::rbReconstruction (
    const Args::ArrayType & detailed_data, const Args::ArrayType & coefficients,
    Args::ArrayType * & mxReturn)
{
  DiscFuncSpaceType &discFuncSpace = *(discFuncSpace_);

  /*TODO: check if RB needs to be loaded.*/

  /*Maybe we do not need to instantiate a discrete function list here.*/
  DiscreteFunctionList_XDR reconsList( discFuncSpace, "Reconstruction", true );
  rbReconstruction( coefficients, reconsList );

  /*TODO: read the filename for the reconstruction out of the detailed_data.*/
  DiscreteFunctionType reduced( "reduced_vtk", discFuncSpace );
  typedef tuple< DiscreteFunctionType* > DataIOTupleType;
  DataIOTupleType dataIO( &reduced );
  typedef DataOutput< GridType, DataIOTupleType > DataWriterType;
  GridTimeProvider< GridType > tp( 0, **(gridPtr_) );
  DataWriterType dataWriter( **(gridPtr_), dataIO, tp );

  const double endtime = Parameter::getValue< double >( "rb.endTime" );
  const double dt = getDt();
  tp.init( dt );
  for (tp.provideTimeStepEstimate( dt ); tp.time() < endtime;
      tp.next(), tp.provideTimeStepEstimate( dt ))
  {
    reduced.clear();
    reconsList.getFunc( tp.timeStep(), reduced );

    dataWriter.write();
  }

  /*  trajectoryToMatlab(reconsList, mxReturn);*/

  this->fillStructWithOutputData( mxReturn, "reconstruction-0" );

  reconsList.clear();
}

template< class ST, class PT >
inline void Library< ST, PT >::reconstructAndCompare (
    const Args::ArrayType &coefficients)
{
  DiscFuncSpaceType &discFuncSpace = *(discFuncSpace_);
  DiscreteFunctionType reduced( "reduced", discFuncSpace );
  //  DiscreteFunctionType detailed("detailed", discFuncSpace);

  typedef tuple< DiscreteFunctionType* > DataIOTupleType;
  DataIOTupleType dataIO( &reduced );
  typedef DataOutput< GridType, DataIOTupleType > DataWriterType;
  GridTimeProvider< GridType > tp( 0, **(gridPtr_) );
  DataWriterType dataWriter( **(gridPtr_), dataIO, tp );

  const double endtime = Parameter::getValue< double >( "rb.endTime" );
  const double dt = getDt();

  DiscreteFunctionList_MEM reconsList( discFuncSpace, getNoTimeSteps() );
  //    DiscreteFunctionList_XDR reconsList(discFuncSpace, "Reconstruction");
  // call rb_reconstruction to reconstruct to current coefficients
  rbReconstruction( coefficients, reconsList );
  // now perfom a detailed simulation to the current parameter
  //DiscreteFunctionList_MEM detailedSimulation(discFuncSpace, noTimeSteps);
  //     DiscreteFunctionList_XDR detailedSimulation(discFuncSpace, "Detailed_Simulation");

  //solveDetailed(detailedSimulation);
  //assert(reconsList.size() == detailedSimulation.size());

  tp.init( dt );
  for (tp.provideTimeStepEstimate( dt ); tp.time() < endtime;
      tp.next(), tp.provideTimeStepEstimate( dt ))
  {
    //detailed.clear();
    //detailedSimulation.getFunc(timestep, detailed);
    reduced.clear();
    reconsList.getFunc( tp.timeStep(), reduced );

    dataWriter.write();
    //reduced -= detailed;
    //double l2error = linAlgLib_->l2norm(reduced);
    //std :: ostringstream oss;
    //oss << "timestep: " << timestep << " l2Error: " << l2error << "\n";
    //ServerType :: printStatusMsg(oss.str());
  }
} // reconsAndCompare

template< class ST, class PT >
template< class ContainerT >
inline void Library< ST, PT >::setMu (
    const std::vector< std::string > & mu_names, const ContainerT & mu_values)
{
#ifndef NDEBUG
  bool inRange = checkInsideMuRange( mu_values );
  assert( inRange );
#endif
  for (unsigned int i = 0; i < mu_names.size(); ++i)
  {
    Parameter::replaceKey( mu_names[i], mu_values[i] );
  }
}

template< class ST, class PT >
template< class ContainerT >
inline void Library< ST, PT >::setMu (const ContainerT & mu_values)
{
  setMu( mu_names_, mu_values );
}

template< class ST, class PT >
inline void Library< ST, PT >::setMu (const Args::ArrayType & mu_values)
{
  unsigned int values_size = std::max( mu_values.getN(), mu_values.getM() );

  if (values_size != mu_names_.size())
  {
    std::ostringstream oss;
    oss << "Size of Mu vector should be " << mu_names_.size() << " ["
        << values_size << "!=" << mu_names_.size() << "]\n";
    ServerType::printErrMsg( oss.str() );
  }

  setMu( mu_names_, mu_values.getPr() );
}

template< class ST, class PT >
inline void Library< ST, PT >::solveDetailed (Args::ArrayType * &U)
{
  typedef typename DiscreteFunctionType::DofIteratorType DofIteratorType;

  DiscFuncSpaceType &discFuncSpace = *(discFuncSpace_);
  ModelType &model = *model_;
  ConvDiffDiscretizationType &discr = *discretization_;

  SolverType solver( model, discr );
  DiscreteFunctionList_XDR discFuncList( discFuncSpace, "detailed_sim", true );
  solver.solve( discFuncList );

  /*  trajectoryToMatlab(discFuncList, U);*/

  DiscreteFunctionType detailed( "detailed_vtk", discFuncSpace );
  typedef tuple< DiscreteFunctionType* > DataIOTupleType;
  DataIOTupleType dataIO( &detailed );
  typedef DataOutput< GridType, DataIOTupleType > DataWriterType;
  GridTimeProvider< GridType > tp( 0, **(gridPtr_) );
  DataWriterType dataWriter( **(gridPtr_), dataIO, tp );
  const double endtime = Parameter::getValue< double >( "rb.endTime" );
  const double dt = getDt();
  tp.init( dt );
  for (tp.provideTimeStepEstimate( dt ); tp.time() < endtime;
      tp.next(), tp.provideTimeStepEstimate( dt ))
  {
    detailed.clear();
    discFuncList.getFunc( tp.timeStep(), detailed );

    dataWriter.write();
  }

  this->fillStructWithOutputData( U, "detailed_sim-0" );
  // delete discFuncList from hard disk
  discFuncList.clear();
} // solve detailed

template< class ST, class PT >
template< class DiscreteFunctionListImp >
inline void Library< ST, PT >::solveDetailed (DiscreteFunctionListImp &U)
{
  ModelType &model = *model_;
  ConvDiffDiscretizationType &discr = *discretization_;

  SolverType solver( model, discr );
  solver.solve( U );
}

}  // namespace LinEvol
}  // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

/* vim: set sw=2 et: */
