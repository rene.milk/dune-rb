#ifndef  __LINEVOL_DEFAULT_HH__
#define  __LINEVOL_DEFAULT_HH__

#include "../../misc/clapack.h"

#include <dune/rb/matlabcomm/library/ilinevol.hh>

#include <dune/grid/utility/gridtype.hh>

// include for GlobalRefine
#include <dune/fem/space/common/adaptmanager.hh>

#include <dune/fem/misc/l1error.hh>

#include "../reducedbasisspace/reducedbasisspace.hh"
#include "../../misc/discfunclist/discfunclist_mem.hh"
#include "../../misc/discfunclist/discfunclistsubsetwrapper.hh"
#include <dune/rb/matlabcomm/serializedarray/cmatrixwrapper.hh>
#include "../../misc/linearalgebra.hh"

#if HAVE_GRAPE
#include <dune/grid/io/visual/grapedatadisplay.hh>
#endif
#include  <cstdlib>

//#define RB_DEBUG
//
namespace Dune
{
namespace RBFem
{
namespace Example
{
namespace LinEvol
{


/** \class   Library
 *  \ingroup linevol
 *  @brief   provides all functionality needed for a reduced basis simulation of
 *           a linear evolution problem
 *
 *  An implementation for a dune-rb/RBmatlab for a linear evolution problem.
 *  It should be used with LinEvolFacade.
 *
 *  @rbparam{rb.gridFile, string name for a dgf grid file}
 *  @rbparam{rb.mu_names, vector of string holding the parameter names}
 *  @rbparam{rb.mu_vectors, vector of intervals holding the parameter ranges}
 *  @rbparam{rb.startLevel, initial grid refinement steps, 0}
 *  @rbparam{rb.endTime, end of time discretization interval}
 *  @rbparam{rb.dtEst, estimated time step length}
 */
template< class ServerTraitsImp, class ProblemTraitsImp >
class Library: public RB::MatlabComm::Library::ILinEvol< ServerTraitsImp >
{
private:
  typedef ServerTraitsImp ServerTraits;
  typedef ProblemTraitsImp ProblemType;
  typedef Library< ServerTraits, ProblemType > ThisType;
  typedef RB::MatlabComm::Server::Interface< typename ServerTraits::ServerType > ServerType;
  typedef RB::MatlabComm::SerializedArray::IFactory< typename ServerTraits::ArrayFactory > ArrayFactory;
  typedef RB::MatlabComm::Server::Args Args;
  typedef typename ProblemType::DiscFuncSpaceType DiscFuncSpaceType;
  typedef typename ProblemType::GridPartType GridPartType;
  typedef typename GridPartType::GridType GridType;
  typedef typename ProblemType::SolverType SolverType;
  typedef typename SolverType::StepperType StepperType;
  typedef LA::Library< ServerType, DiscFuncSpaceType > LinAlgLib;

public:
  typedef typename StepperType::DiscreteFunctionType DiscreteFunctionType;
  typedef Space::Implementation< DiscreteFunctionType > ReducedBasisSpace;
  typedef typename DiscFuncSpaceType::BaseFunctionSetType BaseFunctionSetType;
  typedef typename DiscreteFunctionType::RangeType RangeType;
  typedef typename SolverType::DiscreteFunctionListType DiscreteFunctionList_XDR;
  typedef FunctionList::Memory< SolverType > DiscreteFunctionList_MEM;
  typedef typename DiscreteFunctionType::ConstDofIteratorType ConstDofIterator;
  typedef typename StepperType::ModelType ModelType;
  typedef typename StepperType::DiscretizationType ConvDiffDiscretizationType;

public:
  /** constructor
   */
  Library ();

  /** destructor
   */
  ~Library ();

private:
  //Prohibit copying and assignment
  inline Library (const ThisType &);
  inline ThisType & operator= (const ThisType &);

  //====================================
  // all private methods, sorted by name
  //====================================
private:

  template< class ContainerT >
  bool checkInsideMuRange (const ContainerT & muvalues) const;

  ConvDiffDiscretizationType & discretization ()
  {
    return *discretization_;
  }

  const ConvDiffDiscretizationType & discretization () const
  {
    return *discretization_;
  }

  /** @brief initialize the internal reduced basis space
   *
   *  @param[in] rbPath   optional header file from where the reduced basis
   *                      functions can be read in, respectively are written
   *                      to.
   */
  void initReducedBasisSpace (const std::string rbPath);

  /** @brief perform the Gram-Schmidt algorithm
   *
   *  Orthonormalize the space spanned by discrete functions
   *  given as a discrete function list.
   *  \todo can we put this in a linear algebra utility class?
   *
   *  @param[in, out]  funcs the list of functions  that span the
   *                         space you want to be orthonormalized.
   *                         These functions are overwritten with
   *                         the orthonormalized ones.
   */
  template< class DiscFuncListType >
  inline void gram_schmidt (DiscFuncListType &funcs);

  //! \todo docme!
  GridPartType & gridPart ()
  {
    return *gridPart_;
  }

  const GridPartType & gridPart () const
  {
    return *gridPart_;
  }

  //! \todo docme!
  ModelType & model ()
  {
    return *model_;
  }

  const ModelType & model () const
  {
    return *model_;
  }

  /** @brief return the reduced basis space
   *
   *  @return a reference to the reduced basis space
   */
  ReducedBasisSpace & redBaseSpace ()
  {
    return *redBaseSpace_;
  }

  /** @brief return the reduced basis space
   *
   *  @return a const reference to the reduced basis space
   */
  const ReducedBasisSpace & redBaseSpace () const
  {
    return *redBaseSpace_;
  }

  /** @brief copies the degrees of freedom of trajectory stored in a discrete
   * function list in a matlab array.
   *
   * @param[in] trajectory   the discrete function list storing the trajectory
   * @param[out] U           a matlab array storing the dofs
   */
  inline void trajectoryToMatlab (const DiscreteFunctionList_XDR & trajectory,
                                  Args::ArrayType * &U) const;

  //===================================
  // all public methods, sorted by name
  //===================================
public:

  /** @brief Delete the files, written by 
   the reduced basis space, from disk.
   */
  void clearReducedSpaceFiles ();

  DiscFuncSpaceType & discFuncSpace ()
  {
    return *discFuncSpace_;
  }

  const DiscFuncSpaceType & discFuncSpace () const
  {
    return *discFuncSpace_;
  }

  /**
   * @brief local entry point for calls from RBMatlab
   *
   * This method by the surrounding facade (probably LinEvolFacade), when the
   * Matlab user requested an operation that is not registered in this facades.
   * This allows us to add non-standard and more specific operations here.
   *
   * @param[in] operation  the name of the operation the user requested in
   *                       RBmatlab
   * @param[out] alhs      vector of T::ArrayType arguments (lhs)
   * @param[in] arhs       vector of T::ArrayType arguments (rhs)
   *
   * @return               boolean indicating whether the requested operation is
   *                       known to this class.
   */
  bool mexFunction (const std::string operation, Args::LArgType & alhs,
                    const Args::RArgType & arhs)
  {
    if (operation.compare( "reconstruct_and_compare" ) == 0)
    {
      reconstructAndCompare( *arhs[1] );
      return true;
    }

    return false;
  }

  /**
   * @brief runs a detailed simulation of the numerical scheme for parameters
   * that need to be set beforehand via the set_mu method.
   * The solution is written to U, such that it is readable by
   * RBMatlab.
   *
   * @param[out] U      reference to the DoFs of the DiscreteFunction
   */
  inline void detailedSimulation (Args::ArrayType * &U);

  /**
   * @brief if there is a known exact solution, this methods calculates the
   * error of the discrtized solution \a U at time \a time
   *
   * @param[in] u      reference to the discretized solution
   * @param[in] time   time at which the discretized solution shall be compared
   *                   to exact solution
   * @return           l1 error between exact and discretized solution
   */
  inline double errorToExact (DiscreteFunctionType & u, double time);

  /**
   * @brief expand current reduced basis with a given discrete function
   *
   * @param[in] dofVec a matlab dof vec, to be stored in basis as discrete function
   * @param[out] ret give back the new reduced basis
   */
  inline void expandRB (const Args::ArrayType & dofVec, Args::ArrayType * &ret);

  /** @brief generate model data
   *
   * This computes the scalar products \f$ \langle\phi_i | \phi_i\rangle\f$ where \f$\phi_i\f$
   * is the i'th basis function of the discrete function space. The values are saved internally
   * and used e.g. for compunting the scalar product of two functions.
   *
   * param[out] W        mass of one cell, can be used for scalar products...
   *
   */
  inline void genModelData (Args::ArrayType *& W);

  /** @brief returns a "gramian" matrix of all coefficient evaluations for
   * the two given decomposed operators.
   *
   * @param op1          first decomposed operator
   * @param op2          second decomposed operator
   * @param returnArray  matrix structure for the gramian
   */
  template< class DecomposedOperatorType1, class DecomposedOperatorType2 >
  inline void getCoefficientErrorEstimator_matrix (DecomposedOperatorType1 &op1,
                                                   DecomposedOperatorType2 &op2,
                                                   Args::ArrayType &returnArray);

  /**
   * @brief give end time and number of timesteps to matlab
   *
   * @param[out] T   mxArray where end time is stored
   * @param[out] nt  mxArray where number of timesteps is stored
   */
  inline void getEndtimeAndTimesteps (Args::ArrayType* &T, Args::ArrayType* &nt);

  /** @brief return timestep size
   *
   *
   *  @return timestep size
   */
  inline double getDt () const;

  /** @brief return number of numbersteps
   *
   *  This is only an approximation given by
   *  ceil(endtime/ dt estimation), where endtime and
   *  dt estimation are taken from the parameter file.
   *
   *  @return number of timesteps
   */
  inline int getNoTimeSteps () const;

  /**
   * @brief write coefficient evaluations of decomposed operator in a matrix
   * structure.
   *
   * @param[in]  op           decomposed operator
   * @param[out] returnArray  storage container for coefficient evaluations
   */
  template< class DecomposedOperatorType >
  inline void getOperatorCoefficients (const DecomposedOperatorType& op,
                                       Args::ArrayType &returnArray);

  /**
   * @brief write symbolicCoefficient evaluations of decomposed operator in
   * a cell structure.
   *
   * @param[in]  op           decomposed operator
   * @param[out] returnArray  storage container for symbolicCoefficient
   *                          evaluations
   */
  template< class DecomposedOperatorType >
  inline void getSymbolicOperatorCoefficients (const DecomposedOperatorType& op,
                                               Args::ArrayType &returnArray);
  /**
   * @brief returns a vector of parameter values corresponding to the values in
   * mu_names_
   *
   * @param[out] mu_values      T::ArrayType holding mu values
   */
  inline void getMu (Args::ArrayType *& mu_values);

  /**
   * @brief returns a vector of parameter values corresponding to the values in
   * mu_names
   *
   * @param[out] muValues      Conatiner holding mu values
   */
  template< class ContainerT >
  inline void getMu (ContainerT & muValues);

  /** @brief get the current size of the reduced basis space
   *
   *  @return returns the number of base functions in the reduced space
   */
  inline void getRBSize (Args::ArrayType *& rbSize);

  //! returns the grid
  inline GridSelector::GridType & grid ()
  {
    return **gridPtr_;
  }
  ;

  /** @brief initialize the reduced basis with the data function \f$u_0\f$
   *
   *  each component, that is the space-dependent part, of u0 is added as
   *  a function to the reduced basis space
   */
  inline void initDataBasis (Args::ArrayType *& num);

  /**
   * @brief needs to be run at the beginning of the program in order to
   * initialize the grid and the Dune::Parameter singleton
   */
  inline void initModel (Args::ArrayType *& retArray, const std::string&);

  /**
   * @brief checks wether a valid reduced basis space has beenn loaded
   */
  inline void isValidRBSpace (Args::ArrayType *& retArray);

  /**
   * @brief extracts a solution from a DiscreteFunctionList identified by the
   * id 'savepath' and makes the DoF vector readable by RBmatlab.
   *
   * @param[in] savepath a string id for the DiscreteFunctionList
   * @param[in] params   a Matlab mxArray structure
   * @param[out] U       DoF Vector of solution
   */
  inline void loadDetailedSimulation (std::string savepath,
                                      const Args::ArrayType & params,
                                      Args::ArrayType & U);

  /** @brief pca fixspace algorithm
   *
   *  \note This uses Lapacks dseyv.
   *
   *  \todo can we maybe put his method inside a linear algebra class?
   *
   *  @param[in]        U         a list of functions, a function in span(U)
   *                              will be used to expand the space specified by oldSpace
   *  @param[in, out]   oldSpace  the space which is to be expanded
   *  @param[in]        k         optional: the number of functions to be added to the space (default: 1)
   *
   */
  template< class DiscreteFunctionListImp >
  inline void PCA_fixspace (DiscreteFunctionListImp &U,
                            ReducedBasisSpace &oldSpace, int k = 1);

  /**
   * @brief projects the inital values onto the reduced basis space
   *
   * @param[in] decomp_mode determines whether the complete function, only
   *                        the components or only the coefficients are
   *                        projected
   * @param[out] a0         an mxArray where the projected values will be written
   */
  inline void rbInitValues (const int decomp_mode, Args::ArrayType * & a0);

  /**
   * @brief returns offline operators that can be used by rb_simulation in RBmatlab.
   * This method can be seen to be the equivalent of gen_online_data in RBmatlab.
   *
   * @param[in]  decomp_mode integer indicating whether the complete operator (0), only
   *                         the operator components (1) or only the coefficient vector(2)
   *                         shall be returned.
   * @param[out] operators   components, coefficients or complete data
   */
  inline void rbOperators (const int decomp_mode, Args::ArrayType *& operators);

  /**
   * @brief returns symbolic coefficient strings for offline operators
   *
   * This method can be seen as a substitute for rbOperators() in
   * decomp_mode = 'coefficients'.
   *
   * @param  symCoeffs   cell structure holding the symbolic coefficient
   *                     strings
   */
  inline void rbSymbolicCoefficients (Args::ArrayType *& symCoeffs);

  /**
   * @brief returns offline operators that can be used by rb_simulation in
   * RBmatlab.
   *
   * This method can be seen to be the equivalent of gen_online_data in
   * RBmatlab.
   *
   * @param[in] decomp_mode integer indicating wheter the complete operator (0), only
   * @param[out] operators   online data (independent of H)
   *                         the operator components (1) or only the coefficient vector
   *                         shall be returned.
   */
  inline void rbOperatorsNew (const int decomp_mode, Args::ArrayType* &operators);
//
//  /**
//   * @brief reconstructs a reduced discrete function given by the coefficient
//   * vector 'coefficients'.
//   *
//   * @param[in]  coefficients  coefficient vector \f$[\mathbf{a}^k]\f$
//   * @param[out] reconsList    a list containing the reconstructed functions for each
//   *                           timestep. \f$[U_N^k = \sum_{i=0}^N a_i^k \varphi_i]\f$.
//   */
//  template< class DiscreteFunctionListImp >
//  inline void rbReconstruction( const T::ArrayType * coefficients,
//                                DiscreteFunctionListImp & reconsList);

  /** @brief extend the reduced space
   *
   *  Given a vector of parameter values, this function computes the high dimensional
   *  solutions to these values and, using the pca algorithm, chooses one function
   *  which is then added to the reduced basis.
   *
   *  @param[in] mu an mxArray* of parameter values
   *  @param[in] k  parameter defining the number of functions to be added to
   *                the space
   *  @param[in] percentage optional parameter defining the percentage of total
   *                variance covered by the span of the principal components
   *                (only used in case of k==-1)
   *  @return number of added principal components
   */
  inline int rbExtensionPCA (const Args::ArrayType & mu, const int k,
                             double percentage = 0.98);

  /** @brief reconstructs a reduced simulation in the high dimensional function
   * space @f$W_H\f$.
   *
   * @param detailed_data       Matlab struct representing the current reduced
   *                            basis space.
   * @param coefficients        coefficient vectors @f$\mathbf{a}^k@f$ for some
   *                            time steps @f$k@f$, that ought to be
   *                            reconstructed.
   * @param ret                 matlab array vector with dofs of reconstructed
   *                            function
   */
  inline void rbReconstruction (const Args::ArrayType & detailed_data,
                                const Args::ArrayType & coefficients,
                                Args::ArrayType *& ret);

  /** @brief reconstructs a reduced simulation in the high dimensional function
   * space @f$W_H\f$.
   *
   * @param coefficients[in]    coefficient vectors @f$\mathbf{a}^k@f$ for some
   *                            time steps @f$k@f$, that ought to be
   *                            reconstructed.
   * @param reconsList[out]     discrete function list
   */
  template< class DiscreteFunctionListImp >
  inline void rbReconstruction (const Args::ArrayType & coefficients,
                                DiscreteFunctionListImp & reconsList);
  /**
   * @brief reconstructs a given reduced simulation and compares it to the
   *        corresponding detailed simulation.
   *
   * This computes and prints the L2 error between the reconstruction and
   * the corresponding detailed simulation.
   *
   * @param[in]  coefficients  coefficient vector \f$[\mathbf{a}^k]\f$
   */
  inline void reconstructAndCompare (const Args::ArrayType &coefficients);

  /**
   * @brief performs a row of detailed simulations which are all identified by
   * an id given as the first argument (internally a DiscreteFunctionList is
   * used for storage of functions)
   *
   * @param[in] savepath a string id for the list of solutions
   * @param[in] params   a Matlab mxArray structure
   */
  inline void saveDetailedSimulations (std::string savepath,
                                       const Args::ArrayType & params);

  /**
   * @brief replaces the values for parameter keys given through the vector
   *         in mu_names by values from vector mu_values
   *
   * @param[in] mu_names vector of parameter keys
   * @param[in] mu_values the parameter vector \f$\mu\f$.
   */
  template< class ContainerT >
  inline void setMu (const std::vector< std::string > & mu_names,
                     const ContainerT & mu_values);

  /**
   * @brief replaces the values for parameter keys given through the vector
   *        in the private variable mu_names_ by values from vector mu_values
   *
   * @param[in] mu_values the parameter vector \f$\mu\f$.
   */
  template< class ContainerT >
  inline void setMu (const ContainerT & mu_values);

  /**
   * @brief replaces the values for parameter keys given through the vector
   *        in the private variable mu_names_ by values from vector mu_values
   *
   * @param[in] mu_values the parameter vector \f$\mu\f$.
   */
  inline void setMu (const Args::ArrayType & mu_values);

  /**
   * @brief starts a number of detailed simulations
   *
   * @note The number of simulations is determined by the number of given mu
   * values
   *
   * @param[out] U  Discrete function list, this will contain the detailed
   *                simulations
   */
  template< class DiscreteFunctionListImp >
  inline void solveDetailed (DiscreteFunctionListImp &U);

  /**
   * @brief starts a number of detailed simulations
   *
   * @note The number of simulations is determined by the number of given mu
   * values
   *
   * @param[out] U  T::ArrayType with DoFs of the solution of the detailed
   *                simulation
   */
  inline void solveDetailed (Args::ArrayType * &U);

  //=================
  // Member variables
  //=================

private:
#ifdef SHARED_PTR_NAMESPACE
  shared_ptr< GridType > shGridPtr_;
#endif
  GridPtr< GridType > *gridPtr_;
  GridPartType *gridPart_;
  LinAlgLib *linAlgLib_;
  DiscFuncSpaceType *discFuncSpace_;
  ReducedBasisSpace *redBaseSpace_;
  ModelType *model_;
  std::vector< std::string > mu_names_;
  std::vector< MuRangeType > mu_ranges_;
  ConvDiffDiscretizationType *discretization_;
  std::string rbListPath_;
};
// end of LinEvolDefault

}  // namespace LinEvol
}  // namespace Example
}// end of namespace RBFem
} // end of namespace Dune

#include "linevoldefault.cc"

#endif  /*__LINEVOL_DEFAULT_HH__*/

/* vim: set sw=2 et: */
