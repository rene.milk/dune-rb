/***********************************************************************************************
 LinEvolSolver is the interface class used to define the solver used for the problem
***********************************************************************************************/

#ifndef __LINEVOLSOLVER_HH__
#define __LINEVOLSOLVER_HH__

#include <iostream>

#include <dune/grid/utility/gridtype.hh>

#include <dune/fem/misc/femtimer.hh>
#include <dune/fem/misc/femeoc.hh>
#include <dune/fem/misc/gridwidth.hh>
#include <dune/fem/io/file/dataoutput.hh>

/* include definition of the physical problem (coefficient functions, etc.) */
#include "linevoldiscr.hh"

//include interface for matlab-control

//include discfunc list header
#include "../../misc/discfunclist/discfunclist_xdr.hh"
#include "../../misc/discfunclist/discfunclist_mem.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

  /** @ingroup linevol
   * @brief provides methods for control of time steps
   *
   * implements an interface that can specifies a generic method providing a
   * numerical evolution scheme.
   *
   * @tparam DescrImp    class that describes the underlying model and chosen
   *                     discretizations of the numerical scheme
   *
   * @rbparam{rb.gridFile, name of dgf grid file}
   * @rbparam{rb.printCount, integer indicating how frequently status information shall be printed to stdout, -1 (always)}
   * @rbparam{rb.startTime, start of time descretization interval}
   * @rbparam{rb.endTime, end of time discretization interval}
   * @rbparam{rb.eocSteps, number of eoc steps to compute, -1 (none)}
   * @rbparam{rb.dtEst, estimated time step length}
   */
  template <typename DescrImp>
  struct Stepper {
    typedef typename DescrImp :: DiscrType                           DiscretizationType;
    // The discrete function for the unknown solution
    typedef typename DiscretizationType :: DestinationType           DiscreteFunctionType;
    // ... as well as the Space type
    typedef typename DiscretizationType :: SpaceType                 DiscreteSpaceType;

    //! grid type
    typedef typename DescrImp :: GridType                            GridType;
    //! grid part type
    typedef typename DescrImp :: GridPartType                        GridPartType;
    //! number of runge kutta steps
    static const int rkSteps = POLORDER + 1;
    //! ode solver type
    typedef typename DescrImp :: ODEType                             ODEType;
    //! model type
    typedef typename DescrImp :: ModelType                           ModelType;

    //! constructor
    Stepper(ModelType &linEvolModel, DiscretizationType &discr) :
      space_(discr.space()),
      grid_(space_.grid()),
      gridPart_(space_.gridPart()),
      linEvolModel_(linEvolModel),
      fvOp_(discr)
    {
      // if (Parameter :: getValue<int>("rb.eocSteps", -1) != -1) {
      //   eocId = FemEoc::addEntry(std::string("$L^2$-error"));
      // }
    }

    //! returns discrete function space
    const DiscreteSpaceType& space() {
      return space_;
    }

    /** gets called at the beginning of an evolution scheme
     *
     * Executes e.g. initial data projection and time step estimation.
     */
    template<typename TimeProviderImp>
    void initialize(TimeProviderImp& tp, DiscreteFunctionType& u)
    {
      typedef typename ModelType :: DecomposedInitDataType           DecomposedInitialDataType;
      typedef typename DecomposedInitialDataType :: CompleteType     InitialDataCompleteType;

      odeptr = fvOp_.newODESolver(tp, rkSteps);

      /* FemEoc::initialize("eoctable", "Eoc Table");*/

      const DecomposedInitialDataType & initDataDecomp = linEvolModel_.initialData();
      // project initial data on discrete function u
      initDataDecomp.projectComplete(u);

#if defined(DEBUG) && DEBUG > 5
      std::cout << "in Stepper::initialize(tp, u): u0 evaluates to:" << std::endl;
      u.print(std::cout);
#endif

      //! \todo the following might be unnecessary
      // ode.initialize applies the DG Operator once to get an estimate for dt.
      odeptr->initialize(u);
    }

    //! calls in every time step the ode solver
    void step(TimeProviderBase& tp, DiscreteFunctionType& u) {
       odeptr->solve(u);
    }

    //! after we reached the end time value, this method is called
    void finalize(TimeProviderBase& tp, DiscreteFunctionType& u) {
      if (Parameter :: getValue<int>("rb.eocSteps", -1) != -1)
	{
/*          L2Error<DiscreteFunctionType> L2err;
 *          // Compute L2 error of discretized solution ...
 *          FieldVector<double,ModelType::dimRange> error;
 *          error = L2err.norm(linEvolModel_.initialData(), u, tp.time());
 *          // ... and print the statistics out to the eocOutputPath file
 *          FemEoc::setErrors(eocId,error.two_norm());*/
	}
      delete odeptr;
    }

    //! returns a problem description
    const std::string problemDescription() const {
      return std::string("No Description given yet!");
    }

    //! returns the discretization
    const DiscretizationType& discretization() {
      return fvOp_;
    }

  private:
    const DiscreteSpaceType &space_;
    const GridType          &grid_;
    const GridPartType      &gridPart_;
    ModelType               &linEvolModel_;
    DiscretizationType      &fvOp_;
    unsigned int             eocId;
    ODEType                 *odeptr;
    double                   cfl_;
  };

  /** @ingroup linevol
   * @brief class that provides all necessary methods for the solution of
   * linear evolution scheme.
   */
  template <class Stepper>
  class Solver
  {
    typedef typename Stepper :: DiscreteSpaceType                    DiscreteFunctionSpaceType;
  public:
    //! discrete function list type
    typedef FunctionList::Xdr
              < FunctionList::Traits< Stepper > >              DiscreteFunctionListType;
    //! discrete function type
    typedef typename Stepper :: DiscreteFunctionType                 DiscreteFunctionType;
    //! stepper type
    typedef Stepper                                                  StepperType;
    //! model type
    typedef typename StepperType :: ModelType                        ModelType;
    //! discretization type
    typedef typename StepperType :: DiscretizationType               DiscretizationType;
  public:
    //! constructor
    Solver (ModelType&, DiscretizationType&);

    //! destructor
    ~Solver ();

    //! solves the numerical scheme and writes result into a discrete function
    //! list.
    template< class DiscreteFunctionListImp >
    void solve(DiscreteFunctionListImp &funcList);


  private:
    template< class DiscreteFunctionListImp >
    void evolve(DiscreteFunctionListImp &funcList);

  private:
    const typename GridSelector :: GridType & grid_;
    Stepper stepper_;
    bool skipEoc_;
  };

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#include "linevolsolve.cc"

#endif

/* vim: set sw=2 et: */
