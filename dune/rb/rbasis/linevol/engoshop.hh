#ifndef __ENGOSHOP_HH__
#define __ENGOSHOP_HH__

#include <dune/fem/operator/common/spaceoperatorif.hh>
#include "../../misc/parameter/parameter.hh"
#include <dune/grid/genericgeometry/referenceelements.hh>

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Operator {


/** Engquist Osher Finite Volume Operator from DUNE-FEM-HOWTO module
 *
 * @rbparam{rb.dtEst, estimate for time step length}
 */
template <class DestinationType,class Problem> /*@LST0S@*/
struct EngquistOsher
  : public SpaceOperatorInterface<DestinationType>
{
  //! The global coordinates and range of problem
  typedef Dune :: FieldVector< double, 1 >                           RangeType;
  typedef typename Problem :: DomainType                             DomainType;
  typedef Dune :: FieldVector< double, DomainType :: dimension-1 >   FaceDomainType;
  typedef GenericReferenceElement< double, DomainType
                                  :: dimension-1 >                   ReferenceElementType;
  typedef GenericReferenceElements< double, DomainType
                                   :: dimension-1 >                  ReferenceElementContainerType;

  //! type of discrete function space 
  typedef typename DestinationType :: DiscreteFunctionSpaceType DiscreteSpaceType;
  //! Iterator over the space
  typedef typename DiscreteSpaceType::IteratorType              IteratorType;
  //! The codim 0 entity and the entityptr
  typedef typename IteratorType::Entity                         Entity;
  typedef typename Entity::EntityPointer                        EntityPointer;
  //! The grid part
  typedef typename DiscreteSpaceType::GridPartType              GridPartType;
  //! The entity intersections
  typedef typename GridPartType::IntersectionIteratorType       IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection       IntersectionType;
  //! The geometry
  typedef typename Entity::Geometry                             Geometry;
  //! The local function
  typedef typename DestinationType::LocalFunctionType           LocalFunctionType; /*@LST0E@*/

  EngquistOsher(const DiscreteSpaceType &space,
                     const Problem &problem)
  : space_(space),
    problem_(problem)
  {
    RBFem::ParameterHelper :: fillVector("rb.velocity", velocity_);
  }

  /*@LST0S@*/ //! return reference to space (needed by ode solvers)
  const DiscreteSpaceType& space() const 
  {
    return space_;
  } /*@LST0E@*/

  void initialize(DestinationType& u) /*@LST0S@*/ 
  {
    IteratorType endit = space().end();
    for (IteratorType it = space().begin(); it != endit; ++it) /*@\label{fv:initLoop}@*/
    {
      const Entity &en = *it;
      const Geometry &geo = en.geometry();
      DomainType xGlobal = baryCenter(geo);
      LocalFunctionType lf = u.localFunction(en);
      RangeType u0;
      //      problem_.evaluate(xGlobal,u0);
      u0[0] = sin(2*M_PI*xGlobal.two_norm2());
      lf[0] = u0[0];
    }
  } /*@LST0E@*/ 

  /*@LST0S@*/ //! apply operator 
  void operator () (const DestinationType& u, DestinationType& update) const 
  {
    // clear update 
    update.clear();

    const GridPartType &gridPart = space().gridPart();
    // time step size (using std:min(.,dt) so set to maximum) 
    dt_ = std::numeric_limits<double>::infinity(); 

    IteratorType endit = space().end();
    for (IteratorType it = space().begin(); it != endit; ++it) /*@\label{fv:entityLoop}@*/
    {
      // get entity and geometry and index
      const Entity &entity = *it; /*@\label{fv:entityInit0}@*/
      const Geometry &geo = entity.geometry();
      unsigned int enIdx = gridPart.indexSet().index(entity);
      // assert CFL condition for quadrilateral grids
      double timeFactor = ( geo.type().isCube() ) ? 0.5 : 1.0;

      LocalFunctionType lfEn    = u.localFunction(entity);
      LocalFunctionType lfUpdEn = update.localFunction(entity);

      // cell volume
      const double enVolume = geo.volume(); 
      
      // 1 over cell volume
      const double enVolume_1 = 1.0/enVolume; /*@\label{fv:entityInit1}@*/

      IntersectionIteratorType endnit = gridPart.iend(entity);
      for (IntersectionIteratorType nit = gridPart.ibegin(entity); 
           nit != endnit; ++nit) 
      {
        const IntersectionType& intersection=*nit; /*@\label{fv:intersectionInit0}@*/

        const ReferenceElementType& referenceElement =
          ReferenceElementContainerType::general(intersection.type());

        // center in face's reference element
        const FaceDomainType& xIntersection = referenceElement.position(0,0);

        const double volumeIntersection = referenceElement.volume();  /*@\label{fv:intersectionInit1}@*/

        // handle interior face
        if( intersection.neighbor() ) /*@\label{fv:interiorFace}@*/
        {
          // access neighbor
          const EntityPointer outside = intersection.outside(); /*@\label{fv:neighborInit0}@*/
          const Entity &neighbor = *outside;
          unsigned int nbIdx = gridPart.indexSet().index(neighbor); /*@\label{fv:neighborInit1}@*/

          // compute flux from one side only
          // this should become easier with the new IntersectionIterator functionality!
          if( (entity.level() > neighbor.level()) /*@\label{fv:calcFluxJustOnce}@*/
              || ((entity.level() == neighbor.level()) && (enIdx < nbIdx))
              || (neighbor.partitionType() != Dune::InteriorEntity) )
          { /*@LST0E@*/ 

            // calculate (1 / neighbor volume)
            const double nbVolume = neighbor.geometry().volume();
            const double nbVolume_1 = 1.0 / nbVolume;
            // get outer normal 
            DomainType normal = intersection.integrationOuterNormal(xIntersection); /*@LST0S@*/ /*@LST0E@*/ 

            // get local functions 
            LocalFunctionType lfNb = u.localFunction(neighbor);
            LocalFunctionType lfUpdNb = update.localFunction(neighbor);

            // apply numerical flux /*@LST0S@*/ /*@\label{fv:numFlux0}@*/
            //            const DomainType &velocity = problem_.velocity();

            // calculate wave speed for local dt 
            double waveSpeed = velocity_ * normal * volumeIntersection;

            RangeType flux; 

            // upwinding 
            if (waveSpeed>0)
              flux[0] = lfEn[0]*waveSpeed;
            else
              flux[0] = lfNb[0]*waveSpeed; /*@\label{fv:numFlux1}@*/

            // calc update of entity 
            lfUpdEn[0] -= enVolume_1 * flux[0] ;
            // calc update of neighbor 
            lfUpdNb[0] += nbVolume_1 * flux[0] ;

            // compute dt restriction
            //            dt_ = std::min( dt_, timeFactor * std::min(enVolume,nbVolume) / std::abs(waveSpeed) );
            dt_ = Parameter::getValue<double>("rb.dtEst");
          }
        }
        // handle boundary face
        else /*@\label{fv:boundaryFace}@*/
        {
          DomainType normal = intersection.integrationOuterNormal(xIntersection);

          // apply numerical flux
          RangeType flux; 
          //          const DomainType &velocity = problem_.velocity();

          double waveSpeed = velocity_ * normal * volumeIntersection;
          if (waveSpeed>0)
            flux[0] = lfEn[0]*waveSpeed;
          else 
          {
            RangeType uBnd; /*@\label{fv:inflowBoundary0}@*/
            DomainType xBnd = 
              geo.global( intersection.geometryInInside().global(xIntersection) );

            //            problem_.evaluate(time_, xBnd, uBnd);
            DomainType velTimeProd = velocity_;
            xBnd.axpy(-time_,velocity_);
            uBnd[0] = sin( 2. *M_PI* (xBnd * xBnd)); 
            
            flux[0] = uBnd[0]*waveSpeed; /*@\label{fv:inflowBoundary1}@*/
          }

          // calc update of entity 
          lfUpdEn[0] -= enVolume_1 * flux[0] ;

          // compute dt restriction
          //dt_ = std::min( dt_, timeFactor * enVolume / std::abs(waveSpeed) );
          dt_ = Parameter::getValue<double>("rb.dtEst");
        }
      }
    }
  } /*@LST0E@*/

  /** \brief set time for operators 
      \param time current time of evaluation 
  */
  void setTime(const double time) {
    time_ = time;
  }

  /** \brief returns maximal possible dt to assure stabil 
       explicit Runge Kutta ODE Solver. */
  double timeStepEstimate () const 
  {
    return dt_;  
  }

  private:
  template <class Geometry>
  Dune::FieldVector<double,Geometry::coorddimension>
  baryCenter(const Geometry& geo) const
  {
    Dune::FieldVector<double,Geometry::coorddimension> xGlobal(geo.corner(0));
    for (int i=1;i<geo.corners();++i)
      xGlobal += geo.corner(i);
    xGlobal /= geo.corners();
    return xGlobal;
  }

  DomainType velocity_;
  const DiscreteSpaceType &space_;
  const Problem &problem_;
  mutable double dt_;
  double time_;
};

}  // namespace Operator
}  // namespace Lib
} // namespace RBFem
} // namespace Dune
#endif // __ENGOSHOP_HH__
