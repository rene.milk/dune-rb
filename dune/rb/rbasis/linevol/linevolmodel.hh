#ifndef LINEVOLMODEL_HH_
#define LINEVOLMODEL_HH_

// headers for decomposed functions
#include "../functions/discretedecomposedfunction.hh"
#include "../functions/localmuscaledfunction.hh"
#include "../functions/common/makedecomposedfunctions.hh"

// include function space
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/function/common/function.hh>

namespace Dune {
namespace RBFem {
namespace Example {


//! boundary types that may be used by the model
enum BoundaryType { Dirichlet, Neumann, NoFlow, OutFlow };

namespace LinEvol {

template < int rangeDim, class FunctionSpaceImp, class GridPartImp >
struct ModelTraits
{
  //! grid part type
  typedef GridPartImp                                                GridPartType;
  //! function space type
  typedef FunctionSpaceImp                                           FunctionSpaceType;
  //! domain vector space type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range vector space type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! field type
  typedef typename FunctionSpaceType :: RangeFieldType               FieldType;
  //! vector function space type
  typedef FunctionSpace< FieldType, FieldType,
                         DomainType :: dimension,
                         DomainType :: dimension >                   VectorFunctionSpaceType;
  //! grid type
  typedef typename GridPartType :: GridType                          GridType;

  //! dimension of grid
  static const int dim       = GridType::dimension;
  //! dimension of world in which grid lives
  static const int dimworld  = GridType::dimensionworld;
  //! dimension of domain vector space
  static const int dimDomain = FunctionSpaceType :: dimDomain;
  //! dimension of range vector space
  static const int dimRange  = rangeDim;

  //! entity type
  typedef typename GridType :: template Codim< 0 > :: Entity         Entity;
  //! entity pointer type
  typedef typename GridType :: template Codim< 0 > :: EntityPointer  EntityPointer;
  //! intersection iterator type
  typedef typename GridPartType :: IntersectionIteratorType          IntersectionIterator;

};

//! model switcher enumeration
/** @ingroup models
 */
enum ModelEnum
{
  //! default convdiff
  /** a very basic convection-diffusion equation with a blob as
   * initial data function, all further data
   *
   * LinEvol
   */
  ConvDiff = 0,
  //! 1D test problem
  /**
   * a problem with an unsteady wave as initial data, best suited
   * for 1d grids.
   */
  OneD,
  //! Settings for the wonapde example by Sven
  /** a derivation of ConvDiff with a non-constant vector field,
   * suited for 3d simulations and presented on WONAPDE
   * conference by Sven.
   */
  WONAPDE,
  //! 2 dimensional test settings
  TEST2DTRANSPORT,
  //! 3 dimensional test settings
  TEST3DCONVDIFF
};

//! selector for model
/**
 * For possible choices see Dune::RB::ModelEnum
 */
template<class ModelParamType, enum ModelEnum me>
struct Model
{
  //typedef NoImplementationForRequestedModelEnum                      Type;
};

} // namespace LinEvol
} // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#include "convdiffbase.hh"

#include "../../models/convdiff.hh"
#include "../../models/oned.hh"
#include "../../models/wonapde.hh"
#include "../../models/test2dtransport.hh"
#include "../../models/test3dconvdiff.hh"


#endif /* LINEVOLMODEL_HH_ */

