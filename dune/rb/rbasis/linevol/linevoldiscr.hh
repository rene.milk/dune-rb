#ifndef __LINEVOLDISCRETIZATION_HH__
#define __LINEVOLDISCRETIZATION_HH__

/* include definition of the physical problem (coefficient functions, etc.) */
#include  "linevolmodel.hh"

//#include  <dune/grid/common/defaultindexsets.hh>

#include  <dune/fem/function/adaptivefunction.hh>
#include  <dune/fem/operator/common/spaceoperatorif.hh>

// include L2 projection into discrete function spaces
#include <dune/fem/operator/projection/l2projection.hh>


// choose discrete function type
#include  <dune/fem/space/fvspace.hh>
#include <dune/fem/space/dgspace.hh>

//#include <dune/fem/space/dgspace/dgadaptiveleafgridpart.hh>

#include  "../../misc/utility.hh"
#include  "../../rbasis/operators/eye.hh"
//#include "../misc/stuff.cc"
#include <dune/fem/operator/projection/l2projection.hh>

// fv operators
#include "../operators/fv/convection.hh"
#include "../operators/fv/diffusion.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

// set default discretization
#ifndef DISCRETIZATION
#define DISCRETIZATION Explicit
#endif

//! switcher for the chosen time discretization
//! @ingroup linevol
enum TimeDiscretizationEnum
{
  //! explicit time discretization
  Explicit = 0,
  //! semi-implicit time discretization
  SemiImplicit
};

/** traits parameter class for the discretization implementations
 */
template <class ModelImpType, int polOrd=0 >
struct DiscretizationTraits
{
  //! model type
  typedef ModelImpType                                               ModelType;
  //! model parameter type
  typedef typename ModelImpType :: ParamType                         ModelParamType;

  //! dimension of the domain vector space
  static const int dimRange = ModelParamType::dimRange;

  //! grid type
  typedef typename ModelParamType :: GridType                        GridType;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;

  //! dimension of the grid
  static const int dim      = GridType :: dimension;
  //! world dimension in which grid lives
  static const int dimworld = GridType :: dimensionworld;

  //! domain/range vector field type
  typedef typename ModelParamType :: FieldType                       FieldType;

  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;

  //! discrete function space type
  // typedef DiscontinuousGalerkinSpace< FunctionSpaceType,
  //                                     GridPartType, 0,
  //                                     CachingStorage >               DiscFuncSpaceType;
  typedef FiniteVolumeSpace< FunctionSpaceType, GridPartType, polOrd,
                             CachingStorage >                        DiscFuncSpaceType;
/*typedef LagrangeDiscreteFunctionSpace 
                   < FunctionSpaceType, GridPartType, polOrd > DiscFuncSpaceType;*/

  //! discrete function type
  typedef AdaptiveDiscreteFunction< DiscFuncSpaceType >              DiscreteFunctionType;
};

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace Dune::RBFem
} // end of namespace Dune

#include "linevolexplicitdiscr.hh"
#include "linevolsemiimplicitdiscr.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

/** @addtogroup linevol
 * @brief Selects a discretization
 *
 * Possible choices via the macro \c DISCRETIZATION are:
 *  \arg \c Explicit     (default) for an explicit time discretization scheme
 *  \arg \c SemiImplicit for a semi implicit time discretization scheme, where
 *                       diffusion contributions are discretized implicitly.
 */
template<class DiscrType, enum TimeDiscretizationEnum tde>
struct Discretization
{
  //  typedef NoImplementationForRequestedDiscretizationType             Type;
};

template<class DiscrType>
struct Discretization<DiscrType, Explicit>
{
   typedef ExplicitDiscretization< DiscrType >                 Type;
};

template<class DiscrType>
struct Discretization<DiscrType, SemiImplicit>
{
  typedef SemiImplicitDiscretization< DiscrType >             Type;
};

} // end of namespace LinEvol
} // end of namespace Example
} // end of namespace Dune::RBFem
} // end of namespace Dune

#endif // __LINEVOLDISCRETIZATION_HH__

/* vim: set et sw=2: */
