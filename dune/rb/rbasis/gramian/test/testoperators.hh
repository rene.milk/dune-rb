
namespace Dune
{

//! \todo add a full operator

template< class ArgDiscreteFunctionType, class DestDiscreteFunctionType >
class ConstantOperator
  : public Dune::RBFem::Lib::Operator::ILocalParametrized<DestDiscreteFunctionType,
                       ConstantOperator<ArgDiscreteFunctionType,
                         DestDiscreteFunctionType> >
{
  typedef double                                                     DomainFieldType;
  typedef double                                                     RangeFieldType;
  typedef typename ArgDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             ArgDiscFuncSpaceType;
  typedef typename ArgDiscFuncSpaceType :: BaseFunctionSetType       ArgBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DestDiscFuncSpaceType;
  typedef typename DestDiscFuncSpaceType :: BaseFunctionSetType      DestBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType :: LocalFunctionType     DestLocalFuncType;
  typedef typename ArgDiscreteFunctionType :: LocalFunctionType      ArgLocalFuncType;
  typedef typename DestDiscFuncSpaceType :: GridPartType             GridPartType;
  typedef typename GridPartType :: GridType                          GridType;

public:
  ConstantOperator(const double constant=1.0) : constant_(constant)
  {};

  void prepareGlobal(const ArgDiscreteFunctionType &arg, DestDiscreteFunctionType &dest) {
    arg_  = &arg;
    dest_ = &dest;
    dest_->clear();
    destDiscFuncSpace_ = &(dest.space());
  };

  void finalizeGlobal() {};

  template< class EntityType >
  void applyLocal(const EntityType &en)
  {
    DestLocalFuncType destLocal      = dest_->localFunction(en);
    const ArgLocalFuncType &argLocal = arg_->localFunction(en);

    typedef typename EntityType :: Geometry                          GeometryType;
    const GeometryType & geo         = en.geometry();

    // get base function set
    const DestBaseFunctionSetType destBaseFuncSet = destDiscFuncSpace_->baseFunctionSet(en);

    //get a quadrature for this entity
    const unsigned int order = 0; //order for the quadrature
    CachingQuadrature<GridPartType, 0> quadrature(en, order);

    for (unsigned int dofnum = 0; dofnum < (unsigned) argLocal.numDofs(); ++dofnum)
    {
      destLocal[dofnum] = 0.0;

      //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint < quadrature.nop(); ++quadPoint)
      {

        double integrationElement = geo.integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);

        // evaluate the current base function
        typename DestDiscreteFunctionType::RangeType destBaseFunctionValue;
        destBaseFuncSet.evaluate(dofnum, quadrature.point(quadPoint), destBaseFunctionValue);

        double res =  destBaseFunctionValue * constant_;
        destLocal[dofnum] += integrationElement * weight * res;

      } //loop over all quadrature points
    } // loop over all dofs
  }

  template <class EntityType>
  void prepareLocal(const EntityType& en) {};

  template <class EntityType>
  void finalizeLocal(const EntityType& en) {};

  double coefficient() const
  {
    return constant_;
  }

  bool isMuDependent() const
  {
    return false;
  }

  bool isTimeDependent() const
  {
    return false;
  }

  std::string symbolicCoefficient() const
  {
    std::ostringstream oss;
    oss << constant_;
    return oss.str();
  }

  void setTime(double time)
  {};

private:
  DestDiscreteFunctionType* dest_;
  ArgDiscreteFunctionType const * arg_;
  DestDiscFuncSpaceType const * destDiscFuncSpace_;

  double constant_;

};


template< class ArgDiscreteFunctionType, class DestDiscreteFunctionType >
class PlusOperator
  : public Dune::RBFem::Lib::Operator::ILocalParametrized<DestDiscreteFunctionType,
                       PlusOperator<ArgDiscreteFunctionType,
                         DestDiscreteFunctionType> >
{
  typedef double                                                     DomainFieldType;
  typedef double                                                     RangeFieldType;
  typedef typename ArgDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             ArgDiscFuncSpaceType;
  typedef typename ArgDiscFuncSpaceType :: BaseFunctionSetType       ArgBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DestDiscFuncSpaceType;
  typedef typename DestDiscFuncSpaceType :: BaseFunctionSetType      DestBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType :: LocalFunctionType     DestLocalFuncType;
  typedef typename ArgDiscreteFunctionType :: LocalFunctionType      ArgLocalFuncType;
  typedef typename DestDiscFuncSpaceType :: GridPartType             GridPartType;
  typedef typename GridPartType :: GridType                          GridType;

public:
  PlusOperator(const double constant=1.0) : constant_(constant)
  {};

  void prepareGlobal(const ArgDiscreteFunctionType &arg, DestDiscreteFunctionType &dest) {
    arg_  = &arg;
    dest_ = &dest;
    dest_->clear();
    destDiscFuncSpace_ = &(dest.space());
  };

  void finalizeGlobal() {};

  template< class EntityType >
  void applyLocal(const EntityType &en)
  {
    DestLocalFuncType destLocal      = dest_->localFunction(en);
    const ArgLocalFuncType &argLocal = arg_->localFunction(en);

    typedef typename EntityType :: Geometry                          GeometryType;
    const GeometryType & geo         = en.geometry();

    // get base function set
    const DestBaseFunctionSetType destBaseFuncSet = destDiscFuncSpace_->baseFunctionSet(en);

    //get a quadrature for this entity
    const unsigned int order = 0; //order for the quadrature
    CachingQuadrature<GridPartType, 0> quadrature(en, order);

    for (unsigned int dofnum = 0; dofnum < (unsigned) argLocal.numDofs(); ++dofnum)
    {

      destLocal[dofnum] = 0.0;

      //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint < quadrature.nop(); ++quadPoint)
      {

        double integrationElement = geo.integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);

        // evaluate the current base function
        typename DestDiscreteFunctionType::RangeType destBaseFunctionValue;
        destBaseFuncSet.evaluate(dofnum, quadrature.point(quadPoint), destBaseFunctionValue);

        typename ArgDiscreteFunctionType::RangeType value;
        argLocal.evaluate(quadrature.point(quadPoint), value);

        double res =  destBaseFunctionValue * (value + constant_);
        destLocal[dofnum] += integrationElement * weight * res;

      } //loop over all quadrature points
    } // loop over all dofs
  }

  template <class EntityType>
  void prepareLocal(const EntityType& en) {};

  template <class EntityType>
  void finalizeLocal(const EntityType& en) {};

  double coefficient() const
  {
    return constant_;
  }

  bool isMuDependent() const
  {
    return false;
  }

  bool isTimeDependent() const
  {
    return false;
  }

  std::string symbolicCoefficient() const
  {
    std::ostringstream oss;
    oss << constant_;
    return oss.str();
  }

  void setTime(double time)
  {};

private:
  DestDiscreteFunctionType* dest_;
  ArgDiscreteFunctionType const * arg_;
  DestDiscFuncSpaceType const * destDiscFuncSpace_;

  double constant_;

};


template< class ArgDiscreteFunctionType, class DestDiscreteFunctionType >
class GradientOperator
  : public Dune::RBFem::Lib::Operator::ILocalParametrized<DestDiscreteFunctionType,
                       GradientOperator<ArgDiscreteFunctionType,
                         DestDiscreteFunctionType> >
{
  typedef double                                                     DomainFieldType;
  typedef double                                                     RangeFieldType;
  typedef typename ArgDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             ArgDiscFuncSpaceType;
  typedef typename ArgDiscFuncSpaceType :: BaseFunctionSetType       ArgBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DestDiscFuncSpaceType;
  typedef typename DestDiscFuncSpaceType :: BaseFunctionSetType      DestBaseFunctionSetType;
  typedef typename DestDiscreteFunctionType :: LocalFunctionType     DestLocalFuncType;
  typedef typename ArgDiscreteFunctionType :: LocalFunctionType      ArgLocalFuncType;
  typedef typename DestDiscFuncSpaceType :: GridPartType             GridPartType;
  typedef typename GridPartType :: GridType                          GridType;

public:
  GradientOperator() {};

  void prepareGlobal(const ArgDiscreteFunctionType &arg, DestDiscreteFunctionType &dest) {
    arg_  = &arg;
    dest_ = &dest;
    dest_->clear();
    destDiscFuncSpace_ = &(dest.space());
  };

  void finalizeGlobal() {};

  template< class EntityType >
  void applyLocal(const EntityType &en)
  {
    // to cut a long story short: L2-project the gradient of arg onto the space
    // where dest lives and assign the dofs of arg to dest.
    DestLocalFuncType destLocal      = dest_->localFunction(en);
    const ArgLocalFuncType &argLocal = arg_->localFunction(en);

    typedef typename EntityType :: Geometry                          GeometryType;
    const GeometryType & geo         = en.geometry();

    // get base function set
    const DestBaseFunctionSetType destBaseFuncSet = destDiscFuncSpace_->baseFunctionSet(en);

    //get a quadrature for this entity
    const unsigned int order = 0; //order for the quadrature
    CachingQuadrature<GridPartType, 0> quadrature(en, order);

    for (unsigned int dofnum = 0; dofnum < (unsigned) argLocal.numDofs(); ++dofnum) {

      destLocal[dofnum] = 0.0;

      //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint < quadrature.nop(); ++quadPoint)
      {
        double integrationElement = geo.integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);

        // evaluate the current base function
        typename DestDiscreteFunctionType::RangeType destBaseFunctionValue;
        destBaseFuncSet.evaluate(dofnum, quadrature.point(quadPoint), destBaseFunctionValue);

        // evaluate the gradient of the argument's base function
        typename ArgDiscreteFunctionType::LocalFunctionType::JacobianRangeType argGradValue;
        argLocal.jacobian( quadrature.point(quadPoint), argGradValue );

        double gradEvaled =  destBaseFunctionValue * argGradValue[0];

        // interpolate the gradient -> compute DoF
        destLocal[dofnum] += integrationElement * weight * gradEvaled;

      } //loop over all quadrature points
    }
  }

  template <class EntityType>
  void prepareLocal(const EntityType& en) {};

  template <class EntityType>
  void finalizeLocal(const EntityType& en) {};

  double coefficient() const
  {
    return 1.0;
  }

  bool isMuDependent() const
  {
    return false;
  }

  bool isTimeDependent() const
  {
    return false;
  }

  std::string symbolicCoefficient() const
  {
    return "1.0";
  }

  void setTime(double time)
  {};

private:
  DestDiscreteFunctionType* dest_;
  ArgDiscreteFunctionType const * arg_;
  DestDiscFuncSpaceType const * destDiscFuncSpace_;

};

}
