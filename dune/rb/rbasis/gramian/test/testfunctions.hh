#ifndef __TESTFUNCTIONS_HH
#define __TESTFUNCTIONS_HH

namespace Dune
{

struct SineFunction
{

  typedef double DomainFieldType;
  typedef double RangeFieldType;

  SineFunction (const unsigned int dim, const unsigned int &k)
      : dim_( dim ), k_( k )
  {
  }
  ;

  template< typename DomainType, typename RangeType >
  void evaluate (const DomainType &x, RangeType &ret) const
  {
    ret[0] = 1.0;
    for (unsigned int i = 0; i != dim_; ++i)
      ret[0] *= sin( 2 * M_PI * k_ * x[i] );
    return;
  }

private:
  const unsigned int dim_;
  const unsigned int k_;
};

struct ConstantFunction
{

  typedef double DomainFieldType;
  typedef double RangeFieldType;

  ConstantFunction ()
  {
  }
  ;

  template< typename DomainType, typename RangeType >
  void evaluate (const DomainType &x, RangeType &ret) const
  {
    ret[0] = 1.0;
  }

};

struct LinearFunction
{
  typedef double DomainFieldType;
  typedef double RangeFieldType;

  LinearFunction (double start, double end)
      : start_( start ), end_( end )
  {
  }
  ;

  template< class FVType, typename RangeType >
  void evaluate (const FVType &x, RangeType &ret) const
  {
    const unsigned int dim = FVType::size;
    ret[0] = start_;
    for (unsigned int i = 0; i < dim; i++)
    {
      ret[0] += x[0] * (end_ - start_);
    }
  }

private:
  double start_, end_;
};

}

#endif // __TESTFUNCTIONS_HH
