#define BOOST_TEST_MODULE GramianPipeline

#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#define GRIDWALK_TEST 1

// constants
//static const int NUM_ELEMENTS = 1000;
static const int K_START      = 0;
static const int K_END        = 10;

#include "testcommon.hh"

BOOST_AUTO_TEST_SUITE( GramianPipeline )

/*BOOST_FIXTURE_TEST_CASE( gramian_matrix_1, GMT )
 *{
 *  addSineFunctions(K_START, K_END);

 *  //! access only every second function
 *  DiscFuncListSubSetWrapperType subsetWrapper(dfList, 1);

 *  GramianPipelineType pipeline(subsetWrapper);

 *  setting1(pipeline);

 *  pipeline.run();

 *  check();

 *|+#if DEBUG
 * *  // print the functions
 * *  dfList.print(std::cout);
 * *  std::cout << "Subset wrapper:\n";
 * *  subSetWrapper.print(std::cout);
 * *#endif+|
 *}*/

BOOST_FIXTURE_TEST_CASE( gramian_matrix_1, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList);

  setting1(pipeline);

  BOOST_CHECK_NO_THROW(pipeline.run());
}

BOOST_FIXTURE_TEST_CASE( gramian_matrix_2, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList, 2);

  setting1(pipeline);

  BOOST_CHECK_NO_THROW(pipeline.run());
}

BOOST_FIXTURE_TEST_CASE( gramian_matrix_3, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList, 3);

  setting1(pipeline);

  BOOST_CHECK_NO_THROW(pipeline.run());
}
BOOST_AUTO_TEST_SUITE_END( )


