#ifndef TESTCOMMON_V3II97KN
#define TESTCOMMON_V3II97KN

#undef NDEBUG

#ifndef NUM_ELEMENTS
#define NUM_ELEMENTS 100
#endif

#include <config.h>

// local operators definitions etc
#include "../../operators/discretedecomposedoperator.hh"

// include the GridType typedef
#include <dune/grid/onedgrid.hh>

#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/misc/femtimer.hh>

// include of gridPart
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include L2-Projection
#include <dune/fem/operator/projection/l2projection.hh>

// include MPI Manager
#include <dune/fem/misc/mpimanager.hh>

// include Discrete Function Space
#include <dune/fem/space/dgspace.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/fvspace.hh>

// include Discrete Function
#include <dune/fem/function/adaptivefunction.hh>

// include discrete functiton list headers
#include "../../../misc/discfunclist/discfunclist_xdr.hh"

// include gramian pipeline
#include "../gramianpipeline.hh"

#include "../../operators/interfaces.hh"

#include "../../operators/eye.hh"

// include matrix wrapper
#include "../../../misc/cwrapper/cmatrixwrapper.hh"

// gridinfo header
#include <dune/grid/common/gridinfo.hh>

// subset wrapper for discrete function list
#include "../../../misc/discfunclist/discfunclistsubsetwrapper.hh"
#include "../../../common/test/utility.hh"

#include "testfunctions.hh"

#include "testoperators.hh"

using boost::test_tools::output_test_stream;

using namespace Dune;

struct Traits {
  typedef OneDGrid                                                   GridType;
  typedef DGAdaptiveLeafGridPart< GridType >                         GridPartType;
  enum { dim      = GridType :: dimension };
  enum { dimworld = GridType :: dimensionworld };
  enum { dimRange = 1 };
  enum { polOrd   = 1 };

  typedef FunctionSpace< double, double, dim, dimRange >             FunctionSpaceType;
  typedef FunctionSpace< double, double, dim, dim >                  GradientFunctionSpaceType;
//   typedef DiscontinuousGalerkinSpace
//      <FunctionSpaceType, GridPartType, 0, CachingStorage>         DiscFuncSpaceType;
  typedef FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0,
                             CachingStorage >                        DiscFuncSpaceType;
//    typedef LagrangeDiscreteFunctionSpace
//      < FunctionSpaceType, GridPartType, 1 > DiscFuncSpaceType;
  typedef AdaptiveDiscreteFunction< DiscFuncSpaceType >              DiscreteFunctionType;
  typedef FunctionSpaceType :: RangeType                             RangeType;
  typedef FunctionSpaceType :: DomainType                            DomainType;
  typedef FunctionSpaceType :: DomainFieldType                       DomainFieldType;
  typedef FunctionSpaceType :: RangeFieldType                        RangeFieldType;
};

struct GMT
{

  typedef Traits :: GridType                                         GridType;
  typedef Traits :: GridPartType                                     GridPartType;
  typedef Traits :: FunctionSpaceType                                FunctionSpaceType;
  typedef Traits :: DiscFuncSpaceType                                DiscFuncSpaceType;
  typedef Traits :: DiscreteFunctionType                             DiscreteFunctionType;
  typedef Traits :: RangeType                                        RangeType;
  typedef Traits :: RangeFieldType                                   RangeFieldType;
  typedef Traits :: DomainType                                       DomainType;
  typedef Traits :: DomainFieldType                                  DomainFieldType;
  typedef RBFem :: FunctionList:: Xdr< Traits >                   DiscreteFunctionListType;
  typedef RB :: CMatrixWrapper                                       MatrixType;
  typedef DiscFuncSpaceType :: BaseFunctionSetType                   BaseFunctionSetType;
  typedef GridType :: Codim< 0 > :: Entity                           EntityType;
  typedef DiscFuncSpaceType :: IteratorType                          IteratorType;
  typedef L2Projection< DomainFieldType, RangeFieldType,
                        SineFunction, DiscreteFunctionType >         L2SineProjectionType;

  typedef L2Projection< DomainFieldType, RangeFieldType,
                        ConstantFunction, DiscreteFunctionType >     L2ConstantProjectionType;

  typedef L2Projection< DomainFieldType, RangeFieldType,
                        LinearFunction, DiscreteFunctionType >       L2LinearProjectionType;

  typedef RBFem::LA :: GramianPipeline< DiscreteFunctionListType,
                                 MatrixType >                        GramianPipelineType;
  typedef GramianPipelineType :: OpHandle                            OpHandle;
  typedef GramianPipelineType :: FuncHandle                          FuncHandle;

  typedef RBFem::Decomposed::Operator::IdentityFactory< DiscreteFunctionType >                      MakeEyeType;

  typedef MakeEyeType :: OperatorType :: CompleteType                EyeOperator;

  typedef GradientOperator< DiscreteFunctionType,
                            DiscreteFunctionType >                   GradientLocalOperator;
  typedef DiscreteOperator< GradientLocalOperator,
                            DiscreteFunctionType,
                            DiscreteFunctionType >                   GradientOperatorType;
  typedef ConstantOperator< DiscreteFunctionType,
                            DiscreteFunctionType >                   ConstantLocalOperator;
  typedef DiscreteOperator< ConstantLocalOperator,
                            DiscreteFunctionType,
                            DiscreteFunctionType >                   ConstantOperatorType;
  typedef PlusOperator< DiscreteFunctionType, DiscreteFunctionType > PlusLocalOperator;
  typedef DiscreteOperator< PlusLocalOperator,
                            DiscreteFunctionType,
                            DiscreteFunctionType >                   PlusOperatorType;

  GMT()
    :
      dummy(Dune::RB::InitHelper::one(std::string(getenv("DUNERBHOME")) + "/gramiantest.params")),
      grid(NUM_ELEMENTS, 0, 1),
      gridPart(grid),
      space(gridPart),
      dfList(space, "dfList", true),
      makeeye(space),
      eye(makeeye.getOperator().complete()),
      lop1(1),
      lop2(1),
      lop3(),
      op1(lop1),
      op2(lop2),
      op3(lop3),
      f1("ones", space),
      f2("linear1to10", space)
  {

    ConstantFunction constFunc;
    L2ConstantProjectionType l2constpro;
    l2constpro(constFunc, f1);

    L2LinearProjectionType l2linearpro;
    LinearFunction linearFunc(0, 1);
    l2linearpro(linearFunc, f2);
  }

  ~GMT()
  {
    for (unsigned int i = 0; i < matrixptrs.size(); i++) {
      delete matrices[i];
      delete matrixptrs[i];
      delete refmatrices[i];
      delete refmatrixptrs[i];
    }
    matrices.clear();
    matrixptrs.clear();
    refmatrices.clear();
    refmatrixptrs.clear();
    for (unsigned int i = 0; i < vecptrs.size(); i++) {
      delete vectors[i];
      delete vecptrs[i];
      delete refvectors[i];
      delete refvecptrs[i];
    }
    vectors.clear();
    vecptrs.clear();
    refvectors.clear();
    refvecptrs.clear();
    for (unsigned int i = 0; i < scalarptrs.size(); i++) {
      delete scalars[i];
      delete scalarptrs[i];
      delete refscalars[i];
      delete refscalarptrs[i];
    }
    scalars.clear();
    scalarptrs.clear();
    refscalars.clear();
    refscalarptrs.clear();
  }

  void addSineFunctions(int a, int b)
  {
    for (int k=a; k<=b; ++k) {
      // define a sine function...
      SineFunction contFunc(Traits::dim, k);
      // ...and project it onto the discrete space
      DiscreteFunctionType discFunc("discFunc", space);
      l2pro(contFunc, discFunc);
      // add the function
      dfList.push_back(discFunc);
    }
  }

  double scalarProduct(DiscreteFunctionType & df1, DiscreteFunctionType & df2)
  {
    double res = 0.0;
    typedef GridPartType :: Codim< 0 > :: IteratorType               IteratorType;
    IteratorType gridEnd = gridPart.end<0>();
    for( IteratorType gridIt = gridPart.begin<0>(); gridIt != gridEnd ; ++gridIt )
    {
      EntityType & en = *gridIt;
      typedef DiscreteFunctionType :: LocalFunctionType              LocalFunction;
      typedef EntityType :: Geometry                                 GeometryType;
      LocalFunction lf1 = df1.localFunction(en);
      LocalFunction lf2 = df2.localFunction(en);
      int order_ = 2; //order for the quadrature
      CachingQuadrature<GridPartType, 0> quadrature(en, order_);
      for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint)
      {
        double integrationElement = en.geometry().integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);

        RangeType ret1;
        RangeType ret2;
        lf1.evaluate( quadrature.point(quadPoint), ret1 );
        lf2.evaluate( quadrature.point(quadPoint), ret2 );
        res += integrationElement * weight * ret1[0] * ret2[0];
      }
    }
    return res;
  }

  void initMatrices(const unsigned int matrix_num,
                    const unsigned int vec_num=0,
                    const unsigned int scalar_num=0)
  {
    const unsigned int dfListSize = dfList.size();
    const unsigned int matrixSize = dfListSize * dfListSize;
    const unsigned int vectorSize = dfListSize;
    const unsigned int scalarSize = 1;
    matrixptrs.resize(matrix_num);
    matrices.resize(matrix_num);
    refmatrixptrs.resize(matrix_num);
    refmatrices.resize(matrix_num);
    for (unsigned int i = 0; i < matrix_num; i++) {
      matrixptrs[i] = new double[matrixSize];
      matrices[i]   = new MatrixType(&(matrixptrs[i][0]), dfListSize, dfListSize);
      refmatrixptrs[i] = new double[matrixSize];
      refmatrices[i]   = new MatrixType(&(refmatrixptrs[i][0]), dfListSize, dfListSize);
    }
    vecptrs.resize(vec_num);
    vectors.resize(vec_num);
    refvecptrs.resize(vec_num);
    refvectors.resize(vec_num);
    for (unsigned int i = 0; i < vec_num; i++) {
      vecptrs[i] = new double[vectorSize];
      vectors[i] = new MatrixType(&(vecptrs[i][0]), dfListSize, 1);
      refvecptrs[i] = new double[vectorSize];
      refvectors[i] = new MatrixType(&(refvecptrs[i][0]), dfListSize, 1);
    }
    scalarptrs.resize(scalar_num);
    scalars.resize(scalar_num);
    refscalarptrs.resize(scalar_num);
    refscalars.resize(scalar_num);
    for (unsigned int i = 0; i < scalar_num; i++) {
      scalarptrs[i] = new double[scalarSize];
      scalars[i] = new MatrixType(&(scalarptrs[i][0]), 1, 1);
      refscalarptrs[i] = new double[scalarSize];
      refscalars[i] = new MatrixType(&(refscalarptrs[i][0]), 1, 1);
    }
  }

  template<class Op1, class Op2>
  void gramMatrix(Op1 &o1, Op2 &o2, MatrixType & mat)
  {
    DiscreteFunctionType df1("df1", space);
    DiscreteFunctionType df2("df2", space);
    DiscreteFunctionType arg1("arg1", space);
    DiscreteFunctionType arg2("arg2", space);
    for (unsigned int i = 0; i < dfList.size(); i++) {
      df1.clear();
      arg1.clear();
      dfList.getFunc(i, arg1);
      o1(arg1,df1);
      for (unsigned int j = 0; j < dfList.size(); j++) {
        df2.clear();
        arg2.clear();
        dfList.getFunc(j, arg2);
        o2(arg2,df2);

        mat.set(i,j, scalarProduct(df1, df2) );
      }
    }
  }

  template<class Op>
  void gramVector(Op &o, DiscreteFunctionType & df2, MatrixType & mat)
  {
    DiscreteFunctionType df1("df1", space);
    DiscreteFunctionType arg1("arg1", space);
    for (unsigned int i = 0; i < dfList.size(); i++) {
      df1.clear();
      arg1.clear();
      dfList.getFunc(i, arg1);
      o(arg1,df1);
      mat.set(i,0, scalarProduct(df1, df2) );
    }
  }

  void gramScalar(DiscreteFunctionType &df1, DiscreteFunctionType & df2, MatrixType & mat)
  {
    mat.set(0,0, scalarProduct(df1, df2) );
  }

  void check()
  {
    for (unsigned int i = 0; i < matrices.size(); i++) {
      BOOST_TEST_CHECKPOINT("checking matrix " << i);
      BOOST_CHECK(matrices[i]->compare(*refmatrices[i]));
    }
    for (unsigned int i = 0; i < vectors.size(); i++) {
      BOOST_TEST_CHECKPOINT("checking vector " << i);
      BOOST_CHECK(vectors[i]->compare(*refvectors[i]));
    }
    for (unsigned int i = 0; i < scalars.size(); i++) {
      BOOST_TEST_CHECKPOINT("checking vector " << i);
      BOOST_CHECK(scalars[i]->compare(*refscalars[i]));
    }
  }

  void setting1(GramianPipelineType & pipeline)
  {
    initMatrices(4, 2, 1);
    OpHandle hIdOp = pipeline.getIdentityHandle();
    OpHandle hOp1  = pipeline.registerDiscreteOperator(op1);
    OpHandle hOp2  = pipeline.registerDiscreteOperator(op2);

    FuncHandle hFunc1 = pipeline.registerDiscreteFunction(f1);

    pipeline.addSymmetricGramMatrixComputation(hIdOp, hIdOp, *matrices[0]);
    pipeline.addGramMatrixComputation(hOp1, hIdOp, *matrices[1]);
    pipeline.addSymmetricGramMatrixComputation(hOp1, hOp1, *matrices[2]);
    pipeline.addGramMatrixComputation(hOp2, hOp1, *matrices[3]);

    pipeline.addVectorComputation(hIdOp, hFunc1, *vectors[0]);
    pipeline.addVectorComputation(hOp1, hFunc1, *vectors[1]);

    pipeline.addScalarComputation(hFunc1, hFunc1, *scalars[0]);

    ExecutionTimer timer;

    timer.start();
    gramMatrix(eye, eye, *refmatrices[0]);
    gramMatrix(op1, eye, *refmatrices[1]);
    gramMatrix(op1, op1, *refmatrices[2]);
    gramMatrix(op2, op1, *refmatrices[3]);
    gramVector(eye, f1, *refvectors[0]);
    gramVector(op1, f1, *refvectors[1]);
    gramScalar(f1, f1, *refscalars[0]);
    timer.end();
    std::cout << "Stupid matrix computations took: " << (-timer.read()) << " sec" << std::endl;
  }

  int                      dummy;
  GridType                 grid;
  GridPartType             gridPart;
  DiscFuncSpaceType        space;
  L2SineProjectionType     l2pro;
  DiscreteFunctionListType dfList;

  std::vector<MatrixType*> matrices;
  std::vector<MatrixType*> vectors;
  std::vector<MatrixType*> scalars;
  std::vector<MatrixType*> refmatrices;
  std::vector<MatrixType*> refvectors;
  std::vector<MatrixType*> refscalars;

  MakeEyeType             makeeye;
  EyeOperator             eye;


  PlusLocalOperator       lop1;
  ConstantLocalOperator   lop2;
  GradientLocalOperator   lop3;

  PlusOperatorType            op1;
  ConstantOperatorType        op2;
  GradientOperatorType        op3;

  DiscreteFunctionType    f1;
  DiscreteFunctionType    f2;

private:
  std::vector<double *> matrixptrs;
  std::vector<double *> vecptrs;
  std::vector<double *> scalarptrs;
  std::vector<double *> refmatrixptrs;
  std::vector<double *> refvecptrs;
  std::vector<double *> refscalarptrs;
};

#endif /* end of include guard: TESTCOMMON_V3II97KN */
