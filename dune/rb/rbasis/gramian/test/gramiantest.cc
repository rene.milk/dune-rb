#define BOOST_TEST_MODULE GramianPipeline
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

// constants
//static const int NUM_ELEMENTS = 500;
static const int K_START      = 0;
static const int K_END        = 25;

#include "testcommon.hh"


BOOST_AUTO_TEST_SUITE( GramianPipeline )

BOOST_FIXTURE_TEST_CASE( gramian_matrix_1, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList,K_END+1);

  setting1(pipeline);

  ExecutionTimer timer;
  timer.start();
  pipeline.run();
  timer.end();
  std::cout << "time elapsed for gramian computation: " << (-timer.read()) << std::endl;

  check();
}

BOOST_FIXTURE_TEST_CASE( gramian_matrix_2, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList, 1);

  setting1(pipeline);

  ExecutionTimer timer;
  timer.start();
  pipeline.run();
  timer.end();
  std::cout << "time elapsed for gramian computation: " << (-timer.read()) << std::endl;

  check();
}

BOOST_FIXTURE_TEST_CASE( gramian_matrix_3, GMT )
{
  addSineFunctions(K_START, K_END);

  GramianPipelineType pipeline(dfList, 3);

  setting1(pipeline);

  ExecutionTimer timer;
  timer.start();
  pipeline.run();
  timer.end();
  std::cout << "time elapsed for gramian computation: " << (-timer.read()) << std::endl;

  check();
}
BOOST_AUTO_TEST_SUITE_END( )


