#include  <config.h>

#include "../discretedecomposedfunction.hh"

#include <dune/fem/gridpart/gridpart.hh>
#include <dune/grid/sgrid.hh>
#include <dune/fem/space/dgspace/dgspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

#include  <dune/fem/misc/mpimanager.hh>
#include  <dune/fem/function/common/function.hh>
#include  <dune/fem/function/common/discretefunctionadapter.hh>

#define DUNE_DUCKTYPING 1

#include "../../../datafunc/datafuncif.hh"
#include  <iostream>

using namespace Dune;
using namespace Dune::RB;
using namespace std;

const int dim = 2;

const int polOrder = POLORDER;

typedef Dune::SGrid< dim, dim > SGridType;
typedef LeafGridPart< SGridType > GridPartType;
typedef double DomainFieldType;
typedef double RangeFieldType;

typedef FunctionSpace< double, double, dim, 1 > FunctionSpaceType;

typedef DiscontinuousGalerkinSpace< FunctionSpaceType, GridPartType, polOrder > DiscreteFunctionSpaceType;

typedef AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
typedef DiscreteFunctionType::DomainType DomainType;
typedef DiscreteFunctionType::RangeType RangeType;
//typedef DiscreteFunctionType DomainType;

template< class Output >
void vector_print(Output o)
{
  std::cout << o << " ";
}

class TestFunction: public Fem::Function< FunctionSpaceType, TestFunction >,
public Dune::RBFem::Lib::ITimeCoefficient< double >
{
  typedef Function< FunctionSpaceType, TestFunction > BaseType;
public:
  typedef FunctionSpaceType::DomainType DomainType;
  typedef FunctionSpaceType::RangeType RangeType;

  void evaluate(const DomainType & arg, RangeType & dest) const
  {
    // f(x) = x_{1}^2 ;
    dest[0] = (arg[0] + 10) + 1000 * (arg[1] + 10);
//    std::cout << "in TestFunction: evaluate f(" << arg << ") = " << dest << std::endl;
  }

  inline double coefficient() const
  {
//    std::cout << "in TestFunction: return coefficient: " << 3.141 << std::endl;
    return 3.141;
  }

  std::string symbolicCoefficient() const
  {
    return "3.141";
  }

  bool isMuDependent() const
  {
    return false;
  }

  bool isTimeDependent() const
  {
    return false;
  }

  void setTime(double time)
  {
  }
};

class TestFunction2: public Fem::Function< FunctionSpaceType, TestFunction2 >,
public Dune::RBFem::Lib::ITimeCoefficient< double >
{
  typedef Function< FunctionSpaceType, TestFunction2 > BaseType;
public:
  typedef FunctionSpaceType::DomainType DomainType;
  typedef FunctionSpaceType::RangeType RangeType;

  void evaluate(const DomainType & arg, RangeType & dest) const
  {
    // f(x) = x_{1}^2 ;
    dest[0] = 20;
//    std::cout << "in TestFunction2: evaluate f(" << arg << ") = " << dest << std::endl;
  }

  inline double coefficient() const
  {
//    std::cout << "in TestFunction2: return coefficient: " << 42 << std::endl;
    return 42;
  }

  std::string symbolicCoefficient() const
  {
    return "42";
  }

  bool isMuDependent() const
  {
    return false;
  }

  bool isTimeDependent() const
  {
    return false;
  }

  void setTime(double time)
  {
  }
};

int main(int argc, char *argv[])
{
  typedef DiscreteFunctionAdapter< TestFunction, GridPartType > AdapterType;
  typedef DiscreteFunctionAdapter< TestFunction2, GridPartType > AdapterType2;
  typedef RBFem::Decomposed::Function::Addend< AdapterType, TestFunction > FuncType1;
  typedef RBFem::Decomposed::Function::Addend< AdapterType2, TestFunction2 > FuncType2;
  typedef RBFem::Decomposed::Function::Addend< DiscreteFunctionType, TestFunction2 > FuncType3;

  int ret = 0;
  try
  {
    MPIManager::initialize(argc, argv);
    Parameter::append(argc, argv);

    // init grid
    Dune::FieldVector< int, dim > N(2);
    Dune::FieldVector< SGridType::ctype, dim > L(-1.0);
    Dune::FieldVector< SGridType::ctype, dim > H(1.0);
    SGridType grid(N, L, H);
    GridPartType gridPart(grid);

    // init discrete function space
    DiscreteFunctionSpaceType discreteFunctionSpace(gridPart);
    std::cout
      << (discreteFunctionSpace.continuous() ? std::string("continuous") :
        std::string("not continuous")) << std::endl;

    DiscreteFunctionType argument("argument", discreteFunctionSpace);
    L2Projection< DomainFieldType, RangeFieldType, AdapterType,
        DiscreteFunctionType > l2pro;

    // initialize test functions
    TestFunction f;
    TestFunction2 f2;
    AdapterType test("test", f, gridPart, polOrder);
//  std::cout << "l2 projection on third function" << std::endl;
    l2pro(test, argument);
    AdapterType2 test2("test2", f2, gridPart, polOrder);
    FuncType1 func(test, f);
    FuncType2 func2(test2, f2);
    FuncType3 func3(argument, f2);

    typedef Dune::RB::Tuple::Tuple< FuncType1, FuncType2, FuncType3 > LocalFunctionTuple;

    LocalFunctionTuple lfTuple(func, func2, func3);

    typedef RBFem::Decomposed::Function::Discrete< LocalFunctionTuple, DiscreteFunctionType > DiscreteDecomposedFunctionType;
    typedef RBFem::Decomposed::Function::VirtualInterface< DiscreteFunctionType > DecomposedVectorType;

    DiscreteDecomposedFunctionType ddf(lfTuple, discreteFunctionSpace);
    DiscreteDecomposedFunctionType ddf2(lfTuple, discreteFunctionSpace);

//  std::cout << "sum of two discreteDecomposedFunctions" << std::endl;
    DecomposedVectorType &sumFunc = ddf + ddf2;

    typedef DiscreteDecomposedFunctionType::ComponentType FunctionComponentType;
    typedef DiscreteDecomposedFunctionType::CompleteType CompleteFunctionType;
    typedef DiscreteDecomposedFunctionType::CoefficientList CoefficientList;
    typedef DecomposedVectorType::ComponentType SumFunctionComponentType;
    typedef DecomposedVectorType::CoefficientList SumCoefficientList;
    typedef DecomposedVectorType::CompleteType SumCompleteFunctionType;

    const FunctionComponentType & funcComp1 = ddf.component(0);
    const FunctionComponentType & funcComp2 = ddf.component(1);
    const FunctionComponentType & funcComp3 = sumFunc.component(5);
    const CompleteFunctionType & completeFunc = ddf.complete();
    const SumCompleteFunctionType & sumCompleteFunc = sumFunc.complete();

    typedef DiscreteFunctionSpaceType::IteratorType IteratorType;
    {
      IteratorType it = discreteFunctionSpace.begin();
      typedef FunctionComponentType::LocalFunctionType LocalFunction;
      LocalFunction localF = funcComp1.localFunction(*it);
      LocalFunction localF2 = funcComp2.localFunction(*it);
      LocalFunction localF3 = funcComp3.localFunction(*it);
      RangeType x;
      DomainType null(0.5);
      localF.evaluate(null, x);
      std::cout << "expecting: f(-0.5, -0.5) = 9509.5" << "\ngetting:";
      std::cout << " " << x << " " << std::endl;
      if (x != 9509.5)
      {
        ret++;
      }
      localF2.evaluate(null, x);
      std::cout << "expecting: f2(-0.5, -0.5) = 20" << "\ngetting:";
      std::cout << " " << x << " " << std::endl;
      if (x != 20)
      {
        ret++;
      }
      localF3.evaluate(null, x);
      std::cout << "expecting: f3(-0.5, -0.5) \\circ  9509.5" << "\ngetting:";
      std::cout << " " << x << " " << std::endl;
      if (x < 9509.499 || x > 9509.501)
      {
        ret++;
      }
    }

    {
      IteratorType it = discreteFunctionSpace.begin();
      typedef CompleteFunctionType::LocalFunctionType LocalFunction;
      LocalFunction localF = completeFunc.localFunction(*it);
      LocalFunction localF2 = sumCompleteFunc.localFunction(*it);
      RangeType x;
      DomainType null(0.5);
      localF.evaluate(null, x);
      double expected = 9509.5 * 3.141 + 20 * 42 + 9509.5 * 42;
      std::cout << "expecting: f1(x) * 3.141 + (f2(x) + f3(x)) * 42 = "
        << expected << "\ngetting:";
      std::cout << " " << x << " " << std::endl;
      if (x != expected)
      {
        ret++;
      }
      localF2.evaluate(null, x);
      std::cout << "expecting: 2*(f1(x) * 3.141 + (f2(x) + f3(x)) * 42) = "
        << 2 * expected << "\ngetting:";
      std::cout << " " << x << " " << std::endl;
      if (x != 2 * expected)
      {
        ret++;
      }
    }
  }
  catch (Dune::Exception &e)
  {
    std::cerr << e << std::endl;
  }
  catch (...)
  {
    std::cerr << "unknown exception!" << std::endl;
  }
  return ret;
}
/* vim: set et sw=2: */
