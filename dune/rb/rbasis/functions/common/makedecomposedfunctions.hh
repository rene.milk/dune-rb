#ifndef  __MAKEDECOMPOSEDFUNCTIONS_HH__
#define  __MAKEDECOMPOSEDFUNCTIONS_HH__

#include  "../decomposedfunction.hh"
#include  "../discretedecomposedfunction.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Function
{

/**
 * @brief  class choosing correct container type for a tuple of parametrized
 * functions.
 *
 * @tparam TupleType             tuple of parametrized functions
 * @tparam ModelImp              Model (parameter) type
 * @tparam FunctionSpaceType     function space type underlying the
 *                               parametrized functions.
 * @tparam DiscreteFunctionType  discrete function type or Empty
 */
template<class TupleType, class ModelImp, class FunctionSpaceType,
         class DiscreteFunctionType>
struct TypeChooser
{
private:
  typedef typename ModelImp :: FunctionSpaceType                     ModelFunctionSpaceType;
  typedef typename ModelImp :: GridPartType                          ModelGridPartType;
public:
  /** Selector type for decomposed container
   *
   * This is either a DecomposedFunctionContainer wrapped around a tuple of
   * analytical functions, or if a discrete function space is given as fourth
   * template argument, it is a DiscreteDecomposedFunction around a tuple of
   * discrete functions.
   */
  typedef typename SelectType< is_same< DiscreteFunctionType, Empty >::value,
  Container< TupleType, ModelGridPartType, FunctionSpaceType > ,
  Discrete< TupleType, DiscreteFunctionType > >::Type Type;
};

/** @brief Traits class for the MakeDecomposedFunction class for LocalFunction
 * implementations with zero integer arguments.
 *
 * @tparam LocalFuncImp   LocalFunction implementation taking ModelParams as
 *                        first and only template parameter.
 * @tparam ModelParams    Model Parameter
 */
template<template <class> class LocalFuncImp, class ModelParams>
struct FactoryParams0
{
  typedef ModelParams                                                ModelParamsType;
  template<int i>
  struct Chooser {
    typedef LocalFuncImp< ModelParamsType >                          LocalFuncType;
  };
  static const int numComps = Chooser<0> :: LocalFuncType :: numComps;
  typedef typename ModelParamsType :: GridPartType                   GridPartType;
};

/** @brief Traits class for the MakeDecomposedFunction class for LocalFunction
 * implementations with one integer argument.
 *
 * @tparam LocalFuncImp   LocalFunction implementation taking an integer
 *                        argument and ModelParams as template parameters.
 * @tparam ModelParams    Model Parameter
 */
template<template<int, class> class LocalFuncImp, class ModelParams>
struct FactoryParams1
{
  typedef ModelParams                                                ModelParamsType;
  template<int i>
  struct Chooser {
    typedef LocalFuncImp< i, ModelParamsType >                       LocalFuncType;
  };
  static const int numComps = Chooser<0> :: LocalFuncType :: numComps;
  typedef typename ModelParams :: GridPartType                       GridPartType;
};

/** @brief Traits class for the MakeDecomposedFunction class for LocalFunction
 * implementations with two integer argument.
 *
 * @tparam LocalFuncImp   LocalFunction implementation taking two integer
 *                        arguments and ModelParams as template parameters.
 * @tparam ModelParams    Model Parameter
 */
template<template<int, int, class> class LocalFuncImp,
         class ModelParams,
         int compMax1,
         int compMax2>
struct FactoryParams2
{
  typedef ModelParams                                                ModelParamsType;
  template<int i>
  struct Chooser {
    typedef LocalFuncImp< i % compMax1, (int)(i/compMax1),
                          ModelParamsType >                          LocalFuncType;
  };
  static const int numComps = Chooser<0> :: LocalFuncType :: numComps;
  typedef typename ModelParams :: GridPartType                       GridPartType;
};

/** @brief Traits class for the MakeDecomposedFunction class for LocalFunction
 * implementations with three integer argument.
 *
 * @tparam LocalFuncImp   LocalFunction implementation taking three integer
 *                        arguments and ModelParams as template parameters.
 * @tparam ModelParams    Model Parameter
 */
template<template<int, int, int, class> class LocalFuncImp,
         class ModelParams,
         int compMax1,
         int compMax2,
         int compMax3>
struct FactoryParams3
{
  typedef ModelParams                                                ModelParamsType;
  template<int i>
  struct Chooser {
    typedef LocalFuncImp< i % compMax1,
                          ((int)(i/compMax1)) % compMax2,
                          (int)(i/(compMax1*compMax2)),
                          ModelParamsType >                          LocalFuncType;
  };
  static const int numComps = Chooser<0> :: LocalFuncType :: numComps;
  typedef typename ModelParams :: GridPartType                       GridPartType;
};

template<int start, int end, typename ParamsImp, typename DiscreteFunctionType=Empty>
class AnalyticalFactory
{
public:
  typedef typename ParamsImp :: template Chooser< start >
            :: LocalFuncType                                         LocalFuncType;
  typedef typename ParamsImp :: ModelParamsType                      ModelParamsType;
  typedef typename ParamsImp :: GridPartType                         GridPartType;

  typedef LocalFunctionAdapter< LocalFuncType >                      AdapterType;
  typedef Addend< AdapterType, LocalFuncType >     FuncType;
  typedef AnalyticalFactory< start+1, end, ParamsImp,
                                  DiscreteFunctionType >             PrevType;
  typedef typename PrevType :: TupleType                             PrevTupleType;
  typedef typename RB::Tuple :: Concat< FuncType, PrevTupleType >        ConcatType;
  typedef typename ConcatType :: Type                                TupleType;
public:

  template<class ModelType, class DiscreteFunctionSpaceType>
  AnalyticalFactory(ModelType & model, const DiscreteFunctionSpaceType & space)
    : localFunc_(model, space.gridPart()),
      adapter_(std::string("decomposed"), localFunc_, space.gridPart()),
      msf_(adapter_, localFunc_),
      prev_(model, space),
      prevTuple_(prev_.getTuple()),
      tuple_(ConcatType::concat(msf_, prevTuple_))
  {}

  template<class ModelType>
  AnalyticalFactory(ModelType & model, const GridPartType & gridPart)
    : localFunc_(model, gridPart),
      adapter_(std::string("decomposed"), localFunc_, gridPart),
      msf_(adapter_, localFunc_),
      prev_(model, gridPart),
      prevTuple_(prev_.getTuple()),
      tuple_(ConcatType::concat(msf_, prevTuple_))
  {}

  TupleType & getTuple()
  {
    return tuple_;
  }

private:
  LocalFuncType  localFunc_;
  AdapterType    adapter_;
  FuncType       msf_;
  PrevType       prev_;
  PrevTupleType &prevTuple_;
  TupleType      tuple_;
};

/** @ingroup decomposed
 * @brief makes a decomposed function around a recursively constructed tuple
 * of implementations of DataFuncInterface.
 *
 * This class defines
 *   -# a tuple of discrete functions from local functions with 0 to 3 integer
 *   template arguments by iterating over these template parameters. The local
 *   functions must first be wrapped into one of MakeDecomposedFunctionParams0,
 *   MakeDecomposedFunctionParams1, MakeDecomposedFunctionParams2 or
 *   MakeDecomposedFunctionParams3 and then
 *   -# wraps the tuple inside a decomposed function given by
 *   DecomposedFunctionTypeChooser
 *
 * @tparam end                   length of tuple to be generated.
 * @tparam ParamsImp             traits class describing the DataFuncInterface
 *                               implementation, probably one of
 *                               MakeDecomposedFunctionParams{0,1,2,3}
 * @tparam DiscreteFunctionType  underlying discrete function or Empty
 */
template<int end, typename ParamsImp, typename DiscreteFunctionType>
class AnalyticalFactory<0, end, ParamsImp, DiscreteFunctionType>
{
public:
  //! model parameter type
  typedef typename ParamsImp :: ModelParamsType                      ModelParamsType;
  //! local function type
  typedef typename ParamsImp :: template Chooser< 0 >
            :: LocalFuncType                                         LocalFuncType;
  //! grid part type
  typedef typename ParamsImp :: GridPartType                         GridPartType;

  //! LocalFunctionAdapter around LocalFuncType
  typedef LocalFunctionAdapter< LocalFuncType >                      AdapterType;
  //! Function type decorated with coefficient methods, thus giving
  //! @f$\mu@f$-dpendency.
  typedef Decomposed::Function::Addend< AdapterType, LocalFuncType >     FuncType;

  typedef AnalyticalFactory< 1, end, ParamsImp,
                                  DiscreteFunctionType >             PrevType;
  typedef typename PrevType :: TupleType                             PrevTupleType;
  //! Tuple of local functions
  typedef typename RB::Tuple :: Concat< FuncType, PrevTupleType >        ConcatType;
  typedef typename ConcatType :: Type                                TupleType;
  //! local function space type
  typedef typename AdapterType :: FunctionSpaceType
            :: FunctionSpaceType                                     LocalFunctionSpaceType;

  //! type of the decomposed function container
  typedef typename TypeChooser< TupleType,
                                                  ModelParamsType,
                                                  LocalFunctionSpaceType,
                                                  DiscreteFunctionType >
              :: Type                                                DecomposedFunctionType;
public:
  //! constructor
  template<class ModelType, class DiscreteFunctionSpaceType>
  AnalyticalFactory(ModelType & model, const DiscreteFunctionSpaceType & space)
    : localFunc_(model, space.gridPart()),
      adapter_(std::string("decomposed"), localFunc_, space.gridPart() ),
      msf_(adapter_, localFunc_),
      prev_(model, space),
      prevTuple_(prev_.getTuple()),
      tuple_(ConcatType::concat(msf_,prevTuple_)),
      decFunc_(tuple_, space)
  {}

  //! alternative constructor
  template<class ModelType>
  AnalyticalFactory(ModelType & model, const GridPartType & gridPart)
    : localFunc_(model, gridPart),
      adapter_(std::string("decomposed"), localFunc_, gridPart ),
      msf_(adapter_, localFunc_),
      prev_(model, gridPart),
      prevTuple_(prev_.getTuple()),
      tuple_(ConcatType::concat(msf_,prevTuple_)),
      decFunc_(tuple_, gridPart)
  {}

  //! returns a reference to the tuple of local functions
  TupleType & getTuple()
  {
    return tuple_;
  }

  //! returns a reference to the decomposed function container
  DecomposedFunctionType & getFunction()
  {
    return decFunc_;
  }

private:
  LocalFuncType           localFunc_;
  AdapterType             adapter_;
  FuncType                msf_;
  PrevType                prev_;
  PrevTupleType          &prevTuple_;
  TupleType               tuple_;
  DecomposedFunctionType  decFunc_;
};

template<int end, typename ParamsImp, typename DiscreteFunctionType>
class AnalyticalFactory< end, end, ParamsImp, DiscreteFunctionType >
{
public:
  typedef typename ParamsImp :: GridPartType                         GridPartType;
  typedef RB::Tuple :: Nil                                               TupleType;
public:

  template<class ModelType, class DiscreteFunctionSpaceType>
  AnalyticalFactory(ModelType & model, const DiscreteFunctionSpaceType & space) {}

  template<class ModelType>
  AnalyticalFactory(ModelType & model, const GridPartType & gridPart) {}

  TupleType & getTuple()
  {
    return null_;
  }
private:
  TupleType null_;
};

/** @ingroup decomposed
 * @brief  simplification of MakeDecomposedFunction
 */
template<class DecomposedParams, int end=DecomposedParams::numComps>
class Factory
{
public:
  typedef AnalyticalFactory< 0, end, DecomposedParams >         DecomposedMaker;
  typedef typename DecomposedMaker :: DecomposedFunctionType         DecomposedType;
public:
  template<class DataType, class GridPartType>
  inline Factory(const DataType & dataType,
                                   const GridPartType & gridPart)
  :
    decMaker_( dataType, gridPart ),
    decFunc_( decMaker_.getFunction() ) {}

  inline DecomposedType & getFunction()
  {
    return decFunc_;
  }

  inline const DecomposedType & getFunction() const
  {
    return decFunc_;
  }

private:
  DecomposedMaker  decMaker_;
  DecomposedType  &decFunc_;
};

} // end namespace Function
} // end namespace Decomposed
} // end namespace Dune::RB
} // end namespace Dune

#endif  /*__MAKEDECOMPOSEDFUNCTIONS_HH__*/
/* vim: set et sw=2: */
