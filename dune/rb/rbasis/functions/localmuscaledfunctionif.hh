#ifndef DUNE_RB_LOCALMUSCALEDFUNCTIONIF_HH_
#define DUNE_RB_LOCALMUSCALEDFUNCTIONIF_HH_

#include "../../misc/ducktyping.hh"

namespace Dune {
namespace RBFem {
namespace Lib
{
namespace Function
{

/** @brief interface class for local function objects that can be parametrized
 * by a parameter @f$\mu@f$ and a time variable @f$t@f$.
 *
 * The @link DecomposedVector::complete() complete() @endlink method of a
 * DecomposedVector object usually returns an object of this type wrapped in a
 * LocalFunctionAdapter.
 */
template<class DiscreteFunctionBase>
class ILocalMuScaled
{
public:
  typedef DiscreteFunctionBase                                       DiscreteFunctionBaseType;
  typedef typename DiscreteFunctionBaseType :: RangeType             RangeType;
  typedef typename DiscreteFunctionBaseType
            :: DiscreteFunctionSpaceType :: GridPartType
            :: GridType                                              GridType;
  typedef typename GridType :: template Codim< 0 > :: Entity         EntityType;
public:

  virtual ~ILocalMuScaled() {}

  /** @brief updates the scaling factor for this component by evaluating the
   * coefficient() method. */
  __DUCK_START__
  void setMu()
  __DUCK_END__

  /** @brief init method for LocalFunctionAdapter
   */
  __DUCK_START__
  void init(const EntityType & en)
  __DUCK_END__

  /** @brief local evaluation of component scaled with coefficient
   */
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret)
  {
    DUNE_THROW(NotImplemented, "Method not implemented by derived class");
  }

  /** @brief updates the time variable if discrete function is time dependent
   */
  __DUCK_START__
  void setTime(double time)
  __DUCK_END__

  /** @brief returns a string describing this function
   */
  __DUCK_START__
  std::string name() const
  __DUCK_END__
};

}  // namespace Function
}  // namespace Lib
} // end namespace RBFem
} // end namespace Dune


#endif /* DUNE_RB_LOCALMUSCALEDFUNCTIONIF_HH_ */

