#ifndef  __DISCRETEMUSCALEDFUNCTION_HH__
#define  __DISCRETEMUSCALEDFUNCTION_HH__

#include  <dune/fem/operator/discreteoperatorimp.hh>
#include  <dune/fem/operator/projection/l2projection.hh>

#include "../linevol/spaceoperatorwrapper.hh"
#include "../common/coefficientif.hh"
#include "localmuscaledfunctionif.hh"
#include "../operators/interfaces.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Operator
{
namespace Fv
{
template<class ProjectionFunctionImp, class DiscreteFunctionImp>
class LocalL2Projection;
}
}
namespace Function
{

/** @ingroup decomposed
 * @brief copies the functionality of a wrapped discrete function and adds
 * methods for coefficients to it.
 *
 * This class implements something like
 * @f[ \sigma(\mu) \cdot b(x), \f]
 * where @f$b@f$ is the wrapped discrete function and @f$\sigma@f$ the
 * coefficient function.
 *
 * Usually the type of the wrapped discrete function is a LocalFunctionAdapter
 * of a DatafuncInterface implementation and the coefficient type is the
 * implementation itself. An example usage of this class would be:
 *
 * @code
 *  typedef VelocityConstant<0, ModelParam>       DataFuncImp;
 *  typedef LocalFunctionAdapter<DataFuncImp>     DataFuncImpAdapter;
 *
 *  DataFuncImp velocityFunc(model, gridPart);
 *  DataFuncImpAdapter velocityAdapter("adapter", velocityFunc, gridPart);
 *
 *  typedef Tuple<velocityAdapter>                FuncTuple;
 * @endcode
 *
 * @sa MakeDecomposedFunction for an automatic generation of a tuple of
 * DecomposedFunctionAddend objects from data functions
 *
 * @tparam DiscreteFunctionImp  type of discrete function @f$b@f$ derived from
 *                              HasLocalFunction
 * @tparam CoefficientImp       type of coefficient function @f$\sigma@f$
 *                              derived from CoefficientTimeInterface
 */
template <class DiscreteFunctionImp, class CoefficientImp>
class Addend
  : public HasLocalFunction,
    public Lib::ITimeCoefficient< typename DiscreteFunctionImp :: RangeType :: field_type >
{
public:
  //! discrete function space type
  typedef typename DiscreteFunctionImp :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
  //! grid part type
  typedef typename DiscreteFunctionSpaceType :: GridPartType         GridPartType;
  //! function space type
  typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType    FunctionSpaceType;
  //! entity type
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: Entity                                                EntityType;
  //! domain vector space
  typedef typename DiscreteFunctionImp :: DomainType                 DomainType;
  //! domain field type
  typedef typename DomainType :: field_type                          DomainFieldType;
  //! range vector type
  typedef typename DiscreteFunctionImp :: RangeType                  RangeType;
  //! range field type
  typedef typename RangeType :: field_type                           RangeFieldType;
  //! field type of coefficient method
  typedef RangeFieldType                                             ScalarType;
  //! local function type
  typedef typename DiscreteFunctionImp :: LocalFunctionType          LocalFunctionType;

  //! coefficient interface
  typedef Lib::ITimeCoefficient< RangeFieldType >                 CoefficientInterfaceType;
public:
  //! constructor
  Addend(DiscreteFunctionImp & func, CoefficientImp & coeff)
    : func_(func), coeff_(coeff)
  {
  }

  //! returns a local function
  const LocalFunctionType localFunction(const EntityType & en)
  {
    return func_.localFunction(en);
  }

  //! global evaluation of the function
  void evaluate(const DomainType & localX, RangeType & ret)
  {
    func_.evaluate(localX, ret);
  }

  inline ScalarType coefficient() const
  {
    return coeff_.coefficient();
  }

  inline std::string symbolicCoefficient() const
  {
    return coeff_.symbolicCoefficient();
  }

  inline const DiscreteFunctionSpaceType & space() const
  {
    return func_.space();
  }

  inline bool isMuDependent() const
  {
    return coeff_.isMuDependent();
  }

  inline bool isTimeDependent() const
  {
    return coeff_.isTimeDependent();
  }

  inline void setTime(double time)
  {
    coeff_.setTime(time);
  }
private:
  DiscreteFunctionImp & func_;
  CoefficientImp & coeff_;
};

/** @brief local function type functionality for the affine combination of
 * function returned by the complete() method of a decomposed function.
 *
 * A decomposed function is solely defined by a tuple of coefficients and
 * components. In order to efficiently evaluate a discrete function constructed
 * out of this tuple, the coefficients need to be evaluated @em first and only
 * @em once, such that the components can be added with correct scaling factors
 * on each grid entity.
 *
 * This class adds the described functionality to the local function of a
 * component and can again be used as a local function by wrapping it in a
 * LocalFunctionAdapter.
 */
template <class DiscreteFunctionBase>
class LocalMuScaled
  : public ObjPointerStorage,
    public Lib::Function::ILocalMuScaled<DiscreteFunctionBase>
{
public:
  //! discrete function type
  typedef DiscreteFunctionBase                                       DiscreteFunctionBaseType;
  //! discrete function space type
  typedef typename DiscreteFunctionBase :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  //! function space type
  typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType    FunctionSpaceType;
  //! grid part type
  typedef typename DiscreteFunctionSpaceType :: GridPartType         GridPartType;
  //! grid type
  typedef typename GridPartType :: GridType                          GridType;
  //! entity type
  typedef typename GridType :: template Codim< 0 > :: Entity         EntityType;
  //! domain vector space
  typedef typename DiscreteFunctionBase :: DomainType                DomainType;
  //! range vector space
  typedef typename DiscreteFunctionBase :: RangeType                 RangeType;

public:
  //! constructor
  LocalMuScaled(DiscreteFunctionBase & dfBase, std::string name)
    : name_(name), dfBase_(dfBase), sigma_(dfBase_.coefficient())
  {
  }

  inline void setMu()
  {
    sigma_ = dfBase_.coefficient();
  }

  void init(const EntityType & en)
  {
    en_ = &en;
  }

  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret)
  {
    typedef typename DiscreteFunctionBase :: LocalFunctionType       LocalFunctionType;

    LocalFunctionType lf = dfBase_.localFunction(*en_);
    lf.evaluate(localX, ret);
    // scale with sigma
    ret *= sigma_;
  }

  inline void setTime(double time)
  {
    dfBase_.setTime(time);
  }

  std::string name() const
  {
    return name_;
  }

private:
  std::string name_;
  DiscreteFunctionBase & dfBase_;
  double sigma_;
  const EntityType * en_;
};

/** An object of this class is returned, when two LocalMuScaledFunction objects
 * are added via an operator+() method.
 */
template <class Afunc, class Bfunc>
class CombinedLocalMuScaled
  : public ObjPointerStorage,
    public Lib::Function::ILocalMuScaled< typename Afunc :: DiscreteFunctionBaseType >
{
public:
  typedef typename Afunc :: DiscreteFunctionBaseType                 DiscreteFunctionBaseType;
  typedef typename DiscreteFunctionBaseType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionBaseType :: GridPartType          GridPartType;
  typedef typename DiscreteFunctionBaseType :: FunctionSpaceType     FunctionSpaceType;
  typedef typename DiscreteFunctionBaseType :: GridPartType
            :: GridType                                              GridType;
  typedef typename GridType :: template Codim< 0 > :: Entity         EntityType;
  typedef typename DiscreteFunctionBaseType :: DomainType            DomainType;
  typedef typename DiscreteFunctionBaseType :: RangeType             RangeType;

public:
  //! constructor
  CombinedLocalMuScaled(Afunc & a, Bfunc & b)
    : a_(a), b_(b)
  {
  }

  inline void setMu()
  {
    a_.setMu();
    b_.setMu();
  }

  inline void init(const EntityType & en)
  {
    a_.init(en);
    b_.init(en);
  }

  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret)
  {
    a_.evaluate(localX, ret);
    RangeType ret2;
    b_.evaluate(localX, ret2);
    ret += ret2;
  }

  inline void setTime(double time)
  {
    a_.setTime(time);
    b_.setTime(time);
  }

  std::string name() const
  {
    return a_.name() + "plus" + b_.name();
  }

private:
  Afunc a_;
  Bfunc b_;
};

/** recursively constructs a linear combination of LocalMuScaledFunction
 * objects by adding them together.
 *
 * This is just a helper class used by LocalMuScaledFunctionDefault.
 *
 * @sa LocalMuScaledFunction
 *
 * @tparam  i             number of elements in DFTupleType
 * @tparam  DFTupleType   tuple of discrete functions
*/
template<int i, typename DFTupleType>
class LocalMuScaledDefaultCreator
{
public:
  typedef typename RB::Tuple :: ElementType< i, DFTupleType > :: type    BBaseType;

  typedef typename BBaseType :: DiscreteFunctionSpaceType            DiscreteFunctionSpaceType;

  typedef Decomposed::Function::LocalMuScaled< BBaseType >                         BType;

  typedef LocalMuScaledDefaultCreator< i-1, DFTupleType >    PrevType;

  typedef typename PrevType :: type                                  AType;

  typedef CombinedLocalMuScaled< AType, BType >              type;
public:
  //! constructor
  LocalMuScaledDefaultCreator(DFTupleType & t)
    : a_(t),
      b_(RB::Tuple::ElementAccess<i>::get(t),"dummy"),
      c_(a_.getCombinedFunc(),b_) {}

  //! return the combined local operator
  type & getCombinedFunc()
  {
    return c_;
  }
private:
  PrevType a_;
  BType b_;
  type c_;
};

template<typename DFTupleType>
class LocalMuScaledDefaultCreator<0, DFTupleType>
{
public:
  typedef typename RB::Tuple :: ElementType< 0, DFTupleType > :: type    BBaseType;
  typedef typename BBaseType :: DiscreteFunctionSpaceType            DiscreteFunctionSpaceType;
  typedef LocalMuScaled< BBaseType >                         BType;
  typedef BType                                                      type;
public:
  LocalMuScaledDefaultCreator(DFTupleType & t)
    : b_(RB::Tuple::ElementAccess<0>::get(t),"dummy") {}

  BType & getCombinedFunc()
  {
    return b_;
  }
private:
  BType b_;
};

/** @brief makes a sum of LocalMuScaledFunction objects from a tuple of
 * component functions with coefficient information.
 *
 * After wrapped in a LocalFunctionAdapter an object of this class can be the
 * equivalent of the local function of a discrete function returned by the
 * complete() method of decomposed function.
 *
 * @sa LocalMuScaledFunction, LocalMuScaledFunctionDefaultCreator and
 * DecomposedVector :: complete()
 */
template<typename DFTupleType>
class LocalMuScaledDefault
  : public ObjPointerStorage,
    public Lib::Function::ILocalMuScaled<
             typename LocalMuScaledDefaultCreator<
                       RB::Tuple :: Size< DFTupleType > :: value - 1,
                       DFTupleType >
               :: type >
{
  //! number of components in tuple resp. sum of local functions
  static const int numComps = RB::Tuple :: Size< DFTupleType > :: value;
  typedef LocalMuScaledDefaultCreator< numComps-1,
                                               DFTupleType >         CreatorType;
  typedef typename CreatorType :: type                               WrapeeType;
public:
  //! discrete function base type
  typedef WrapeeType                                                 DiscreteFunctionBaseType;
  //! domain vector type
  typedef typename WrapeeType :: DomainType                          DomainType;
  //! range vector type
  typedef typename WrapeeType :: RangeType                           RangeType;
  //! function space type
  typedef typename WrapeeType :: FunctionSpaceType                   FunctionSpaceType;
  //! grid part type
  typedef typename WrapeeType :: DiscreteFunctionSpaceType
            :: GridPartType                                          GridPartType;
  //! grid type
  typedef typename GridPartType :: GridType                          GridType;
  //! entity type
  typedef typename GridType :: template Codim< 0 > :: Entity         EntityType;
public:
  //! constructor
  LocalMuScaledDefault(DFTupleType & tuple)
    : creator_(tuple), localFunc_(creator_.getCombinedFunc())
  {}

  inline void setMu()
  {
    localFunc_.setMu();
  }

  inline void init(const EntityType & en)
  {
    localFunc_.init(en);
  }

  template<class PointType>
  inline void evaluate(const PointType & localX, RangeType & ret)
  {
    localFunc_.evaluate(localX, ret);
  }

  inline std::string name() const
  {
    return localFunc_.name();
  }

  inline void setTime(double time)
  {
    localFunc_.setTime(time);
  }
private:
  CreatorType creator_;
  WrapeeType & localFunc_;
};


//! \todo define use of this class
template<class DiscreteFunctionBase>
class IGlobalMuScaledAssembler
{
public:
  virtual ~IGlobalMuScaledAssembler() {}

  virtual DiscreteFunctionBase & assemble() = 0;
};


// forward declaration
template <class DiscreteFunctionBase, class LocalFunctionImp, class DiscreteMuScaledFunctionImp>
class GlobalMuScaledDefault;

/** @ingroup decomposed
 * @brief a decomposed function returns this class via its completeMuScaled()
 * method.
 *
 * This class wraps a LocalFunctionAdapter of a LocalMuScaledFunctionDefault
 * object.
 */
template<class DiscreteFunctionBase, class LocalFunctionImp>
class GlobalMuScaled
  : public GlobalMuScaledDefault
             <DiscreteFunctionBase,
              LocalFunctionImp,
              GlobalMuScaled
                <DiscreteFunctionBase, LocalFunctionImp> >,
    public LocalFunctionAdapter<LocalFunctionImp>
{
public:
  //! discrete function type
  typedef DiscreteFunctionBase                                       DiscreteFunctionBaseType;
  //! local (mu scaled) function type functionality
  typedef LocalFunctionImp                                           LocalMuScaledFunctionType;
  //! domain vector space type
  typedef typename DiscreteFunctionBaseType :: DomainType            DomainType;
  //! domain vector field type
  typedef typename DomainType :: field_type                          DomainFieldType;
  //! range vector space type
  typedef typename DiscreteFunctionBaseType :: RangeType             RangeType;
  //! range vector field type
  typedef typename RangeType :: field_type                           RangeFieldType;
  //! discrete function space type
  typedef typename DiscreteFunctionBaseType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  //! grid part type
  typedef typename DiscreteFunctionSpaceType :: GridPartType         GridPartType;
  //! local function adapter LocalMuScaledFunction
  typedef LocalFunctionAdapter< LocalFunctionImp >                   LFAdapterType;
  //! efficient, i.e. local l2 projection into the discrete function type
  typedef Decomposed::Operator::Fv::LocalL2Projection< LFAdapterType, DiscreteFunctionBase >   WrapperType;
  //! constant operator type of the l2 projection
  typedef DiscreteOperator< WrapperType, DiscreteFunctionBaseType,
                            DiscreteFunctionBaseType >               ConstantOperatorType;
  typedef Mapping< DomainFieldType, RangeFieldType,
                   DiscreteFunctionBaseType,
                   DiscreteFunctionBaseType >                        MappingType;
  typedef Lib::Operator::TimeSpaceWrapper< ConstantOperatorType, MappingType,
                                DiscreteFunctionBaseType >           ConstantSpaceOperatorType;
  typedef Lib::Operator::TimeSpaceMapping< MappingType,
                                DiscreteFunctionBaseType >           ConstantMappingType;

  /**
   * @brief this *IS* a dirty hack!
   *
   * But the LocalFunctionAdapter says it is a discrete function, such that
   * the L2Projection does not work out of the box, because it wants to use a
   * global evaluate method.
   */
  class MyL2Projection
    : public DGL2ProjectionImpl
  {
  public:
  };

public:
  //! constructor
  GlobalMuScaled(LocalMuScaledFunctionType & lmsf,
                           const DiscreteFunctionSpaceType & space) :
    LFAdapterType(lmsf.name(), lmsf, space.gridPart()),
    space_(space),
    lmsf_(lmsf),
    dfBase_(0),
    constLocalOp_(*this, lmsf_),
    constOp_(constLocalOp_),
    constSpaceOp_(space_, constOp_)
  {}

  //! destructor
  virtual ~GlobalMuScaled()
  {
    if( dfBase_ != 0 ) {
      delete dfBase_;
    }
  }

  //! evaluate the sum of local functions and write into an "assembled"
  //! discrete function
  //!
  //! @return the newly assembled discrete function
  virtual DiscreteFunctionBase & assemble()
  {
    if( dfBase_ == 0 ) {
      dfBase_ = new DiscreteFunctionBase(lmsf_.name(), space_);
    }

    lmsf_.setMu();
    L2Projection< DomainFieldType, RangeFieldType, LFAdapterType, DiscreteFunctionBase > l2pro;
    l2pro(*this, *dfBase_);
    return *dfBase_;
  }

  //! returns the discrete function as a constant operator
  //!
  //! \note This does not consume a grid walk, because the operator is given
  //! purely local, thanks to the LocalL2Projection
  inline ConstantSpaceOperatorType & constantOperator()
  {
    return constSpaceOp_;
  }

  //! returns the local (mu scaled) function (not a real LocalFunction, needs
  //! to be wrapped in a LocalFunctionAdapter first)
  LocalMuScaledFunctionType & getLocalMuScaledFunc() const
  {
    return lmsf_;
  }

  //! returns the discrete function space
  const DiscreteFunctionSpaceType & space() const
  {
    return space_;
  }

private:
  const DiscreteFunctionSpaceType &space_;
  LocalMuScaledFunctionType       &lmsf_;
  DiscreteFunctionBase            *dfBase_;
  WrapperType                      constLocalOp_;
  ConstantOperatorType             constOp_;
  ConstantSpaceOperatorType        constSpaceOp_;
};

//! default implementation of a DiscreteMuScaledFunction that implements an operator+() method.
//!
//! \todo is this really a \em default implementation?
template<class DiscreteFunctionBase, class LocalFunctionImp, class DiscreteMuScaledFunctionImp>
class GlobalMuScaledDefault
  : public IGlobalMuScaledAssembler
            <DiscreteFunctionBase>,
    public ObjPointerStorage
{
  typedef LocalFunctionImp                                           LocalFunctionType;
public:
  typedef DiscreteFunctionBase                                       DiscreteFunctionBaseType;
public:
  template<class LocalFunctionAddend>
  struct AddType
  {
    typedef CombinedLocalMuScaled< LocalFunctionType,
                                           LocalFunctionAddend >     CombinedLocalType;
    typedef GlobalMuScaled< DiscreteFunctionBaseType,
                                      CombinedLocalType >            type;
  };

  /** returns a new DiscreteMuScaledFunction object by adding two others.
   *
   * The new DiscreteMuScaledFunction works very efficiently because under the
   * hood the local function parts are combined, so every operation on it,
   * needs at most one grid walk only.
   *
   * An example usage of this function would look like this:
   * @code
   *   typedef typeof(makeT(d1) + makeT(d2))    AddType;
   *
   *   AddType sum = d1 + d2;
   *
   *   typedef typename AddType :: DiscreteFunctionBaseType DFType;
   *
   *   // only ONE grid walk needed
   *   DFType df = sum.assemble();
   * @endcode
   */
  template<class LocalFunctionAddend>
  typename AddType<LocalFunctionAddend> :: type &
  operator + (const GlobalMuScaled
                      <DiscreteFunctionBaseType, LocalFunctionAddend> & addend)
  {
    typedef typename AddType< LocalFunctionAddend > :: type          NewFuncType;
    typedef typename AddType< LocalFunctionAddend >
              :: CombinedLocalType                                   NewLocalFuncType;

    NewLocalFuncType * newLocFunc
      = new NewLocalFuncType(asImp().getLocalMuScaledFunc(), addend.getLocalMuScaledFunc());

    NewFuncType * newFunc = new NewFuncType(*newLocFunc, asImp().space());

    this->saveObjPointer( newFunc, newLocFunc );

    return (*newFunc);
  }
private:
  DiscreteMuScaledFunctionImp & asImp()
  {
    return static_cast<DiscreteMuScaledFunctionImp &>(*this);
  }
};

} // end namespace Function

namespace Operator {
namespace Fv {

/** makes a constant operator out of a discrete function
 *
 * Sometimes it is necessary to add a discrete function as a constant offset to
 * an operator. This class helps here by creating a local operator that always
 * returns the underlying discrete function regardless of the input, the
 * operator applies to. Then, the operator can be added as the constant offset.
 */
template<typename DiscreteFunctionImp>
class FunctionWrapper
  : public Lib::Operator::ILocalParametrized
             <DiscreteFunctionImp,
              FunctionWrapper<DiscreteFunctionImp> >
{
public:

  //! discrete function type
  typedef DiscreteFunctionImp                                        DiscreteFunctionType;
  //! DFDomainType
  typedef DiscreteFunctionImp                                        DomainType;
  //! DFRangeType
  typedef DiscreteFunctionImp                                        RangeType;
  //! the scalar coefficient's type
  typedef typename DomainType :: DomainType :: field_type            ScalarType;
  //! realization of the LocalOperatorInterface
  typedef Lib::Operator::ILocalParametrized< DomainType,
                           FunctionWrapper
                               < DiscreteFunctionImp > >             InterfaceType;
public:
  //! constructor
  FunctionWrapper(const DiscreteFunctionType & wrapee)
    : wrapee_(wrapee)
  { }

  //! prepareGlobal is called before the grid walktrough
  void prepareGlobal(const DomainType & arg, RangeType & dest)
  {
    dest_ = &dest;
  }

  //! finalize the walktrough
  void finalizeGlobal()
  {
#if defined(DEBUG) && DEBUG > 1000
    std::cout << "in DiscreteFunctionToConstantOperatorWrapper :: finalizeGlobal():\nreturned discrete fucntion:" << std::endl;
    dest_->print(std::cout);
#endif
  }

  //! called before applyLocal on every entity (one entity version)
  template<class EntityType>
  void prepareLocal (EntityType & en)
  { }

  //! called after applyLocal on everyEntity (one entity version)
  template<class EntityType>
  void finalizeLocal(EntityType & en)
  { }

  //! things to do on one entity
  template<class EntityType>
  void applyLocal(EntityType & en)
  {
    typedef typename DiscreteFunctionType :: LocalFunctionType       LocalFunction;
    typedef typename RangeType :: LocalFunctionType                  RangeLocalFunction;

    LocalFunction          lf = wrapee_.localFunction(en);
    RangeLocalFunction destLf = (*dest_).localFunction(en);

    for( int i = 0 ; i < destLf.numDofs()  ; ++i )
    {
      destLf[i] += lf[i];
    }
  }

  //! updates the scalar the local operator is multiplied with
  inline void scaleIt(ScalarType scalar)
  { }

private:
  const DiscreteFunctionType &wrapee_;
  RangeType                  *dest_;
}; // end of DiscreteFunctionToConstantOperatorWrapper

/** @brief A local operator implementing an @f$L^2@f$-projection of an
 * analytical function
 *
 * @tparam ProjectionFunctionImp  the analytical function type to be projected
 *                                this should be of type LocalMuScaledFunction
 *                                or LocalMuScaledFunctionDefault
 * @tparam DiscreteFunctionImp    the discrete function type to project on
 */
template<class ProjectionFunctionImp, class DiscreteFunctionImp>
class LocalL2Projection
  : public Lib::Operator::ILocalParametrized
             <DiscreteFunctionImp,
              LocalL2Projection<ProjectionFunctionImp, DiscreteFunctionImp> >
{
public:

  //! local analytic function that is to be projected
  typedef ProjectionFunctionImp                                      ProjectionFunction;
  //! eval type for LocalFunctionAdapter
  typedef typename ProjectionFunctionImp :: LocalFunctionImplType    EvalType;
  //! discrete function type
  typedef DiscreteFunctionImp                                        DiscreteFunctionType;
  //! DFDomainType
  typedef DiscreteFunctionImp                                        DomainType;
  //! DFRangeType
  typedef DiscreteFunctionImp                                        RangeType;
  //! the scalar coefficient's type
  typedef typename DomainType :: DomainType :: field_type            ScalarType;
  //! realization of the LocalOperatorInterface
  typedef Lib::Operator::ILocalParametrized< DomainType,
                                          LocalL2Projection
                                            < ProjectionFunctionImp,
                                              DiscreteFunctionImp > > InterfaceType;
private:
  //! discrete function space type
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  //! grid part type
  typedef typename DiscreteFunctionSpaceType :: Traits
            :: GridPartType                                          GridPartType;
  //! grid type
  typedef typename GridPartType :: GridType                          GridType;
  //! type of quadrature
  typedef CachingQuadrature< GridPartType, 0 >                       QuadratureType;
  //! type of local mass matrix
  typedef LocalDGMassMatrix< DiscreteFunctionSpaceType,
                             QuadratureType >                        LocalMassMatrixType;
public:
  //! constructor
  LocalL2Projection(const ProjectionFunction & wrapee, EvalType & eval, int polOrder = -1)
    : wrapee_(wrapee),
      eval_(eval),
      polOrder_(polOrder)
  { }

  //! prepareGlobal is called before the grid walktrough
  void prepareGlobal(const DomainType & arg, RangeType & dest)
  {
    eval_.setMu();
    dest_ = &dest;

    // clear destination
    (*dest_).clear();

    const DiscreteFunctionSpaceType & space = dest.space();

    quadOrder_ = (polOrder_ == -1) ? (2 * space.order()) : polOrder_;
    // create local mass matrix object
    massMatrix_ = new LocalMassMatrixType( space, quadOrder_ );
  }

  //! finalize the walktrough
  void finalizeGlobal()
  {
#if defined(DEBUG) && DEBUG > 1000
    std::cout << "in DiscreteFunctionToConstantOperatorWrapper :: finalizeGlobal():\nreturned discrete function:" << std::endl;
    dest_->print(std::cout);
#endif
    delete massMatrix_;
  }

  //! called before applyLocal on every entity (one entity version)
  template<class EntityType>
  void prepareLocal (EntityType & en)
  { }

  //! called after applyLocal on everyEntity (one entity version)
  template<class EntityType>
  void finalizeLocal(EntityType & en)
  { }

  //! things to do on one entity
  template<class EntityType>
  void applyLocal(EntityType & en)
  {
    typedef typename DiscreteFunctionType :: LocalFunctionType       LocalFuncType;
    typedef typename ProjectionFunction :: LocalFunctionType         LocalFType;
    typedef typename LocalFuncType :: BaseFunctionSetType            BaseFunctionSetType;


    // check whether geometry mappings are affine or not
    const bool affineMapping = massMatrix_->affine();


    const typename GridType::template Codim<0>::Geometry& geo = en.geometry();

    // get quadrature
    QuadratureType quad(en, quadOrder_);

    // get local function of destination
    LocalFuncType lf = (*dest_).localFunction(en);
    // get local function of argument
    const LocalFType f = wrapee_.localFunction(en);

    // get base function set
    const BaseFunctionSetType & baseset = lf.baseFunctionSet();

    const int quadNop = quad.nop();
    const int numDofs = lf.numDofs();

    typename DiscreteFunctionSpaceType::RangeType ret (0.0);
    typename DiscreteFunctionSpaceType::RangeType phi (0.0);

    for(int qP = 0; qP < quadNop ; ++qP)
    {
      const double intel = (affineMapping) ?
        quad.weight(qP) : // affine case
        quad.weight(qP) * geo.integrationElement( quad.point(qP) ); // general case

      // evaluate function
      f.evaluate(quad[qP], ret);

      // do projection
      for(int i=0; i<numDofs; ++i)
      {
        baseset.evaluate(i, quad[qP], phi);
        lf[i] += intel * (ret * phi) ;
      }
    }

    // in case of non-linear mapping apply inverse
    if ( ! affineMapping )
    {
      (*massMatrix_).applyInverse( en, lf );
    }
  }

  inline void setTime(double time)
  {
    eval_.setTime(time);
  }

  bool isTimeDependent() const
  {
    return eval_.isTimeDependent();
  }

  bool isMuDependent() const
  {
    return eval_.isMuDependent();
  }

  std::string symbolicCoefficient() const
  {
    return eval_.symbolicCoefficient();
  }

  ScalarType coefficient() const
  {
    return coefficient();
  }

private:
  const ProjectionFunction &wrapee_;
  EvalType                 &eval_;
  const int                 polOrder_;
  int                       quadOrder_;
  RangeType                *dest_;
  LocalMassMatrixType      *massMatrix_;
}; // LocalL2Projection

} // end namespace Fv
} // end namespace Operator

} // end namespace Decomposed
} // end namespace Dune::RB
} // end namespace Dune

#endif  /*__DISCRETEMUSCALEDFUNCTION_HH__*/
/* vim: set et sw=2: */
