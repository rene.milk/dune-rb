#ifndef  __FUNCTIONCOMPONENT_HH__
#define  __FUNCTIONCOMPONENT_HH__

#include <dune/fem/function/common/discretefunction.hh>

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Function
{

/** @ingroup decomposed
 * @brief class for a space dependent component in a decomposed function
 * returned by the component() method
 *
 * It wraps a pointer to a Dune::Fem::Function and should only be used with the
 * component() method of a decomposed function class.
 *
 * The use of this class is to hide a functions' full type such that a list of
 * functions with usually different types can be realized by a container class.
 *
 * \note The function component is only usable after it is initialized with an
 * Dune::Function instance that it can point to. This Dune::Function can be
 * given to a FunctionComponent object either via a suitable constructor or via
 * the setFunction method. The safest way to initialize the pointer, however,
 * is the aforementioned way to copy a FunctionComponent out of a
 * DecomposedFunction through the component method, which follows this
 * procedure reliably.
 */
template <typename GridPartImp, typename FunctionSpaceImp>
class ComponentEval
{
public:
  //! function space type
  typedef FunctionSpaceImp                                           FunctionSpaceType;
  //! grid part type
  typedef GridPartImp                                                GridPartType;
  //! type of domain (vector space) of function component
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! type of range (vector space) of function component
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! type of domain field (scalar value)
  typedef typename FunctionSpaceType :: DomainFieldType              DomainFieldType;
  //! type of range field (scalar value)
  typedef typename FunctionSpaceType :: DomainFieldType              RangeFieldType;
  //! type of Entity on which we can evaluate the function locally
  typedef typename GridPartType :: GridType :: template Codim< 0 >
            :: Entity                                                EntityType;
  //! iterator type
  typedef typename GridPartType :: template Codim< 0 >
            :: IteratorType                                          IteratorType;

private:
  //! HasLocalFunction is base type of all functions that we can handle
  typedef HasLocalFunction                                           BaseType;
  //! function type needed for the WrapHelper
  typedef void EvaluateLocalFuncType(BaseType * &, const EntityType &, const DomainType&, RangeType &);
  typedef void SetTimeInDecorator(BaseType * &, const double time);
private:

  //! helper class that handles a pointer to the <it>actual</it> implementation
  //! of the underlying operator's apply method.
  template <class FuncType>
  struct WrapHelper
  {
    //! calls the apply method of the OpType class.
    static void evaluateLocalWrapper(BaseType *& b, const EntityType & en, const DomainType & localX, RangeType & ret)
    {
      typedef typename FuncType :: LocalFunctionType                 LocalFunctionType;
      LocalFunctionType lf = static_cast<FuncType &>(*b).localFunction(en);
      lf.evaluate(localX, ret);
    }

    //! calls the setTime method in DecomposedFunctionAddend wrapper
    static void setTimeWrapper(BaseType *& b, const double time)
    {
      static_cast<FuncType &>(*b).setTime(time);
    }

    //! sets a function pointer applyFunc to the apply method of the "real"
    //! operator implementation OpType.
    static void addInternalFunc(BaseType *& bfunc,
                                EvaluateLocalFuncType* & evaluateLocalFunc,
                                SetTimeInDecorator* & setTimeFunc,
                                FuncType & func)
    {
      bfunc = &func;
      evaluateLocalFunc = evaluateLocalWrapper;
      setTimeFunc = setTimeWrapper;
    }
  };

public:

  //! constructor that binds FunctionComponent to function func
  //
  // \param op actual implementation of underlying discrete function or
  //           discrete function adapter
  template <class FuncType>
  ComponentEval(FuncType & func)
   : gridPart_(func.space().gridPart()),
     muDependent_(func.isMuDependent()),
     timeDependent_(func.isTimeDependent())
  {
    WrapHelper<FuncType> :: addInternalFunc(func_, evaluateLocalFunc_, setTimeFunc_, func);
  }

  inline void init(const EntityType & entity)
  {
    en_ = &entity;
  }

  template<class PointType>
  inline void evaluate(const PointType & localX, RangeType & ret)
  {
    evaluateLocal(*en_, coordinate( localX ), ret);
  }

  inline bool isMuDependent() const
  {
    return muDependent_;
  }

  inline bool isTimeDependent() const
  {
    return timeDependent_;
  }

  //! methods that evaluates function component locally
  //!
  //! \param[in] en     Entity on which function is to be evaluated
  //! \param[in] localX position at which function is to be evaluated in local
  //! coordinates
  //! \param[out] ret    return value
  void evaluateLocal(const EntityType & en, const DomainType & localX, RangeType & ret) const
  {
    BaseType * bfunc = const_cast<BaseType *>(func_);
    assert(bfunc);
    (*evaluateLocalFunc_)(bfunc, en, localX, ret);
  }

  void setTime(const double time)
  {
    BaseType * bfunc = const_cast<BaseType *>(func_);
    assert(bfunc);
    (*setTimeFunc_)(bfunc, time);
  }

private:
  BaseType              *func_;
  EvaluateLocalFuncType *evaluateLocalFunc_;
  SetTimeInDecorator    *setTimeFunc_;
  const GridPartType    &gridPart_;
  const EntityType      *en_;
  bool                   muDependent_;
  bool                   timeDependent_;
};

}  // namespace Function
}  // namespace Lib
} // end namespace RBFem
} // end namespace Dune


#endif  /*__FUNCTIONCOMPONENT_HH__*/
/* vim: set et sw=2: */
