#ifndef  __DISCRETEDECOMPOSEDFUNCTION_HH__
#define  __DISCRETEDECOMPOSEDFUNCTION_HH__

#include "../../misc/tuples.hh"
#include  <dune/fem/operator/discreteoperatorimp.hh>
#include <dune/common/deprecated.hh>
#include  <iostream>

#include  "functioncomponent.hh"
#include  "localmuscaledfunction.hh"
#include  "decomposedfunction.hh"

#define __DDF_HH_MIN(a,b)  ((a < b) ? a : b)

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Function
{

/** @ingroup decomposed
 * @brief default implementation for %decomposed functions where
 * DiscreteDecomposedFunction and DecomposedFunctionContainer derive from
 *
 * @sa
 *   - IFunction for a more thorough description of what a
 *   %decomposed function is.
 *   - AnalyticFunctionFactory and Factory for an easy
 *   generation of %decomposed functions from data functions
 */
template <typename Traits>
class Default
  : public Interface<Traits>
{
public:
  //! function tuple type
  typedef typename Traits :: FunctionTuple                           FunctionTuple;
  //! underlying grid part type
  typedef typename Traits :: GridPartType                            GridPartType;
  //! function space type
  typedef typename Traits :: FunctionSpaceType                       FunctionSpaceType;
  //! type of domain of operator
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! type of range of operator
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  typedef Lib::Function::ComponentEval< GridPartType,
                                    FunctionSpaceType >              FunctionComponentEvalType;
  //! type of an operator component \f$b^q\f$
  typedef LocalFunctionAdapter< FunctionComponentEvalType >          ComponentType;

  //! vector type for coefficients
  //! \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$
  typedef std :: vector< double >                                    CoefficientList;
  //! vector type for symbolic coefficients
  //! \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$
  typedef std :: vector< std :: string >                             SymbolicCoefficientList;
  //! number of components/coefficients
  static const int numComps = RB::Tuple::Size< FunctionTuple > :: value;
private:
  typedef std :: pair< FunctionComponentEvalType*, ComponentType* >  FunctionComponentPair;
  //! list type for operator components
  typedef std :: vector< FunctionComponentPair >                     FunctionComponentList;
private:

  //! helper class that creates a list of OperatorComponent objects out of the
  // given tuple of LocalFunction objects.
  class MakeComponentsFromFunctions
  {
  public:
    MakeComponentsFromFunctions(FunctionComponentList& fcList,
                                const GridPartType& gridPart)
      : fcList_(fcList), gridPart_(gridPart), counter_(0)
    {};

    template <class FuncType>
    void visit(FuncType & func)
    {
      std::ostringstream ostemp;
      ostemp << "fcomponent" << counter_;
      FunctionComponentEvalType * funcCompEval = new FunctionComponentEvalType(func);
      ComponentType * funcComp
        = new ComponentType(ostemp.str(), *funcCompEval, gridPart_);
      fcList_.push_back( std::make_pair(funcCompEval, funcComp) );
      counter_++;
    }
  private:
    FunctionComponentList &fcList_;
    const GridPartType    &gridPart_;
    int                    counter_;
  };

  class CollectCoefficients
  {
  public:
    CollectCoefficients(CoefficientList& coeffs)
      : coeffs_(coeffs)
    { }

    template<class FuncType>
    void visit(FuncType & func)
    {
      double scalar;
      scalar = func.coefficient();
      coeffs_.push_back(scalar);
    }
  private:
    CoefficientList& coeffs_;
  };

  class SymbolicCollectCoefficients
  {
  public:
    SymbolicCollectCoefficients(SymbolicCoefficientList& coeffs)
      : coeffs_(coeffs)
    { }

    template<class FuncType>
    void visit(FuncType & func)
    {
      std::string coeffstring;
      coeffstring = func.symbolicCoefficient();
      coeffs_.push_back(coeffstring);
    }
  private:
    SymbolicCoefficientList& coeffs_;
  };

public:
  //! constructor
  Default(FunctionTuple & funcTuple,
                            const GridPartType & gridPart)
    : funcTuple_(funcTuple),
      funcComponentList_(),
      gridPart_(gridPart)
  {
    RB::Tuple::ForEachVal<FunctionTuple> forEachFunc(funcTuple_);
    MakeComponentsFromFunctions visitor(funcComponentList_, gridPart_);
    forEachFunc.apply(visitor);
  }

  //! destructor
  virtual ~Default()
  {
    for( unsigned int i = 0 ; i < funcComponentList_.size() ; ++i )
    {
      delete funcComponentList_[i].first;
      delete funcComponentList_[i].second;
    }
  }


  inline int numComponents() const
  {
    return numComps;
  }

  virtual inline ComponentType & component(const int i) const
  {
//    std::cout << "i = " << i << "  size of list: " << funcComponentList_.size() << std::endl;
    assert( i < numComponents() );
    return *(funcComponentList_[i].second);
  }

  inline double coefficient(const int i) const
  {
    assert( i < numComponents() );
    if( i == 0 ) {
      return RB::Tuple::ElementAccess<0>::get(funcTuple_).coefficient();
    } else if( i == 1 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,1)>::get(funcTuple_).coefficient();
    } else if( i == 2 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,2)>::get(funcTuple_).coefficient();
    } else if( i < 5 ) {
      if (i == 3) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,3)>::get(funcTuple_).coefficient();
      }
      else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,4)>::get(funcTuple_).coefficient();
      }
    } else if( i < 7 ) {
      if (i == 5) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,5)>::get(funcTuple_).coefficient();
      } else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,6)>::get(funcTuple_).coefficient();
      }
    } else if( i < 9 ) {
      if (i == 7) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,7)>::get(funcTuple_).coefficient();
      } else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,8)>::get(funcTuple_).coefficient();
      }
    } else if( i == 9 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,9)>::get(funcTuple_).coefficient();
    }
    // fallback
    CoefficientList coeffs;
    coefficients(coeffs);
    return coeffs[i];
  }

  inline std::string symbolicCoefficient(const int i) const
  {
    assert( i < numComponents() );
    if( i == 0 ) {
      return RB::Tuple::ElementAccess<0>::get(funcTuple_).symbolicCoefficient();
    } else if( i == 1 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,1)>::get(funcTuple_).symbolicCoefficient();
    } else if( i == 2 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,2)>::get(funcTuple_).symbolicCoefficient();
    } else if( i < 5 ) {
      if (i == 3) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,3)>::get(funcTuple_).symbolicCoefficient();
      }
      else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,4)>::get(funcTuple_).symbolicCoefficient();
      }
    } else if( i < 7 ) {
      if (i == 5) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,5)>::get(funcTuple_).symbolicCoefficient();
      } else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,6)>::get(funcTuple_).symbolicCoefficient();
      }
    } else if( i < 9 ) {
      if (i == 7) {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,7)>::get(funcTuple_).symbolicCoefficient();
      } else {
        return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,8)>::get(funcTuple_).symbolicCoefficient();
      }
    } else if( i == 9 ) {
      return RB::Tuple::ElementAccess<__DDF_HH_MIN(numComps-1,9)>::get(funcTuple_).symbolicCoefficient();
    }
    // fallback
    SymbolicCoefficientList coeffs;
    symbolicCoefficients(coeffs);
    return coeffs[i];
  }

  void coefficients(CoefficientList& coeffs) const
  {
    RB::Tuple::ForEachVal<FunctionTuple>
      forEach(funcTuple_);
    CollectCoefficients visitor(coeffs);
    forEach.apply(visitor);
  }

  void symbolicCoefficients(SymbolicCoefficientList& coeffs) const
  {
    RB::Tuple::ForEachVal<FunctionTuple>
      forEach(funcTuple_);
    SymbolicCollectCoefficients visitor(coeffs);
    forEach.apply(visitor);
  }

  bool isMuDependent(const int i) const
  {
    assert( i < numComponents() );
    FunctionComponentEvalType & evalType = *(funcComponentList_[i].first);
    return evalType.isMuDependent();
  }

  bool isTimeDependent(const int i) const
  {
    assert( i < numComponents() );
    FunctionComponentEvalType & evalType = *(funcComponentList_[i].first);
    return evalType.isTimeDependent();
  }

  void setTime(const int i, const double time)
  {
    assert( i < numComponents() );
    FunctionComponentEvalType & evalType = *(funcComponentList_[i].first);
    evalType.setTime(time);
  }

private:
  FunctionTuple                &funcTuple_;
  FunctionComponentList         funcComponentList_;
  const GridPartType           &gridPart_;
}; // end of class DecomposedFunctionDefault

template<typename FunctionTuple, typename DiscreteFunctionBase>
class Discrete;

template<typename FunctionTuple, typename GridPartImp, typename FunctionSpaceImp>
class Container;

/** A traits class for the DiscreteDecomposedFunction class
 */
template<typename FunctionTupleImp, typename DiscreteFunctionBaseImp>
struct DiscreteTraits
{
  //! function tuple type
  typedef FunctionTupleImp                                           FunctionTuple;
  //! discrete function type
  typedef DiscreteFunctionBaseImp                                    DiscreteFunctionBase;
  //! grid part type
  typedef typename DiscreteFunctionBase :: DiscreteFunctionSpaceType
            :: GridPartType                                          GridPartType;
  //! function space type
  typedef typename DiscreteFunctionBase :: DiscreteFunctionSpaceType
           :: FunctionSpaceType                                      FunctionSpaceType;
  typedef Discrete< FunctionTuple,
                                      DiscreteFunctionBase >         BaseType;
};

/** A traits class for the DecomposedFunctionContainer class
 */
template <typename FunctionTupleImp,
          typename GridPartImp,
          typename FunctionSpaceImp>
struct ContainerTraits
{
  //! function tuple
  typedef FunctionTupleImp                                           FunctionTuple;
  //! grid part type
  typedef GridPartImp                                                GridPartType;
  //! function space type
  typedef FunctionSpaceImp                                           FunctionSpaceType;
  //! discrete function space type is set to Empty
  typedef Empty                                                      DiscreteFunctionBase;
  typedef Container< FunctionTuple, GridPartType,
                                       FunctionSpaceType >           BaseType;
};

/** @ingroup decomposed
 * @brief A %decomposed function for purely analytical functions that do not
 * carry any information about discretizations. So it is solely a \em container
 * of functions.
 *
 * An exception is the GridPart that is needed in order to initialize the
 * FunctionComponentEvalImp objects returned by the component method.
 *
 * @sa
 *  - DecomposedFunctionInterface for a thorough explanation about what a
 *  %decomposed function is and
 *  - @ref datafunc for a collection of possible analytical functions that can
 *  be put into such a FunctionTuple
 *
 * @tparam FunctionTuple     a tuple of functions of type DecomposedFunctionAddend
 * @tparam GridPartImp       a grid part type
 * @tparam FunctionSpaceImp  analytical function space for functions in tuple
 */
template<typename FunctionTuple, typename GridPartImp, typename FunctionSpaceImp>
class Container
  : public Default
             <ContainerTraits
                <FunctionTuple, GridPartImp, FunctionSpaceImp> >
{
public:
  //! base type
  typedef Default
            < ContainerTraits< FunctionTuple,
                                                 GridPartImp,
                                                 FunctionSpaceImp > > BaseType;
  //! grid part type
  typedef GridPartImp                                                GridPartType;
  //! function  space type
  typedef FunctionSpaceImp                                           FunctionSpaceType;
  typedef Lib::Function::ComponentEval< GridPartType,
                                    FunctionSpaceType >              FunctionComponentEvalType;
  //! component type
  typedef LocalFunctionAdapter< FunctionComponentEvalType >          ComponentType;

  using BaseType :: DecomposedVectorType;

  //! complete LocalFunction class
  typedef LocalMuScaledDefault< FunctionTuple >              CompleteLocalFunctionType;
  //! complete function type
  typedef LocalFunctionAdapter< CompleteLocalFunctionType >          CompleteType;

private:
  /*
   * @brief this *IS* a dirty hack!
   *
   * But the LocalFunctionAdapter says it is a discrete function, such that
   * the dune-fem L2Projection class does not work out of the box, because it
   * wants to use a global evaluate method.
   */
  class MyL2Projection
    : public DGL2ProjectionImpl
  {
  public:
    template<class LFAdapterType, class DiscreteFunctionBase>
    static void project(const LFAdapterType& f, DiscreteFunctionBase& discFunc, int polOrd = -1)
    {
      ProjectChooser<0, true > :: project(f,discFunc,polOrd);
    }
  };

public:
  //! constructor
  template<class DiscreteFunctionSpace>
  Container(FunctionTuple & tuple, DiscreteFunctionSpace & space)
    : BaseType(tuple, space.gridPart()),
      localFuncComplete_(tuple),
      funcComplete_(std::string("completeFunc"), localFuncComplete_, space.gridPart()) {}

  //! constructor
  Container(FunctionTuple & tuple, const GridPartImp & gridPart)
    : BaseType(tuple, gridPart),
      localFuncComplete_(tuple),
      funcComplete_(std::string("completeFunc"), localFuncComplete_, gridPart) {}

  const CompleteType & complete() const
  {
    localFuncComplete_.setMu();
    return funcComplete_;
  }

  //! projects the analytically given complete function onto a discrete
  //! function space
  //!
  //! \tparam DiscreteFunctionType type of discrete function
  //! \param[out] discFunc      discrete function to be returned
  //! \param[in]  polOrd        polynomial order for projection operator
  template<typename DiscreteFunctionType>
  void projectComplete(DiscreteFunctionType & discFunc, int polOrd = -1) const
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
    typedef typename DiscreteFunctionSpaceType :: DomainFieldType    DomainFieldType;
    typedef typename DiscreteFunctionSpaceType :: RangeFieldType     RangeFieldType;

    L2Projection<DomainFieldType, RangeFieldType, CompleteType, DiscreteFunctionType> l2pro;
    l2pro(complete(), discFunc);
  }

  //! projects the component function onto a discrete function space
  //!
  //! \tparam DiscreteFunctionType type of discrete function
  //! \param[in]  i             component number
  //! \param[out] discFunc      discrete function to be returned
  //! \param[in]  polOrd        polynomial order for projection operator
  template<typename DiscreteFunctionType>
  void projectComponent(const int i, DiscreteFunctionType & discFunc, int polOrd = -1) const
  {
    const ComponentType & funcComponent_ = this->component(i);
    MyL2Projection :: project(funcComponent_, discFunc, polOrd);
  }

private:
  mutable CompleteLocalFunctionType localFuncComplete_;
  CompleteType                      funcComplete_;
};

/** @ingroup decomposed
 * @brief creates an affinely %decomposed function from a given tuple of
 * Functions derived from the HasLocalFunction class (e.g.
 * AdaptiveDiscreteFunction or DiscreteFunctionAdapter)
 *
 * @sa
 *  - DecomposedFunctionInterface for a thorough explanation about what a
 *  %decomposed function is
 *
 * @tparam FunctionTuple  a tuple of discrete functions derived from
 *                        HasLocalFunction
 * @tparam DiscreteFunctionBase discrete function type for functions in tuple
 **/
template<typename FunctionTuple, typename DiscreteFunctionBase>
class Discrete
  : public Default
             <DiscreteTraits
                <FunctionTuple, DiscreteFunctionBase> >
{
public:
  //! base type
  typedef Default
            < DiscreteTraits< FunctionTuple,
                                                DiscreteFunctionBase > > DefaultBaseType;

  using DefaultBaseType :: DecomposedVectorType;

  //! discrete function base type
  typedef DiscreteFunctionBase                                       DiscreteFunctionBaseType;

  //! complete LocalFunction class see @ref LocalMuScaledFunctionDefault
  typedef LocalMuScaledDefault< FunctionTuple >              CompleteLocalFunctionType;
  //! complete type that is returned by completeMuScaled(), probably you want
  //! to use complete() or constantOperator() instead. See
  //! @ref DiscreteMuScaledFunction for a description of the return type
  typedef GlobalMuScaled< DiscreteFunctionBase,
                                    CompleteLocalFunctionType >      CompleteMuScaledFunctionType;
  //! complete Function class
  typedef DiscreteFunctionBase                                       CompleteType;
  //! underlying discrete function space of complete function
  typedef typename CompleteType :: DiscreteFunctionSpaceType         DiscreteFunctionSpaceType;
  typedef typename CompleteMuScaledFunctionType
            :: ConstantSpaceOperatorType                             ConstantSpaceOperatorType;
  typedef typename CompleteMuScaledFunctionType
            :: ConstantMappingType                                   ConstantMappingType;

public:
  //! constructor
  Discrete(FunctionTuple & funcTuple,
                             const DiscreteFunctionSpaceType & space)
    : DefaultBaseType(funcTuple, space.gridPart()),
      completeLocalFunc_(funcTuple),
      funcComplete_(completeLocalFunc_, space)
  {}

  /** @brief returns the complete @f$\mu@f$ scaled function @f$b(\mu)@f$
   *
   * The returned structure can be projected on a discrete function space, or
   * wrapped into a constant discrete operator that returns an
   * @f$L^2@f$-projection of @f$b(\mu)@f$. Furthermore, this structure gives
   * access to meta-information on the complete function. For more details, see
   * the documentation of the return type DiscreteMuScaledFunction.
   *
   * @note The methods complete() and constantOperator() call the
   * aforementioned methods for projection on a discrete function space
   * respectively generation of a constant discrete operator, directly from
   * this class.
   */
  inline CompleteMuScaledFunctionType & completeMuScaled()
  {
    return funcComplete_;
  }

  /** @brief returns a projection of @f$b(\mu)@f$ onto the underlying discrete
   * function space.
   */
  inline CompleteType & complete()
  {
    return funcComplete_.assemble();
  }

  /** @brief returns a constant discrete operator that evaluates to
   * @f$b(\mu)@f$ regardless of its input.
   *
   * Such an operator can be seen as constant contributions to a space
   * operator. In RBmatlab, the constant parts are considered separately, and
   * therefore this class makes sense.
   */
  inline ConstantSpaceOperatorType & constantOperator()
  {
    return funcComplete_.constantOperator();
  }

private:
  CompleteLocalFunctionType    completeLocalFunc_;
  CompleteMuScaledFunctionType funcComplete_;
};

} // end namespace Function
} // end namespace Decomposed
} // end namespace Dune::RB
} // end namespace Dune

#endif  /*__DISCRETEDECOMPOSEDFUNCTION_HH__*/
/* vim: set et sw=2: */
