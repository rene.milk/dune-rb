#ifndef  __DECOMPOSEDFUNCTION_HH__
#define  __DECOMPOSEDFUNCTION_HH__

#include  <dune/common/bartonnackmanifcheck.hh>
#include  "functioncomponent.hh"
#include  "localmuscaledfunction.hh"

namespace Dune
{
namespace RBFem
{
namespace Decomposed
{
namespace Function
{

/**
 * @brief an abstract base class for the decomposed functions interface.
 *
 * @see DecomposedFunctionInterface for more details on what a decomposed
 * function is.
 *
 * @tparam DiscreteFunctionBase  discrete function type for components
 *                               @f$b^q@f$ and the complete function @f$b@f$
 */
template< typename DiscreteFunctionBase >
class VirtualInterface
{
public:
  //! function space type
  typedef typename DiscreteFunctionBase::DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
  //! grid part type
  typedef typename DiscreteFunctionBase::DiscreteFunctionSpaceType::GridPartType GridPartType;
  //! type of domain of function
  typedef typename FunctionSpaceType::DomainType DomainType;
  //! type of range of function
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename RangeType::field_type RangeFieldType;
  typedef typename DiscreteFunctionBase::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  //! function component eval type
  typedef Lib::Function::ComponentEval< GridPartType, FunctionSpaceType > FunctionComponentEvalType;
  //! function component type
  typedef LocalFunctionAdapter< FunctionComponentEvalType > ComponentType;
  //! scalar type
  typedef RangeFieldType ScalarType;
  //! coefficient list type
  typedef std::vector< ScalarType > CoefficientList;
  //! symbolic coefficient list type
  typedef std::vector< std::string > SymbolicCoefficientList;
  //! type of the complete discrete function
  typedef DiscreteFunctionBase CompleteType;

public:
  //! deconstructor
  virtual ~VirtualInterface()
  {
  }
  ;

  /** @brief returns the number of components
   */
  virtual int numComponents() const = 0;

  /** @brief returns the full function \f$b(\cdot,t;\mu)\f$.
   *
   * @note The parameter @f$\mu@f$ is set accordingly to the values stored in
   * the Parameter singleton given. In order to change the time variable
   * @f$t@f$, the user needs to call the setTime() method for each component,
   * before executing this method.
   *
   * \return    returned full function (linear combination of components,
   *            scaled with coefficients) */
  virtual CompleteType & complete() = 0;

  /** @brief returns the ith component @f$b^i@f$
   *
   * \param[in] i      index of the component to be returned
   * \return           returned function component */
  virtual const ComponentType & component(const int i) const = 0;

  /** @brief returns the ith coefficient @f$\sigma^i(\mu)@f$
   *
   * \param[in] i      index of the coefficient to be returned
   * \return           returned coefficient */
  virtual double coefficient(const int i) const = 0;

  /** @brief returns symbolic string for computation of ith coefficient
   *
   * \param[in] i      index of the coefficient
   * \return           symbolic math string that can be evaluated by
   *                   MATLAB&copy; */
  virtual std::string symbolicCoefficient(const int i) const = 0;

  /** @brief returns a vector of coefficents
   *  \f$\left(\sigma^{1}(\mu), \ldots, \sigma^{Q}(\mu)\right)\f$.
   *
   *  \param[out] coeffs  a vector of scalar entries that should a priori be
   *                      empty. */
  virtual inline void coefficients(CoefficientList& coeffs) const = 0;

  /** @brief a vector of all symbolic coefficient strings
   *
   *  @see symbolicCoefficient()
   *
   *  \param[out]  coeffs  a vector of symbolic coefficient strings */
  virtual void symbolicCoefficients(SymbolicCoefficientList& coeffs) const = 0;

  /** @brief returns true if the \a i -th component is parameter dependent, i.e.
   * @f$\sigma^i \not \equiv \mbox{const} @f$
   *
   *  \param[in] i index of component
   *  \return    boolean for mu dependency of \a i -th component */
  virtual inline bool isMuDependent(const int i) const = 0;

  /** @brief returns wether the \a i -th component is time dependent
   *
   *  \param[in] i index of component
   *  \return    boolean indicating time dependency of \a i -th component */
  virtual inline bool isTimeDependent(const int i) const = 0;

  /** @brief sets the time variable of the \a i -th component
   *
   * @note this also changes the components in the linear combined complete
   * function.
   *
   *  \param[in] i     index of component
   *  \param[in] time  new time */
  virtual inline void setTime(const int i, double time) = 0;
};

// non virtual empty specialization, if no complete function is needed.
template< >
class VirtualInterface< Empty >
{
};

// forward declarations
template< class Traits >
class Interface;

template< class Afunc, class Bfunc >
class Combined;

//! traits class for CombinedDecomposedFunction
template< class Afunc, class Bfunc >
struct CombinedTraits
{
  typedef typename Afunc::GridPartType GridPartType;
  typedef typename Afunc::FunctionSpaceType FunctionSpaceType;
  typedef typename Afunc::DiscreteFunctionBase DiscreteFunctionBase;
  typedef Combined< Afunc, Bfunc > BaseType;
};

/**
 * @brief Class for combination of two decomposed functions.
 *
 * Objects of this class are returned by the operator+() method of the
 * DecomposedFunctionDefaultImp
 *
 * @tparam Afunc   first addend (an implementation of DecomposedVector)
 * @tparam Bfunc   second addend (an implementation of DecomposedVector)
 */
template< class Afunc, class Bfunc >
class Combined: public Interface<
    CombinedTraits< Afunc, Bfunc > >
{
private:
  typedef typename Afunc::DiscreteFunctionBaseType DiscreteFunctionBaseType;
  typedef typename Afunc::DomainType DomainType;
  typedef typename DomainType::field_type DomainFieldType;
  typedef typename Afunc::RangeType RangeType;
  typedef typename RangeType::field_type RangeFieldType;

  /**
   * @brief contains the type of the complete function after adding FullFunc1 and
   * FullFunc2
   *
   * @tparam FullFunc1  first addend
   * @tparam FullFunc2  second addend
   */
  template< class FullFunc1, class FullFunc2 >
  struct FullFuncAddType
  {
    //! combined type
    typedef Decomposed::Function::IGlobalMuScaledAssembler< DiscreteFunctionBaseType > type;
  };

  /**
   * @brief contains the type of the complete operator after adding FullOp1 and
   * FullOp2
   *
   * @tparam LocalFunc1  local function of first addend
   * @tparam LocalFunc2  local function of second addend
   */
  template< class LocalFunc1, class LocalFunc2 >
  struct FullFuncAddType<
      Decomposed::Function::GlobalMuScaled< DiscreteFunctionBaseType, LocalFunc1 > ,
      Decomposed::Function::GlobalMuScaled< DiscreteFunctionBaseType, LocalFunc2 > >
  {
    //! combined type
    typedef Decomposed::Function::GlobalMuScaled< DiscreteFunctionBaseType,
        Decomposed::Function::CombinedLocalMuScaled< LocalFunc1, LocalFunc2 > > type;
  };

public:
  typedef typename Afunc::ComponentType ComponentType;
  typedef typename Afunc::CoefficientList CoefficientList;
  typedef typename Afunc::SymbolicCoefficientList SymbolicCoefficientList;
  typedef typename FullFuncAddType<
      typename Afunc::CompleteMuScaledFunctionType,
      typename Bfunc::CompleteMuScaledFunctionType >::type CompleteMuScaledFunctionType;
  typedef typename Afunc::CompleteType CompleteType;
  typedef typename CompleteMuScaledFunctionType::ConstantSpaceOperatorType ConstantSpaceOperatorType;
  typedef typename CompleteMuScaledFunctionType::ConstantMappingType ConstantMappingType;
public:

  //! constructor
  //! @param a   the first DecomposedVector implementation
  //! @param b   the second DecomposedVector implementation
  Combined(Afunc & a, Bfunc & b) :
      a_(a), b_(b),
      compMuScaledSum_(a_.completeMuScaled() + b_.completeMuScaled())
  {
  }

  virtual ~Combined()
  {
  }

  //! \copydoc DecomposedVector :: numComponents()
  virtual int numComponents() const
  {
    return a_.numComponents() + b_.numComponents();
  }

  //! returns the new complete function
  inline CompleteMuScaledFunctionType & completeMuScaled()
  {
    return compMuScaledSum_;
  }

  //! returns an operator that always outputs the underlying functions,
  //! regardless of the input.
  inline ConstantSpaceOperatorType & constantOperator()
  {
    return completeMuScaled().constantOperator();
  }

  //! \copydoc DecomposedVector :: complete()
  virtual CompleteType & complete()
  {
    return completeMuScaled().assemble();
  }

  //! \copydoc DecomposedVector :: component()
  virtual const ComponentType & component(const int i) const
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      return a_.component(i);
    }
    else
    {
      return b_.component(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedVector :: coefficient()
  virtual double coefficient(const int i) const
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      return a_.coefficient(i);
    }
    else
    {
      return b_.coefficient(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedVector :: symbolicCoefficient()
  virtual std::string symbolicCoefficient(const int i) const
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      return a_.symbolicCoefficient(i);
    }
    else
    {
      return b_.symbolicCoefficient(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedVector :: coefficients()
  virtual void coefficients(CoefficientList& coeffs) const
  {
    a_.coefficients(coeffs);
    b_.coefficients(coeffs);
  }

  //! \copydoc DecomposedVector :: symbolicCoefficients()
  virtual void symbolicCoefficients(SymbolicCoefficientList& coeffs) const
  {
    a_.symbolicCoefficients(coeffs);
    b_.symbolicCoefficients(coeffs);
  }

  //! \copydoc DecomposedVector :: isMuDependent()
  inline bool isMuDependent(const int i) const
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      return a_.isMuDependent(i);
    }
    else
    {
      return b_.isMuDependent(i - a_.numComponents());
    }
  }

  //! \copydoc DecomposedVector :: isTimeDependent()
  inline bool isTimeDependent(const int i) const
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      return a_.isTimeDependent(i);
    }
    else
    {
      return b_.isTimeDependent(i - a_.numComponents());
    }
  }

  //! @copydoc DecomposedVector :: setTime()
  inline void setTime(const int i, double time)
  {
    assert(i < numComponents());
    if (i < a_.numComponents())
    {
      a_.setTime(i, time);
    }
    else
    {
      b_.setTime(i - a_.numComponents(), time);
    }
  }

private:
  Afunc &a_;
  Bfunc &b_;
  CompleteMuScaledFunctionType &compMuScaledSum_;
};

/** @ingroup decomposed
 * @brief Interface for decomposed functions.
 *
 * A decomposed function implements an affine combination of factors of
 * parameter dependent coefficients and linear and parameter-independent
 * functions, i.e.
 *
 * @f[ b(\mu; x) = \sum_{q=1}^{\mbox{numComps}} b^q(x) \cdot \sigma^q(\mu) @f]
 *
 * We use the following terminology:
 *  - @f$ b @f$ is the @em complete function
 *  - @f$ b^q @f$ are the @em components of the decomposed function
 *  - @f$ \sigma^q @f$ return the @em coefficients of the decomposed function
 *
 * This interface already adds the operator+ method to a decomposed function.
 * The CombinedDecomposedFunction that we get after adding two decomposed
 * functions implements the new complete function, such that it can be
 * evaluated efficiently. Here, "efficiently" means that only one grid
 * iteration is needed in order to assemble a DoF vector for this function,
 * because the local functions of the two addends are combined as well.
 *
 * \note The operator+ only works on DecomposedFunctionInterface implemantions
 * with the same \c BaseType
 */
template< typename Traits >
class Interface: public VirtualInterface<
    typename Traits::DiscreteFunctionBase >, public ObjPointerStorage
{
public:
  //! type of decomposed function implemenation
  typedef typename Traits::BaseType BaseType;
  //! analytical function space for decomposed function
  typedef typename Traits::FunctionSpaceType FunctionSpaceType;
  //! discrete function type
  typedef typename Traits::DiscreteFunctionBase DiscreteFunctionBase;
  //! grid part type
  typedef typename Traits::GridPartType GridPartType;

  //! virtual base type to which the result of a sum of decomposed function can
  //! be casted to.
  typedef VirtualInterface< typename Traits::DiscreteFunctionBase > DecomposedVectorType;
public:
  /**
   * @brief efficiently adds two decomposed functions with same base type
   *
   * extends the list of function components and coefficients and updates the
   * complete function, such that it can still be evaluated efficiently. In
   * order to avoid using the new complex CombinedDecomposedFunction type, the
   * result is usually casted to a DecomposedVector type in the end.
   *
   * @code
   * typedef typename DecomposedFunctionExample :: VectorType
   *   DecomposedVectorType;
   *
   * DecomposedVectorType sum = decFunc1 + decFunc2 + decFunc3;
   * @endcode
   *
   * @tparam     DecomposedFunctionAddend  type of addend
   * @param[in]  addend                    the addend
   * @return sum of decomposed functions
   **/
  template< class DecomposedFunctionAddend >
  Combined< BaseType, DecomposedFunctionAddend > &
  operator +(DecomposedFunctionAddend & addend)
  {
    typedef Combined< BaseType, DecomposedFunctionAddend > FuncType;

    FuncType * sum = new FuncType(asImp(), addend);

    this->saveObjPointer(sum);

    return (*sum);
  }

  template< class DecomposedFunctionAddend >
  Combined< BaseType, DecomposedFunctionAddend > &
  add(DecomposedFunctionAddend &addend)
  {
    typedef Combined< BaseType, DecomposedFunctionAddend > FuncType;

    FuncType * sum = new FuncType(asImp(), addend);

    this->saveObjPointer(sum);

    return (*sum);
  }

private:
  inline const BaseType& asImp() const
  {
    return static_cast< const BaseType& >(*this);
  }

  inline BaseType& asImp()
  {
    return static_cast< BaseType& >(*this);
  }
};

} // end namespace Function
} // end namespace Decomposed
} // end namespace Dune::RB
} // end namespace Dune

#endif  /*__DECOMPOSEDFUNCTION_HH__*/
/* vim: set et sw=2: */
