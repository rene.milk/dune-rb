/**
  * \file   cornerpoint.cc
  * \brief  dune/rb/grid/provider/cornerpoint.cc - Contains a class to construct a cornerpoint grid from file.
  **/

// local
#include "cornerpoint.hh"

const std::string Cornerpoint::id = "rb.grid.provider.cornerpoint";

Cornerpoint::Cornerpoint(Dune::ParameterTree& paramTree)
  : grid_()
{
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
  if (paramsP->hasSub(id))
    paramsP = &(paramsP->sub(id));
  Dune::ParameterTree& params = *paramsP;
  // build grid
  const std::string key = "filename";
  assert(params.hasKey(key));
  buildGrid(params.get(key, ""));
  /*buildMDGrid();*/
} // Cornerpoint::Cornerpoint(const Dune::ParameterTree& paramTree)

Cornerpoint::Cornerpoint(std::string filename)
  : grid_()
{
  buildGrid(filename);
  /*buildMDGrid();*/
} // Cornerpoint::Cornerpoint(std::string filename)

Dune::CpGrid& Cornerpoint::grid()
{
  return grid_;
}

void Cornerpoint::visualize(Dune::ParameterTree& paramTree)
{
  const std::string localId = "visualize";
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
  if (paramsP->hasSub(id))
    paramsP = &(paramsP->sub(id));
  if (paramsP->hasSub(localId))
    paramsP = &(paramsP->sub(localId));
  Dune::ParameterTree& params = *paramsP;
  // check for grid visualization
  if (params.hasKey("grid")) {
    const std::string filenameGrid = params.get("grid", id + ".grid");
    // grid view
    typedef Dune::CpGrid GridType;
    GridType& grid = this->grid();
    typedef GridType::LeafGridView GridView;
    GridView gridView = grid.leafView();
    // mapper
    Dune::LeafMultipleCodimMultipleGeomTypeMapper< GridType, P0Layout > mapper(grid);
    std::vector< double > data(mapper.size());
    // walk the grid
    typedef GridView::Codim<0>::Iterator ElementIterator;
    typedef GridView::Codim<0>::Entity ElementType;
    typedef ElementType::LeafIntersectionIterator FacetIteratorType;
    for (ElementIterator it = gridView.begin<0>(); it != gridView.end<0>(); ++it)
    {
      ElementType& element = *it;
      data[mapper.map(element)] = 0.0;
      int numberOfBoundarySegments = 0;
      bool isOnBoundary = false;
      for (FacetIteratorType facet = element.ileafbegin();
           facet != element.ileafend();
           ++facet) {
        if (!facet->neighbor() && facet->boundary()){
          isOnBoundary = true;
          numberOfBoundarySegments += 1;
          data[mapper.map(element)] += double(facet->boundaryId());
        }
      }
      if (isOnBoundary) {
        data[mapper.map(element)] /= double(numberOfBoundarySegments);
      }
    }
    // write to vtk
    Dune::VTKWriter< GridView > vtkwriter(gridView);
    vtkwriter.addCellData(data, "boundaryId");
    vtkwriter.write(filenameGrid, Dune::VTK::ascii);
  } // check for grid visualization
} // void Cornerpoint::visualize(Dune::ParameterTree& paramTree)

void Cornerpoint::buildGrid(std::string filename)
{
  grid_.readEclipseFormat(filename, 0.0, false, false);
  return;
} // void Cornerpoint::buildGrid(std::string filename)

/*void Cornerpoint::buildMDGrid()
{
  typedef Dune::CpGrid GridType;
  typedef Dune::MultiDomainGrid< GridType, Dune::mdgrid::FewSubDomainsTraits< dim, 2 > > MDGridType;
  typedef typename MDGridType::ctype ctype;
  mdGrid_ = Dune::shared_ptr< MDGridType >(new MDGridType(grid()));
  // partition into two subdomains
  typedef typename MDGridType::LeafGridView MDGridViewType;
  MDGridViewType mdGridView = mdGrid_->leafView();
  typedef typename MDGridViewType::Codim<0>::Entity MDElementType;
  typedef typename MDGridViewType::Codim<0>::Iterator MDElementIteratorType;
  mdGrid_->startSubDomainMarking();
  for (MDElementIteratorType mdElementIterator = mdGridView.begin<0>();
       mdElementIterator != mdGridView.end<0>();
       ++mdElementIterator) {
    const MDElementType& mdElement = *mdElementIterator;
    Dune::FieldVector< ctype, dim > coordinate
      = mdElement.geometry().global(Dune::GenericReferenceElements< ctype, dim >::general(mdElement.type()).position(0, 0));
    double y = coordinate[1];
    if (y < 6.71e06)
      mdGrid_->addToSubDomain(0, mdElement);
    else
      mdGrid_->addToSubDomain(1, mdElement);
  }
  mdGrid_->preUpdateSubDomains();
  mdGrid_->updateSubDomains();
  mdGrid_->postUpdateSubDomains();
} // void Cornerpoint::buildMDGrid()*/
