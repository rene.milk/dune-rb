#ifndef DUNE_RB_GRID_PROVIDER_P0LAYOUT_HH
#define DUNE_RB_GRID_PROVIDER_P0LAYOUT_HH

namespace Dune
{

namespace RB
{

namespace Grid
{

namespace Provider
{

// layout class for codim 0 mapper
template< int dim >
struct P0Layout
{
  bool contains(Dune::GeometryType gt)
  {
    if (gt.dim() == dim)
      return true;
    return false;
  }
}; // layout class for codim 0 mapper

} // namespace Provider

} // namespace Grid

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_GRID_PROVIDER_P0LAYOUT_HH
