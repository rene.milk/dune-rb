#ifndef DUNE_RB_GRID_PROVIDER_INTERFACE_HH
#define DUNE_RB_GRID_PROVIDER_INTERFACE_HH

// dune-common
#include <dune/common/parametertree.hh>

namespace Dune
{

namespace RB
{

namespace Grid
{

namespace Provider
{

class Interface
{
public:
  typedef GridType GridType;

  static const int dim;

  static const std::string id;

  typedef MDGridType MDGridType;

  Interface(Dune::ParameterTree& parameterTree);

  GridType& grid();

  MDGridType& mdGrid();

  void visualize(Dune::ParameterTree& parameterTree);

}; // class Interface

} // namespace Provider

} // namespace Grid

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_GRID_PROVIDER_INTERFACE_HH
