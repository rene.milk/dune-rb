/**
  * \file   cube.hh
  * \brief  dune/rb/grid/provider/cube.hh - Contains class to construct various cubes.
  **/

template< typename GridImp, int variant >
const std::string Cube< GridImp, variant >::id = "rb.grid.provider.cube";

template< typename GridImp, int variant >
Cube< GridImp, variant >::Cube(const Dune::ParameterTree& paramTree)
{
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
  if (paramsP->hasSub(id))
    paramsP = &(paramsP->sub(id));
  Dune::ParameterTree& params = *paramsP;
  const double lowerLeft = params.get("lowerLeft", 0.0);
  const double upperRight = params.get("upperRight", 1.0);
  buildGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  buildMDGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  buildMsGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
} // Cube< GridImp, variant >::Cube(const Dune::ParameterTree& parameterTree)

template< typename GridImp, int variant >
Cube< GridImp, variant >::Cube(const CoordinateType& lowerLeft, const CoordinateType& upperRight)
{
  buildGrid(lowerLeft, upperRight);
  buildMDGrid(lowerLeft, upperRight);
  buildMsGrid(lowerLeft, upperRight);
}

template< typename GridImp, int variant >
Cube< GridImp, variant >::Cube(double lowerLeft, double upperRight)
{
  buildGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  buildMDGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  buildMsGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
}

template< typename GridImp, int variant >
GridImp& Cube< GridImp, variant >::grid()
{
  return *grid_;
}

template< typename GridImp, int variant >
//Dune::RB::Grid::Multiscale::Default< GridImp >& Cube< GridImp, variant >::msGrid()
Dune::RB::Grid::Multiscale::Multidomain< GridImp >& Cube< GridImp, variant >::msGrid()
{
  return *msGrid_;
}

#ifdef HAVE_DUNE_MULTIDOMAINGRID
template< typename GridImp, int variant >
Dune::MultiDomainGrid< GridImp, Dune::mdgrid::FewSubDomainsTraits< GridImp::dimension, 8 > >& Cube< GridImp, variant >::mdGrid()
{
  return *mdGrid_;
}
#endif // HAVE_DUNE_MULTIDOMAINGRID

template< typename GridImp, int variant >
void Cube< GridImp, variant >::visualize(Dune::ParameterTree& paramTree)
{
  const std::string localId = "visualize";
  // get correct parameter tree
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
  if (paramsP->hasSub(id))
    paramsP = &(paramsP->sub(id));
  if (paramsP->hasSub(localId))
    paramsP = &(paramsP->sub(localId));
  Dune::ParameterTree& params = *paramsP;
  // check for grid visualization
  if (params.hasKey("grid")) {
    const std::string filenameGrid = params.get("grid", id + ".grid");
    // grid view
    typedef GridImp GridType;
    GridType& grid = this->grid();
    typedef typename GridType::LeafGridView GridView;
    GridView gridView = grid.leafView();
    // mapper
    Dune::LeafMultipleCodimMultipleGeomTypeMapper< GridType, P0Layout > mapper(grid);
    std::vector< double > data(mapper.size());
    // walk the grid
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity ElementType;
    typedef typename ElementType::LeafIntersectionIterator FacetIteratorType;
    for (ElementIterator it = gridView.template begin<0>(); it != gridView.template end<0>(); ++it)
    {
      ElementType& element = *it;
      data[mapper.map(element)] = 0.0;
      int numberOfBoundarySegments = 0;
      bool isOnBoundary = false;
      for (FacetIteratorType facet = element.ileafbegin();
           facet != element.ileafend();
           ++facet) {
        if (!facet->neighbor() && facet->boundary()){
          isOnBoundary = true;
          numberOfBoundarySegments += 1;
          data[mapper.map(element)] += double(facet->boundaryId());
        }
      }
      if (isOnBoundary) {
        data[mapper.map(element)] /= double(numberOfBoundarySegments);
      }
    } // walk the grid
    // write to vtk
    Dune::VTKWriter< GridView > vtkwriter(gridView);
    vtkwriter.addCellData(data, "boundaryId");
    vtkwriter.write(filenameGrid, Dune::VTK::ascii);
  } // check for grid visualization
#ifdef HAVE_DUNE_MULTIDOMAINGRID
  // check for multidomain grid visualization
  if (params.hasKey("mdGrid")) {
    const std::string filenameGrid = params.get("mdGrid", id + ".mdGrid");
    // host grid
    typedef GridImp HostGridType;
    const HostGridType& hostGrid = this->grid();
    typedef typename HostGridType::LeafGridView HostGridViewType;
    const HostGridViewType& hostGridView = hostGrid.leafView();
    typedef typename HostGridType::template Codim<0>::Entity HostElementType;
    // mapper
    Dune::LeafMultipleCodimMultipleGeomTypeMapper< HostGridType, P0Layout > hostMapper(hostGrid);
    std::vector< double > data(hostMapper.size());
    // multidomain grid
    typedef Dune::MultiDomainGrid< HostGridType, Dune::mdgrid::FewSubDomainsTraits< HostGridType::dimension, 8 > > MDGridType;
    MDGridType& mdGrid = this->mdGrid();
    // loop over all subdomains
    for (typename MDGridType::SubDomainIndexType subdomainIndex = 0;
         subdomainIndex <= mdGrid.maxSubDomainIndex;
         ++subdomainIndex) {
      // subdomain grid
      typedef typename MDGridType::SubDomainGrid SDGridType;
      const SDGridType& sdGrid = mdGrid.subDomain(subdomainIndex);
      // subdomain grid view
      typedef typename SDGridType::LeafGridView SDGridViewType;
      const SDGridViewType& sdGridView = sdGrid.leafView();
      // walk the subdomain grid
      typedef typename SDGridViewType::template Codim<0>::Iterator SDElementIteratorType;
      typedef typename SDGridViewType::template Codim<0>::Entity SDElementType;
      for (SDElementIteratorType sdElementIterator = sdGridView.template begin<0>();
           sdElementIterator != sdGridView.template end<0>();
           ++sdElementIterator) {
        const SDElementType& sdElement = *sdElementIterator;
        const HostElementType& hostElement = sdGrid.hostEntity(sdElement);
        data[hostMapper.map(hostElement)] = double(subdomainIndex);
      } // walk the subdomain grid
    } // loop over all subdomains
    // write to vtk
    Dune::VTKWriter< HostGridViewType > vtkwriter(hostGridView);
    vtkwriter.addCellData(data, "subdomain");
    vtkwriter.write(filenameGrid, Dune::VTK::ascii);
  } // check for multidomain grid visualization
#endif // HAVE_DUNE_MULTIDOMAINGRID
} // void Cube< GridImp, variant >::visualize(const Dune::ParameterTree& paramTree)

template< typename GridImp, int variant >
void Cube< GridImp, variant >::buildGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)
{
  Dune::array< unsigned int, dim > numElements;
  std::fill(numElements.begin(), numElements.end(), 1);
  switch (variant) {
    case 1:
      grid_ = Dune::StructuredGridFactory< GridType >::createCubeGrid(lowerLeft, upperRight, numElements);
      break;
    case 2:
      grid_ = Dune::StructuredGridFactory< GridType >::createSimplexGrid(lowerLeft, upperRight, numElements);
      break;
    default:
      DUNE_THROW(Dune::NotImplemented, "Variant " << variant << " of cube not implemented.");
    grid_->globalRefine(1);
  }
  return;
} // void Cube< GridImp, variant >::buildCube(const CoordinateType& lowerLeft, const CoordinateType& upperRight)

template< typename GridImp, int variant >
void Cube< GridImp, variant >::buildMDGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)
{
  typedef Dune::MultiDomainGrid< GridImp, Dune::mdgrid::FewSubDomainsTraits< dim, 8 > > MDGridType;
  typedef typename MDGridType::ctype ctype;
  mdGrid_ = Dune::shared_ptr< MDGridType >(new MDGridType(grid()));
  // partition into two subdomains per dimension
  typedef typename MDGridType::LeafGridView MDGridViewType;
  MDGridViewType mdGridView = mdGrid_->leafView();
  typedef typename MDGridViewType::template Codim<0>::Entity MDElementType;
  typedef typename MDGridViewType::template Codim<0>::Iterator MDElementIteratorType;
  mdGrid_->startSubDomainMarking();
  CoordinateType halfLength(0.0);
  if (dim >= 1)
    halfLength[0] = std::abs(upperRight[0] - lowerLeft[0])/2.0;
  if (dim >= 2)
    halfLength[1] = std::abs(upperRight[1] - lowerLeft[1])/2.0;
  if (dim >= 3)
    halfLength[2] = std::abs(upperRight[2] - lowerLeft[2])/2.0;
  for (MDElementIteratorType mdElementIterator = mdGridView.template begin<0>();
       mdElementIterator != mdGridView.template end<0>();
       ++mdElementIterator) {
    const MDElementType& mdElement = *mdElementIterator;
    Dune::FieldVector< ctype, dim > coordinate
      = mdElement.geometry().global(Dune::GenericReferenceElements< ctype, dim >::general(mdElement.type()).position(0, 0));
    if (dim == 1) {
      double x = coordinate[0];
      if (lowerLeft[0] <= x && x < halfLength[0])
        mdGrid_->addToSubDomain(0, mdElement);
      else if (halfLength[1] <= x && x <= upperRight[0])
        mdGrid_->addToSubDomain(1, mdElement);
    } else if (dim == 2) {
      double x = coordinate[0];
      double y = coordinate[1];
      if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1])
        mdGrid_->addToSubDomain(0, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1])
        mdGrid_->addToSubDomain(1, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1])
        mdGrid_->addToSubDomain(2, mdElement);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1])
        mdGrid_->addToSubDomain(3, mdElement);
      else
        DUNE_THROW(Dune::RangeError, "We should not get here.");
    } else if (MDGridType::dimension == 3) {
      double x = coordinate[0];
      double y = coordinate[1];
      double z = coordinate[2];
      if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
        mdGrid_->addToSubDomain(0, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
        mdGrid_->addToSubDomain(1, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
        mdGrid_->addToSubDomain(2, mdElement);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
        mdGrid_->addToSubDomain(3, mdElement);
      else if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
        mdGrid_->addToSubDomain(4, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
        mdGrid_->addToSubDomain(5, mdElement);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
        mdGrid_->addToSubDomain(6, mdElement);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
        mdGrid_->addToSubDomain(7, mdElement);
      else
        DUNE_THROW(Dune::RangeError, "We should not get here.");
    }
  }
  mdGrid_->preUpdateSubDomains();
  mdGrid_->updateSubDomains();
  mdGrid_->postUpdateSubDomains();
} // void Cube< GridImp, variant >::buildMDGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)

template< typename GridImp, int variant >
void Cube< GridImp, variant >::buildMsGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)
{
  // create grid
  typedef typename MsGridType::GridType HostGridType;
  msGrid_ = Dune::shared_ptr< MsGridType >(new MsGridType(grid()));
  // create leaf view
  typedef typename HostGridType::LeafGridView HostGridViewType;
  HostGridViewType hostGridView = msGrid_->grid().leafView();
  // some preparations
  typedef typename HostGridType::ctype ctype;
  typedef typename HostGridViewType::template Codim<0>::Entity EntityType;
  typedef typename HostGridViewType::template Codim<0>::Iterator IteratorType;
  CoordinateType halfLength(0.0);
  if (dim >= 1)
    halfLength[0] = std::abs(upperRight[0] - lowerLeft[0])/2.0;
  if (dim >= 2)
    halfLength[1] = std::abs(upperRight[1] - lowerLeft[1])/2.0;
  if (dim >= 3)
    halfLength[2] = std::abs(upperRight[2] - lowerLeft[2])/2.0;
  msGrid_->prepare();
  // walk the grid
  for (IteratorType iterator = hostGridView.template begin<0>();
       iterator != hostGridView.template end<0>();
       ++iterator) {
    const EntityType& entity = *iterator;
    Dune::FieldVector< ctype, dim > coordinate
      = entity.geometry().global(Dune::GenericReferenceElements< ctype, dim >::general(entity.type()).position(0, 0));
    if (dim == 1) {
      double x = coordinate[0];
      if (lowerLeft[0] <= x && x < halfLength[0])
        msGrid_->add(entity, 0);
      else if (halfLength[1] <= x && x <= upperRight[0])
        msGrid_->add(entity, 1);
    } else if (dim == 2) {
      double x = coordinate[0];
      double y = coordinate[1];
      if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1])
        msGrid_->add(entity, 0);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1])
        msGrid_->add(entity, 1);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1])
        msGrid_->add(entity, 2);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1])
        msGrid_->add(entity, 3);
      else
        DUNE_THROW(Dune::RangeError, "We should not get here.");
    } else if (MDGridType::dimension == 3) {
      double x = coordinate[0];
      double y = coordinate[1];
      double z = coordinate[2];
      if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
        msGrid_->add(entity, 0);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
        msGrid_->add(entity, 1);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
        msGrid_->add(entity, 2);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
        msGrid_->add(entity, 3);
      else if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
        msGrid_->add(entity, 4);
      else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
        msGrid_->add(entity, 5);
      else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
        msGrid_->add(entity, 6);
      else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
        msGrid_->add(entity, 7);
      else
        DUNE_THROW(Dune::RangeError, "We should not get here.");
    }
  } // walk the grid
  // finalize
  msGrid_->finalize();
} // void Cube< GridImp, variant >::buildMsGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)

template< typename GridImp >
UnitCube< GridImp >::UnitCube(Dune::ParameterTree&)
  : Cube< GridImp, 2 >(0.0, 1.0)
{}

template< int dim >
UnitCube< Dune::YaspGrid< dim > >::UnitCube(Dune::ParameterTree&)
  : Cube< Dune::YaspGrid< dim >, 1 >(0.0, 1.0)
{}

template< int dim >
UnitCube< Dune::SGrid< dim, dim > >::UnitCube(Dune::ParameterTree&)
  : Cube< Dune::SGrid< dim, dim >, 1 >(0.0, 1.0)
{}

template< int dim >
UnitCube< Dune::ALUCubeGrid< dim, dim > >::UnitCube(Dune::ParameterTree&)
  : Cube< Dune::ALUCubeGrid< dim, dim >, 1 >(0.0, 1.0)
{}
