/**
  \file   cube.hh
  \brief  dune/rb/grid/provider/cube.hh - Contains classes to construct various cubes, i.e. GenericCube, Cube and
          UnitCube.
  **/

#ifndef DUNE_RB_GRID_PROVIDER_CUBE_HH
#define DUNE_RB_GRID_PROVIDER_CUBE_HH

// dune-common
#include <dune/common/parametertree.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

// dune-grid
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/alugrid.hh>
#include <dune/grid/sgrid.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-rb
#include <dune/rb/grid/multiscale/default.hh>
#include <dune/rb/grid/multiscale/multidomain.hh>

// local
#include "p0layout.hh"

namespace Dune
{

namespace RB
{

namespace Grid
{

namespace Provider
{

/**
  \brief  Creates a grid of a cube in various dimensions.

          Default implementation using the Dune::StructuredGridFactory to create a grid of a cube in 1, 2 or 3
          dimensions. Tested with
          <ul><li> \c YASPGRID, \c variant 1, dim = 1, 2, 3,
          <li> \c SGRID, \c variant 1, dim = 1, 2, 3,
          <li> \c ALUGRID_SIMPLEX, \c variant 2, dim = 2, 3,
          <li> \c ALUGRID_CONFORM, \c variant 2, dim = 2, 2 and
          <li> \c ALUGRID_CUBE, \c variant 1, dim = 2, 3.</ul>
  \tparam GridImp
          Type of the underlying grid.
  \tparam variant
          Type of the codim 0 elements:
          <ul><li>\c 1: cubes
          <li>2: simplices</ul>
  **/
template< typename GridImp, int variant, class MsGridImp /*= Dune::RB::Grid::Multiscale::Default< GridImp >*/ >
class GenericCube
{
public:
  //! Type of the provided grid.
  typedef GridImp GridType;

  //! Type of the provided multiscale grid (derived from Dune::RB::Grid::Multiscale::Interface).
  typedef MsGridImp MsGridType;

  //! Dimension of the provided grid.
  static const int dim = GridType::dimension;

  //! Type of the grids coordinates.
  typedef Dune::FieldVector< typename GridType::ctype, dim > CoordinateType;

  //! Unique identifier: \c rb.grid.provider.cube
  static const std::string id;

  /**
    \brief      Creates a cube.
    \param[in]  paramTree
                A Dune::ParameterTree containing
                <ul><li> the following keys directly or
                <li> a subtree named Cube::id, containing the following keys.</ul>
                The actual keys are:
                <ul><li> \c lowerLeft: \a double that is used as a lower left corner in each dimension.
                <li> \c upperRight: \a double that is used as a upper right corner in each dimension.</ul>
    **/
  GenericCube(const Dune::ParameterTree& paramTree)
  {
    // get correct parameters
    Dune::ParameterTree* paramsP = const_cast< Dune::ParameterTree* >(&paramTree);
    if (paramsP->hasSub(id))
      paramsP = &(paramsP->sub(id));
    const Dune::ParameterTree& params = const_cast< const Dune::ParameterTree& >(*paramsP);
    const double lowerLeft = params.get("lowerLeft", 0.0);
    const double upperRight = params.get("upperRight", 1.0);
    const int level = params.get("level", 1);
    buildGrid(CoordinateType(lowerLeft), CoordinateType(upperRight), level);
    buildMsGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  } // Cube(const Dune::ParameterTree& paramTree)

  /**
    \brief      Creates a cube.
    \param[in]  lowerLeft
                A vector that is used as a lower left corner.
    \param[in]  upperRight
                A vector that is used as a upper right corner.
    **/
  GenericCube(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level = 1)
  {
    buildGrid(lowerLeft, upperRight, level);
    buildMsGrid(lowerLeft, upperRight);
  }

  /**
    \brief      Creates a cube.
    \param[in]  lowerLeft
                A double that is used as a lower left corner in each dimension.
    \param[in]  upperRight
                A double that is used as a upper right corner in each dimension.
    **/
  GenericCube(double lowerLeft, double upperRight, const int level = 1)
  {
    buildGrid(CoordinateType(lowerLeft), CoordinateType(upperRight), level);
    buildMsGrid(CoordinateType(lowerLeft), CoordinateType(upperRight));
  }

  /**
    \brief  Provides access to the created grid.
    \return Reference to the grid.
    **/
  GridType& grid()
  {
    return *grid_;
  }

  const GridType& grid() const
  {
    return *grid_;
  }

  MsGridType& msGrid()
  {
    return *msGrid_;
  }

  const MsGridType& msGrid() const
  {
    return *msGrid_;
  }

  /**
    \brief      Visualizes the grid using Dune::VTKWriter.
    \param[in]  paramTree
                A Dune::ParameterTree containing
                <ul><li> the following keys directly or
                <li> a subtree named Cube::id, containing the following keys, or
                <li> a subtree named Cube::id + \c .visualize, containing the following keys.</ul>
                The actual keys are:
                <ul><li> \c grid: if specified, filename of the vtk file in which the grid which can be obtained via
                  grid() is visualized (\a if \a not \a specified: \a no \a visualization).
                <li> \c mdGrid: if specified, filename of the vtk file in which the multidomain grid which can be
                  obtained via mdGrid() is visualized (\a if \a not \a specified: \a no \a visualization).</ul>
    **/
  void visualize(Dune::ParameterTree& paramTree)
  {
    const std::string localId = "visualize";
    // get correct parameters
    Dune::ParameterTree* paramsP = &paramTree;
    if (paramsP->hasSub(id))
      paramsP = &(paramsP->sub(id));
    if (paramsP->hasSub(localId))
      paramsP = &(paramsP->sub(localId));
    Dune::ParameterTree& params = *paramsP;
    // check for grid visualization
    if (params.hasKey("grid")) {
      const std::string filenameGrid = params.get("grid", id + ".grid");
      // grid view
      typedef GridImp GridType;
      GridType& grid = this->grid();
      typedef typename GridType::LeafGridView GridView;
      GridView gridView = grid.leafView();
      // mapper
      Dune::LeafMultipleCodimMultipleGeomTypeMapper< GridType, P0Layout > mapper(grid);
      std::vector< double > data(mapper.size());
      // walk the grid
      typedef typename GridView::template Codim<0>::Iterator ElementIterator;
      typedef typename GridView::template Codim<0>::Entity ElementType;
      typedef typename ElementType::LeafIntersectionIterator FacetIteratorType;
      for (ElementIterator it = gridView.template begin<0>(); it != gridView.template end<0>(); ++it)
      {
        ElementType& element = *it;
        data[mapper.map(element)] = 0.0;
        int numberOfBoundarySegments = 0;
        bool isOnBoundary = false;
        for (FacetIteratorType facet = element.ileafbegin();
             facet != element.ileafend();
             ++facet) {
          if (!facet->neighbor() && facet->boundary()){
            isOnBoundary = true;
            numberOfBoundarySegments += 1;
            data[mapper.map(element)] += double(facet->boundaryId());
          }
        }
        if (isOnBoundary) {
          data[mapper.map(element)] /= double(numberOfBoundarySegments);
        }
      } // walk the grid
      // write to vtk
      Dune::VTKWriter< GridView > vtkwriter(gridView);
      vtkwriter.addCellData(data, "boundaryId");
      vtkwriter.write(filenameGrid, Dune::VTK::ascii);
    } // check for grid visualization
    // check for multiscale grid visualization
    /*if (params.hasKey("msGrid")) {
      const std::string filenameGrid = params.get("msGrid", id + ".msGrid");
      // fine grid view (this is not as it should be, only because the leaf grid view of the multidomaingrid sucks)
      typedef typename MsGridType::GridViewType GridViewType;
      const GridViewType gridView = msGrid().gridView();
      // prepare mapper and storage
      typedef Dune::MultipleCodimMultipleGeomTypeMapper< GridViewType, P0Layout > MapperType;
      MapperType mapper(gridView);
      std::vector< double > data(mapper.size());
      // loop over all subdomains
      for (int subdomain = 0; subdomain < msGrid_->size(); ++subdomain) {
        // local grid
        typedef typename MsGridType::LocalGridViewType LocalGridViewType;
        const LocalGridViewType localGridView = msGrid_->localGridView(subdomain);
        // walk the local grid
        typedef typename LocalGridViewType::template Codim<0>::Entity EntityType;
        typedef typename LocalGridViewType::template Codim<0>::Iterator IteratorType;
        for (IteratorType iterator = localGridView.template begin<0>();
             iterator != localGridView.template end<0>();
             ++iterator) {
          const EntityType& entity = *iterator;
          data[mapper.map(entity)] = double(subdomain);
        } // walk the local grid
      } // loop over all subdomains
      // write to vtk
      Dune::VTKWriter< typename GridType::LeafGridView > vtkwriter(grid_->leafView());
      vtkwriter.addCellData(data, "subdomain");
      vtkwriter.write(filenameGrid, Dune::VTK::ascii);
    } // check for multiscale grid visualization*/
  } // void visualize(Dune::ParameterTree& paramTree)

private:
  void buildGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level)
  {
    Dune::array< unsigned int, dim > numElements;
    std::fill(numElements.begin(), numElements.end(), 1);
    switch (variant) {
      case 1:
        grid_ = Dune::StructuredGridFactory< GridType >::createCubeGrid(lowerLeft, upperRight, numElements);
        break;
      case 2:
        grid_ = Dune::StructuredGridFactory< GridType >::createSimplexGrid(lowerLeft, upperRight, numElements);
        break;
      default:
        DUNE_THROW(Dune::NotImplemented, "Variant " << variant << " of cube not implemented.");
    }
    grid_->globalRefine(level);
    return;
  } // void buildGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)

  void buildMsGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)
  {
    // create grid
    msGrid_ = Dune::shared_ptr< MsGridType >(new MsGridType(grid()));
    // get view
    typedef typename MsGridType::GridViewType GridViewType;
    GridViewType gridView = msGrid_->gridView();
    // some preparations
    typedef typename GridViewType::template Codim<0>::Entity EntityType;
    typedef typename GridViewType::template Codim<0>::Iterator IteratorType;
    CoordinateType halfLength(0.0);
    if (dim >= 1)
      halfLength[0] = std::abs(upperRight[0] - lowerLeft[0])/2.0;
    if (dim >= 2)
      halfLength[1] = std::abs(upperRight[1] - lowerLeft[1])/2.0;
    if (dim >= 3)
      halfLength[2] = std::abs(upperRight[2] - lowerLeft[2])/2.0;
    msGrid_->prepare();
    // walk the grid
    for (IteratorType iterator = gridView.template begin<0>();
         iterator != gridView.template end<0>();
         ++iterator) {
      const EntityType& entity = *iterator;
      const CoordinateType center = entity.geometry().global(entity.geometry().center());
      int subdomain = -1;
      // treat dimension
      if (dim == 1) {
        double x = center[0];
        if (lowerLeft[0] <= x && x < halfLength[0])
          subdomain = 0;
        else if (halfLength[1] <= x && x <= upperRight[0])
          subdomain = 1;
        else {
          std::cerr << "Center is not inside [0 1]: " << center << std::endl;
          DUNE_THROW(Dune::RangeError, "We should not get here.");
        }
      } else if (dim == 2) { // treat dimension
        double x = center[0];
        double y = center[1];
        if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1])
          subdomain = 0;
        else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1])
          subdomain = 1;
        else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1])
          subdomain = 2;
        else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1])
          subdomain = 3;
        else {
          std::cerr << "Center is not inside [0 1]: " << center << std::endl;
          DUNE_THROW(Dune::RangeError, "We should not get here.");
        }
      } else if (dim == 3) { // treat dimension
        double x = center[0];
        double y = center[1];
        double z = center[2];
        if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
          subdomain = 0;
        else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && lowerLeft[2] <= z && z < halfLength[2])
          subdomain = 1;
        else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
          subdomain = 2;
        else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && lowerLeft[2] <= z && z < halfLength[2])
          subdomain = 3;
        else if (lowerLeft[0] <= x && x < halfLength[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
          subdomain = 4;
        else if (halfLength[0] <= x && x <= upperRight[0] && lowerLeft[1] <= y && y < halfLength[1] && halfLength[2] <= z && z <= upperRight[2])
          subdomain = 5;
        else if (halfLength[0] <= x && x <= upperRight[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
          subdomain = 6;
        else if (lowerLeft[0] <= x && x < halfLength[0] && halfLength[1] <= y && y <= upperRight[1] && halfLength[2] <= z && z <= upperRight[2])
          subdomain = 7;
        else {
          std::cerr << "Center is not inside [0 1]: " << center << std::endl;
          DUNE_THROW(Dune::RangeError, "We should not get here.");
        }
      } // treat dimension
      msGrid_->add(entity, subdomain);
    } // walk the grid
    // finalize
    msGrid_->finalize();
    return;
  } // void buildMsGrid(const CoordinateType& lowerLeft, const CoordinateType& upperRight)

  Dune::shared_ptr< GridType > grid_;
  Dune::shared_ptr< MsGridType > msGrid_;
}; // class GenericCube

template< typename GridImp, int variant, class MsGridImp >
const std::string GenericCube< GridImp, variant, MsGridImp >::id = "rb.grid.provider.cube";

// default implementation of a cube for any grid
// tested for
// dim = 2
//  ALUGRID_SIMPLEX, variant 2
//  ALUGRID_CONFORM, variant 2
// dim = 3
//  ALUGRID_SIMPLEX, variant 2
template< typename GridType, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< GridImp >*/ >
class Cube
  : public GenericCube< GridType, 2, MsGridType >
{
private:
  typedef GenericCube< GridType, 2, MsGridType > BaseType;

public:
  typedef typename BaseType::CoordinateType CoordinateType;

  Cube(Dune::ParameterTree& paramTree)
    : BaseType(paramTree)
  {}

  Cube(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}

  Cube(const double lowerLeft, const double upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}
}; // class Cube

// specialization of Cube for YaspGrid
// tested for dim = 1, 2, 3
template< int dim, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< Dune::YaspGrid< dim > >*/ >
class Cube< Dune::YaspGrid< dim >, MsGridType >
  : public GenericCube< Dune::YaspGrid< dim >, 1, MsGridType >
{
private:
  typedef GenericCube< Dune::YaspGrid< dim >, 1, MsGridType > BaseType;

public:
  typedef typename BaseType::CoordinateType CoordinateType;

  Cube(Dune::ParameterTree& paramTree)
    : BaseType(paramTree)
  {}

  Cube(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}

  Cube(const double lowerLeft, const double upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}
}; // class Cube< Dune::YaspGrid< dim >, MsGridType >

// specialization of Cube for SGrid
// tested for dim = 1, 2, 3
template< int dim, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< Dune::SGrid< dim, dim > >*/ >
class Cube< Dune::SGrid< dim, dim >, MsGridType >
  : public GenericCube< Dune::SGrid< dim, dim >, 1, MsGridType >
{
private:
  typedef GenericCube< Dune::SGrid< dim, dim >, 1, MsGridType > BaseType;

public:
  typedef typename BaseType::CoordinateType CoordinateType;

  Cube(Dune::ParameterTree& paramTree)
    : BaseType(paramTree)
  {}

  Cube(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}

  Cube(const double lowerLeft, const double upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}
}; // class Cube< Dune::SGrid< dim, dim >, MsGridType >

// specialization of Cube for ALUCubeGrid
// tested for dim = 2, 3
template< int dim, class MsGridType >
class Cube< Dune::ALUCubeGrid< dim, dim >, MsGridType >
  : public GenericCube< Dune::ALUCubeGrid< dim, dim >, 1, MsGridType >
{
private:
  typedef GenericCube< Dune::ALUCubeGrid< dim, dim >, 1, MsGridType > BaseType;

public:
  typedef typename BaseType::CoordinateType CoordinateType;

  Cube(Dune::ParameterTree& paramTree)
    : BaseType(paramTree)
  {}

  Cube(const CoordinateType& lowerLeft, const CoordinateType& upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}

  Cube(const double lowerLeft, const double upperRight, const int level = 1)
    : BaseType(lowerLeft, upperRight, level)
  {}
}; // class Cube< Dune::ALUCubeGrid< dim, dim >, MsGridType >

// default implementation of a unit cube for any grid
// tested for
// dim = 2
//  ALUGRID_SIMPLEX, variant 2
//  ALUGRID_CONFORM, variant 2
// dim = 3
//  ALUGRID_SIMPLEX, variant 2
template< typename GridType, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< GridImp >*/ >
class UnitCube
  : public Cube< GridType, MsGridType >
{
private:
  typedef Cube< GridType, MsGridType > BaseType;

public:
  UnitCube(Dune::ParameterTree& paramTree)
    : BaseType(0.0, 1.0, paramTree.get("level", 1))
  {}

  UnitCube(const int level = 1)
    : BaseType(0.0, 1.0, level)
  {}
}; // class UnitCube

//// specialization of UnitCube for YaspGrid
//// tested for dim = 1, 2, 3
//template< int dim, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< Dune::YaspGrid< dim > >*/ >
//class UnitCube< Dune::YaspGrid< dim >, MsGridType >
//  : public Cube< Dune::YaspGrid< dim >, 1, MsGridType >
//{
//private:
//  typedef Cube< Dune::YaspGrid< dim >, 1, MsGridType > BaseType;

//public:
//  UnitCube(Dune::ParameterTree& paramTree)
//    : BaseType(0.0, 1.0, paramTree.get("level", 1))
//  {}
//}; // class UnitCube< Dune::YaspGrid< dim >, MsGridType >

//// specialization of UnitCube for SGrid
//// tested for dim = 1, 2, 3
//template< int dim, class MsGridType /*= Dune::RB::Grid::Multiscale::Default< Dune::SGrid< dim, dim > >*/ >
//class UnitCube< Dune::SGrid< dim, dim >, MsGridType >
//  : public Cube< Dune::SGrid< dim, dim >, 1, MsGridType >
//{
//private:
//  typedef Cube< Dune::SGrid< dim, dim >, 1, MsGridType > BaseType;

//public:
//  UnitCube(Dune::ParameterTree& paramTree)
//    : BaseType(0.0, 1.0, paramTree.get("level", 1))
//  {}
//}; // class UnitCube< Dune::SGrid< dim, dim >, MsGridType >

//// specialization of UnitCube for ALUCubeGrid
//// tested for dim = 2, 3
//template< int dim, class MsGridImp >
//class UnitCube< Dune::ALUCubeGrid< dim, dim >, MsGridImp >
//  : public Cube< Dune::ALUCubeGrid< dim, dim >, 1, MsGridImp >
//{
//private:
//  typedef Cube< Dune::ALUCubeGrid< dim, dim >, 1, MsGridImp > BaseType;

//public:
//  UnitCube(Dune::ParameterTree& paramTree)
//    : BaseType(0.0, 1.0, paramTree.get("level", 1))
//  {}
//}; // class UnitCube< Dune::ALUCubeGrid< dim, dim >, MsGridImp >

//#include "cube.cc"

} // namespace Provider

} // namespace Grid

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_GRID_PROVIDER_CUBE_HH
