/**
  \file   dune/rb/grid/multiscale/default.hh
  \brief  dune/rb/grid/multiscale/default.hh - Contains a default implementation of a multiscale grid
  **/

#ifndef DUNE_RB_GRID_MULTISCALE_DEFAULT_HH
#define DUNE_RB_GRID_MULTISCALE_DEFAULT_HH

// system
#include <algorithm>
#include <map>
#include <set>
#include <vector>

// dune-common
#include <dune/common/exceptions.hh>

namespace Dune
{

namespace RB
{

namespace Grid
{

namespace Multiscale
{

template< class GridImp >
class Default
{
public:
  typedef GridImp GridType;

private:
  typedef typename GridType::template Codim< 0 >::EntitySeed EntitySeedType;

public:
  typedef typename GridType::template Codim< 0 >::Entity EntityType;

  static const std::string id;

  Default(GridType& grid)
    : grid_(grid),
      subdomains_(),
      entitySeeds_(),
      finalized_(false),
      size_(0)
  {
  }

  GridType& grid()
  {
    return grid_;
  }

  int size() const
  {
    return size_;
  }

  bool finalized() const
  {
    return finalized_;
  }

  void prepare()
  {
  }

  void add(const EntityType& entity, int subdomain)
  {
    // assert
    assert(!finalized_);
    assert(0 <= subdomain);
    // add subdomain
    subdomains_.insert(subdomain);
    // get seed
    EntitySeedType seed = entity.seed();
    // only add if entity is not yet in subdomain
//#ifndef NDEBUG
//    if (!std::find(entitySeeds_[subdomain].begin(), entitySeeds_[subdomain].end(), seed)) {
//#endif
      entitySeeds_[subdomain].push_back(seed);
//#ifndef NDEBUG
//    } // only add if entity is not yet in subdomain
//#endif
  } // void add(const EntityType& entity, int subdomain)

  void finalize()
  {
    // check for consecutive numbering
    size_ = subdomains_.size();
    // set flag
    finalized_ = true;
    for (int i = 0; i < size(); ++i) {
      if (subdomains_.count(i) == 0) {
        finalized_ = false;
        std::stringstream msg;
        msg << "Error in " << id << ": numbering of subdomains not consecutive in the following set (after ordering):" << std::endl;
        typedef std::set< int >::iterator IteratorType;
        for (IteratorType it = subdomains_.begin(); it != subdomains_.end(); ++it) {
          msg << *it << " ";
        }
        DUNE_THROW(Dune::InvalidStateException, msg.str());
      }
    } // check for consecutive numbering
  } // void finalize()

private:
  GridType& grid_;
  std::set< int > subdomains_;
  std::map< int, std::vector< EntitySeedType > > entitySeeds_;
  bool finalized_;
  int size_;
}; // class Default

template< class GridImp >
const std::string Default< GridImp >::id = "rb.grid.multiscale.default";

} // namespace Multiscale

} // namespace Grid

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_GRID_MULTISCALE_DEFAULT_HH
