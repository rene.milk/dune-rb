/**
  \file   dune/rb/grid/multiscale/interface.hh
  \brief  dune/rb/grid/multiscale/interface.hh - Contains the interface for all multiscale grids
  **/

#ifndef DUNE_RB_GRID_MULTISCALE_INTERFACE_HH
#define DUNE_RB_GRID_MULTISCALE_INTERFACE_HH

namespace Dune
{

namespace RB
{

namespace Grid
{

namespace Multiscale
{

template< class T >
class InterfaceTraits
{};

template< class Imp >
class Interface
{
public:
  typedef InterfaceTraits< Imp > Traits;

  typedef typename Traits::derived_type derived_type;

  typedef typename Traits::HostGridType HostGridType;

  typedef typename Traits::GridType GridType;

  typedef typename Traits::GridViewType GridViewType;

  typedef typename Traits::EntityType EntityType;

  typedef typename Traits::LocalGridViewType LocalGridViewType;

  static const std::string id;

  static const unsigned int dim = GridType::dimension;

  Interface()
  {}

  GridType& grid()
  {
    return asImp().grid();
  }

  int size() const
  {
    return asImp().size();
  }

  bool finalized() const
  {
    return asImp().finalized();
  }

  void prepare()
  {
    asImp().prepare();
  }

  void add(const EntityType& entity, int subdomain)
  {
    asImp().add(entity, subdomain);
  }

  void finalize()
  {
    asImp().finalize();
  }

  GridViewType gridView()
  {
    return asImp().gridView();
  }

  LocalGridViewType localGridView(int subdomain)
  {
    return asImp().localGridView(subdomain);
  }

private:
  derived_type& asImp()
  {
    return static_cast< derived_type& >(*this);
  }

  const derived_type& asImp() const
  {
    return static_cast< const derived_type& >(*this);
  }
}; // class Interface

template< class derived_type >
const std::string Interface< derived_type >::id = "rb.grid.multiscale.interface";

} // namespace Multiscale

} // namespace Grid

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_GRID_MULTISCALE_INTERFACE_HH
