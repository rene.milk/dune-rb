#ifndef DUNE_RB_GRID_COARSE_HH
#define DUNE_RB_GRID_COARSE_HH

// stl stuff
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <assert.h>

// common
#include <dune/common/exceptions.hh>

namespace Dune {
namespace RB {
namespace Grid {
namespace Coarse {
template< class GridPartImp >
class Multidomain
  : public GridPartImp
{
public:
  typedef GridPartImp                                                   BaseType;
  typedef Multidomain< BaseType >                                       ThisType;

  typedef typename BaseType::GridType                                   GridType;
  typedef typename GridType::Traits::template Codim< 0 >::Entity        EntityType;
  typedef typename GridType::Traits::template Codim< 0 >::EntitySeed    EntitySeedType;
  typedef typename GridType::Traits::template Codim< 0 >::EntityPointer EntityPointerType;
  typedef typename BaseType::IntersectionIteratorType                   IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection               IntersectionType;
  typedef std::map< int, int >                                          MapType;
  typedef typename MapType::iterator                                    MapIterator;
  typedef typename MapType::const_iterator                              ConstMapIterator;

public:
  Multidomain(GridType& grid)
    : BaseType(grid),
      grid_(grid),
      subdomains_()
  {}

  const size_t numSubdomains() const
  {
    return subdomains_.size();
  }

  const int size() const
  {
    return numSubdomains();
  }

  /** @remark Numbering starts at zero!*/
  void addToSubdomain(const int subdomain, const EntityType& entity)
  {
    subdomains_.insert(subdomain);
    int index = BaseType::indexSet().index(entity);
    entityToSubdomainMap_[index] = subdomain;

    // EntitySeedType seed = entity.seed();
    // assert(std::find(entitySeeds_[subdomain].begin(),entitySeeds_[subdomain].end(), seed)
    // ==entitySeeds_[subdomain].end());
    // entitySeeds_[subdomain].push_back(seed);
    subdomainSizes_[subdomain]++;
  } // addToSubdomain

  const int enclosedFineCells(const int subdomain) const
  {
    std::map< int, int >::const_iterator it = subdomainSizes_.find(subdomain);
    assert(it != subdomainSizes_.end());
    return it->second;
  }

  const bool subdomainContains(const int subdomain,
                               const EntityType& entity) const
  {
    int              index = BaseType::indexSet().index(entity);
    ConstMapIterator it    = entityToSubdomainMap_.find(index);

    assert(it != entityToSubdomainMap_.end());
    // DUNE_THROW(InvalidStateException, "The given entity was not marked to belong to any subdomain!");
    const int enSubdomain = it->second;
    bool      contains    = (enSubdomain == subdomain);
    return contains;
  } // subdomainContains

  int getSubdomain(const EntityType& entity) const
  {
    int              index = BaseType::indexSet().index(entity);
    ConstMapIterator it    = entityToSubdomainMap_.find(index);

    assert(it != entityToSubdomainMap_.end());
    // DUNE_THROW(InvalidStateException, "The given entity was not marked to belong to any subdomain!");
    return it->second;
  } // getSubdomain

  bool onCoarseCellIntersection(const IntersectionType& intersection) const
  {
    if (intersection.boundary())
      return false;

    EntityPointerType insidePtr  = intersection.inside();
    const EntityType& insideEn   = *insidePtr;
    EntityPointerType outsidePtr = intersection.outside();
    const EntityType& outsideEn  = *outsidePtr;
    return getSubdomain(insideEn) != getSubdomain(outsideEn);
  } // onCoarseCellIntersection

private:
  const GridType& grid_;

public:
  MapType              entityToSubdomainMap_;
  std::map< int, int > subdomainSizes_;

public:
  std::map< int, std::vector< EntitySeedType > > entitySeeds_;

private:
  std::set< int > subdomains_;
  // IntersectionVector coarseCellIntersections_;
}; // class Interface
} // namespace Coarse
} // namespace Grid
} // namespace RB
} // namespace Dune

#endif // DUNE_RB_GRID_COARSE_HH