/**
  \file dune/rb/parameter/function.cc
  **/

// local
#include "function.hh"

template< class DomainFieldImp, int maxDimDomain, class RangeFieldImp, int maxDimRange >
const std::string Function< DomainFieldImp, maxDimDomain, RangeFieldImp, maxDimRange >::id = "rb.parameter.function";

/*template< class DomainFieldType, class RangeFieldType >
const std::string Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::id = "rb.parameter.function";*/

/*template< class DomainType, class RangeType >
Function< DomainType, RangeType >::Function(Dune::ParameterTree& paramTree)
{
  // assert dims
  assert(dimDomain > 0);
  assert(dimRange > 0);
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
    if (paramsP->hasSub(id))
      paramsP = &(paramsP->sub(id));
  Dune::ParameterTree& params = *paramsP;
  //get variable
  if (params.hasKey("variable")) {
    variable_ = params.get("variable", "");
  } else {
    std::stringstream msg;
    msg << "Error in " << id << ": key 'variable' not found in the following Dune::Parametertree" << std::endl;
    params.report(msg);
    DUNE_THROW(Dune::InvalidStateException, msg.str());
  }
  for (int i = 0; i < dimDomain; ++i) {
    std::stringstream variable;
    variable << variable_ << "[" << i << "]";
    variables_.push_back(variable.str());
  }
  // get expressions
  for (int i = 0; i < dimRange; ++ i) {
    std::stringstream expression;
    expression << "expression." << i;
    if (params.hasKey(expression.str())) {
      expressions_.push_back(params.get(expression.str(), ""));
    } else {
      std::stringstream msg;
      msg << "Error in " << id << ": key '" << expression.str() << "' not found in the following Dune::Parametertree" << std::endl;
      params.report(msg);
      DUNE_THROW(Dune::InvalidStateException, msg.str());
    }
  }
  // set up
  setUp();
} // Function< DomainType, RangeType >::Function(Dune::ParameterTree& paramTree)*/

/*template< class DomainFieldType, class RangeFieldType >
Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::Function(Dune::ParameterTree& paramTree)
{
  // get correct parameters
  Dune::ParameterTree* paramsP = &paramTree;
    if (paramsP->hasSub(id))
      paramsP = &(paramsP->sub(id));
  Dune::ParameterTree& params = *paramsP;
  // variable
  if (params.hasKey("variable")) {
    variable_ = params.get("variable", "");
  } else {
    std::stringstream msg;
    msg << "Error in " << id << ": key 'variable' not found in the following Dune::Parametertree" << std::endl;
    params.report(msg);
    DUNE_THROW(Dune::InvalidStateException, msg.str());
  }
  // expressions
  std::stringstream expression;
  expression << "expression";
  if (params.hasSub(expression.str()))
  {
    std::stringstream variable;
    variable << variable_ << "[0]";
    variables_.push_back(variable.str());
    expression << ".0";
    if (params.hasKey(expression.str())) {
      expressions_.push_back(params.get(expression.str(), ""));
    } else {
      std::stringstream msg;
      msg << "Error in " << id << ": key '" << expression.str() << "' not found in the following Dune::Parametertree" << std::endl;
      params.report(msg);
      DUNE_THROW(Dune::InvalidStateException, msg.str());
    }
  } else if (params.hasKey(expression.str()))
  {
    variables_.push_back(variable_);
    expressions_.push_back(params.get(expression.str(), ""));
  } else {
    std::stringstream msg;
    msg << "Error in " << id << ": neither key nor subtree '" << expression.str() << "' found in the following Dune::Parametertree" << std::endl;
    params.report(msg);
    DUNE_THROW(Dune::InvalidStateException, msg.str());
  }
  // set up
  setUp();
} // Function< DomainType, RangeType >::Function(Dune::ParameterTree& paramTree)*/

/*template< class DomainType, class RangeType >
Function< DomainType, RangeType >::~Function()
{
  for (int i = 0; i < dimRange; ++i) {
    delete op_[i];
  }
  for (int i = 0; i < dimDomain; ++i) {
    delete var_arg_[i];
    delete arg_[i];
  }
} // Function< DomainType, RangeType >::~Function()*/

/*template< class DomainFieldType, class RangeFieldType >
Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::~Function()
{
  delete op_[0];
  delete var_arg_[0];
  delete arg_[0];
} // Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::~Function()*/

/*template< class DomainType, class RangeType >
std::string Function< DomainType, RangeType >::variable() const
{
  return variable_;
} // std::string Function< DomainType, RangeType >::variable() const*/

/*template< class DomainFieldType, class RangeFieldType >
std::string Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::variable() const
{
  return variable_;
} // std::string Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::variable() const*/

/*template< class DomainType, class RangeType >
std::string Function< DomainType, RangeType >::expression(int i) const
{
  assert(0 <= i);
  assert(i < dimRange);
  return expressions_[i];
} // std::string Function< DomainType, RangeType >::expression(int i) const*/

/*template< class DomainFieldType, class RangeFieldType >
std::string Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::expression(int i = 0) const
{
  assert(i == 0);
  return expressions_[0];
}*/

/*template< class DomainType, class RangeType >
void Function< DomainType, RangeType >::evaluate(const DomainType& arg, RangeType& ret) const
{
  // ensure right dimensions
  assert(arg.size() == dimDomain);
  assert(ret.size() == dimRange);
  // arg
  for (int i = 0; i < dimDomain; ++ i) {
    *(arg_[i]) = arg[i];
  }
  // ret
  for (int i = 0; i < dimRange; ++ i) {
    ret[i] = op_[i]->Val();
  }
} // void Function< DomainType, RangeType >::evaluate(const DomainType& arg, RangeType& ret) const*/

/*template< class DomainFieldType, class RangeFieldType >
void Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::evaluate(const DomainType& arg, RangeType& ret) const
{
  // ensure right dimensions
  assert(arg.size() == 1);
  assert(ret.size() == 1);
  // arg
  *(arg_[0]) = arg[0];
  // ret
  ret[0] = op_[0]->Val();
} // void Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::evaluate(const DomainType& arg, RangeType& ret) const*/

/*template< class DomainType, class RangeType >
RangeType Function< DomainType, RangeType >::evaluate(const DomainType& arg) const
{
  RangeType ret(0.0);
  evaluate(arg, ret);
  return ret;
} // void Function< DomainType, RangeType >::evaluate(const DomainType& arg, RangeType& ret) const*/

/*template< class DomainFieldType, class RangeFieldType >
Dune::FieldVector< RangeFieldType, 1 > Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::evaluate(const DomainType& arg) const
{
  RangeType ret(0.0);
  evaluate(arg, ret);
  return ret;
} // RangeType Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::evaluate(const DomainType& arg) const*/

/*template< class DomainType, class RangeType >
void Function< DomainType, RangeType >::setUp()
{
  for (int i = 0; i < dimDomain; ++i) {
    arg_[i] = new DomainFieldType(0.0);
    var_arg_[i] = new RVar(variables_[i].c_str(), arg_[i]);
    vararray_[i] = var_arg_[i];
  }
  for (int i = 0; i < dimRange; ++ i) {
    op_[i] = new ROperation(expressions_[i].c_str(), dimDomain, vararray_);
  }
} // void Function< DomainType, RangeType >::setUp()*/

/*template< class DomainFieldType, class RangeFieldType >
void Function< Dune::FieldVector< DomainFieldType, 1 >, Dune::FieldVector< RangeFieldType, 1 > >::setUp()
{
  arg_[0] = new DomainFieldType(0.0);
  var_arg_[0] = new RVar(variables_[0].c_str(), arg_[0]);
  vararray_[0] = var_arg_[0];
  op_[0] = new ROperation(expressions_[0].c_str(), 1, vararray_);
} // void Function< DomainType, RangeType >::setUp()*/
