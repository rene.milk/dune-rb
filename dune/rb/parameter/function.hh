/**
  \file   dune/rb/parameter/function.hh
  \brief  dune/rb/parameter/function.hh - Contains a function which evaluates an expression at runtime.
 **/

#ifndef DUNE_RB_PARAMETER_FUNCTION_HH
#define DUNE_RB_PARAMETER_FUNCTION_HH

// system
#include <sstream>

// dune-common
#include <dune/common/fvector.hh>
#include <dune/common/dynvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/exceptions.hh>

// eigen
#include <Eigen/Core>

// local
#include "function/mathexpr.hh"

namespace Dune
{

namespace RB
{

namespace Parameter
{

/**
  \brief  Provides a function which evaluates a given mathematical expression at runtime.

          Given a mathematical expression as a string, a domain \f$ K_d^{m \geq 1} \f$ and a range \f$ K_r^{n \geq 1}
          \f$ this function represents the map
          \f{eqnarray}
            f:K_d^m \to K_r^n\\
            x = (x_1, \dots, x_m)' \mapsto (f_1(x), \dots f_n(x))',
          \f}
          where \f$ K_d \f$ is the DomainType and \f$ K_r \f$ is the RangeType, usually a power of \f$ \mathcal{R} \f$.
          The name of the variable as well as the \f$ n \f$ expressions of \f$f_1, \dots, f_n\f$ have to be given in a
          Dune::ParameterTree in the following form:
\code variable: x
expression.0: 2*x[0]
expression.1: sin(x[1])*x[0]\endcode
          There have to exist at least \f$n\f$ expressions; the entries of the variable can are indexed by \f$[i]\f$ for
          \f$ 0 \leq i \leq m - 1 \f$.
  \tparam DomainTypeImp
          Type of the
  \tparam
 **/
template< class DomainFieldImp, int maxDimDomain, class RangeFieldImp, int maxDimRange >
class Function
{
public:
  typedef DomainFieldImp DomainFieldType;

  typedef RangeFieldImp RangeFieldType;

  static const std::string id;

public:
  Function(const Dune::ParameterTree& paramTree)
  {
    // assert dims
    assert(maxDimDomain > 0);
    assert(maxDimRange > 0);
    // get correct parameters
    Dune::ParameterTree* paramsP = const_cast< Dune::ParameterTree* >(&paramTree);
      if (paramsP->hasSub(id))
        paramsP = &(paramsP->sub(id));
    const Dune::ParameterTree& params = const_cast< const Dune::ParameterTree& >(*paramsP);
    //get variable
    if (params.hasKey("variable")) {
      variable_ = params.get("variable", "");
    } else {
      std::stringstream msg;
      msg << "Error in " << id << ": key 'variable' not found in the following Dune::Parametertree" << std::endl;
      params.report(msg);
      DUNE_THROW(Dune::InvalidStateException, msg.str());
    }
    for (unsigned int i = 0; i < maxDimDomain; ++i) {
      std::stringstream variable;
      variable << variable_ << "[" << i << "]";
      variables_.push_back(variable.str());
    }
    // get expressions
    bool goOn = true;
    unsigned int i = 0;
    while (goOn) {
      std::stringstream expression;
      expression << "expression." << i;
      if (params.hasKey(expression.str())) {
        expressions_.push_back(params.get(expression.str(), ""));
        ++i;
      } else {
        goOn = false;
      }
      actualDimRange_ = i;
    } // while (goOn)
    if (actualDimRange_ < 1) {
      std::stringstream msg;
      msg << "Error in " << id << ": no subtree 'expression' with keys 0, 1, ... found in the following Dune::Parametertree" << std::endl;
      params.report(msg);
      DUNE_THROW(Dune::InvalidStateException, msg.str());
    }
    // set up
    setUp();
  } // Function(Dune::ParameterTree& paramTree)


  Function(const std::string variable, const std::vector<std::string>& expressions)
  : variable_(variable),
  expressions_(expressions),
  actualDimRange_(expressions.size())
  {
    // assert dims
    assert(maxDimDomain > 0);
    assert(maxDimRange > 0);

    for (int i = 0; i < maxDimDomain; ++i) {
      std::stringstream variableStream;
      variableStream << variable_ << "[" << i << "]";
      variables_.push_back(variableStream.str());
    }
    if (actualDimRange_ < 1) {
      std::stringstream msg;
      msg << "Error in " << id << ": Given expressions-vector is empty!" << std::endl;
      DUNE_THROW(Dune::InvalidStateException, msg.str());
    }
    // set up
    setUp();
  } // Function(Dune::ParameterTree& paramTree)


  ~Function()
  {
    for (unsigned int i = 0; i < dimRange(); ++i) {
      delete op_[i];
    }
    for (unsigned int i = 0; i < maxDimDomain; ++i) {
      delete var_arg_[i];
      delete arg_[i];
    }
  } // ~Function()

  std::string variable() const
  {
    return variable_;
  }

  std::string expression(unsigned int i) const
  {
    assert(0 <= i);
    assert(i < dimRange());
    return expressions_[i];
  } // std::string expression(int i) const

  const std::vector<std::string>& expressions() const
  {
    return expressions_;
  }

  unsigned int dimRange() const
  {
    return actualDimRange_;
  }

  template< class DomainType, class RangeType >
  void evaluate(const DomainType& arg, RangeType& ret) const
  {
    DUNE_THROW(Dune::NotImplemented, "Not implemented for arbitrary DomainType and RangeType");
  }

  template< class DomainK, int domainSize, class RangeK, int rangeSize >
  void evaluate(const Dune::FieldVector< DomainK, domainSize >& arg, Dune::FieldVector< RangeK, rangeSize >& ret) const
  {
    // ensure right dimensions
    assert(arg.size() <= maxDimDomain);
    assert(ret.size() <= dimRange());
    // arg
    for (unsigned int i = 0; i < arg.size(); ++ i) {
      *(arg_[i]) = arg[i];
    }
    // ret
    for (unsigned int i = 0; i < ret.size(); ++ i) {
      ret[i] = op_[i]->Val();
    }
  }

  template< class DomainK, class RangeK >
  void evaluate(const Dune::DynamicVector< DomainK >& arg, Dune::DynamicVector< RangeK >& ret) const
  {
    // ensure right dimensions
    assert(arg.size() <= maxDimDomain);
    assert(ret.size() <= dimRange());
    // arg
    for (int i = 0; i < arg.size(); ++ i) {
      *(arg_[i]) = arg[i];
    }
    // ret
    for (int i = 0; i < ret.size(); ++ i) {
      ret[i] = op_[i]->Val();
    }
  }

  void evaluate(const Eigen::VectorXd& arg, Eigen::VectorXd& ret) const
  {
    // ensure right dimensions
    assert(arg.rows() <= maxDimDomain);
    assert(ret.rows() <= dimRange());
    // arg
    for (int i = 0; i < arg.rows(); ++ i) {
      *(arg_[i]) = arg(i);
    }
    // ret
    for (int i = 0; i < ret.size(); ++ i) {
      ret(i) = op_[i]->Val();
    }
  }

private:
  void setUp()
  {
    for (unsigned int i = 0; i < maxDimDomain; ++i) {
      arg_[i] = new DomainFieldType(0.0);
      var_arg_[i] = new RVar(variables_[i].c_str(), arg_[i]);
      vararray_[i] = var_arg_[i];
    }
    for (unsigned int i = 0; i < dimRange(); ++ i) {
      op_[i] = new ROperation(expressions_[i].c_str(), maxDimDomain, vararray_);
    }
  } // void setUp()

  std::string                variable_;
  std::vector< std::string > variables_;
  std::vector< std::string > expressions_;
  unsigned int actualDimRange_;
  mutable DomainFieldType* arg_[maxDimDomain];
  RVar* var_arg_[maxDimDomain];
  RVar* vararray_[maxDimDomain];
  ROperation* op_[maxDimRange];
}; // class Function

template< class DomainFieldImp, int maxDimDomain, class RangeFieldImp, int maxDimRange >
const std::string Function< DomainFieldImp, maxDimDomain, RangeFieldImp, maxDimRange >::id = "rb.parameter.function";

} // namespace Parameter

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_PARAMETER_FUNCTION_HH
