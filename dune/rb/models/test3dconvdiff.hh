#ifndef __MODELSTEST3DCOSSIN_HH_
#define __MODELSTEST3DCOSSIN_HH_

#include "../datafunc/datafunc.hh"
#include "../rbasis/linevol/linevolmodel.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

/** @ingroup models
 * @brief  linear evolution model definition named "TEST3DCONVDIFF"
 *
 * This is a linear advection-diffusion model with following data functions:
 *   - Initial Data: DirichletCosSin
 *   - Dirichlet Boundary: DirichletCosSin
 *   - Diffusion Data: DiffusionConstantIsotropic
 *   - Velocity Field: VelocityConstant
 *
 *
 */
template<class ModelParamType>
struct Model<ModelParamType, TEST3DCONVDIFF> {
  //! domain vector type
  typedef typename ModelParamType :: DomainType                      DomainType;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! this type
  typedef Model< ModelParamType, TEST3DCONVDIFF >             ThisType;
  //! domain of domain vector space
  static const int dimDomain = FunctionSpaceType :: dimDomain;

  typedef Lib::Data::DirichletCosSin< ModelParamType >                          ExactLocalFunction;

  typedef Decomposed::Function::FactoryParams0< Lib::Data::DirichletCosSin,
                                         ModelParamType >            InitDataParams;
  //! initial data function factory
  typedef Decomposed::Function::Factory< InitDataParams >                InitDataFactory;
  typedef Decomposed::Function::FactoryParams1< Lib::Data::VelocityConstant,
                                         ModelParamType >            VelocityParams;
  //! diffusion data function factory
  typedef Decomposed::Function::Factory< VelocityParams >                VelocityFactory;
  typedef Decomposed::Function::FactoryParams0< Lib::Data::DiffusionConstantIsotropic,
                                         ModelParamType >            DiffusionParams;
  typedef Decomposed::Function::Factory< DiffusionParams >               DiffusionFactory;
  //! dirichlet data function factory
  typedef Decomposed::Function::FactoryParams0< Lib::Data::DirichletCosSin,
                                         ModelParamType >            DirichletParams;
  typedef Decomposed::Function::Factory< DirichletParams >               DirichletFactory;

  //! boundary type selection
  struct BoundaryBox
  {

    template <class IntersectionIterator, class FaceDomainType>
    static int boundaryType(const IntersectionIterator & it, const double time ,
                            const FaceDomainType & localPoint, const DomainType & point )
    {
      return boundaryType(point);
    }

    //! define the type of boundary condtions
    static int boundaryType(const DomainType & midPoint)
    {
      // if ( (midPoint[1] < 0.001) || (midPoint[0] < 0.001) )
        return BoundaryType(Dirichlet);
      // else  if (midPoint[1] > 0.95)
      //   return BoundaryType(NoFlow);
      //else 
        // return BoundaryType(OutFlow);
    }

  };

  //! model implementation type
  typedef ConvectionDiffusionModel< ModelParamType, InitDataFactory,
                        VelocityFactory, DiffusionFactory,
                        DirichletFactory, BoundaryBox,
                        ExactLocalFunction >                         Type;
};

} // namespace LinEvol
} // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif /* __MODELSTEST3DCOSSIN_HH_ */

