#ifndef MODELSONED_HH_

#define MODELSONED_HH_

#include "../datafunc/datafunc.hh"
#include "../rbasis/linevol/linevolmodel.hh"

namespace Dune {
namespace RBFem {
namespace Example {
namespace LinEvol {

/** @ingroup models
 * @brief  linear evolution model definition named "OneD"
 *
 * This is a linear advection-diffusion model with following data functions:
 *   - Initial Data: InitDataUnsteadyWave
 *   - Dirichlet Boundary: DirichletConstant
 *   - Diffusion Data: DiffusionConstantIsotropic
 *   - Velocity Field: VelocityConstant
 *
 * The Boundary type is all NoFlow.
 */
template<class ModelParamType>
struct Model<ModelParamType, OneD>
{
  //! domain vector type
  typedef typename ModelParamType :: DomainType                      DomainType;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! this type
  typedef Model< ModelParamType, OneD >                       ThisType;

  static const int dimDomain = FunctionSpaceType :: dimDomain;

  typedef Decomposed::Function::FactoryParams1< Lib::Data::InitDataUnsteadyWave,
                                         ModelParamType >            InitDataParams;
  //! initial data function factory
  typedef Decomposed::Function::Factory< InitDataParams, 2 >             InitDataFactory;
  typedef Decomposed::Function::FactoryParams1< Lib::Data::VelocityConstant,
                                         ModelParamType >            VelocityParams;
  //! velocity data function factory
  typedef Decomposed::Function::Factory< VelocityParams >                VelocityFactory;
  typedef Decomposed::Function::FactoryParams0< Lib::Data::DiffusionConstantIsotropic,
                                         ModelParamType >            DiffusionParams;
  //! diffusion data function factory
  typedef Decomposed::Function::Factory< DiffusionParams >               DiffusionFactory;
  typedef Decomposed::Function::FactoryParams0< Lib::Data::DirichletConstant,
                                         ModelParamType >            DirichletParams;
  //! dirichlet data function factory
  typedef Decomposed::Function::Factory< DirichletParams > DirichletFactory;

  //! boundary type selection
  struct BoundaryBox
  {

    template <class IntersectionIterator, class FaceDomainType>
    static int boundaryType(const IntersectionIterator & it, const double time ,
                            const FaceDomainType & localPoint, const DomainType & point )
    {
      return BoundaryType(NoFlow);
    }

  };

  //! model implementation type
  typedef ConvectionDiffusionModel< ModelParamType, InitDataFactory,
                        VelocityFactory, DiffusionFactory,
                        DirichletFactory, BoundaryBox >              Type;
};

} // namespace LinEvol
} // namespace Example
} // end of namespace RBFem
} // end of namespace Dune

#endif /* MODELS/CONVDIFF_HH_ */
