#ifndef __DUNE_RB_LA_ALGORITHMS_GRAMSCHMIDT_HH__
#define __DUNE_RB_LA_ALGORITHMS_GRAMSCHMIDT_HH__


namespace Dune {
namespace RB {
namespace LA {
namespace Algorithms {
/** Perform the Gram-Schmidt algorithm using a given matrix for the scalar products.
 *
 *  @param[in,out] u    The function that is to be projected on the orthorgonal complement of the space spanned by
 *                      the vectors in phi.
 *  @param[in]     phi  The function that span the space to which u shall be orthorgonal.
 *  @param[in]     A    The matrix that defines the scalar product.
 */
template< class BasisType, class MatrixType >
void gramSchmidt(Eigen::VectorXd& u, const BasisType& phi, const MatrixType& A)
{
  Eigen::VectorXd uCopy(u);

  int baseSetSize = phi.cols();

  // perfom the projection algorithm
  for (int j = 0; j != baseSetSize; ++j) {
    double prod       = uCopy.transpose() * A * phi.col(j);
    double squarenorm = phi.col(j).transpose() * A * phi.col(j);
    u -= phi.col(j) * prod / squarenorm;
  }

  return;
} // gramSchmidt
} // namespace Algorithms
} // namespace LA
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_LA_ALGORITHMS_GRAMSCHMIDT_HH__