#ifndef __DUNE_RB_LA_ALGORITHMS_PCA_HH__
#define __DUNE_RB_LA_ALGORITHMS_PCA_HH__

#include <Eigen/Core>
#include <Eigen/Eigen>

namespace Dune {
namespace RB {
namespace LA {
namespace Algorithms {
template< class MatrixImpl >
class PCA
{
  typedef MatrixImpl                                       EigenMatrixType;
  typedef Eigen::SelfAdjointEigenSolver< EigenMatrixType > EigenvalueSolverType;
  typedef typename Eigen::VectorXd                         EigenvalueVectorType;

public:
  PCA(const EigenMatrixType& matrix)
    : matrix_(matrix)
  {}

  void subtractMean(const Eigen::MatrixXi& restrictionInfo, const int subdomain)
  {
    // Calculate and substract the deviations from the mean
    matrix_ -= matrix_.colwise().mean().transpose().replicate(matrix_.rows(), 1);
    // restrict basis again, this is a hack that should be made superfluous
    matrix_ = (restrictionInfo.col(subdomain).replicate(1, matrix_.cols()).array() > 0).select(matrix_, 0);
    return;
  } // subtractMean

  const EigenMatrixType& computeCovarianceMatrix()
  {
    covarianceMatrix_ = (matrix_.transpose() * matrix_) / matrix_.cols();
    return covarianceMatrix_;
  }

  template< class ProductMatrixType >
  const EigenMatrixType& computeCovarianceMatrix(const ProductMatrixType& productMatrix)
  {
    covarianceMatrix_ = (matrix_.transpose() * productMatrix * matrix_);

    return covarianceMatrix_;
  }


  void computeEigenvalues()
  {
    if (covarianceMatrix_.size() == 0)
      DUNE_THROW(InvalidStateException, "You need to call computeGramMatrix() before computing the eigenvalues!");
    eigenvalueSolver_.compute(covarianceMatrix_);
    if (eigenvalueSolver_.info() != Eigen::Success) {
      std::cout << "\nCovariance matrix:\n" << covarianceMatrix_ << std::endl;
      throw std::runtime_error("Solver for eigenvalues failed to converge!");
    }
    return;
  } // computeEigenvalues

  double getTotalVariation() const
  {
    return eigenvalueSolver_.eigenvalues().sum();
  }

  const int computeNeededNumberByPercentage(const int desiredPercentage,
                                            const int minimumSize = -1)
  {
    const EigenvalueVectorType& eigenvalues = eigenvalueSolver_.eigenvalues();

    neededComponents_ = 1;
    double totalVariation = getTotalVariation();

    // compute the percentage of the total variation that the first pc covers
    double currentPercentage = eigenvalues(eigenvalues.rows() - 1) / totalVariation;
    // while the desired percentage is not reached, enlarge the set of pc's that
    // will be returned
    while (currentPercentage < desiredPercentage && neededComponents_ < matrix_.cols()) {
      ++neededComponents_;
      currentPercentage += eigenvalues(eigenvalues.rows() - neededComponents_) / totalVariation;
    }

#ifdef RBDEBUG
    std::cout << "We need " << neededComponents_
              << " principal components to cover the desired total variation of "
              << desiredPercentage << " percent.\n";
#endif // ifdef RBDEBUG

    if (minimumSize != -1)
      // make shure a minimum amount of information is incorporated into the basis
      neededComponents_ = std::max(int(neededComponents_), minimumSize);

    return neededComponents_;
  } // computeNeededNumberByPercentage

  /** This computes the number of needed principal components to
   *  ensure the mean quadratic projection error to be below a given tolerance.
   */
  const int computeNeededNumberByError(const double tolerance)
  {
    const EigenvalueVectorType& eigenvalues = eigenvalueSolver_.eigenvalues();

    neededComponents_ = matrix_.cols();
    double errorMade = eigenvalues(0);
    int    i         = 1;
    while (errorMade < tolerance && i < eigenvalues.rows()) {
      --neededComponents_;
      errorMade += eigenvalues(i);
      ++i;
    }
    return neededComponents_;
  } // computeNeededNumberByError

  void computePrincipalComponents(EigenMatrixType& principalComponents)
  {
    if (eigenvalueSolver_.eigenvalues().rows() == 0)
      DUNE_THROW(InvalidStateException,
                 "You need to call computeEigenvalues() before computing the principal components!");
    if (neededComponents_ == 0)
      DUNE_THROW(InvalidStateException,
                 "You need to call computeNeededPrincipalComponents() before computing the principal components!");

    // "Convert the source data to z-scores"
    matrix_ /= covarianceMatrix_.diagonal().cwiseSqrt().sum();
    // "Project the z-scores of the data onto the new basis"
    principalComponents = matrix_ * eigenvalueSolver_.eigenvectors().rightCols(neededComponents_);
    return;
  } // computePrincipalComponents

private:
  EigenMatrixType      matrix_;
  EigenMatrixType      covarianceMatrix_;
  int                  neededComponents_;
  EigenvalueSolverType eigenvalueSolver_;
};
} // namespace Algorithms
} // namespace LA
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_LA_ALGORITHMS_PCA_HH__