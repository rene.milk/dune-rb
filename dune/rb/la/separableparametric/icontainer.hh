#ifndef ICONTAINER_HH_
#define ICONTAINER_HH_

#include <vector>
#include <string>

namespace Dune
{

namespace RB
{

namespace LA
{

namespace SeparableParametric
{

template< class ScalarImp, class ContainerImp >
class IContainer
{
public:
  typedef ScalarImp ScalarType;
  typedef ContainerImp MatrixType;
public:

  virtual ~IContainer ()
  {
  }

  virtual ScalarType coefficient (
      const unsigned int q, const Detailed::Parameter::Vector &param) const = 0;

  std::vector< ScalarType > coefficients (
      const Detailed::Parameter::Vector& param) const
  {
    std::vector< ScalarType > coeffs(this->numComponents());
    for (unsigned int i = 0; i < coeffs.size(); ++i)
    {
      coeffs[i] = coefficient(i, param);
    }
    return coeffs;
  }

  virtual const ContainerImp & component (const unsigned int q) const = 0;

  const std::vector< const ContainerImp * > components () const
  {
    std::vector< const ContainerImp * > comps(this->numComponents());
    for (unsigned int i = 0; i < comps.size(); ++i)
    {
      const ContainerImp & temp = component(i);
      comps[i] = &temp;
    }
    return comps;
  }

  virtual unsigned int numComponents () const = 0;

  virtual const ContainerImp* complete (
      const Detailed::Parameter::Vector & param) const = 0;

  virtual const std::string getIdentifier () const = 0;

  virtual std::vector< std::string > symbolicCoefficients () const = 0;

};
// class IContainer

template< class T >
class DefaultMatrixContainer: public IContainer< typename T::RangeFieldType,
    typename T::MatrixType >
{
public:
  typedef typename T::RangeFieldType ScalarType;
  typedef typename T::MatrixContainerType MatrixContainerType;
  typedef typename T::MatrixType MatrixType;
public:

  DefaultMatrixContainer ()
      : matrix_()
  {
  }

  virtual ~DefaultMatrixContainer ()
  {
    for (unsigned int i = 0; i < matrix_.size(); ++i)
    {
      if (matrix_[i] != NULL) delete matrix_[i];
    }
  }

  virtual const MatrixType* complete (
      const Detailed::Parameter::Vector & param) const
  {
    const std::vector< ScalarType > coeffs = this->coefficients(param);
    const std::vector< const MatrixType* > comps = this->components();
    MatrixType * res = new MatrixType;
    MatrixType & resRef = *res;
    resRef = *(comps[0]);
    resRef *= coeffs[0];
    for (unsigned int i = 1; i < comps.size(); ++i)
    {
      MatrixType temp(*comps[i]);
      temp *= coeffs[i];
      resRef += temp;
    }
    return res;
  }

  virtual const MatrixType & component (const unsigned int q) const
  {
    assert(q < this->numComponents());
    if (matrix_.empty())
      matrix_.resize(this->numComponents(), NULL);

    if (matrix_[q] == NULL) matrix_[q] = generate_component(q);

    return *(matrix_[q]);
  }

protected:

  virtual MatrixContainerType * generate_component (const unsigned int q) const = 0;

protected:
  // cached matrix
  mutable std::vector< MatrixContainerType * > matrix_;
};

template< class T >
class DefaultVectorContainer: public IContainer< typename T::RangeFieldType,
    typename T::VectorType >
{
public:
  typedef typename T::RangeFieldType ScalarType;
  typedef typename T::VectorContainerType VectorContainerType;
  typedef typename T::VectorType VectorType;
public:

  DefaultVectorContainer ()
      : vector_()
  {
  }

  virtual ~DefaultVectorContainer ()
  {
    for (unsigned int i = 0; i < vector_.size(); ++i)
    {
      if (vector_[i] != NULL) delete vector_[i];
    }
  }

  virtual const VectorType* complete (
      const Detailed::Parameter::Vector & param) const
  {
    const std::vector< ScalarType > coeffs = this->coefficients(param);
    const std::vector< const VectorType* > comps = this->components();
    VectorType * res = new VectorType;
    VectorType & resRef = *res;
    resRef = *(comps[0]);
    resRef *= coeffs[0];

    for (unsigned int i = 1; i < comps.size(); ++i)
    {
      VectorType temp;
      temp = *(comps[i]);
      temp *= coeffs[i];
      resRef += temp;
    }
    return res;
  }

  virtual const VectorType & component (const unsigned int q) const
  {
    assert(q < this->numComponents());
    if (vector_.empty())
      vector_.resize(this->numComponents(), NULL);

    if (vector_[q] == NULL)
      vector_[q] = generate_component(q);

    return vector_[q]->base();
  }

protected:

  virtual VectorContainerType * generate_component (const unsigned int q) const = 0;

protected:
  // cached vector (therefore mutable!)
  mutable std::vector< VectorContainerType * > vector_;
};

} // namespace SeparableParametric

} // namespace LA

} // namespace RB

} // namespace Dune

#endif /* ICONTAINER_HH_ */
