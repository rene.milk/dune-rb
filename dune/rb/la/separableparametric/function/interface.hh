//
// seperableparametricfunction.h
// dune-rb
//
// Created by Sven Kaulmann on 10.01.12.
// Copyright 2012. All rights reserved.
//

#ifndef DUNE_RB_SEPERABLEPARAMETRICFUNCTION
#define DUNE_RB_SEPERABLEPARAMETRICFUNCTION


namespace Dune {
namespace RB {
namespace LA {
namespace Separableparametric {
namespace Function {
/** Interface class for all seperable parametric functions.
 *
 *  A seperable parametric function is a function
 *  @f$f:X\times\mathcal{P}\rightarrow {R}^k@f$
 *  with a Hilbert space @f$X@f$ (spatial domain) and
 *  a open bounded set @f$\mathcal{P}@f$ (parameter domain) that can be written as
 *  @f[
 *    f(x,\mu)=\sum_{i=1}^N \theta_i(\mu)f_i(x)
 *  @f]
 *  with parameter dependent functions ("coefficients") @f$\theta_i@f$ and parameter independent functions
 *  ("components") @f$ f_i@f$.
 *
 *  @remark The coefficient is assumed to be double-valued!
 */
template< class DomainImpl, class RangeImpl, class ParameterDomainImpl >
class Interface
{
public:
  // ! The type of the spatial domain.
  typedef DomainImpl                     DomainType;
  // ! The type of the spatial range.
  typedef RangeImpl                      RangeType;
  // ! The type of the parameter domain.
  typedef ParameterDomainImpl            ParameterDomainType;
  // ! The field type in the range
  typedef typename RangeType::field_type RangeFieldType;

  /** Return the number of summands
   *
   *  @returns The number of summands in this function.
   */
  virtual const int size() const = 0;

  /** Evaluate a coefficient in a given point in the parameter domain.
   *
   *  @param[in] i  The number of the coefficient.
   *  @param[in] mu The point in parameter space where the coefficient shall be evaluated.
   *  @returns The evaluation of the i'th coefficient.
   */
  virtual const double coefficient(const int i, const ParameterDomainType& mu) const = 0;

  /** Supply the coefficient function as a string representation that can be read by
   * Dune::RB::Parameter::Function.
   *
   * Example: If size() of this is 2 and coefficient(0, mu) is sin(mu(0)) and coefficient(1, mu)
   * is mu(0)*mu(1), then symbolic coefficient should return a std::vector with entries
   * "sin(mu[0])" and "mu[0]*mu[1]" and getVariable"()" should return "mu".
   *
   * @returns Returns the symbolic coefficients as std::vector of std::string.
   */
  virtual const std::vector< std::string >& getSymbolicCoefficients() const = 0;

  /** Defines the string that represents the variable in the symbolic coefficients return by
   * getSymbolicCoefficients"()"
   *
   * @returns Returns a std::string containing the name of the variable in the expressions returned by
   * getSymbolicCoefficients"()"
   */
  virtual const std::string getVariable() const = 0;

  /** Evaluate a component in a given point in the spatial domain.
   *
   *  @param[in] i The number of the component.
   *  @param[in] x The point where the component shall be evaluated.
   *  @returns The evaluation of the desired component.
   */
  virtual const RangeType component(const int i, const DomainType& x) const = 0;

  /** Evaluate the whole function in a given spatial and parameter point.
   *
   *  This evaluates the sum
   *  @f[
   *  f(x,\mu)=\sum_{i=1}^N \theta_i(\mu)f_i(x)
   *  @f]
   *
   *  @param[in] x  The point in the spatial domain where the evaluation is desired.
   *  @param[in] mu The point in the parameter domain where the evaluation is desired.
   *  @returns Returns the evaluation of this function.
   */
  const RangeType evaluate(const DomainType& x, const ParameterDomainType& mu) const
  {
    RangeType ret(0.0);

    for (int i = 0; i != size(); ++i) {
      ret += component(i, x) * coefficient(i, mu);
    }
    return ret;
  } // evaluate

protected:
  // ParameterDomainType mu_;
};
}
}
}
} // namespace Dune
} // namespace RB

#endif // ifndef DUNE_RB_SEPERABLEPARAMETRICFUNCTION