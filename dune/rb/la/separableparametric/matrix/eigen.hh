#ifndef __DUNE_RB_LA_SEPARABLEPARAMETRIC_CONTAINER_EIGENBASE_HH__
#define __DUNE_RB_LA_SEPARABLEPARAMETRIC_CONTAINER_EIGENBASE_HH__

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>


#include <dune/common/parametertree.hh>

#include <dune/rb/parameter/function.hh>

namespace Dune {
namespace RB {
namespace LA {
namespace SeparableParametric {
namespace Container {
template< typename EigenMatrixImp >
class EigenBase
{
public:
  typedef EigenMatrixImp                                    ComponentType;

  typedef ComponentType                                     CompleteType;
  typedef Eigen::Matrix< ComponentType, Eigen::Dynamic, 1 > ComponentsVectorType;
  typedef Eigen::VectorXd                                   CoefficientsVectorType;

  EigenBase(const int rows, const int cols)
    : rows_(rows),
      cols_(cols),
      coefficientsFunction_(NULL)
  {}

  EigenBase()
    : rows_(0),
      cols_(0),
      coefficientsFunction_(NULL)
  {}

  template< class ParamType >
  double coefficient(const int q, const ParamType param) const;

  template< class ParamType >
  CoefficientsVectorType coefficients(const ParamType param) const;

  const ComponentType& component(const int q) const
  {
    return components_(q);
  }

  ComponentType& component(const int q)
  {
    return components_(q);
  }

  const ComponentsVectorType& components() const
  {
    return components_;
  }

  int numComponents() const
  {
    return components_.rows();
  }


  bool hasParameterindependentPart() const
  {
    return parameterindependentPart_.rows() != 0;
  }

  const ComponentType& parameterindependentPart() const
  {
    return parameterindependentPart_;
  }

  ComponentType& parameterindependentPart()
  {
    return parameterindependentPart_;
  }

  template< class ParameterType >
  const CompleteType& complete(const ParameterType param) const
  {
    assert((coefficientsFunction_ != NULL) && "You need to set the symbolicCoefficients first!");
    CoefficientsVectorType coeffs(numComponents());
    coefficientsFunction_->evaluate(param, coeffs);

    complete_ = CompleteType::Zero(rows_, cols_);
    for (int q = 0; q != numComponents(); ++q)
      complete_ += components_(q) * coeffs(q);

    if (hasParameterindependentPart())
      complete_ += parameterindependentPart_;

    return complete_;
  } // complete

  const std::vector< std::string >& getSymbolicCoefficients() const
  {
    assert((coefficientsFunction_ != NULL) && "You need to set the symbolicCoefficients first!");
    return coefficientsFunction_->expressions();
  }

  const std::string getVariable() const
  {
    return coefficientsFunction_->variable();
  }

  void setSymbolicCoefficients(std::vector< std::string > symbolicCoefficients,
                               const std::string variable = "mu")
  {
    if (coefficientsFunction_ != NULL)
      delete coefficientsFunction_;
    coefficientsFunction_ = new Dune::RB::Parameter::Function< double, 50, double, 50 >(variable, symbolicCoefficients);
    symbolicCoefficients.push_back(variable);
    symbolicCoefficients_ = Eigen::Map<Eigen::Matrix<std::string, Eigen::Dynamic, 1> >(&(symbolicCoefficients[0]),
                                                                                    symbolicCoefficients.size());
    return;
  }

  void setSymbolicCoefficients(const Dune::ParameterTree& coefficientsTree)
  {
    assert(0 && "If you want to use me, please implement saving and loading of the coefficientsTree!");
    
    if (coefficientsFunction_ != NULL)
      delete coefficientsFunction_;
    coefficientsFunction_ = new Dune::RB::Parameter::Function< double, 50, double, 50 >(coefficientsTree);
  }

  const int rows() const
  {
    return rows_;
  }

  const int cols() const
  {
    return cols_;
  }


  inline bool save(const std::string& path) const
  {
    bool allSaved = true;

    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath))
      std::cerr << "Path given to LA::Separable::Parametric::Container::save is not a directory, not saving!\n";
    boost::filesystem::create_directory(datapath);

    // first: write number of components and existence of parameterindependent part to file
    size_t        numComps       = numComponents();
    bool          paramIndepPart = hasParameterindependentPart();
    std::string filename = path + "/info.dat";
    std::ofstream out(filename.c_str(), std::ios::binary);
    if (!out.is_open()) {
      std::cerr << "Could not open info file in LA::Separable::Parametric::Container::save!\n";
      allSaved = false;
    } else {
      out.write(reinterpret_cast< char* >(&numComps), sizeof(numComps));
      out.write(reinterpret_cast< char* >(&paramIndepPart), sizeof(paramIndepPart));
      out.close();
    }
    // now: write components and parameter independent part
    for (int comp = 0; (comp != numComponents() && allSaved); ++comp) {
      filename = path + "/comp_" + boost::lexical_cast< std::string >(comp) + ".dat";
      allSaved = (allSaved && component(comp).saveToBinaryFile(filename));
    }
    if (hasParameterindependentPart() && allSaved) {
      filename = path + "/parameterindependent_part.dat";
      allSaved = (allSaved && parameterindependentPart_.saveToBinaryFile(filename));
    }
    if (allSaved) {
      filename = path + "/symbolic_coefficients.dat";     
      allSaved = (allSaved && symbolicCoefficients_.saveToTextFile(filename));
    }
    return allSaved;
  } // save

  inline bool load(const std::string& path)
  {
    bool                    allLoaded = true;
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath))
      std::cerr << "Path given to LA::Separable::Parametric::Container::load is not a directory, not loading!\n";

    // first: load number of components and existence of parameterindependent part
    size_t        numComps;
    bool          paramIndepPart;
    std::string filename = path + "/info.dat";
    std::ifstream in(filename.c_str(), std::ios::binary);
    if (!in.is_open()) {
      std::cerr << "Could not open info file in LA::Separable::Parametric::Container::load!\n";
      allLoaded = false;
    } else {
      in.read(reinterpret_cast< char* >(&numComps), sizeof(numComps));
      in.read(reinterpret_cast< char* >(&paramIndepPart), sizeof(paramIndepPart));
      in.close();
    }

    components_.resize(numComps, 1);
    for (int comp = 0; (comp != numComponents() && allLoaded); ++comp) {
      filename = path + "/comp_" + boost::lexical_cast< std::string >(comp) + ".dat";
      allLoaded = (allLoaded && component(comp).loadFromBinaryFile(filename));
      assert(component(0).rows()==component(comp).rows() && component(0).cols()==component(comp).cols()
             && "We only support equally sized matrices in all components at the moment!");
    }
      
    if (paramIndepPart && allLoaded) {
      filename = path + "/parameterindependent_part.dat";
      allLoaded = (allLoaded && parameterindependentPart_.loadFromBinaryFile(filename));
      assert(component(0).rows()==parameterindependentPart_.rows() 
             && component(0).cols()==parameterindependentPart_.cols()
             && "We only support equally sized matrices in all components at the moment!");

    }
    if (allLoaded) {
      filename = path + "/symbolic_coefficients.dat";
      allLoaded = (allLoaded && symbolicCoefficients_.loadFromTextFile(filename));    
      typedef Dune::RB::Parameter::Function< double, 50, double, 50 > ParamFuncT;
      if (coefficientsFunction_!=NULL)
        delete coefficientsFunction_;
      coefficientsFunction_ = new ParamFuncT(symbolicCoefficients_.tail(1).value(),
                                             std::vector<std::string>(symbolicCoefficients_.data(),
                                                                      symbolicCoefficients_.data()
                                                                      +symbolicCoefficients_.rows()-1));
    }
    if (allLoaded) {
      rows_ = components_(0).rows();
      cols_ = components_(0).cols();
    }
    return allLoaded;
  } // load

protected:
  int                                                      rows_;
  int                                                      cols_;
  ComponentsVectorType                                     components_;
  ComponentType                                            parameterindependentPart_;
  mutable CompleteType                                     complete_;
  Dune::RB::Parameter::Function< double, 50, double, 50 >* coefficientsFunction_;
  Eigen::Matrix<std::string, Eigen::Dynamic, 1>            symbolicCoefficients_;
}; // class Eigen


class EigenDenseMatrix
  : public EigenBase< Eigen::MatrixXd >
{
  typedef EigenBase< Eigen::MatrixXd > BaseType;

public:
  EigenDenseMatrix(const int rows, const int cols)
    : BaseType(rows, cols)
  {}

  EigenDenseMatrix()
  {}

  void reserve(const int numComponents, const bool parameterindependentPart = false)
  {
    BaseType::components_ = ComponentsVectorType::Constant(numComponents,
                                                           ComponentType::Zero(BaseType::rows_,
                                                                               BaseType::cols_));
    BaseType::complete_ = BaseType::ComponentType::Zero(BaseType::rows_, BaseType::cols_);
    if (parameterindependentPart)
      BaseType::parameterindependentPart_ = BaseType::ComponentType::Zero(BaseType::rows_, BaseType::cols_);
    return;
  } // reserve
};


class EigenDenseVector
  : public EigenBase< Eigen::VectorXd >
{
  typedef EigenBase< Eigen::VectorXd > BaseType;

public:
  EigenDenseVector(const int rows)
    : BaseType(rows, 1)
  {}

  EigenDenseVector()
  {}

  void reserve(const int numComponents, const bool parameterindependentPart = false)
  {
    BaseType::components_
      = ComponentsVectorType::Constant(numComponents,
                                       ComponentType::Zero(BaseType::rows_, BaseType::cols_));
    BaseType::complete_ = BaseType::ComponentType::Zero(BaseType::rows_);
    if (parameterindependentPart)
      BaseType::parameterindependentPart_ = BaseType::ComponentType::Zero(BaseType::rows_);
    return;
  } // reserve
};

class EigenSparseMatrix
  : public EigenBase< Eigen::SparseMatrix< double > >
{
  typedef EigenBase< Eigen::SparseMatrix< double > > BaseType;

public:
  EigenSparseMatrix(const int rows, const int cols)
    : BaseType(rows, cols)
  {}

  void reserve(const int numComponents, const Eigen::VectorXi& nnzVector, const bool parameterindependentPart = false)
  {
    nnzVector_ = nnzVector;

    BaseType::components_ = BaseType::ComponentsVectorType::Constant(numComponents,
                                                                     ComponentType(BaseType::rows_, BaseType::cols_));
    for (int q = 0; q != BaseType::numComponents(); ++q)
      BaseType::components_(q).reserve(nnzVector);
    BaseType::complete_.resize(BaseType::rows_, BaseType::cols_);
    BaseType::complete_.reserve(nnzVector);
    if (parameterindependentPart) {
      BaseType::parameterindependentPart_.resize(BaseType::rows_, BaseType::cols_);
      BaseType::parameterindependentPart_.reserve(nnzVector);
    }

    return;
  } // reserve

  template< class ParameterType >
  const CompleteType& complete(const ParameterType param) const
  {
    CoefficientsVectorType coeffs(BaseType::numComponents());

    BaseType::coefficientsFunction_->evaluate(param, coeffs);

    BaseType::complete_.resize(BaseType::rows_, BaseType::cols_);
    BaseType::complete_.reserve(nnzVector_);
    for (int q = 0; q != BaseType::numComponents(); ++q)
      BaseType::complete_ += BaseType::components_(q) * coeffs(q);

    if (BaseType::hasParameterindependentPart())
      BaseType::complete_ += BaseType::parameterindependentPart_;

    BaseType::complete_.makeCompressed();

    return BaseType::complete_;
  } // complete

private:
  Eigen::VectorXi nnzVector_;
};
} // namespace Container
} // namespace SeparableParametric
} // namespace LA
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_LA_SEPARABLEPARAMETRIC_CONTAINER_EIGENBASE_HH__