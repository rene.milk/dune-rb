
namespace Dune{

namespace RB{

namespace LA{

namespace SeparableParametric{

namespace Matrix{

template< class ContainerType, class VectorType >
class Interface{
public:

  typedef ContainerType
    ComponentType;

  template< class ParamType >
  double coefficient(const int q, const ParamType param) const

  template< class ParamType >
  const VectorType< double >& coefficients(const ParamType param) const

  const ComponentType& component(const int q) const

  const VectorType< ComponentType >& components() const

  int numComponents() const
  
  bool hasParameterindependentPart() const
  
  const ComponentType& parameterIndependentPart() const

  template< class ParamType >
  const ComponentType& complete(const ParamType param) const

  const VectorType< std::string >& symbolicCoefficients() const

}; // class Interface

} // namespace Matrix

} // namespace SeparableParametric

} // namespace LA

} // namespace RB

} // namespace Dune