#ifndef __DUNE_RB_OFFLINE_MAPPER_COARSE_HH__
#define __DUNE_RB_OFFLINE_MAPPER_COARSE_HH__

namespace Dune
{
namespace RB
{
namespace Offline {

namespace Mapper {

template< class DofMatrixListImp, class GridImp >
class Coarse
{
public:
    typedef DofMatrixListImp DofMatrixListType;
    typedef GridImp          GridType;

public:
    Coarse(const DofMatrixListType &dofMatrixList,
           const GridType &grid)
         : dofMatrixList_(dofMatrixList),
           grid_(grid)
    {}

    int toGlobal( const int element, const int localDof ) const
    {
            // find the coarse cell where this entity lives
        int globalDof = 0;
        for (int i=0; i<element; ++i)
            globalDof += dofMatrixList_[i].cols();
        globalDof += localDof;
        return globalDof;
    }

    int size(const int element) const
    {
        return dofMatrixList_.component(element).cols();
    }

    int size () const
    {
        int size=0;
        for (int element = 0; element<grid_.size(); ++element) {
            size+=this->size(element);
        }
        return size;
    }

    int maxSize() const
    {
        int size=0;
        for (int element = 0; element<grid_.size(); ++element) {
            int localSize = this->size(element);
            if (localSize>size)
                size=localSize;
        }
        return size;
    }

private:
    const DofMatrixListType& dofMatrixList_;
    const GridType& grid_;


};
}
}
}
}

#endif    // __DUNE_RB_OFFLINE_MAPPER_COARSE_HH__