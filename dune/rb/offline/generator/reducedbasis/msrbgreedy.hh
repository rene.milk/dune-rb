#ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_MSRBGREEDY_HH__
#define __DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_MSRBGREEDY_HH__

#include <string>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/lexical_cast.hpp>

#include <dune/rb/reduced/parameter/sample/interface.hh>
#include <dune/rb/la/algorithms/gramschmidt.hh>
#include <dune/rb/la/algorithms/pca.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace ReducedBasis {
template< class OfflineSpaceImp, class ModelImp, class DISCOImp,
          class RESGOImp, class ESGOImp, class CoarseGridImp, class LocalizerImp >
class MSRBGreedy
{
public:
  typedef OfflineSpaceImp                                   OfflineSpaceType;
  typedef ModelImp                                          ModelType;
  typedef DISCOImp                                          DISCOType;
  // Reduced solver connector
  typedef RESGOImp                                          RESGOType;
  // Estimator connector
  typedef ESGOImp                                           ESGOType;
  typedef CoarseGridImp                                     CoarseGridType;
  typedef LocalizerImp                                      LocalizerType;

  typedef typename RESGOType::RescoType                     RescoType;
  typedef typename ESGOType::EscoType                       EscoType;
  typedef typename DISCOType::DofVectorType                 DetailedDofVectorType;
  typedef typename RESGOType::RescoType::DofVectorType      ReducedDofVectorType;
  typedef typename LocalizerType::LocalVectorType           LocalVectorType;
  typedef typename LocalizerType::LocalMatrixType           LocalMatrixType;
  typedef typename ModelType::ParameterType                 ParameterType;
  typedef typename OfflineSpaceType::DofMatrixType          DofMatrixType;
  typedef Eigen::Matrix< DofMatrixType, Eigen::Dynamic, 1 > DofMatrixVectorType;

  MSRBGreedy(OfflineSpaceType& space,
             const ModelType& model,
             DISCOType& disco,
             RESGOType& resgo,
             ESGOType& esgo,
             const CoarseGridType& coarseGrid,
             LocalizerType& localizer)
    : space_(space),
      model_(model),
      disco_(disco),
      resgo_(resgo),
      esgo_(esgo),
      coarseGrid_(coarseGrid),
      localizer_(localizer),
      snapshots_(coarseGrid.size())
  {}

  void init(const ParameterType& param)
  {
    disco_.init();

    // solve for given parameter
    DetailedDofVectorType vec;

    disco_.solve(param, vec);

    // initialize space with computed solution
    initFromVec(vec);

    std::cout << "Intialized reduced basis using parameter = [ " << param.transpose() << " ]\n";

    resgo_.update();
    esgo_.update();

    initialized_ = true;

    return;
  } // init

  template< class ParameterSampleType >
  const boost::uuids::uuid generate(const ParameterSampleType& paramSample)
  {
    if (!initialized_)
      DUNE_THROW(InvalidStateException, "Please call MSRBGreedy::init before calling MSRBGreedy::generate!");

    const int    paramSampleSize = paramSample.size();
    const double desiredError    = Dune::Parameter::getValue< double >("podgreedy.desiredError", 1e-6);
    const double maxRuntime      = Dune::Parameter::getValue< int >("podgreedy.maxRuntime", 7200);
    const int    desiredSize     = Dune::Parameter::getValue< int >("podgreedy.desiredSize", 10);

    boost::uuids::uuid uid                   = boost::uuids::random_generator() ();
    std::string        uidString             = boost::lexical_cast< std::string >(uid);
    bool               breakOnTime           = false;
    bool               breakOnError          = false;
    bool               breakOnSize           = false;
    bool               breakOnEmptyExtension = false;

    // prepare file for output
    // this generates a unique filename
    std::string filename = uidString + "_rbgeneration.log";
    std::ofstream file(filename.c_str());

    assert(file);
    file << "POD-Greedy Basis Generation\n"
         << "---------------------------\n\n"
         << "For params see matching parameter backup file!\n"
         << "ExtensionStep\tBasisSize\tBasisSizePerSD\tMaxError"
         << "\tMinError\tMeanError\tStdDeviation\tOpAssTime\tEstAssTime\tTrainingTime\n";

    // backup used parameters
    Dune::Parameter::write(uidString + "_parameters.log");

    clock_t generationstartTime = clock();

    std::multiset< double > operatorAssemblingTimes;
    std::multiset< double > estimatorAssemblingTimes;
    std::multiset< double > trainingTimes;

    while ((!breakOnTime) && (!breakOnError) && (!breakOnSize) && (!breakOnEmptyExtension)) {
      std::multimap< double, ParameterType > errorVec;
      // assemble reduced operator, returns used time
      RescoType reducedSolverConnector = resgo_.generate();
      // assemble error estimator
      EscoType estimatorConnector = esgo_.generate();

      std::cout << "Performing reduced simulation and evaluating error"
                << " estimator for all parameters in the training set!\n";

      clock_t trainingstartTime = clock();
      for (int k = 0; k < paramSampleSize; ++k) {
        ParameterType        param  = paramSample.get(k);
        ReducedDofVectorType redVec = reducedSolverConnector.solve(param);
        double               error  = estimatorConnector.estimate(param, redVec);
        
//
//
//        // =================== Compute true error ============================
//        DetailedDofVectorType trueSolution;
//        disco_.solve(param, trueSolution);
//        DetailedDofVectorType difference = space_.getDofMatrix(0) * redVec - trueSolution;
//        double trueErrorSq = difference.transpose() * disco_.getSystemMatrix().complete(esgo_.muBar()) * difference;
//        double lowerBound = estimatorConnector.lowerBound(param, redVec);
//        if (sqrt(trueErrorSq)>error || lowerBound > sqrt(trueErrorSq))
//          std::cout << "\033[1;31m";
//        std::cout << "Error: " << lowerBound
//                  << " <= " << sqrt(trueErrorSq)
//                  << " <= " << error
//                  << " For parameter " << param.transpose();
//        std::cout << " Solution: " << redVec.transpose();
//        if (sqrt(trueErrorSq)>error || lowerBound > sqrt(trueErrorSq)) {
//          std::cout << "\033[0m";          
//        }
//        std::cout << std::endl;
//
//        // =================== Compute true error ============================
        
        errorVec.insert(std::pair< double, ParameterType >(error, param));
        
      }


      // save time needed for training
      double trainingTime = (clock() - trainingstartTime) / CLOCKS_PER_SEC;
      trainingTimes.insert(trainingTime);

      // if desired error was reached: break
      double        maximumError      = errorVec.rbegin()->first;
      ParameterType maximumErrorParam = errorVec.rbegin()->second;
      if (maximumError < desiredError) {
        breakOnError = true;
        std::cout << "Reached desired accuracy, will break greedy now!\n";
      }
      // else extend the basis
      else {
        std::cout << "Maximum error " << errorVec.rbegin()->first
                  << " was reached for mu = ["
                  << maximumErrorParam.transpose() << "]\n";
        // extend the space
        bool wasExtended = extend(maximumErrorParam);
        if (wasExtended) {
          std::cout << "Extended reduced basis to size " << space_.size()
                    << " using parameter = [ " << maximumErrorParam.transpose() << " ]\n";
        } else {
          breakOnEmptyExtension = true;
          std::cout << "Extension was empty, will break greedy now!\n";
        }
      }

      // update reduced solver and estimator generator
      resgo_.update();
      esgo_.update();

      // if the desired maximum size was reached: break
      if (space_.size() >= desiredSize) {
        std::cout << "Reached desired size of RB space, will break greedy now!\n";
        breakOnSize = true;
      }
      // if the desired maximum runtime was reached: break
      double runTime = (clock() - generationstartTime) / CLOCKS_PER_SEC;
      if (runTime > maxRuntime) {
        std::cout << "Time limit for basis construction reached, will break greedy now!\n";
        breakOnTime = true;
      }
    }

    return uid;
  } // generate


  void compressInformation()
  {
    for (unsigned int element = 0; element < coarseGrid_.size(); ++element) {
      const DofMatrixType& elementBasis     = snapshots_(element);
      const DofMatrixType& elementBasisFull = localizer_.getFullMatrixRows(elementBasis, element);

      LA::Algorithms::PCA< DofMatrixType > pca(elementBasisFull);
      std::cout << "Subdomain " << element << ". Size before: " << elementBasisFull.cols();
      pca.computeCovarianceMatrix();
      pca.computeEigenvalues();
      // pca.computeNeededNumberByPercentage(Parameter::getValue<double>("pca.desiredPercentage"));
      pca.computeNeededNumberByError(Dune::Parameter::getValue< double >("podgreedy.pca.eps"));
      DofMatrixType principalComponents;
      pca.computePrincipalComponents(principalComponents);
      std::cout << ". Size after: " << principalComponents.cols() << std::endl;
      space_.setDofMatrix(element, localizer_.localizeMatrixRows(principalComponents, element));
    }

    return;
  } // compressInformation

private:
  void initFromVec(const DetailedDofVectorType& vec)
  {
    assert(space_.size() == 0);

    for (unsigned int element = 0; element != coarseGrid_.size(); element++) {
      // localize dof vector to element
      LocalVectorType& localVec = localizer_.localizeVector(vec, element);

      // save snapshot for later usage
      snapshots_(element)        = DofMatrixType::Zero(localVec.rows(), 1);
      snapshots_(element).col(0) = localVec;

      LocalMatrixType& localScalarProduct
        = localizer_.localizeMatrixRows(disco_.getSystemMatrix().complete(esgo_.muBar()), element);
      // normalize
      const double normSquared = localVec.transpose() * localScalarProduct * localVec;
      assert(normSquared > 0.0);
      localVec /= sqrt(normSquared);
      // add dof vector to space
      space_.addDofVector(element, localVec);
    }

    return;
  } // init


  bool extend(const ParameterType& param)
  {
    bool extended = false;

    DetailedDofVectorType extensionVec;

    disco_.solve(param, extensionVec);

    for (int element = 0; element != coarseGrid_.size(); element++) {
      // localize dof vector to element
      LocalVectorType localVec = localizer_.localizeVector(extensionVec, element);

      LocalMatrixType& localScalarProduct
        = localizer_.localizeMatrixRows(disco_.getSystemMatrix().complete(esgo_.muBar()), element);
      
      // orthonomalize
      LA::Algorithms::gramSchmidt(localVec, space_.getDofMatrix(element), localScalarProduct);
      double squaredLocalVecNorm = localVec.transpose() * localScalarProduct * localVec;
      assert(squaredLocalVecNorm >= 0.0);

      // add dof vector to space
      if (squaredLocalVecNorm > 1e-12) {
        localVec /= sqrt(squaredLocalVecNorm);
        space_.addDofVector(element, localVec);

        // save snapshot for later usage
        snapshots_(element).conservativeResize(Eigen::NoChange, snapshots_(element).cols() + 1);
        snapshots_(element).rightCols(1) = localVec;
        extended                         = true;
      }
    }
    return extended;
  } // extend

  OfflineSpaceType&     space_;
  const ModelType&      model_;
  DISCOType&            disco_;
  RESGOType&            resgo_;
  ESGOType&             esgo_;
  const CoarseGridType& coarseGrid_;
  LocalizerType&        localizer_;
  bool                  initialized_;
  DofMatrixVectorType   snapshots_;
}; // class MSRBGreedy
} // namespace ReducedBasis
} // namespace Generator
} // namespace Offline
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_MSRBGREEDY_HH__