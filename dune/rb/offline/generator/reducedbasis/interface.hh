
namespace Dune{

namespace RB{

namespace Offline{

namespace Generator{

namespace ReducedBasis{

template< class SpaceImp >
class Interface
{
public:
  typedef SpaceImp
    SpaceType;

  const SpaceType& space() const

  void init()

  void extend()

}; // class Interface

} // namespace ReducedBasis

} // namespace Generator

} // namespace Offline

} // namespace RB

} // namespace Dune
