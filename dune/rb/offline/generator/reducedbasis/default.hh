#ifndef DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_DEFAULT_HH
#define DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_DEFAULT_HH

#ifdef HAVE_CMAKE_CONFIG
  #include "cmake_config.h"
#elif defined (HAVE_CONFIG_H)
  #include <config.h>
#endif // ifdef HAVE_CMAKE_CONFIG

#include <string>
#include <vector>
#include <algorithm>

#include <Eigen/Core>

#include <dune/common/static_assert.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>

#include <dune/stuff/common/string.hh>
#include <dune/stuff/la/container/eigen.hh>
#include <dune/stuff/la/algorithm/normalize.hh>
#include <dune/stuff/la/algorithm/gramschmidt.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace Reducedbasis {

template< class DetailedSolverConnectorImp >
class Default
{
public:
  typedef DetailedSolverConnectorImp                                  DetailedSolverConnectorType;
  typedef typename DetailedSolverConnectorType::VectorType            VectorType;
  typedef typename VectorType::ElementType                            ElementType;
  typedef typename DetailedSolverConnectorType::ModelType::ParamType  ParamType;
  typedef Dune::Stuff::LA::Container::EigenDenseMatrix< ElementType > BasisMatrixType;

private:
  typedef typename DetailedSolverConnectorType::MatrixType            ScalarPoductType;

public:
  Default(const DetailedSolverConnectorType& _detailedSolverConnector,
          const ElementType _epsilon = 1e-10)
    : detailedSolverConnector_(_detailedSolverConnector)
    , epsilon_(_epsilon)
    , initialized_(false)
  {}

  void init(const ParamType& mu,
            const ParamType& scalarProductMu,
            const std::string scalarProductType,
            const std::string prefix = "",
            std::ostream& out = Dune::Stuff::Common::Logger().debug())
  {
    if (initialized_)
      DUNE_THROW(Dune::InvalidStateException, "\nERROR: only call init() once!");
    out << prefix << "computing scalar product... " << std::flush;
    Dune::Timer timer;
    scalarProduct_ = detailedSolverConnector_.getScalarProduct(scalarProductType, scalarProductMu);
    out << "done (took " << timer.elapsed() << "s)" << std::endl;
    timer.reset();
    out << prefix << "solving detailed problem... " << std::flush;
    tmpVector_ = detailedSolverConnector_.createVector();
    detailedSolverConnector_.solve(mu, tmpVector_, prefix + "  ", Dune::Stuff::Common::Logger().devnull());
    out << "done (took " << timer.elapsed() << "s)" << std::endl;
    out << prefix << "normalizing solution... " << std::flush;
    timer.reset();
    const bool success = Dune::Stuff::LA::Algorithm::normalize(*scalarProduct_, *tmpVector_, epsilon_);
    if (success) {
      out << "done (took " << timer.elapsed() << "s)" << std::endl;
      out << prefix << "initializing basis... " << std::flush;
      timer.reset();
      basisMatrix_ = Dune::shared_ptr< BasisMatrixType >(new BasisMatrixType(*tmpVector_));
      out << "done (took " << timer.elapsed() << "s)" << std::endl;
      previousParams_.push_back(mu);
      initialized_ = true;
    } else
      DUNE_THROW(Dune::InvalidStateException,
                 "\n"
                 << Dune::Stuff::Common::colorStringRed("ERROR:")
                 << " solutions norm is below " << epsilon_ << "!");
  } // void init(...)

  void extend(const ParamType& mu,
              const std::string prefix = "",
              std::ostream& out = Dune::Stuff::Common::Logger().debug())
  {
    if (!initialized_)
      DUNE_THROW(Dune::InvalidStateException, "\nERROR: please call init() before calling extend()!");
    if (std::find(previousParams_.begin(), previousParams_.end(), mu) != previousParams_.end())
      out << Dune::Stuff::Common::colorString("WARNING:")
          << " doing nothing, because [" << mu.transpose() << "] has already been added!" << std::endl;
    else {
      out << prefix << "solving detailed problem... " << std::flush;
      Dune::Timer timer;
      detailedSolverConnector_.solve(mu, tmpVector_, prefix + "  ", Dune::Stuff::Common::Logger().devnull());
      out << "done (took " << timer.elapsed() << "s)" << std::endl;
      out << prefix << "orthonormalizing solution... " << std::flush;
      timer.reset();
      const bool success = Dune::Stuff::LA::Algorithm::gramSchmidt(*scalarProduct_, *basisMatrix_, *tmpVector_);
      if (success) {
        out << "done (took " << timer.elapsed() << "s)" << std::endl;
        out << prefix << "adding to basis... " << std::flush;
        timer.reset();
        basisMatrix_->backend().conservativeResize(::Eigen::NoChange_t(), basisMatrix_->cols() + 1);
        basisMatrix_->backend().rightCols(1) = tmpVector_->backend();
        out << "done (took " << timer.elapsed() << "s)" << std::endl;
      } else {
        out << "failed (norm is below " << epsilon_ << ")" << std::endl;
      }
      previousParams_.push_back(mu);
    }
  } // void extend(...)

  Dune::shared_ptr< BasisMatrixType > basisMatrix()
  {
    if (!initialized_)
      DUNE_THROW(Dune::InvalidStateException, "\nERROR: please call init() before calling basisMatrix()!");
    return basisMatrix_;
  }

private:
  const DetailedSolverConnectorType& detailedSolverConnector_;
  const ElementType epsilon_;
  bool initialized_;
  std::vector< ParamType > previousParams_;
  Dune::shared_ptr< ScalarPoductType > scalarProduct_;
  Dune::shared_ptr< BasisMatrixType > basisMatrix_;
  Dune::shared_ptr< VectorType > tmpVector_;
}; // class Default

} // namespace Reducedbasis
} // namespace Generator
} // namespace Offline
} // namespace RB
} // namespace Dune

#endif // DUNE_RB_OFFLINE_GENERATOR_REDUCEDBASIS_DEFAULT_HH
