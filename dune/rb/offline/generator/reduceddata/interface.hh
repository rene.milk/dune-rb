#ifndef DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_INTERFACE_HH__
#define DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_INTERFACE_HH__

#include <vector>
#include <string>

namespace Dune
{

namespace RB
{

namespace Offline
{

namespace Generator
{

namespace ReducedData
{

template< class MatrixTraits, class Imp >
class Interface
{
public:
  typedef typename MatrixTraits :: MatrixType MatrixType;
  typedef std::vector<MatrixType *> MatrixListType;
  typedef typename MatrixTraits :: VectorType VectorType;
  typedef std::vector<VectorType *> VectorListType;
public:

  virtual ~Interface ()
  {
  }

  void generate ()
  {
    asImp().generate();
  }

  void update (int start, int end)
  {
    asImp().update(start, end);
  }

  MatrixListType & getOfflineMatrix (std::string id)
  {
    return asImp().getOfflineMatrix(id);
  }

  VectorListType & getOfflineVector (std::string id)
  {
    return asImp().getOfflineVector(id);
  }

private:

  Imp & asImp ()
  {
    return static_cast< Imp& >(*this);
  }

  const Imp & asImp () const
  {
    return static_cast< const Imp& >(*this);
  }
};
// class Interface

}// namespace ReducedData

} // namespace Generator

} // namespace Offline

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_INTERFACE_HH__
