/*
 * linevol.hh
 *
 *  Created on: 17.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_LINEVOL_HH_
#define DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_LINEVOL_HH_

#include "interface.hh"
#include <dune/rb/offline/space/interface.hh>
#include <dune/rb/offline/space/rbdefault.hh>

#include <map>
#include <string>
#include <vector>
#include <dune/rb/detailed/solver/connector/interface.hh>


namespace Dune
{
namespace RB
{
namespace Offline
{
namespace Generator
{
namespace ReducedData
{

/*
 *
 */
template< typename DetailedConnectorImp >
class LinEvol: public Interface< Offline::Space::EigenMatrixTraits,
    LinEvol< DetailedConnectorImp > >
{
private:
  typedef Space::EigenMatrixTraits MatrixTraits;
  typedef Interface< MatrixTraits, LinEvol > BaseType;
  typedef typename DetailedConnectorImp :: BasicTraits DetailedTraits;
public:
  typedef Detailed::Solver::Connector::Interface< DetailedTraits,
      DetailedConnectorImp > DetailedConnectorType;
  typedef Offline::Space::RBDefault OfflineSpaceType;
  typedef typename MatrixTraits::MatrixType MatrixType;
  typedef typename MatrixTraits::VectorType VectorType;
  typedef MatrixTraits BasicTraits;
  typedef typename BaseType::MatrixListType MatrixListType;
  typedef typename BaseType::VectorListType VectorListType;
public:
  LinEvol (const DetailedConnectorType & connector,
           const OfflineSpaceType & space);

  virtual ~LinEvol ();

  virtual typename LinEvol< DetailedConnectorImp >::MatrixListType & getOfflineMatrix (
      std::string id) const;

  virtual void update (int start, int end);

  virtual int getN ();

private:

  const MatrixType & getOperatorEvaluations (const std::string & opId, int i) const;
  std::string isOperator (const std::string & opCandidate) const;
  void splitInnerProduct (const std::string str, std::string & first,
                          std::string & second, std::string & W) const;
  unsigned int getNumberOfComponents (const std::string & idstring) const;

private:
  mutable std::map< std::string, MatrixListType > cache_;
  const DetailedConnectorType & connector_;
  const OfflineSpaceType & space_;
  int N_;
};

} /* namespace ReducedData */
} /* namespace Generator */
} /* namespace Offline */
} /* namespace RB */
}

/* namespace Dune */
#endif /* DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_LINEVOL_HH_ */
