
namespace Dune{

namespace RB{

namespace Offline{

namespace Generator{

namespace ReducedData{

template< class EstimatorImp >
class Estimator
{
public:
  typedef EstimatorImp
    ObjectType;

    ObjectType& generate()

}; // class Estimator

} // namespace ReducedData

} // namespace Generator

} // namespace Offline

} // namespace RB

} // namespace Dune
