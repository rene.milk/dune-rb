#ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_ESTIMATOR_RESIDUAL_HH__
#define __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_ESTIMATOR_RESIDUAL_HH__

#include <dune/rb/reduced/estimator/connector/residual.hh>

#include <boost/filesystem/operations.hpp>
#include <boost/timer/timer.hpp>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace ReducedData {
namespace Estimator {
template< class ModelImp, class OfflineSpaceImp, class DetailedSolverConnectorImp, class LocalizerImp >
class Residual
{
public:
  typedef ModelImp        ModelType;
  typedef OfflineSpaceImp OfflineSpaceType;
  typedef DetailedSolverConnectorImp
  DetailedSolverConnectorType;
  typedef LocalizerImp                      LocalizerType;

  typedef typename ModelType::ParameterType ParameterType;
  typedef typename DetailedSolverConnectorType::MatrixType::CompleteType
  SystemMatrixCompleteType;
  // typedef Eigen::BiCGSTAB< SystemMatrixCompleteType > SolverType;
  typedef Eigen::BiCGSTAB< SystemMatrixCompleteType, Eigen::IncompleteLUT< double > > SolverType;
  typedef typename OfflineSpaceType::DofMatrixType                                    DofMatrixType;
  typedef typename OfflineSpaceType::DofVectorType                                    DofVectorType;
  typedef DofMatrixType
  RieszRepresentantsListType;
  typedef Reduced::Estimator::Connector::ResidualConnector< DofVectorType, DofMatrixType, ModelType > EscoType;

  Residual(const ModelType& model,
           const OfflineSpaceType& offlineSpace,
           const DetailedSolverConnectorType& disco,
           LocalizerType& localizer)
    : model_(model),
      offlineSpace_(offlineSpace),
      disco_(disco),
      localizer_(localizer),
      muBar_((model.maxParameter() - model.minParameter()) / 2),
      initialized_(false),
      subdomainBasisSizes_(DofVectorType::Zero(offlineSpace.grid().size())),
      esco_(NULL)
  {}

  ~Residual()
  {
    if (esco_ != 0)
      delete esco_;
  }

  void update()
  {
    // if error estimator was not initialized so far, prepare the solver and compute the representants for Lh
    if (!initialized_) {
      /* solve for all riesz representants where the right hand sides are the right hand side parts of the
       *  high dimensional equation*/
      solveLh();

      initialized_ = true;

      // compute the new riesz representants
      solveBh();
      // precompute G
      computeGFull();
    } else {
      // compute the new riesz representants
      solveBh();
      // precompute G
      computeGNew();
    }
    return;
  } // update

  EscoType& generate()
  {
    if (esco_!=0)
      delete esco_;
    esco_ = new EscoType(G_, muBar_, subdomainBasisSizes_, model_);
    return *esco_;
  } // generate

  ParameterType muBar() const
  {
    return muBar_;
  }

private:
  const SystemMatrixCompleteType& systemMatrix()
  {
    if ((systemMatrix_.cols() == 0) && (systemMatrix_.rows() == 0))
      // copy the system matrix. Sadly, this is neccessary if we want to compute the preconditioner only once.
      systemMatrix_ = disco_.getSystemMatrix().complete(muBar_);
    assert(systemMatrix_.cols() != 0 && systemMatrix_.rows() != 0 && "System matrix is not set correctly!");
    return systemMatrix_;
  } // systemMatrix

  void solveLh()
  {
    // timing for this routine
//    boost::timer::auto_cpu_timer timer(2, "(Solve for Lh: %w) ");

    double dropTol    = Dune::Parameter::getValue< double >("residualestimator.solver.preconditioner.dropTol", 1e-4);
    int    fillFactor = Dune::Parameter::getValue< int >("residualestimator.solver.preconditioner.fillFactor", 20);
    double solverEps  = Dune::Parameter::getValue< double >("residualestimator.solver.eps", 1.0e-12);

    rieszRepresentants_ = RieszRepresentantsListType::Zero(disco_.getRightHandSide().rows(),
                                                           1 + disco_.getRightHandSide().numComponents());

    {
      const DofVectorType& rightHandSide = disco_.getRightHandSide().parameterindependentPart();

      DofVectorType solution;

      SolverType solver;
      solver.preconditioner().setDroptol(dropTol);
      solver.preconditioner().setFillfactor(fillFactor);
      solver.setTolerance(solverEps);
      SystemMatrixCompleteType sysCopy = systemMatrix();
      solver.compute(sysCopy);
      solution                   = solver.solve(rightHandSide);
      rieszRepresentants_.col(0) = solution;
    }


#pragma omp parallel for
    for (int i = 0; i < disco_.getRightHandSide().numComponents(); ++i) {
      const DofVectorType& rightHandSide = disco_.getRightHandSide().component(i);

      DofVectorType solution;

      SolverType solver;
      solver.preconditioner().setDroptol(dropTol);
      solver.preconditioner().setFillfactor(fillFactor);
      solver.setTolerance(solverEps);
      SystemMatrixCompleteType sysCopy = systemMatrix();
      solver.compute(sysCopy);
      solution                       = solver.solve(rightHandSide);
      rieszRepresentants_.col(1 + i) = solution;
    }
    return;
  }   /* solveLh */

  void solveBh()
  {
    boost::timer::auto_cpu_timer timer;

    const int QBh      = disco_.getSystemMatrix().numComponents();
    const int sizeOfTh = offlineSpace_.grid().size();

    int oldSize         = rieszRepresentants_.cols();
    int numNewFunctions = 0;

    for (int element = 0; element < sizeOfTh; ++element) {
      numNewFunctions += offlineSpace_.size(element) - subdomainBasisSizes_(element);
    }

    // enlarge the matrix holding the riesz representants and get a reference to the new column
    rieszRepresentants_.conservativeResize(Eigen::NoChange, oldSize + numNewFunctions * (QBh + 1));

    double dropTol    = Dune::Parameter::getValue< double >("residualestimator.solver.preconditioner.dropTol", 1e-4);
    int    fillFactor = Dune::Parameter::getValue< int >("residualestimator.solver.preconditioner.fillFactor", 20);
    double solverEps  = Dune::Parameter::getValue< double >("residualestimator.solver.eps", 1.0e-12);

#pragma omp parallel for
    for (int k = 0; k < sizeOfTh * (QBh + 1); ++k) {
      const int element = std::floor(k / (QBh + 1));
      const int i       = k - element * (QBh + 1);

      const DofMatrixType& elementBasis          = offlineSpace_.getDofMatrix(element);
      const int            numLocalBaseFunctions = offlineSpace_.size(element);
      if (numLocalBaseFunctions > subdomainBasisSizes_(element)) {
        int elementsUpdateBefore = 0;
        for (int j = 0; j < element; ++j)
          if (offlineSpace_.size(j) > subdomainBasisSizes_(j))
            ++elementsUpdateBefore;

        // for the time being, we assume, that at most one base function is added in each step
        assert(numLocalBaseFunctions == subdomainBasisSizes_(element) + 1);

        int baseFunc = numLocalBaseFunctions - 1;

        const DofVectorType baseFunctionLocalized = elementBasis.col(baseFunc);
        DofVectorType       baseFunctionFull;
        localizer_.getFullVector(baseFunctionLocalized, element, baseFunctionFull);

        DofVectorType rightHandSide;
        if (i == 0)
          rightHandSide = disco_.getSystemMatrix().parameterindependentPart() * baseFunctionFull;
        else
          rightHandSide = disco_.getSystemMatrix().component(i - 1) * baseFunctionFull;

        // solve
        SolverType solver;
        solver.preconditioner().setDroptol(dropTol);
        solver.preconditioner().setFillfactor(fillFactor);
        solver.setTolerance(solverEps);
        SystemMatrixCompleteType sysCopy = systemMatrix();
        solver.compute(sysCopy);
        DofVectorType solution = solver.solve(rightHandSide);

        rieszRepresentants_.col(oldSize + (elementsUpdateBefore * QBh) + i) = solution;
      }
    }

    // update the basis sizes
    for (int element = 0; element < sizeOfTh; ++element) {
      subdomainBasisSizes_(element) = offlineSpace_.size(element);
    }

    return;
  } // solveBh

  void computeGFull()
  {
    boost::timer::auto_cpu_timer timer(2, "(Update G: %w) ");

    G_ = rieszRepresentants_.transpose() * systemMatrix() * rieszRepresentants_;
    return;
  }

  void computeGNew()
  {
    boost::timer::auto_cpu_timer timer(2, "(Update G (new): %w) ");

    const int oldSize  = G_.rows();
    const int numOfNew = rieszRepresentants_.cols() - oldSize;

    G_.conservativeResize(oldSize + numOfNew, oldSize + numOfNew);
    G_.bottomRows(numOfNew) = rieszRepresentants_.rightCols(numOfNew).transpose() * systemMatrix()
                              * rieszRepresentants_;
    G_.rightCols(numOfNew) = rieszRepresentants_.transpose() * systemMatrix() * rieszRepresentants_.rightCols(numOfNew);
    return;
  } // computeGNew

private:
  const ModelType&                   model_;
  const OfflineSpaceType&            offlineSpace_;
  const DetailedSolverConnectorType& disco_;
  LocalizerType&                     localizer_;
  const ParameterType                muBar_;
  RieszRepresentantsListType         rieszRepresentants_;
  DofMatrixType                      G_;
  bool                               initialized_;
  SystemMatrixCompleteType           systemMatrix_;
  DofVectorType                      subdomainBasisSizes_;
  EscoType*                          esco_;
}; // class Estimator
}   // namespace Estimator
} // namespace ReducedData
} // namespace Generator
} // namespace Offline
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_ESTIMATOR_RESIDUAL_HH__
