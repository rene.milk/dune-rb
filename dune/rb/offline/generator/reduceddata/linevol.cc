/*
 * linevol.cc
 *
 *  Created on: 17.04.2012
 *      Author: martin
 */

#include <config.h>
#include "linevol.hh"
#include <dune/rb/runtime_error.hh>

namespace Dune
{
namespace RB
{
namespace Offline
{
namespace Generator
{
namespace ReducedData
{

template<typename DetailedConnectorImp>
LinEvol<DetailedConnectorImp>::LinEvol (const DetailedConnectorType & connector,
                  const OfflineSpaceType & space)
    : connector_(connector), space_(space), N_(space.size())
{
}

template<typename DetailedConnectorImp>
LinEvol<DetailedConnectorImp>::~LinEvol ()
{
  typedef typename std::map< std::string, MatrixListType >::iterator ItType;
  for (ItType it = cache_.begin(); it != cache_.end(); ++it)
  {
    MatrixListType & ml = it->second;
    for (unsigned int i = 0; i < ml.size(); ++i)
    {
      if (ml[i] != NULL) delete ml[i];
    }
  }
}


template< typename DetailedConnectorImp > typename LinEvol< DetailedConnectorImp >::MatrixListType & LinEvol<
    DetailedConnectorImp >::getOfflineMatrix (std::string id) const
{
  typedef typename std::map< std::string, MatrixListType >::iterator CacheItType;
  CacheItType cit = cache_.find(id);
  if (cit == cache_.end())
  {
    std::string first, second, Wstr;
    splitInnerProduct(id, first, second, Wstr);
    typedef typename DetailedConnectorType::SeparableMatrixType SMT;
    typedef typename DetailedConnectorType::SeparableVectorType SVT;
    typedef typename SMT::MatrixType MT;
    typedef typename SVT::MatrixType VT;
    const SMT & wsm = connector_.getSystemMatrix(Wstr);
    const MT & W = wsm.component(0);
    unsigned int leftCompSize = getNumberOfComponents(first);
    unsigned int rightCompSize = getNumberOfComponents(second);
    unsigned int compSize = leftCompSize * rightCompSize;
    MatrixListType res(compSize, NULL);

    std::string firstOp = isOperator(first);
    std::string secondOp = isOperator(second);

    for (unsigned int i = 0; i < leftCompSize; ++i)
    {
      if (first == "phi" || !firstOp.empty())
      {
        const MatrixType * fMp;
        if (firstOp.empty())
          fMp = &(space_.getDofMatrix());
        else
          fMp = &(getOperatorEvaluations(firstOp, i));
        const MatrixType & fM = *fMp;

        for (unsigned int j = 0; j < rightCompSize; ++j)
        {
          if (second == "phi" || !secondOp.empty())
          {
            const MatrixType * sMp;
            if (secondOp.empty())
              sMp = &(space_.getDofMatrix());
            else
              sMp = &(getOperatorEvaluations(firstOp, j));
            const MatrixType & sM = *sMp;
            res[i * rightCompSize + j] = new MatrixType(fM.transpose() * W * sM);
          }
          else
          {
            const VT & sV = connector_.getSystemVector(second).component(j);
            res[i * rightCompSize + j] = new MatrixType(fM.transpose() * W * sV);
          }
        }
      }
      else
      {
        const VT & fV = connector_.getSystemVector(first).component(i);
        for (unsigned int j = 0; j < rightCompSize; ++j)
        {
          if (second == "phi" || !secondOp.empty())
          {
            const MatrixType * sMp;
            if (secondOp.empty())
              sMp = &(space_.getDofMatrix());
            else
              sMp = &(getOperatorEvaluations(firstOp, j));
            const MatrixType & sM = *sMp;
            res[i * rightCompSize + j] = new MatrixType(fV.transpose() * W * sM);
          }
          else
          {
            const VT & sV = connector_.getSystemVector(second).component(j);
            res[i * rightCompSize + j] = new MatrixType(fV.transpose() * W * sV);
          }
        }
      }
    }
    cache_.insert(make_pair(id, res));
    return (cache_.find(id))->second;
  }
  else
  {
    MatrixListType & ml = cit->second;
    return ml;
  }
}

template< typename DetailedConnectorImp >
void LinEvol< DetailedConnectorImp >::update (int start, int end)
{
  // TODO: make the update method in ReducedData::LinEvol more intelligent!!!
  typedef typename std::map< std::string, MatrixListType >::iterator CacheItType;
  CacheItType it = cache_.begin();
  for (; it != cache_.end(); ++it)
  {
    MatrixListType & ml = (*it).second;
    for (unsigned int i = 0; i < ml.size(); ++i)
    {
      delete ml[i];
    }
  }
  cache_.clear();
//  throw(Dune::RB::RuntimeError("not yet implemented!"));
}

template< typename DetailedConnectorImp >
int LinEvol< DetailedConnectorImp >::getN ()
{
  return N_;
}

template< typename DetailedConnectorImp >
const typename LinEvol< DetailedConnectorImp >::MatrixType & LinEvol< DetailedConnectorImp >::getOperatorEvaluations (
    const std::string & opId, int i) const
{
  typedef typename std::map< std::string, MatrixListType >::iterator CacheItType;
  CacheItType cit = cache_.find(opId);

  if (cit == cache_.end())
  {
    typedef typename DetailedConnectorType::SeparableMatrixType SMT;
    typedef typename SMT::MatrixType MT;

    const MatrixType & RB = space_.getDofMatrix();
    const SMT & smOp = connector_.getSystemMatrix(opId);
    MatrixListType ml(smOp.numComponents(), NULL);
    for (unsigned int j = 0; j < smOp.numComponents(); ++j)
    {
      const MT & opM = smOp.component(j);
      ml[j] = new MatrixType(opM * RB);
    }
    cache_.insert(make_pair(opId, ml));
    return *(ml.at(i));
  }
  else
  {
    MatrixListType ml = cit->second;
    return *(ml.at(i));
  }
}

template< typename DetailedConnectorImp >
std::string LinEvol< DetailedConnectorImp >::isOperator (
    const std::string & opCandidate) const
{
  const std::string parseError = "Parse Error: Expected Id, phi or OpId[phi]";

  typedef std::string::size_type stype;

  stype argPos = opCandidate.find_first_of('[');

  if (argPos == std::string::npos)
    return "";

  if (opCandidate.substr(argPos) != "[phi]")
    throw(Dune::RB::RuntimeError(parseError));

  return opCandidate.substr(0, argPos);
}

template< typename DetailedConnectorImp >
void LinEvol< DetailedConnectorImp >::splitInnerProduct (const std::string str,
                                                  std::string & first,
                                                  std::string & second,
                                                  std::string & W) const
{
  const std::string parseError =
    "Parse Error: Syntax must be <Id1,Id2>_W, where Id* can be one of phi, FuncId or OpId[phi]";

  typedef std::string::size_type stype;

  stype sepPos = str.find_first_of(',');
  if (str[0] != '<') throw(Dune::RB::RuntimeError(parseError));
  if (sepPos == std::string::npos) throw(Dune::RB::RuntimeError(parseError));
  stype inProdEndPos = str.find_first_of('>');
  if (inProdEndPos == std::string::npos)
    throw(Dune::RB::RuntimeError(parseError));
  first = str.substr(1, sepPos - 1);
  second = str.substr(sepPos + 1, inProdEndPos - sepPos - 1);
  if (str.length() <= inProdEndPos + 1 || str[inProdEndPos + 1] != '_')
    throw(Dune::RB::RuntimeError(parseError));
  W = str.substr(inProdEndPos + 2);
}

template< typename DetailedConnectorImp >
unsigned int LinEvol< DetailedConnectorImp >::getNumberOfComponents (
    const std::string & idstring) const
{
  unsigned int compSize;
  std::string opString = isOperator(idstring);
  if (idstring == "phi")
    compSize = 1;

  else if (opString.empty())
    compSize = connector_.getSystemVector(idstring).numComponents();

  else
    compSize = connector_.getSystemMatrix(opString).numComponents();

  return compSize;
}

} /* namespace ReducedData */
} /* namespace Generator */
} /* namespace Offline */
} /* namespace RB */
} /* namespace Dune */
