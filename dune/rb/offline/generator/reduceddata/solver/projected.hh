#ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_PROJECTED_HH_
#define __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_PROJECTED_HH_

#include <dune/rb/la/separableparametric/matrix/eigen.hh>
#include <dune/rb/reduced/solver/connector/eigen.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace ReducedData {
namespace Solver {
template< class ModelImp, class OfflineSpaceImp, class DetailedSolverConnectorImp, class LocalizerImp >
class Projected
{
public:
  typedef ModelImp                                                                              ModelType;
  typedef OfflineSpaceImp                                                                       OfflineSpaceType;
  typedef DetailedSolverConnectorImp                                                            DiscoType;
  typedef LocalizerImp                                                                          LocalizerType;
  typedef typename OfflineSpaceType::DofMatrixType                                              DofMatrixType;
  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseMatrix                        MatrixType;
  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseVector                        VectorType;
  typedef Dune::RB::Reduced::Solver::Connector::EigenBased< ModelType, MatrixType, VectorType > RescoType;

  Projected(const ModelType& model,
            const OfflineSpaceType& offlineSpace,
            const DiscoType& disco,
            LocalizerType& localizer)
    : model_(model),
      offlineSpace_(offlineSpace),
      resco_(NULL),
      disco_(disco),
      localizer_(localizer)
  {}

  ~Projected()
  {
    if (resco_ != 0)
      delete resco_;
  }

  const RescoType& generate()
  {
	  if (resco_!=NULL)
		  delete resco_;
    resco_ = new RescoType(model_, systemMatrix_, rhs_);
    return *resco_;
  }

  void update()
  {
    const int numDetailedDofs = disco_.getSystemMatrix().rows();
    const int numReducedDofs  = offlineSpace_.size();

    // Write all subdomain bases to one big sparse matrix
    Eigen::SparseMatrix< double > fullBasis(numDetailedDofs, numReducedDofs);
    typedef Eigen::Triplet< double > TripletType;
    std::vector< TripletType > triplets;
    triplets.reserve(offlineSpace_.size(0) * numDetailedDofs);
    int start = 0;
    for (int element = 0; element != offlineSpace_.grid().size(); ++element) {
      const DofMatrixType& elementBasis = offlineSpace_.getDofMatrix(element);
      localizer_.addTriplets(elementBasis, element, start, triplets);
      start += elementBasis.cols();
    }
    fullBasis.setFromTriplets(triplets.begin(), triplets.end());

    // project all components of the high dimensional operator and right hand side to the new reduced basis
    const int numOpComps = disco_.getSystemMatrix().numComponents();
    systemMatrix_ = MatrixType(fullBasis.cols(), fullBasis.cols());
    systemMatrix_.reserve(numOpComps, true);

    const int numRHSComps = disco_.getRightHandSide().numComponents();
    rhs_ = VectorType(fullBasis.cols());
    rhs_.reserve(numRHSComps, true);
    
    setSymbolicCoefficients();

    // operator components
    for (int opComp = 0; opComp != numOpComps; ++opComp) {
      // element terms
      Eigen::SparseMatrix<double> temp = fullBasis.transpose() * disco_.getSystemMatrix().component(opComp) * fullBasis;
      systemMatrix_.component(opComp) = temp.toDense();
    }
    if (disco_.getSystemMatrix().hasParameterindependentPart()) {
      Eigen::SparseMatrix<double> temp = fullBasis.transpose() * disco_.getSystemMatrix().parameterindependentPart() * fullBasis;
      systemMatrix_.parameterindependentPart() = temp.toDense();
    }
    // right hand side components
    for (int rhsComp = 0; rhsComp != numRHSComps; ++rhsComp) {
      rhs_.component(rhsComp) = fullBasis.transpose() * disco_.getRightHandSide().component(rhsComp);
    }
    if (disco_.getRightHandSide().hasParameterindependentPart())
      rhs_.parameterindependentPart() = fullBasis.transpose() * disco_.getRightHandSide().parameterindependentPart();
    
    return;
  }             // update

private:
  void setSymbolicCoefficients() 
  {
    const int numMobilityComps  = model_.mobility().size();
    const int numDirichletComps = model_.dirichletValues().size();
    const int numNeumannComps   = model_.neumannValues().size();
    const std::vector<std::string>& dirichletCoeffs = model_.dirichletValues().getSymbolicCoefficients();
    const std::vector<std::string>& neumannCoeffs = model_.neumannValues().getSymbolicCoefficients();
    const std::vector<std::string>& mobilityCoeffs = model_.mobility().getSymbolicCoefficients();
    assert(model_.mobility().getVariable()=="mu");
    assert(model_.dirichletValues().getVariable()=="mu");
    assert(model_.neumannValues().getVariable()=="mu");
    
    systemMatrix_.setSymbolicCoefficients(mobilityCoeffs, "mu");
    
    std::vector<std::string> rhsSymbCoeffs(dirichletCoeffs);
    for (int gDComp=0; gDComp!=model_.dirichletValues().size(); ++gDComp)
      for (int mobilityComp=0; mobilityComp!=model_.mobility().size(); ++mobilityComp)
        rhsSymbCoeffs.push_back(dirichletCoeffs[gDComp]+"*"+mobilityCoeffs[mobilityComp]);
    
    rhsSymbCoeffs.insert(rhsSymbCoeffs.end(), neumannCoeffs.begin(), neumannCoeffs.end());
    rhs_.setSymbolicCoefficients(rhsSymbCoeffs, "mu");
    
  }
  
  
  const ModelType&        model_;
  const OfflineSpaceType& offlineSpace_;
  RescoType*              resco_;
  const DiscoType&        disco_;
  LocalizerType&          localizer_;
  MatrixType              systemMatrix_;
  VectorType              rhs_;
};             // class Projected
}           // namespace Solver
}         // namespace ReducedData
}       // namespace Generator
}     // namespace Offline
}   // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_PROJECTED_HH_
