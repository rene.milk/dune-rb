namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace ReducedData {
template< class SolverImp >
class Solver
{
public:
  typedef SolverImp
  ObjectType;

  ObjectType& generate()
};           // class Solver
}         // namespace ReducedData
}       // namespace Generator
}     // namespace Offline
}   // namespace RB
} // namespace Dune