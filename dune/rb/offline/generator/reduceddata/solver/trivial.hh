#ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_TRIVIAL_HH_
#define __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_TRIVIAL_HH_

#include <dune/rb/la/separableparametric/matrix/eigen.hh>
#include <dune/rb/reduced/solver/connector/eigen.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace ReducedData {
namespace Solver {
template< class ModelImp >
class Trivial
{
public:
//  typedef ModelImp                                                                              ModelType;
//  typedef OfflineSpaceImp                                                                       OfflineSpaceType;
//  typedef DetailedSolverConnectorImp                                                            DiscoType;
//  typedef LocalizerImp                                                                          LocalizerType;
//  typedef typename OfflineSpaceType::DofMatrixType                                              DofMatrixType;
//  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseMatrix                        MatrixType;
//  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseVector                        VectorType;
//  typedef Dune::RB::Reduced::Solver::Connector::EigenBased< ModelType, MatrixType, VectorType > RescoType;
  typedef Dune::RB::Reduced::Solver::Connector::Trivial RescoType;
  
  Projected()
  {}

  const RescoType& generate()
  {
    return resco_;
  }

  void update()
  {}

private:
  RescoType resco_;
};             // class Projected
}           // namespace Solver
}         // namespace ReducedData
}       // namespace Generator
}     // namespace Offline
}   // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_OFFLINE_GENERATOR_REDUCEDDATA_SOLVER_TRIVIAL_HH_
