#ifndef __DUNE_RB_OFFLINE_GENERATOR_DICTIONARY_DEFAULT_HH__
#define __DUNE_RB_OFFLINE_GENERATOR_DICTIONARY_DEFAULT_HH__

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/lexical_cast.hpp>

namespace Dune {
namespace RB {
namespace Offline {
namespace Generator {
namespace Dictionary {
template< class DictionaryImp, class ModelImp, class DetailedSolverConnectorImp >
class Default
{
public:
  typedef DictionaryImp                                     DictionaryType;
  typedef ModelImp                                          ModelType;
  typedef DetailedSolverConnectorImp DetailedSolverConnectorType;

  typedef typename DictionaryType::DofVectorType            DofVectorType;
  typedef typename ModelType::ParameterType                 ParameterType;
  
  Default(DictionaryType& dict,
          const ModelType& model, 
          DetailedSolverConnectorType& disco)
    : dict_(dict),
      model_(model),
      disco_(disco)
  {}


  template< class ParameterSampleType >
  const boost::uuids::uuid generate(const ParameterSampleType& paramSample)
  { 
    const int    paramSampleSize = paramSample.size();
    
    boost::uuids::uuid uid                   = boost::uuids::random_generator() ();
    std::string        uidString             = boost::lexical_cast< std::string >(uid);

    for (int k = 0; k < paramSampleSize; ++k) {
      ParameterType        param  = paramSample.get(k);
      DofVectorType solution;
      disco_.solve(param, solution);
      dict_.add(solution, param);
    }
    
    return uid;
  } // generate

private:

  DictionaryType&   dict_;
  const ModelType&  model_;
  DetailedSolverConnectorType& disco_;
}; // class Default
} // namespace Dictionary
} // namespace Generator
} // namespace Offline
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_OFFLINE_GENERATOR_DICTIONARY_DEFAULT_HH__