#ifndef DUNE_RB_OFFLINE_SPACE_DEFAULT_HH
#define DUNE_RB_OFFLINE_SPACE_DEFAULT_HH

#ifdef HAVE_CMAKE_CONFIG
  #include "cmake_config.h"
#elif defined (HAVE_CONFIG_H)
  #include <config.h>
#endif // ifdef HAVE_CMAKE_CONFIG

#include <dune/common/shared_ptr.hh>
#include <dune/common/exceptions.hh>

#include <dune/stuff/common/color.hh>
#include <dune/stuff/la/container/eigen.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Space {

template< class ElementImp = double >
class Default
{
public:
  typedef ElementImp                                                  ElementType;
  typedef Dune::Stuff::LA::Container::EigenDenseMatrix< ElementType > MatrixType;
  typedef typename MatrixType::size_type                              size_type;

  Default(Dune::shared_ptr< MatrixType > _basisMatrix)
    : basisMatrix_(_basisMatrix)
  {
    // if this is an empty matrix, throw up!
    if (basisMatrix_->rows() == 0 || basisMatrix_->cols() == 0)
      DUNE_THROW(Dune::RangeError,
                 "\n"
                 << Dune::Stuff::Common::colorString("ERROR:", Dune::Stuff::Common::Colors::red)
                 << " given '_basisMatrix' is empty!");
  }

  size_type size() const
  {
    return basisMatrix_.cols();
  }

  Dune::shared_ptr< MatrixType > basis()
  {
    return basisMatrix_;
  }

  const Dune::shared_ptr< const MatrixType > basis() const
  {
    return basisMatrix_;
  }

private:
  Dune::shared_ptr< MatrixType > basisMatrix_;
}; // class Default

} // namespace Space
} // namespace Offline
} // namespace RB
} // namespace Dune

#endif // DUNE_RB_OFFLINE_SPACE_DEFAULT_HH
