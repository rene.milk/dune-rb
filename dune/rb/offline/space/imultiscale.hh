#ifndef DUNE_RB_OFFLINE_SPACE_IMULTISCALE_HH__
#define DUNE_RB_OFFLINE_SPACE_IMULTISCALE_HH__

#include "interface.hh"

namespace Dune{

namespace RB{

namespace Offline{

namespace Space{

template< class Traits >
class IMultiScale : public Interface<Traits>
{
public:
  typedef typename Traits::OfflineMatrixType
    RBMatrixType;

  typedef typename Traits::OfflineVectorType
    RBVectorType;

  typedef typename Traits::CoarseGridType
    CoarseGridType;

  typedef typename Traits::CoarseMapperType
    CoarseMapperType;

  const CoarseGridType& grid() const;

  const CoarseMapperType& mapper() const;

  void save(const std::string filename) const;

  void load(const std::string filename);

  const int size() const;

  const int size(const int Element) const;

  const RBMatrixType& getDofMatrix() const;

  const RBMatrixType& getDofMatrix(const int Element) const;

  const RBVectorType& getDofVector(const int i) const;

  const RBVectorType& getDofVector(const int Element, const int i) const;

  void setDofMatrix(const RBMatrixType& dofMatrix);

  void setDofMatrix(const int Element, const RBMatrixType& dofMatrix);

  void setDofVectors(const int i, const RBVectorType& dofVector);

  void setDofVectors(const int Element, const int i, const RBVectorType& dofVector);

  void addDofMatrix(const RBMatrixType& dofMatrix);

  void addDofMatrix(const int Element, const RBMatrixType& dofMatrix);

  void addDofVector(const RBMatrixType& dofMatrix);

  void addDofVector(const int Element, const RBVectorType& dofMatrix);
}; // class Interface

} // namespace Space

} // namespace Offline

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_OFFLINE_SPACE_IMULTISCALE_HH__

