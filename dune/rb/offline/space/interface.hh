#ifndef DUNE_RB_OFFLINE_SPACE_INTERFACE_HH__
#define DUNE_RB_OFFLINE_SPACE_INTERFACE_HH__


#include <dune/rb/matlabcomm/serializedarray/interface.hh>

#if HAVE_EIGEN
#include <stdexcept>
#include <iomanip>
#include <dune/rb/backend/fulleigenmatrix.hh>
#endif

namespace Dune
{

namespace RB
{

namespace Offline
{

namespace Space
{

#if HAVE_EIGEN
struct EigenMatrixTraits
{
  typedef Backend::FullEigenMatrix FullMatrixBackendType;

  typedef Eigen::Map<Eigen::MatrixXd> MappedType;

  typedef Eigen::MatrixXd MatrixType;

  typedef Eigen::VectorXd VectorType;
};
#endif

template< class Traits >
class Interface
{
public:
  typedef typename Traits::FullMatrixBackendType FullMatrixBackendType;
  typedef typename Traits::MatrixType RBMatrixType;

  typedef typename Traits::VectorType RBVectorType;

  void save (const std::string filename) const;

  void load (const std::string filename);

  const int size () const;

  const RBMatrixType& getDofMatrix () const;

  const RBVectorType& getDofVector (const int i) const;

  void setDofMatrix (const RBMatrixType& dofMatrix);

  void setDofVector (const int i, const RBVectorType& dofVector);

  void addDofMatrix (const RBMatrixType& dofMatrix);

  void addDofVector (const RBMatrixType& dofMatrix);

  bool isValid() const;

  const std::string & getIdentifier();

  void reconstruct(MatlabComm::SerializedArray::CMatrixWrapper & coeffs, RBVectorType & reconstruction);

  void reconstruct(const RBVectorType & coeffs, RBVectorType & reconstruction);
};
// class Interface

}// namespace Space

} // namespace Offline

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_OFFLINE_SPACE_RBDEFAULT_HH__
