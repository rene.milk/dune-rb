/*
 * rbdefault.cc
 *
 *  Created on: 19.04.2012
 *      Author: martin
 */

#include <config.h>
#include <dune/rb/runtime_error.hh>
#include "rbdefault.hh"
#include <cassert>

namespace Dune
{

namespace RB
{

namespace Offline
{

namespace Space
{

RBDefault::RBDefault ()
    : rbMatrix_(0, 0)
{
}

void RBDefault::save (const std::string filename) const
{
  rbMatrix_.saveToBinaryFile(filename);
}

void RBDefault::load (const std::string filename)
{
  rbMatrix_.loadFromBinaryFile(filename);
}

const int RBDefault::size () const
{
  return rbMatrix_.cols();
}

const typename RBDefault::RBMatrixType & RBDefault::getDofMatrix () const
{
  return rbMatrix_;
}

const typename RBDefault::RBVectorType RBDefault::getDofVector (
    const int i) const
{
  return rbMatrix_.col(i);
}

void RBDefault::setDofMatrix (const RBMatrixType & dofMatrix)
{
  rbMatrix_ = dofMatrix;
}

void RBDefault::setDofVector (const int i, const RBVectorType & dofVector)
{
  if (i < rbMatrix_.cols())
    rbMatrix_.col(i) = dofVector;
  else if (i == rbMatrix_.cols())
  {
    RBMatrixType oldRbMatrix(rbMatrix_);
    FullMatrixBackendType::resize(oldRbMatrix, rbMatrix_.rows(),
        rbMatrix_.cols() + 1, rbMatrix_);
    rbMatrix_.col(i) = dofVector;
  }
  else
  {
    throw(Dune::RB::RuntimeError(
        "In Offline::Space::RBDefault:\n In setDofVector():\n  Index too large!"));
  }
}

void RBDefault::addDofMatrix (const RBMatrixType & dofMatrix)
{
  RBMatrixType oldRbMatrix(rbMatrix_);
  int oldCols = rbMatrix_.cols();
  int H = dofMatrix.rows();
  assert(rbMatrix_.rows() == 0 || H == rbMatrix_.rows());
  int addCols = dofMatrix.cols();

  FullMatrixBackendType::resize(oldRbMatrix, H, oldCols + addCols, rbMatrix_);
  rbMatrix_.block(0, oldCols, H, addCols) = dofMatrix;
}

void RBDefault::addDofVector (const RBVectorType & dofVector)
{
  this->setDofVector(rbMatrix_.cols(), dofVector);
}

bool RBDefault::isValid () const
{
  return (rbMatrix_.cols() > 0 && rbMatrix_.rows() > 0);
}

void RBDefault::reconstruct (
    const MatlabComm::SerializedArray::CMatrixWrapper & coeffs,
    RBVectorType & reconstruction)
{
  typedef typename Traits::MappedType MappedType;
  MappedType * mapped = FullMatrixBackendType::mapToBaseType< MappedType >(
      coeffs);
  MappedType & ref = *mapped;
  reconstruction = rbMatrix_ * ref;
  delete mapped;
}

void RBDefault::reconstruct (const RBVectorType & coeffs,
                             RBVectorType & reconstruction)
{
  reconstruction = rbMatrix_ * coeffs;
}

} // namespace Space

} // namespace Offline

} // namespace RB

} // namespace Dune

