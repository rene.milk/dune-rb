#ifndef __DUNE_RB_OFFLINE_SPACE_LOCALIZED_HH__
#define __DUNE_RB_OFFLINE_SPACE_LOCALIZED_HH__

#include <Eigen/Core>
#include <boost/filesystem.hpp>

#include <dune/rb/la/separableparametric/matrix/eigen.hh>

namespace Dune {
namespace RB {
namespace Offline {
namespace Space {
template< class CoarseGridImp,
          class CoarseMapperImp >
class Localized
{
public:
  typedef CoarseGridImp                                        GridType;

  typedef CoarseMapperImp                                      MapperType;
  typedef LA::SeparableParametric::Container::EigenDenseMatrix DofMatrixVectorType;
  typedef DofMatrixVectorType::ComponentType                   DofMatrixType;
  typedef Eigen::VectorXd                                      DofVectorType;

  template< class DetailedMapperType > Localized(const GridType& grid, const DetailedMapperType& detailedMapper)
    : grid_(grid),
      mapper_(eigenBaseFunctionLists_, grid)
  {
    eigenBaseFunctionLists_.reserve(grid_.size(), false);
    for (int element = 0; element != grid_.size(); ++element)
      eigenBaseFunctionLists_.component(element) = DofMatrixType::Zero(grid.enclosedFineCells(
                                                                       element) * detailedMapper.maxNumDofs(), 0);
  }

  inline ~Localized()
  {}

  int size() const
  {
    return mapper().size();
  }

  int size(const int element) const
  {
    return mapper().size(element);
  }

  const DofMatrixType& getDofMatrix() const
  {
    assert(grid().size() == 1 && "Use getDofMatrix(element) for coarse grid size > 1!");
    return getDofMatrix(0);
  }

  const DofMatrixType& getDofMatrix(const int element) const
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");
    return eigenBaseFunctionLists_.component(element);
  }

  const DofVectorType getDofVector(const int i) const
  {
    assert(grid().size() == 1 && "Use getDofVector(element, i) for coarse grid size > 1!");
    return getDofVector(0, i);
  }

  DofVectorType getDofVector(const int element, const int i) const
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");
    assert(i >= 0 && i < eigenBaseFunctionLists_.component(element).cols() && "Invalid dof vector number given!");

    return eigenBaseFunctionLists_.component(element).col(i);
  }

  void setDofMatrix(const DofMatrixType& dofMatrix)
  {
    assert(grid().size() == 1 && "Use setDofMatrix(element, dofMatrix) for coarse grid size > 1!");
    return setDofMatrix(0, dofMatrix);
  }

  void setDofMatrix(const int element, const DofMatrixType& dofMatrix)
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");
    eigenBaseFunctionLists_.component(element) = dofMatrix;
    return;
  }

  void setDofVector(const int i, const DofVectorType& dofVector)
  {
    assert(grid().size() == 1 && "Use setDofVector(element, i, dofVector) for coarse grid size > 1!");
    return setDofVector(0, i);
  }

  void setDofVector(const int element, const int i, const DofVectorType& dofVector)
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");
    assert(i >= 0 && i < eigenBaseFunctionLists_.component(element).cols() && "Invalid dof vector number given!");
    eigenBaseFunctionLists_.component(element).col(i) = dofVector;
    return;
  }

  void addDofMatrix(const DofMatrixType& dofMatrix)
  {
    assert(grid().size() > 1 && "Use addDofMatrix(element, dofMatrix) for coarse grid size > 1!");

    return addDofMatrix(0, dofMatrix);
  }

  void addDofMatrix(const int element, const DofMatrixType& dofMatrix)
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");

    eigenBaseFunctionLists_.component(element).conservativeResize(eigenBaseFunctionLists_.component(element).rows(),
                                                                  eigenBaseFunctionLists_.component(
                                                                    element).cols() + dofMatrix.cols());
    eigenBaseFunctionLists_.component(element).rightCols(dofMatrix.cols()) = dofMatrix;
    return;
  } // addDofMatrix

  void addDofVector(const DofVectorType& dofVector)
  {
    assert(grid().size() == 1 && "Use addDofVector(element, dofVector) for coarse grid size > 1!");
    return addDofVector(0, dofVector);
  }

  void addDofVector(const int element, const DofVectorType& dofVector)
  {
    assert(element >= 0 && element < eigenBaseFunctionLists_.numComponents() && "Invalid subdomain given!");
    eigenBaseFunctionLists_.component(element).conservativeResize(eigenBaseFunctionLists_.component(element).rows(),
                                                                  eigenBaseFunctionLists_.component(element).cols() + 1);
    eigenBaseFunctionLists_.component(element).rightCols(1) = dofVector;
    return;
  } // addDofVector

  inline const GridType& grid() const
  {
    return grid_;
  }

  inline const MapperType& mapper() const
  {
    return mapper_;
  }

  inline bool save(const std::string& path) const
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Offline::Space::Localized::save is not a directory, not saving!\n";
      return false;
    }
    boost::filesystem::create_directory(path+"/localized_space");
    return eigenBaseFunctionLists_.save(path + "/localized_space");
  } // save

  inline bool load(const std::string& path)
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Offline::Space::Localized::load is not a directory, not loading!\n";
      return false;
    }
    return eigenBaseFunctionLists_.load(path + "/localized_space");
  } // load

private:
  DofMatrixVectorType eigenBaseFunctionLists_;
  MapperType          mapper_;
  GridType            grid_;
};
}
}
}
}


#endif // ifndef __DUNE_RB_OFFLINE_SPACE_LOCALIZED_HH__