#ifndef DUNE_RB_OFFLINE_SPACE_RBDEFAULT_HH__
#define DUNE_RB_OFFLINE_SPACE_RBDEFAULT_HH__

#include "interface.hh"

namespace Dune
{

namespace RB
{

namespace Offline
{

namespace Space
{

// TODO Allow matrix traits other than EigenMatrixTraits, too!
class RBDefault: public Interface< EigenMatrixTraits >
{
public:
  typedef EigenMatrixTraits Traits;

  typedef typename Traits::FullMatrixBackendType FullMatrixBackendType;

  typedef typename Traits::MatrixType RBMatrixType;

  typedef typename Traits::VectorType RBVectorType;

public:
  RBDefault();

  virtual ~RBDefault()
  {
  }

  void save (const std::string filename) const;

  void load (const std::string filename);

  const int size () const;

  const RBMatrixType& getDofMatrix () const;

  const RBVectorType getDofVector (const int i) const;

  void setDofMatrix (const RBMatrixType& dofMatrix);

  void setDofVector (const int i, const RBVectorType& dofVector);

  void addDofMatrix (const RBMatrixType& dofMatrix);

  void addDofVector (const RBVectorType& dofMatrix);

  bool isValid() const;

  const std::string & getIdentifier()
  {
    return id;
  }

  void reconstruct(const MatlabComm::SerializedArray::CMatrixWrapper & coeffs, RBVectorType & reconstruction);

  void reconstruct(const RBVectorType & coeffs, RBVectorType & reconstruction);

private:
  RBMatrixType rbMatrix_;
  const std::string id;

};
// class RBDefault

}// namespace Space

} // namespace Offline

} // namespace RB

} // namespace Dune

#endif // DUNE_RB_OFFLINE_SPACE_RBDEFAULT_HH__
