#ifndef __DUNE_RB_OFFLINE_DICTIONARY_DEFAULT_HH__
#define __DUNE_RB_OFFLINE_DICTIONARY_DEFAULT_HH__

#include <Eigen/Core>
#include <boost/filesystem.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace Dune {
namespace RB {
namespace Offline {
namespace Dictionary {
template< class ParameterImp >
class Default
{
public:
  typedef ParameterImp     ParameterType;
  typedef Eigen::MatrixXd  DofMatrixType;
  typedef Eigen::VectorXd  DofVectorType;

  Default()
  {}

  int size() const
  {
    return dofMatrix_.cols();
  }

  const DofMatrixType& getDofMatrix() const
  {
    return dofMatrix_;
  }
  
  bool contains(const ParameterType& param) const 
  {
    
    return (parameterMap_.find(boost::lexical_cast<std::string>(param))!=parameterMap_.end());
  }
  
  bool add(const DofVectorType& solution, const ParameterType& param) 
  {
    dofMatrix_.conservativeResize(solution.rows(), dofMatrix_.cols()+1);
    dofMatrix_.col(dofMatrix_.cols()-1) = solution;
    parameterMap_[boost::lexical_cast<std::string>(param)] = dofMatrix_.cols()-1;
    return true;
  }

  DofMatrixType get(const ParameterType& param) const 
  {
    assert(contains(param));
    const int index = parameterMap_.find(boost::lexical_cast<std::string>(param))->second;
    return dofMatrix_.col(index);
  }
  
  inline bool save(const std::string& path) const
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Offline::Dictionary::save is not a directory, not saving!\n";
      return false;
    }
    boost::filesystem::create_directory(path+"/dictionary");
    std::string mapfilename = path+"/dictionary/parameterMap.bin";
    std::ofstream mapof(mapfilename.c_str(), std::ios::binary);
    boost::archive::binary_oarchive boostarchive(mapof);
    boostarchive << parameterMap_;
    
    
    std::string filename = path + "/dictionary/dofMatrix.bin";
    dofMatrix_.saveToBinaryFile(filename);
    
    return true;
  } // save

  inline bool load(const std::string& path)
  {
    boost::filesystem::path datapath(path);
    
    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Offline::Dictionary::load is not a directory, not loading!\n";
      return false;
    }
    
    parameterMap_.clear();
    std::ifstream mapif("/dictionary/parameterMap.bin", std::ios::binary);
    boost::archive::binary_iarchive boostarchive(mapif);
    boostarchive >> parameterMap_;
    
    
    std::string filename = path + "/dictionary/dofMatrix.bin";
    dofMatrix_.loadFromBinaryFile(filename);
    
    return true;
  } // load

private:
  DofMatrixType dofMatrix_;
  std::map<std::string, int> parameterMap_;
};
}
}
}
}


#endif // ifndef __DUNE_RB_OFFLINE_DICTIONARY_DEFAULT_HH__