#ifndef __DUNE_RB_OFFLINE_LOCALIZER_DEFAULT_HH__
#define __DUNE_RB_OFFLINE_LOCALIZER_DEFAULT_HH__

#include <Eigen/Core>

namespace Dune {
namespace RB {
namespace Offline {
namespace Localizer {
template< class FineGridImp, class CoarseGridImp, class FineMapperImp >
class Default
{
 
public:
  typedef FineGridImp   FineGridType;
  typedef CoarseGridImp CoarseGridType;
  typedef FineMapperImp FineMapperType;
  
  typedef Eigen::MatrixXi RestrictionMatrixType;
  typedef Eigen::MatrixXd MatrixType;
  typedef Eigen::VectorXd VectorType;
  typedef MatrixType LocalMatrixType;
  typedef VectorType LocalVectorType;
  
  Default(const FineGridType& fineGrid, const CoarseGridType& coarseGrid, const FineMapperType& mapper)
    : fineGrid_(fineGrid),
      coarseGrid_(coarseGrid)
  {
    init(mapper);
  }

  void init(const FineMapperType& mapper)
  {
    computeRestrictionMatrix(mapper, restrictionMatrix_);
    return;
  }

  MatrixType& localizeMatrixRows(const MatrixType& matrix, const int element) const
  {
    assert(element < restrictionMatrix_.cols()
           && element >= 0
           && "You tried to restrict a matrix to an element that does not exist!");
    assert(matrix.rows() == restrictionMatrix_.rows()
           && "You tried to restrict a matrix that does not have the right dimension!");
    const int newRows = restrictionMatrix_.col(element).sum();
    localizedMatrix_ = MatrixType::Zero(newRows, matrix.cols());
    int i = 0;
    for (int row = 0; row != matrix.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        for (int col = 0; col != matrix.cols(); ++col) {
          localizedMatrix_(i, col) = matrix(row, col);
        }
        ++i;
      }
    }
    return localizedMatrix_;
  } // localizeMatrixRows

  VectorType& localizeVector(const VectorType& vector, const int element) const
  {
    assert(element < restrictionMatrix_.cols()
           && element >= 0
           && "You tried to restrict a vector to an element that does not exist!");
    assert(vector.rows() == restrictionMatrix_.rows()
           && "You tried to restrict a vector that does not have the right dimension!");
    const int newRows = restrictionMatrix_.col(element).sum();
    localizedVector_ = VectorType::Zero(newRows);
    int i = 0;
    for (int row = 0; row != vector.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        localizedVector_(i) = vector(row);
        ++i;
      }
    }
    return localizedVector_;
  } // localizeVector

  VectorType& getFullVector(const VectorType& vector, const int element) const
  {
    assert(element < restrictionMatrix_.cols()
           && element >= 0
           && "You tried to restrict a vector to an element that does not exist!");
    assert(vector.rows() == restrictionMatrix_.col(element).sum()
           && "You tried to restrict a vector that does not have the right dimension!");
    localizedVector_ = VectorType::Zero(restrictionMatrix_.rows());
    int i = 0;
    for (int row = 0; row != localizedVector_.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        localizedVector_(row) = vector(i);
        ++i;
      } else {
        localizedVector_(row) = 0.0;
      }
    }
    return localizedVector_;
  } // getFullVector

  void getFullVector(const VectorType& vector, const int element, VectorType& ret) const
  {
    assert(element < restrictionMatrix_.cols()
           && element >= 0
           && "You tried to restrict a vector to an element that does not exist!");
    assert(vector.rows() == restrictionMatrix_.col(element).sum()
           && "You tried to restrict a vector that does not have the right dimension!");
    ret = VectorType::Zero(restrictionMatrix_.rows());
    int i = 0;
    for (int row = 0; row != ret.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        ret(row) = vector(i);
        ++i;
      } else {
        ret(row) = 0.0;
      }
    }
    return;
  } // getFullVector

  const MatrixType& getFullMatrixRows(const MatrixType& matrix, const int element) const
  {
    assert(element < restrictionMatrix_.cols()
           && element >= 0
           && "You tried to get the full version of a matrix to an element that does not exist!");
    assert(matrix.rows() == restrictionMatrix_.col(element).sum()
           && "You tried to rget the full version of a matrix that does not have the right dimension!");
    localizedMatrix_ = MatrixType::Zero(restrictionMatrix_.rows(), matrix.cols());
    int i = 0;
    for (int row = 0; row != localizedMatrix_.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        localizedMatrix_.row(row) = matrix.row(i);
        ++i;
      }
    }
    return localizedMatrix_;
  } // getFullMatrixRows

  template< class TripletsVectorType >
  void addTriplets(const MatrixType& matrix, const int element,
                   const int start, TripletsVectorType& triplets) const
  {
    int i = 0;

    for (int row = 0; row != restrictionMatrix_.rows(); ++row) {
      if (restrictionMatrix_(row, element) == 1) {
        for (int j = 0; j != matrix.cols(); ++j)
          triplets.push_back(typename TripletsVectorType::value_type(row, start + j, matrix(i, j)));
        ++i;
      }
    }
    return;
  } // addTriplets

private:
  void computeRestrictionMatrix(const FineMapperType& mapper,
                                RestrictionMatrixType& restrictionMatrix) const
  {
    typedef typename FineGridType::LeafGridView::template Codim<0>::Iterator IteratorType;
    typedef typename FineGridType::LeafGridView::template Codim<0>::Entity   EntityType;

    const int coarseGridSize = coarseGrid_.size();
    restrictionMatrix = Eigen::MatrixXi::Zero(mapper.size(), coarseGridSize);

    IteratorType fineEnd          = fineGrid_.leafView().template end<0>();
    for (IteratorType it = fineGrid_.leafView().template begin<0>(); it != fineEnd; ++it) {
      const EntityType& en        = *it;
      const int         subdomain = coarseGrid_.getSubdomain(en);
      int               numDofs   = mapper.numDofs(en);
      for (int i = 0; i != numDofs; ++i) {
        const int globalDof = mapper.mapToGlobal(en, i);
//        Eigen::RowVectorXi unitRow = Eigen::RowVectorXi::Unit(coarseGridSize, subdomain);
        restrictionMatrix.row(globalDof).setZero();
        restrictionMatrix(globalDof, subdomain) = 1;
      }
    }
  } // computeRestrictionMatrix

  const FineGridType& fineGrid_;
  const CoarseGridType& coarseGrid_;
  RestrictionMatrixType restrictionMatrix_;
  mutable MatrixType    localizedMatrix_;
  mutable VectorType    localizedVector_;
};
} // namespace Localizer
} // namespace Offline
} // namespace RB
} // namespace Dune


#endif // #ifndef __DUNE_RB_OFFLINE_LOCALIZER_DEFAULT_HH__