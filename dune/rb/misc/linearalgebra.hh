#ifndef LINEARALGEBRA_22JWC23P
#define LINEARALGEBRA_22JWC23P

// we use std::accumulate and std::for_each
#include <numeric>
#include <algorithm>

// include lapack
#include "clapack.h"

#include "discfunclist/discfunclistsubsetwrapper.hh"
#include "cwrapper/cmatrixwrapper.hh"
#include "../rbasis/gramian/gramianpipeline.hh"

#include <dune/fem/operator/lagrangeinterpolation.hh>
#include <dune/fem/operator/projection/l2projection.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/space/lagrangespace.hh>

#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
#include <boost/test/unit_test.hpp>
#endif


namespace Dune {
namespace RBFem {
namespace LA {

  namespace PCA {
    struct Subtract {
      Subtract(double sub):sub_(sub){};
      void operator() (double& x) {
        x -= sub_;
      }
    private:
      double sub_;
   };
  }

/** @class Algorithms
 * @brief Library of linear algebra algorithms
 *
 *
 *  @rbparam{pca.subsetwrapperstepsize, size of subsetwrappers to be used for gramian computations, 1}
 *  @rbparam{gramianpipeline.blocksize, blocks hold in memory parallely during gramian computations, 10}
 */
template<class ServerImp, class DiscFuncSpaceImp>
class Library
{
private:
  typedef ServerImp                                                  ServerType;
  typedef DiscFuncSpaceImp                                           DiscFuncSpaceType;
public:
  Library(DiscFuncSpaceType & space)
    : space_(space)
  {
    typedef typename DiscFuncSpaceType :: GridPartType               GridPartType;
    typedef typename DiscFuncSpaceType :: BaseFunctionSetType        BaseFunctionSetType;
    typedef typename DiscFuncSpaceType :: RangeType                  RangeType;

    const GridPartType & gridPart = space_.gridPart();
    squareBaseFuncNorms_.clear();

    squareBaseFuncNorms_.resize(space_.size());

    typedef typename GridPartType :: template Codim< 0 >
              :: IteratorType                                        IteratorType;
    //loop over grid
    IteratorType end = gridPart.template end<0>();
    for (IteratorType it = gridPart.template begin<0>(); it != end; ++it)
    {
      //get baseFunctionSet for this entity
      BaseFunctionSetType baseFuncSet = space_.baseFunctionSet(*it);
      //loop over all base functions
      unsigned int numBaseFuncs = baseFuncSet.numBaseFunctions();
      assert(numBaseFuncs < squareBaseFuncNorms_.size());
      for (unsigned int i=0; i!=numBaseFuncs; ++i)
      {
        double entityIntegral=0.0;
        //get a quadrature for this entity
        int order_ = space_.order(); //order for the quadrature
        CachingQuadrature<GridPartType, 0> quadrature(*it, 2*order_+1);

        //loop over all quadrature points
        for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint) {
          double integrationElement = it->geometry().integrationElement(quadrature.point(quadPoint));
          double weight = quadrature.weight(quadPoint);
          RangeType baseFunctionValue;
          baseFuncSet.evaluate(i, quadrature.point(quadPoint), baseFunctionValue);
          entityIntegral += integrationElement * weight * baseFunctionValue * baseFunctionValue;
        } //loop over all quadrature points
        //accumulate integral for this base function
        squareBaseFuncNorms_[space.mapToGlobal(*it, i)] += entityIntegral;
      } // loop over all base functions
    } //loop over grid

#ifdef RB_DEBUG //give back some debugging information

    int size = squareBaseFuncNorms_.size();
    std :: ostringstream oss;
    oss << "The square basefunction norms are: \n";
    for (int i=0; i!=size; ++i)
      oss << squareBaseFuncNorms_[i] << " \n";
    ServerType :: printStatusMsg(oss.str());

#endif
  }


  const std::vector<double> & squareBaseFuncNorms() const
  {
    return squareBaseFuncNorms_;
  }

  /** @brief perform the Gram-Schmidt algorithm
   *
   *  Orthonormalize the space spanned by discrete functions
   *  given as a discrete function list.
   *
   *  @param[in, out]  funcs the list of functions  that span the
   *                         space you want to be orthonormalized.
   *                         These functions are overwritten with
   *                         the orthonormalized ones.
   */
  template<class DiscFuncListType >
  void gram_schmidt( DiscFuncListType &funcs ) const
  {
    typedef typename DiscFuncListType :: DiscreteFunctionSpaceType   DiscreteFunctionSpaceType;
    typedef typename DiscFuncListType :: DiscreteFunctionType        DiscreteFunctionType;

    const DiscreteFunctionSpaceType & discFuncSpace = funcs.space();
    //if U is empty, just leave it the way it is
    if (funcs.size()>0)
    {
      //two buffer functions
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old", discFuncSpace);

      int uSize = funcs.size();
      /*loop over funcs, every function in funcs is replaced with its projection*/
      for (int i=0; i < uSize; ++i)
      {
        DiscreteFunctionType sum("sum", discFuncSpace);
        sum.clear();
        buffer_old.clear();
        funcs.getFunc(i, buffer_old);
        //perfom the gram-schmidt algorithm
        for (int j=0; j < i; ++j)
        {
          buffer_ortho.clear();
          funcs.getFunc(j, buffer_ortho);
          double sp = scalarProduct(buffer_old, buffer_ortho);
          buffer_ortho *= sp;
          sum += buffer_ortho;
        }//loop over baseFuncs

        buffer_old -= sum;
        double norm = l2norm(buffer_old);
        if(norm < 1e-10)
        {
          std::ostringstream oss;
          oss << "Warning: Matrix seems to be singular. Gram-Schmidt might produce bad results!\n";
          oss << "         Last added component has norm: " << norm << "\n";
          ServerType::printWarnMsg(oss.str());
        }
        buffer_old /= norm;
        funcs.setFunc(i, buffer_old);
      }//loop over funcs
    } //if baseFuncs.size>0

#ifdef RB_DEBUG
    printf("After gram schmidt, the space spanned by 'funcs' consists\
           of the functions with the following dofs:\n");
    int funcsSize = funcs.size();
    DiscreteFunctionType buffer1("buffer1", discFuncSpace);
    for (int i=0; i!=funcsSize; ++i) {
      buffer1.clear();
      funcs.getFunc(i, buffer1);
      printf("discrete function number %d:\n", i);
      ConstDofIterator end = buffer1.dend();
      for (ConstDofIterator dof = buffer1.dbegin();
           dof != end; ++dof)
        printf(" %4.4f ", *dof);
      printf("\n");
    }
#endif

  }//void gram_schmidt

  /** @brief compute the L1 norm of a discrete function.
   *
   *  @param[in]  func     the discrete function
   *  @return     returns  the L1 norm
   */
  template<class DiscreteFunctionType>
  double l1norm(const DiscreteFunctionType &func) const
  {
    typedef typename DiscFuncSpaceType :: GridPartType               GridPartType;
    typedef typename DiscFuncSpaceType :: RangeType                  RangeType;
    typedef typename GridPartType :: template Codim< 0 >
              :: IteratorType                                        IteratorType;

    const GridPartType & gridPart = space_.gridPart();

    double integral=0.0;
    IteratorType end = gridPart.template end<0>();
    IteratorType it = gridPart.template begin<0>();
    for (; it != end; ++it) {
      //get baseFunctionSet for this entity
      typename DiscreteFunctionType::LocalFunctionType lf = func.localFunction(*it);

      //get a quadrature for this entity
      int order_ = space_.order(); //order for the quadrature
      CachingQuadrature<GridPartType, 0> quadrature(*it, 2*order_+1);

      //loop over all quadrature points
      for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint) {
        double integrationElement = it->geometry().integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);
        RangeType ret;
        lf.evaluate(quadrature.point(quadPoint), ret);
        integral += integrationElement * weight * fabs(ret[0]) ;
      } //loop over all quadrature points
      //accumulate integral for this base function
    } //loop over grid
    return integral;
  } // double l1norm()

  /** @brief compute the L2 norm of a discrete function.
   *
   *  @param[in]  func     the discrete function
   *  @return     returns  the L2 norm
   */
  template<class DiscreteFunctionType>
  inline double l2norm(const DiscreteFunctionType &func) const
  {
    return std::sqrt(scalarProduct(func, func));
  }

  /** @brief pca fixspace algorithm
   *
   *  @note This uses Lapacks dsyev.
   *
   *  @param[in]        U          a list of functions, a function in span(U) will
   *                               be used to expand the orthonormal space
   *                               specified by princComp
   *  @param[in,out]    princComp  the principal components, ordered by their
   *                               significance. If the value is not empty, the
   *                               principal components are added with respect to
   *                               the space spanned by the given functions. In
   *                               this case, the given functions need to be
   *                               orthonormal. @attention There is no check for
   *                               orthonormality.
   *  @param[in] k                 Number of principal components to add.
   *  @param[in] desiredPercentage The percentage of total variation that the
   *                               resulting principal components should
   *                               contribute. This value is only used if k==-1.
   *
   *  @tparam DiscreteFunctionListType1 type of function list for input
   *  @tparam DiscreteFunctionListType2 type of function list for output
   */
  template< class DiscreteFunctionListType1, class DiscreteFunctionListType2>
  int pca(DiscreteFunctionListType1 &U,
           DiscreteFunctionListType2 &princComp,
           const int    k,
           const double desiredPercentage=0.9) const
  {
    //assert(desiredPercentage<=1.0 && desiredPercentage>=0);
    typedef typename DiscreteFunctionListType1 :: DiscreteFunctionType  DiscreteFunctionType;
    /**********************************************************************************
     * 1 Wrap trajectory U to run PCA on a subset only                                *
     **********************************************************************************/
    // Wrap U to run PCA only on a subset
    unsigned int subSetStepSize = 
      Parameter::getValue<unsigned int>("pca.subsetwrapperstepsize", 1);

    // choose a subset of the trajectory
    typedef DiscFuncListSubSetWrapper< DiscreteFunctionListType1 >     SubSetWrapper;
    SubSetWrapper subSetWrapper(U, subSetStepSize);
    unsigned int uSize = subSetWrapper.size();
    assert(uSize>0);


    /**********************************************************************************
     * 2 Subtract Mean                                                                *
     **********************************************************************************/

    // Calculate the deviations from the mean
    DiscreteFunctionType temp("temp", U.space());
    DiscreteFunctionType temp2("temp2", U.space());

    if (Parameter::getValue<bool>("pca.substractMean", false))
    {
      for (unsigned int i=0; i<uSize; ++i)
      {
        temp.clear();
        subSetWrapper.getFunc(i, temp);

        // calc the mean
        double mean = std::accumulate(temp.dbegin(), temp.dend(), 0.0);
        mean /= temp.size();
        PCA::Subtract subtract(mean);
        std::for_each(temp.dbegin(), temp.dend(), subtract);
        subSetWrapper.setFunc(i, temp);
      }
    }


    /*********************************************************************************
     * 3 Orthonormalize new trajectory with respect to old principal components      *
     *********************************************************************************/
    if(princComp.size() > 0)
    {
      //just to be sure that the old functions are orthonormal
      gram_schmidt(princComp);

      //orthonormalize subSetWrapper with respect to oldSpace()
      projectComplement(subSetWrapper, princComp);
    }

    /***************************************************************************
     * 4 Compute Gram Matrix                                                   *
     ***************************************************************************/

    DiscreteFunctionType buffer1( "buffer1", U.space() );

    // Construct the gramian pipeline for computation of gram matrix
    typedef GramianPipeline< SubSetWrapper, RB::CMatrixWrapper >  GramianPipelineType;
    typedef typename GramianPipelineType :: OpHandle          OpHandle;

    const unsigned int blockSize = Parameter::getValue<int>("gramianpipeline.blocksize", 8);
    GramianPipelineType pipeline(subSetWrapper, blockSize);

    OpHandle hIdOp = pipeline.getIdentityHandle();
    // *4a* initialize storage for gram matrix and wrap it with CMatrixWrapper
    unsigned int gramMatrixSize = uSize * uSize;
    double * gramMatrix = new double[gramMatrixSize];
    RB::CMatrixWrapper gramMatrixWrapper(&gramMatrix[0], uSize, uSize, 0.0);

    // *4b* register gram matrix computation
    pipeline.addGramMatrixComputation(hIdOp, hIdOp, gramMatrixWrapper);
    // *4c* execute pipeline
    pipeline.run();

    // *4d* save diagonal matrix entries, will be needed later
    std::vector<double> stdDeviation;
    for (unsigned int i=0; i<uSize; ++i) {
#ifndef NDEBUG
      if(gramMatrix[i*uSize+i] < 0.0)
      {
        stdDeviation.push_back(0.0);
        std::ostringstream oss;
        oss << "Warning: eigenvalue no. " << uSize - 1 << " is negative!";
        ServerType :: printWarnMsg(oss.str());
      }
/*      assert(gramMatrix[i*uSize+i]>0);*/
#endif
      stdDeviation.push_back(sqrt(gramMatrix[i*uSize+i]));
    }


    /*****************************************************************************
     * 5 Compute Eigenvalues with LAPACK                                         *
     *****************************************************************************/
#if defined(HAVE_LAPACK) && HAVE_LAPACK
    double eigenvalues[uSize];
    //calculate eigenvalues

    /*  lapack_ptrdiff_t is set in clapack.c to the correct argument type by the
     *  used lapack implementation (either distributed lapack or Matlab's
     *  lapack)*/

    /*size for the workspace used by dsyev, set to
      -1 first so dsyev will guess the right size*/
    typedef RB::Lapack :: lapack_ptrdiff_t ptrdiffType;
    ptrdiffType lwork = -1;
    ptrdiffType n = uSize; //order of the gram matrix
    char jobz = 'v'; //calculate eigenvalues and eigenvectors
    char uplo = 'l'; //use upper triangular matrix

    //allocate memory for lapack, first with size 1
    double* workHere = new double[1];
    //return value for lapack
    ptrdiffType info; //info = 0 means everthing worked out

    //call Lapack
    RB::Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
                     &eigenvalues[0], &workHere[0], &lwork, &info);

    /*after calling dsyev with lwork=-1 workHere[0] contains the
      right size for workHere*/
    lwork = static_cast<ptrdiffType>(workHere[0]);
    assert(lwork>0);
    delete[] workHere;
    workHere = new double[lwork];
    //finally call lapack and compute the eigenvalues
    RB::Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
                     &eigenvalues[0], &workHere[0], &lwork, &info);
    unsigned int L=1;
    if (info == 0) {
      if( k < 0 )
      {
        /* Decide, how many functions are needed to achieve the desired percentage
           of total variation */
        // the number of functions to be added
        // compute the total variation covered by ALL principal components
        double totalVariation = 0.0;
        for (unsigned int i=0; i < uSize; ++i)
        {
          if(eigenvalues[i] > 1e-11)
            totalVariation += eigenvalues[i];
          if(eigenvalues[i] < 0)
          {
            std::ostringstream oss;
            oss << "Warning: negative eigenvalue during pca encountered: " << eigenvalues[i] << "\n";
            ServerType :: printWarnMsg(oss.str());
          }
        }
        // compute the percentage of the total variation that the first pc covers
        double currentPercentage = eigenvalues[uSize-1]/totalVariation;

        // while the desired percentage is not reached, enlarge the set of pc's that
        // will be returned
        while (currentPercentage<desiredPercentage && L<uSize) {
          currentPercentage += eigenvalues[uSize-1-L]/totalVariation;
          ++L;
#ifdef RB_DEBUG
          if (L==uSize) {
            ServerType :: printStatusMsg("Reached maximum number of principal components!\n");
          }
#endif
        }
#ifdef RB_DEBUG
        std::ostringstream oss;
        oss << "We need " << L
          << " principal components to cover the desired total variation of "
          << desiredPercentage << " percent.\n";
        ServerType :: printStatusMsg(oss.str());
#endif
      }
      else
      {
        L = (unsigned)k;
      }

      /***********************************************************************
       * 6 add principal components                                          *
       ***********************************************************************/
      for (unsigned int l=0; l<L; ++l)
      {
        buffer1.clear();
        typedef typename DiscreteFunctionType :: DofIteratorType       DofIterator;
        typedef typename DiscreteFunctionType :: ConstDofIteratorType  ConstDofIterator;
        //get eigenvector to (uSize-l)-largest eigenvalue
        double dof[uSize];
        /* copy from array of double to DiscreteFunction,
           remember: last vector is the vector to the largest eigenvalue!*/
        int j=0;
        for (unsigned int i=uSize*(uSize-l-1); i!=uSize*(uSize-l); ++i, ++j) {
          dof[j] = gramMatrix[i];
/*        for (unsigned int i = 0; i < uSize; i++) {
 *          gramMatrixWrapper.get(i, uSize-1, dof[i]);*/
        }

        DiscreteFunctionType newBaseFunc("newBaseFunc", U.space());
        newBaseFunc.clear();
        double *eigenvecDof = dof;

        //project the eigenvector back to the span of subSetWrapper
        for (unsigned int i=0; i<uSize; ++i, ++eigenvecDof) {
          buffer1.clear();
          subSetWrapper.getFunc(i, buffer1);
          //"normalize the data set with respect to its variance"
          *eigenvecDof/=stdDeviation[i];
          //          printf("Standart deviation is: %E \n", stdDeviation[i]);
          DofIterator newBaseFuncDof = newBaseFunc.dbegin();
          ConstDofIterator uDof = buffer1.dbegin();

          for (ssize_t j=0; j< buffer1.size(); ++j, ++newBaseFuncDof, ++uDof)
          {
            *newBaseFuncDof += (*eigenvecDof)*(*uDof);
          }
        }

        // normalize the new base function
        //! \todo Do we need this?
        newBaseFunc /= std::sqrt(scalarProduct(newBaseFunc, newBaseFunc));
        princComp.push_back(newBaseFunc);
#ifdef RB_DEBUG
        printf("The function with the following dofs was successfully added to the rb space:\n");
        typedef DiscreteFunctionType :: ConstDofIteratorType           ConstDofIterator;
        ConstDofIterator end = newBaseFunc.dend();
        for (ConstDofIterator out = newBaseFunc.dbegin(); out != end; ++ out)
          printf("%E \n", *out);
#endif //ifdef RB_DEBUG
      }
    } // if (info == 0)
    else if (info<0) {
      std::stringstream errormsg;
      errormsg << "Argument " << info << " of gram matrix had illegal value!";
      ServerType :: printErrMsg( errormsg.str() );
    }
    else if (info>0) {
      std::stringstream errormsg;
      ServerType :: printErrMsg("Eigenvalue algorithm failed to converge!");
    }
#else // if HAVE_LAPACK is not defined

    ServerType :: printErrMsg("You need LAPACK!");
#error "You need Lapack and the macro \"HAVE_LAPACK\" defined to use this!"

#endif //ifdef LAPACK

    delete[] workHere;
    delete[] gramMatrix;

    return L;

  } //pca_fixspace

  /** @brief project functions in U onto space spanned by baseFuncs
   *
   *  Project each function in U onto the space spanned by the functions in
   *  baseFuncs.
   *
   *  @param[in, out]  U          the list of functions you want to be projected
   *  @param[in]       baseFuncs  a list of functions that span the space onto
   *                              which U is to be projected
   */
  template<class DiscFuncListType1, class DiscFuncListType2>
  void project( DiscFuncListType1 &U,
                const DiscFuncListType2 &baseFuncs) const
  {
    typedef typename DiscFuncListType1 :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
    typedef typename DiscFuncListType1 :: DiscreteFunctionType       DiscreteFunctionType;
    const DiscreteFunctionSpaceType & discFuncSpace = U.space();
    //if baseFuncs is empty, just leave U the way it is
    if (baseFuncs.size()>0) {
      //two buffer functions
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old",  discFuncSpace);

      int baseFuncsSize = baseFuncs.size();
      int uSize = U.size();

      /*loop over U, every function in U is replaced with its projection*/
      for (int i=0; i!=uSize; ++i) {
        DiscreteFunctionType sum("sum", discFuncSpace);
        sum.clear();
        buffer_old.clear();
        U.getFunc(i, buffer_old);
        //perfom the projection algorithm
        for (int j=0; j!=baseFuncsSize; ++j)
        {
          buffer_ortho.clear();
          baseFuncs.getFunc(j, buffer_ortho);
          double sp = scalarProduct(buffer_old, buffer_ortho);
          buffer_ortho *= sp;
          sum += buffer_ortho;
        } //loop over baseFuncs
        U.setFunc(i, sum);
      } //loop over U
    } //if baseFuncs.size>0

  } // void project

  /** @brief project functions in U onto complement space of baseFuncs
   *
   *  Project each function in U onto the complementary space of the space
   *  spanned by the functions in baseFuncs.
   *
   *  @param[in, out]  U          the list of functions you want to be projected
   *  @param[in]       baseFuncs  a list of functions that span the space onto
   *                              which U is to be projected
   */
  template<class DiscFuncListType1, class DiscFuncListType2>
  void projectComplement( DiscFuncListType1 &U,
                          const DiscFuncListType2 &baseFuncs) const
  {
    typedef typename DiscFuncListType1 :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
    typedef typename DiscFuncListType1 :: DiscreteFunctionType       DiscreteFunctionType;
    const DiscreteFunctionSpaceType & discFuncSpace = U.space();
    //if baseFuncs is empty, just leave U the way it is
    if (baseFuncs.size()>0) {
      //two buffer functions
      DiscreteFunctionType buffer_ortho("buffer_ortho", discFuncSpace);
      DiscreteFunctionType buffer_old("buffer_old",  discFuncSpace);

      int baseFuncsSize = baseFuncs.size();
      int uSize = U.size();

#ifdef RB_DEBUG
      printf("Before projection onto 'baseFuncs'\n");
      DiscreteFunctionType buffer1("buffer1", discFuncSpace);
      for (int i=0; i!=uSize; ++i) {
        buffer1.clear();
        U.getFunc(i, buffer1);
        printf("discrete function number %d:\n", i);
        ConstDofIterator end = buffer1.dend();
        for (ConstDofIterator dof = buffer1.dbegin();
             dof != end; ++dof)
          printf(" %4.4f ", *dof);
        printf("\n");
      }
#endif // ifdef RB_DEBUG

      /*loop over U, every function in U is replaced with its projection*/
      for (int i=0; i!=uSize; ++i) {
        DiscreteFunctionType sum("sum", discFuncSpace);
        sum.clear();
        buffer_old.clear();
        U.getFunc(i, buffer_old);
        //perfom the projection algorithm
        for (int j=0; j!=baseFuncsSize; ++j)
        {
          buffer_ortho.clear();
          baseFuncs.getFunc(j, buffer_ortho);
          double sp = scalarProduct(buffer_old, buffer_ortho);
          buffer_ortho *= sp;
          sum += buffer_ortho;
        } //loop over baseFuncs
        buffer_old -= sum;
        U.setFunc(i, buffer_old);
      } //loop over U
    } //if baseFuncs.size>0

#ifdef RB_DEBUG
    printf("After projection onto the space spanned by 'baseFuncs', \
           U consists of the functions with the following dofs:\n");
    int uSize = U.size();
    DiscreteFunctionType buffer1("buffer1", discFuncSpace);
    for (int i=0; i!=uSize; ++i) {
      buffer1.clear();
      U.getFunc(i, buffer1);
      printf("discrete function number %d:\n", i);
      ConstDofIterator end = buffer1.dend();
      for (ConstDofIterator dof = buffer1.dbegin();
           dof != end; ++dof)
        printf(" %4.4f ", *dof);
      printf("\n");
    }
#endif // ifdef RB_DEBUG

  } // void projectComplement


  /** @brief compute the L2 scalar product of two functions
   *
   *  \note You need to run gen_model_data before running this!
   *
   *  @param[in] func1 the first function
   *  @param[in] func2 the second function
   *  @return returns the L2 scalar product
   */
  template <class DiscFuncType1, class DiscFuncType2>
  inline double scalar_product(const DiscFuncType1 &func1,
                               const DiscFuncType2 &func2) const
  {
    assert(func1.size()==func2.size());

    //perform scalar product
    double result = 0.0;

    typedef typename DiscFuncType1 :: ConstDofIteratorType           ConstDofIteratorType1;
    typedef typename DiscFuncType2 :: ConstDofIteratorType           ConstDofIteratorType2;

    ConstDofIteratorType2 func2it = func2.dbegin();
    int i=0;
    ConstDofIteratorType1 func1it = func1.dbegin();
    for (; func1it!= func1.dend(); ++func1it, ++func2it, ++i)
    {
      result += (*func1it) * (*func2it) * squareBaseFuncNorms_[i];
    }//end of sum loop

    // #ifdef RB_DEBUG
    //     printf("Result of scalar product was %E\n", result);
    // #endif

    return result;
  } // double scalar_product()


  template<class DiscreteFunctionType>
  double scalarProduct(DiscreteFunctionType & df1, DiscreteFunctionType & df2) const
  {
    double res = 0.0;
    typedef typename DiscFuncSpaceType :: GridPartType               GridPartType;
    typedef typename DiscFuncSpaceType :: RangeType                  RangeType;

    typedef typename GridPartType :: template Codim< 0 >
              :: IteratorType                                        IteratorType;
    typedef typename IteratorType :: Entity                          EntityType;

    const GridPartType & gridPart = space_.gridPart();

    IteratorType gridEnd = gridPart.template end<0>();
    for( IteratorType gridIt = gridPart.template begin<0>(); gridIt != gridEnd ; ++gridIt )
    {
      EntityType & en = *gridIt;
      typedef typename DiscreteFunctionType :: LocalFunctionType     LocalFunction;
      typedef typename EntityType :: Geometry                        GeometryType;
      LocalFunction lf1 = df1.localFunction(en);
      LocalFunction lf2 = df2.localFunction(en);
      int order_ = 2*space_.order()+1; //order for the quadrature
      CachingQuadrature<GridPartType, 0> quadrature(en, order_);
      for (unsigned int quadPoint=0; quadPoint != quadrature.nop(); ++quadPoint)
      {
        double integrationElement = en.geometry().integrationElement(quadrature.point(quadPoint));
        double weight = quadrature.weight(quadPoint);

        RangeType ret1;
        RangeType ret2;
        lf1.evaluate( quadrature.point(quadPoint), ret1 );
        lf2.evaluate( quadrature.point(quadPoint), ret2 );
        res += integrationElement * weight * ret1[0] * ret2[0];
      }
    }
    return res;
  }

  template <class DiscFuncListType>
#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
  boost::test_tools::predicate_result
#else
  bool
#endif
  orthonormality_test(DiscFuncListType &funcList,
                      RB::CMatrixWrapper & output,
                      const double epsilon = 1e-10) const
  {
    const unsigned int flSize = funcList.size();
    assert(funcList.size() == output.rows() && funcList.size() == output.cols());

    typedef GramianPipeline< DiscFuncListType, RB::CMatrixWrapper >      GramianPipelineType;
    typedef typename GramianPipelineType :: OpHandle                 OpHandleType;
    GramianPipeline< DiscFuncListType, RB::CMatrixWrapper > gp(funcList, 2);

    OpHandleType id = gp.getIdentityHandle();
    gp.addSymmetricGramMatrixComputation(id, id, output);
    gp.run();

    double * ref_matrix = new double[flSize*flSize];
    RB::CMatrixWrapper reference(ref_matrix, flSize, flSize, 0.0);
    for (unsigned int i = 0; i < flSize; i++) {
      reference.set(i,i,1.0);
    }

#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
    boost::test_tools::predicate_result
#else
    bool
#endif
      cres = output.compare(reference, epsilon);

#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
    if( !cres )
    {
      boost::test_tools::predicate_result res(false);
      res.message () << "DiscFuncList is not orthonormal.\n"
        << "Expected identity matrix, but:\n\n"
        << cres.message();
      return res;
    }
    else
    {
      boost::test_tools::predicate_result res(true);
      return res;
    }
#else
    return cres;
#endif
  }

private:
  const DiscFuncSpaceType   &space_;
  std::vector<double>        squareBaseFuncNorms_;

}; // LinAlg class

struct Interpolation
{
  template<class DiscreteFunctionSpaceType>
  struct InterpolationHelper
  {
    template
      <class ArgFuncType,
       class DiscreteFunctionType >
    static void
    interpolate(const ArgFuncType& argFunc, DiscreteFunctionType & interpolant) {};
  };

  template<class FSType, class GPType, int polorder>
  struct InterpolationHelper<FiniteVolumeSpace<FSType, GPType, polorder> >
  {
    template
      <class ArgFuncType,
       class DiscreteFunctionType >
    static void
    interpolate(const ArgFuncType& argFunc, DiscreteFunctionType & interpolant)
    {
      typedef L2Projection< double, double, ArgFuncType,
                            DiscreteFunctionType >                   L2ProjectionType;
      L2ProjectionType l2pro(polorder);
      l2pro( argFunc, interpolant );
    }
  };

  template<class FSType, class GPType, int polorder>
  struct InterpolationHelper<LagrangeDiscreteFunctionSpace<FSType, GPType, polorder> >
  {
    template
      <class ArgFuncType,
       class DiscreteFunctionType >
    static void
    interpolate(const ArgFuncType& argFunc, DiscreteFunctionType & interpolant)
    {
      LagrangeInterpolation< DiscreteFunctionType >
        :: interpolateFunction( argFunc, interpolant );
    }
  };

  template
     <class ArgFuncType,
      class DiscreteFunctionType >
  static void
  interpolate(const ArgFuncType& argFunc, DiscreteFunctionType & interpolant)
  {
    InterpolationHelper<typename DiscreteFunctionType :: DiscreteFunctionSpaceType>
                          :: interpolate(argFunc, interpolant);
  }
}; // end of Interpolate

}  // namespace LA
} // namespace RBFem
} // namespace Dune


#endif /* end of include guard: LINEARALGEBRA_22JWC23P */

/* vim: set sw=2 et foldmethod=syntax: */
