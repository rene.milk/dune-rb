/* This is generally a copy of the /usr/include/assert.h, but overwrites the
 * assert macro in case we are inside of a mex-file such that a mexErrMsgTxt is
 * send instead of an abort that would exit the entire Matlab session instead.
 * 
 * In order to use this macro simply add the path where this file lives to the
 * beginning of your CPPFLAGS in your Makefile */

#ifndef __DUNE_RB_ASSERT_H__
#define __DUNE_RB_ASSERT_H__

#ifdef MATLAB_MEX_FILE

#include <sys/cdefs.h>
#include <mex.h>

static void __assert_matlab(__const char * msg, __const char * file, unsigned int line)
{
  char __assert_text[500];
  snprintf(__assert_text, 500, "Assertion failed: %s in %s:%i", msg, file, line);
  mexErrMsgTxt(__assert_text);
}
#endif

#endif

#ifdef	_DUNE_RB_ASSERT_H

# undef	_DUNE_RB_ASSERT_H
# undef	assert
# undef __ASSERT_VOID_CAST

#endif

#define	_DUNE_RB_ASSERT_H	1
#include <features.h>

#if defined __cplusplus && __GNUC_PREREQ (2,95)
# define __ASSERT_VOID_CAST static_cast<void>
#else
# define __ASSERT_VOID_CAST (void)
#endif

/* void assert (int expression);

   If NDEBUG is defined, do nothing.
   If not, and EXPRESSION is zero, print an error message and abort.  */

#ifdef	NDEBUG

# define assert(expr)		(__ASSERT_VOID_CAST (0))

/* void assert_perror (int errnum);

   If NDEBUG is defined, do nothing.  If not, and ERRNUM is not zero, print an
   error message with the error text for ERRNUM and abort.
   (This is a GNU extension.) */

#else /* Not NDEBUG.  */

#ifdef MATLAB_MEX_FILE
#define assert(expr) \
  ((expr) \
   ? __ASSERT_VOID_CAST (0) \
   : __assert_matlab(__STRING(expr), __FILE__, __LINE__) )

#else /* Not MATLAB_MEX_FILE */

__BEGIN_DECLS

/* This prints an "Assertion failed" message and aborts.  */
extern void __assert_fail (__const char *__assertion, __const char *__file,
			   unsigned int __line, __const char *__function)
     __THROW __attribute__ ((__noreturn__));

/* Likewise, but prints the error text for ERRNUM.  */
extern void __assert_perror_fail (int __errnum, __const char *__file,
				  unsigned int __line,
				  __const char *__function)
     __THROW __attribute__ ((__noreturn__));


/* The following is not at all used here but needed for standard
   compliance.  */
extern void __assert (const char *__assertion, const char *__file, int __line)
     __THROW __attribute__ ((__noreturn__));


__END_DECLS

# define assert(expr)							\
  ((expr)								\
   ? __ASSERT_VOID_CAST (0)						\
   : __assert_fail (__STRING(expr), __FILE__, __LINE__, __ASSERT_FUNCTION))

# ifdef	__USE_GNU
#  define assert_perror(errnum)						\
  (!(errnum)								\
   ? __ASSERT_VOID_CAST (0)						\
   : __assert_perror_fail ((errnum), __FILE__, __LINE__, __ASSERT_FUNCTION))
# endif

/* Version 2.4 and later of GCC define a magical variable `__PRETTY_FUNCTION__'
   which contains the name of the function currently being defined.
   This is broken in G++ before version 2.6.
   C9x has a similar variable called __func__, but prefer the GCC one since
   it demangles C++ function names.  */
# if defined __cplusplus ? __GNUC_PREREQ (2, 6) : __GNUC_PREREQ (2, 4)
#   define __ASSERT_FUNCTION	__PRETTY_FUNCTION__
# else
#  if defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
#   define __ASSERT_FUNCTION	__func__
#  else
#   define __ASSERT_FUNCTION	((__const char *) 0)
#  endif
# endif

#endif /* MATLAB_MEX_FILE */

#endif /* NDEBUG */

