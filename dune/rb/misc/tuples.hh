#ifndef TUPLES_ZZLVAXVJ
#define TUPLES_ZZLVAXVJ


/* some day the following ought to be unneccessary!!!
 *
 * This hack is necessary, because the standard headers <tr1/tuple> and <tuple>
 * are incompatible with recursively defined structures like ForEachValue or
 * ForEachValuePair which are heavily used in dune-fem!
 *
 * So we need to manually turn of the inclusion of the headers, at the
 * beginning of every program!
 *
 * To become independent of all future development of tuple headers in
 * dune-common and dune-fem, we copied a version that fits all our needs at the
 * end of this file.
 */


// START OF STUPID HACK

#if defined(DUNE_FEM_UTILITY_HH)
#error "dune/rb/misc/tuples.hh needs to be included before dune/fem/utility.hh"
#endif

/*#ifdef HAVE_TUPLE
 *#define UNDEFINED_HAVE_TUPLE HAVE_TUPLE
 *#undef HAVE_TUPLE
 *#endif

 *#ifdef HAVE_TR1_TUPLE
 *#define UNDEFINED_HAVE_TR1_TUPLE HAVE_TR1_TUPLE
 *#undef HAVE_TR1_TUPLE
 *#endif*/

#include <dune/fem/misc/utility.hh>

/*#ifdef UNDEFINED_HAVE_TUPLE
 *#define HAVE_TUPLE UNDEFINED HAVE_TUPLE
 *#undef UNDEFINED_HAVE_TUPLE
 *#endif

 *#ifdef UNDEFINED_HAVE_TR1_TUPLE
 *#define HAVE_TR1_TUPLE UNDEFINED HAVE_TR1_TUPLE
 *#undef UNDEFINED_HAVE_TR1_TUPLE
 *#endif*/

// END OF REALLY STUPID HACK


namespace Dune {

namespace RB {

namespace Tuple {

  template<class T>
  struct TupleAccessTraits
  {
    typedef typename ConstantVolatileTraits<T>::ConstType& ConstType;
    typedef T& NonConstType;
    typedef const typename ConstantVolatileTraits<T>::UnqualifiedType& ParameterType;
  };

  template<class T>
  struct TupleAccessTraits<T*>
  {
    typedef typename ConstantVolatileTraits<T>::ConstType* ConstType;
    typedef T* NonConstType;
    typedef T* ParameterType;
  };

  template<class T>
  struct TupleAccessTraits<T&>
  {
    typedef typename ConstantVolatileTraits<T>::ConstType& ConstType;
    typedef T& NonConstType;
    typedef T& ParameterType;
  };


/* This is a copy of the dune/fem/femtuples.hh in a new namespace!!! */


  /**
   * @brief An empty class.
   */
  struct Nil
  {};

  namespace
  {
    inline const Nil nullType()
    {
      return Nil();
    }
  }

  /**
   * @brief A Tuple consisting of two objects.
   *
   * This is similar to std::pair
   */
  template<typename T1, typename TT>
  struct Pair
  {
    /**
     * @brief The type of the first field.
     */
    typedef T1 Type1;

    /**
     * @brief The type of the second field.
     */
    typedef TT Type2;
//     enum{
//     /**
//      * @brief The number of values we hold.
//      */
//       values = 2;
//     };

    /**
     * @brief Constructor
     *
     * @param t1 The value of the first field.
     * @param t2 The value of the second field.
     * @param t3 The value of the third field.
     * @param t4 The value of the fourth field.
     * @param t5 The value of the fifth field.
     * @param t6 The value of the sixth field.
     * @param t7 The value of the seventh field.
     * @param t8 The value of the eightth field.
     * @param t9 The value of the nineth field.
     */
    template<typename T2, typename T3, typename T4, typename T5,
       typename T6, typename T7, typename T8, typename T9>
    Pair(typename TupleAccessTraits<T1>::ParameterType t1, T2& t2, T3& t3, T4& t4, T5& t5,
   T6& t6, T7& t7, T8& t8, T9& t9);

    /**
     * @brief Constructor
     *
     * @param t1 The value of the first field.
     * @param t2 The value of the second field.
     */
    Pair(typename TupleAccessTraits<Type1>::ParameterType t1, TT& t2);

    Pair();

    /**
     * @brief Copy Constructor for implicit type conversion
     * @param other The Tuple to copy.
     */
    template<typename U1, typename U2>
    Pair(const Pair<U1,U2>& other);

    /**
     * @brief Assignment operator for implicit type conversion
     * @param other The Tuple to assign.
     */
    template<typename U1, typename U2>
    Pair& operator=(const Pair<U1,U2>& other);

    Pair& operator=(const Pair& other);

    /**
     * @brief Get the first value
     * @return The first value
     */
    typename TupleAccessTraits<Type1>::NonConstType first();

    /**
     * @brief Get the first value
     * @return The first value
     */
    typename TupleAccessTraits<Type1>::ConstType
    first() const;

    /**
     * @brief Get the second value
     * @return The second value
     */
    typename TupleAccessTraits<Type2>::NonConstType
    second();

    /**
     * @brief Get the second value
     * @return The second value
     */
    typename TupleAccessTraits<Type2>::ConstType
    second() const;

    /** @brief The value of the first field. */
    Type1 first_;
    /** @brief The value of the second field. */
    Type2 second_;

  };

  /**
   * @brief A Tuple consisting of one object.
   * Specialization of Pair that really is a single value.
   */
  template<typename T1>
  struct Pair<T1,Nil>
  {
    /**
     * @brief The type of the first field.
     */
    typedef T1 Type1;

    /**
     * @brief The type of the (non-existent) second field is Nil.
     * This typedef is useful in template metaprogramming, since it allows
     * you to specialise for Nil instead of Pair<T, Nil>
     */
    typedef Nil Type2;

    /**
     * @brief Constructor.
     * @param first The values for the first field.
     */
    Pair(typename TupleAccessTraits<T1>::ParameterType first, const Nil&, const Nil&, const Nil&, const Nil&,
   const Nil&, const Nil&, const Nil&, const Nil&);

    /**
     * @brief Constructor.
     * @param first The values for the first field.
     */
    Pair(typename TupleAccessTraits<T1>::ParameterType first,
   const Nil&);

    Pair();

    /**
     * @brief Copy constructor for type conversion.
     */
    template<typename T2>
    Pair(const Pair<T2,Nil>& other);

    /**
     * @brief Assignment operator for type conversion.
     */
    template<typename T2>
    Pair& operator=(const Pair<T2,Nil>& other);

    /**
     * @brief Assignment operator.
     */
    Pair& operator=(const Pair& other);

    /**
     * @brief Get the first value
     * @return The first value
     */
    typename TupleAccessTraits<Type1>::NonConstType
    first();

    /**
     * @brief Get the first value
     * @return The first value
     */
    typename TupleAccessTraits<Type1>::ConstType
    first() const;

    /** @brief The value of the first field.*/
    Type1 first_;
  };


  /**
   * @brief Converts the Tuple to a list of pairs.
   */
  template<typename T1, typename T2, typename T3, typename T4, typename T5,
     typename T6, typename T7, typename T8, typename T9>
  struct TupleToPairs
  {
    typedef Pair<T1, typename TupleToPairs<T2,T3,T4,T5,T6,T7,T8,T9,Nil>::Type > Type;
  };

  /**
   * @brief Specialization for a Tuple consisting only of one type.
   */
  template<typename T1>
  struct TupleToPairs<T1,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil>
  {
    typedef Pair<T1,Nil> Type;
  };


  /**
   * @brief A Tuple of objects.
   *
   * A maximum of 9 objects is supported.
   *
   * Use the following construction to access the individual elements.
   \code
      Tuple<std::string, float*, int> my_Tuple;

      std:string& s = get<0>(my_Tuple);
      float*      p = get<1>(my_Tuple);

      // Access the third element in a generic way
      typedef Tuple_element<2, Tuple<std::string, float*, int> >::type Type;
      Type&       i = get<2>(my_Tuple);
   \endcode
   */
  template<typename T1, typename T2 = Nil, typename T3 = Nil,
     typename T4 = Nil, typename T5 = Nil,typename T6 = Nil,
     typename T7 = Nil, typename T8 = Nil, typename T9 = Nil>
  class Tuple : public TupleToPairs<T1,T2,T3,T4,T5,T6,T7,T8,T9>::Type
  {
  public:
    //! Type of the first Pair defining the Tuple
    typedef typename TupleToPairs<T1,T2,T3,T4,T5,T6,T7,T8,T9>::Type FirstPair;

    Tuple()
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1)
      : FirstPair(t1, nullType(), nullType(), nullType(),
      nullType(), nullType(), nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
    typename TupleAccessTraits<T2>::ParameterType t2)
      : FirstPair(t1, t2, nullType(), nullType(),
      nullType(), nullType(), nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3)
      : FirstPair(t1, t2, t3, nullType(),
      nullType(), nullType(), nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4)
      : FirstPair(t1, t2, t3, t4,
      nullType(), nullType(), nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4,
          typename TupleAccessTraits<T5>::ParameterType t5)
      : FirstPair(t1, t2, t3, t4,
      t5, nullType(), nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4,
          typename TupleAccessTraits<T5>::ParameterType t5,
          typename TupleAccessTraits<T6>::ParameterType t6)
      : FirstPair(t1, t2, t3, t4,
      t5, t6, nullType(), nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4,
          typename TupleAccessTraits<T5>::ParameterType t5,
          typename TupleAccessTraits<T6>::ParameterType t6,
          typename TupleAccessTraits<T7>::ParameterType t7)
      : FirstPair(t1, t2, t3, t4,
      t5, t6, t7, nullType(),
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4,
          typename TupleAccessTraits<T5>::ParameterType t5,
          typename TupleAccessTraits<T6>::ParameterType t6,
          typename TupleAccessTraits<T7>::ParameterType t7,
          typename TupleAccessTraits<T8>::ParameterType t8)
      : FirstPair(t1, t2, t3, t4,
      t5, t6, t7, t8,
      nullType())
    {}

    Tuple(typename TupleAccessTraits<T1>::ParameterType t1,
          typename TupleAccessTraits<T2>::ParameterType t2,
          typename TupleAccessTraits<T3>::ParameterType t3,
          typename TupleAccessTraits<T4>::ParameterType t4,
          typename TupleAccessTraits<T5>::ParameterType t5,
          typename TupleAccessTraits<T6>::ParameterType t6,
          typename TupleAccessTraits<T7>::ParameterType t7,
          typename TupleAccessTraits<T8>::ParameterType t8,
          typename TupleAccessTraits<T9>::ParameterType t9)
      : FirstPair(t1, t2, t3, t4, t5, t6, t7, t8, t9)
    {}

    template<class U1, class U2>
    Tuple& operator=(const Pair<U1,U2>& other)
    {
      FirstPair::operator=(other);
      return *this;
    }
  };

  /**
   * @brief Get the type of the N-th element of the tuple.
   */
  template<int N, class Tuple>
  struct ElementType;

  template< int N, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9 >
  struct ElementType< N, Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  {
    typedef Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > TupleType;
    /**
     * @brief The type of the N-th element of the tuple.
     */
    typedef typename ElementType<N,typename TupleType::FirstPair>::type type;
    typedef typename ElementType<N,typename TupleType::FirstPair>::type Type;
  };

  template< int N, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9 >
  struct ElementType< N, const Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  : public ElementType< N, Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  {};

  template<int N, typename T1, typename T2>
  struct ElementType<N,Pair<T1,T2> >
  {
    /**
     * @brief The type of the N-th element of the tuple.
     */
    typedef typename ElementType<N-1,T2>::Type type;
    typedef typename ElementType<N-1,T2>::Type Type;
  };

  /**
   * @brief Get the type of the first element of the tuple.
   */
  template<typename T1, typename T2>
  struct ElementType<0, Pair<T1,T2> >
  {
    /**
     * @brief The type of the first element of the tuple.
     */
    typedef T1 type;
    typedef T1 Type;
  };


  /**
   * @brief Get the N-th element of a tuple.
   */
  template<int N>
  struct ElementAccess
  {
    /**
     * @brief Get the N-th element of the tuple.
     * @param tuple The tuple whose N-th element we want.
     * @return The N-th element of the tuple.
     */
    template<typename T1, typename T2>
    static inline typename TupleAccessTraits<
      typename ElementType<N,Pair<T1,T2> >::type
    >::NonConstType
    get(Pair<T1,T2>& tuple)
    {
      return ElementAccess<N-1>::get(tuple.second());
    }

    /**
     * @brief Get the N-th element of the tuple.
     * @param tuple The tuple whose N-th element we want.
     * @return The N-th element of the tuple.
     */
    template<typename T1, typename T2>
    static inline typename TupleAccessTraits<
      typename ElementType<N,Pair<T1,T2> >::type
    >::ConstType
    get(const Pair<T1,T2>& tuple)
    {
      return ElementAccess<N-1>::get(tuple.second());
    }
  };

  /**
   * @brief Get the first element of a tuple.
   */
  template<>
  struct ElementAccess<0>
  {
    /**
     * @brief Get the first element of the tuple.
     * @param tuple The tuple whose first element we want.
     * @return The first element of the tuple.
     */
    template<typename T1, typename T2>
    static inline typename TupleAccessTraits<T1>::NonConstType get(Pair<T1,T2>& tuple)
    {
      return tuple.first();
    }

    /**
     * @brief Get the first element of the tuple.
     * @param tuple The tuple whose first element we want.
     * @return The first element of the tuple.
     */
    template<typename T1, typename T2>
    static inline typename TupleAccessTraits<T1>::ConstType get(const Pair<T1,T2>& tuple)
    {
      return tuple.first();
    }
  };

  template<int i, typename T1, typename T2, typename T3, typename T4,typename T5, typename T6, typename T7, typename T8, typename T9>
  inline typename TupleAccessTraits<typename ElementType<i, Tuple<T1,T2,T3,T4,T5,T6,T7,T8,T9> >::type> :: NonConstType
  get(Tuple<T1,T2,T3,T4,T5,T6,T7,T8,T9>& t)
  {
    return ElementAccess<i>::get(t);
  }

  /**
   * @brief Template meta_programm to query the size of a Tuple
   *
   */
  template<class T>
  struct Size;

  template< class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9 >
  struct Size< Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  {
    typedef Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > TupleType;
    static const int value = Size< typename TupleType::FirstPair >::value;
  };

  template< class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9 >
  struct Size< const Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  : public Size< Tuple< T1, T2, T3, T4, T5, T6, T7, T8, T9 > >
  {};

  template< typename T1, typename T2 >
  struct Size< Pair< T1, T2 > >
  {
    static const int value = 1 + Size< T2 >::value;
  };


  template< typename T1 >
  struct Size< Pair< T1, Nil > >
  {
    static const int value = 1;
  };

  template<>
  struct Size< Pair< Nil, Nil > >
  {
    static const int value = 0;
  };


  /**
   * @brief Equality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1, typename T2, typename U1, typename U2>
  inline bool operator==(const Pair<T1,T2>& Tuple1, const Pair<U1,U2>& Tuple2)
  {
    return (Tuple1.first()==Tuple2.first() && Tuple1.second()==Tuple2.second());
  }

  /**
   * @brief Inequality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1, typename T2, typename U1, typename U2>
  inline bool operator!=(const Pair<T1,T2>& Tuple1, const Pair<U1,U2>& Tuple2)
  {
    return (Tuple1.first()!=Tuple2.first() || Tuple1.second()!=Tuple2.second());
  }

  /**
   * @brief Less operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1, typename T2, typename U1, typename U2>
  inline bool operator<(const Pair<T1,T2>& Tuple1, const Pair<U1,U2>& Tuple2)
  {
    return Tuple1.first() < Tuple2.first()
      || (Tuple1.first() == Tuple2.first() && Tuple1.second() < Tuple2.second());
  }

  /**
   * @brief Equality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1,typename U1>
  inline bool operator==(const Pair<T1,Nil>& Tuple1, const Pair<U1,Nil>& Tuple2)
  {
    return (Tuple1.first()==Tuple2.first());
  }

  /**
   * @brief Inequality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1, typename U1>
  inline bool operator!=(const Pair<T1,Nil>& Tuple1, const Pair<U1,Nil>& Tuple2)
  {
    dune_static_assert( (IsInteroperable< T1, U1 > :: value), "error" );
    return (Tuple1.first()!=Tuple2.first());
  }

  /**
   * @brief Less operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple,
   */
  template<typename T1, typename U1>
  inline bool operator<(const Pair<T1,Nil>& Tuple1, const Pair<U1,Nil>& Tuple2)
  {
    return (Tuple1.first()<Tuple2.first());
  }

  /**
   * @brief Equality comparison operator for Tuples.
   *
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple.
   * @return False as the type of the compared objects are different.
   */
  template<typename T1,typename U1, typename U2>
  inline bool operator==(const Pair<T1,Nil>& Tuple1, const Pair<U1,U2>& Tuple2)
  {
    return false;
  }

  /**
   * @brief Inequality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple.
   * @return True as the type of the compared objects are different.
   */
  template<typename T1, typename U1, typename U2>
  inline bool operator!=(const Pair<T1,Nil>& Tuple1, const Pair<U1,U2>& Tuple2)
  {
    return true;
  }


  /**
   * @brief Equality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple.
   * @return False as the type of the compared objects are different.
   */
  template<typename T1, typename T2, typename U1>
  inline bool operator==(const Pair<T1,T2>& Tuple1, const Pair<U1,Nil>& Tuple2)
  {
    return false;
  }

  /**
   * @brief Inequality comparison operator for Tuples.
   * @param Tuple1 The first Tuple.
   * @param Tuple2 The second Tuple.
   * @return True as the type of the compared objects are different.
   */
  template<typename T1, typename T2, typename U1>
  inline bool operator!=(const Pair<T1,T2>& Tuple1, const Pair<U1,Nil>& Tuple2)
  {
    return true;
  }

  /**
   * @brief Create a Tuple and initialize it.
   * @param first The value of the first field.
   * @param second The value of the second field.
   */
  template<typename T1, typename T2>
  inline Pair<T1,T2> makePair(const T1& first, const T2& second)
  {
    return Pair<T1,T2>(first, second);
  }

  /**
   * @brief Print aa pair or  Tuple.
   */
  template<typename T1, typename T2>
  inline std::ostream& operator<<(std::ostream& os, const Pair<T1,T2>& pair)
  {
    os<<pair.first()<<" "<<pair.second();
    return os;
  }

  template<typename T1>
  inline std::ostream& operator<<(std::ostream& os, const Pair<T1,Nil>& pair)
  {
    os<<pair.first();
    return os;
  }

  template<typename T1, typename TT>
  template<typename T2, typename T3, typename T4, typename T5,
     typename T6, typename T7, typename T8, typename T9>
  inline Pair<T1,TT>::Pair(typename TupleAccessTraits<T1>::ParameterType first,
         T2& t2, T3& t3, T4& t4, T5& t5,
         T6& t6, T7& t7, T8& t8, T9& t9)
    : first_(first), second_(t2,t3,t4,t5,t6,t7,t8,t9, nullType())
  {}

  template <typename T1, typename TT>
  inline Pair<T1, TT>::Pair(typename TupleAccessTraits<T1>::ParameterType first, TT& second)
    : first_(first), second_(second)
  {}

  template<typename T1, typename T2>
  inline Pair<T1,T2>::Pair()
    : first_(), second_()
  {}

  template<typename T1, typename T2>
  template<typename U1, typename U2>
  inline Pair<T1,T2>::Pair(const Pair<U1,U2>& other)
    : first_(other.first_), second_(other.second_)
  {}

  template<typename T1, typename T2>
  template<typename U1, typename U2>
  inline Pair<T1,T2>& Pair<T1,T2>::operator=(const Pair<U1,U2>& other)
  {
    first_=other.first_;
    second_=other.second_;
    return *this;
  }

  template<typename T1, typename T2>
  inline Pair<T1,T2>& Pair<T1,T2>::operator=(const Pair& other)
  {
    first_=other.first_;
    second_=other.second_;
    return *this;
  }

  template<typename T1, typename T2>
  inline typename TupleAccessTraits<T1>::NonConstType
  Pair<T1,T2>::first()
  {
    return first_;
  }

  template<typename T1, typename T2>
  inline typename TupleAccessTraits<T1>::ConstType
  Pair<T1,T2>::first() const
  {
    return first_;
  }

  template<typename T1, typename T2>
  inline typename TupleAccessTraits<T2>::NonConstType
  Pair<T1,T2>::second()
  {
    return second_;
  }

  template<typename T1, typename T2>
  inline typename TupleAccessTraits<T2>::ConstType
  Pair<T1,T2>::second() const
  {
    return second_;
  }

  template<typename T1>
  inline Pair<T1,Nil>::Pair(typename TupleAccessTraits<T1>::ParameterType first,
          const Nil&, const Nil&, const Nil&, const Nil&,
          const Nil&, const Nil&, const Nil&, const Nil&)
    : first_(first)
  {}

  template <typename T1>
  inline Pair<T1, Nil>::Pair(typename TupleAccessTraits<T1>::ParameterType first,
           const Nil&)
    : first_(first)
  {}

  template<typename T1>
  inline Pair<T1,Nil>::Pair()
    : first_()
  {}

  template<typename T1>
  template<typename T2>
  inline Pair<T1,Nil>::Pair(const Pair<T2,Nil>& other)
    : first_(other.first_)
  {}

  template<typename T1>
  template<typename T2>
  inline Pair<T1,Nil>& Pair<T1,Nil>::operator=(const Pair<T2,Nil>& other)
  {
    first_ = other.first_;
    return *this;
  }


  template<typename T1>
  inline Pair<T1,Nil>& Pair<T1,Nil>::operator=(const Pair& other)
  {
    first_ = other.first_;
    return *this;
  }

  template<typename T1>
  inline typename TupleAccessTraits<T1>::NonConstType
  Pair<T1,Nil>::first()
  {
    return first_;
  }

  template<typename T1>
  inline typename TupleAccessTraits<T1>::ConstType
  Pair<T1,Nil>::first() const
  {
    return first_;
  }

  /**
   * @brief Read a pair or  Tuple.
   */
  template<typename T1, typename T2>
  inline std::istream& operator>>(std::istream& os, Pair<T1,T2>& pair)
  {
    os>>pair.first()>>pair.second();
    return os;
  }

  template<typename T1>
  inline std::istream& operator>>(std::istream& os, Pair<T1,Nil>& pair)
  {
    os>>pair.first();
    return os;
  }

  ///////////////////////////////////////
  //
  //  TupleLength
  //
  ///////////////////////////////////////

  //! length of a tuple
  template<class T>
  struct TupleLength  {
    enum { value = 1 + TupleLength<typename T::Type2>::value };
  };

  //! length of an empty tuple is zero
  template<>
  struct TupleLength<Tuple<Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil> > {
    enum { value = 0 };
  };

  //! length of an empty tuple is zero
  template<>
  struct TupleLength<Nil> {
    enum { value = 0 };
  };

  template<class T1, class T2>
  struct Concat
  {
    typedef Pair< T1, T2 >                                         Type;

    static Type concat(T1 t1, T2 t2)
    {
      return Type(t1, t2);
    }
  };

  /**
   * @brief Helper template to clone the type definition of a tuple with the
   * storage types replaced by a user-defined rule.
   *
   * Suppose all storage types A_i in a tuple define a type A_i::B. You can
   * build up a pair consisting of the types defined by A_i::B in the following
   * way:
   \code
   template <class A>
   struct MyEvaluator {
     typedef typename A::B Type;
   };

   typedef ForEachT<MyEvaluator, ATuple>::Type BTuple;
   \endcode
   * Here, MyEvaluator is a helper struct that extracts the correct type from
   * the storage types of the tuple defined by the tuple ATuple.
   */
  template< template< class > class TypeEvaluator, class TupleType >
  struct ForEachT
  /** \cond */
  : ForEachT< TypeEvaluator, typename TupleType :: FirstPair >
  /** \endcond */
  {};

  // Specialisation for standard tuple element
  template <template <class> class TypeEvaluator, class _Head, class _Tail>
  struct ForEachT<TypeEvaluator, Pair<_Head, _Tail> > {
    //! Defines type corresponding to the subtuple defined by Pair<Head, Tail>
    typedef Pair<typename TypeEvaluator<_Head>::Type,
                 typename ForEachT<TypeEvaluator, _Tail>::Type> Type;
  };
  // Specialisation for last element
  template <template <class> class TypeEvaluator>
  struct ForEachT<TypeEvaluator, Nil> {
    typedef Nil Type;
  };

//   template <template <class> class TypeEvaluator, class Head>
//   struct ForEachT<TypeEvaluator, Pair<Head, Nil> > {
//     //! For the storage element, Head is replaced by the expression provided
//     //! by the TypeEvaluator helper template.
//     typedef Pair<typename TypeEvaluator<Head>::Type, Nil> Type;
//   };

  /**
   * @brief Helper template which implements iteration over all storage
   * elements in a tuple.
   *
   * Compile-time constructs that allows to process all elements in a tuple.
   * The exact operation performed on an element is defined by a function
   * object, which needs to implement a visit method which is applicable to
   * all storage elements of a tuple.
   *
   * The following example implements a function object which counts the
   * elements in a tuple
   \code
   template <class T>
   struct Counter {
   Counter() : result_(0) {}

   template <class T>
   void visit(T& elem) { ++result_; }

   int result_;
   };
   \endcode
   * The number of elements in the tuple are stored in the member variable
   * result_. The Counter can be used as follows, assuming a tuple t of type
   * MyTuple is given:
   \code
   Counter c;
   ForEachVal<MyTuple> forEach(t);

   forEach.apply(c);
   std::cout << "Number of elements is: " << c.result_ << std::endl;
   \endcode
   */
  template <class TupleType>
  class ForEachVal {
  public:
    //! \brief Constructor
    //! \param tuple The tuple which we want to process.
    ForEachVal(TupleType& tuple) : tuple_(tuple) {}

    //! \brief Applies a function object to each storage element of the tuple.
    //! \param f Function object.
    template <class Functor>
    void apply(Functor& f) {
      apply(f, tuple_);
    }

  private:
    //! Specialisation for the last element
    template <class Functor, class Head>
    void apply(Functor& f, Pair<Head, Nil>& last) {
      f.visit(last.first());
    }

    //! Specialisation for a standard tuple element
    template <class Functor, class Head, class Tail>
    void apply(Functor& f, Pair<Head, Tail>& pair) {
      f.visit(pair.first());
      apply(f, pair.second());
    }
  private:
    TupleType& tuple_;
  };

  //- Definition ForEachValPair class
  // Assertion: both tuples have the same length and the contained types are
  // compatible in the sense of the applied function object
  /**
   * @brief Extension of ForEachVal to two tuples...
   *
   * This class provides the framework to process two tuples at once. It works
   * the same as ForEachVal, just that the corresponding function object
   * takes one argument from the first tuple and one argument from the second.
   *
   * \note You have to ensure that the two tuples you provide are compatible
   * in the sense that they have the same length and that the objects passed
   * to the function objects are related in meaningful way. The best way to
   * enforce it is to build the second tuple from the existing first tuple
   * using ForEachT.
   */
  template <class TupleType1, class TupleType2>
  class ForEachValPair {
  public:
    //! Constructor
    //! \param t1 First tuple.
    //! \param t2 Second tuple.
    ForEachValPair(TupleType1& t1, TupleType2& t2) :
      tuple1_(t1),
      tuple2_(t2)
    {}

    //! Applies the function object f to the pair of tuples.
    //! \param f The function object to apply on the pair of tuples.
    template <class Functor>
    void apply(Functor& f) {
      apply(f, tuple1_, tuple2_);
    }

  private:
    //! Specialisation for the last element.
    template <class Functor, class Head1, class Head2>
    void apply(Functor& f, Pair<Head1, Nil>& last1, Pair<Head2, Nil>& last2) {
      f.visit(last1.first(), last2.first());
    }

    //! Specialisation for a standard element.
    template <class Functor, class Head1, class Tail1, class Head2,class Tail2>
    void apply(Functor& f, Pair<Head1, Tail1>& p1, Pair<Head2, Tail2>& p2) {
      f.visit(p1.first(), p2.first());
      apply(f, p1.second(), p2.second());
    }

  private:
    TupleType1& tuple1_;
    TupleType2& tuple2_;
  };



} // end of namespace Dune::RB::Tuples

} //end of namespace Dune::RB

} //end of namespace Dune

#endif /* end of include guard: TUPLES_ZZLVAXVJ */
