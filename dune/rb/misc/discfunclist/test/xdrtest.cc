#include <config.h>

// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// discrete function list
#include "../discfunclist_xdr.hh"


using namespace Dune;
using namespace RB;

int main(int argc, char *argv[])
{
  try {
    typedef LeafGridPart<GridSelector::GridType>                      GridPart;
    typedef FunctionSpace<double, double, 2, 1>                       FunctionSpace;
    typedef LagrangeDiscreteFunctionSpace<FunctionSpace, GridPart, 1> DiscFuncSpace;
    typedef AdaptiveDiscreteFunction<DiscFuncSpace>                   DiscreteFunction;
    typedef DiscreteFunctionList_xdr<DiscreteFunction>                DiscFuncList;

    MPIManager::initialize(argc, argv);
    Parameter::append("xdrtest.params");

    GridPtr<GridSelector::GridType> gridPtr("unitcube2.dgf");
    GridSelector::GridType &grid = *gridPtr;

    GridPart gridPart(grid);
    DiscFuncSpace discFuncSpace(gridPart);
    DiscreteFunction func1("func1", discFuncSpace);
    DiscreteFunction func2("func2", discFuncSpace);
    DiscreteFunction func3("func3", discFuncSpace);
    {
    DiscFuncList list1(discFuncSpace, "list1");
    list1.push_back(func1);
    list1.push_back(func2);

    DiscFuncList list2(list1);
    }
    DiscFuncList list3(discFuncSpace, "list1", "list1");
    list3.push_back(func3);

    std::vector<DiscFuncList> vec;
    //    vec.push_back(list1);
    //    vec.push_back(list2);
    vec.push_back(list3);

    for (int i = 0; i<vec.size(); ++i) {
      std::cout << "List " << i << " size: "
                << vec[i].size() << std::endl;
    }

  //   std::cout << "List 1 size: " << list1.size() 
  //             << "\nList 2 (copy of 1) size: " 
  //             << list2.size() 
  //             << "\nList 3 (made from header file): "
  //             << list3.size() << std::endl;
  // list1.clear();
  //  list2.clear();
  list3.clear();

  } catch (Dune::Exception e) {
    std::cerr << e.what();
  }
    return 0;
}
