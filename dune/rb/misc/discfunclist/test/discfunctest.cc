
#include <iostream>
#include <config.h>
#include <dune/common/stdstreams.cc>
#include <../discfunclist2.hh>

#include <dune/common/exceptions.hh>
#include <fstream>

#include <dune/common/stack.hh>
#include <dune/common/misc.hh>
#include <dune/fem/discretefunction/dfadapt.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/function/common/function.hh>  
#include <dune/fem/discretefunction/adaptivefunction.hh>

#include <dune/grid/common/gridpart.hh>
#include <dune/grid/common/referenceelements.hh>

#include <dune/fem/quadrature/quadrature.hh>
#include <dune/fem/io/file/grapedataio.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>
#include <dune/grid/albertagrid/agrid.hh>

//#include <dune/grid/io/visual/combinedgrapedisplay.hh>
 
using namespace std;
using namespace Dune;

int main( int argc, char ** argv){ 

  typedef LeafGridPart<GridType> GridPartType;
  typedef FunctionSpace<double,double,dimworld,1> FuncSpace;
  typedef LagrangeDiscreteFunctionSpace<FuncSpace,GridPartType,1> DiscreteFunctionSpaceType;
  typedef AdaptiveDiscreteFunction<DiscreteFunctionSpaceType> DiscreteFunctionType;

  // moegliche Tests:

    (DiscreteFunctionType ist definiert)

    ....

    // initialization by normal constructor
    DiscFuncList<DiscreteFunctionType> funclist;

    // generate some discretefunctions with reasonable name and 
    // different values    
    DiscreteFunctionType func = ...; // nullfunktion
    DiscreteFunctionType func2 = ...; // einsfunktion
    DiscreteFunctionType rhs = ...; // ...

    // hinzufuegen einer discretefunction
    funclist.push_back(func);
    // und nochmal 
    funclist.push_back(func2);
    // und nochmal die erste
    funclist.push_back(func);

    // kopieren einer gesamten Liste
    DiscFuncList<DiscreteFunctionType> funclist2(funclist);

    // hintenanhaengen einer gesamten Liste
    funclist2.push_back(funclist2);

    // leeren einer Liste, d.h. size soll nachher 0 sein
    funclist.clear();

    // bestimmen der Laenge:
    std::cout << funclist.size() << "\n"; // sollte 0 sein
    // bestimmen der Laenge:
    std::cout << funclist2.size() << "\n"; // sollte 6 sein

    // auswahl aus einer Liste: nur funktion 2 und 3 auswaehlen
    bool* mask = new bool[funclist2.size()];  
    for (int i = 0; i< funclist2.size();i++)
       mask[i] = false;    
    mask[1] = true; // ist funktion 2!
    mask[2] = true; // ist funktion 2!           
    DiscFuncList<DiscreteFunctionType> funclist3(funclist2,mask);
    // gebe die Namen der Funktionen aus
    funclist3.printNames();
     
    ... // ueberpreufen, dass tatsaechlich korrekter Teil kopiert wurde

    // auswahl einer Funktion
    DiscreteFunctionType& func2 = funclist2[3];
      
    .... // irgendwie ueberpruefen, dass richtige Funktion extrahiert wurde
    

    // constructor with 5 zero functions
    DiscreteFunctionSpaceType fspace ...
    DiscFuncList<DiscreteFunctionType> funclist4(fspace, 5);

    ... // irgendwie testen

    // 
    funclist4.visualize_as_list();
    
    // assignment operator
    funclist3 = funclist4;

    ... irgendwie ueberpruefen, dass das richtige passiert ist

   
    ... Groesse ausgeben lassen, visualisieren
    

    // austauschen von Funktionen

    funclist.set_function(4,rhs); // loeschen des momentanen nummer
                                 // 4, speichern von rhs



    // Konkrete Implementation Nadine:

  const char* filename = argv[1];
  int maxlevel = 5;

  GridPtr<GridType> gridPtr_;
  GridPartType *gridPart_;
  gridPart_ = NULL;
  gridPtr_ = NULL;
  DiscreteFunctionSpaceType *discFuncSpace ;
  GridPtr<GridType>  gridptr(filename);
  gridptr->globalRefine(maxlevel);
  gridPart_ = new GridPartType (*gridptr);
  discFuncSpace = new DiscreteFunctionSpaceType(*gridPart_);
  GridPartType part(*gridptr);

  DiscreteFunctionSpaceType linFuncSpace(part);

  // foldende def der discfunc bricht das programm ab!! warum ??
  // DiscreteFunctionType det("dett", linFuncSpace);

  DiscreteFunctionType sol2("solution", linFuncSpace);
  DiscreteFunctionType rhs2("RHS", linFuncSpace);

  //sol2.print(std::cout);
  //rhs2.print(std::cout);

  sol2.clear();
  //sol2.add(7);
  //sol2.print(std::cout);
  //rhs.print(std::cout);
  //sol.print(std::cout);

  std::cout << "test1 : " << sol2.name() << " " << rhs2.size() << std::endl;

  DiscFuncList<DiscreteFunctionType> funclist; //(linFuncSpace);
  std::cout << " test 2  " << std::endl;
  std::cout << " test 2b " << std::endl;

  DiscFuncList<DiscreteFunctionType> funclist2; //(linFuncSpace);
  DiscFuncList<DiscreteFunctionType> func2;
  //funclist2(&funclist);

  std::cout << " test 3 " << std::endl;
  funclist.push_back(sol2,0);
  funclist2.push_back(sol2,0);
  funclist.push_back(rhs2,0);
  funclist2 = funclist;
  funclist.printNames();
  funclist.push_back(func2);
  funclist.clear();
  std::cout << " SIZE : " << funclist.size() << std::endl;


  // std::cout << DiscreteFunctionType.name();  
  // funclist.printNames();  
  // funclist.clear();

  // funclist.print();

  funclist.clear();    
  std::cout << " test 3 " << std::endl;


  delete discFuncSpace;
  delete gridPart_;
  // delete gridptr;

  return 0;
}

