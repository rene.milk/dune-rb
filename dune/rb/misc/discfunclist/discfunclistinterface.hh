#ifndef DISCFUNCLIST_INTERFACE
#define DISCFUNCLIST_INTERFACE

#include <dune/common/bartonnackmanifcheck.hh>
#include "../utility.hh"
#include <iostream>
#include <fstream>
#include <vector>

namespace Dune
{
namespace RBFem
{
namespace FunctionList
{

// the following macro adds structures
// HasTypeAttributeType and SelectTypeAttributeTypeIfEnabled
CHECK_FOR_ATTRIBUTE( AttributeType )

/** \class   AttributeActions
 *  \ingroup DiscFuncList
 *  \brief   Write attribute to disk and read it from disk
 *
 *  The AttributeActions template struct provides functionality to write an attribute to disk
 *  and read it from disk. This is the standart implementation for all AttributeTypes that
 *  can be written to disk using std::ofstream and the write-memberfunction of std::ofstream. 
 *  If you like to use another type you have to specialize this class for your attribute type
 *
 *  \param AttributeType the type of the attribute to be handled by this 
 *                       class
 */
template< typename AttributeType >
struct AttributeActions
{
  /**
   *  \brief write attribute to disk
   *
   *  \param[in] arg   attribute to be written to disk
   *  \param[in] file  filename where attribute should be written
   *
   *  \return returns true if attribute was read without errors
   */
  bool write (AttributeType &arg, const std::string &file)
  {
    std::ofstream foo( file.c_str() );
    foo.precision( 10 );
    //convert AttributeType to c string
    //  std::ostringstream bar;
    //  bar << arg;
    //  const char * bar_c_str = bar.str().c_str();
    //  //write arg to disk
    //  foo.write(bar_c_str, strlen(bar_c_str));
    foo << arg;
    return true;
  }

  /**
   *  \brief read attribute from disk
   *
   *  \param[out] dest destination where attribute should be 
   *                   written after beeing read from disk
   *  \param[in]  file filename to be used for reading from disk
   *  
   *  \return          returns true if attribute was read without 
   *                   errors
   *
   */
  bool read (AttributeType &dest, const std::string &file)
  {
    std::ifstream foo( file.c_str() );
    if (!foo)
    {
      DUNE_THROW( IOError, "Could not open " << file << " for reading!" );
    }
    foo.precision( 10 );
    //  char * bar;
    //  //TODO: use istringstream
    //         foo.read(bar, sizeof (T));
    foo >> dest;
    return true;
  }
};

/** \class   DiscreteFunctionListInterface
 *  \ingroup DiscFuncList
 *  \brief   Stores functions together with attributes in a list
 *
 *  You have to provide a class DiscreteFunctionListTraits that provides
 *  a type definition
 *  DiscreteFunctionType
 *  for the type of the discrete functions to be stored by this list.
 *  You can also store an attribute together with the function. For this you have to
 *  define AttributeType in DiscreteFunctionListTraits, e.g.
 *    typedef double AttributeType;
 *  if you want to store time information together with the function.
 *
 *  \param DiscreteFunctionListTraits a traits class that defines the
 *                                    discrete function type
 *  \param DiscreteFunctionListImp    derived class (crtp)
 *
 */
template< class DiscreteFunctionListTraits >
class Interface
{

public:
  //typedefs
  typedef DiscreteFunctionListTraits Traits;
  typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef Interface< DiscreteFunctionListTraits > ThisType;
  /*the next line determines whether AttributeType is defined in Traits class, if it is not defined,
   int is used as default*/
  typedef typename SelectTypeAttributeTypeIfEnabled< DiscreteFunctionListTraits,
      int >::Type AttributeType;

public:

  virtual ~Interface ()
  {
  }
  ;

  /**
   * \brief  store discrete function in list
   *
   *  writes given discrete function to disc and stores given attribute
   *  to the given discrete function.
   *  \param[in]  discFunc    The discrete function to be stored
   *  \param[in]  attr        The parameter to be stored with discrete function,
   *                          name for instance
   *  \return returns unique id connected to stored discrete function
   */
  virtual int push_back (const DiscreteFunctionType& discFunc,
                         AttributeType attr = AttributeType()) = 0;

  /**
   * \brief append a whole DiscreteFunctionList
   *
   * inserts the given DiscreteFunctionList at the end
   * \param[in] other the other DiscreteFunctionList
   */
  void push_back_list (const ThisType &other)
  {
    const int otherSize = other.size();
    DiscreteFunctionType temp( "temp", this->space() );
    for (int i = 0; i != otherSize; ++i)
    {
      temp.clear();
      other.getFunc( i, temp );
      this->push_back( temp );
    }
  }

  /**
   *  \brief returns the number of stored discrete functions
   *
   *  \return returns the number of stored discrete functions
   */
  virtual unsigned int size () const = 0;

  virtual int clear ()
  {
    return 0;
  }

private:
  /**
   * \brief  returns discrete function with identifier id.
   *
   * Assumes, that grid didn't change
   * since discFunc was written to disk
   * \param[in]   index  index of the desired discrete function
   * \param[out]  dest   result is stored to dest
   * \return returns true if discFunc was read without errors
   */
  virtual bool getFuncByIndex (const unsigned int index,
                               DiscreteFunctionType& dest) const = 0;

public:
  /**
   * \brief  get discrete function by attribute
   *
   * returns the first discrete function in functions_ with matching parameter, i.e. if attribute is not
   * unique, you may not get the expected discfunc back! Assumes, that grid didn't change since discFunc
   * was written.
   * \param[in]  attr the attribute of the desired discrete function
   * \param[out]  dest result is stored to dest
   * \return returns true if discFunc was read without errors
   */
  virtual bool getFuncByAttribute (const AttributeType & attr,
                                   DiscreteFunctionType& dest) const = 0;

private:
  virtual bool getFunc (const unsigned int i,
                        DiscreteFunctionType& dest) const = 0;

public:
  //!return underlying space
  virtual const DiscreteFunctionSpaceType& space () const = 0;

};

/** @brief This is just a wrapper for all mu values so tuples of mu
 *         values can be compared. (Can be used as attribute)
 *
 */
class MuTuple
{

  typedef MuTuple ThisType;

  typedef std::vector< std::string > NamesType;
  typedef std::vector< double > ValuesType;
  typedef NamesType::const_iterator ConstNameIterator;
  typedef ValuesType::const_iterator ConstValuesIterator;

public:

  typedef std::map< std::string, double >::const_iterator const_iterator;
  typedef std::map< std::string, double >::iterator iterator;

  MuTuple (NamesType &mu_names, ValuesType &mu_values)
  {
    assert( mu_names.size()==mu_values.size() );
    ConstNameIterator nameEnd = mu_names.end();
    ConstValuesIterator valIt = mu_values.begin();
    for (ConstNameIterator nameIt = mu_names.begin(); nameIt != nameEnd;
        ++nameIt, ++valIt)
      pairs_[*nameIt] = *valIt;
  }

  MuTuple (const ThisType &other)
  {
    this->pairs_ = other.pairs_;
  }

  MuTuple ()
  {
  }

  template< class NameIteratorType, class ValueIteratorType >
  void set (NameIteratorType &nameBegin, NameIteratorType &nameEnd,
            ValueIteratorType &valEnd)
  {
    false;
  }

  const_iterator begin () const
  {
    return pairs_.begin();
  }
  const_iterator end () const
  {
    return pairs_.end();
  }
  iterator begin ()
  {
    return pairs_.begin();
  }
  iterator end ()
  {
    return pairs_.end();
  }

  /** @brief Get value of parameter named name.
   *
   * @param[in]  name  name of the desired paramter
   * @param[out] value value of the paramter
   *
   * @return Returns true if paramter was in the list, false otherwise.
   */
  bool get (const std::string &name, double &value)
  {
    if (pairs_.find( name ) == pairs_.end())
      return false;
    else
    {
      value = pairs_[name];
      return true;
    }
  }
  ;

  void add (const std::string &name, const double &value)
  {
    pairs_[name] = value;
  }

  unsigned int size () const
  {
    return pairs_.size();
  }

  void clear ()
  {
    pairs_.clear();
  }
  ;

  friend std::ostream& operator<< (std::ostream& out, ThisType &muTuple)
  {
    out.precision( 10 );
    const_iterator end = muTuple.end();
    for (const_iterator it = muTuple.begin(); it != end; ++it)
    {
      out << it->first << ";" << it->second << ";";
    }
    return out;
  }

  friend std::istream& operator>> (std::istream &in, ThisType &muTuple)
  {
    muTuple.clear();
    in.precision( 10 );
    std::string row;
    //read line from given istream
    while (getline( in, row ))
    {
      // ignore blank lines
      if (strlen( row.c_str() ) == 0) continue;

      // ignore comments
      if (row[0] == '#') continue;

      std::string::size_type position = row.find( ';' );
      if (position != std::string::npos)
      {
        while (position != std::string::npos)
        {
          row.erase( 0, row.find_first_not_of( " " ) );
          position = row.find( ';' );
          std::string name = row.substr( 0, position );
          row.erase( 0, position + 1 );
          // now the character beginning at position 0 is a double
          position = row.find( ';' );
          double val = atof( row.substr( 0, position ).c_str() );
          row.erase( 0, position + 1 );
          muTuple.add( name, val );
        }
        // if we found a line containing one m tuple, this file is done
        break;
      }
    }
    return in;
  } // operator>>

  bool operator== (const ThisType &other) const
  {
    // first check if both Tuples have the same length
    if (this->size() != other.size()) return false;
    // now, compare the two tuples
    return std::equal( this->pairs_.begin(), this->pairs_.end(),
                       other.pairs_.begin() );
  }

private:
  std::map< std::string, double > pairs_;

};

template< class Stepper >
class Traits
{
public:
  // The discrete function for the unknown solution
  typedef typename Stepper::DiscreteFunctionType DiscreteFunctionType;
  // ... as well as the Space type
  typedef typename Stepper::DiscreteSpaceType DiscreteFunctionSpaceType;
  typedef MuTuple AttributeType;
};

} //end of namespace FunctionList
} //end of namespace Dune::RBFem
} //end of namespace Dune

#endif
/*vim: set et sw=2:*/
