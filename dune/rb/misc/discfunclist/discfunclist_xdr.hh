#ifndef DISCFUNCLIST_XDR_HH
#define DISCFUNCLIST_XDR_HH

#include <config.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>

#include "discfunclistinterface.hh"
#include <dune/fem/io/file/iointerface.hh>

#define BOOST_FILESYSTEM_VERSION 2

// include boost
#include <boost/filesystem/operations.hpp>

namespace Dune {
namespace RBFem {
namespace FunctionList {

    using std::vector;
    using std::pair;
    using std::make_pair;


    /** \class   DiscreteFunctionList_xdr
     *  \ingroup DiscFuncList
     *  \brief   Stores functions together with attributes in a list
     *
     *  The function is stored to disc using the functions memberfunction "write_xdr", that is:
     *  it is written with xdr encoding. You can also store an attribute together with each function
     *  (see interface documentation for details). This attribute is stored on disc aswell using the AttributeActions
     *  class. If you would like to use an AttributeType other than int, double, float, string or
     *  char* you will have to implement the AttributeActions class for that AttributeType.
     *  For convinience a header file is written which stores the following information about all
     *  functions handled by the current instance of this class:
     *
     *       index;Attributefilename;Datafilename
     *
     *  So one might change this file to change the list (remove, add, resort functions etc.)
     *
     *  \param DiscreteFunctionListTraits a traits class that defines the discrete function
     *                                    type
     */
    template<class DiscreteFunctionListTraits>
    class Xdr : public
    RBFem::FunctionList::Interface< DiscreteFunctionListTraits >
    {
      //typedefs
      typedef DiscreteFunctionListTraits                               Traits;
      typedef RBFem::FunctionList::Xdr< DiscreteFunctionListTraits >   ThisType;
    public:
      typedef typename Traits :: DiscreteFunctionType                  DiscreteFunctionType;
      typedef typename DiscreteFunctionType
      :: DiscreteFunctionSpaceType                           DiscreteFunctionSpaceType;
      /*the next line determines whether AttributeType is defined in Traits class, if it is not defined,
        int is used as default.*/
      typedef typename SelectTypeAttributeTypeIfEnabled
      < DiscreteFunctionListTraits, int > :: Type              AttributeType;


    private:
      /** Default constructor
       *
       *  \todo Improve this
       */
      Xdr()
        :headerName_("discFuncs"), 
         dataPath_("./" + headerName_ + "/" ),
         cleared_(false),
         verbose_(Parameter::getValue<bool>("discfunclist.verbose", false))
      {
        boost::filesystem::create_directory(dataPath_);
        headerFile_+=dataPath_;
        headerFile_+=headerName_;
        headerFile_+=".lst";
        if (headerOpened(headerFile_))
          DUNE_THROW(Dune::IOError, "The given header is already opened by another list! (default constructor)");
        header_.open(headerFile_.c_str());
        header_ << "# This is the header file for the discrete function list \n"
                << "# You may change the list by changing this file. Directory must contain\n"
                << "# a '.dat' and a '.att' file named as the entry in this list. You may also\n"
                << "# use the '[a:b:c]' syntax, meaning the list will contain each function from a.dat\n"
                << "# to c.dat, counting in b stepsize\n";
      };

    public:

      /** \brief constructor 
       *
       *  checks for existence of data path and tries to create it if it 
       *  doesn't exist.
       *
       * @remark A default name for the header file will be used. If it should 
       *         exists already, another one will be choosen.
       *
       *  \param[in] discFuncSpace discrete function space where the functions are defined
       *  \param[in] dataPath path were data will be stored
       */
      Xdr(const DiscreteFunctionSpaceType &discFuncSpace, const std::string& dataPath)
        :headerName_("discFuncs"), dataPath_(dataPath + "/"), discFuncSpace_(&discFuncSpace), cleared_(false),
         verbose_(Parameter::getValue<bool>("discfunclist.verbose", false))
      {
        boost::filesystem::create_directory(dataPath_);
        setHeaderFileName(dataPath_, headerName_);
        if (headerOpened(headerFile_))
          DUNE_THROW(Dune::IOError, "The given header is already opened by another list! (constructor only using path)");
        header_.open(headerFile_.c_str());
        header_ << "# This is the header file for the discrete function list \n"
                << "# You may change the list by changing this file. Directory must contain\n"
                << "# a '.dat' and a '.att' file named as the entry in this list. You may also\n"
                << "# use the '[a:b:c]' syntax, meaning the list will contain each function from a.dat\n"
                << "# to c.dat, counting in b stepsize\n";
      };


    public:
      /** \brief constructor 
       *
       * checks for existence of data path and tries to create it if 
       * it doesn't exist. 
       *
       *  @remark If the header exists, it is reused!!!
       *
       *  \param[in] discFuncSpace discrete function space where the functions are defined 
       *  \param[in] path          the (absolute or relative) path where this list should be stored
       *  \param[in] name          the name for the header file
       */
      Xdr(const DiscreteFunctionSpaceType &discFuncSpace, 
                               const std::string path,
                               const std::string name)
        :headerName_(name), dataPath_(path + "/" ), discFuncSpace_(&discFuncSpace), cleared_(false),
         verbose_(Parameter::getValue<bool>("discfunclist.verbose", false))
      {
        headerFile_+=dataPath_;
        headerFile_+=headerName_;
        headerFile_+=".lst";
        if (headerOpened(headerFile_))
          DUNE_THROW(Dune::IOError, "The given header is already opened by another list! (constructor using path and name)");
        
        // if header exists already, read it
        if (boost::filesystem::is_regular_file(headerFile_)) 
          init();
        // else open new one
        else {
          boost::filesystem::create_directory(dataPath_);
          header_.open(headerFile_.c_str());
          header_ << "# This is the header file for the discrete function list \n"
                  << "# You may change the list by changing this file. Directory must contain\n"
                  << "# a '.dat' and a '.att' file named as the entry in this list. You may also\n"
                  << "# use the '[a:b:c]' syntax, meaning the list will contain each function from a.dat\n"
                  << "# to c.dat, counting in b stepsize\n";
        }
      }

       /** @brief Copy Constructor
       *
       *  Constructs a discrete function list from another one. Uses 
       *  the same files on the hard disk.
       *
       * @param[in] other The other discrete function list
       */
      Xdr(const ThisType& other) 
        : headerName_(other.headerName_),
          dataPath_(other.dataPath_), 
          discFuncSpace_(other.discFuncSpace_),
          cleared_(other.cleared_),
          verbose_(Parameter::getValue<bool>("discfunclist.verbose", false))
      {
        // first copy the header file
        // make shure we don't overwrite an existing header
        setHeaderFileName(dataPath_, headerName_);
        // Throw an exception if the given header is already opened at this moment
        if (headerOpened(headerFile_))
          DUNE_THROW(Dune::IOError, "The given header is already opened by another list! (Copy Constructor)");
        boost::filesystem::copy_file((const boost::filesystem::path&)other.headerFile_,(const boost::filesystem::path&)this->headerFile_);
        init();
      }


      /** @brief Sets the variable headerFile_ to a name that is 
       *         not taken by any other list.
       */
      void setHeaderFileName(const std::string& dataPath_, const std::string headerName_) {
        std::string tmpName;
        tmpName = dataPath_+headerName_+".lst";
        int i=0;
        while (boost::filesystem::is_regular_file(tmpName)) {
          ++i;
          std::stringstream foo;
          foo << i;
          tmpName = dataPath_+headerName_+"_copy"+foo.str()+".lst";
        }
        headerFile_ = tmpName;
        return;
      }


      /** Assignment operator
       *
       * @param[in] other The other discrete function list
       */
      ThisType& operator=(const ThisType &other)  {
        cleared_=false;
        // remove all functions from this list but not from the disk
        attributes_.clear();
        filenames_.clear();
        // remove the header file
        boost::filesystem::remove(headerFile_);
        // the header is not opened any more
        headerOpened(headerFile_, true);

        // init this list with the data from the other list
        // first copy the header file
        headerName_ = other.headerName_;
        dataPath_ = other.dataPath_;
        // We don't want to overwrite the existing header
        setHeaderFileName(dataPath_, headerName_);
        // Throw an exception if the given header is already opened at this moment
        if (headerOpened(headerFile_))
          DUNE_THROW(Dune::IOError, "The given header is already opened by another list! (=-operator)");
        boost::filesystem::copy_file(other.headerFile_,this->headerFile_);
        init();
        return *this;
      }

    private:
      void init() {
        //open inward file stream to read indices and attributes to this list...
        std::ifstream in(headerFile_.c_str());
        if (!in)
          {
            //...and throw an exeption if the headerfile doesn't exist
            DUNE_THROW(IOError, "Could not open " << headerFile_ << " for reading!");
          }

        std::string row;
        std::string attributefile, datafile;
        AttributeType attr;
        std::string filename;
        unsigned int index;
        //read given header file
        while (getline(in, row))
          {
            // ignore blank lines
            if (strlen(row.c_str())==0)
              continue;

            // ignore comments
            if (row[0] == '#')
              continue;

            //wrap matlab-like "[a:b:c]" terms
            if ( row.find('[')!=std::string::npos )
              {
                // we found an occurrence of a "[a:b:c]" term, delete all preceding text
                row.erase( 0, row.find('[')+1 );
                int a,b,c;
                // \todo Only numbers of length one are covered here!!!
                a=atoi(&row[0]);
                b=atoi(&row[2]);
                c=atoi(&row[4]);

                for (int i=a; i<=c; i+=b)
                  {
                    index=nextIndex();
                    std::stringstream foo;
                    foo << a;
                    filename = foo.str();

                    //read datafile name ...

                    datafile = dataPath_ ;
                    datafile += filename;
                    datafile += ".dat";
                    //and test if datafile exists in current path...
                    std::ifstream test( datafile.c_str() );
                    if (!test)
                      {
                        //...throw an exeption if it can't be opened
                        DUNE_THROW(IOError, "Could not open " << datafile << " for reading!");
                      }
                    test.close();

                    //save filename
                    filenames_.push_back(make_pair(index, filename));

                    //get attributefile name
                    attributefile = dataPath_;
                    attributefile += filename;
                    attributefile += ".att";
                    //and read attribute from attribute file, existence is checked in AttributeActions::read
                    attrAct_.read(attr, attributefile);

                    attributes_.push_back(make_pair(index, attr));
                  }
                continue;
              }


            index=nextIndex();
            std::string::size_type pos = row.find('.');
            //if there is a line containing "." read only the name without extension
            if ( pos != std::string::npos )
              filename = row.substr(0, pos);
            else
              filename = row;

            //construct datafile name ...
            datafile = dataPath_ ;
            datafile += filename;
            datafile += ".dat";
            //and test if datafile exists in current path...
            std::ifstream test( datafile.c_str() );
            if (!test)
              {
                //...throw an exeption if it can't be opened
                DUNE_THROW(IOError, "Could not open " << datafile << " for reading!");
              }
            test.close();

            //save filename
            filenames_.push_back(make_pair(index, filename));
            //construct attributefile name
            attributefile = dataPath_;
            attributefile += filename;
            attributefile += ".att";
            //and read attribute from attribute file, existence is checked in AttributeActions::read
            attrAct_.read(attr, attributefile);

            attributes_.push_back(make_pair(index, attr));
          }

        // //if header file was empty, give a warning
        // if (attributes_.size()==0)
        //   std::cout << "Warning! Headerfile " << headerFile_ << " is empty!\n";

        //open header for this list
        header_.open( headerFile_.c_str(), std::ios::out|std::ios::app );
      }

    public:

      /**destructor, close the header file*/
      ~Xdr()
      {
        // clear all stashed functions
        dropStash();

        if (header_.is_open()) {
          // make shure all changes have been written to the header file
          header_.flush();
          // close the header file
          header_.close();
        }
        // we don't use the header anymore
        headerOpened(headerFile_, true);
      };

      void deleteHeaderFile() {
        // prohibit further operations on this list
        cleared_=true;
        boost::filesystem::remove(headerFile_);
        return;
      }

      /**
       * \brief  store discrete function in list
       *
       *  writes given discrete function to disc and stores given attribute
       *  to the given discrete function.
       *  \param[in]  discFunc    The discrete function to be stored
       *  \param[in]  attr        The parameter to be stored with discrete function,
       *                          name for instance
       *
       *  \return returns the unique index of this function in the list
       */
      inline int push_back(const DiscreteFunctionType& discFunc, AttributeType attr=AttributeType())
      {
        // check if this function list has been cleared
        checkCleared();

        //we started a new run, get a unique index for the filename
        const int currentIndex=nextIndex();

        //transform currentIndex to string and append ".dat" for the datafile
        std::stringstream datafile;
        datafile << dataPath_ << currentIndex << ".dat";

        //append ".attr" to the current index for the attribute file
        std::stringstream attributefile;
        attributefile << dataPath_ << currentIndex << ".att";

        //write discFunc and attribute to disc
        discFunc.write_xdr(datafile.str());
        attrAct_.write(attr, attributefile.str().c_str());

        //write information about this functions' location to header file
        header_ << currentIndex << ".dat" << std::endl;

        std::pair<int, AttributeType> paar=make_pair(currentIndex, attr);
        attributes_.push_back(paar);
        std::stringstream indexAsString;
        indexAsString << currentIndex;
        std::pair<int, std::string> filenamepaar = make_pair(currentIndex, indexAsString.str());
        filenames_.push_back(filenamepaar);
        return currentIndex;
      }

      /**
       *  \brief returns the number of stored discrete functions
       *
       *  \return returns the number of stored discrete functions
       */
      inline unsigned int size() const
      {
        // check if this function list has been cleared
        checkCleared();
        return this->attributes_.size();
      }

    private:
      /**
       *  \brief  returns discrete function with unique identifier index.
       *
       *  Assumes, that grid didn't change
       *  since discFunc was written to disk
       *  \param[in]  index index of the desired discrete function
       *  \param[out]  dest result is stored to dest
       *  \return returns true if discFunc was read without errors
       */
      bool getFuncByIndex(const unsigned int index, DiscreteFunctionType& dest) const
      {
        // check if this function list has been cleared
        checkCleared();
        //typedef
        typedef typename vector<std::pair<int, std::string> >::const_iterator IteratorType;

        std::string datafile = dataPath_;
        //look for matching index in filename vector
        for (IteratorType it=this->filenames_.begin(); it!=this->filenames_.end(); ++it)
          if (it->first==index)
            {
              datafile += it->second;
            }
        datafile += ".dat";
    
        dest.clear();
        return dest.read_xdr(datafile);
      }
  
    public:
      /**
       *  \brief  returns discrete function number i.
       *
       *  Get the i'th function,
       *  numbering may change during runtime due to deletion of functions.
       *  Assumes, that grid didn't change since discFunc was written to disk.
       *
       *  \param[in]  i number of the desired discrete function
       *  \param[out]  dest result is stored to dest
       *  \return returns true if discFunc was read without errors
       */
      inline bool getFunc(unsigned int i, DiscreteFunctionType& dest) const
      {
        // check if this function list has been cleared
        checkCleared();
        assert(i<filenames_.size());
        if (i>=filenames_.size())
          DUNE_THROW(RangeError, "The given index is out of the lists range!");
        std::string filename = filenames_[i].second;
      
        dest.clear();
        std::string arg = dataPath_;
        arg+=filename;
        arg+=".dat";
        bool readWithoutErrors = dest.read_xdr(arg);
        return readWithoutErrors;
      }

      inline void print(std::ostream &out) const {
        // check if this function list has been cleared
        checkCleared();
        assert(discFuncSpace_!=NULL);
        int size = this->size();
        DiscreteFunctionType buffer("buffer", *discFuncSpace_ );
        for (int i=0; i!=size; ++i) {
          this->getFunc(i, buffer);
          buffer.print(out);
        }

      }

      /**
       *  \brief sets the i'th function.
       *
       *  Set the i'th function,
       *  numbering may change during runtime due to deletion of functions.
       *  Assumes, that grid didn't change since discFunc was written to disk.
       *
       *  \param[in]  i number of the discrete function that is to be altered
       *  \param[in] arg the function that should replace the i'th function
       *  \return returns true if discFunc was written without errors
       */
      inline bool setFunc(unsigned int i, DiscreteFunctionType& arg) const
      {
        // check if this function list has been cleared
        checkCleared();
        assert(i>=0 && i<filenames_.size());

        std::string filename = filenames_[i].second;

        std::string dest = dataPath_;
        dest+=filename;
        dest+=".dat";
        if (arg.write_xdr(dest))
          {
            return true;
          }
        return false;
      }

      /**@brief Stash a given range of functions to the memory.
       *
       * This may be usefull if memory is no issue but a certain range
       * of function is needed often in a row. The functions can then
       * be accesses through the method getFunc(const int i).
       *
       * @attention This will clear all currently stashed functions!
       *
       * @param[in] first The index of the first function to be loaded
       * @param[in] last  Index one higher than the last function to be loaded.
       */
      void stashFunctions(const int first, const int onePastLast) const {
        assert(first>=0);
        assert(onePastLast<=int(size()));
        typedef typename std::vector<DiscreteFunctionType*>::iterator Iterator;
        // clear all current functions
        this->dropStash();

        // now, get the functions in range
        int mapIndex = 0;
        for (int i=first; i!=onePastLast; ++i) {
          std::stringstream name;
          name << "Stashed Function " << i;
          DiscreteFunctionType* stashedFunc = new DiscreteFunctionType(name.str(), *discFuncSpace_);
          getFunc(i, *stashedFunc);
          stashedFunctions_.push_back(stashedFunc);
          stashedFunctionsMap_[i] = mapIndex;
          ++mapIndex;
        }
      }

      /**@brief Stash all functions to the memory.
       *
       * This may be usefull if memory is no issue but the functions are needed often in a row. 
       * The functions can the be accesses through the method getFunc(const int i).
       *
       * @attention This will clear all currently stashed functions!
       *
       */
      void stashFunctions() const {
        stashFunctions(0, this->size());
        return;
      }

      /** @brief Drop the stashed functions, that is: Clear the memory
       *         taken by them.
       *
       */
      void dropStash() const {
        typedef typename std::vector<DiscreteFunctionType*>::iterator Iterator;
        Iterator end = stashedFunctions_.end();
        for (Iterator it = stashedFunctions_.begin(); it!=end; ++it)
          if (*it!=0) {
            delete *it;
            *it=0;
          }
        stashedFunctions_.clear();
        stashedFunctionsMap_.clear();
      } 

      /**
       *  \brief  returns discrete function number i.
       *
       *  Get the i'th function. This assumes, that the respective
       *  function was previously stashed using the method stashFunctions.
       *  
       *  @remark The function cannot be changed!
       *
       *  Assumes, that grid didn't change
       *  since discFunc was written to disk.
       *
       *  \param[in]  i number of the desired discrete function
       *  \return returns a const reference to the i'th discrete function
       */
      const DiscreteFunctionType& getFunc(const unsigned int i) const {
        typedef std::map<int,int>::const_iterator Iterator;
        Iterator it = stashedFunctionsMap_.find(i);
        assert(it!=stashedFunctionsMap_.end());
        return *(stashedFunctions_[it->second]);
      }

      /**
       *  \brief  returns discrete function number i.
       *
       *  Get the i'th function. This assumes, that the respective
       *  function was previously stashed using the method stashFunctions.
       *  
       *  Assumes, that grid didn't change
       *  since discFunc was written to disk.
       *
       *  \param[in]  i number of the desired discrete function
       *  \return returns a const reference to the i'th discrete function
       */
      DiscreteFunctionType& getFunc(const unsigned int i) {
        typedef std::map<int,int>::const_iterator Iterator;
        Iterator it = stashedFunctionsMap_.find(i);
        assert(it!=stashedFunctionsMap_.end());
        return *(stashedFunctions_[it->second]);
      }


      /** \brief return the i'th attribute
       *
       *  The i'th attribute is returned,
       *  numbering may change during runtime due to deletion of functions and attributes.
       *
       *  \param[in]  i number of the desired discrete attribute
       *  \param[out]  dest result is stored to dest
       *  \return returns true if attribute was read without errors
       */
      inline bool getAttribute(int i, AttributeType &dest) const
      {
        // check if this function list has been cleared
        checkCleared();
        std::string filename = dataPath_;
        filename += filenames_[i].second;
        filename += ".att";
        if (attrAct_.read(dest, filename))
          {
            return true;
          }
        return false;
      }

      /**
       *  \brief  get discrete function by attribute
       *
       *  returns the first discrete function in functions_ with matching parameter, i.e. if attribute is not
       *  unique, you may not get the expected discfunc back! Assumes, that grid didn't change since discFunc
       *  was written. Also, if you didn't push back the functions togther with an attribute, this doesn't make
       *  sense!
       *
       *  \param[in]  attr the attribute of the desired discrete function
       *  \param[out]  dest result is stored to dest
       *  \return returns true if discFunc was read without errors
       */
      bool getFuncByAttribute(const AttributeType& attr, DiscreteFunctionType& dest) const
      {
        // check if this function list has been cleared
        checkCleared();
        //typedef
        typedef typename vector<std::pair<int, AttributeType> >::const_iterator IteratorType;

        //look for matching attribute in attribute vector
        for (IteratorType it=this->attributes_.begin(); it!=this->attributes_.end(); ++it)
          if (it->second==attr) {
            //it->first==index for correspondig discrete function
            if (this->getFuncByIndex(it->first, dest))
              return true;
            return false;
          }
        return false;
      }

    private:
      static int nextIndex(int i=-1)
      {
        static int id=0;
        if (i!=-1)
          id=i;
        return id++;
      }


      /** Check if a given header is opened at the moment.
      
          @param[in] headerName the name of the header
          @param[in] del Shall we delete the given header from the list of open headers?
          @return  returns true if the header is already open, false otherwise
      */
      static bool headerOpened(const std::string &headerName, bool del=false) {
        static std::vector<std::string> takenNames;

        typedef std::vector<std::string>::iterator Iterator;
        Iterator begin=takenNames.begin();
        Iterator end=takenNames.end();
        Iterator pos = std::find(begin, end, headerName);
        if (pos==end && (!del)) {
          takenNames.push_back(headerName);
          return false;
        }
        else if (del) {
          takenNames.erase(pos);
          return true;
        }
        return true;
      }

    public:
      //!return underlying space
      const DiscreteFunctionSpaceType& space() const
      {
        assert(discFuncSpace_!=NULL);
        // check if this function list has been cleared
        checkCleared();
        return *discFuncSpace_;
      }

      /** @brief delete all functions in this list
       *
       *  That means, that all functions, managed by this list
       *  are deleted from the hard disk! If a file belonging to
       *  to this list can not be deleted, an error message is 
       *  given to std:cerr.
       *
       *  @return returns 0 if list was deleted without errors, -1 if
       *          there where errors.
       */
      int clear() {
        // check if this function list has been cleared 
        checkCleared();
        int size = filenames_.size();
        int returnarg = 0;
        // delete a attribute and xdr files
        for (int i=0; i!=size; ++i) {
          std::string xdrfile = dataPath_;
          xdrfile += filenames_[i].second;
          std::string attributefile = xdrfile;
          attributefile += ".att";
          xdrfile += ".dat";
          if (boost::filesystem::remove(attributefile)!=1) {
            std::string errormsg;
            errormsg += "Error clearing function list (in file ";
            errormsg += filenames_[i].second;
            errormsg += ".att)";
            perror(errormsg.c_str());
            returnarg = -1;
          }
          if (boost::filesystem::remove(xdrfile)!=1) {
            std::string errormsg;
            errormsg += "Error clearing function list (in file ";
            errormsg += filenames_[i].second;
            errormsg += ".dat)";
            perror(errormsg.c_str());
            returnarg = -1;
          }
        }
        // close header file
        header_.close();
        // now, delete header file
        if (boost::filesystem::remove(headerFile_)!=1) {
          std::string errormsg;
          errormsg += "Error clearing function list (in file ";
          errormsg += headerFile_;
          errormsg += ")";
          perror(errormsg.c_str());
          returnarg = -1;
        }
        // finally, delete the containing directory
        // rmdir only deletes the directory if it is empty
        if (boost::filesystem::is_empty(dataPath_) == 0) {
          std::string errormsg;
          errormsg += "Error deleting folder containing function list ( ";
          errormsg += dataPath_;
          errormsg += ")";
          if (verbose_)
            perror(errormsg.c_str());
          returnarg = -1;
        }
        else if (boost::filesystem::remove_all(dataPath_) == 0) {
          std::string errormsg;
          errormsg += "Error deleting folder containing function list ( ";
          errormsg += dataPath_;
          errormsg += ")";
          if (verbose_)
            perror(errormsg.c_str());
          returnarg = -1;
        }
        cleared_ = true;
        return returnarg;
      };

    private:
      void checkCleared() const {
        // check if function list has been cleared
        if ( cleared_ == true )
          DUNE_THROW(InvalidStateException, "This discrete function list has been cleared already, further operations on it are prohibited!");
      };


    protected:
      std::string                                headerFile_;
      std::string                                headerName_;
      std::vector<std::pair<int, AttributeType> > attributes_;
      std::vector<std::pair<int, std::string> >   filenames_;
      std::string                                dataPath_;
      const DiscreteFunctionSpaceType*           discFuncSpace_;
      AttributeActions<AttributeType>            attrAct_;
      std::ofstream                              header_;
      bool                                       cleared_;
      mutable std::vector<DiscreteFunctionType*> stashedFunctions_;
      mutable std::map<int,int>                  stashedFunctionsMap_;
      bool                                       verbose_;
    };

}
}
}
#endif
