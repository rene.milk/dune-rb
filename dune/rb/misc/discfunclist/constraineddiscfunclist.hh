#ifndef __CONSTRAINEDDISCFUNCLIST_HH__
#define __CONSTRAINEDDISCFUNCLIST_HH__

#include <string>
#include <dune/rb/misc/discfunclist/discfunclist_xdr.hh>


//! \todo Mention, that this only works for XDR lists at the moment!!!
namespace Dune {
namespace RB {

  //  template<class Traits, template<class> class DiscreteFunctionListImp>
  template<class DiscreteFunctionImp>
    class ConstrainedDiscreteFunctionList : public RBFem::FunctionList::Xdr<DiscreteFunctionImp>
{

  typedef RBFem::FunctionList::Xdr<DiscreteFunctionImp> BaseType;

public:
  typedef DiscreteFunctionImp                             DiscreteFunctionType;
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType::MacroElement              MacroElement;
  typedef typename DiscreteFunctionType::MDGridPart                MDGridPart;
  typedef typename BaseType::AttributeType                         AttributeType;

  using BaseType::getFunc;

public:
  ConstrainedDiscreteFunctionList(const DiscreteFunctionSpaceType &discFuncSpace,
                                  const std::string dataPath)
    : BaseType(discFuncSpace, dataPath),
      mdGridPart_(&(discFuncSpace.gridPart()))
  {};
  ConstrainedDiscreteFunctionList(const DiscreteFunctionSpaceType &discFuncSpace, 
                                  const std::string path, 
                                  const std::string name)
    : BaseType(discFuncSpace, path, name),
      mdGridPart_(&(discFuncSpace.gridPart()))
  {};


  ConstrainedDiscreteFunctionList() 
    :BaseType(),
     mdGridPart_(NULL)
  {};
  
  inline int push_back(const DiscreteFunctionType& discFunc, AttributeType attr=AttributeType()) {
    if (mdGridPart_==NULL)
      mdGridPart_ = &(discFunc.gridPartition());
    return BaseType::push_back(discFunc, discFunc.getSupport());
  }

  /**@brief Stash a given range of functions to the memory.
   *
   * This may be usefull if memory is no issue but a certain range
   * of function is needed often in a row. The functions can then
   * be accesses through the method getFunc(const int i).
   *
   * @attention This will clear all currently stashed functions!
   *
   * @param[in] first The index of the first function to be loaded
   * @param[in] last  Index one higher than the last function to be loaded.
   */
  void stashFunctions(const unsigned int first, const unsigned int onePastLast) const {
    assert(first>=0);
    assert(onePastLast<=BaseType::size());
    typedef typename std::vector<DiscreteFunctionType*>::iterator Iterator;
    // clear all current functions
    Iterator end = BaseType::stashedFunctions_.end();
    for (Iterator it = BaseType::stashedFunctions_.begin(); it!=end; ++it)
      delete *it;
    BaseType::stashedFunctions_.clear();
    BaseType::stashedFunctionsMap_.clear();

    // now, get the functions in range
    int mapIndex = 0;
    for (unsigned int i=first; i!=onePastLast; ++i) {
      std::stringstream name;
      name << "Stashed Function " << i;
      DiscreteFunctionType* stashedFunc = new DiscreteFunctionType(name.str(), *BaseType::discFuncSpace_);
      this->getFunc(i, *stashedFunc);
      BaseType::stashedFunctions_.push_back(stashedFunc);
      BaseType::stashedFunctionsMap_[i] = mapIndex;
      ++mapIndex;
    }

    return;
  }
  
  void stashFunctions() const {
    stashFunctions(0, this->size());
    return; 
  }

  inline bool getFunc(unsigned int i, DiscreteFunctionType& dest) const {
    if (BaseType::getFunc(i, dest)) {
      assert(mdGridPart_!=NULL);
      MacroElement supp;
      getAttribute(i, supp);
      dest.setSupport(supp);
      dest.setGridPartition(*mdGridPart_);
      return true;
    }
    return false;
  }

private:
  const MDGridPart* mdGridPart_;

};

}
}
#endif /* __CONSTRAINEDDISCFUNCLIST_HH__ */
