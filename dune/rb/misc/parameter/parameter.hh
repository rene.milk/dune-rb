#ifndef DUNE_RB_PARAMETER_HH
#define DUNE_RB_PARAMETER_HH


#include <map>
#include <queue>
#include <iostream>
#include <fstream>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/common/static_assert.hh>

//#include <dune/common/fvector.hh>

#include <dune/fem/misc/validator.hh>
#include <dune/fem/misc/mpimanager.hh>
#include  <dune/fem/io/parameter.hh>

// boost stuff
#include <boost/lexical_cast.hpp>

// Eigen stuff
#include <Eigen/Core>

namespace Dune {

struct MuRangeType {
  double from;
  double to;

  MuRangeType() : from(0), to(0) {};
  MuRangeType(const MuRangeType & copy) : from(copy.from), to(copy.to) {};

  bool isValid(const double test) const
  {
    return (from <= test && test <= to);
  }

  friend std::ostream & operator<<(std::ostream & os, const MuRangeType mr)
  {
    os << "[" << mr.from << ", " << mr.to << "]";
    return os;
  }
};

/** \brief specialization of the parse method for a vector of range intervals 
 *
 * \param[in] s argument string probably associated with the key 'mu_ranges'
 * \param[out] value vector of range intervals
 */
template<>
struct ParameterParser< std::vector<MuRangeType> > 
{
  static bool parse(const std :: string & s, std::vector<MuRangeType> &value ) {
    size_t last_found = 0;
    size_t found = s.find_first_of("[],");
    enum {
      bracket_open,
      bracket_closed,
    } state = bracket_closed;
    MuRangeType temp;
    
    bool ret = true;
    
    while(found != std::string::npos)
      {
        if( s[found] == '[' ) {
          state = bracket_open;
        } else if ( s[found] == ',') {
          if( state == bracket_open ) {
            std::string subs = s.substr(last_found, found-last_found);
            ret &= ParameterParser<double>::parse(subs, temp.from);
          }
        } else if ( s[found] == ']') {
          std::string subs = s.substr(last_found, found-last_found);
          ret &= ParameterParser<double>::parse(subs, temp.to);
          
          value.push_back(temp);
          state = bracket_closed;
        }
        last_found = found + 1;
        found = s.find_first_of("[],", last_found);
      }
    return ret;
  }

    static std::string toString ( std::vector<MuRangeType> &value ) {
      typedef std::vector<MuRangeType>::const_iterator IteratorType;
      std::string ret;
      IteratorType end = value.end();
      IteratorType it = value.begin();
      ret += "[" + boost::lexical_cast<std::string>(it->from)
        + "," + boost::lexical_cast<std::string>(it->to) + "]";
      ++it;
      for (; it!=end; ++it) {
         ret += " [" + boost::lexical_cast<std::string>(it->from)
        + "," + boost::lexical_cast<std::string>(it->to) + "]";
      }
      return ret;
    }

};

/** \brief specialization of the parse method for a list of strings
 * 
 * \param[in] s string probably associated with the parameter key 'mu_names'
 * \param[out] value vector of strings
 */
template<>
struct ParameterParser< std::vector<std::string> > 
{
  static bool parse(const std :: string & s, std::vector<std::string> &value )  {
    size_t last_found = 0;
    size_t found = s.find_first_of("\"");
    enum {
      bracket_open,
      bracket_closed,
    } state = bracket_closed;
    /*  double temp[2];*/

    while(found != std::string::npos)
      {
        if( s[found] == '"' ) {
          if( state == bracket_open ) {
            std::string subs = s.substr(last_found, found-last_found);
            value.push_back(subs);
            state = bracket_closed;
          } else {
            state = bracket_open;
          }
        }
        last_found = found + 1;
        found = s.find_first_of("\"", found+1);
      }
    return true;
  }

    static std::string toString ( std::vector<std::string> &value ) {
      typedef std::vector<std::string>::const_iterator IteratorType;
      std::string ret;
      IteratorType end = value.end();
      IteratorType it = value.begin();
      ret += *it;
      ++it;
      for (; it!=end; ++it) {
        ret += " " + *it;
      }
      return ret;
    }
};

/** \brief specialization of the parse method for a vector of doubles
 *
 * \param[in] s argument string
 * \param[out] value vector of doubles
 */
template<>
struct ParameterParser< std::vector<double> > 
{
  static bool parse(const std :: string & s, std::vector<double> &value )
  {
    size_t last_found = 0;
    size_t found = s.find_first_of(",");
    double temp;

    bool ret = true;

    do
      {
        if(found == std::string::npos)
          found = s.size();
        std::string subs = s.substr(last_found, found-last_found);
        last_found = found+1;
        ret &= ParameterParser<double>::parse(subs, temp);
        value.push_back(temp);
        found = s.find_first_of(",", last_found);
      } while(last_found != s.size()+1);

    return ret;
  }

    static std::string toString ( std::vector<double> &value ) {
      typedef std::vector<double>::const_iterator IteratorType;
      std::string ret;
      IteratorType end = value.end();
      IteratorType it = value.begin();
      ret += boost::lexical_cast<std::string>(*it);
      ++it;
      for (; it!=end; ++it) {
        ret += " " + boost::lexical_cast<std::string>(*it);
      }
      return ret;
    }

};

/** \brief specialization of the parse method for a Eigen::MatrixBase
 *
 * \param[in] s argument string
 * \param[out] value vector of doubles
 */
template<>
struct ParameterParser<Eigen::VectorXd> 
{
  typedef Eigen::VectorXd VectorType;
  static bool parse(const std :: string & s, VectorType &value )
  {
    size_t last_found = 0;
    size_t found = s.find_first_of(",");
    double temp;

    bool ret = true;
    value = VectorType::Zero(0);
    do
      {
        if(found == std::string::npos)
          found = s.size();
        std::string subs = s.substr(last_found, found-last_found);
        last_found = found+1;
        ret &= ParameterParser<double>::parse(subs, temp);
        value.conservativeResize(value.rows()+1);
        value(value.rows()-1)=temp;
        found = s.find_first_of(",", last_found);
      } while(last_found != s.size()+1);

    return ret;
  }

    static std::string toString ( VectorType &value ) {
      std::stringstream ret;
      ret << value.transpose();
      return ret.str();
    }

};

#if 0
/** \brief specialization of the parse method for a vector of ints
 *
 * \param[in] s argument string
 * \param[out] value vector of doubles
 */
template<>
struct ParameterParser< std::vector<double> > 
{
  static bool parse(const std :: string & s, std::vector<int> &value )
  {
    size_t last_found = 0;
    size_t found = s.find_first_of(",");
    int temp;

    bool ret = true;

    do
      {
        if(found == std::string::npos)
          found = s.size();
        std::string subs = s.substr(last_found, found-last_found);
        last_found = found+1;
        ret &= parse(subs, temp);
        value.push_back(temp);
        found = s.find_first_of(",", last_found);
      } while(last_found != s.size()+1);

    return ret;
  }

    static std::string toString ( std::vector<int> &value ) {
      typedef std::vector<std::string>::const_iterator IteratorType;
      std::string ret;
      IteratorType end = value.end();
      IteratorType it = value.begin();
      ret += boost::lexical_cast<std::string>(*it);
      ++it;
      for (; it!=end; ++it) {
        ret += " " + boost::lexical_cast<std::string>(*it);
      }
      return ret;
    }

};

/** \brief specialization of the parse method for a FieldVector
 *
 * \param[in] s argument string
 * \param[out] value vector of doubles
 */
template<int dim>
  inline void
Parameter :: parse(const std :: string & s, FieldVector<double, dim> &value )
{
  size_t last_found = 0;
  size_t found = s.find_first_of(",");
  double temp;

  unsigned int i = 0;
  do
  {
    if(found == std::string::npos)
      found = s.size();
    std::string subs = s.substr(last_found, found-last_found);
    last_found = found+1;
    parse(subs, temp);
    value[i] = temp;
    found = s.find_first_of(",", last_found);
    ++i;
  } while(last_found != s.size()+1 && i < dim-1);
}
#endif

namespace RBFem {

class ParameterHelper
{
public:

  static bool isInMuNames(const std::string & str)
  {
    std::vector<std::string> mu_namesList;
    Parameter::get("rb.mu_names", mu_namesList);
    if( std::find(mu_namesList.begin(), mu_namesList.end(), str)
        != mu_namesList.end() )
      return true;
    else
      return false;
  }

  template<int dim>
  static std::string vectorParamString(const std::string & prefix)
  {
    dune_static_assert((dim > 2), "You must be crazy! More than 3 dimensions?");
    return "crazy error!";
  }

  template<typename FVType>
  static void fillVector(const std::string & prefix, FVType & fv,
                         const FVType & defvec );

};

template<>
std::string ParameterHelper :: vectorParamString<0>(const std::string & prefix)
{
  return prefix + "_x";
}

template<>
std::string ParameterHelper :: vectorParamString<1>(const std::string & prefix)
{
  return prefix + "_y";
}
template<>
std::string ParameterHelper :: vectorParamString<2>(const std::string & prefix)
{
  return prefix + "_z";
}

template<typename FVType>
void ParameterHelper::fillVector(const std::string & prefix, FVType & fv,
                                 const FVType & defvec )
{
  const int dim = FVType::size;
  typedef typename FVType :: field_type                             T;
  std::string name1 = vectorParamString<0>(prefix);
  if(Parameter::exists(name1))
    fv[0] = Parameter::getValue<T>(name1);
  else
    fv[0] = defvec[0];
  if(dim >= 1)
  {
    std::string name2 = vectorParamString<1>(prefix);
    if(Parameter::exists(name2))
      fv[1] = Parameter::getValue<T>(name2);
    else
      fv[1] = defvec[1];
  }
  if(dim >= 2)
  {
    std::string name3 = vectorParamString<2>(prefix);
    if(Parameter::exists(name3))
      fv[2] = Parameter::getValue<T>(name3);
    else
      fv[2] = defvec[2];
  }
}

} // end of namespace Dune :: RB
} // namespace Dune



#endif // DUNE_RB_PARAMETER_HH
