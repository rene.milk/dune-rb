#include <config.h>

#include <dune/common/stdstreams.cc>


#include <dune/fem/misc/mpimanager.hh>
#include "../parameter.hh"

using Dune::MPIManager;
using Dune::Parameter;
using Dune::MuRangeType;

static const char paramfiledir[] = PARAMFILEDIR;

int main (int argc, char **argv)
{
  int ret = 0;

  MPIManager :: initialize( argc, argv );

  Parameter::append(std::string(paramfiledir) + std::string("/testparams"));
  try {

  std::vector<double> test1;
  std::vector<double> test2;

  test1 = Parameter::getValue<std::vector<double> >("test.test1");
  Parameter::get("test.test2", test2);

  if( test2.size() != 1 )
  {
    std::cerr << "wrong number of elements in test2! got: " << test2.size() << "!" << std::endl;
    ret = 10;
  }
  else if( test2[0] != 0.5 )
  {
    std::cerr << "wrong values in test2!" << std::endl;
    ret = 11;
  }

  if( test1.size() != 3 )
  {
    std::cerr << "wrong number of elements in test1, got: " << test1.size() << "!" << std::endl;
    ret = 12;
  }
  else if( test1[0] != 1 || test1[1] != 2 || test1[2] != 3 )
  {
    std::cerr << "wrong values in test1!" << test1[0] << " " << test1[1] << " " << test1[2] << std::endl;
    ret = 13;
  }

  std::vector<std::string> mu_names;
  Parameter::get("test.mu_names", mu_names);

  if( mu_names.size() != 2 ) {
    std::cerr << "wrong number of mu_names:" << std::endl;
    std::cerr << "expected: 2, got: " << mu_names.size() << std::endl;
    ret = 1;
  }
  if( mu_names[1] != "test.param2" ) {
    std::cerr << "expected string \"test.param2\", got: " << mu_names[1] << std::endl;
    ret = 2;
  }

  std::vector<MuRangeType > mu_ranges;
  Parameter::get("test.mu_ranges", mu_ranges);

  if( mu_ranges.size() != 2 ) {
    std::cerr << "wrong number of mu_ranges\n\
        expected: 2, got: " << mu_ranges.size() << std::endl;
    ret = 3;
  }
  if( mu_ranges[1].from != 0 || mu_ranges[1].to != 2 ) {
    std::cerr << "read wrong range for second parameter" << std::endl;
    ret = 4;
  }

  std::vector<double> values;
  for( unsigned int i = 0 ; i < mu_names.size() ; ++i ) {
    values.push_back(0.5);
  }

  for( unsigned int i = 0 ; i < mu_names.size() ; ++i )
    Parameter::replaceKey(mu_names[i], values[i]);

/*  Parameter::setMu(mu_names, values);*/

  for( unsigned int i = 0 ; i < mu_names.size() ; ++i ) {
    if( Parameter::getValue<double>(mu_names[i]) != 0.5 ) {
      std::cerr << "setMu failed" << std::endl;
      ret = 5;
    }
  }


  }
  catch (Dune::Exception &e) {
    std::cerr << e << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }

  return ret;
}

