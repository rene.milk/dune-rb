#ifndef DUNE_RB_MISC_DUCKTYPING_HH_
#define DUNE_RB_MISC_DUCKTYPING_HH_

#include <dune/common/exceptions.hh>

#ifdef DUNE_DUCKTYPING

#define __DUCK_START__ virtual
#define __DUCK_END__   =0;

#else

#define __DUCK_START__

#ifndef NDEBUG
#define __DUCK_END__   \
{ DUNE_THROW(NotImplemented, "Method not implemented. Use flag -DDUNE_DUCKTYPING=1 for details"); }
#else
#define __DUCK_END__ ;
#endif

#endif

#endif /* DUNE_RB_MISC_DUCKTYPING_HH_ */

