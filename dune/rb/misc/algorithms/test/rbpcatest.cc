#include <config.h>
#include <dune/rb/rbasis/twophaseflow/rb/reducedoperator.hh>

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>


// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridinfo.hh>

// multidomaingrid
#include <dune/rb/rbasis/twophaseflow/grid/multidomaingridpart.hh>

// fem stuff
//#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/file/datawriter.hh>
#include <dune/fem/operator/matrix/spmatrix.hh>
#include <dune/fem/solver/inverseoperators.hh>
// l2 norm and h1 norm 
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/h1norm.hh>

// rb stuff
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>
#include <dune/rb/rbasis/reducedbasisspace/constrainedrbspace.hh>
#include <dune/rb/rbasis/twophaseflow/rb/rbgeneration.hh>

#include <dune/rb/rbasis/twophaseflow/highdim/algorithm.hh>
#include <dune/rb/rbasis/twophaseflow/algorithms/restrictfunction.hh>
#include <dune/rb/rbasis/twophaseflow/helpers/math.hh>



// visualizer for functions
#include <dune/rb/rbasis/twophaseflow/helpers/plotfunction.hh>
#include <dune/grid/io/visual/grapedatadisplay.hh>

using namespace Dune;
using namespace RB;

int main(int argc, char *argv[])
{
  try
  {
    typedef GridSelector :: GridType                                 GridType;
    typedef LeafGridPart< GridType >                                 GridPart;
    typedef FunctionSpace< double, double, GridType :: dimension,
                           1 >                                       FunctionSpace;
    typedef LagrangeDiscreteFunctionSpace< FunctionSpace, GridPart,
                                           1 >                       DiscFuncSpace;
    typedef AdaptiveDiscreteFunction< DiscFuncSpace >                DiscreteFunction;
    typedef AdaptiveDiscreteFunction< RBSpace >                      ReducedFunction;
    typedef DiscreteFunctionList_xdr< ReducedFunction >              ReducedList;
    typedef DiscFuncSpace :: IteratorType                            IteratorType;
    typedef DiscFuncSpace :: EntityType                              Entity;
    typedef RBGenerationTraits< RBSpace >                            RBGenerationTraits;
    typedef RBGeneration< RBGenerationTraits >                       RBGeneration;
    typedef Dune :: SparseRowMatrixTraits< RBSpace, RBSpace >        MatrixObjectTraits;
    typedef ReducedOperator< ReducedList, MatrixObjectTraits >       LinearOperatorType;
    typedef ProblemInterface< FunctionSpace >                        ProblemType;
    //---- InverseOperator ----------------------------------------------------
    //---- good choice for build in CG solver ---------------------------------
    //! define the inverse operator we are using to solve the system 
    typedef Dune::CGInverseOp<ReducedFunction, LinearOperatorType>
      //---- other choices ------------------------------------------------------ 
      //    typedef Dune::ISTLBICGSTABOp<ReducedFunction, LinearOperatorType>
      //typedef ISTLCGOp< DiscreteFunctionType, LaplaceOperatorType >
      //typedef OEMCGOp<ReducedFunction, LinearOperatorType>
      //typedef Dune::OEMBICGSTABOp<ReducedFunction, LinearOperatorType>
      //typedef Dune::OEMBICGSQOp<ReducedFunction, LinearOperatorType>
      //typedef Dune::OEMGMRESOp<ReducedFunction, LinearOperatorType>
      InverseOperatorType;

    // class ReducedAlgorithm {

    //   ReducedAlgorithm(MDGridPart& mdGridPart) : mdGridPart_(mdGridPart)
    //   {}
    // public:
    //   MDGridPart& mdGridPart_;
    // };

    MPIManager::initialize(argc, argv);
    Parameter::append("rbpcatest.params");

    GridPtr<GridType> gridPtr("unitcube2.dgf");
    GridType &grid = *gridPtr;
    // refine the grid globally
    grid.globalRefine(Parameter::getValue<int>("fem.globalRefine", 0));
    // print some information about the grid
    Dune::gridinfo(grid);
    GridPart gridPart(grid);
    Dune::VTKWriter<GridType::LeafGridView> vtkwriter(grid.leafView());
    vtkwriter.write("grid");


    DiscFuncSpace discFuncSpace(gridPart);

    // create problem 
    ProblemType* problem = createProblem<GridType>();
    assert( problem );

    std::vector<MuRangeType> mu_ranges;
    Dune::Parameter::get( "rb.mu_ranges", mu_ranges );

    // prepare the convergence table
    std::ofstream file("errors.dat");
    file << "{PCA Percentage} {Mean L2 Error} {Mean H1 Error} {STDDeviation L2} "
         << "{STDDeviation H1} {Max L2 Error} {Min L2 Error}\n";

    const double startPCA = Parameter::getValue<double>("test.startPCA");
    const double endPCA = Parameter::getValue<double>("test.endPCA");
    const double pcaStepSize = Parameter::getValue<double>("test.pcaStepSize");
    for (double pcaPercentage=startPCA;
         pcaPercentage<=endPCA;
         pcaPercentage+=pcaStepSize)
    {

      std::vector<double> l2errors;
      std::vector<double> h1errors;

      Parameter::replaceKey("pca.percentage", pcaPercentage);

      // now, get a reduced space
      RBSpace rbSpace(discFuncSpace);

      // RB generator
      RBGeneration rbGenerator(rbSpace);

      // add a initial base function to the space
      int rbinitTime = FemTimer::addTo("Initialization of RB");
      FemTimer::start(rbinitTime);
      rbGenerator.init();
      FemTimer::stop(rbinitTime);

      // extend the reduced basis to given size
      const int extensionAlgorithm = Parameter::getValue<int>("rbgeneration.algorithm");
      int rbextensionTime = FemTimer::addTo("Extension of RB");
      FemTimer::start(rbextensionTime);
      if (extensionAlgorithm==0)
        rbGenerator.randomExtension(Parameter::getValue<int>("rb.size"));
      else if (extensionAlgorithm==1)
        rbGenerator.uniformExtension(Parameter::getValue<int>("rb.size"));
      else
        DUNE_THROW(RangeError, "Desired extension algorithm does not exist!");
      FemTimer::stop(rbextensionTime);


      // if (Parameter::getValue<bool>("rb.plotBaseFunctions")) {
      //   ConstrainedDF baseFunction("baseFunction", discFuncSpace);
      //   FunctionPlotter<ConstrainedDF> plotter(baseFunction);
      //   for (int i=0; i!=4; ++i) {
      //     const RBSpace::ConstrainedList &list = rbSpace.getFuncList(i);
      //     for (unsigned int j=0; j!=list.size(); ++j) {
      //       list.getFunc(j, baseFunction);
      //       plotter.plot();
      //     }
      //   }
      // }

      // get a reduced operator
      int assemblyTime = FemTimer::addTo("Assemblation of reduced matrix");
      FemTimer::start(assemblyTime);
      LinearOperatorType rbOp(rbSpace, *problem);
      //      std::cout << "Starting assemblation of reduced matrix!\n";
      rbOp.assemble();
      //std::cout << "\nAssemblation done!\n";
      FemTimer::stop(assemblyTime);
      if (Parameter::getValue<bool>("rb.verbose"))
        rbOp.print(std::cout);


      const unsigned int numParams = problem->Nlambda();
      const unsigned int testSize = Parameter::getValue<int>("test.testSize");
      for (unsigned int testParam=0; testParam!=testSize; ++testParam)
      {
        GrapeDataDisplay<GridType> grapeDisp(grid);

        if (testSize==1) {
          std::vector<double> simParams;
          Parameter::get("simulation.parameters", simParams);
          std::cout << "Set parameters for simulation: mu=[";
          for (unsigned int i=0; i!=simParams.size(); ++i)
            std::cout << simParams[i] << " ";
          std::cout << "]\n";
          for (unsigned int param = 0; param!=numParams; ++param) {
            std::stringstream paramName;
            paramName << "test.param" << param;
            Parameter::replaceKey(paramName.str(), simParams[param]);
          }
        }
        else {
          std::cout << "Set parameters for simulation: mu=[";
          for (unsigned int param = 0; param!=numParams; ++param) {
            const MuRangeType& paramRange = mu_ranges[param];
            double span = paramRange.to - paramRange.from;
            const double paramValue = 
              static_cast<double>(rand())*span/static_cast<double>(RAND_MAX)+paramRange.from;
            std::stringstream paramName;
            paramName << "test.param" << param;
            Parameter::replaceKey(paramName.str(), paramValue);
            std::cout << paramValue << " ";
          }
          std::cout << "]\n";
        }


        // create inverse operator
        const double dummy = 12345.67890;
        double solverEps = 1e-8;
        const bool solverVerbose = Parameter::getValue<bool>("fem.solver.verbose");
        InverseOperatorType cg(rbOp, dummy, solverEps, std::numeric_limits<int>::max(), solverVerbose);

        // solve the reduced system
        ReducedFunction solution("solution", rbSpace);
        solution.clear();
        int reducedSolutionTime = FemTimer::addTo("Reduced Solution");
        //        std::cout << "Starting reduced simulation!\n";
        FemTimer::start(reducedSolutionTime);
        cg(rbOp.rightHandSide(), solution);
        FemTimer::stop(reducedSolutionTime);

        //solution.print(std::cout);

        // reconstruct the reduced solution + visualize
        int reconstructionTime = FemTimer::addTo("Reconstruction");
        std::stringstream redSolName;
        redSolName << "reduced_" << pcaPercentage << "_baseFuncs";
        ConstrainedDF recons(redSolName.str(), discFuncSpace, mdGridPart, -1);
        FemTimer::start(reconstructionTime);
        rbSpace.project(solution, recons);
        FemTimer::stop(reconstructionTime);
        grapeDisp.addData(recons);

        // compute the matching high dim solution + visualize
        std::stringstream highDimName;
        highDimName << "HighDimSol_" << pcaPercentage << "_baseFuncs";
        DiscreteFunction highDimSolution(highDimName.str(), discFuncSpace);
        rbGenerator.detailedSimulation(highDimSolution);
        grapeDisp.addData(highDimSolution);

        // compute the difference function and plot it
        DiscreteFunction difference(highDimSolution);
        difference -=recons;
        grapeDisp.addData(difference, "difference", 0.0);

        if (Parameter::getValue<bool>("fem.io.grapedisplay"))
          grapeDisp.display();


        // compute errors
        // create L2 - Norm 
        Dune::L2Norm<MDGridPart> l2norm(mdGridPart);
        // calculate L2 - Norm 
        const double l2error = l2norm.distance( highDimSolution, recons );      /*@LST0E@*/
        // create H1 - Norm 
        Dune::H1Norm<MDGridPart> h1norm(mdGridPart);
        // calculate H1 - Norm 
        const double h1error = h1norm.distance( highDimSolution, recons );

        l2errors.push_back(l2error);
        h1errors.push_back(h1error);
        //        FemTimer::print(std::cout);
      } // loop over all test mus
      file << pcaPercentage << " " << mean(l2errors) << " " << mean(h1errors)
           << " " << stddeviation(l2errors) << " " << stddeviation(h1errors)
           << " " << *std::max_element(l2errors.begin(), l2errors.end())
           << " " << *std::min_element(l2errors.begin(), l2errors.end())
           << std::endl;

    } // loop over pca percentages
    file.close();
  }
  catch (Dune::Exception e)
  {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}
