#include <config.h>

#include "../pca.hh"

// grid stuff
#include <dune/grid/io/file/dgfparser/dgfgridtype.hh>
#include <dune/grid/multidomaingrid.hh>

// fem stuff
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/lagrangespace.hh>
#include <dune/fem/space/fvspace.hh>
#include <dune/fem/function/adaptivefunction.hh>

// this included in pca->gramianpipeline->wrapper already, but maybe 
// we should kick that out
#include <dune/rb/misc/discfunclist/discfunclist_xdr.hh>
#include <dune/rb/misc/discfunclist/constraineddiscfunclist.hh>
#include <dune/rb/rbasis/twophaseflow/function/constraineddiscfunc.hh>


using namespace Dune;
using namespace RB;

int main(int argc, char *argv[])
{
  MPIManager::initialize(argc, argv);
  Parameter::append("pca.params");
  typedef Dune::mdgrid::FewSubDomainsTraits<GridSelector::GridType::dimension,4>   MDGridTraits;
  typedef MultiDomainGrid<GridSelector::GridType, MDGridTraits>                    MDGridType;
  typedef LeafGridPart<MDGridType>                                                 GridPartType;
  typedef FunctionSpace<double, double, 2, 1>                                      FunctionSpaceType;
  typedef Dune::LagrangeDiscreteFunctionSpace
    <FunctionSpaceType, GridPartType, 1, Dune::CachingStorage>                     DiscreteSpaceType;
  typedef AdaptiveDiscreteFunction<DiscreteSpaceType>                              DiscreteFunctionType;
  typedef ConstrainedDiscreteFunction<DiscreteFunctionType, MDGridType, int>       ConstrainedDF;
  typedef ConstrainedDiscreteFunctionList<ConstrainedDF, DiscreteFunctionList_xdr> ConstrainedDFListType;
  typedef ConstrainedDF::DofIteratorType                                           DofIterator;
  typedef DiscreteSpaceType::IteratorType                                          IteratorType; 
  typedef DiscreteSpaceType::EntityType                                            EntityType;
 
  // get a two and a one function
  GridPtr<GridSelector::GridType> gridPtr("unitcube2.dgf");
  GridSelector::GridType &grid = *gridPtr;
  MDGridType mdGrid(grid);
  GridPartType gridPart(mdGrid);
  DiscreteSpaceType discFuncSpace(gridPart);

  // define subdomains
  mdGrid.startSubDomainMarking();
  for (IteratorType it = gridPart.begin<0>(); it != gridPart.end<0>(); ++it) {
    const EntityType& e = *it;
    typedef Dune::FieldVector<MDGridType::ctype,2> Vec;
    Vec local = Dune::GenericReferenceElements<MDGridType::ctype,2>::general(e.type()).position(0,0);
    Vec c = e.geometry().global(local);
    double x = c[0];
    double y = c[1];
    if (x<0.25) {
      if (y<0.5)
        mdGrid.addToSubDomain(0,e);
      else
        mdGrid.addToSubDomain(2,e);
    }
    else {
      if (x<0.6)
        mdGrid.addToSubDomain(1,e);
      else
        mdGrid.addToSubDomain(3,e);
    }
  }
  mdGrid.preUpdateSubDomains();
  mdGrid.updateSubDomains();
  mdGrid.postUpdateSubDomains();

  // get constrained discrete functions
  ConstrainedDF one("one", discFuncSpace, mdGrid, Parameter::getValue<int>("one.support"));
  ConstrainedDF two("two", discFuncSpace, mdGrid, Parameter::getValue<int>("two.support"));
  ConstrainedDF three("three", discFuncSpace, mdGrid, Parameter::getValue<int>("three.support"));
  ConstrainedDF four("four", discFuncSpace, mdGrid, Parameter::getValue<int>("four.support"));

  DofIterator twoIt = two.dbegin();
  DofIterator threeIt = three.dbegin();
  DofIterator fourIt = four.dbegin();
  double j=0;
  double k = 0.72;
  double t = 50.0;
  for (DofIterator dIt = one.dbegin();
       dIt!=one.dend(); ++dIt, ++twoIt, ++threeIt, ++fourIt) {
    *dIt = j;
    *twoIt = k*0.5;
    *threeIt = t*t;
    *fourIt = k*k*k;
    j+=0.1;
    k+=0.63*j;
    t-=0.5;
  }

  double mean1 = std::accumulate(one.dbegin(), one.dend(), 0.0);
  double mean2 = std::accumulate(two.dbegin(), two.dend(), 0.0);
  double mean3 = std::accumulate(three.dbegin(), three.dend(), 0.0);
  double mean4 = std::accumulate(four.dbegin(), four.dend(), 0.0);
  mean1/=one.size();
  mean2/=two.size();
  mean3/=three.size();
  mean4/=four.size();
  one/=mean1;
  two/=mean2;
  three/=mean3;
  four/=mean4;

  ConstrainedDFListType list(discFuncSpace, mdGrid);

  list.push_back(one);
  list.push_back(two);
  list.push_back(three);
  list.push_back(four);

  // compute the principal components!
  ConstrainedDFListType principalComponents(discFuncSpace, mdGrid);
  pca(list, principalComponents);
  if (Parameter::verbose()) {
    list.print(std::cout);
    principalComponents.print(std::cout);
  }

  return 0;
}
