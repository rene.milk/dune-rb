template< class DiscreteFunctionListType>
void pca(DiscreteFunctionListType &U,
         DiscreteFunctionListType &princComp,
         const double desiredPercentage=0.9)
{
  typedef DiscreteFunctionListImp                                    DiscreteFunctionListType;
  // Wrap U to run PCA only on a subset
  unsigned int subSetStepSize = 
    Parameter::getValue<unsigned int>("pca.subsetwrapperstepsize", 1);
  typedef DiscFuncListSubSetWrapper<DiscreteFunctionListType> SubSetWrapper;
  SubSetWrapper subSetWrapper(U, subSetStepSize);
  assert(subSetWrapper.size()>0);

  //just to be sure that oldSpace is orthonormal
/*  if (oldSpace.numBaseFunctions()!=0)
 *    theInstance().gram_schmidt(oldSpace.getFuncList());*/

  //orthonormalize subSetWrapper with respect to oldSpace()
  project(subSetWrapper, oldSpace.getFuncList());

  unsigned int uSize = subSetWrapper.size();

  DiscreteFunctionType buffer1("buffer1", *(discFuncSpace_));

  typedef GramianPipeline< SubSetWrapper, CMatrixWrapper >  GramianPipelineType;
  typedef typename GramianPipelineType :: OpHandle          OpHandle;
  //! \todo change this to reasonable value
  const unsigned int blockSize = Parameter::getValue<int>("gramianpipeline.blocksize");
  GramianPipelineType pipeline(subSetWrapper, blockSize);

  OpHandle hIdOp = pipeline.getIdentityHandle();

  //Compute Gram-Matrix and save it in vec
  unsigned int gramMatrixSize = uSize * uSize;
  double * gramMatrix = new double[gramMatrixSize];
  CMatrixWrapper gramMatrixWrapper(&gramMatrix[0], uSize, uSize);
  pipeline.addGramMatrixComputation(hIdOp, hIdOp, gramMatrixWrapper);
  pipeline.run();

  //save some matrix entries, will be needed later
  std::vector<double> stdDeviation;
  for (unsigned int i=0; i!=uSize; ++i) {
    assert(gramMatrix[i*uSize+i]>0);
    stdDeviation.push_back(sqrt(gramMatrix[i*uSize+i]));
  }


#if defined(HAVE_LAPACK) && HAVE_LAPACK
  double eigenvalues[uSize];
  //calculate eigenvalues

  /*  lapack_ptrdiff_t is set in clapack.c to the correct argument type by the
   *  used lapack implementation (either distributed lapack or Matlab's
   *  lapack)*/

  /*size for the workspace used by dsyev, set to
    -1 first so dsyev will guess the right size*/
  typedef Lapack :: lapack_ptrdiff_t ptrdiffType;
  ptrdiffType lwork = -1;
  ptrdiffType n = uSize; //order of the gram matrix
  char jobz = 'v'; //calculate eigenvalues and eigenvectors
  char uplo = 'l'; //use upper triangular matrix

  //allocate memory for lapack, first with size 1
  double* workHere = new double[1];
  //return value for lapack
  ptrdiffType info; //info = 0 means everthing worked out

  //call Lapack
  Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
                   &eigenvalues[0], &workHere[0], &lwork, &info);

  /*after calling dsyev with lwork=-1 workHere[0] contains the
    right size for workHere*/
  lwork = static_cast<ptrdiffType>(workHere[0]);
  assert(lwork>0);
  delete workHere;
  workHere = new double[lwork];
  //finally call lapack and compute the eigenvalues
  Lapack :: dsyev_(&jobz, &uplo, &n, &gramMatrix[0], &n,
                   &eigenvalues[0], &workHere[0], &lwork, &info);
  if (info == 0) {
    for (int l=0; l!=k; ++l) {
      buffer1.clear();
      typedef typename DiscreteFunctionType :: DofIteratorType       DofIterator;
      typedef typename DiscreteFunctionType :: ConstDofIteratorType  ConstDofIterator;
      //get eigenvector to (uSize-l)-largest eigenvalue

      double dof[uSize];
      /*copy from array of double to DiscreteFunction,
        remember: last vector is the vector to the largest eigenvalue!*/
      int j=0;
      for (unsigned int i=uSize*(uSize-l-1); i!=uSize*(uSize-l); ++i, ++j)
        dof[j] = gramMatrix[i];

      DiscreteFunctionType newBaseFunc
        ("newBaseFunc", *(discFuncSpace_));
      newBaseFunc.clear();
      double *eigenvecDof = dof;

      //project the eigenvector back to the span of subSetWrapper
      for (unsigned int i=0; i!=uSize; ++i, ++eigenvecDof) {
        buffer1.clear();
        subSetWrapper.getFunc(i, buffer1);
        //"normalize the data set with respect to its variance"
        *eigenvecDof/=stdDeviation[i];
        //          printf("Standart deviation is: %E \n", stdDeviation[i]);
        DofIterator newBaseFuncDof = newBaseFunc.dbegin();
        ConstDofIterator uDof = buffer1.dbegin();

        for (size_t j=0; j!=(unsigned)buffer1.size(); ++j, ++newBaseFuncDof, ++uDof) {
          *newBaseFuncDof += (*eigenvecDof)*(*uDof);

        }
      }

      // normalize the new base function
      newBaseFunc /= std::sqrt(scalar_product(newBaseFunc, newBaseFunc));
      oldSpace.addBaseFunction(newBaseFunc);
#ifdef RB_DEBUG
      printf("The function with the following dofs was successfully added to the rb space:\n");
      typedef DiscreteFunctionType :: ConstDofIteratorType       ConstDofIterator;
      ConstDofIterator end = newBaseFunc.dend();
      for (ConstDofIterator out = newBaseFunc.dbegin();
           out != end; ++ out)
        printf("%E \n", *out);
#endif //ifdef RB_DEBUG
      if (oldSpace.numBaseFunctions() > 1) {
        DiscreteFunctionType baseFunc1("baseFunc1", *(discFuncSpace_));
        DiscreteFunctionType baseFunc2("baseFunc2", *(discFuncSpace_));
        oldSpace.baseFunction(0, baseFunc1);
        oldSpace.baseFunction(1, baseFunc2);
        std :: ostringstream oss;
        oss << "scalar product: " <<  scalar_product(baseFunc1, newBaseFunc)
            << " numBaseFuncs " << oldSpace.numBaseFunctions()
            << " ( scalar product between base function 0 and 1 after PCA )" << std::endl;
        oss << "scalar product: " <<  scalar_product(newBaseFunc, newBaseFunc)
            << " ( scalar product of new base function with itself after PCA )" << std::endl;
        ServerType :: printStatusMsg( oss.str() );
      }
    }
  } // if (info == 0)
  else if (info<0) {
    std::stringstream errormsg;
    errormsg << "Argument " << info << " of gram matrix had illegal value!";
    ServerType :: printErrMsg( errormsg.str() );
  }
  else if (info>0) {
    std::stringstream errormsg;
    ServerType :: printErrMsg("Eigenvalue algorithm failed to converge!");
  }
#else // if HAVE_LAPACK is not defined

//#error "You need Lapack and the macro \"HAVE_LAPACK\" defined to use this!"

#endif //ifdef LAPACK

  delete gramMatrix;

} //pca_fixspace
