#ifndef __RESTRICTFUNCTION_HH__
#define __RESTRICTFUNCTION_HH__

#include <set>
#include <cassert>

namespace Dune
{
namespace RBFem
{
namespace Lib
{
namespace Function
{

template< class ConstrainedDFImpl >
void restrictFunction (const ConstrainedDFImpl &p, ConstrainedDFImpl &ret)
{

  typedef ConstrainedDFImpl ConstrainedDF;
  typedef typename ConstrainedDF::DiscreteFunctionSpaceType DiscreteSpace;
  typedef typename ConstrainedDF::DofIteratorType DofIterator;
  typedef typename DiscreteSpace::GridPartType GridPartType;
  typedef typename ConstrainedDF::LocalFunctionType LocalFunctionType;
  typedef typename DiscreteSpace::IteratorType IteratorType;
  typedef typename ConstrainedDF::MacroElement MacroElement;
  typedef typename GridPartType::GridType::template Codim< 0 >::Entity Entity;
  typedef typename GridPartType::GridType::Traits::template Codim< 0 >::EntityPointer EntityPointer;
  typedef typename GridPartType::IntersectionIteratorType IntIteratorType;
  typedef typename IntIteratorType::Intersection IntersectionType;

  MacroElement support = p.getSupport();
  const DiscreteSpace& discFuncSpace = p.space();
  const GridPartType& gridPart = discFuncSpace.gridPart();

  ret.clear();

  // count all dofs that have been handled, so they are not written twice
  std::set< int > globalDofsDone;

  IteratorType gEnd = gridPart.template end< 0 >();
  for (IteratorType it = gridPart.template begin< 0 >(); it != gEnd; ++it)
  {
    const Entity &en = *it;
    // if given entity is in the support-macro element of p
    if (gridPart.subDomainContains( support, en ))
    {
      // get local functions for p and ret
      LocalFunctionType pLocal = p.localFunction( en );
      LocalFunctionType retLocal = ret.localFunction( en );

      int numDofs = pLocal.numDofs();
      assert( numDofs==retLocal.numDofs() );

      for (int i = 0; i != numDofs; ++i)
        retLocal[i] = pLocal[i];

    } // if p has support in this element
  } // end of grid run
}

template< class ConstrainedDFImpl >
class FunctionRescaler
{

  typedef ConstrainedDFImpl ConstrainedDF;
  typedef typename ConstrainedDF::DiscreteFunctionSpaceType DiscreteSpace;
  typedef typename ConstrainedDF::DofIteratorType DofIterator;
  typedef typename DiscreteSpace::GridPartType GridPartType;
  typedef typename ConstrainedDF::LocalFunctionType LocalFunctionType;
  typedef typename DiscreteSpace::IteratorType IteratorType;
  typedef typename ConstrainedDF::MacroElement MacroElement;
  typedef typename GridPartType::GridType::template Codim< 0 >::Entity Entity;
  typedef typename GridPartType::GridType::Traits::template Codim< 0 >::EntityPointer EntityPointer;
  typedef typename GridPartType::IntersectionIteratorType IntIteratorType;
  typedef typename IntIteratorType::Intersection IntersectionType;

public:

  FunctionRescaler ()
      : initialized_( false )
  {
  }
  ;

  void rescaleFunctionOnIntersection (ConstrainedDFImpl &p)
  {
    if (!initialized_) init( p );

    for (std::map< int, int >::const_iterator it = globalDofs_.begin();
        it != globalDofs_.end(); ++it)
    {
      const size_t dofIndex = it->first;
      if (it->second <= 4)
        p.leakPointer()[dofIndex] *= 0.5;
      else
        p.leakPointer()[dofIndex] *= 0.25;
    }
  }

private:
  void init (const ConstrainedDF& p)
  {
    initialized_ = true;

    const DiscreteSpace& discFuncSpace = p.space();
    const GridPartType& gridPart = discFuncSpace.gridPart();

    IteratorType gEnd = gridPart.template end< 0 >();
    for (IteratorType it = gridPart.template begin< 0 >(); it != gEnd; ++it)
    {
      const Entity &en = *it;
      // get local function for p
      LocalFunctionType pLocal = p.localFunction( en );

      int numDofs = pLocal.numDofs();

      /*check, if one of the current dofs belongs to an
       intersection that is shared by different coarse cells.
       If that is the case, we assign only the half value.*/
      IntIteratorType iend = gridPart.iend( en );
      for (IntIteratorType iit = gridPart.ibegin( en ); iit != iend; ++iit)
      {
        const IntersectionType &intersection = *iit;
        if (gridPart.onCoarseCellIntersection( intersection ))
        {
          EntityPointer outsidePtr = intersection.outside();
          const Entity& outsideEn = *outsidePtr;
          LocalFunctionType outsideLF = p.localFunction( outsideEn );

          std::set< int > outsideGlobalDofs;
          for (int i = 0; i < outsideLF.numDofs(); ++i)
          {
            outsideGlobalDofs.insert(
                discFuncSpace.mapToGlobal( outsideEn, i ) );
          }
          for (int i = 0; i < numDofs; ++i)
          {
            const unsigned int globalDof = discFuncSpace.mapToGlobal( en, i );
            std::set< int >::iterator setIt = outsideGlobalDofs.find(
                globalDof );
            if (setIt != outsideGlobalDofs.end())
            {
              std::map< int, int >::iterator globalDofsIt = globalDofs_.find(
                  globalDof );
              if (globalDofsIt == globalDofs_.end())
                globalDofs_[globalDof] = 1;
              else
                ++(globalDofsIt->second);
            }
          }
        }
      }
    } // end of grid run
  } // void init

  std::map< int, int > globalDofs_;
  bool initialized_;
};
} // namespace Function
} // namespace Lib
} // namespace RBFem
} // namespace Dune

#endif /* __RESTRICTFUNCTION_HH__ */

