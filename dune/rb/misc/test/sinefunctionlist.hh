#ifndef SINEFUNCTIONLIST_E68N97GP
#define SINEFUNCTIONLIST_E68N97GP

#include <dune/fem/function/common/function.hh>
#include "../linearalgebra.hh"

namespace Dune
{
namespace RBFem {
namespace Test {


template< class FunctionSpace >
class SineBaseFunction
: public Dune::Fem::Function< FunctionSpace, SineBaseFunction< FunctionSpace > >
{
  typedef SineBaseFunction< FunctionSpace > ThisType;
  typedef Dune::Fem::Function< FunctionSpace, ThisType > BaseType;

public:
  typedef FunctionSpace                                              FunctionSpaceType;

  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  typedef typename FunctionSpaceType :: RangeType                    RangeType;

  typedef typename FunctionSpaceType :: DomainFieldType              DomainFieldType;
  typedef typename FunctionSpaceType :: RangeFieldType               RangeFieldType;

  static const int dimDomain = FunctionSpaceType::dimDomain;
  static const int dimRange = FunctionSpaceType::dimRange;

  typedef FieldVector< double, dimDomain >                           CoefficientType;

  explicit SineBaseFunction ( const CoefficientType coefficient )
  : coefficient_( coefficient )
  {}

  void evaluate ( const DomainType &x, RangeType &y ) const
  {
    y = 1;
    for( int i = 0; i < dimDomain; ++i )
    {
      /*
      if( coefficient_[ i ] < 0 )
        y *= sqrt( 2 ) * cos( 2 * M_PI * coefficient_[ i ] * x[ i ] );
      else if( coefficient_[ i ] > 0 )
        y *= sqrt( 2 ) * sin( 2 * M_PI * coefficient_[ i ] * x[ i ] );
      */
      y *= std::fabs(sqrt( 2 ) * sin( 2* M_PI * coefficient_[ i ] * x[ i ] ));
    }
  }

  void evaluate ( const DomainType &x, const RangeFieldType t, RangeType &y ) const
  {
    evaluate( x, y );
  }

protected:
  const CoefficientType coefficient_;
};


template< class DiscreteFunctionListImp >
class SineDiscreteFunctionList
: public DiscreteFunctionListImp
{
  typedef SineDiscreteFunctionList< DiscreteFunctionListImp >        ThisType;
  typedef DiscreteFunctionListImp                                    DiscreteFunctionListType;
  typedef DiscreteFunctionListType                                   BaseType;
public:
  typedef typename DiscreteFunctionListType :: DiscreteFunctionType  DiscreteFunctionType;
  typedef typename DiscreteFunctionType
            :: DiscreteFunctionSpaceType                             DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType    FunctionSpaceType;

public:
  typedef SineBaseFunction< FunctionSpaceType >                      ContinuousFunctionType;

private:
  typedef typename ContinuousFunctionType :: CoefficientType         CoefficientType;


public:
  explicit SineDiscreteFunctionList ( DiscreteFunctionSpaceType &space,
                                      double offset = 0.5,
                                      double maxCoefficient = 5) :
    BaseType( space ),
    maxCoefficient_(maxCoefficient),
    coefficientStep_(offset)
  {
    // CoefficientType coefficient( -maxCoefficient );
    CoefficientType coefficient( 1 );
    while( true )
    {
      addFunction( coefficient );

      coefficient[ 0 ]+=1;
      for( unsigned int i = 0; coefficient[ i ] > maxCoefficient; ++i )
      {
        // coefficient[ i ] = -maxCoefficient;
        coefficient[ i ] = 1;
        if( i+1 < CoefficientType :: dimension )//std::min((unsigned int)(CoefficientType :: dimension), 2) )
          coefficient[ i+1 ] += 1;
        else
        {
          std::cout << "assembled sine function list..." << std::endl;
          return;
        }
      }
    }
  }

private:
/*  static int abs( const CoefficientType &coefficient )
 *  {
 *    int value = 0;
 *    for( unsigned int i = 0; i < CoefficientType :: dimension; ++i )
 *      value += (coefficient[ i ] < 0 ? -coefficient[ i ] : coefficient[ i ]);
 *    return value;
 *  }*/

  void addFunction( const CoefficientType &coefficient )
  {
    DiscreteFunctionType discreteFunction( "base function", this->space() );
    ContinuousFunctionType continuousFunction( coefficient );

    Interpolation :: interpolate(continuousFunction, discreteFunction);

    BaseType::push_back( discreteFunction );
  }


private:
  double maxCoefficient_;
  double coefficientStep_;
};

  } // namespace Test
  } // end of namespace RBFem
}   // end of namespace Dune

#endif /* end of include guard: SINEFUNCTIONLIST_E68N97GP */
