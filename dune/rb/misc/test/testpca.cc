#define BOOST_TEST_MODULE PCATest
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <config.h>
#include "../../misc/tuples.hh"

#include "../../misc/discfunclist/discfunclist_mem.hh"

#include <dune/grid/yaspgrid.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace/lagrangespace.hh>
#include <dune/fem/space/fvspace/fvspace.hh>
#include <dune/fem/function/adaptivefunction/adaptivefunction.hh>

#include "../../common/test/utility.hh"
#include "../linearalgebra.hh"

#include "sinefunctionlist.hh"

#include <cmath>

namespace Dune {
  namespace RB {

template<unsigned int gridDim>
struct FVDFLTraits
{
  typedef Dune :: YaspGrid< gridDim >                                YaspGridType;
  typedef LeafGridPart< YaspGridType >                               GridPartType;

  typedef FunctionSpace< double, double, gridDim, 1 >                FunctionSpaceType;
  typedef FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 >    DiscreteFunctionSpaceType;
  typedef AdaptiveDiscreteFunction< DiscreteFunctionSpaceType >      DiscreteFunctionType;
};

template<unsigned int gridDim>
struct P1DFLTraits
{
  typedef Dune :: YaspGrid< gridDim >                                YaspGridType;
  typedef LeafGridPart< YaspGridType >                               GridPartType;

  typedef FunctionSpace< double, double, gridDim, 1 >                FunctionSpaceType;
  typedef LagrangeDiscreteFunctionSpace< FunctionSpaceType,
                                         GridPartType, 1 >           DiscreteFunctionSpaceType;
  typedef AdaptiveDiscreteFunction< DiscreteFunctionSpaceType >      DiscreteFunctionType;
};
template< template<unsigned int>class TraitsImp,
          template <class> class DiscreteFunctionListImp,
          unsigned int gridDim,
          unsigned int listSize,
          unsigned int funcSize >
class PCATest
{
  typedef TraitsImp< gridDim >                DFLTraitsType;
  typedef typename DFLTraitsType :: YaspGridType                     YaspGridType;
  typedef typename DFLTraitsType :: GridPartType                     GridPartType;
  typedef typename DFLTraitsType :: DiscreteFunctionSpaceType        DiscreteFunctionSpaceType;
  typedef typename DFLTraitsType :: DiscreteFunctionType             DiscreteFunctionType;
  typedef typename DFLTraitsType :: FunctionSpaceType :: DomainType  DomainType;
  typedef FieldVector< int, gridDim >                                GridSizeType;
  typedef FieldVector< bool, gridDim >                               PeriodicType;

  typedef DiscreteFunctionListImp< DFLTraitsType >                   DiscreteFunctionListType;
  typedef SineDiscreteFunctionList< DiscreteFunctionListType >       SineListType;
  typedef LinAlg< DefaultRBServer, DiscreteFunctionSpaceType >         LinAlgType;

private:

  struct MaxOfTuple
  {
    MaxOfTuple(double * max) : max_(max) {};

    void operator() (double d)
    {
      *max_ = std::max(d, *max_);
    }

    template<class Tuple>
    static double apply(const Tuple & tup)
    {
      double max=std::numeric_limits<double>::min();
      struct MaxOfTuple mot(&max);
      std::for_each(tup.begin(), tup.end(), mot);
      return max;
    }

  private:
    double * max_;
  };

public:

  PCATest()
    : dummy(InitHelper :: one("test.params")),
      yaspgrid_(DomainType(1), GridSizeType(funcSize), PeriodicType(false), 0),
      gridPart_(yaspgrid_),
      space_(gridPart_),
      sl_(space_, 0.3, listSize ),
      la_(space_)
  {
  }

  ~PCATest()
  {
    sl_.clear();
  }

  double projectionTest(DiscreteFunctionListType & pc)
  {
    DiscreteFunctionList_mem<DFLTraitsType> slCopy(space_);
    slCopy.push_back_list(sl_);

    la_.gram_schmidt(pc);

    la_.project(slCopy, pc);

    DiscreteFunctionType snapCopy("snapshot_copy", space_);
    DiscreteFunctionType snap("snapshot", space_);
    std::vector<double> l2norms;
    for (unsigned int i = 0; i < slCopy.size(); i++)
    {
      snapCopy.clear();
      snap.clear();
      slCopy.getFunc(i, snapCopy);
      sl_.getFunc(i, snap);

      snap -= snapCopy;

      double norm = la_.l2norm(snap);
      l2norms.push_back(norm);
    }
    return MaxOfTuple :: apply(l2norms);
  }

  double runTest(double percentage)
  {
    DiscreteFunctionListType pc(space_, "pcs");
    la_.pca(sl_, pc, -1, percentage);

    std::cout << "    number of principal components: " << pc.size() << "\n";

    double maxError = projectionTest(pc);

    pc.clear();

    return maxError;
  }

  double runTest(unsigned int k)
  {
    DiscreteFunctionListType pc(space_, "pcs");
    la_.pca(sl_, pc, k);

    BOOST_CHECK_EQUAL(pc.size(), k);

    double maxError = projectionTest(pc);

    pc.clear();

    return maxError;
  }

  void runTest()
  {
    std::cout << "Running PCATest for\n"
              << " DiscreteFunctionListType, : " << typeid(sl_).name() << "\n"
              << " Yaspgrid of dimension     : " << gridDim << "\n"
              << "  and number of cells,     : " << std::pow(funcSize, gridDim) << "\n"
              << " and list size             : " << sl_.size() << "\n\n";

    {
      const unsigned int sls = sl_.size();
      double errors[5];
      for (unsigned int i = 0; i < 5; i++)
      {
        unsigned int siz = std::ceil((i+1) * sls/5.0001);

        std::cout << "  Generate PCA with " << siz << " components:\n";
        errors[i] = runTest(siz);
        std::cout << "   maximum projection error: " << errors[i] << "\n";
        if (i > 0)
          BOOST_CHECK(errors[i] < errors[i-1]);
      }

      BOOST_CHECK_SMALL(errors[4], 1e-12);
    }

    {
      double errors[5];
      int i = 0;
      for (double ratio = 0.8; ratio <= 1.001; ratio+=0.05, i++)
      {
        std::cout << "  Generate PCA with total variation of " << ratio << ":\n";
        errors[i] = runTest(ratio);
        std::cout << "   maximum projection error: " << errors[i] << "\n";
        if (i > 0)
        {
          BOOST_CHECK(errors[i] <= errors[i-1]);
        }
      }

      BOOST_CHECK_SMALL(errors[4], 1e-12);
    }
  }

private:
  int                        dummy;
  YaspGridType               yaspgrid_;
  GridPartType               gridPart_;
  DiscreteFunctionSpaceType  space_;
  SineListType               sl_;
  LinAlgType                 la_;
};

  } // end of namespace Dune :: RB
}   // end of namespace Dune


BOOST_AUTO_TEST_SUITE( PCATest )

/*int main(int argc, const char *argv[])
 *{
 *  PCATest1 gmt;

 *|+  gmt.projectionTest();+|
 *  gmt.runTest();
 *  return 0;
 *}*/

typedef Dune :: RB :: PCATest< Dune :: RB :: FVDFLTraits,
                               Dune :: RB :: DiscreteFunctionList_xdr,
                               1, 32, 100 >                         PCATest1;
BOOST_FIXTURE_TEST_CASE( pca_1, PCATest1 )
{
  runTest();
}
typedef Dune :: RB :: PCATest< Dune :: RB :: FVDFLTraits,
                               Dune :: RB :: DiscreteFunctionList_xdr,
                               2, 8, 25 >                            PCATest2;
BOOST_FIXTURE_TEST_CASE( pca_2, PCATest2 )
{
  runTest();
}
typedef Dune :: RB :: PCATest< Dune :: RB :: P1DFLTraits,
                               Dune :: RB :: DiscreteFunctionList_xdr,
                               2, 8, 25 >                            PCATest3;
BOOST_FIXTURE_TEST_CASE( pca_3, PCATest3 )
{
  runTest();
}
BOOST_AUTO_TEST_SUITE_END( )
