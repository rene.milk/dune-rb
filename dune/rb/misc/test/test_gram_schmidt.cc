#define BOOST_TEST_MODULE GramSchmidtTest
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <config.h>
#include "../../misc/tuples.hh"

#include <dune/grid/yaspgrid.hh>
#include <dune/fem/gridpart/gridpart.hh>
#include <dune/fem/space/lagrangespace/lagrangespace.hh>
#include <dune/fem/space/fvspace/fvspace.hh>
#include <dune/fem/function/adaptivefunction/adaptivefunction.hh>

#include "../../common/test/utility.hh"
#include "../linearalgebra.hh"
#include "../../rbasis/gramian/gramianpipeline.hh"

#include "sinefunctionlist.hh"

#include <cmath>

namespace Dune {
  namespace RB {

template<unsigned int gridDim>
struct DFLTraits
{
  typedef Dune :: YaspGrid< gridDim >                                YaspGridType;
  typedef LeafGridPart< YaspGridType >                               GridPartType;

  typedef FunctionSpace< double, double, gridDim, 1 >                FunctionSpaceType;
  typedef LagrangeDiscreteFunctionSpace< FunctionSpaceType,
                                         GridPartType, 1 >           DiscreteFunctionSpaceType;
/*  typedef FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 >    DiscreteFunctionSpaceType;*/
  typedef AdaptiveDiscreteFunction< DiscreteFunctionSpaceType >      DiscreteFunctionType;
};

template< template <class> class DiscreteFunctionListImp,
          unsigned int gridDim,
          unsigned int listSize,
          unsigned int funcSize >
class GramSchmidtTest
{
  typedef DFLTraits< gridDim >                                       DFLTraitsType;
  typedef typename DFLTraitsType :: YaspGridType                     YaspGridType;
  typedef typename DFLTraitsType :: GridPartType                     GridPartType;
  typedef typename DFLTraitsType :: DiscreteFunctionSpaceType        DiscreteFunctionSpaceType;
  typedef typename DFLTraitsType :: DiscreteFunctionType             DiscreteFunctionType;
  typedef typename DFLTraitsType :: FunctionSpaceType :: DomainType  DomainType;
  typedef FieldVector< int, gridDim >                                GridSizeType;
  typedef FieldVector< bool, gridDim >                               PeriodicType;

  typedef DiscreteFunctionListImp< DFLTraitsType >                   DiscreteFunctionListType;
  typedef SineDiscreteFunctionList< DiscreteFunctionListType >       SineListType;
  typedef LinAlg< DefaultRBServer, DiscreteFunctionSpaceType >         LinAlgType;
public:

  GramSchmidtTest()
    : dummy(InitHelper :: one("test.params")),
      yaspgrid_(DomainType(1), GridSizeType(funcSize), PeriodicType(false), 0),
      gridPart_(yaspgrid_),
      space_(gridPart_),
      sl_(space_, 0.3, listSize ),
      la_(space_)
  {
  }

  ~GramSchmidtTest()
  {
    sl_.clear();
  }

  void runTest()
  {
    std::cout << "Running GramianTest for\n"
              << " DiscreteFunctionListType, : " << typeid(sl_).name() << "\n"
              << " Yaspgrid of dimension     : " << gridDim << "\n"
              << "  and number of cells,     : " << std::pow(funcSize, gridDim) << "\n"
              << " and list size             : " << sl_.size() << "\n\n";

    double * data = new double[sl_.size() * sl_.size()];
    CMatrixWrapper output(data, sl_.size(), sl_.size(), 0.0);

    la_.gram_schmidt(sl_);

    output.clear();

    BOOST_CHECK(la_.orthonormality_test(sl_, output, 5e-11));

    delete data;
  }

  void runScalarProductTest()
  {
    double * data_ref = new double[sl_.size() * sl_.size()];
    CMatrixWrapper output_ref(data_ref, sl_.size(), sl_.size(), 0.0);

/*    la_.orthonormality_test(sl_, output_ref);*/

    double * data = new double[sl_.size() * sl_.size()];
    CMatrixWrapper output(data, sl_.size(), sl_.size(), 0.0);

    DiscreteFunctionType func1("func1", space_);
    DiscreteFunctionType func2("func2", space_);

    for (unsigned int i = 0; i < sl_.size(); i++)
    {
      func1.clear();
      sl_.getFunc(i, func1);
      for (unsigned int j = 0; j < i; j++)
      {
        func2.clear();
        sl_.getFunc(j, func2);
        double sp = la_.scalar_product(func1, func2);
        output.set(i, j, sp);
        output.set(j, i, sp);
        sp = la_.scalarProduct(func1, func2);
        output_ref.set(i, j, sp);
        output_ref.set(j, i, sp);
      }
      double sp = la_.scalar_product(func1, func1);
      sp = la_.scalarProduct(func1, func1);
      output.set(i, i, sp);
      output_ref.set(i, i, sp);
    }
    output.print(std::cout);

    std::cout << "\nReference:\n";
    output_ref.print(std::cout);

    double * data_gram = new double[sl_.size() * sl_.size()];
    CMatrixWrapper output_gram(data_ref, sl_.size(), sl_.size(), 0.0);

    typedef GramianPipeline< SineListType, CMatrixWrapper >          GramianPipelineType;
    typedef typename GramianPipelineType :: OpHandle                 OpHandleType;
    GramianPipeline< SineListType, CMatrixWrapper > gp(sl_);

    OpHandleType id = gp.getIdentityHandle();
    gp.addSymmetricGramMatrixComputation(id, id, output_gram);
    gp.run();

    std::cout << "\nGram:\n";
    output_gram.print(std::cout);

    delete data_gram;
    delete data_ref;
    delete data;
  }
private:
  int                        dummy;
  YaspGridType               yaspgrid_;
  GridPartType               gridPart_;
  DiscreteFunctionSpaceType  space_;
  SineListType               sl_;
  LinAlgType                 la_;
};

  } // end of namespace Dune :: RB
}   // end of namespace Dune

/*int main(int argc, const char *argv[])
 *{
 *  GramSchmidtTest1 gmt;

 *  gmt.runTest();
 *  return 0;
 *}*/

BOOST_AUTO_TEST_SUITE( GramSchmidtTest )

typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_xdr, 1,
                                       32, 1000 >                    GramSchmidtTest1;
BOOST_FIXTURE_TEST_CASE( gram_1, GramSchmidtTest1 )
{
  runTest();
}

typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_xdr, 2,
                                       8, 50 >                      GramSchmidtTest2;
BOOST_FIXTURE_TEST_CASE( gram_2, GramSchmidtTest2 )
{
  runTest();
}

typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_xdr, 3,
                                       3, 20 >                       GramSchmidtTest3;
BOOST_FIXTURE_TEST_CASE( gram_3, GramSchmidtTest3 )
{
  runTest();
}
typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_mem, 1,
                                       32, 1000 >                    GramSchmidtTest4;
BOOST_FIXTURE_TEST_CASE( gram_4, GramSchmidtTest4 )
{
  runTest();
}

typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_mem, 2,
                                       8, 50 >                      GramSchmidtTest5;
BOOST_FIXTURE_TEST_CASE( gram_5, GramSchmidtTest5 )
{
  runTest();
}

typedef Dune :: RB :: GramSchmidtTest< Dune :: RB
                                      :: DiscreteFunctionList_mem, 3,
                                       3, 20 >                       GramSchmidtTest6;
BOOST_FIXTURE_TEST_CASE( gram_6, GramSchmidtTest6 )
{
  runTest();
}

BOOST_AUTO_TEST_SUITE_END( )
