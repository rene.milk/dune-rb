#ifndef __MXDISCFUNCWRAPPER_HH__
#define __MXDISCFUNCWRAPPER_HH__
#define MATLAB_MEX_FILE 1

#include <mex.h>
#include <matrix.h>

//! \todo DOCME!!!
class MXIterator {

public:
  MXIterator(double* &array,
	     const unsigned int index):
    array_(array),
    index_(index)
  {};

  MXIterator& operator++() {
    ++index_;
    return (*this);
  }

  bool operator==(const MXIterator &other) {
    return other.index() == index_;
  }

  bool operator!=(const MXIterator &other) {
    return other.index() != index_;
  }

  double& operator*() {
    return array_[index_];
  }

  unsigned int index() const {
    return index_;
  }

private:
  double* &array_;
  unsigned int index_;
}; // class MXIterator

//! TODO \todo docme
class MXDiscFuncWrapper {

public:

  typedef MXIterator DofIteratorType;

  MXDiscFuncWrapper(mxArray* &array,
		    const unsigned int length):
    array_(array),
    length_(length)
  {
    entries_ = mxGetPr(array_);
  };
  
  DofIteratorType dbegin() {
    return DofIteratorType(entries_, 0);
  }

  DofIteratorType dend() {
    return DofIteratorType(entries_, length_);
  }

  unsigned int size() const {
    return length_;
  }

private:
  mxArray * &array_;
  double * entries_;
  unsigned int length_;

}; //MXDiscFuncWrapper

#endif //__MXDISCFUNCWRAPPER_HH__
