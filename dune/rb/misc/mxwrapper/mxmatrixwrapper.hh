#ifndef __MXMATRIXWRAPPER_HH__
#define __MXMATRIXWRAPPER_HH__

//include matlab header file
#include <cassert>
#include "mex.h"

//! \todo DOCME!!!
class MXMatrixWrapper {

public:
  MXMatrixWrapper(mxArray* entries,
                  const unsigned int rows,
                  const unsigned int cols):
    entries_(entries),
    rows_(rows),
    cols_(cols)
  {
    //    assert(entries != NULL);
    entries_ptr_=mxGetPr(entries);
  };

  void add(const unsigned int i, const unsigned int j,
           const double &val) {
    assert(i < mxGetM(entries_) && j < mxGetN(entries_) && entries_ptr_ == mxGetPr(entries_));
    assert(i+j*rows_ < mxGetM(entries_) * mxGetN(entries_));
    entries_ptr_[i+j*rows_]+=val;
  }

  void set(const unsigned int i, const unsigned int j,
           const double &val) {
    assert(i < mxGetM(entries_) && j < mxGetN(entries_));
    assert(i+j*rows_ < mxGetM(entries_) * mxGetN(entries_));
    entries_ptr_[i+j*rows_]=val;
  }

  void get(const unsigned int i, const unsigned int j,
           double &val) const {
    assert(i < mxGetM(entries_) && j < mxGetN(entries_));
    assert(i+j*rows_ < mxGetM(entries_) * mxGetN(entries_));
    val = entries_ptr_[i+j*rows_];
  }

  unsigned int rows() const {
    return rows_;
  }

  unsigned int cols() const {
    return cols_;
  }

private:
  mxArray * entries_;
  unsigned int rows_;
  unsigned int cols_;
  double *entries_ptr_;

};

#endif //__MXMATRIXWRAPPER_HH__
