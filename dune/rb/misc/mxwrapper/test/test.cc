#include  <config.h>
#include  <cassert>
#include "mex.h"
#include "../mxmatrixwrapper.hh"
#include "../mxdiscfuncwrapper.hh"

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  mxArray *U;
  U = mxCreateDoubleMatrix(4, 4, mxREAL);
  MXMatrixWrapper matrix( U, 4, 4 );
  double val;
  matrix.set(0, 0, 0.0506);
  matrix.get(0, 0, val);
  printf("%E\n", val);

  mxArray *b;
  b = mxCreateDoubleMatrix(1,4, mxREAL);
  MXDiscFuncWrapper discFunc(b, 4);
  typedef MXDiscFuncWrapper :: DofIteratorType                      DofIteratorType;
  DofIteratorType end =  discFunc.dend();
  for (DofIteratorType it = discFunc.dbegin();
       it != end; ++it) {
    printf("%E\n", *it);
  }
}

