#include <config.h>
#include "../../misc/tuples.hh"
#include "../../matlab/rbsocks/rbserver.hh"
#include "../../matlab/facades/linevolfacade.hh"

#include "../../rbasis/linevol/linevoldescr.hh"
#include "../../rbasis/linevol/linevoldiscr.hh"
#include "../../rbasis/linevol/linevolsolve.hh"
#include "../../rbasis/linevol/linevoldefault.hh"
#include "../../rbasis/linevol/linevolempty.hh"

namespace Dune
{
namespace RB
{

typedef RBFem::Example::LinEvol::Description< double, 1, 0,
    RBFem::Example::LinEvol::Explicit, RBFem::Example::LinEvol::WONAPDE > DescrType;

struct ProblemTraits
{
  typedef RBFem::Example::LinEvol::Library< ServerTypeTraits, DescrType > DiscrType;

  typedef RBMatlabLinEvolFacade< ServerTypeTraits > FacadeType;

  typedef typename ServerTypeTraits::ServerType ServerType;
};

}
}

#include "../../matlab/duneserver/main.inc"

