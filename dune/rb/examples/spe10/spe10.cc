#include <config.h>
#include <vector>

// dune-fem includes
#include <dune/fem/gridpart/gridpart.hh>

// dune-rb includes
#include <dune/rb/grid/coarse/multidomain.hh>
#include <dune/rb/offline/mapper/coarse.hh>
#include <dune/rb/offline/space/localized.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>
#include <dune/rb/offline/generator/reducedbasis/msrbgreedy.hh>
#include <dune/rb/detailed/model/spe10.hh>
#include <dune/rb/detailed/model/thermalblock.hh>
#include <dune/rb/detailed/solver/connector/ellipticdg.hh>
#include <dune/rb/offline/localizer/default.hh>
#include <dune/rb/offline/generator/reduceddata/projected.hh>
#include <dune/rb/offline/generator/reduceddata/residual.hh>
#include <dune/rb/reduced/parameter/sample/triangularrandom.hh>

using namespace Dune;
using namespace RB;

int main(int argc, char* argv[])
{
  try {
    MPIManager::initialize(argc, argv);

    typedef GridSelector::GridType                                                    GridType;
    typedef LeafGridPart< GridType >                                                  GridPartType;
    typedef Dune::RB::Grid::Coarse::Multidomain< GridPartType >                       MDGridPartType;
    typedef Detailed::Model::Spe10< Eigen::VectorXd >                                 ModelType;
//    typedef Detailed::Model::Thermalblock< Eigen::VectorXd >                                 ModelType;
    typedef Offline::Mapper::Coarse< LA::SeparableParametric::Container::EigenDenseMatrix, MDGridPartType > CoarseMapperType;
    typedef Offline::Space::Localized< MDGridPartType, CoarseMapperType >             OfflineSpaceType;
    typedef Detailed::Solver::Connector::EllipticDG< ModelType, GridPartType >        DetailedSolverConnectorType;


    typedef Offline::Localizer::Default< GridType, MDGridPartType,
                                         DetailedSolverConnectorType::MapperType > LocalizerType;
    typedef Offline::Generator::ReducedData::Solver::Projected< ModelType, OfflineSpaceType,
                                                                DetailedSolverConnectorType, LocalizerType >
    ReducedSolverGeneratorType;
    typedef Offline::Generator::ReducedData::Estimator::Residual< ModelType,
                                                                  OfflineSpaceType,
                                                                  DetailedSolverConnectorType,
                                                                  LocalizerType > EstimatorGeneratorType;
    typedef Offline::Generator::ReducedBasis::MSRBGreedy< OfflineSpaceType, ModelType, DetailedSolverConnectorType,
                                                          ReducedSolverGeneratorType, EstimatorGeneratorType,
                                                          MDGridPartType,
                                                          LocalizerType > MSRBGreedyType;
    
    
    typedef Reduced::Parameter::Sample::TriangularRandom<ModelType::ParameterType> ParameterSampleType;
    
    Parameter::append("../spe10.params");

    GridPtr< GridType > gridPtr(Parameter::getValue< std::string >("grid.macroGridFile"));
    GridType&           grid = *gridPtr;
    GridPartType        gridPart(grid);
    MDGridPartType      mdGridPart(grid);

    SubdomainMarker sdMarker;
    sdMarker.readSubdomains();
    sdMarker.markSubdomains(mdGridPart);


    ModelType                   model;
    model.visualizePermeability(grid);
    
    DetailedSolverConnectorType disco(model, gridPart);
    OfflineSpaceType            rbSpace(mdGridPart, disco.mapper());
    LocalizerType               localizer(grid, mdGridPart, disco.mapper());
    ReducedSolverGeneratorType  resgo(model, rbSpace, disco, localizer);
    EstimatorGeneratorType      esgo(model, rbSpace, disco, localizer);
    MSRBGreedyType              greedy(rbSpace, model, disco, resgo, esgo, mdGridPart, localizer);
    
    
    // get a parameter sample
    const int paramDimensionSize = Parameter::getValue< int >("podgreedy.paramspace.size");
    ParameterSampleType parameterSample(model.minParameter(), model.maxParameter(), paramDimensionSize, 19910527);
    
    // run the msrb greedy for this parameter sample
    greedy.init(Parameter::getValue<ModelType::ParameterType>("podgreedy.initialParameter"));
    boost::uuids::uuid myUuid = greedy.generate(parameterSample);
    greedy.compressInformation();
    
    std::string datapath = "spe10_data_" + boost::lexical_cast<std::string>(myUuid);
    boost::filesystem::create_directory(datapath);
    rbSpace.save(datapath);
    resgo.generate().save(datapath);
//    esgo.generate().save(datapath);
    
  } catch (...) {
    std::cerr << "Caught an unknown exeption!\n";
    return 1;
  }


  return 0;
} // main