#ifndef DUNE_RB_EXAMPLES_UNITCUBE_HH
#define DUNE_RB_EXAMPLES_UNITCUBE_HH

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/utility/structuredgridfactory.hh>


namespace Dune
{
namespace RB
{
namespace Examples
{


// default implementation for any template parameter
template<typename T, int variant>                      /*@\label{uc:uc0}@*/
class UnitCube 
{
public:
  typedef T GridType;
  
  static const int dim = GridType::dimension;

  // constructor throwing exception
  UnitCube ()
  {
	  FieldVector<typename GridType::ctype,dim> lowerLeft(0);
      FieldVector<typename GridType::ctype,dim> upperRight(1);
      array<unsigned int,dim> elements;
      std::fill(elements.begin(), elements.end(), 1);
      
      switch (variant) {
        case 1:
        grid_ = StructuredGridFactory<GridType>::createCubeGrid(lowerLeft, upperRight, elements);
        break;
        case 2:
        grid_ = StructuredGridFactory<GridType>::createSimplexGrid(lowerLeft, upperRight, elements);
        break;
        default:  
        DUNE_THROW( NotImplemented, "Variant "
                  << variant << " of unit cube not implemented." );
        break;
    }
  }

  T& grid ()
  {
	return *grid_;
  }

private:
  // the constructed grid object
  ::Dune::shared_ptr<T> grid_;
};                                                     /*@\label{uc:uc1}@*/

}  // namespace Examples
}  // namespace RB
}  // namespace Dune

// include specializations
#include"unitcube_sgrid.hh"
#include"unitcube_yaspgrid.hh"
#include"unitcube_albertagrid.hh"
#include"unitcube_alugrid.hh"

#endif
