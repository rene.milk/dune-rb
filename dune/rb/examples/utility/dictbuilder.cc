#include <dune/rb/examples/utility/dictbuilder.hh>
#include <boost/exception/all.hpp> 

int main(int argc, char* argv[])
{
  try {
    Dune::MPIManager::initialize(argc, argv);
    Dune::Parameter::append(argc, argv);
    if (Dune::Parameter::exists("parameterFile"))
      Dune::Parameter::append(Dune::Parameter::getValue<std::string>("parameterFile"));
    Dune::RB::Utilities::DictBuilder builder;
    builder.build();
    builder.save();
  } catch (Dune::Exception& e) {
    std::cerr << e.what() << std::endl;
    return 1; 
  } catch (boost::exception& e) {
    std::cerr << boost::diagnostic_information(e) << std::endl;
    return 1;
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "Caught an unknown exeption!\n";
    return 1;
  }
  
  
  return 0;
} // main
