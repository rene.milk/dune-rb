#ifndef DUNE_RB_EXAMPLES_UNITCUBE_SGRID_HH
#define DUNE_RB_EXAMPLES_UNITCUBE_SGRID_HH

#include "unitcube.hh"

#include <dune/grid/sgrid.hh>

namespace Dune
{
namespace RB
{
namespace Examples
{

// SGrid specialization
template< int gdim >
class UnitCube< SGrid< gdim, gdim > ,1 >
{
public:
  typedef SGrid< gdim, gdim > GridType;

  SGrid< gdim, gdim >& grid ()
  {
    return grid_;
  }

private:
  SGrid< gdim, gdim > grid_;
};

}  // namespace Examples
}  // namespace RB
}  // namespace Dune

#endif
