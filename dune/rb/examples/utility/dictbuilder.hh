#ifndef __DUNE_RB_UTILITIES_DICTBUILDER_HH__
#define __DUNE_RB_UTILITIES_DICTBUILDER_HH__

#include <config.h>
#include <vector>
#include <boost/type_traits/is_same.hpp>
// dune-fem includes
#include <dune/fem/gridpart/gridpart.hh>

// dune-rb includes
#include <dune/rb/offline/generator/dictionary/default.hh>
#include <dune/rb/offline/dictionary/default.hh>
#include <dune/rb/grid/coarse/multidomain.hh>
#include <dune/rb/detailed/solver/connector/projection.hh>
#include <dune/rb/reduced/parameter/sample/triangularrandom.hh>
#include <dune/rb/detailed/model/circular_vulcano.hh>

namespace Dune {
namespace RB {
namespace Utilities {
class DictBuilder
{
  typedef GridSelector::GridType                                          GridType;
  typedef LeafGridPart< GridType >                                        GridPartType;
  typedef Dune::RB::Grid::Coarse::Multidomain< GridPartType >             MDGridPartType;
  typedef Dune::RB::Detailed::Model::CircularVulcano<Eigen::VectorXd>     ModelType;
  typedef Offline::Dictionary::Default< ModelType::ParameterType >        DictionaryType;
  typedef Detailed::Solver::Connector::Projection< ModelType, GridPartType,
                                                   DictionaryType > DetailedSolverConnectorType;
  typedef Dune::RB::Offline::Generator::Dictionary::Default<DictionaryType, ModelType, DetailedSolverConnectorType>
  DictGeneratorType;

  typedef Reduced::Parameter::Sample::TriangularRandom< ModelType::ParameterType > ParameterSampleType;

public:
  DictBuilder()
    : gridPtr_(Dune::Parameter::getValue< std::string >("grid.macroGridFile")),
      grid_(*gridPtr_),
      gridPart_(grid_),
      mdGridPart_(grid_),
      model_(),
      dict_(),
      disco_(model_, gridPart_, dict_),
      dictGenerator_(dict_, model_, disco_)
  {
  }

  void build()
  {
    // get a parameter sample
    const int           paramDimensionSize = Dune::Parameter::getValue< int >("podgreedy.paramspace.size");
    ParameterSampleType parameterSample(model_.minParameter(), model_.maxParameter(), paramDimensionSize, 20110729);

    // run the msrb greedy for this parameter sample
    myUuid_ = dictGenerator_.generate(parameterSample);
    return;
  } // build

  void save()
  {
    std::string datapath;
    
    if (Dune::Parameter::exists("saveTo"))
      datapath = Dune::Parameter::getValue< std::string >("saveTo");
    else
      datapath = "dictbuilder_data_" + boost::lexical_cast< std::string >(myUuid_);
    
    boost::filesystem::create_directory(datapath);
    bool saved = dict_.save(datapath);
    if (saved)
      std::cout << "Generated dictionary has been saved to: " << datapath << " !\n";
    else
      std::cout << "An error occured saving dictionary to: " << datapath << " !\n";
    // return;
  } // save

private:
  GridPtr< GridType >         gridPtr_;
  GridType&                   grid_;
  GridPartType                gridPart_;
  MDGridPartType              mdGridPart_;
  ModelType                   model_;
  DictionaryType              dict_;
  DetailedSolverConnectorType disco_;
  DictGeneratorType           dictGenerator_;
  boost::uuids::uuid          myUuid_;
};
}
}
}

#endif // #ifndef __DUNE_RB_UTILITIES_DICTBUILDER_HH__