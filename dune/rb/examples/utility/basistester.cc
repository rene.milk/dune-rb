#include <dune/rb/examples/utility/basistester.hh>
#include <dune/common/exceptions.hh>
int main(int argc, char* argv[])
{
  try {
    Py_Initialize();
    PySys_SetArgv(argc, argv);
    
    Dune::MPIManager::initialize(argc, argv);
    Dune::Parameter::append(argc, argv);
    if (!(Dune::Parameter::exists("reducedDataPath") && Dune::Parameter::exists("reducedDataPath")))
      DUNE_THROW(
        Dune::IOError,
        "Please provide the path to the precomputed reduced data using ./basistester <additional parameter> reducedDataPath:/path/to/the/data outputPath:/your/output/path");
    if (Dune::Parameter::exists("parameterFile"))
      Dune::Parameter::append(Dune::Parameter::getValue< std::string >("parameterFile"));
    Dune::RB::Utilities::BasisTester tester;
    tester.load(Dune::Parameter::getValue< std::string >("reducedDataPath"));
    tester.runTest();
    tester.saveTestData(Dune::Parameter::getValue< std::string >("outputPath"));
    
    tester.visualizeTestData(Dune::Parameter::getValue< std::string >("outputPath"));
    Py_Finalize();
  } catch (Dune::Exception& e) {
    std::cerr << e.what();
    return 1;
  } catch (...) {
    std::cerr << "Caught an unknown exeption!\n";
    return 1;
  }


  return 0;
} // main
