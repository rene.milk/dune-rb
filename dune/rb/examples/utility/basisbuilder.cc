#include <dune/rb/examples/utility/basisbuilder.hh>

int main(int argc, char* argv[])
{
  try {
    Dune::MPIManager::initialize(argc, argv);
    Dune::Parameter::append(argc, argv);
    if (Dune::Parameter::exists("parameterFile"))
      Dune::Parameter::append(Dune::Parameter::getValue<std::string>("parameterFile"));
    Dune::RB::Utilities::BasisBuilder builder;
    builder.build();
    builder.save();
  } catch (Dune::Exception& e) {
    std::cerr << e.what();
    return 1;
  } catch (...) {
    std::cerr << "Caught an unknown exeption!\n";
    return 1;
  }
  
  
  return 0;
} // main
