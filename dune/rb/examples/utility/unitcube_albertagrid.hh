#ifndef DUNE_RB_EXAMPLES_UNITCUBE_ALBERTAGRID_HH
#define DUNE_RB_EXAMPLES_UNITCUBE_ALBERTAGRID_HH

#include "unitcube.hh"
#include "basicunitcube.hh"


#if HAVE_ALBERTA
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/gridfactory.hh>

namespace Dune
{
namespace RB
{
namespace Examples
{


template< int dim >
class UnitCube< Dune::AlbertaGrid< dim, dim >, 1 >
: public BasicUnitCube< dim >
{
public:
  typedef Dune::AlbertaGrid< dim, dim > GridType;

private:
  GridType *grid_;

public:
  UnitCube ()
  {
    Dune::GridFactory< GridType > factory;
    BasicUnitCube< dim >::insertVertices( factory );
    BasicUnitCube< dim >::insertSimplices( factory );
    grid_ = factory.createGrid( "UnitCube" );
  }

  ~UnitCube ()
  {
    Dune::GridFactory< GridType >::destroyGrid( grid_ );
  }

  GridType &grid ()
  {
    return *grid_;
  }
};

}  // namespace Examples
}  // namespace RB
}  // namespace Dune


#endif // #if HAVE_ALBERTA

#endif
