#ifndef __DUNE_RB_UTILITIES_BASISTESTER_HH__
#define __DUNE_RB_UTILITIES_BASISTESTER_HH__

#include <config.h>

// dune-rb includes
#include <dune/rb/detailed/model/provider/default.hh>
#include <dune/rb/reduced/solver/connector/eigen.hh>
#include <dune/rb/reduced/estimator/connector/residual.hh>
#include <dune/rb/la/separableparametric/matrix/eigen.hh>
#include <dune/rb/reduced/parameter/sample/triangularrandom.hh>
#include <dune/rb/detailed/solver/connector/ellipticdg.hh>

#include <boost/filesystem.hpp>
// for plotting vecs
#include "Python.h"
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

namespace Dune {
namespace RB {
namespace Utilities {
class BasisTester
{
  typedef GridSelector::GridType                                          GridType;
  typedef LeafGridPart< GridType >                                        GridPartType;
  typedef Detailed::Model::Provider::Default< Eigen::VectorXd, GridType > ModelProviderType;
  typedef ModelProviderType::ModelType                                    ModelType;


  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseMatrix                                  SepMatrixType;
  typedef Dune::RB::LA::SeparableParametric::Container::EigenDenseVector                                  SepVectorType;
  typedef Reduced::Solver::Connector::EigenBased< ModelType, SepMatrixType, SepVectorType >               RescoType;
  typedef Reduced::Estimator::Connector::ResidualConnector< Eigen::VectorXd, Eigen::MatrixXd, ModelType > EscoType;


  typedef ModelType::ParameterType                                      ParameterType;
  typedef Reduced::Parameter::Sample::TriangularRandom< ParameterType > ParameterSampleType;

public:
  BasisTester()
    : modelProvider_(Dune::Parameter::getValue< std::string >("model.identifier")),
      model_(modelProvider_.model()),
      esco_(model_),
      resco_(model_)
  {
  }

  void load(const std::string path)
  {
    resco_.load(path);
    esco_.load(path);
    return;
  }

  void runTest()
  {
    // get a parameter sample
    const int           paramDimensionSize = Dune::Parameter::getValue< int >("testing.paramspace.size");
    ParameterSampleType parameterSample(model_.minParameter(), model_.maxParameter(), paramDimensionSize, 19910527);
    const int           paramSampleSize = parameterSample.size();

    errors_.resize(paramSampleSize, 2);

    for (int k = 0; k < paramSampleSize; ++k) {
      ParameterType        param      = parameterSample.get(k);
      RescoType::DofVectorType redVec     = resco_.solve(param);
      double               upperBound = esco_.estimate(param, redVec);
      double               lowerBound = esco_.lowerBound(param, redVec);
      errors_(k, 0) = upperBound;
      errors_(k, 1) = lowerBound;
    }

    return;
  } // runTest

  // template<class GridPartType>
  // void runTest(GridPartType& gridPart)
  // {
  // // get a parameter sample
  // const int           paramDimensionSize = Dune::Parameter::getValue< int >("testing.paramspace.size");
  // ParameterSampleType parameterSample(model_.minParameter(), model_.maxParameter(), paramDimensionSize, 20110729);
  // const int    paramSampleSize = paramSample.size();
  // errors_.resize(paramSampleSize, 2);
  //
  // // get a high-dimensional solver
  // Detailed::Solver::Connector::EllipticDG< ModelType, GridPartType > disco;
  //
  // for (int k = 0; k < paramSampleSize; ++k) {
  // ParameterType        param  = paramSample.get(k);
  // ReducedDofVectorType redVec = resco_.solve(param);
  // double               upperBound  = esco_.estimate(param, redVec);
  // double               lowerBound  = esco_.lowerBound(param, redVec);
  // errors_(k, 1) = upperBound;
  // errors_(k, 3) = lowerBound;
  //
  // // compute high-dimensional solution and it
  // }
  //
  // return;
  // }

  bool saveTestData(const std::string path) const
  {
    boost::filesystem::create_directory(path);

    return errors_.saveToTextFile(path + "/basistester_output.txt");
  } // saveTestData

  void visualizeTestData(const std::string path)
  {
    //Py_Initialize();
    // PySys_SetArgv(argc, argv);
    // plot the error bounds
    try {
      boost::python::class_< std::vector< double > >("PyVec")
        .def(boost::python::vector_indexing_suite< std::vector< double > >());

      
      boost::python::object mymodule = boost::python::import("mymodule");
      boost::python::object dict     = mymodule.attr("__dict__");
      boost::python::object plot     = dict["plotErrorBars"];
      if (errors_.cols()==2)
        plot(std::vector<double>(errors_.col(0).data(),errors_.col(0).data()+errors_.rows()-1),
             std::vector<double>(errors_.col(1).data(),errors_.col(1).data()+errors_.rows()-1),
             std::string(path + "basistest_output.png"));
      else if (errors_.cols()==3)
        plot(errors_.col(0), errors_.col(1), errors_.col(2), std::string(path + "basistest_output.png"));
    } catch (boost::python::error_already_set&) {
      PyErr_Print();
      boost::python::object sys(boost::python::handle< >(PyImport_ImportModule("sys")));
      boost::python::object err      = sys.attr("stderr");
      std::string           err_text = boost::python::extract< std::string >(err.attr("getvalue") ());
      std::cout << "PythonError: " << err_text << endl;
      std::cerr << "Plotting the error with bounds did not work!\n";
    }

    // Py_Finalize();
  } // visualizeTestData

private:
  ModelProviderType modelProvider_;
  ModelType&        model_;
  EscoType          esco_;
  RescoType         resco_;
  Eigen::MatrixXd   errors_;
};
}
}
}

#endif // #ifndef __DUNE_RB_UTILITIES_BASISTESTER_HH__
