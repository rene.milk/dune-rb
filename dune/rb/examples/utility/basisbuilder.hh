#ifndef __DUNE_RB_UTILITIES_BASISBUILDER_HH__
#define __DUNE_RB_UTILITIES_BASISBUILDER_HH__

#include <config.h>
#include <vector>
#include <boost/type_traits/is_same.hpp>
// dune-fem includes
#include <dune/fem/gridpart/gridpart.hh>

// dune-rb includes
#include <dune/rb/grid/coarse/multidomain.hh>
#include <dune/rb/offline/mapper/coarse.hh>
#include <dune/rb/offline/space/localized.hh>
#include <dune/rb/rbasis/twophaseflow/grid/devidegrid.hh>
#include <dune/rb/offline/generator/reducedbasis/msrbgreedy.hh>
#include <dune/rb/detailed/model/spe10.hh>
#include <dune/rb/detailed/model/thermalblock.hh>
#include <dune/rb/detailed/solver/connector/ellipticdg.hh>
#include <dune/rb/offline/localizer/default.hh>
#include <dune/rb/offline/generator/reduceddata/solver/projected.hh>
#include <dune/rb/offline/generator/reduceddata/estimator/residual.hh>
#include <dune/rb/reduced/parameter/sample/triangularrandom.hh>
#include <dune/rb/detailed/model/provider/default.hh>

namespace Dune {
namespace RB {
namespace Utilities {
class BasisBuilder
{
  typedef GridSelector::GridType                                          GridType;
  typedef LeafGridPart< GridType >                                        GridPartType;
  typedef Dune::RB::Grid::Coarse::Multidomain< GridPartType >             MDGridPartType;
  typedef Detailed::Model::Provider::Default< Eigen::VectorXd, GridType > ModelProviderType;
  typedef ModelProviderType::ModelType                                    ModelType;


  typedef Offline::Mapper::Coarse< LA::SeparableParametric::Container::EigenDenseMatrix,
                                   MDGridPartType > CoarseMapperType;
  typedef Offline::Space::Localized< MDGridPartType,
                                     CoarseMapperType > OfflineSpaceType;
  typedef Detailed::Solver::Connector::EllipticDG< ModelType,
                                                   GridPartType >
  DetailedSolverConnectorType;


  typedef Offline::Localizer::Default< GridType, MDGridPartType,
                                       DetailedSolverConnectorType::MapperType > LocalizerType;
  typedef Offline::Generator::ReducedData::Solver::Projected< ModelType, OfflineSpaceType,
                                                              DetailedSolverConnectorType, LocalizerType >
  ReducedSolverGeneratorType;
  typedef Offline::Generator::ReducedData::Estimator::Residual< ModelType,
                                                                OfflineSpaceType,
                                                                DetailedSolverConnectorType,
                                                                LocalizerType > EstimatorGeneratorType;
  typedef Offline::Generator::ReducedBasis::MSRBGreedy< OfflineSpaceType, ModelType, DetailedSolverConnectorType,
                                                        ReducedSolverGeneratorType, EstimatorGeneratorType,
                                                        MDGridPartType,
                                                        LocalizerType > MSRBGreedyType;


  typedef Reduced::Parameter::Sample::TriangularRandom< ModelType::ParameterType > ParameterSampleType;

public:
  BasisBuilder()
    : gridPtr_(Dune::Parameter::getValue< std::string >("grid.macroGridFile")),
      grid_(*gridPtr_),
      gridPart_(grid_),
      mdGridPart_(grid_),
      sdMarker_(mdGridPart_),
      modelProvider_(Dune::Parameter::getValue< std::string >("model.identifier")),
      model_(modelProvider_.model()),
      disco_(model_, gridPart_),
      rbSpace_(mdGridPart_, disco_.mapper()),
      localizer_(grid_, mdGridPart_, disco_.mapper()),
      resgo_(model_, rbSpace_, disco_, localizer_),
      esgo_(model_, rbSpace_, disco_, localizer_),
      greedy_(rbSpace_, model_, disco_, resgo_, esgo_, mdGridPart_, localizer_)
  {
  }

  void build()
  {
    // get a parameter sample
    const int           paramDimensionSize = Dune::Parameter::getValue< int >("podgreedy.paramspace.size");
    ParameterSampleType parameterSample(model_.minParameter(), model_.maxParameter(), paramDimensionSize, 20110729);

    // run the msrb greedy for this parameter sample
    greedy_.init(Dune::Parameter::getValue< ModelType::ParameterType >("podgreedy.initialParameter"));
    myUuid_ = greedy_.generate(parameterSample);
    greedy_.compressInformation();
    return;
  } // build

  void save()
  {
    std::string datapath;

    if (Dune::Parameter::exists("saveTo"))
      datapath = Dune::Parameter::getValue< std::string >("saveTo");
    else
      datapath = "basisbuilder_data_" + boost::lexical_cast< std::string >(myUuid_);

    boost::filesystem::create_directory(datapath);
    bool saved = (rbSpace_.save(datapath) && resgo_.generate().save(datapath) && esgo_.generate().save(datapath));
    if (saved)
      std::cout << "Generated offline and online data has been saved to: " << datapath << " !\n";
    else
      std::cout << "An error occured saving offline and online data to: " << datapath << " !\n";
    return;
  } // save

private:
  GridPtr< GridType >         gridPtr_;
  GridType&                   grid_;
  GridPartType                gridPart_;
  MDGridPartType              mdGridPart_;
  SubdomainMarker             sdMarker_;
  ModelProviderType           modelProvider_;
  ModelType&                  model_;
  DetailedSolverConnectorType disco_;
  OfflineSpaceType            rbSpace_;
  LocalizerType               localizer_;
  ReducedSolverGeneratorType  resgo_;
  EstimatorGeneratorType      esgo_;
  MSRBGreedyType              greedy_;
  boost::uuids::uuid          myUuid_;
};
}
}
}

#endif // #ifndef __DUNE_RB_UTILITIES_BASISBUILDER_HH__