#ifndef DUNE_RB_EXAMPLES_UNITCUBE_YASPGRID_HH
#define DUNE_RB_EXAMPLES_UNITCUBE_YASPGRID_HH

#include "unitcube.hh"

#include <dune/grid/yaspgrid.hh>

namespace Dune
{
namespace RB
{
namespace Examples
{

// YaspGrid specialization
template<int gdim, int size>
class UnitCube<YaspGrid<gdim>,size>
{
public:
  typedef YaspGrid<gdim> GridType;

  UnitCube () : Len(1.0), s(size), p(false),
#if HAVE_MPI
  grid_(MPI_COMM_WORLD,Len,s,p,1)
#else
  grid_(Len,s,p,1)
#endif
  {  }

  YaspGrid<gdim>& grid ()
  {
	return grid_;
  }

private:  
  FieldVector<double,gdim> Len;
  FieldVector<int,gdim> s;
  FieldVector<bool,gdim> p;
  YaspGrid<gdim> grid_;
};

}  // namespace Examples
}  // namespace RB
}  // namespace Dune

#endif
