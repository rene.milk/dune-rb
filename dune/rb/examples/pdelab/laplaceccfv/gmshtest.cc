/*
 * gmshtest.cc
 *
 *  Created on: 02.04.2012
 *      Author: mdroh_01
 */

#include "config.h"

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>

#include <dune/common/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/gmshreader.hh>

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#error "We need UG Grid for the Gmsh reader"
#endif

#include <dune/grid/alugrid.hh>

int main (int argc, char **argv)
{
  Dune::MPIHelper::instance( argc, argv );
  if (argc!=3)
  {
    std::cerr << argv[0] << ": Command line arguments: <gridname> <level>" << std::endl;
  }

  int level = 0;
  sscanf(argv[2], "%d", &level);

//  typedef Dune::ALUCubeGrid<3,3> GridType;
//  typedef GridType IGridType;
  //typedef Dune::ALUSimplexGrid<3,3> GridType;

  typedef Dune::UGGrid<3> GridType;

  GridType grid;

  std::string gridName = argv[1];
  Dune::GmshReader<GridType> gmshreader;
  gmshreader.read(grid, gridName);

  gridName.erase(0, gridName.rfind("/")+1);
  gridName.erase(gridName.find(".", 0), gridName.length());

  grid.globalRefine(level);

  typedef GridType::LeafGridView GV;

  const GV & gv = grid.leafView();

  std::vector<int> a(gv.size(0), 1);

  Dune::VTKWriter<GV> vtkwriter(gv);
  vtkwriter.addCellData(a, "celldata");
  std::cout << "Write out file: " << gridName << std::endl;
  vtkwriter.write(gridName.c_str(), Dune::VTKOptions::ascii);

  return 0;

}


