/*
 * linevolofflinespace.cc
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */


#include <dune/rb/offline/generator/reduceddata/linevol.cc>

#include "traits.hh"

template class Dune::RB::Offline::Generator::ReducedData::LinEvol<typename Dune::RB::ProblemTraits::DetailedConnectorType>;
