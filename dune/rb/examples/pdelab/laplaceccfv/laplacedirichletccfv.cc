// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file 
 \brief Solve Laplace equation with cell-centered finite volume method
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include<iostream>
#include<vector>
#include<map>
#include<dune/common/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/timer.hh>
#include<dune/grid/yaspgrid.hh>

#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/p12dfem.hh>
#include<dune/pdelab/finiteelementmap/pk2dfem.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
#include<dune/pdelab/localoperator/laplacedirichletccfv.hh>
#include <dune/pdelab/eigenvectorbackend.hh>
#include <dune/pdelab/eigenmatrixbackend.hh>
#include <dune/pdelab/eigensolverbackend.hh>

#include <dune/rb/examples/utility/unitcube.hh>

#include <Eigen/Sparse>

// define some grid functions to interpolate from
template< typename GV, typename RF >
class G: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits< GV, RF, 1 >,
    G< GV, RF > >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits< GV, RF, 1 > Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase< Traits, G< GV, RF > > BaseT;

  G (const GV& gv)
      : BaseT( gv )
  {
  }
  inline void evaluateGlobal (const typename Traits::DomainType& x,
                              typename Traits::RangeType& y) const
  {
    typename Traits::DomainType center;
    for (int i = 0; i < GV::dimension; i++)
      center[i] = 0.5;
    center -= x;
    y = exp( -2*center.two_norm2() );
  }
};

// define some boundary grid functions to define boundary conditions
template< typename GV >
class B: public Dune::PDELab::BoundaryGridFunctionBase<
    Dune::PDELab::BoundaryGridFunctionTraits< GV, int, 1,
        Dune::FieldVector< int, 1 > >,
    B< GV > >
{
  const GV& gv;

public:
  typedef Dune::PDELab::BoundaryGridFunctionTraits< GV, int, 1,
      Dune::FieldVector< int, 1 > > Traits;
  typedef Dune::PDELab::BoundaryGridFunctionBase< Traits, B< GV > > BaseT;

  B (const GV& gv_)
      : gv( gv_ )
  {
  }

  template< typename I >
  inline void evaluate (const Dune::PDELab::IntersectionGeometry< I >& ig,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    y = 1; // all is Dirichlet boundary
  }

  //! get a reference to the GridView
  inline const GV& getGridView ()
  {
    return gv;
  }
};

template< class GV >
void test (const GV& gv)
{
  typedef typename GV::Grid::ctype DomainFieldType;
  typedef double RangeFieldType;
  const int dim = GV::dimension;
  Dune::Timer watch;

  // instantiate finite element maps
  typedef Dune::PDELab::P0LocalFiniteElementMap< DomainFieldType,
      RangeFieldType, dim > FEM;

  FEM fem( Dune::GeometryType( Dune::GeometryType::cube, dim ) ); // works only for cubes

  typedef Dune::PDELab::NoConstraints ConstraintsType;
  typedef Dune::PDELab::EigenVectorBackend VectorBackendType;
  typedef Dune::PDELab::SimpleGridFunctionStaticSize GFStaticSizeType;
  // make function space
  typedef Dune::PDELab::GridFunctionSpace< GV, FEM, ConstraintsType,
      VectorBackendType, GFStaticSizeType > GFS;
  watch.reset();

  GFS gfs( gv, fem );
  std::cout << "=== function space setup " << watch.elapsed() << " s"
            << std::endl;

  // make coefficent Vector and initialize it from a function
  typedef typename GFS::template VectorContainer< RangeFieldType >::Type V;
  V x0( gfs );

  x0 = 0.0;

  typedef G< GV, RangeFieldType > GType;
  GType g( gv );

  Dune::PDELab::interpolate( g, gfs, x0 );

  // make grid function operator
  Dune::PDELab::LaplaceDirichletCCFV< GType > la( g );
  typedef Dune::PDELab::GridOperatorSpace< GFS, GFS,
      Dune::PDELab::LaplaceDirichletCCFV< GType >,
      Dune::PDELab::EmptyTransformation, Dune::PDELab::EmptyTransformation,
      Dune::PDELab::SparseEigenMatrixBackend > GOS;
  GOS gos( gfs, gfs, la );

  // represent operator as a matrix
  typedef typename GOS::template MatrixContainer< RangeFieldType >::Type M;
  watch.reset();
  M m( gos );
  std::cout << "=== matrix setup " << watch.elapsed() << " s" << std::endl;
  m = 0.0;
  watch.reset();
  gos.jacobian( x0, m );

  std::cout << "=== jacobian assembly " << watch.elapsed() << " s" << std::endl;
  //  std::cout << m << std::endl;
  //Dune::printmatrix(std::cout,m.base(),"global stiffness matrix","row",9,1);

  // evaluate residual w.r.t initial guess
  V r( gfs );
  r = 0.0;
  watch.reset();
  gos.residual( x0, r );
  std::cout << "=== residual evaluation " << watch.elapsed() << " s"
            << std::endl;
  //  std::cout << r << std::endl;

  V x(gfs);
  r*=-1.0;
  Dune::PDELab::EigenBackend_BiCGSTAB_IILU solver;
  solver.apply(m, x, r, 1e-12);

  //  std::cout << x << std::endl;
  x += x0;

  std::cout << "Used iterations: " << solver.result().iterations << std::endl;
  std::cout << "Solver residual: " << solver.result().reduction << std::endl;

  typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
  DGF dgf(gfs,x);

  // output grid function with VTKWriter
  Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTKOptions::nonconforming);
  vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(dgf,"u"));
  vtkwriter.write("testlaplacedirichletccfv",Dune::VTKOptions::ascii);

}

int main (int argc, char** argv)
{
  try
  {
    //Maybe initialize Mpi
    Dune::MPIHelper::instance( argc, argv );

    // 2D
    {

      typedef Dune::YaspGrid< 2 > GridType;
      // make grid
      Dune::RB::Examples::UnitCube< GridType, 1 > unitcube;

      GridType & grid = unitcube.grid();

      grid.globalRefine( 6 );

      // solve problem :)
      test( grid.leafView() );
    }
    // test passed
    return 0;

  }
  catch (Dune::Exception &e)
  {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
