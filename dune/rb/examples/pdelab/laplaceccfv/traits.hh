/*
 * traits.hh
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */

#ifndef DUNE_RB_EXAMPLES_PDELAB_LAPLACECCFV_TRAITS_HH_
#define DUNE_RB_EXAMPLES_PDELAB_LAPLACECCFV_TRAITS_HH_

#include <dune/rb/matlabcomm/server/sockets.hh>
#include <dune/rb/detailed/solver/connector/pdelab/laplaceccfv.hh>
#include <dune/rb/offline/generator/reduceddata/linevol.hh>
#include <dune/rb/matlabcomm/library/pdelablinevol.hh>

namespace Dune
{
namespace RB
{


struct ProblemTraits
{
  typedef Dune::RB::Detailed::Solver::Connector::LaplaceCCFV DetailedConnectorType;
  typedef typename DetailedConnectorType::BasicTraits DetailedTraits;
  typedef Dune::RB::Offline::Generator::ReducedData::LinEvol<DetailedConnectorType> OfflineDataGeneratorType;
};
typedef Dune::RB::MatlabComm::Server::SocketsTraits ServerTraits;
typedef typename ServerTraits::ServerType ServerType;

typedef Dune::RB::MatlabComm::Library::PdeLabLinEvol<ServerTraits, ProblemTraits> LibraryType;

} // end of namespace RB
} // end of namespace Dune

#endif /* DUNE_RB_EXAMPLES_PDELAB_LAPLACECCFV_TRAITS_HH_ */
