/*
 * pdelablinevol.cc
 *
 *  Created on: 18.04.2012
 *      Author: martin
 */

#include <dune/rb/matlabcomm/library/pdelablinevol.cc>

#include "traits.hh"

template class Dune::RB::MatlabComm::Library::PdeLabLinEvol<Dune::RB::ServerTraits, Dune::RB::ProblemTraits>;

