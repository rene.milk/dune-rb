#include <config.h>

#include <dune/common/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/rb/matlabcomm/facade/base.hh>
#include <dune/rb/matlabcomm/library/pdelablinevol.hh>
#include <dune/rb/matlabcomm/facade/linevol.hh>

#ifndef HAVE_EIGEN
#error "We need the Eigen library!"
#endif

#include "traits.hh"

using namespace Dune::RB;
using Dune::MPIHelper;

bool MatlabComm::Server::Sockets::isInitialized_ = false;

/**
 * @brief entry point for a stand-alone application acting as a server
 */
int main (int argc, char* argv[])
{
  /* initialize MPIManager */
  MPIHelper::instance(argc, argv);

  Dune::ParameterTree pt;
  ServerType::initParameterTree(argc, argv, pt);

  Dune::ParameterTree & modelPT = pt.sub("model");
  typename ProblemTraits::DetailedConnectorType connector(modelPT);

  LibraryType discr(connector);

  Dune::RB::MatlabComm::Facade::LinEvol<ServerTraits> facade(discr);

  ServerType::bindToFacade(facade);

  ServerType::run();

  return 0;
}
