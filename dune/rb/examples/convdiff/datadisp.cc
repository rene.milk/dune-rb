#include  "config.h"

#if HAVE_GRAPE
#include  "convdiffdescr.hh"


typedef DescrType::IOTupleType                GR_InputType;
typedef DescrType::ModelType                  ModelType;
typedef ModelType::InitialDataType   InitialDataType;

#include  <dune/fem/io/visual/grape/datadisp/errordisplay.hh>

namespace Dune {

  template<typename DiscreteFunctionType, typename SolutionType>
  class DisplayExactFunction
  {
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteFunctionSpaceType;

    typedef typename DiscreteFunctionSpaceType :: GridPartType
      GridPartType;

    struct ExactSolution 
    {
    public:

      typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;

      typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType
        FunctionSpaceType;

      typedef typename FunctionSpaceType :: DomainType DomainType;
      typedef typename FunctionSpaceType :: RangeType RangeType;
      typedef typename FunctionSpaceType :: JacobianRangeType JacobianRangeType;

    private:
      typedef typename GridPartType :: template Codim< 0 > :: IteratorType :: Entity
        EntityType;
      typedef typename EntityType :: Geometry GeometryType;
    public:

      ExactSolution(double time, const SolutionType & solution):
        time_(time),
        initU_(solution),
        geometry_( 0 ) {};

      template< typename PointType> 
      void evaluate(const PointType & x, RangeType & ret)
      {
        DomainType global = geometry_->global( coordinate( x ) );
        initU_.evaluate(global, time_, ret);
      }

      inline void init (const EntityType & entity )
      {
        geometry_ = &( entity.geometry() );
      }

    private:
      double time_;
      const SolutionType & initU_;
      const GeometryType *geometry_;
    };

    typedef LocalFunctionAdapter<ExactSolution> ExactFunctionType; 
  public:
    template< class GrapeDispType >
      DisplayExactFunction( GrapeDispType & disp,
                            const DiscreteFunctionType & Uh,
                            const SolutionType &solution,
                            const double time = 0 )
      : gridPart_( Uh.space().gridPart() ),
      exact_(time, solution),
      exactFunction_("exact", exact_, gridPart_)
    {

      disp.addData( exactFunction_, "exact", time);
    }
  private:
    const GridPartType & gridPart_;
    ExactSolution exact_;
    ExactFunctionType exactFunction_;

  };

  template<class GrapeDispType,
    class GR_GridType,
    class DestinationType>
  void postProcessing(GrapeDispType& disp,
                      const GR_GridType& grid,
                      const double time,
                      const int tstep,
                      const DestinationType &Uh)
  {
    DescrType::DataType * data = new DescrType::DataType();
    ModelType * model          = new ModelType(*data);
    const InitialDataType & U0 = model->initialData();
    typedef DisplayErrorFunction<DestinationType, InitialDataType, true> ErrorFunctionType;
    ErrorFunctionType * errorFunction = new ErrorFunctionType(disp, Uh, U0, time);
    typedef DisplayExactFunction<DestinationType, InitialDataType> ExactFunctionType;
    ExactFunctionType * exactFunction = new ExactFunctionType(disp, Uh, U0, time);
  }

}

#include <dune/fem/io/visual/grape/datadisp/datadisp.cc>
#else
int main()
{ 
}
#endif // HAVE_GRAPE

