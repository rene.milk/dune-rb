#ifndef __DATAFUNCINITDATA_HH_
#define __DATAFUNCINITDATA_HH_

#include "datafuncif.hh"
#include <dune/common/fmatrix.hh>

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Data {


/** @addtogroup datafunc */
/*@{*/

/**
 * @brief data function implementing an initial data function implementing a
 * blob function
 *
 * This class implements
 * @f[ u_{0,\mbox{blob}}(\mu;x)
 *  = \sigma(\mu) \cdot \exp(-S \| x-b \|_2 ) \chi(\| x-b \|_2 \leq R) \f]
 * with
 * @f[ \sigma(\mu) = \mu @f]
 *
 * List of parameters:
 *  - <tt> rb.initScale </tt>  scaling factor @f$\mu@f$
 *  - <tt> rb.blobCenter </tt> coordinate vector of blob center @f$b@f$
 *  - <tt> rb.blobRadius </tt> scalar for blob radius @f$R@f$
 *  - <tt> rb.blobSlope </tt>  scalar for blob value gradient @f$S@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.initScale, scaling factor}
 *
 * Other parameters:
 * @rbparam{rb.blobCenter, coordinate vector of blob center, (0.5\,0.5\,0.5)}
 * @rbparam{rb.blobRadios, scalar for blob radius, 0.2}
 * @rbparam{rb.blobSlope, scalar for blob value slope factor, 10}
 *
 * @tparam ModelParam parameter class describing the model
 */
template< class ModelParam >
class InitDataSingleBlob
    : public SpaceFunctionDefault< InitDataSingleBlob<ModelParam>, ModelParam >
{
public:
  //! domain vector type
  typedef typename ModelParam :: DomainType                          DomainType;
  //! range vector type
  typedef typename ModelParam :: RangeType                           RangeType;
  //! entity pointer type
  typedef typename ModelParam :: EntityPointer                       EntityPointer;
  //! entity type
  typedef typename ModelParam :: Entity                              Entity;
  //! function space type
  typedef typename ModelParam :: FunctionSpaceType                   FunctionSpaceType;
  //! grid part type
  typedef typename ModelParam :: GridPartType                        GridPartType;

  //! dimension of domain vector space
  static const unsigned int dimDomain = DomainType :: dimension;
  //! number of summands in data function (=1)
  static const int numComps = 1;

public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  InitDataSingleBlob(const ModelType& model, const GridPartType & gridPart)
    : muDependent_(ParameterHelper :: isInMuNames("rb.initScale")),
      blobCenter_(0.5),
      blobRadius_(0.2),
      blobSlope_(10)
  {
    if(Parameter::exists("rb.blobRadius"))
      blobRadius_ = Parameter::getValue<double>("rb.blobRadius");
    if(Parameter::exists("rb.blobSlope"))
      blobSlope_ = Parameter::getValue<double>("rb.blobSlope");

    if(Parameter::exists("rb.blobCenter"))
    {
      std::vector<double> temp;
      Parameter::get("rb.blobCenter", temp);
      if( temp.size() == 1)
        for(size_t i = 0; i < dimDomain; ++i)
          blobCenter_[i] = temp[0];
      else
        for( size_t i = 0; i < dimDomain; ++i )
          blobCenter_[i] = temp[i];
    }
  }

  template<class PointType>
  void evaluate ( const PointType &localx, RangeType &ret) const
  {
    DomainType  x = this->global( coordinate(localx) );

    double radius2 = (x - blobCenter_).two_norm2();
    if( radius2 < blobRadius_*blobRadius_ ) {
      ret[0] = exp(-blobSlope_*radius2);
    } else {
      ret[0] = 0.0;
    }
  }

  double coefficient() const
  {
    return Parameter::getValue<double>("rb.initScale");
  }

  inline std::string symbolicCoefficient() const
  {
    return "rb.initScale";
  }

  inline bool isMuDependent() const
  {
    return muDependent_;
  }

private:
  bool          muDependent_;
  DomainType    blobCenter_;
  double        blobRadius_;
  double        blobSlope_;
};

/**
 * @brief data function implementing a 2D-sine initial data function.
 *
 * This class implements
 * @f[ 
 * u_{0,\mbox{sine}}(x) = \sin(2\pi\|x\|^2) 
 * @f]
 *
 * List of parameters:
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 *
 * @tparam ModelParam parameter class describing the model
 */
template< class ModelParam >
class InitData2DSine
    : public SpaceFunctionDefault< InitData2DSine<ModelParam>, ModelParam >
{
public:
  //! domain vector type
  typedef typename ModelParam :: DomainType                          DomainType;
  //! range vector type
  typedef typename ModelParam :: RangeType                           RangeType;
  //! entity pointer type
  typedef typename ModelParam :: EntityPointer                       EntityPointer;
  //! entity type
  typedef typename ModelParam :: Entity                              Entity;
  //! function space type
  typedef typename ModelParam :: FunctionSpaceType                   FunctionSpaceType;
  //! grid part type
  typedef typename ModelParam :: GridPartType                        GridPartType;

  //! dimension of domain vector space
  static const unsigned int dimDomain = DomainType :: dimension;
  //! number of summands in data function (=1)
  static const int numComps = 1;

public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  InitData2DSine(const ModelType& model, const GridPartType & gridPart)
    : muDependent_(false)
  {};

  template<class PointType>
  void evaluate ( const PointType &localx, RangeType &ret) const
  {
    DomainType  x = this->global( coordinate(localx) );

    ret[0] = sin(2*M_PI*x.two_norm2());
  }

  double coefficient() const
  {
    return 1.;
  }

  inline std::string symbolicCoefficient() const
  {
    return "1";
  }

  inline bool isMuDependent() const
  {
    return muDependent_;
  }

  inline bool isTimeDependent() const
  {
    return false;
  }
private:
  bool          muDependent_;
};



/**
 * @brief data function implementing an initial data function implementing an
 * unsteady wave function
 *
 * This class implements
 * @f[ u_{0,\mbox{blob}}(\mu;x)
 *  = \left( \sigma^1(\mu) \cdot x_1 + \sigma^2(\mu) \cdot 1 \right)
 *    \chi( L \leq \| x_1 \leq R) \f]
 * with
 * @f[ \sigma^i(\mu) = \mu_1 \cdot \mu_{i+1} \quad \mbox{for } i=1,2 @f]
 *
 * List of parameters:
 *  - <tt> rb.initScale </tt>     global scaling factor @f$\mu_1@f$
 *  - <tt> rb.waveGradient </tt>  gradient @f$\mu_2@f$
 *  - <tt> rb.waveOffset </tt>    y-offset @f$\mu_3@f$
 *  - <tt> rb.waveLeft </tt>      left boundary @f$L@f$
 *  - <tt> rb.waveRight </tt>     right boundary @f$R@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.initScale, global scaling factor}
 * @rbparam{rb.waveGradient, gradient scaling factor}
 * @rbparam{rb.waveOffset, y-offset}
 *
 * Other Parameters:
 * @rbparam{rb.waveLeft, left boundary of wave, 1.0}
 * @rbparam{rb.waveRight, right boundary of wave, 3.0}
 *
 * @tparam ModelParam parameter class describing the model
 */
template< int part, class ModelParam >
class InitDataUnsteadyWaveBase
    : public SpaceFunctionDefault< InitDataUnsteadyWaveBase<part, ModelParam>, ModelParam >
{
public:
  typedef typename ModelParam :: DomainType                          DomainType;
  typedef typename ModelParam :: RangeType                           RangeType;
  typedef typename ModelParam :: EntityPointer                       EntityPointer;
  typedef typename ModelParam :: Entity                              Entity;
  typedef typename ModelParam :: FunctionSpaceType                   FunctionSpaceType;
  typedef typename ModelParam :: GridPartType                        GridPartType;

  static const int numComps = 2;

public:
  template<class ModelType>
  InitDataUnsteadyWaveBase(const ModelType& model, const GridPartType & gridPart)
    : muDependent_(ParameterHelper::isInMuNames("rb.initScale")),
      waveLeft_(1.0),
      waveRight_(3.0)
  {
    if(Parameter::exists("rb.waveLeft"))
      waveLeft_ = Parameter::getValue<double>("rb.waveLeft");
    if(Parameter::exists("rb.waveRight"))
      waveRight_ = Parameter::getValue<double>("rb.waveRight");
    switch(part)
    {
    case 0:
      muDependent_ |= ParameterHelper::isInMuNames("rb.waveGradient");
      break;
    case 1:
      muDependent_ |= ParameterHelper::isInMuNames("rb.waveOffset");
      break;
    }

  }

  inline bool isMuDependent() const
  {
    return muDependent_;
  }

protected:
  bool          muDependent_;
  double        waveLeft_;
  double        waveRight_;
};

template<int part, class ModelParam>
class InitDataUnsteadyWave;

template<class ModelParam>
class InitDataUnsteadyWave<0, ModelParam> : public InitDataUnsteadyWaveBase<0, ModelParam>
{
private:
  typedef InitDataUnsteadyWaveBase< 0, ModelParam >                     BaseType;
  using BaseType :: waveLeft_;
  using BaseType :: waveRight_;
public:
  typedef typename BaseType :: GridPartType                          GridPartType;
  typedef typename BaseType :: Entity                                Entity;
  typedef typename BaseType :: RangeType                             RangeType;
  typedef typename BaseType :: DomainType                            DomainType;
public:
  template<class ModelType>
  InitDataUnsteadyWave(const ModelType& model, const GridPartType & gridPart)
    : BaseType(model, gridPart)
  {}

  template<class PointType>
  void evaluate ( const PointType &localx, RangeType &ret) const
  {
    DomainType  x = this->global( coordinate(localx) );

    if ( x[0] >= waveLeft_ && x[0] <= waveRight_ )
      ret[0] = x[0];
    else
      ret[0] = 0.0;
  }

  double coefficient() const
  {
    return Parameter::getValue<double>("rb.initScale")
      * Parameter::getValue<double>("rb.waveGradient");
  }

  std::string symbolicCoefficient() const
  {
    return "rb.initScale * rb.waveGradient";
  }

};

template<class ModelParam>
class InitDataUnsteadyWave<1, ModelParam> : public InitDataUnsteadyWaveBase<1, ModelParam>
{
private:
  typedef InitDataUnsteadyWaveBase< 1, ModelParam >                     BaseType;
  using BaseType :: waveLeft_;
  using BaseType :: waveRight_;
public:
  typedef typename BaseType :: GridPartType                          GridPartType;
  typedef typename BaseType :: Entity                                Entity;
  typedef typename BaseType :: RangeType                             RangeType;
  typedef typename BaseType :: DomainType                            DomainType;
public:
  template<class ModelType>
  InitDataUnsteadyWave(const ModelType& model, const GridPartType & gridPart)
    : BaseType(model, gridPart)
  {}

  template<class PointType>
  void evaluate ( const PointType &localx, RangeType &ret) const
  {
    DomainType  x = this->global( coordinate(localx) );

    if ( x[0] >= waveLeft_ && x[0] <= waveRight_ )
      ret[0] = 1.0;
    else
      ret[0] = 0.0;
  }

  double coefficient() const
  {
    return Parameter::getValue<double>("rb.initScale")
      * Parameter::getValue<double>("rb.waveOffset");
  }

  std::string symbolicCoefficient() const
  {
    return "rb.initScale * rb.waveOffset";
  }

};

/*@}*/
}  // namespace Data
}  // namespace Lib
} // end of namespace Dune :: RB
} // end of namespace Dune

#endif /* __DATAFUNCINITDATA_HH_ */

