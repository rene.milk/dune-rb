#ifndef __DATAFUNCDIRICHLET_HH_
#define __DATAFUNCDIRICHLET_HH_

#include "datafuncif.hh"
#include <vector>
#include <string>
#include <dune/common/fmatrix.hh>

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Data {

/** @addtogroup datafunc */
/*@{*/

/**
 * @brief data function implementing a constant dirichlet coefficient
 *
 * This class implements
 * @f[ c_{\mbox{dir}}(\mu;x,t) = \sigma(\mu) \cdot 1 \f]
 * with
 * @f[ \sigma(\mu) = \mu @f]
 *
 * List of parameters:
 *  - <tt> rb.c_dir </tt>  dirichlet constant @f$\mu@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.c_dir, dirichlet constant}
 *
 * @tparam ModelParam parameter class describing the model
 */
template<class ModelParam>
class DirichletConstant
 : public SpaceFunctionDefault<DirichletConstant<ModelParam>, ModelParam>
{
public:
  //! model parameter type
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! domain vector type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! number of summands in this data function (=1)
  static const int numComps = 1;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  DirichletConstant(const ModelType & model, const GridPartType & gridPart)
    : muDependent_(false)
  {
    std::vector<std::string> mu_namesList;
    Parameter::get("rb.mu_names", mu_namesList);
    if( std::find(mu_namesList.begin(), mu_namesList.end(), std::string("rb.c_dir"))
        != mu_namesList.end() )
    {
      muDependent_ = true;
    }
  }

  virtual ~DirichletConstant() {};

  /* \f$
     b_d(x, t; \mu) = \prod_{i=1}^\textnormal{dim} c_{1_i} \cdot \prod_{i=1}^\textnormal{dim}
     \left(\cos(c_{2_i} \pi (x_i-v_i\cdot t)) + \sin(c_{2_i}\pi (x_i-v_i\cdot t))\right)
     \cdot \exp( -d \cdot t\cdot \sqrt{pi} \cdot \|c_2\|)
     \f$
  */

  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
/*    //! \todo do something with the time
 *    double t = 0.0;
 *    ret[0] = 1.0;
 *    DomainType globalX = this->global(localX);
 *    for (int i=0; i != dim; ++i) {
 *      double argument = periodicity_[i] * M_PI * ( globalX[i] - velocity_[i] * t );
 *      ret[0] *= cos( argument ) + sin( argument );
 *    }
 *    double c2_norm2 = periodicity_.two_norm2();
 *    ret[0] *= exp( -1.0 * diffusion_ * t * SQR( M_PI ) * c2_norm2 );*/
    ret[0] = 1;
  }

  inline double coefficient() const
  {
    return Parameter::getValue<double>("rb.c_dir");
  }

  inline std::string symbolicCoefficient() const
  {
    return "rb.c_dir";
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }
private:
  bool          muDependent_;
  double        coefficient_;
  double        diffusion_;
  DomainType    velocity_;
  DomainType    periodicity_;
};

/**
 * @brief data function implementing a sine dirichlet coefficient
 *
 * This class implements
 * @f[ c_{\mbox{dir}}(\mu;x,t) =  \sigma(\mu) sin(\|x-at\|)\f]
 * where
 * @f[a=(v_1, ..., v_d)\f]
 * is the velocity used in the model and
 * @f[ \sigma(\mu) = \mu @f]
 *
 * List of parameters:
 *  - <tt> rb.c_dir </tt>  dirichlet constant @f$\mu@f$
 *  - <tt> rb.velocity_x </tt>  velocity component @f$v_1@f$
 *  - <tt> rb.velocity_y </tt>  velocity component @f$v_2@f$
 *  - <tt> rb.velocity_z </tt>  velocity component @f$v_3@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.c_dir, dirichlet constant}
 *
 * Other parameters:
 * @rbparam{rb.velocity_x, first component of velocity vector}
 * @rbparam{rb.velocity_y, second component of velocity vector}
 * @rbparam{rb.velocity_z, third component of velocity vector}
 *
 * @tparam ModelParam parameter class describing the model
 */
template<class ModelParam>
class DirichletSine
 : public Data::FunctionDefault<DirichletSine<ModelParam>, ModelParam>
{
public:
  //! model parameter type
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! domain vector type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! number of summands in this data function (=1)
  static const int numComps = 1;
  //! dimension of Domain
  static const int dimDomain = FunctionSpaceType :: dimDomain;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  DirichletSine(ModelType & model, const GridPartType & gridPart)
    : muDependent_(false)
  {
    std::vector<std::string> mu_namesList;
    Parameter::get("rb.mu_names", mu_namesList);
    if( ParameterHelper ::isInMuNames("rb.c_dir") )
    {
      muDependent_ = true;
    }

#ifndef NDEBUG
    bool rbvelocity_xyz_is_member_of_rbmu_names =
                    !( ParameterHelper :: isInMuNames("rb.velocity_x")
                     || ParameterHelper :: isInMuNames("rb.velocity_y")
                     || ParameterHelper :: isInMuNames("rb.velocity_z") );
    assert(rbvelocity_xyz_is_member_of_rbmu_names); // "rb.velocity_{x,y,z} is not allowed as member of rb.mu_names here" )
#endif

    DomainType defaultVelocity(1.0);
    ParameterHelper::fillVector("rb.velocity", velocity_, defaultVelocity);
  }

  virtual ~DirichletSine() {};

  template<class PointType>
  void evaluateTimed(const PointType & localX, double time, RangeType & ret) const
  {
    DomainType globalX = this->global(localX);
    DomainType velTimeProd(velocity_);
    velTimeProd *= time;
    ret[0] = sin( 2*M_PI*( globalX - velTimeProd ).two_norm2() );
  }

  inline double coefficient() const
  {
    return Parameter::getValue<double>("rb.c_dir");
  }

  inline std::string symbolicCoefficient() const
  {
    return "rb.c_dir";
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }

private:
  bool          muDependent_;
  double        coefficient_;
  double        diffusion_;
  DomainType    velocity_;
  DomainType    periodicity_;
};

/**
 * @brief data function implementing a cosine-sine dirichlet coefficient
 *
 * This class implements
 * @f[
 * u(x,t) = \sigma(\mu) 
 * \sum_{i=1}^{2}
 * \exp(-\varepsilon t \pi^2 |{c^{i}|})
 * \left(\prod_{j=1}^{3}
 *       \tilde{c}_{j}^{i} \cos(c_{j}^{i}\pi(x_{j}-v_{j} t)) +
 * {}    \hat{c}_{j}^{i}   \sin(c_{j}^{i}\pi(x_{j}-v_{j} t))
 * \right),
 * @f]
 * where
 * @f[a=(v_1,\ldots,v_d)\f]
 * is the velocity used in the model,
 * @f$ \varepsilon @f$ is the diffusion constant in the model, and
 * @f[
 * \sigma(mu) = mu
 * @f]
 *
 * List of parameters:
 *  - <tt> rb.c_dir </tt>  dirichlet constant @f$\mu@f$
 *  - <tt> rb.velocity_x </tt>  velocity component @f$v_1@f$
 *  - <tt> rb.velocity_y </tt>  velocity component @f$v_2@f$
 *  - <tt> rb.velocity_z </tt>  velocity component @f$v_3@f$
 *  - <tt> rb.diffusion </tt>  diffusion constant @f$\varepsilon@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.c_dir, dirichlet constant}
 *
 * Other parameters:
 * @rbparam{rb.velocity_x, first component of velocity vector}
 * @rbparam{rb.velocity_y, second component of velocity vector}
 * @rbparam{rb.velocity_z, third component of velocity vector}
 * @rbparam{rb.diffusion, diffusion constant}
 *
 * @tparam ModelParam parameter class describing the model
 */
template<class ModelParam>
class DirichletCosSin
 : public Data::FunctionDefault<DirichletCosSin<ModelParam>, ModelParam>
{
public:
  //! model parameter type
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! domain vector type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! number of summands in this data function (=1)
  static const int                                                   numParts = 1;
  typedef FieldMatrix< double, 2*DomainType :: size, DomainType
                      :: size >                                      FieldMatrixT;

  //! number of summands in data function (=1)
  static const int numComps = 1;

public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  DirichletCosSin(ModelType & model, const GridPartType & gridPart)
    : muDependent_(false)
  {
    std::vector<std::string> mu_namesList;
    Parameter::get("rb.mu_names", mu_namesList);
    if( ParameterHelper::isInMuNames("rb.c_dir") )
    {
      muDependent_ = true;
    }
#ifndef NDEBUG
    bool rbvelocity_xyz_is_member_of_rbmu_names =
                    !( ParameterHelper :: isInMuNames("rb.velocity_x")
                     || ParameterHelper :: isInMuNames("rb.velocity_y")
                     || ParameterHelper :: isInMuNames("rb.velocity_z") );
    assert(rbvelocity_xyz_is_member_of_rbmu_names); // "rb.velocity_{x,y,z} is not allowed as member of rb.mu_names here" )
#endif

    DomainType defaultVelocity(1.0);
    ParameterHelper::fillVector("rb.velocity", velocity_, defaultVelocity);

    if(Parameter::exists("rb.diffusion"))
      diffusion_ = Parameter::getValue<double>("rb.diffusion");
    else
      diffusion_ = 0.1;

    c[0][0]=2.0;    c[0][1]=1.0;    c[0][2]=1.3; // = c^1
    c[1][0]=0.7;    c[1][1]=0.5;    c[1][2]=0.1; // = c^2
    c[2][0]=0.8;    c[2][1]=0.4;    c[2][2]=-0.4;// = \hat c^1
    c[3][0]=0.2;    c[3][1]=0.1;    c[3][2]=0.2; // = \hat c^2
    c[4][0]=0.6;    c[4][1]=1.2;    c[4][2]=0.1; // = \tilde c^1
    c[5][0]=0.9;    c[5][1]=0.3;    c[5][2]=-0.3;// = \tilde c^2
  }

  virtual ~DirichletCosSin() {};

  template<class PointType>
  void evaluateTimed(const PointType & localX, double time, RangeType & ret) const
  {
    DomainType xGlobal = this->global(localX);
    int dim = DomainType::size;
    double sum = 0.0;
    for (int i=0; i!=dim-1; ++i) {
      double prod=1.0;
      for (int j = 0; j!=dim; ++j) {
        double cosTerm = cos(c[i][j]*M_PI* ( xGlobal[j] - velocity_[j]*time ));
        double sinTerm = sin(c[i][j]*M_PI* ( xGlobal[j] - velocity_[j]*time ));

        prod *= ( c[4+i][j]*cosTerm + c[2+i][j]*sinTerm  );
      } //loop over j
      prod *= exp( -1.0*diffusion_*time*M_PI*M_PI* c[i].two_norm2() );
      sum += prod;
    } // loop over i
    ret[0] = sum;

  }

  inline double coefficient() const
  {
    return Parameter::getValue<double>("rb.c_dir");
  }

  inline std::string symbolicCoefficient() const
  {
    return "rb.c_dir";
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }

private:
  bool          muDependent_;
  double        coefficient_;
  double        diffusion_;
  DomainType    velocity_;
  DomainType    periodicity_;
  FieldMatrixT  c;
};


/*@}*/
} //  namespace Lib
} // namespace Data

} // end of namespace Dune :: RB

} // end of namespace Dune

#endif /* __DATAFUNCDIRICHLET_HH_ */

