#ifndef __DATAFUNCIF_HH_
#define __DATAFUNCIF_HH_

#include "../misc/ducktyping.hh"
#include "../misc/parameter/parameter.hh"
#include "../rbasis/common/coefficientif.hh"
#include <cassert>
#include <iostream>

/**
 * @addtogroup datafunc
 *
 * Library of parametrized data functions
 *
 * @rbparam{rb.mu_names, Liste mit Namen der Parameter}
 */

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Data {


/**
 * @brief Interface class for data functions
 *
 * @ingroup datafunc
 * @interfaceclass
 */
template<class ModelParam>
class IFunction :
  virtual ICoefficient<typename ModelParam :: FieldType>
{
public:
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! field type
  typedef typename RangeType :: field_type                           FieldType;
public:
  virtual ~IFunction() {};

  /**
   * @brief selects a grid entity on which the data function (summand) can be
   * evaluated with local coordinates with evaluate()
   *
   * @param en  grid entity
   */
  __DUCK_START__
  inline void init(const Entity & en)
  __DUCK_END__

  /**
   * @brief evaluates the data function (summand) in given local coordinates.
   *
   * @param localX  local coordinates or quadrature point on which the data
   * function is evaluated
   * @param ret     result of evaluation
   */
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
    ::std::cerr << "DUCK TYPING does not work here!\nTherefore you get this error at runtime!" << std::endl;
    assert(false);
  }

};

/** @brief default implementation for an affinely separable data function */
template< class ImpType, class ModelParamType >
class FunctionDefault
  : public IFunction< ModelParamType >
{
private:
  typedef IFunction< ModelParamType >                        BaseType;
public:
  typedef typename BaseType :: Entity                                Entity;
  typedef typename Entity :: Geometry                                Geometry;
  typedef typename Geometry :: LocalCoordinate                       LocalCoordinate;
  typedef typename Geometry :: GlobalCoordinate                      GlobalCoordinate;
public:
  inline void init(const Entity & en)
  {
    en_ = &en;
  }

  void setTime(double time)
  {
    time_ = time;
  }

  template<class PointType, class RangeType>
  void evaluate(const PointType & localX,  RangeType & ret) const
  {
    asImp().evaluateTimed(coordinate(localX), time_, ret);
  }

  template<class RangeType>
  void evaluateTimed(const LocalCoordinate & localX,  RangeType & ret) const
  {
    std::cerr << "evaluateTimed not implemented!" << std::endl;
    assert(false);
  }

  const Geometry & geo() const
  {
    return en_->geometry();
  }

  bool isTimeDependent() const
  {
    return true;
  };

protected:
  FunctionDefault()
  {
    if( Parameter::exists("rb.startTime") )
    {
      time_ = Parameter::getValue<double>("rb.startTime");
    }
    else
    {
      time_ = 0.0;
    }
  }

  const GlobalCoordinate global(LocalCoordinate localX) const
  {
    const Geometry & ge = this->geo();
    return ge.global(localX);
  }

private:
  ImpType &asImp()
  {
    return static_cast<ImpType&>(*this);
  }
  const ImpType &asImp() const
  {
    return static_cast<const ImpType&>(*this);
  }

private:
  double        time_;
  Entity const *en_;
};

/**
 * @brief default implementation of a separable data function depending on the space field only.
 *
 * This class provides empty implementations for the time handling methods setTime() and isTimeDependent().
 *
 */
template< class ImpType, class ModelParamType >
class SpaceFunctionDefault
  : public FunctionDefault<ImpType, ModelParamType>
{
public:
  void setTime(double time)
  {};

  bool isTimeDependent() const
  {
    return false;
  };
};

} // end of namspace Data
} // end of namespace Lib
} // end of namespace Dune :: RB
} // end of namespace Dune


#endif /* __DATAFUNCIF_HH_ */

/* vim: set et sw=2: */

