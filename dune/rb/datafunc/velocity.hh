#ifndef __DATAFUNCVELOCITY_HH_
#define __DATAFUNCVELOCITY_HH_

#include "datafuncif.hh"
#include <string>
#include "../misc/parameter/parameter.hh"

namespace Dune {
namespace RBFem {
namespace Lib
{
namespace Data
{

/** @addtogroup datafunc */
/*@{*/

/**
 * @brief data function implementing a base for a (constant) velocity vector
 *
 * This class implements
 * @f[ v_{\mbox{dir}}(\mu;x,t) = \sum_{i=1}^{\mbox{dimDomain}} \sigma_i(\mu) \cdot v_i \f]
 * with
 * @f[ \sigma_i(\mu) = \mu_i \qquad v_i = e_i \quad \mbox{for } i=1,\dots,\mbox{dimDomain} @f]
 *
 * List of parameters:
 *  - <tt> rb.velocity_{x,y,z} </tt>  velocity component @f$\mu_{\{1,2,3\}}@f$.
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 *   @rbparam{rb.velocity_x, first component of velocity vector}
 *   @rbparam{rb.velocity_y, second component of velocity vector}
 *   @rbparam{rb.velocity_z, third component of velocity vector}
 *
 * @note this class is missing the @link DataFuncInterface::evaluate()
 * evaluate() @endlink method. So it is still abstract in duck typing mode and
 * needs to be derived from.
 *
 * @tparam component summand @f$i@f$
 * @tparam ModelParam parameter class describing the model
 */
template<int dim, class EvalImp, class ModelParam>
class VelocityConstantBase
  : public SpaceFunctionDefault< EvalImp, ModelParam >
{
public:
  //! model parameter type
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! (Vector) function space type
  typedef typename ModelParamType :: VectorFunctionSpaceType         FunctionSpaceType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! domain vector type
  typedef typename FunctionSpaceType :: DomainType                   DomainType;
  //! dimension of the domain vector space
  static const int dimDomain = ModelParamType :: dimDomain;
  //! number of summands in this data function (=dimDomain)
  static const int numComps = dimDomain;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  VelocityConstantBase(const ModelType & model, const GridPartType & gridPart)
  : paramstring_(ParameterHelper::vectorParamString<dim>("rb.velocity")),
    muDependent_(ParameterHelper::isInMuNames(paramstring_))
  {
    for( int i = 0 ; i < dimDomain ; ++i )
    {
      if( i == dim ) {
        vector_[i] = 1.0;
      } else {
        vector_[i] = 0.0;
      }
    }
  }

  virtual ~VelocityConstantBase() {};

  inline void init(const Entity & en)
  {
    en_ = &en;
  }

  inline double coefficient() const
  {
    return Parameter::getValue<double>(paramstring_);
  }

  inline std::string symbolicCoefficient() const
  {
    return paramstring_;
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }

protected:
  Entity const *en_;
  std::string   paramstring_;
  bool          muDependent_;
  RangeType     vector_;
};

/** adds an evaluate method to VelocityConstantBase and thus makes it a full
 * implementation of DataFuncInterface */
template<int dim, class ModelParam>
class VelocityConstant : public VelocityConstantBase<dim, VelocityConstant<dim, ModelParam>, ModelParam>
{
private:
  //! this type
  typedef VelocityConstant< dim, ModelParam >                        ThisType;
  //! base type
  typedef VelocityConstantBase< dim, ThisType, ModelParam >          BaseType;
public:
  //! grid part type
  typedef typename BaseType :: GridPartType                          GridPartType;
  //! range type
  typedef typename BaseType :: RangeType                             RangeType;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  VelocityConstant(const ModelType & model, const GridPartType & gridPart)
    : BaseType(model, gridPart)
  { }

  virtual ~VelocityConstant() {};

  /** @copydoc DataFuncInterface :: evaluate() */
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
    ret = BaseType :: vector_;
  }
};

/**
 * @brief data function implementing a constant velocity vector
 *
 * This class implements
 * @f[ v_{\mbox{dir}}(\mu;x,t)
 *      = \sum_{i=1}^{\mbox{dimDomain}} \sigma_i(\mu) \cdot v_i(x) \f]
 * with
 * @f[ \sigma_i(\mu) = \mu_i
 *     \mbox{for } i=1,\dots,\mbox{dimDomain} @f] and
 * @f[ v_1(x) = e_1 \quad v_2(x) = x_2^2 + 0.25\cdot e_2
 *     \quad v_3(x) = -e_3 @f]
 *
 * List of parameters:
 *  - <tt> rb.c_velocity_{x,y,z} </tt>  velocity component
 *  @f$\mu_{\{1,2,3\}}@f$.
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * <tt> rb.c_velocity_{x,y,z} </tt>
 *
 * @tparam component summand @f$i@f$
 * @tparam ModelParam parameter class describing the model
 */
template<int dim, class ModelParam>
class VelocityFieldParabola : public VelocityConstantBase<dim, VelocityFieldParabola<dim, ModelParam>, ModelParam>
{
private:
  //! this type
  typedef VelocityFieldParabola< dim, ModelParam >                   ThisType;
  //! base type
  typedef VelocityConstantBase< dim, ThisType, ModelParam >          BaseType;
public:
  //! grid part type
  typedef typename BaseType :: GridPartType                          GridPartType;
  //! range vector type
  typedef typename BaseType :: RangeType                             RangeType;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  VelocityFieldParabola(const ModelType & model, const GridPartType & gridPart)
    : BaseType(model, gridPart)
  {
    if(dim == 2)
      BaseType :: vector_[2] = -1.0;
  }

  /** @copydoc DataFuncInterface :: evaluate() */
  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
    ret = BaseType :: vector_;
  }
};

template<class ModelParam>
class VelocityFieldParabola<1, ModelParam> 
  : public VelocityConstantBase<1, VelocityFieldParabola<1, ModelParam>, ModelParam>
{
private:
  typedef VelocityFieldParabola< 1, ModelParam >                     ThisType;
  typedef VelocityConstantBase< 1, ThisType, ModelParam >            BaseType;
  using BaseType :: en_;
public:
  typedef typename BaseType :: GridPartType                          GridPartType;
  typedef typename BaseType :: Entity                                Entity;
  typedef typename BaseType :: RangeType                             RangeType;
  typedef typename BaseType :: DomainType                            DomainType;
public:
  template<class ModelType>
  VelocityFieldParabola(const ModelType & model, const GridPartType & gridPart)
    : BaseType(model, gridPart)
  { }

  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
    const Entity & en = *en_;
    DomainType  globalX = en.geometry().global( coordinate(localX) );

    ret[0] = 0.0;
    ret[1] = globalX[1]*globalX[1]+0.25;
    ret[2] = 0.0;
  }

};

/*@}*/

}  // namespace Data
}  // namespace Lib
} // end of namespace RBFem
} // end of namespace Dune

#endif /* __DATAFUNCVELOCITY_HH_ */

