#ifndef DATAFUNC_HH_

#define DATAFUNC_HH_

#ifdef DUNE_DUCKTYPING
#include "datafuncif.hh"
#endif

#include "velocity.hh"
#include "dirichlet.hh"
#include "diffusion.hh"
#include "initdata.hh"

#endif /* DATAFUNC_HH_ */

