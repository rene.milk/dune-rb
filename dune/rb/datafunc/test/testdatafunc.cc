#define DUNE_DUCKTYPING
//#undef DUNE_DUCKTYPING

#include <config.h>

#include "../../misc/tuples.hh"
#include "../../rbasis/linevol/linevoldescr.hh"
#include "../datafunc.hh"

const char paramfiledir[] = PARAMFILEDIR;

using namespace Dune :: RBFem;
using namespace Dune;

typedef Example::LinEvol::Description< double, 1, 0, Example::LinEvol::TimeDiscretizationEnum
                     :: Explicit, Example::LinEvol::OneD >                             DescrType;

class DataFuncTest
{
public:
    typedef DescrType :: ModelParamType                              ModelParamType;
    typedef DescrType :: ModelType                                   ModelType;
    typedef DescrType :: GridPartType                                GridPartType;
    typedef GridPartType :: GridType                                 GridType;
    typedef GridPtr< GridType >                                      GridPtrType;
public:
    DataFuncTest(const char * filename)
        : gridPtr_(filename),
          grid_(*gridPtr_),
          gridPart_(grid_),
          model_(gridPart_)
    {}

    template<class DataFuncImp>
    int run()
    {
        DataFuncImp dataFunc(model_, gridPart_);
        return 0;
    }

private:
    GridPtrType   gridPtr_;
    GridType     &grid_;
    GridPartType  gridPart_;
    ModelType     model_;
};

int main (int argc, char * argv[])
{
    try {
    MPIManager :: initialize(argc, argv);
    std::string paramfile(std::string(paramfiledir) + std::string("/default"));
    Parameter :: append(paramfile.c_str());

    std::string gridFile;
    Parameter :: get("rb.gridFile", gridFile);
    gridFile = std::string(paramfiledir) + "/" + gridFile;

    DataFuncTest dft(gridFile.c_str());

    typedef DataFuncTest :: ModelParamType                           MPT;

    typedef Lib::Data::VelocityConstant< 0, MPT >                               VC0;
    typedef Lib::Data::VelocityConstant< 1, MPT >                               VC1;
    typedef Lib::Data::VelocityConstant< 2, MPT >                               VC2;

    typedef Lib::Data::VelocityFieldParabola< 0, MPT >                          VF0;
    typedef Lib::Data::VelocityFieldParabola< 1, MPT >                          VF1;
    typedef Lib::Data::VelocityFieldParabola< 2, MPT >                          VF2;

    typedef Lib::Data::InitDataSingleBlob< MPT >                                IDSB;
    typedef Lib::Data::InitDataUnsteadyWave< 0, MPT >                           IDUW0;
    typedef Lib::Data::InitDataUnsteadyWave< 1, MPT >                           IDUW1;

    typedef Lib::Data::DirichletConstant< MPT >                                 DIRC;

    typedef Lib::Data::DiffusionConstantIsotropic< MPT >                        DIFCI;
    int ret = 0;
    ret += dft.run<VC0>();
    ret += dft.run<VC1>();
    ret += dft.run<VC2>();

    ret += dft.run<VF0>();
    ret += dft.run<VF1>();
    ret += dft.run<VF2>();

    ret += dft.run<IDSB>();

    ret += dft.run<IDUW0>();
    ret += dft.run<IDUW1>();

    ret += dft.run<DIRC>();

    ret += dft.run<DIFCI>();

    return ret;
    }
    catch (Dune::Exception & e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return -1;
    }
}

