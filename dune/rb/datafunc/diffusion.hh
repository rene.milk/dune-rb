#ifndef __DATAFUNCDIFFUSION_HH_

#define __DATAFUNCDIFFUSION_HH_

#include "datafuncif.hh"
#include <vector>
#include <string>
#include "../misc/parameter/parameter.hh"

namespace Dune {
namespace RBFem {
namespace Lib {
namespace Data {

/** @addtogroup datafunc */
/*@{*/

/**
 * @brief data function implementing a constant diffusion coefficient
 *
 * This class implements
 * @f[ d(\mu) = \sigma(\mu) \cdot 1 \f]
 * with
 * @f[ \sigma(\mu) = \mu @f]
 *
 * List of parameters:
 *  - <tt> rb.diffusion </tt>  diffusion constant @f$\mu@f$
 *
 * Parameters that can be added to <tt> rb.mu_names </tt>:
 * @rbparam{rb.diffusion, diffusion constant}
 *
 * @tparam ModelParam parameter class describing the model
 */
template<class ModelParam>
class DiffusionConstantIsotropic
 : public SpaceFunctionDefault<DiffusionConstantIsotropic<ModelParam>, ModelParam>
{
public:
  typedef ModelParam                                                 ModelParamType;
  //! entity type
  typedef typename ModelParamType :: Entity                          Entity;
  //! grid part type
  typedef typename ModelParamType :: GridPartType                    GridPartType;
  //! function space type
  typedef typename ModelParamType :: FunctionSpaceType               FunctionSpaceType;
  //! range vector type
  typedef typename FunctionSpaceType :: RangeType                    RangeType;
  //! dimension of domain vector space
  static const int dimDomain = ModelParamType :: dimDomain;
  //! number of summands in data function (=1)
  static const int numComps = 1;
public:
  /** @brief constructor
   *
   * @param model     the underlying model
   * @param gridPart  grid part
   */
  template<class ModelType>
  DiffusionConstantIsotropic(const ModelType & model, const GridPartType & gridPart)
    : muDependent_(false)
  {
    std::vector<std::string> mu_namesList;
    Parameter::get("rb.mu_names", mu_namesList);

    if( std::find(mu_namesList.begin(), mu_namesList.end(), std::string("rb.diffusion"))
        != mu_namesList.end() )
    {
      muDependent_ = true;
    }
  }

  virtual ~DiffusionConstantIsotropic() {};

  template<class PointType>
  void evaluate(const PointType & localX, RangeType & ret) const
  {
    //    DomainType globalX = this->global(localX);
    ret[0] = 1.0;
  }

  inline double coefficient() const
  {
    return Parameter::getValue<double>("rb.diffusion");
  }

  inline std::string symbolicCoefficient() const
  {
    return "rb.diffusion";
  }

  bool isMuDependent() const
  {
    return muDependent_;
  }

private:
  bool          muDependent_;
};

/*@}*/
}  // namespace Data
}  // namespace Lib

} // end of namespace Dune :: RBFem

} // end of namespace Dune

#endif /* __DATAFUNCDIFFUSION_HH_ */

