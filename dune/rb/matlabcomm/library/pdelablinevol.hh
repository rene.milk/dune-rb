/*
 * PdeLabLinEvol.hh
 *
 *  Created on: 13.04.2012
 *      Author: mdroh_01
 */

#ifndef DUNE_RB_MATLABCOMM_LIBRARY_PDELABLINEVOL_HH_
#define DUNE_RB_MATLABCOMM_LIBRARY_PDELABLINEVOL_HH_

#include "ilinevol.hh"
#include <dune/rb/detailed/parameter/vector.hh>
#include <dune/rb/detailed/solver/connector/interface.hh>
#include <dune/rb/offline/generator/reduceddata/interface.hh>
#include <dune/rb/la/separableparametric/icontainer.hh>
#include <dune/rb/backend/fulleigenmatrix.hh>
#include <dune/rb/detailed/parameter/nullsample.hh>
#include <dune/rb/offline/space/rbdefault.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Library
{

enum DofModeEnum
{
  NoCopy, CopyAll
};

/*
 *
 */
template< class ServerTraits, class ProblemTraits >
class PdeLabLinEvol: public Dune::RB::MatlabComm::Library::ILinEvol<
    ServerTraits >
{
private:

  typedef PdeLabLinEvol< ServerTraits, ProblemTraits > ThisType;

  typedef MatlabComm::Server::Interface< typename ServerTraits::ServerType > ServerType;
  typedef MatlabComm::SerializedArray::IFactory<
      typename ServerTraits::ArrayFactory > ArrayFactory;
  typedef MatlabComm::Server::Args Args;

  typedef typename ProblemTraits::DetailedTraits DetailedTraits;

  typedef Dune::RB::Backend::FullEigenMatrix FullMatrixBackendType;

  typedef Offline::Space::RBDefault OfflineSpaceType;

  typedef typename ProblemTraits::OfflineDataGeneratorType::BasicTraits OfflineMatrixTraits;

#ifdef ECLIPSE
  typedef Offline::Generator::ReducedData::Interface< OfflineMatrixTraits,
      typename ProblemTraits::OfflineDataGeneratorType > OfflineDataType;
#else
  typedef typename ProblemTraits::OfflineDataGeneratorType OfflineDataType;
#endif
  typedef Detailed::Solver::Connector::Interface< DetailedTraits,
      typename ProblemTraits::DetailedConnectorType > DetailedConnectorType;
  typedef LA::SeparableParametric::IContainer<
      typename DetailedTraits::RangeFieldType,
      typename DetailedTraits::MatrixType > SeparableMatrixType;
  typedef LA::SeparableParametric::IContainer<
      typename DetailedTraits::RangeFieldType,
      typename DetailedTraits::VectorType > SeparableVectorType;
public:
  PdeLabLinEvol (DetailedConnectorType & detailedConnector);

  virtual ~PdeLabLinEvol ()
  {
    if (paramSample_ != NULL) delete paramSample_;
  }

  virtual void clearReducedSpaceFiles ()
  {
    std::cerr << "We need to implement the file clear ReducedSpaceFiles()"
      << std::endl;
    exit(-2);
  }

  virtual bool mexFunction (const std::string operation, Args::LArgType & alhs,
                            const Args::RArgType & arhs);

  virtual void detailedSimulation (Args::ArrayType *& sim_data, bool visualize,
                                   bool copy);

//  virtual void expandRB (const Args::ArrayType & dofVec,
//                         Args::ArrayType * &ret);

  virtual void genModelData (Args::ArrayType *& model_data);

  virtual void getMu (Args::ArrayType *& mu_values);

  virtual void getRBSize (Args::ArrayType *& rbSize);

  virtual int initReducedBasis ();

  virtual void initModel (Args::ArrayType *& retArray,
                          const std::string& rbPath);

  virtual void isValidRBSpace (Args::ArrayType *& retArray);

  virtual void rbInitValues (const int decomp_mode, Args::ArrayType * & a0);

  virtual void rbOperators (const int decomp_mode,
                            const std::vector< std::string >& opIds,
                            const std::vector< std::string > & funcIds,
                            Args::ArrayType *& operators);

  virtual void rbSymbolicCoefficients (
      const std::vector< std::string >& opIds,
      const std::vector< std::string >& funcIds, Args::ArrayType *& symCoeffs);

  virtual void rbReconstruction (const Args::ArrayType & detailed_data,
                                 const Args::ArrayType & coefficients,
                                 Args::ArrayType *& ret);

  virtual int rbExtensionPCA (const Args::ArrayType & mu, const int k,
                              double percentage = 0.98);

  virtual void setMu (const Args::ArrayType & mu_values);

  const std::string getSimulationPrefix ()
  {
    return "simulation";
  }

  const std::string getReconstructionPrefix ()
  {
    return "reconstruction";
  }

private:
  DetailedConnectorType & detailedConnector_;
  OfflineSpaceType space_;
  OfflineDataType offlineData_;
  Detailed::Parameter::NullSample * paramSample_;
  Detailed::Parameter::Vector currentMu_;
};

} /* namespace Library */
} /* namespace MatlabComm */
} /* namespace RB */
} /* namespace Dune */

#endif /* DUNE_RB_MATLABCOMM_LIBRARY_PDELABLINEVOL_HH_ */
