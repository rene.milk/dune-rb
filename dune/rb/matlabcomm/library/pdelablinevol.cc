/*
 * pdelablinevol.cc
 *
 *  Created on: 13.04.2012
 *      Author: mdroh_01
 */

#include <config.h>
#include <boost/filesystem.hpp>
#include <dune/rb/runtime_error.hh>
#include "pdelablinevol.hh"
#include <dune/rb/rbasis/twophaseflow/algorithms/pca.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Library
{
template< class ServerTraits, class ProblemTraits >
PdeLabLinEvol< ServerTraits, ProblemTraits >::PdeLabLinEvol (
    DetailedConnectorType & connector)
    : detailedConnector_(connector),
      space_(),
      offlineData_(detailedConnector_, space_),
      paramSample_(
          new Detailed::Parameter::NullSample(
              detailedConnector_.getModelParameterTree()))
{
}

template< class ServerTraits, class ProblemTraits >
bool PdeLabLinEvol< ServerTraits, ProblemTraits >::mexFunction (
    const std::string operation, Args::LArgType& alhs,
    const Args::RArgType& arhs)
{
//  if (operation.compare( "specific_operation" ) == 0)
//  {
//    specific_operation( *arhs[1] );
//    return true;
//  }

  return false;
}

template< class ServerTraits, class ProblemTraits >
int PdeLabLinEvol< ServerTraits, ProblemTraits >::initReducedBasis ()
{
 #ifdef ECLIPSE
   typedef Eigen::VectorXd DVT;
   typedef Eigen::MatrixXd OMT;
 #else
   typedef typename DetailedTraits::VectorContainerType DVT;
   typedef typename OfflineMatrixTraits::MatrixType OMT;
 #endif

   const typename DetailedTraits::GFS & gfs = detailedConnector_
     .getGridFunctionSpace();
   DVT simulation(gfs);
   simulation = 1;
   space_.addDofMatrix(simulation);
   return 1;
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::rbInitValues (
    const int decomp_mode, Args::ArrayType * & a0)
{
  switch (decomp_mode)
  {
  case 2:
    a0 = ArrayFactory::createMatrix(0, 0);
    break;
  case 1:
    a0 = ArrayFactory::createCell(0, 0);
    break;
  default:
    throw(Dune::RB::RuntimeError(
        "in MatlabComm::Library::PdeLabLinEvol:\nrbInitValues() not yet implemented for decomp mode 0!\n"));
    break;
  }
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::detailedSimulation (
    Args::ArrayType*& ret, bool visualize = false, bool copy = false)
{
#ifdef ECLIPSE
  typedef Eigen::MatrixXd VCT;
#else
  typedef typename DetailedTraits::VectorContainerType VCT;
#endif
  const typename DetailedTraits::GFS & gfs = detailedConnector_
    .getGridFunctionSpace();

  VCT simulation(gfs);
  detailedConnector_.solve(currentMu_, simulation);
  std::vector< std::string > extra_fields;
  extra_fields.push_back("size");
  if (copy)
  {
    extra_fields.push_back("U");
  }
  if (visualize)
  {
    const std::string cellArrayName = "Simulation";
    detailedConnector_.visualize(simulation, getSimulationPrefix(),
        cellArrayName);

    this->fillStructWithOutputData(ret, cellArrayName, getSimulationPrefix(),
        detailedConnector_.getVtkPath(), extra_fields);
  }
  else
    ret = ArrayFactory::createStruct(extra_fields);

  if (copy)
  {
    int U_fnr = ret->getFieldNumber("U");
    SerializedArray::CMatrixWrapper * cmatrix;
    FullMatrixBackendType::wrapCMatrix(simulation, cmatrix);
    Args::ArrayType * U = ArrayFactory::createMatrix(cmatrix);
    ret->setField(U_fnr, U);
  }
  int size_fnr = ret->getFieldNumber("size");
  ret->setField(size_fnr, ArrayFactory::createScalar(simulation.rows()));
}

template< class ServerTraits, class ProblemTraits >
inline void PdeLabLinEvol< ServerTraits, ProblemTraits >::getMu (
    Args::ArrayType*& mu_values)
{
  const std::vector< std::string > & muNames = paramSample_->getMuNames();
  mu_values = ArrayFactory::createMatrix(1, muNames.size());
  typename Args::ArrayType::CMatrixType & cmatrix = mu_values->getMatrix();
  for (unsigned int i = 0; i < muNames.size(); ++i)
  {
    cmatrix(0, i) = currentMu_.getValue(muNames[i]);
  }
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::setMu (
    const Args::ArrayType& mu_values)
{
  currentMu_ = paramSample_->fromMatlab(mu_values);
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::genModelData (
    Args::ArrayType *& model_data)
{
  std::cout << "Initializing Detailed::Solver::Connector" << std::endl;

  detailedConnector_.init();
  const SeparableMatrixType &separableMatrix = detailedConnector_
    .getSystemMatrix("L2");
  std::vector< std::string > fieldNames;
  fieldNames.push_back("W");
  model_data = ArrayFactory::createStruct(fieldNames);
  model_data->setField(0,
      ArrayFactory::createString(separableMatrix.getIdentifier()));

}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::getRBSize (
    Args::ArrayType *& rbSize)
{
  const unsigned int rbSizeTmp = space_.size();
  rbSize = ArrayFactory::createScalar(rbSizeTmp);
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::initModel (
    Args::ArrayType *& retArray, const std::string & rbPath = "")
{

  if (!rbPath.empty())
  {
    space_.load(rbPath);
  }

  if (!detailedConnector_.isInitialized()) detailedConnector_.init();

  std::vector< std::string > fieldNames;
  fieldNames.push_back("mu_names");
  fieldNames.push_back("mu_ranges");
  fieldNames.push_back("T");
  fieldNames.push_back("nt");
  fieldNames.push_back("cwd");

  retArray = ArrayFactory::createStruct(fieldNames);
  retArray->setField(0,
      ArrayFactory::createCellOfStrings(paramSample_->getMuNames()));
  std::vector<SerializedArray::CMatrixWrapper *> muRanges = paramSample_->getMuRanges();
  retArray->setField(1,
      ArrayFactory::createCellOfMatrices(muRanges));
  retArray->setField(2, ArrayFactory::createScalar(0));
  retArray->setField(3, ArrayFactory::createScalar(1));
  retArray->setField(4,
      ArrayFactory::createString(boost::filesystem::initial_path().string()));
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::isValidRBSpace (
    Args::ArrayType *& retArray)
{
  bool isValid = space_.isValid();

  retArray = ArrayFactory::createScalar(isValid);
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::rbReconstruction (
    const Args::ArrayType & detailed_data, const Args::ArrayType & coefficients,
    Args::ArrayType *& ret)
{
  assert(detailed_data.isA(SerializedArray::ArrayEnum::Struct));

  // TODO: add reduced basis id handling
//  typename Args::ArrayType::IndexType RB_id_fnr = detailed_data.getFieldNumber(
//      "RB_id");
//  if (RB_id_fnr == detailed_data.getNumberOfFields())
//    ServerType::printErrMsg(
//        "in PdeLabLinEvol::rbReconstruction : missing field RB_id in second argument");

  typename Args::ArrayType::IndexType dof_mode_fnr = detailed_data
    .getFieldNumber("dof_mode");
  enum DofModeEnum dofMode;
  if (dof_mode_fnr == detailed_data.getNumberOfFields())
    dofMode = NoCopy;
  else
  {
    std::string dof_mode_string;
    detailed_data.getField(dof_mode_fnr).getString(dof_mode_string);
    if (dof_mode_string == "no_copy")
      dofMode = NoCopy;
    else if (dof_mode_string == "copy" || dof_mode_string == "copy_all")
      dofMode = CopyAll;
  }
// TODO: Add RB id handling
//  std::string RB_id;
//  detailed_data.getField(RB_id_fnr).getString(RB_id);
//  if (RB_id != space_.getIdentifier())
//  {
//    std::ostringstream oss;
//    oss << "Wrong RB identifier: requested RB identifier " << RB_id
//      << " != existent RB identifier " << space_.getIdentifier();
//    ServerType::printErrMsg(oss.str());
//  }

  assert(coefficients.isA(SerializedArray::ArrayEnum::Matrix));

  std::vector< std::string > extra_fields;
  if (dofMode == CopyAll)
  {
    extra_fields.push_back("U");
  }

#ifdef ECLIPSE
  typedef Eigen::VectorXd VCT;
#else
  typedef typename DetailedTraits::VectorContainerType VCT;
#endif
  const typename DetailedTraits::GFS & gfs = detailedConnector_
    .getGridFunctionSpace();
  VCT reconstruction(gfs);

  space_.reconstruct(coefficients.getMatrix(), reconstruction);

  const std::string cellArrayName = "Reconstruction";
  detailedConnector_.visualize(reconstruction, getReconstructionPrefix(),
      cellArrayName);

  this->fillStructWithOutputData(ret, cellArrayName, getReconstructionPrefix(),
      detailedConnector_.getVtkPath(), extra_fields);

  if (dofMode == CopyAll)
  {
    int U_fnr = ret->getFieldNumber("U");
    MatlabComm::SerializedArray::CMatrixWrapper * cmatrix;
    FullMatrixBackendType::wrapCMatrix(reconstruction, cmatrix);
    Args::ArrayType * U = ArrayFactory::createMatrix(cmatrix);
    ret->setField(U_fnr, U);
  }
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::rbOperators (
    const int decomp_mode, const std::vector< std::string > & opIds,
    const std::vector< std::string > & funcIds, Args::ArrayType *& operators)
{
  std::vector< std::string > fields(opIds);
  for (unsigned int i = 0; i < funcIds.size(); ++i)
    fields.push_back(funcIds[i]);
  operators = ArrayFactory::createStruct(fields);

  switch (decomp_mode)
  {
  case 2:
    for (unsigned int i = 0; i < opIds.size(); ++i)
    {
      const SeparableMatrixType& systemMatrix = detailedConnector_
        .getSystemMatrix(opIds[i]);
      std::vector< double > coefficients = systemMatrix.coefficients(
          currentMu_);
      Args::ArrayType * sigmaVector = ArrayFactory::createVector(coefficients);
      operators->setField(i, sigmaVector);
    }
    for (unsigned int i = 0; i < funcIds.size(); ++i)
    {
      const SeparableVectorType& systemVector = detailedConnector_
        .getSystemVector(funcIds[i]);
      std::vector< double > coefficients = systemVector.coefficients(
          currentMu_);
      Args::ArrayType * sigmaVector = ArrayFactory::createVector(coefficients);
      operators->setField(i + opIds.size(), sigmaVector);
    }
    break;
  case 1:
    for (unsigned int i = 0; i < opIds.size(); ++i)
    {
      typedef typename OfflineDataType::MatrixListType OML;
      OML & reducedGramianList = offlineData_.getOfflineMatrix(opIds[i]);
      std::vector< SerializedArray::CMatrixWrapper* > reducedGramianListTmp;
      FullMatrixBackendType::wrapCMatrix(reducedGramianList,
          reducedGramianListTmp);
      Args::ArrayType * reducedGramianCell = ArrayFactory::createCellOfMatrices(
          reducedGramianListTmp);
      operators->setField(i, reducedGramianCell);
    }
    if (!funcIds.empty())
    {
      ServerType::printWarnMsg(
          "In rbOperators: Third argument should be empty for decomposition mode == 1");
    }
    break;
  case 0:
    ServerType::printErrMsg("Not yet implemented!!!\n");
//    for (int i = 0; i < opIds.size(); ++i)
//    {
//      typedef typename SeparableMatrixType::MatrixType SMM;
//      const SeparableMatrixType & systemMatrix = detailedConnector_.getSystemMatrix(
//          opIds[i]);
//      const SMM & smComplete = systemMatrix.complete(currentMu_);
//
//      Args::ArrayType * completeMatrix = ArrayFactory::createMatrix(*smComplete);
//      operators->setField(i, completeMatrix);
//    }
    break;
  default:
    ServerType::printErrMsg(
        "Wrong decomposition mode! Must be one of 0(complete), 1(component) or 2(coefficient)");
    break;
  }
}

template< class ServerTraits, class ProblemTraits >
void PdeLabLinEvol< ServerTraits, ProblemTraits >::rbSymbolicCoefficients (
    const std::vector< std::string > & opIds,
    const std::vector< std::string > & funcIds, Args::ArrayType *& symCoeffs)
{
  std::vector< std::string > ids = opIds;
  for (unsigned int i = 0; i < funcIds.size(); ++i)
  {
    ids.push_back(funcIds[i]);
  }
  symCoeffs = ArrayFactory::createStruct(ids);

  for (unsigned int i = 0; i < opIds.size(); ++i)
  {
    const SeparableMatrixType & systemMatrix = detailedConnector_
      .getSystemMatrix(opIds[i]);
    std::vector< std::string > symbolicCoefficients = systemMatrix
      .symbolicCoefficients();
    for (unsigned int j = 0; j < symbolicCoefficients.size(); ++j)
    {
      paramSample_->postprocessSymbolicCoefficient(symbolicCoefficients[j]);
    }
    Args::ArrayType * symbolicCoefficientCell =
      ArrayFactory::createCellOfStrings(symbolicCoefficients);
    symCoeffs->setField(i, symbolicCoefficientCell);
  }
  for (unsigned int i = 0; i < funcIds.size(); ++i)
  {
    const SeparableVectorType & sv = detailedConnector_.getSystemVector(
        funcIds[i]);
    std::vector< std::string > symbolicCoefficients = sv.symbolicCoefficients();
    for (unsigned int j = 0; j < symbolicCoefficients.size(); ++j)
    {
      paramSample_->postprocessSymbolicCoefficient(symbolicCoefficients[j]);
    }
    Args::ArrayType * symbolicCoefficientCell =
      ArrayFactory::createCellOfStrings(symbolicCoefficients);
    symCoeffs->setField(i + opIds.size(), symbolicCoefficientCell);
  }
}

template< class ServerTraits, class ProblemTraits >
int PdeLabLinEvol< ServerTraits, ProblemTraits >::rbExtensionPCA (
    const Args::ArrayType & mu, const int k, double percentage)
{
  Detailed::Parameter::Vector param = paramSample_->fromMatlab(mu);

#ifdef ECLIPSE
  typedef Eigen::VectorXd DVT;
  typedef Eigen::MatrixXd OMT;
#else
  typedef typename DetailedTraits::VectorContainerType DVT;
  typedef typename OfflineMatrixTraits::MatrixType OMT;
#endif

  const typename DetailedTraits::GFS & gfs = detailedConnector_
    .getGridFunctionSpace();
  DVT simulation(gfs);
  detailedConnector_.solve(param, simulation);

  Dune::RB::PCAEigen< DVT > pca(simulation);
  pca.computeCovarianceMatrix();
  pca.computeEigenvalues();
  int numberOfComponents = pca.computeNeededNumberByPercentage(percentage);
  numberOfComponents = std::min(k, numberOfComponents);
  OMT principalComponents;
  pca.computePrincipalComponents(principalComponents);

  space_.addDofMatrix(principalComponents);
  offlineData_.update(space_.size()-principalComponents.cols(), space_.size());
  return principalComponents.cols();
}

} /* namespace Library */
} /* namespace MatlabComm */
} /* namespace RB */
} /* namespace Dune */
