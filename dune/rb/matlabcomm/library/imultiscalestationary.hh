/*
 * imultiscalestationary.hh
 *
 *  Created on: 16.03.2012
 *      Author: martin
 */

#ifndef DUNE_RB_MATLABCOMM_LIBRARY_IMULTISCALESTATIONARY_HH_
#define DUNE_RB_MATLABCOMM_LIBRARY_IMULTISCALESTATIONARY_HH_

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Library
{

/*
 *
 */
template<class Traits>
class IMultiScaleStationary
{
private:
  typedef IMultiScaleStationary< Traits >                                 ThisType;

  typedef MatlabComm::Server::Interface<typename Traits :: ServerType> ServerType;
  typedef MatlabComm::SerializedArray::IFactory<typename Traits::ArrayFactory> ArrayFactory;
  typedef MatlabComm::Server::Args Args;
public:
  IMultiScaleStationary ();
  virtual ~IMultiScaleStationary ();

  /**
     * @brief local entry point for calls from RBMatlab
     *
     * This method is called by the program facade (probably LinEvolFacade), when
     * the Matlab user requested an operation that is not registered in this
     * facades.  This allows us to add non-standard and more specific operations
     * here.
     *
     * @param[in] operation  the name of the operation the user requested in
     *                       RBmatlab
     * @param[out] alhs      vector of ArrayType arguments (lhs)
     * @param[in] arhs       vector of ArrayType arguments (rhs)
     *
     * @return               boolean indicating wether the requested operation is
     *                       known to this class.
     */
    virtual bool mexFunction( const std::string operation, Args::LArgType & alhs, const Args::RArgType & arhs)
    {
      return true;
    }

    /**
     * @brief runs a detailed simulation of the numerical scheme for parameters
     * that need to be set beforehand via the set_mu method.
     * The solution is written to U, such that it is readable by
     * RBMatlab.
     *
     * @param[in] mu           parameter
     * @param[out] sim_data    reference to the structure holding data about the
     *                         detailed simulation
     */
    virtual void detailedSimulation( double mu, Args::ArrayType * &sim_data) = 0;

    /** @brief generate model data
     *
     * param[out] model_data       structure with model_data information
     */
    virtual void genModelData(Args::ArrayType *& model_data) = 0;

    /** @brief reset reduced basis space
     *
     */
    virtual void resetRB() = 0;

    virtual int getRBSize() = 0;

    virtual int initReducedBasisSpace(double mu) = 0;

    virtual void initModel(Args::ArrayType *& model, const std::string & rbPath) = 0;

    virtual bool isValidRB() = 0;

    virtual void rbComponents(Args::ArrayType *& operator_data) = 0;

    virtual void rbSymbolicCoefficients(Args::ArrayType *& symbolicCoefficients) = 0;

    virtual void rbReconstruction(const Args::ArrayType *& lowDimDofs, Args::ArrayType *& rb_recons_data);
};

} /* namespace Library */
} /* namespace MatlabComm */
} /* namespace RB */
} /* namespace Dune */
#endif /* IMULTISCALESTATIONARY_HH_ */
