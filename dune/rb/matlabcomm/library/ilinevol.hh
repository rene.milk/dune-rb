#ifndef DUNE_RB_MATLABCOMM_LIBRARY_ILINEVOL_HH__
#define DUNE_RB_MATLABCOMM_LIBRARY_ILINEVOL_HH__

#if HAVE_GRAPE
#include <dune/grid/io/visual/grapedatadisplay.hh>
#endif
#include  <cstdlib>
#include <unistd.h>

#include <dune/rb/matlabcomm/server/interface.hh>
#include <dune/rb/matlabcomm/serializedarray/interface.hh>

//#define RB_DEBUG
//
namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Library
{

/** \class   LinEvolInterface
 *  \ingroup linevol
 *  @brief   Interface for the implementation of a linear evolution problem.
 *
 *  An implementation for a dune-rb/RBmatlab for a linear evolution problem.
 *  It can be used with LinEvolFacade.
 *
 */
template< class Traits >
class ILinEvol
{
private:
  typedef ILinEvol< Traits > ThisType;

  typedef MatlabComm::Server::Interface< typename Traits::ServerType > ServerType;
  typedef MatlabComm::SerializedArray::IFactory< typename Traits::ArrayFactory > ArrayFactory;
  typedef MatlabComm::Server::Args Args;

public:
  /** constructor
   */
  ILinEvol ()
  {
  }

  /** destructor
   */
  virtual ~ILinEvol ()
  {
  }

private:
  //Prohibit copying and assignment
  inline ILinEvol (const ThisType &);
  inline ThisType & operator= (const ThisType &);

protected:

  /** @brief fills an Dune::RB::MatlabComm::SerializedArray::Interface structure with information
   *  necessary to visualize vtk output data
   *
   *  @param mxReturn       the structure which is filled with the data
   *  @param cellArrayName  cell array name of the vtk data which shall be visualized
   *  @param prefix         prefix name of the vtk data files
   *  @param path           path to the vtk data files
   *  */

  void fillStructWithOutputData (Args::ArrayType * & mxReturn,
                                 std::string cellArrayName, std::string prefix =
                                   "solution",
                                 std::string path = "",
                                 std::vector< std::string > extraFields =
                                   std::vector< std::string >())
  {
    std::vector< std::string > fieldNames;
    fieldNames.push_back("outdir");
    fieldNames.push_back("prefix");
    fieldNames.push_back("cell_array_name");

    typedef std::vector< std::string >::const_iterator VSCI;
    for (VSCI sit = extraFields.begin(); sit != extraFields.end(); ++sit)
    {
      fieldNames.push_back(*sit);
    }
    mxReturn = ArrayFactory::createStruct(fieldNames);

    if (path.empty())
    {
      char *pathbuf;
      pathbuf = getcwd(NULL, 0);
      path = std::string(pathbuf) + "/output";
      free(pathbuf);
    }
    mxReturn->setField(0, ArrayFactory::createString(path));
    if (prefix.empty())
      prefix = "solution";

    mxReturn->setField(1, ArrayFactory::createString(prefix));
    mxReturn->setField(2, ArrayFactory::createString(cellArrayName));
  }

public:

  /** @brief Delete the files, written by
   the reduced basis space, from disk.
   */
  virtual void clearReducedSpaceFiles () = 0;

  /**
   * @brief local entry point for calls from RBMatlab
   *
   * This method is called by the program facade (probably LinEvolFacade), when
   * the Matlab user requested an operation that is not registered in this
   * facades.  This allows us to add non-standard and more specific operations
   * here.
   *
   * @param[in] operation  the name of the operation the user requested in
   *                       RBmatlab
   * @param[out] alhs      vector of ArrayType arguments (lhs)
   * @param[in] arhs       vector of ArrayType arguments (rhs)
   *
   * @return               boolean indicating wether the requested operation is
   *                       known to this class.
   */
  virtual bool mexFunction (const std::string operation, Args::LArgType & alhs,
                            const Args::RArgType & arhs) = 0;

  /**
   * @brief runs a detailed simulation of the numerical scheme for parameters
   * that need to be set beforehand via the set_mu method.
   * The solution is written to U, such that it is readable by
   * RBMatlab.
   *
   * @param[out] U      reference to the DoFs of the DiscreteFunction
   */
  virtual void detailedSimulation (Args::ArrayType * &sim_data, bool visualize,
                                   bool copy) = 0;

  /**
   * @brief expand current reduced basis with a given discrete function
   *
   * @param[in] dofVec a matlab dof vec, to be stored in basis as discrete function
   * @param[out] ret give back the new reduced basis
   */
//  virtual void expandRB (const Args::ArrayType & dofVec,
//                         Args::ArrayType * &ret) = 0;

  /** @brief generate model data
   *
   * This computes the scalar products \f$ \langle\phi_i | \phi_i\rangle\f$ where \f$\phi_i\f$
   * is the i'th basis function of the discrete function space. The values are saved internally
   * and used e.g. for compunting the scalar product of two functions.
   *
   * param[out] W        mass of one cell, can be used for scalar products...
   *
   */
  virtual void genModelData (Args::ArrayType *& W) = 0;

  /**
   * @brief returns a vector of parameter values corresponding to the values in
   * mu_names_
   *
   * @param[out] mu_values      ArrayType holding mu values
   */
  virtual void getMu (Args::ArrayType *& mu_values) = 0;

  /** @brief get the current size of the reduced basis space
   *
   *  @return returns the number of base functions in the reduced space
   */
  virtual void getRBSize (Args::ArrayType *& rbSize) = 0;

  /** @brief initialize the reduced basis with the data function \f$u_0\f$
   *
   *  each component, that is the space-dependent part, of u0 is added as
   *  a function to the reduced basis space
   *
   *  @return the number of generated basis functions
   */
  virtual int initReducedBasis () = 0;

  /**
   * @brief needs to be run at the beginning of the program in order to
   * initialize the grid and the Dune::Parameter singleton
   */
  virtual void initModel (Args::ArrayType *& retArray, const std::string&) = 0;

  /**
   * @brief checks whether a valid reduced basis space has been loaded
   */
  virtual void isValidRBSpace (Args::ArrayType *& retArray) = 0;

  /**
   * @brief projects the initial values onto the reduced basis space
   *
   * @param[in] decomp_mode determines whether the complete function, only
   *                        the components or only the coefficients are
   *                        projected
   * @param[out] a0         an mxArray where the projected values will be written
   */
  virtual void rbInitValues (const int decomp_mode, Args::ArrayType * & a0) = 0;

  /**
   * @brief returns offline operators that can be used by rb_simulation in RBmatlab.
   * This method can be seen to be the equivalent of gen_online_data in RBmatlab.
   *
   * @param[in]  decomp_mode integer indicating wheter the complete operator (0), only
   * @param[out] operators   online data (independent of H)
   *                         the operator components (1) or only the coefficient vector
   *                         shall be returned.
   */
  virtual void rbOperators (const int decomp_mode,
                            const std::vector< std::string >& opIds,
                            const std::vector< std::string >& funcIds,
                            Args::ArrayType *& operators) = 0;

  /**
   * @brief returns symbolic coefficient strings for offline operators
   *
   * This method can be seen as a substitute for rbOperators() in
   * decomp_mode = 'coefficients'.
   *
   * @param  symCoeffs   cell structure holding the symbolic coefficient
   *                     strings
   */
  virtual void rbSymbolicCoefficients (const std::vector< std::string >& opIds,
                                       const std::vector< std::string >& funcIds,
                                       Args::ArrayType *& symCoeffs) = 0;

  /** @brief reconstructs a reduced simulation in the high dimensional function
   * space @f$W_H\f$.
   *
   * @param detailed_data       Matlab struct representing the current reduced
   *                            basis space.
   * @param coefficients        coefficient vectors @f$\mathbf{a}^k@f$ for some
   *                            time steps @f$k@f$, that ought to be
   *                            reconstructed.
   * @param ret                 matlab array vector with dofs of reconstructed
   *                            function
   */
  virtual void rbReconstruction (const Args::ArrayType & detailed_data,
                                 const Args::ArrayType & coefficients,
                                 Args::ArrayType *& ret) = 0;

  /** @brief extend the reduced space
   *
   *  Given a vector of parameter values, this function computes the high dimensional
   *  solutions to these values and, using the pca algorithm, chooses one function
   *  which is then added to the reduced basis.
   *
   *  @param[in] mu an mxArray* of parameter values
   *  @param[in] k  parameter defining the number of functions to be added to
   *                the space
   *  @param[in] percentage optional parameter defining the percentage of total
   *                variance covered by the span of the principal components
   *                (only used in case of k==-1)
   *  @return number of added principal components
   */
  virtual int rbExtensionPCA (const Args::ArrayType & mu, const int k,
                              double percentage = 0.98) = 0;

  /**
   * @brief replaces the values for parameter keys given through the vector
   *        in the private variable mu_names_ by values from vector mu_values
   *
   * @param[in] mu_values the parameter vector \f$\mu\f$.
   */
  virtual void setMu (const Args::ArrayType & mu_values) = 0;

protected:




};
// end of LinEvolInterface

}// end of namespace Library
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* end of include guard: LINEVOLIF_1MUKMNSJ */

/* vim: set sw=2 et: */
