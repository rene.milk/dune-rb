#ifndef DUNE_RB_MATLABCOMM_CLIENT_HH_
#define DUNE_RB_MATLABCOMM_CLIENT_HH_

#include <dune/rb/matlabcomm/sockets.hh>

#include <netdb.h>
#include <arpa/inet.h>

#include <sstream>
#include <string>
#include <vector>

#include <dune/rb/matlabcomm/serializedarray/mx.hh>
#include <dune/rb/matlabcomm/serializedarray/interface.hh>
#include <dune/rb/matlabcomm/server/interface.hh>

#define char16_t char16_t_backup
// exception handling
#include "mclcppclass.h"
#undef char16_t

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Client
{

/** @ingroup rbsocks
 * @brief class for client communication on the RBmatlab side
 *
 * This class implements the client functionality of the shared library @link
 * mexclient.cc @c mexclient @endlink used for socket communication between
 * RBmatlab and dune-rb.
 */
class Sockets: public MatlabComm::Sockets::Base
{
public:

  typedef MatlabComm::Server::Args Args;

  typedef MatlabComm::SerializedArray::IFactory<
      MatlabComm::SerializedArray::MXArrayFactory > ArrayFactory;
public:
  //! constructor initializing the socket connection
  //!
  //! @param serverhost  server host name or ip address
  //! @param port        port for socket connection
  Sockets (const std::string &serverhost, const std::string port =
             MatlabComm::Sockets::Base::defaultPort())
      : MatlabComm::Sockets::Base(uninitialized), sockfd_(-1)
  {
    struct addrinfo hints, *clientinfo, *p;

    memset(&hints, 0, sizeof hints);
    /* allow IPV4 and IPV6 connections */
    hints.ai_family = AF_UNSPEC;
    /* use stream connections */
    hints.ai_socktype = SOCK_STREAM;

    /* set full clientinfo with hints, ip address argv[1] and port number */
    int rv = getaddrinfo(serverhost.c_str(), port.c_str(), &hints, &clientinfo);
    if (rv != 0)
    {
      std::ostringstream oss;
      oss << "getaddrinfo: " << gai_strerror(rv) << "\n";
      printErrMsg(oss.str());
      return;
    }

    // loop through all the results and connect to the first we can
    for (p = clientinfo; p != NULL; p = p->ai_next)
    {
      sockfd_ = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if (sockfd_ == -1)
      {
        perror("client: socket");
        continue;
      }

      sockState_ = readyforconnection;

      /* connect to an accepting connection */
      int rv = connect(sockfd_, p->ai_addr, p->ai_addrlen);
      if (rv == -1)
      {
        close(sockfd_);
        perror("client: connect");
        char s[INET6_ADDRSTRLEN];
        inet_ntop(p->ai_family,
            get_in_addr(reinterpret_cast< struct sockaddr * >(p->ai_addr)), s,
            sizeof(s));
        std::ostringstream oss;
        oss << "connection to " << s << " failed\n";
        printWarnMsg(oss.str());
        continue;
      }

      break;
    }

    if (p == NULL)
    {
      throw(MatlabComm::Sockets::Error("client: failed to connect\n"));
    }

    sockState_ = connected;

    char s[INET6_ADDRSTRLEN];
    inet_ntop(p->ai_family,
        get_in_addr(reinterpret_cast< struct sockaddr * >(p->ai_addr)), s,
        sizeof(s));
    std::ostringstream oss;
    oss << "client connected to " << s << "\n";
    printStatusMsg(oss.str());

    /* all done with this structure */
    freeaddrinfo(clientinfo);
  }
  ;

  /** destructor closing the socket connection */
  ~Sockets ()
  {
    if (sockState_ == connected)
    {
      printStatusMsg("Closing connection to server!\n");
      close(sockfd_);
    }
  }

  /** @brief indicates whether the client is connected to a DUNE server
   */
  bool isConnected ()
  {
    return (sockState_ == connected);
  }

  /**
   * @brief communicates the arguments given on the RBmatlab side
   *
   * This function serializes the arguments given in RBmatlab and sends it over
   * the socket connection to the server and de-serializes the returned lhs
   * arguments.
   *
   * @param nlhs  number of lhs arguments
   * @param plhs  lhs arguments
   * @param nrhs  number of rhs arguments
   * @param prhs  rhs arguments
   */
  void communicate_mexFunction (int nlhs, mxArray * plhs[], int nrhs,
                                const mxArray * prhs[])
  {
    assert( sockState_ == connected);

    Args::LArgType alhs(nlhs);
    Args::RArgType arhs(nrhs);

    try
    {
      for (size_t i = 0; i < arhs.size(); ++i)
      {
        const ArrayFactory::InterfaceType *& mxaPtr =
          arhs[i];
        mxaPtr = SerializedArray::MXArray::wrapMXArray(prhs[i]);
        if (mxaPtr == NULL)
        {
          std::ostringstream oss;
          oss << "Could not handle argument " << i
            << " which has an unsupported mxArrayclass id.";
          printErrMsg(oss.str());
        }
      }

      std::string operation;
      arhs[0]->getString(operation);

      char command;

      /*      printStatusMsg( std::string("Trying to communicate the operation ")
       *                      + command + std::string(" to DUNE server...\n") );*/

      if (operation == "close")
      {
        command = MatlabComm::Sockets::Protocol::CloseConnection;
        send_chunk(sockfd_, command, "command: close");
      }
      else if (operation == "CLOSE")
      {
        command = MatlabComm::Sockets::Protocol::CloseServer;
        send_chunk(sockfd_, command, "command: CLOSE");
      }
      else
      {
        command = MatlabComm::Sockets::Protocol::CallMexFunction;
        send_chunk(sockfd_, command, "command");

        send_mexFunctionArgs(nlhs, arhs);

        clearArg(arhs);

        /*        printStatusMsg( std::string("DUNE server working on ") + command + std::string("...\n") );*/

        recv_runtimeMessages();

        recv_mexFunctionReturn(alhs);
        for (size_t i = 0; i < (unsigned) nlhs; ++i)
        {
          plhs[i] = dynamic_cast< SerializedArray::MXArray * >(alhs[i])
            ->getMxPtr();
          assert( alhs[i] != NULL);
          delete alhs[i];
        }
        alhs.clear();
      }
    }
    catch (const MatlabComm::Sockets::Error & e)
    {
      clearArg(arhs);
      clearArg(alhs);
      if (!e.canBeContinued())
      {
        printStatusMsg("closing connection...\n");
        close(sockfd_);
        sockState_ = uninitialized;
      }
      std::cerr << e.what() << std::endl;
      sleep(1);
      printErrMsg("err\n");
    }
    catch (const mwException& e)
    {
      std::ostringstream errMsg;
      errMsg << "mwException catched: " << e.what() << "\n";
      printErrMsg(errMsg.str().c_str());
    }
    catch (const std::exception& e)
    {
      printErrMsg(e.what());
    }
    catch (...)
    {
      printErrMsg("Generic exception");
    }
  }
private:
  void send_mexFunctionArgs (size_t nlhs, const Args::RArgType &arhs)
  {
    MatlabComm::Sockets::Base::ArgNumStruct argNum;
    argNum.nlhs = nlhs;
    argNum.nrhs = arhs.size();
    send_chunk(sockfd_, argNum, "no. of arguments");
    for (Args::ArgSizeType i = 0; i < arhs.size(); ++i)
    {
      arhs[i]->sendFD(sockfd_);
    }
  }

  void recv_runtimeMessages ()
  {
    CommandHeader msgHeader;

    bool notFinished = true;
    while (notFinished)
    {
      recv_commandHeader(sockfd_, msgHeader);
      char buf[msgHeader.length];
      recv_chunk(sockfd_, buf[0], "message", false, sizeof(buf));

      switch (msgHeader.name)
      {
      case MatlabComm::Sockets::Protocol::ErrMsg:
        throw(SerializedArray::Error(buf));
        break;
      case MatlabComm::Sockets::Protocol::WarnMsg:
        printWarnMsg(buf);
        break;
      case MatlabComm::Sockets::Protocol::StatusMsg:
        printStatusMsg(buf);
        break;
      case MatlabComm::Sockets::Protocol::ReturnArgs:
        /*        printStatusMsg( "DUNE Server returning results...\n" );*/
        notFinished = false;
        break;
      default:
        std::ostringstream oss;
        oss
          << "Protocol error: expected one of commands: \'e\',\'w\' or \'s\' got \'";
        oss << msgHeader.name << "\'\n";
        throw(MatlabComm::Sockets::Error(oss.str(), false));
      }
    }
  }

  void recv_mexFunctionReturn (Args::LArgType &alhs)
  {
    for (Args::ArgSizeType i = 0; i < alhs.size(); ++i)
    {
      ArrayFactory::InterfaceType *&mxaPtr = alhs[i];
      mxaPtr = ArrayFactory::staticRecvFD(sockfd_);
      if (mxaPtr->getType() == SerializedArray::Undefined)
      {
        sockssize_t len;
        recv_chunk(sockfd_, len, "error: length");
        char buf[len];
        recv_chunk(sockfd_, buf[0], "error: text", false, len * sizeof(char));
        throw(MatlabComm::Sockets::Error(buf, false));
      }
      else if (mxaPtr->getType() == SerializedArray::Unknown)
      {
        throw(MatlabComm::Sockets::Error(
            "unknown return value from mexFunction", false));
      }
    }
  }

  void printErrMsg (const std::string & msg)
  {
    mexErrMsgIdAndTxt("MATLAB:mexclient:error", msg.c_str());
  }

  void printWarnMsg (const std::string & msg)
  {
    mexWarnMsgTxt(msg.c_str());
  }

  void printStatusMsg (const std::string & msg)
  {
    mexPrintf(msg.c_str());
  }

private:
  int sockfd_;
};

} // end of namespace Client
} // end of namespace MatlabComm
} // end of namespace Dune::RB
} // end of namespace Dune

#endif /* DUNE_RB_RBSOCKSCLIENT_HH_ */

/* vim: set sw=2 et: */
