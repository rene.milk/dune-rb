#include <dune/rb/matlabcomm/serializedarray/mx.hh>
#include "sockets.hh"

#include <string>
#include <cstring>

#define char16_t char16_t_backup
#include <mex.h>
#undef char16_t

/** @ingroup rbsocks
 * @file mexclient.cc
 * @brief entry point for a mexclient
 *
 * The library implemented here fulfills the client part of the socket
 * communication with a dune-rb server implementation. This file needs to be
 * compiled as a shared library. */

using namespace Dune::RB::MatlabComm;

static Client::Sockets * rbSocksClient = NULL;

/* default connection values */
static std::string serverhost = "localhost";
static std::string serverport = Client::Sockets::defaultPort();

void usage()
{
  mexPrintf("[largs, ...] = MEXCLIENT(operation, rargs)\n");
  mexPrintf("Communicates via TCP/IP with a server-mode program.\n");
  mexPrintf("\n");
  mexPrintf(
      "The first argument is always an 'operation' string indicating the action to take.\n");
  mexPrintf(
      "Usually the 'operation' string and the right hand side arguments 'rargs' are communicated\n");
  mexPrintf("to the server, but there are also a few default actions:\n");
  mexPrintf("        help\n");
  mexPrintf("        init_server (params)\n");
  mexPrintf("  ret = echo (var)\n");
  mexPrintf("  ret = is_connected\n");
  mexPrintf("        close\n");
  mexPrintf("        CLOSE\n");
  mexPrintf("        lock\n");
  mexPrintf("        unlock\n");
  mexPrintf("\n");
  mexPrintf("HELP prints this message.\n");
  mexPrintf(
      " INIT_SERVER expects one argument which must be a struct with fields 'serverhost' and 'port'\n");
  mexPrintf(
      " and initializes a TCP/IP connection to a server-mode programme.\n");
  mexPrintf("\n");
  mexPrintf(" ECHO echoes an argument 'var' and returns it as is.\n");
  mexPrintf("\n");
  mexPrintf(
      " IS_CONNECTED indicates whether the connection to the server-mode programme is working.\n");
  mexPrintf("\n");
  mexPrintf(
      " close and CLOSE both end the communication between the client and the server-mode pogramme.\n");
  mexPrintf(
      " The upper-case variant also terminates the server-mode programme.\n");
  mexPrintf("\n");
  mexPrintf(
      " LOCK and UNLOCK set and release a lock of the mexclient. A lock prevents the programme from\n");
  mexPrintf(" getting cleared accidentally.\n");
}

void mexClientExitHandler()
{
  if (rbSocksClient != NULL)
  {
    if (rbSocksClient->isConnected())
    {
      mxArray * temp_prhs[1];
      temp_prhs[0] = mxCreateString("close");
      const mxArray ** const_temp_prhs =
        const_cast< const mxArray ** >(temp_prhs);
      rbSocksClient->communicate_mexFunction(0, (mxArray**) NULL, 1,
          const_temp_prhs);
      mxDestroyArray(temp_prhs[0]);
    }
    mexUnlock();
    mexPrintf("mexclient has been unlocked!\n");
    delete rbSocksClient;
  }
}

/**
 * @brief entry point for commands in RBmatlab
 *
 * clears the lock and the generated RBSocksClient at exit;
 *
 * @param nlhs  number of lhs args
 * @param plhs  array of mxArray pointers storing write-able lhs args
 * @param nrhs  number of rhs args
 * @param prhs  array of mxArray pointers storing read-only rhs args
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  if (!nrhs >= 1 || !mxIsChar(prhs[0]))
  {
    usage();
    mexErrMsgTxt("First input argument must be a character string.");
  }

  char * actionString = mxArrayToString(prhs[0]);
  bool communication_necessary = true;
  if (strncmp(actionString, "init_server", 12) == 0)
  {
    communication_necessary = false;
    if (! (nrhs == 2 && mxIsStruct(prhs[1]) ) )
    {
      usage();
      mexErrMsgTxt("Expecting 2 input arguments. Second needs to be a struct.");
    }
    const int sind = mxGetFieldNumber(prhs[1], "serverhost");
    const int pind = mxGetFieldNumber(prhs[1], "port");
    mxArray * mxServ = mxGetFieldByNumber(prhs[1], mwIndex(0), sind);
    mxArray * mxPort = mxGetFieldByNumber(prhs[1], mwIndex(0), pind);
    char * server = mxArrayToString(mxServ);
    if (!mxIsChar(mxServ))
    {
      usage();
      mexErrMsgTxt("Server name must be a string!");
    }

    serverhost = server;
    mxFree(server);
    if (!mxIsChar(mxPort))
    {
      usage();
      mexErrMsgTxt("Port number must be a string!");
    }

    char * port = mxArrayToString(mxPort);
    serverport = port;
    mxFree(port);
  }
  else if (strncmp(actionString, "lock", 5) == 0)
  {
    communication_necessary = false;
    if (!mexIsLocked())
    {
      mexLock();
      mexPrintf("Mexclient has been locked. Unlock it before clearing!\n");
    }
    else
    {
      mexPrintf("Mexclient has already been locked.\n");
    }
  }
  else if (strncmp(actionString, "unlock", 7) == 0)
  {
    communication_necessary = false;
    if (mexIsLocked())
    {
      mexUnlock();
      mexPrintf(
          "Mexclient has been unlocked. It can be cleared from memory now.\n");
    }
    else
    {
      mexPrintf("Mexclient has not been locked.\n");
    }
  }
  else if (strncmp(actionString, "is_connected", 13) == 0)
  {
    communication_necessary = false;
    if (!nlhs == 1)
    {
      usage();
      mexErrMsgTxt("Expecting 1 output argument.");
    }
    else
    {
      bool is_connected = false;
      if (rbSocksClient != NULL)
      {
        is_connected = rbSocksClient->isConnected();
      }

      plhs[0] = mxCreateLogicalScalar(is_connected);
    }
  }
  else if (strncmp(actionString, "help", 5) == 0)
  {
    communication_necessary = false;
    usage();
  }
  mxFree(actionString);

  if (rbSocksClient == NULL)
  {
    rbSocksClient = new Client::Sockets(serverhost, serverport);
    mexAtExit(mexClientExitHandler);
  }

  if (rbSocksClient == NULL)
  {
    mexErrMsgTxt("Could not connect to a Dune server!");
  }
  else
  {
    if (communication_necessary)
    {
      rbSocksClient->communicate_mexFunction(nlhs, plhs, nrhs, prhs);
    }
  }
}

