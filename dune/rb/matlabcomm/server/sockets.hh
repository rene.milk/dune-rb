#ifndef __DUNE_RB_MATLABCOMM_SERVER_SOCKETS_HH_
#define __DUNE_RB_MATLABCOMM_SERVER_SOCKETS_HH_

/** @file rbsocksserver.hh
 * @brief provides class for standalone Dune server waiting for connections
 * from RBmatlab
 */

#include "interface.hh"

#include <dune/rb/matlabcomm/sockets.hh>

#include <netdb.h>
#include <arpa/inet.h>

#include <dune/rb/matlabcomm/serializedarray/std.hh>

#include <dune/rb/matlabcomm/facade/base.hh>

#include <dune/common/parametertreeparser.hh>

/*#include <dune/rb/misc/parameter/parameter.hh>*/

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Server
{

// forward declaration
class Sockets;

typedef Traits< Server::Sockets,
    SerializedArray::RBArrayFactory< SerializedArray::CMatrixWrapper > > SocketsTraits;

/** @ingroup rbsocks
 * @brief  Use this class in order to implement a standalone application
 * working in server mode waiting for commands received from a client running
 * in Matlab
 *
 * This class is singleton that acts as a server waiting for connections from a
 * client preferably running in RBmatlab and on its first use initializes an
 * object of type @c Traits::FacadeType. This object needs to provide a method
 *
 * @code void mexFunction(LArgType &, const RArgType &) @endcode
 *
 * and should be derived from RBMatlabBase.
 * This method \c mexFunction is called for every call of the \b mexclient in
 * Matlab. The function calls in matlab should have the following synopsis:
 *
 * @code
 * mexclient('command', arg1, arg2, ...);
 * @endcode
 *
 * The first parameter \c 'command' is then usually interpreted as the name of
 * function provided by the \c Traits::FacadeType and \c arg1, \c arg2, ... as
 * its parameters.
 *
 * The RBSocksServer knows two special commands that it checks beforehand:
 *
 * @arg \c 'close'     Closes the connection
 * @arg \c 'CLOSE'     Closes the connection to the client and stops the server
 *
 * Command line arguments:
 *
 * The command line should be parsed by initParameterTree() and actually uses
 * the ParameterTreeParser in order to read parameter tree entries from a file
 * specified as the first argument or from the command line. Parameters with an
 * effect on the behaviour on this class are all in the subtree called
 * 'server'.
 *
 * Parameters with effect on this class:
 *
 * @rbparam{server.mode, boolean indicating wether to run the program as a server or standalong, true (servermode)}
 * @rbparam{server.port, integer indicating on which port the server should listen, defaultPort}
 * @rbparam{server.operation, string indicating which command shall be run in standalone, "detailed_simulation"}
 * @rbparam{server.nlhs, integer indicating number of args in command run in standalone, 1}
 *
 * @see RBSocksClient, RBMatlabBase, RBMexLibServer, main.inc, mexclient.cc
 *
 * @tparam Traits  This is a problem specific traits class containing
 *                 a type named "FacadeType" which is the class's type that
 *                 implements the actual computation of the mexFunction.
 */
class Sockets: public MatlabComm::Sockets::Base, public Server::Interface<
    Sockets >
{
private:
  typedef Server::Args Args;
  typedef SerializedArray::IFactory< typename SocketsTraits::ArrayFactory > ArrayFactory;
public:
  //! this type
  typedef Sockets ThisType;

private:

  static bool isInitialized_;
  static const int backlog = 1;
private:

  /**
   * @brief constructor
   *
   * The constructor initializes an object of RBMatlabFacadeType and the server
   * socket for incoming connections (one at a time only!)
   */
  Sockets (Dune::ParameterTree & serverPT)
      : MatlabComm::Sockets::Base(), rbMatIf_(0), serverPT_(serverPT),
        servermode_(serverPT_.get("mode", true))
  {
    if (servermode_)
    {
      struct addrinfo hints;
      const int yes = 1;
      memset(&hints, 0, sizeof hints);
      /* allow IPV4 *and* IPV6 addresses */
      hints.ai_family = AF_UNSPEC;
      /* uses streams, everything else is to complex for me */
      hints.ai_socktype = SOCK_STREAM;
      /* this defines a server socket, listening passively
       * for connections*/
      hints.ai_flags = AI_PASSIVE;

      /* get the full servinfo struct */
      /* Note: in server mode, the first argument must be set
       * to NULL */
      struct addrinfo * servinfo;
      std::string port = serverPT_.get("port", defaultPort());
      int rv = getaddrinfo((char*) NULL, port.c_str(), &hints, &servinfo);
      if (rv != 0)
      {
        std::cerr << "getaddrinfo: " << gai_strerror(rv) << std::endl;
        return;
      }

      /* get a socket */
      /* loop through all the results and bind to the first
       * we can */
      struct addrinfo * p;
      for (p = servinfo; p != NULL; p = p->ai_next)
      {
        servsockfd_ = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (servsockfd_ == -1)
        {
          perror("server: socket ");
          continue;
        }

        /* allow reuse of local addresses in bind??? What
         * does this mean??? */
        rv = setsockopt(servsockfd_, SOL_SOCKET, SO_REUSEADDR, &yes,
            sizeof(int));
        if (rv == -1)
        {
          perror("setsockopt ");
          return;
        }

        /* bind the socket to an address (ip with port) */
        rv = bind(servsockfd_, p->ai_addr, p->ai_addrlen);
        if (rv == -1)
        {
          close(servsockfd_);
          perror("server: bind ");
          continue;
        }

        break;
      }
      if (p == NULL)
      {
        printErrMsg_(std::string("server: failed to bind\n"));
        return;
      }
      /*all done with this structure*/
      freeaddrinfo(servinfo);

      /* make the socket listen on connections. At most backlog
       * connections can be pending while waiting in this state
       */
      rv = listen(servsockfd_, backlog);
      if (rv == -1)
      {
        perror("listen");
        return;
      }
      printStatusMsg_(std::string("server: waiting for connections...\n"));
      sockState_ = listening;
      isInitialized_ = true;
    }
  }

  ~Sockets ()
  {
  }

  //! print out socket error messages with one prefix
  void perror (const char * errmsgPrefix)
  {
    std::ostringstream oss;
    oss << errmsgPrefix << " ";
    oss << strerror(errno) << "\n";
    if (sockState_ == connected)
    {
      printErrMsg_(oss.str());
    }
    else
    {
      std::cerr << oss.str();
    }
  }

  //! return the instance (singleton magic)
  static ThisType & theInstance (Dune::ParameterTree * serverPT = NULL)
  {
    static ThisType theInstance_(*serverPT);
    return theInstance_;
  }

  void runSimulation_ ()
  {
    if (rbMatIf_ != 0)
    {
      std::string operation = serverPT_.get("operation", "detailed_simulation");

      Args::RArgType rargs_init(1);
      Args::LArgType largs_init(1);
      Args::ArrayType * s_init = ArrayFactory::createString("init_model");
      rargs_init[0] = s_init;
      rbMatIf_->mexFunction(largs_init, rargs_init);
      clearArg(largs_init);
      delete (s_init);

      Args::RArgType rargs(1);
      Args::ArrayType * s = ArrayFactory::createString(operation);
      rargs[0] = &(*s);

      Args::LArgType largs(serverPT_.get("nlhs", (int)1));

      rbMatIf_->mexFunction(largs, rargs);

      clearArg(largs);
      delete (s);
    }
    else
    {
      printErrMsg_("No facade has been attached to this server yet!", true);
    }
  }

  //! main routine: listen on the initialized sockets for incoming connections
  //! and evaluate them inside the RBMatlabFacadeType
  void listen_ ()
  {
    if (!sockState_ == listening)
    {
      printErrMsg_(
          std::string(
              "the server socket has not been initialized correctly!\n\
          Cannot wait for connections."));
    }

    bool doWork = true;
    while (doWork)
    {
      struct sockaddr_storage their_addr;
      socklen_t sin_size = sizeof(their_addr);
      clientsockfd_ = accept(servsockfd_,
          reinterpret_cast< struct sockaddr * >(&their_addr), &sin_size);
      if (clientsockfd_ == -1)
      {
        perror("accept");
        continue;
      }

      sockState_ = connected;

      /* make a readable representation of their address. */
      char s[INET6_ADDRSTRLEN];
      inet_ntop(their_addr.ss_family,
          get_in_addr(reinterpret_cast< struct sockaddr * >(&their_addr)), s,
          sizeof(s));
      std::cout << "server: got connection from " << s << std::endl;

      Args::LArgType plhs;
      Args::RArgType prhs;
      Args::ArgSizeType nlhs;
      while (true)
      {
        try
        {
          /* TODO: check for other commands */
          char command;
          int rv = recv_chunk(clientsockfd_, command, "command", true);
          if (rv == 0
            || command == MatlabComm::Sockets::Protocol::CloseConnection)
          {
            break;
          }
          else if (command == MatlabComm::Sockets::Protocol::CallMexFunction)
          {
            recv_mexFunctionArgs(nlhs, prhs);

            plhs.resize(nlhs);
            if (rbMatIf_)
              rbMatIf_->mexFunction(plhs, prhs);
            else
              printErrMsg_("No facade has been attached to this server yet!",
                  false);

            clearArg(prhs);

            send_mexFunctionReturn(plhs);

            clearArg(plhs);
          }
          else if (command == MatlabComm::Sockets::Protocol::CloseServer)
          {
            doWork = false;
            break;
          }
          else
          {
            std::ostringstream oss;
            oss << "Protocol error: expected command \'f\', got \'";
            oss << command << "\'\n";
            throw(MatlabComm::Sockets::Error(oss.str(), false));
          }
        }
        catch (const MatlabComm::Sockets::Error & e)
        {
          clearArg(prhs);
          clearArg(plhs);
          std::cerr << e.what();
          if (e.canBeContinued())
          {
            std::cerr << "continuing" << std::endl;
            continue;
          }
          else
          {
            std::cerr << "breaking" << std::endl;
            break;
          }
        }
      }
      std::cout << "closing connection to client" << std::endl;
      close(clientsockfd_);
      sockState_ = listening;
    }
  }

  //! receive the Arguments to the mexFunction from the Matlab client
  void recv_mexFunctionArgs (Args::ArgSizeType & nlhs, Args::RArgType & prhs)
  {
    ArgNumStruct argNum;
    recv_chunk(clientsockfd_, argNum, "no. of arguments");
    nlhs = argNum.nlhs;
    assert(prhs.empty());
    for (Args::ArgSizeType i = 0; i < argNum.nrhs; ++i)
    {
      Args::ArrayType * rbaPtr = ArrayFactory::staticRecvFD(clientsockfd_);
      prhs.push_back(rbaPtr);
    }
  }

  //! send the result of a mexFunction evaluation back to the Matlab client
  void send_mexFunctionReturn (const Args::LArgType & plhs)
  {
    CommandHeader header;
    header.name = MatlabComm::Sockets::Protocol::ReturnArgs;
    header.length = 0;
    send_commandHeader(clientsockfd_, header);
    for (Args::ArgSizeType i = 0; i < plhs.size(); ++i)
    {
      if (plhs[i] == NULL)
      {
        /*        char command = RBSocksProtocol :: ErrMsg;
         *        send_chunk( clientsockfd_, command, "command: error" );*/
        /*        throw( RBSocksError("return value has not been initialized!\n", false) );*/
        printErrMsg_("return value has not been initialized!\n", false);
      }

      plhs[i]->sendFD(clientsockfd_);
    }
  }

  //! send an arbitrary message through the network channel
  void sendMsg (CommandHeader header, const char * msg) const
  {
    if (sockState_ == connected)
    {
      send_commandHeader(clientsockfd_, header);
      send_string(clientsockfd_, msg);
    }
  }

  //! send an error message through the network channel and throw an
  //! RBSocksError exception
  void printErrMsg_ (const std::string & msg, bool canBeContinued = true) const
  {
    CommandHeader header;
    header.name = 'e';
    header.length = msg.capacity();
    sendMsg(header, msg.c_str());
    std::ostringstream oss;
    oss << "Runtime error in mexFunction: \n\t";
    oss << msg << "\n";
    throw(MatlabComm::Sockets::Error(oss.str(), canBeContinued));
  }

  //! send a warning message through the network channel and print it to \c
  //! std::cerr
  void printWarnMsg_ (const std::string & msg) const
  {
    CommandHeader header;
    header.name = 'w';
    header.length = msg.capacity();
    sendMsg(header, msg.c_str());
    std::cerr << "Warning: " << msg;
  }

  //! send a status message through the network channel and print it to \c
  //! std::cout
  void printStatusMsg_ (const std::string & msg) const
  {
    CommandHeader header;
    header.name = 's';
    header.length = msg.capacity();
    sendMsg(header, msg.c_str());
    std::cout << msg;
  }

  bool inServerMode() const
  {
    return servermode_;
  }
public:
  /**
   * @brief main routine: listen on the initialized sockets for incoming
   * connections and evaluate them inside the RBMatlabFacadeType.
   *
   * \note The method catches some exceptions and on occurrence of some
   * exceptions needs to reset the connection in which case on Matlab side it
   * needs to be reset manually.
   *
   * This method should only return if the Matlab clients sends a \c 'CLOSE'
   * command.
   */
  static inline void run ()
  {
    if (theInstance().inServerMode())
      theInstance().listen_();
    else
      theInstance().runSimulation_();
  }

  static inline void bindToFacade (MatlabComm::Facade::Interface & facade)
  {
    theInstance().rbMatIf_ = &facade;
  }

  /** @brief send an error message through the network channel and throw an
   * RBSocksError exception
   */
  static inline void printErrMsg (const std::string & msg,
                                  const bool canBeContinued = true)
  {
    if (isInitialized_)
    {
      theInstance().printErrMsg_(msg, canBeContinued);
    }
    else
    {
      std::cerr << msg << std::endl;
    }
  }

  /** @brief send a warning message through the network channel and print it to
   * \c std::cerr
   */
  static inline void printWarnMsg (const std::string & msg)
  {
    if (isInitialized_)
    {
      theInstance().printWarnMsg_(msg);
    }
    else
    {
      std::cerr << "Warning: " << msg << std::endl;
    }
  }

  /** @brief send a status message through the network channel and print it to
   * \c std::cout
   */
  static inline void printStatusMsg (const std::string & msg)
  {
    if (isInitialized_)
    {
      theInstance().printStatusMsg_(msg);
    }
    else
    {
      std::cout << msg << std::endl;
    }
  }

  static void initParameterTree (int argc, char *argv[],
                                 Dune::ParameterTree & pt)
  {
    if (argc == 2)
    {
      std::string filename(argv[1]);
      Dune::ParameterTreeParser::readINITree(filename, pt);
    }
    else
    {
      Dune::ParameterTreeParser::readOptions(argc, argv, pt);
    }
    if (pt.hasKey("paramfile"))
    {
      const std::string paramfile = pt.get<std::string>("paramfile");
      std::cout << "Reading paramfile " << paramfile << std::endl;
      Dune::ParameterTreeParser::readINITree(paramfile, pt, false);
    }
    if (!pt.hasSub("server"))
    {
      pt.sub("server")["mode"] = "1";
    }
    Dune::ParameterTree & serverPT = pt.sub("server");
    theInstance(&serverPT);
  }
private:
  int servsockfd_; /* listen on sock_fd        */
  int clientsockfd_; /* new connection on new_fd */
  MatlabComm::Facade::Interface *rbMatIf_; /* the interface instance   */
  Dune::ParameterTree & serverPT_;
  bool servermode_; /* running in server mode?  */
};
// end of class declaration RBSocksServer

}  // namespace Server
}  // namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* __DUNE_RB_MATLABCOMM_SERVER_SOCKETS_HH_ */
/* vim: set et sw=2: */

