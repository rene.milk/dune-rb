#ifndef __DUNE_RB_RBSERVER_HH_
#define __DUNE_RB_RBSERVER_HH_

#include "sockets.hh"
#include "mexlib.hh"

/** @ingroup rbsocks
 * @file rbserver.hh
 * @brief file for choosing between server types
 *
 * Include this file if you want to choose by the macro @c MATLAB_MEX_FILE
 * wether your program is compiled as a server communicating with RBmatlab via
 * sockets or wether it should compile as a shared library with mexFunction
 * bindings.
 */

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Server
{

#ifdef MATLAB_MEX_FILE

//! global traits of used server. This is either set to RBMexLibTraits or RBSocksTraits
typedef MexLibTraits ServerTypeTraits;

#else // RBSocksServer
//! global traits of used server. This is either set to RBMexLibTraits or RBSocksTraits
typedef SocketsTraits ServerTypeTraits;

#endif

}  // namespace Server
}  // namespace MatlabComm
} // end of namespace Dune :: RB
} // end of namespace Dune

#endif /* __DUNE_RB_RBSERVER_HH_ */
