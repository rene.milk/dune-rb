#ifndef RBSERVERINTERFACE_HH_1KHXG3CW
#define RBSERVERINTERFACE_HH_1KHXG3CW

#include <string>
#include <vector>

#include <dune/rb/matlabcomm/facade/interface.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Server
{

template< class ServerImp, class ArrayFactoryImp >
struct Traits
{
  typedef ServerImp ServerType;
  typedef ArrayFactoryImp ArrayFactory;
};

template< class Impl >
class Interface
{
public:
  static void bindToFacade (MatlabComm::Facade::Interface & facade)
  {
    Impl::bindToFacade( facade );
  }

  /** @brief throws an RBSocksError exception initialized with given message.
   */
  static void printErrMsg (const std::string & msg)
  {
    Impl::printErrMsg( msg );
  }

  /** @brief prints a warning to \c std::cerr
   */
  static void printWarnMsg (const std::string & msg)
  {
    Impl::printWarnMsg( msg );
  }

  /** @brief print a status message to \c std::cout
   */
  static void printStatusMsg (const std::string & msg)
  {
    Impl::printStatusMsg( msg );
  }

private:
  Impl & asImp ()
  {
    return static_cast< Impl * >( *this );
  }

  const Impl & asImp () const
  {
    return static_cast< const Impl * >( *this );
  }
};

/** @brief Default implementation of an IRBServer, sending status messages to
 *  stdout and stderr.
 *  */
class Default: public Interface< Default >
{
public:
  static void printStatusMsg (const std::string & s)
  {
    std::cout << "Status: " << s << std::endl;
  }

  static void printWarnMsg (const std::string & s)
  {
    std::cerr << "Warning: " << s << std::endl;
  }

  static void printErrMsg (const std::string & s)
  {
    std::cerr << "Error: " << s << std::endl;
    DUNE_THROW( Dune::InvalidStateException, s.c_str() );
  }
};

} // end of namespace Server
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* end of include guard: RBSERVERINTERFACE_HH_1KHXG3CW */

