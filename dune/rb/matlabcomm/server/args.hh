/*
 * args.hh
 *
 *  Created on: 16.03.2012
 *      Author: martin
 */

#ifndef DUNE_RB_MATLABCOMM_SERVER_ARGS_HH_
#define DUNE_RB_MATLABCOMM_SERVER_ARGS_HH_

#include <dune/rb/matlabcomm/serializedarray/interface.hh>
#include <dune/rb/matlabcomm/serializedarray/cmatrixwrapper.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Server
{

struct Args
{
  //! array type for the mexFunction arguments in the RBMatlabFacadeType
  typedef SerializedArray::Interface<SerializedArray::CMatrixWrapper> ArrayType;

  typedef SerializedArray::SingleUsePtr< ArrayType * > SingleUseArrayType;

  typedef const SerializedArray::SingleUsePtr< ArrayType * > ConstSingleUseArrayType;
  //! type of left hand side arguments in mexFunction
  typedef std::vector< ArrayType * > LArgType;
  //! type of right hand side arguments in mexFunction
  typedef std::vector< const ArrayType * > RArgType;
  //! integer type to store the number of arguments
  typedef LArgType::size_type ArgSizeType;
};

} // end of namespace Server
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune



#endif /* DUNE_RB_MATLABCOMM_SERVER_ARGS_HH_ */
