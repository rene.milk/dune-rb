#ifndef __DUNE_RB_MATLABCOMM_SERVER_MEXLIB_HH_
#define __DUNE_RB_MATLABCOMM_SERVER_MEXLIB_HH_

/** @file mexlib.hh
 * @brief provides class for building a Dune program as mex library that can be
 * run in of RBmatlab
 */

#if (defined(MATLAB_MEX_FILE) || defined(DOXYGEN))
//#include <dune/rb/misc/parameter/parameter.hh>
/*#include <dune/fem/misc/mpimanager.hh>*/

#include <dune/rb/matlabcomm/facade/base.hh>

#include <dune/rb/matlabcomm/serializedarray/mx.hh>

// exception handling
#define char16_t char16_t_backup
#include "mclcppclass.h"
#undef char16_t

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Server
{


// forward declaration
class MexLib;

typedef Server::Traits< Server::MexLib, SerializedArray::MXArrayFactory > MexLibTraits;

/** @ingroup rbsocks
 * @brief  Use this class in order to implement a mex library that can directly
 * run in RBmatlab.
 *
 * This class is a singleton and needs to be bound to an object of type @c
 * RBMatlabFacadeInterface<RBMexLibTraits> via the method bindToFacade().
 *
 * The function calls in RBmatlab should have the following synopsis:
 *
 * @code
 * mexclient('command', arg1, arg2, ...);
 * @endcode
 *
 * The first parameter \c 'command' is then usually interpreted as the name of
 * function provided by the facade object and \c arg1, \c arg2, ... as
 * its parameters.
 *
 * @see RBMatlabBase, RBSocksServer
 */
class MexLib: public Server::Interface< MexLibTraits >
{
private:
  typedef Server::Args Args;
public:
  typedef SerializedArray::IFactory<typename MexLibTraits::ArrayFactory> ArrayFactory;
  //! this type
  typedef MexLib ThisType;
  //! array type for the mexFunction arguments in the RBMatlabFacadeType
private:
  //! constructor
  MexLib()
      : rbMatIf_(0)
  {
  }

  //! delete all elements the pointer objects in a vector of pointers point to
  template< class ArgType >
  void clearArg(ArgType & arg) const
  {
    for (size_t i = 0; i < arg.size(); ++i)
    {
      if (arg[i] != NULL)
      {
        delete arg[i];
      }
    }
    arg.clear();
  }

  //! main routine
  void _run(int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
  {
    if (!rbMatIf_)
    {
      printErrMsg("No facade has been attached to RBMexLibServer yet!");
      return;
    }

    Args::LArgType alhs(nlhs);
    Args::RArgType arhs(nrhs);
    try
    {
      for (size_t i = 0; i < arhs.size(); ++i)
      {
        const SerializedArray::MXArray *& mxaPtr = arhs[i];
        mxaPtr = SerializedArray::MXArray::wrapMXArray(prhs[i]);
        if (mxaPtr == NULL)
        {
          std::ostringstream oss;
          oss << "Could not handle argument " << i
            << " which has an unsupported mxArrayclass id.";
          printErrMsg(oss.str());
        }
      }

      rbMatIf_->mexFunction(alhs, arhs);

      clearArg(arhs);

      for (size_t i = 0; i < (unsigned) nlhs; ++i)
      {
        plhs[i] = dynamic_cast<SerializedArray::MXArray*>(alhs[i])->getMxPtr();
        assert( alhs[i] != NULL);
        delete alhs[i];
      }
      alhs.clear();
    }
    catch (const MatlabComm::Sockets::Error & e)
    {
      clearArg(arhs);
      clearArg(alhs);
      std::cerr << e.what() << std::endl;
      sleep(1);
      printErrMsg("err\n");
    }
    catch (const mwException& e)
    {
      std::ostringstream errMsg;
      errMsg << "mwException caught: " << e.what() << "\n";
      printErrMsg(errMsg.str().c_str());
    }
    catch (const std::exception& e)
    {
      printErrMsg(e.what());
    }
    catch (...)
    {
      printErrMsg("Generic exception");
    }
  }

  //! return the instance (singleton magic)
  static ThisType & theInstance()
  {
    static ThisType theInstance_;
    return theInstance_;
  }

public:
  /**
   * @brief main routine: wrap the mxArray * arguments that Matlab provides in
   * MXArray objects and run the \c Traits::FacadeType's \c mexFunction method
   * with those.
   *
   * \note The method catches some exceptions and tries to handle them
   * correctly.
   */
  static void run(int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
  {
    theInstance()._run(nlhs, plhs, nrhs, prhs);
  }

  static inline void bindToFacade(MatlabComm::Facade::Interface & facade)
  {
    theInstance().rbMatIf_ = &facade;
  }

  /** @brief throws an RBSocksError exception initialized with given message.
   */
  static void printErrMsg(const std::string & msg)
  {
    mexErrMsgTxt(msg.c_str());
  }

  /** @brief print a warning to \c std::cerr
   */
  static void printWarnMsg(const std::string & msg)
  {
    mexWarnMsgTxt(msg.c_str());
  }

  /** @brief print a status message to \c std::cout
   */
  static void printStatusMsg(const std::string & msg)
  {
    mexPrintf(msg.c_str());
  }

  static void initServer()
  {
    int argc_mpi = 1;
    const char * argv_temp[] =
    { "dunerbconvdiff" };
    char ** argv_mpi = const_cast< char ** >(&argv_temp[0]);
    MPIManager::initialize(argc_mpi, argv_mpi);

    Parameter::append(argc_mpi, argv_mpi);
  }

private:
  MatlabComm::Facade::Interface *rbMatIf_; /* the interface instance   */
};
// end of class declaration MexLib

}  // namespace Server
}  // namespace MatlabComm
}// end of namespace Dune :: RB

} // end of namespace Dune

#endif /* MATLAB_MEX_FILE || DOXYGEN */

#endif /* __DUNE_RB_MATLABCOMM_SERVER_MEXLIB_HH_ */
/* vim: set et sw=2: */
