/*
 * msrbfacade.hh
 *
 *  Created on: 16.03.2012
 *      Author: Martin Drohmann
 */

#ifndef RB_MATLAB_FACADE_MULTISCALE_STATIONARY_HH_
#define RB_MATLAB_FACADE_MULTISCALE_STATIONARY_HH_

#include "base.hh"
#include <dune/rb/matlabcomm/library/imultiscalestationary.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Facade
{

/** @brief MultiScaleStationary class for stationary multiscale reduced basis problems
 *
 *
 * **/
template< class ServerTraitsImp >
class MultiScaleStationary: public Facade::Base<MultiScaleStationary< ServerTraitsImp >, ServerTraitsImp >
{
public:
  typedef ServerTraitsImp ServerTraitsType;

  typedef MatlabComm::Server::Args Args;
  //! server type (either RBSocksServer or RBMexLibServer)
  typedef Server::Interface< typename ServerTraitsType::ServerType > ServerType;
  typedef SerializedArray::IFactory< typename ServerTraitsType::ArrayFactory > ArrayFactory;
  //! this type
  typedef MultiScaleStationary< ServerTraitsType > ThisType;
  //! base type
  typedef Facade::Base< RBMatlabLinEvolFacade< ServerTraitsType >, ServerTraitsType > BaseType;
  //! the engine implementing problem specific computations
  typedef Library::IMultiScaleStationary< ServerTraits > ImplementationType;
  //! container type for left hand side arguments
public:
  //! constructor
  MultiScaleStationary(ImplementationType & linEvolImp);

  /**
   * @brief this is the entry point to the RBmatlab program that is called from
   * the \c ServerType singleton.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void mexFunction(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    std::string operation;
    this->checkRHSArgs(arhs, 10000, 1);
    this->checkForArrayType(String, arhs, 0);
    arhs[0]->getString(operation);

    bool operationFound = true;
    operationFound = this->_mexFunction(operation, alhs, arhs);
    if (!operationFound)
      operationFound = library_.mexFunction(operation, alhs, arhs);
    if (!operationFound)
    {
      std::ostringstream oss;
      oss << "There is no operation registered named \"";
      oss << operation << "\"\n";
      ServerType::printErrMsg(oss.str());
    }
  }

  /**
   * @brief computes a detailed simulation in the high dimensional
   * function space for a specific mu.
   *
   * @code
   * sim_data = detailed_simulation(mu);
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void detailedSimulation(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    this->checkLHSArgs(alhs, 1, 1);
    double mu = arhs[1]->getPr()[0];
    library_.detailedSimulation(mu, alhs[0]);
  }

  /**
   * @brief clears the currently loaded reduced basis functions and begins with
   * an empty base function set again
   *
   * @code
   *   resetRB();
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void resetRB(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 0, 0);
    library_.resetRB();
  }

  /**
   * @brief generates model specific data that might be  necessary for
   * detailed simulations.
   *
   * @code
   *   model_data = gen_model_data()
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void genModelData(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 0);
    Args::ArrayType * model_data;
    library_.genModelData(model_data);
    if (alhs.size() == 1) alhs[0] = model_data;
  }

  /**
   * @brief returns the size of the reduced basis.
   *
   * @code
   *  N = get_rb_size()
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void getRBSize(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    int size = library_.getRBSize();
    arhs[0] = ArrayFactory::createScalar(size);
  }

  /**
   * @brief initializes the reduced basis space and returns the number
   * of added functions.
   *
   * @code
   * num_bases = init_reduced_basis([mu])
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void initReducedBasisSpace(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 0);
    this->checkLHSArgs(alhs, 1, 1);
    int num_bases;
    if (arhs.size() == 2)
    {
      double mu = arhs[1]->getPr()[0];
      num_bases = library_.initReducedBasis(mu);
    }
    else
    {
      num_bases = library_.initReducedBasis();
    }
    alhs[0] = ArrayFactory::createScalar(num_bases);
  }

  /**
   * @brief initializes model specific data like the GridPart and
   * discretization classes.
   *
   * @code
   * model = init_model([reduced_basis_path])
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void initModel(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 0);
    this->checkLHSArgs(alhs, 1, 1);
    if (arhs.size() == 1)
      library_.initModel(alhs[0], std::string(""));
    else if (arhs.size() == 2)
    {
      this->checkForArrayType(String, arhs, 1);
      std::string rbPath;
      arhs[1]->getString(rbPath);
      library_.initModel(alhs[0], rbPath);
    }
  }

  /**
   * @brief checks whether a valid reduced basis space has been loaded
   *
   * @code
   * ok = is_valid_rb()
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void isValidRB(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    book ok = library_.isValidRBSpace();
    alhs[0] = ArrayFactory::createScalar(ok);
  }

  /**
   * @brief returns parameter independent online matrices and vectors
   *
   * @code
   * operator_data = rb_components()
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbComponents(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    library_.rbOperators(alhs[0]);
  }

  /**
   * @brief returns parameter dependent coefficient strings
   *
   * @code
   * symbolic_coefficients = rb_symbolic_coefficients()
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbSymbolicCoefficients(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    Args::ArrayType * ret;
    library_.rbSymbolicCoefficients(ret);
    assert(ret != NULL);
    this->postprocessSymbolicCoefficients(alhs[0], *ret);
    delete ret;
  }

  /**
   * @brief takes a low dimensional vector of DoFs and projects onto
   * the high dimensional discrete function space.
   *
   * @code
   * rb_recons_data = rb_reconstruction(detailed_data, rb_sim_data)
   * @endcode
   *
   * @c rb_sim_data must have a field @c a.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbReconstruction(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 2, 2);
    this->checkLHSArgs(alhs, 1, 0);
    this->checkForArrayType(Struct, arhs, 1);
    this->checkForArrayType(Struct, arhs, 2);
    const unsigned int lowDimDofIndex = arhs[2]->getFieldNumber("a");
    if (lowDimDofIndex == arhs[2]->getNumberOfFields())
    {
      ServerType::printErrMsg(
          "Second argument (RBStruct) of 'rb_reconstruction' is missing a "
              "field named 'a'");
      return;
    }
    const Args::ArrayType & lowDimDofs = arhs[2]->getField(0, lowDimDofIndex);
    Args::ArrayType * rb_recons_data;
    library_.rbReconstruction(lowDimDofs, rb_recons_data);
    if (alhs.size() == 1)
    {
      alhs[0] = rb_recons_data;
    }
  }

  /**
   * @brief extends the reduced basis space by a new discrete solution.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void extendReducedBasisSpace(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 3, 2);
    this->checkLHSArgs(alhs, 1, 1);
    this->checkForArrayType(Matrix, arhs, 1);
    this->checkForArrayType(Matrix, arhs, 2);
    const int k = static_cast< int >(arhs[2]->getPr()[0]);
    double percentage = 0.98;
    if (arhs.size() == 4) percentage = arhs[3]->getPr()[0];

    int added = library_.extendReducedBasis(*arhs[1], k, percentage);
    alhs[0] = ArrayFactory::createMatrix(added, 1);
    double * retpr = alhs[0]->getPr();
    for (int i = 0; i < added; i++)
    {
      retpr[i] = 1;
    }
  }


  ImplementationType & getImplementation()
  {
    return library_;
  }

private:
  ImplementationType & library_;
};

template< class Traits >
MultiScaleStationary< Traits >::MultiScaleStationary(
    ImplementationType & linEvolImp)
    : library_(linEvolImp)
{
  registerFunction("detailed_simulation", &ThisType::detailedSimulation);
  registerFunction("reset_rb", &ThisType::resetRB);
  registerFunction("gen_model_data", &ThisType::genModelData);
  registerFunction("get_rb_size", &ThisType::getRBSize);
  registerFunction("init_reduced_basis", &ThisType::initReducedBasisSpace);
  registerFunction("init_model", &ThisType::initModel);
  registerFunction("is_valid_rb", &ThisType::isValidRB);
  registerFunction("extend_reduced_basis", &ThisType::extendReducedBasisSpace);
  registerFunction("rb_components", &ThisType::rbComponents);
  registerFunction("rb_reconstruction", &ThisType::rbReconstruction);
  registerFunction("rb_symbolic_coefficients",
      &ThisType::rbSymbolicCoefficients);
}

} // end of namespace Facade
} // end of namespace Matlab
} // end of namespace RB
} // end of namespace Dune


#endif /* RB_MATLAB_MSRBFACADE_HH_ */
