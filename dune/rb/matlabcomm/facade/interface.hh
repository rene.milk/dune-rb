/*
 * interface.hh
 *
 *  Created on: 16.03.2012
 *      Author: martin
 */

#ifndef RB_MATLABCOMM_FACADE_INTERFACE_HH_
#define RB_MATLABCOMM_FACADE_INTERFACE_HH_

#include <dune/rb/matlabcomm/server/args.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Facade
{

class Interface
{
public:
  typedef Server::Args Args;

public:
  Interface ()
  {}

  virtual ~Interface ()
  {}

  virtual void mexFunction (Args::LArgType &, const Args::RArgType &) = 0;
};

} /* namespace Facade */
} /* namespace MatlabComm */
} /* namespace RB */
} /* namespace Dune */

#endif /* RB_MATLABCOMM_FACADE_INTERFACE_HH_ */
