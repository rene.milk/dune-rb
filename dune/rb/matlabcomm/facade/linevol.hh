#ifndef __DUNE_RB_MATLABCOMM_FACADE_LINEVOL_HH_
#define __DUNE_RB_MATLABCOMM_FACADE_LINEVOL_HH_

/** @file linevolfacade.hh
 * @brief provides a facade class for linear evolution problems that also
 * defines an interface to the RBmatlab framework implementing the low
 * dimensional computations of a linear evolution problem in Matlab
 */

#include <dune/rb/runtime_error.hh>

#include "base.hh"

#include <dune/rb/matlabcomm/library/ilinevol.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Facade
{

/**
 * @brief Facade class for linear evolution problems
 *
 * This class needs a problem description given via the \c Traits template
 * parameter, that defines the types
 *
 *   \arg Implementation a type implementing all high dimensional computations
 *   that are necessary during a detailed simulation, the generation of the
 *   reduced basis space and the post-processing of reduced simulation results.
 *   This needs to be a template instantiation of LinEvolDefault
 *   \arg ServerType a server type (either RBSocksServer or RBMexLibServer)
 *   that defines whether the program is compiled as a dune standalone server or
 *   a mex library
 *
 * The \c Implementation object is created during construction of the facade
 * and used as an engine that runs problem specific computations on data that
 * has been processed by the facade.
 **/
template< class Traits >
class LinEvol: public Facade::Base<
    LinEvol< Traits >, Traits >
{
public:
  typedef MatlabComm::Server::Args Args;
  //! server type (either RBSocksServer or RBMexLibServer)
  typedef MatlabComm::Server::Interface< typename Traits::ServerType > ServerType;
  typedef MatlabComm::SerializedArray::IFactory< typename Traits::ArrayFactory > ArrayFactory;
  //! Traits class defining the problem
  typedef Traits TraitsType;
  //! this type
  typedef LinEvol< Traits > ThisType;
  //! base type
  typedef Facade::Base< LinEvol< Traits >, Traits > BaseType;
  //! the engine implementing problem specific computations
  typedef Library::ILinEvol< Traits > ImplementationType;
  //! container type for left hand side arguments
public:
  //! constructor
  LinEvol(ImplementationType & linEvolImp);

  /**
   * @brief this is the entry point to the RBmatlab program that is called from
   * the \c ServerType singleton.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void mexFunction(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    std::string operation;
    this->checkRHSArgs(arhs, 10000, 0);
    this->checkForArrayType(SerializedArray::ArrayEnum::String, arhs, 0);
    arhs[0]->getString(operation);

    bool operationFound = true;
    operationFound = this->_mexFunction(operation, alhs, arhs);
    if (!operationFound)
      operationFound = linEvolImp_.mexFunction(operation, alhs, arhs);
    if (!operationFound)
    {
      std::ostringstream oss;
      oss << "There is no operation registered named \"";
      oss << operation << "\"\n";
      ServerType::printErrMsg(oss.str());
    }
  }

  /**
   * @brief computes a detailed simulation in the high dimensional
   * function space @f$\mathcal{W}_H@f$
   *
   * \see setMu(), getMu()
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void detailedSimulation(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    linEvolImp_.detailedSimulation(alhs[0], true, false);
  }

  /**
   * @brief expands the current reduced basis space by a new high
   * dimensional solution of the problem for a given parameter
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
//  void expandRB(Args::LArgType & alhs, const Args::RArgType & arhs)
//  {
//    this->checkRHSArgs(arhs, 2, 2);
//    this->checkLHSArgs(alhs, 1, 1);
//    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
//    linEvolImp_.expandRB(*arhs[1], alhs[0]);
//  }

  /**
   * @brief clears the currently loaded reduced basis functions and begins with
   * an empty base function set again
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void resetRB(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 0, 0);
    linEvolImp_.clearReducedSpaceFiles();
  }

  /**
   * @brief generates model specific data that might be  necessary for
   * detailed simulations.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void genModelData(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 0);
    Args::ArrayType * W;
    linEvolImp_.genModelData(W);
    if (alhs.size() == 1) alhs[0] = W;
  }

  /**
   * @brief returns the currently active parameter vector @f$\mu@f$.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void getMu(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    linEvolImp_.getMu(alhs[0]);
  }

  /**
   * @brief returns the size of the reduced basis.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void getRBSize(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    linEvolImp_.getRBSize(alhs[0]);
  }

  /**
   * @brief sets the currently active parameter vector @f$\mu@f$.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void setMu(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 1);
    this->checkLHSArgs(alhs, 0, 0);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    try {
      linEvolImp_.setMu(*arhs[1]);
    } catch (const Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
  }

  /**
   * @brief adds a first set of base functions to the currently fresh
   * and empty reduced basis space.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void initDataBasis(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    int num = linEvolImp_.initReducedBasis();
    alhs[0] = ArrayFactory::createScalar(num);
  }

  /**
   * @brief initializes model specific data like the GridPart and
   * Discretization classes.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void initModel(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 0);
    this->checkLHSArgs(alhs, 1, 1);
    if (arhs.size() == 1)
      linEvolImp_.initModel(alhs[0], std::string(""));
    else if (arhs.size() == 2)
    {
      this->checkForArrayType(SerializedArray::ArrayEnum::String, arhs, 1);
      std::string rbPath;
      arhs[1]->getString(rbPath);
      try
      {
        linEvolImp_.initModel(alhs[0], rbPath);
      }
      catch (const Dune::RB::RuntimeError & e)
      {
        ServerType :: printErrMsg(e.what());
      }
    }
  }

  /**
   * @brief checks wether a valid reduced basis space has been loaded
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void isValidRB(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 0, 0);
    this->checkLHSArgs(alhs, 1, 1);
    linEvolImp_.isValidRBSpace(alhs[0]);
  }

  /**
   * @brief returns discrete function DoFs respectively coefficients
   * of the initial data function @f$u_0\f$.
   *
   * This function returns
   *   - the intial data function's projection on the high dimensional
   *   space @f$P_H[u_0]@f$ in \b decomp_mode \b 0
   *   - the initial data affine components' projections on the
   *   reduced basis space @f$P_N[U_0^q]@f$ for @f$q=0,...Q_{u_0}@f$
   *   in \b decomp_mode \b 1
   *   - the parameter dependent coefficients for the affine
   *   combination of the initial data @f$\sigma^q(\mu)@f$ for
   *   @f$q=0,...Q_{u_0}@f$ in \b decomp_mode \b 2.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbInitValues(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 1, 1);
    this->checkLHSArgs(alhs, 1, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    const int decompMode = static_cast< int >((arhs[1]->getMatrix())(0,0));
    try
    {
      linEvolImp_.rbInitValues(decompMode, alhs[0]);
    }
    catch (const Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
  }

  /**
   * @brief returns offline, online matrices and vectors and parameter
   * dependent coefficients for an affine combination of these
   * matrices and vectors
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbOperators(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 3, 3);
    this->checkLHSArgs(alhs, 1, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Cell, arhs, 2);
    this->checkForArrayType(SerializedArray::ArrayEnum::Cell, arhs, 3);
    std::vector<std::string> opIds, funcIds;
    arhs[2]->getVectorOfStrings(opIds);
    arhs[3]->getVectorOfStrings(funcIds);
    const SerializedArray::CMatrixWrapper & temp = arhs[1]->getMatrix();
    const int decompMode = static_cast< int >(temp(0,0));
    try
    {
      linEvolImp_.rbOperators(decompMode, opIds, funcIds, alhs[0]);
    }
    catch (Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
  }

  void rbSymbolicCoefficients(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 2, 2);
    this->checkLHSArgs(alhs, 1, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Cell, arhs, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Cell, arhs, 2);
    std::vector<std::string> opIds, funcIds;
    arhs[1]->getVectorOfStrings(opIds);
    arhs[2]->getVectorOfStrings(funcIds);
    try {
    linEvolImp_.rbSymbolicCoefficients(opIds, funcIds, alhs[0]);
    }
    catch (const Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
    assert(alhs[0] != NULL);
//    this->postprocessSymbolicCoefficients(alhs[0], *ret);
  }

  /**
   * @brief takes a low dimensional vector of DoFs and projects onto
   * the high dimensional discrete function space @f$\mathcal{W}_H@f$.
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbReconstruction(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 2, 2);
    this->checkLHSArgs(alhs, 1, 0);
    this->checkForArrayType(SerializedArray::ArrayEnum::Struct, arhs, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Struct, arhs, 2);
    const unsigned int coeffIndex = arhs[2]->getFieldNumber("uN");
    if (coeffIndex == arhs[2]->getNumberOfFields())
    {
      ServerType::printErrMsg(
          "Second argument (RBStruct) of 'rb_reconstruction' is missing a "
              "field named 'uN'");
      return;
    }
    const Args::ArrayType & coeffs = arhs[2]->getField(0, coeffIndex);
    Args::ArrayType * ret;
    try
    {
      linEvolImp_.rbReconstruction(*arhs[1], coeffs, ret);
    }
    catch (const Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
    if (alhs.size() == 1)
    {
      alhs[0] = ret;
    }
  }

  /*  |+*
   *   * @brief computes the @f$L^2@f$-norm of the difference between the detailed
   *   * and the reduced solution.
   *   *
   *   * @param alhs  container of left hand side arguments
   *   * @param arhs  container of right hand side arguments
   *   +|
   *  void rbReductionError ( T::LArgType & alhs, const T::RArgType & arhs )
   *  {
   *    checkRHSArgs( arhs, 1,1 );
   *    checkLHSArgs( alhs, 1,1 );
   *    linEvolImp_.rbReductionError( alhs[0] );
   *  }*/

  /**
   * @brief extends a list of orthonormal discrete functions by the
   * @f$k@f$ principal components of another discrete function list.
   *
   * @code
   *   next = rb_extension_PCA(mu, k[, percentage]);
   * @endcode
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void rbExtensionPCA(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    this->checkRHSArgs(arhs, 3, 2);
    this->checkLHSArgs(alhs, 1, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 1);
    this->checkForArrayType(SerializedArray::ArrayEnum::Matrix, arhs, 2);
    const int k = static_cast< int >((arhs[2]->getMatrix())(0,0));
    double percentage = 0.98;
    if (arhs.size() == 4)
      percentage = (arhs[3]->getMatrix())(0,0);

    int added = 0;
    try
    {
      added = linEvolImp_.rbExtensionPCA(*arhs[1], k, percentage);
    }
    catch (const Dune::RB::RuntimeError & e)
    {
      ServerType::printErrMsg(e.what());
    }
    alhs[0] = ArrayFactory::createMatrix(added, 1);
    SerializedArray::CMatrixWrapper & retpr = alhs[0]->getMatrix();
    for (int i = 0; i < added; i++)
    {
      retpr(i,0) = 1;
    }
  }

  /**
   * @brief debugging method: simply copy the right hand side
   * arguments into the left hand side arguments container
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void echo(Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    if (!(alhs.size() + 1 == arhs.size()))
    {
      ServerType::printErrMsg(
          "there must be as much lhs arguments as rhs arguments!\n");
    }
    for (size_t i = 0; i < alhs.size(); ++i)
    {
      std::ostringstream oss;
      oss << "copying argument no. " << i << '\n';
      ServerType::printStatusMsg(oss.str());
      alhs[i] = arhs[i + 1]->copy();
    }
  }

  ImplementationType & getImplementation()
  {
    return linEvolImp_;
  }

private:
  ImplementationType & linEvolImp_;
};

template< class Traits >
LinEvol< Traits >::LinEvol(ImplementationType & linEvolImp)
    : linEvolImp_(linEvolImp)
{
  registerFunction("detailed_simulation", &ThisType::detailedSimulation);
  registerFunction("echo", &ThisType::echo);
//  registerFunction("expand_rb", &ThisType::expandRB);
  registerFunction("reset_rb", &ThisType::resetRB);
  registerFunction("gen_model_data", &ThisType::genModelData);
  registerFunction("get_mu", &ThisType::getMu);
  registerFunction("get_rb_size", &ThisType::getRBSize);
  registerFunction("set_mu", &ThisType::setMu);
  registerFunction("init_data_basis", &ThisType::initDataBasis);
  registerFunction("init_model", &ThisType::initModel);
  registerFunction("is_valid_rb", &ThisType::isValidRB);
  registerFunction("rb_extension_PCA", &ThisType::rbExtensionPCA);
  registerFunction("rb_init_values", &ThisType::rbInitValues);
  registerFunction("rb_operators", &ThisType::rbOperators);
  registerFunction("rb_reconstruction", &ThisType::rbReconstruction);
  /*  registerFunction("rb_reduction_error",       &ThisType::rbReductionError      );*/
  registerFunction("rb_symbolic_coefficients",
      &ThisType::rbSymbolicCoefficients);
}

} // end of namespace Facade
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* __DUNE_RB_MATLABCOMM_FACADE_LINEVOL_HH_ */
/* vim: set et sw=2: */

