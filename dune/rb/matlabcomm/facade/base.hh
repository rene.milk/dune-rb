#ifndef __DUNE_RB_MATLAB_FACADE_BASE_HH_
#define __DUNE_RB_MATLAB_FACADE_BASE_HH_

/** @file
 * @brief provides an abstract base class for Facades that can be used with
 * RBSocksServer or RBMexLibServer
 */

#include <map>
#include <vector>
#include <string>
#include <sstream>
//#include <dune/rb/misc/parameter/parameter.hh>
#include <dune/rb/matlabcomm/serializedarray/interface.hh>
#include <dune/rb/matlabcomm/server/interface.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Facade
{

/** @ingroup rbmatlab
 * @brief an abstract base class (Barton-Nackman Trick) for a
 * RBMatlabFacadeInterface that can be used with an RBMexLibServer or
 * RBSocksServer singleton class.
 *
 * This implementation eases the registration of new commands accessable on the
 * Matlab side, and provides utility functions for checking the correct number
 * and type of the parameters.
 *
 * If the synopsis of calls in RBMatlab is
 *
 * @code
 * mexclient('operation', arg1, arg2, ...)
 * @endcode
 *
 * it is feasable that the facade implementation provides for each known
 * command \c 'operation' an own method
 *
 * @code
 * Impl::operation( LArgType & largs, const RArgType & rargs ).
 * @endcode
 *
 * In order to do this, two steps have to be done:
 *
 *   - In its constructor the facade binds the character string \c 'operation'
 *   to the implemented method \c Impl::operation by
 *   @code
 *     registerFunction( 'operation', &Impl::operation );
 *   @endcode
 *   - The facade adds in its implementation of method mexFunction() a call to
 *   the derived method _mexFunction() which calls the correct method
 *   automatically when the first argument equals the 'operation' string.
 *
 * @tparam Impl           The implementation for this base class
 *                        (c.f Barton-Nackman)
 * @tparam ServerTraits   Traits class providing the used argument types of
 *                        the server
 */
template< class Impl, class RBServerTraitsImp >
class Base: public Facade::Interface
{
private:
  //! base type

  typedef Server::Args Args;
  typedef Server::Interface< typename RBServerTraitsImp::ServerType > ServerType;
  typedef SerializedArray::IFactory< typename RBServerTraitsImp::ArrayFactory > ArrayFactory;

//  typedef RBServerTraits< typename RBServerTraitsImp::Arg1, typename RBServerTraitsImp::Arg2 > T;
//  typedef RBServerTraits< int, int > T;

//! pointer to a mexFunction implementation
  typedef void (Impl::*MexFunctionType) (Args::LArgType &,
                                         const Args::RArgType &);
  //! map of strings to mexFunction pointers
  typedef std::map< std::string, MexFunctionType > FunctionMapType;

public:
  //! constructor
  Base ()
      : funcMap_()
  {
  }

  virtual ~Base ()
  {
  }

protected:

  template< class ArgType >
  inline void checkArgs (const char * text, const ArgType & args, size_t maxNum,
                         size_t minNum = 0)
  {
    /*    std :: cout << minNum << " <= " << args.size() << " <= " << maxNum << std::endl;*/
    if (args.size() > maxNum || args.size() < minNum)
    {
      std::ostringstream oss;
      oss << text;
      oss << " expected ";
      if (minNum == maxNum)
        oss << minNum;
      else
        oss << minNum << " <= num <= " << maxNum;

      oss << ", but got " << args.size() << ". :-(\n";

      ServerType::printErrMsg( oss.str() );
    }
  }

  /**
   * @brief checks for the correct number of right hand side arguments
   *
   * This method checks if the dimension of the container argument \a args lies
   * in the range [\a minNum, \a maxNum] and prints an error message if does
   * not.
   *
   * @param args     arguments container to be checked for checked
   * @param maxNum   maximum number of allows arguments
   * @param minNum   minium number of required arguments
   */
  void checkRHSArgs (const Args::RArgType & args, int maxNum, int minNum = 0)
  {
    checkArgs( "Wrong number of input arguments:", args, maxNum + 1,
               minNum + 1 );
  }

  /**
   * @brief checks for the correct number of left hand side arguments
   *
   * This method checks if the dimension of the container argument \a args lies
   * in the range [\a minNum, \a maxNum] and prints an error message if does
   * not.
   *
   * @param args     arguments container to be checked for checked
   * @param maxNum   maximum number of allows arguments
   * @param minNum   minium number of required arguments
   */
  void checkLHSArgs (const Args::LArgType & args, int maxNum, int minNum = 0)
  {
    checkArgs( "Wrong number of output arguments:", args, maxNum, minNum );
  }

  /**
   * @brief check if an argument is of the required type.
   *
   * @param type  the type the argument should have
   * @param arg   argument container
   * @param i     argument index in the argument container
   */
  template< class ArgType >
  inline void checkForArrayType (SerializedArray::ArrayEnum type,
                                 const ArgType & arg, size_t i)
  {
    if (arg[i]->getType() != type)
    {
      std::ostringstream oss;
      oss << "Argument " << i << " has wrong type!";
      ServerType::printErrMsg( oss.str() );
    }
  }

  /**
   * @brief This method binds a character string to a method in the facade
   * implementation class.
   *
   * @param operation  a std::string that names the operation
   * @param func       pointer to a method in the implementation class
   */
  inline void registerFunction (const std::string & operation,
                                MexFunctionType func)
  {
    funcMap_.insert( make_pair( operation, func ) );
  }

  /**
   * @brief function that automatically delegates the evaluation of the
   * received function arguments to a method in the implementation class that
   * has registered beforehand and bound to the character string \a operation
   *
   * @param operation         a character string indicating which method should
   *                          be called
   * @param alhs              left hand side arguments received from RBmatlab
   * @param arhs              right hand side arguments received from RBmatlab
   *
   * @return  boolean indicating wether an operation named \a operation has
   * been registered.
   */
  inline bool _mexFunction (const std::string & operation,
                            Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    typedef typename FunctionMapType::const_iterator MapIterator;

    std::cout << "\nReceived call for processing '" << operation << "'\n"
              << "with " << arhs.size() << " arguments and " << alhs.size()
              << " return values.\n"
              << "======================================================\n\n";

    MapIterator it = funcMap_.find( operation );

    if (it != funcMap_.end())
    {
      MexFunctionType mexFunction = (*it).second;
      (asImp().*mexFunction)( alhs, arhs );
      return true;
    }
    else if (operation == "echo")
    {
      echo( alhs, arhs );
      return true;
    }
    return false;
  }

  /**
   * @brief debugging method: simply copy the right hand side
   * arguments into the left hand side arguments container
   *
   * @param alhs  container of left hand side arguments
   * @param arhs  container of right hand side arguments
   */
  void echo (Args::LArgType & alhs, const Args::RArgType & arhs)
  {
    if (!(alhs.size() + 1 == arhs.size()))
    {
      ServerType::printErrMsg(
          "there must be as much lhs arguments as rhs arguments!\n" );
    }
    for (size_t i = 0; i < alhs.size(); ++i)
    {
      std::ostringstream oss;
      oss << "copying argument no. " << i << '\n';
      ServerType::printStatusMsg( oss.str() );
      alhs[i] = arhs[i + 1]->copy();
    }
  }




private:
  const Impl & asImp () const
  {
    return static_cast< const Impl& >( *this );
  }

  Impl & asImp ()
  {
    return static_cast< Impl& >( *this );
  }

protected:
  FunctionMapType funcMap_;
};

} // end of namespace Facade
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* __DUNR_RB_MATLAB_BASE_HH_ */
/* vim: set et sw=2: */

