#ifndef __DUNE_RB_MATLABCOMM_SOCKETS_HH_
#define __DUNE_RB_MATLABCOMM_SOCKETS_HH_

#include <sstream>
#include <string>
#include <vector>
#include <iostream>

#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <cstddef>
#include <cstring>
#include <cassert>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace Sockets
{


/** @addtogroup rbsocks
 * @{ */

/**
 * @brief structure defining communication commands used by RBSocksServer or
 * RBSocksClient classes in order to encode what the other side has to do,
 * respectively to decode what the other side expects to be done.
 */
struct Protocol
{
  //! command for "close connection" request
  static const char CloseConnection = 'c';
  //! command for "close server" request
  static const char CloseServer     = 'C';
  //! command for "error message" notification
  static const char ErrMsg          = 'e';
  //! command for "mex function call follows" notification
  static const char CallMexFunction = 'f';
  //! command for "return arguments follow" notification
  static const char ReturnArgs      = 'r';
  //! command for "status message" notification
  static const char StatusMsg       = 's';
  //! command for "warning message" notification
  static const char WarnMsg         = 'w';
};

/**
 * @brief exception handling class for RBSocks communication
 */
class Error
  : public std::exception
{
public:
  /**
   * @brief constructor
   *
   * @param msg              an rough explanation of the exception that is thrown
   * @param canBeContinued   a flag indicating wether the connection can be
   *                         left open or needs to be resetted
   */
  Error(const std::string msg, bool canBeContinued = true)
    : msg_(msg), canBeContinued_(canBeContinued)
  {};

  //! copy constructor
  Error( const Error & error )
    : msg_(error.msg_), canBeContinued_(error.canBeContinued_)
  {}

  virtual ~Error () throw()
  {}

  /**
   * @brief returns the error exception message
   *
   * @return  explanation of the exception
   */
  virtual const char* what () const throw ()
  {
    std :: ostringstream oss;
    oss << "Error: " << msg_;
    if( !canBeContinued_ )
    {
      oss << "\n\n********************************************************************\n";
      oss << "! It probably is safer to reset the connection now.                !\n";
      oss << "! The server will still be usable. The RBMatlab client needs to be !\n";
      oss << "! reset manually by typing                                         !\n";
      oss << "!                                                                  !\n";
      oss << "! > clear mexclient                                                !\n";
      oss << "!                                                                  !\n";
      oss << "! at the MATLAB prompt.                                            !\n";
      oss << "********************************************************************\n";
    }
    return oss.str().c_str();
  }

  /**
   * @brief indicates wether the connection can be left open or needs to be
   * resetted
   *
   * @return  a boolean value
   */
  virtual bool canBeContinued () const
  {
    return canBeContinued_;
  }

private:
  std::string msg_;
  bool        canBeContinued_;
};

/**
 * @brief Base class for all classes that need methods for communication over a
 * network socket.
 *
 * This class comprises functions to send and receive data chunks over TCP/IP
 * Sockets with error handling.
 */
class Base
{
public:
  //! an integer type that can safely be sent/received over the network
  typedef size_t                                                     sockssize_t;

public:
  //! enum for connection status
  typedef enum
  {
    uninitialized,
    readyforconnection,
    listening,
    connected
  } SockState;
protected:

  //! protocol type for exchange of number of arguments
  typedef struct
  {
    sockssize_t nlhs, nrhs;
  } ArgNumStruct;

  //! protocol type for exchange of command and the length of the following
  //! arguments
  typedef struct
  {
    char        name;
    sockssize_t length;
  } CommandHeader;

  //! default constructor initializing the connection status to 'uninitialized'
  Base ()
    : sockState_(uninitialized)
  {}

public:
  //! constructor constructing an Base object with given connection state
  Base (SockState state)
    : sockState_(state)
  {}

  virtual ~Base ()
  {}

  //! returns a default port that can be used for communication over a socket
  static const char * defaultPort()
  {
    static const char defaultPort[] = "1909";
    return defaultPort;
  }


protected:
  /**
   * @brief get sockaddr, IPv4 or IPv6
   *
   * @param sa  generic sockaddr structure
   *
   * @return  specialized sockaddr structure (either encapsulating an IPv4 or
   * an IPv6 address)
   */
  void *get_in_addr(struct sockaddr *sa)
  {
    if (sa->sa_family == AF_INET)
    {
      return &((reinterpret_cast<struct sockaddr_in*>(sa))->sin_addr);
    }

    return &((reinterpret_cast<struct sockaddr_in6*>(sa))->sin6_addr);
  }

  /**
   * @brief utility method deleting objects in an vector of an arbitrary type.
   *
   * @todo move this into a utility class
   *
   * @param arg   a vector container of pointer objects
   * @tparam Arg  Type of objects the pointers point to
   */
  template<class Arg>
  void clearArg(std::vector<Arg *> & arg) const
  {
    for( size_t i = 0; i < arg.size(); ++i )
    {
      if(arg[i] != NULL)
      {
        delete arg[i];
      }
    }
    arg.clear();
  }

  /**
   * @brief tries to receive a given amount of bytes over a network channel.
   *
   * @param sock  socket number specifying the network channel
   * @param buf   pointer to the memory address where the received bytes are
   *              written to.
   * @param len   number of bytes thar are to be received
   *
   * @return      -1 on error, number of bytes received else
   */
  ssize_t recvall(const int sock, char *buf, size_t &len) const
  {
    size_t total     = 0;   // how many bytes we've received
    int    bytesleft = len; // how many we have left to receive
    int    n         = 0;

    int misses = 0;
    assert(sockState_ == connected);
    while(total < len) {
      n = recv(sock, buf+total, bytesleft, 0);
      if(n == 0)
      {
        if(misses > 3) { break; }
        if(misses > 1)
        {
          std::cerr << "Warning: total bytes received: " << total << " of " << len << "\n"
                    << "         Failed " << misses << " times in receiving the "
                    << bytesleft << " bytes." << std::endl;
        }
        ++misses;
      }
      else if (n == -1) { break; }
      else
      {
        misses = 0;
      }
      total     += n;
      bytesleft -= n;
    }

    return n==-1?-1:(signed)total; // return -1 on failure, bytes received on success
  }

  /**
   * @brief tries to send a given amount of bytes over a network channel.
   *
   * @param sock  socket number specifying the network channel
   * @param buf   pointer to the memory address where the bytes to be sent are
   *              read from
   * @param len   number of bytes thar are to be sent
   *
   * @return      -1 on error, number of bytes sent else
   */
  ssize_t sendall(const int sock, const char *buf, size_t &len) const
  {
    size_t total     = 0;   // how many bytes we've sent
    int    bytesleft = len; // how many we have left to send
    int    n         = 0;

    int misses = 0;
    assert(sockState_ == connected);
    while(total < len) {
      n = send(sock, buf+total, bytesleft, 0);
      if(n == 0)
      {
        if(misses > 3) { break; }
        if(misses > 1)
        {
          std::cerr << "Warning: total bytes sent: " << total << " of " << len << "\n"
                    << "         Failed " << misses << " times in sending the "
                    << bytesleft << " bytes." << std::endl;
        }
        ++misses;
      }
      else if (n == -1) { break; }
      else
      {
        misses = 0;
      }
      total     += n;
      bytesleft -= n;
    }

    return n==-1?-1:(signed)total; // return -1 on failure, bytes send on success
  }


  /**
   * @brief sends an object over a specified network channel
   *
   * @param sock   socket number indicating the network channel
   * @param chunk  The object we will send
   * @param descr  a short description of the object
   * @param size   the object's size
   *
   * @exception Dune::RB::RBSocksError  the chunk could not be transmitted or
   *                                    could only be partially transmitted
   */
  template<class T>
  void send_chunk( const int sock,
                   const T & chunk,
                   const char * descr,
                   size_t size = sizeof(T) ) const
  {
    ssize_t numbytes;
#ifdef CHECK_WITH_FILES
    numbytes = write( sock,
                      reinterpret_cast<const char *>(&chunk),
                      size );
#else
    numbytes = sendall( sock,
                        reinterpret_cast<const char *>(&chunk),
                        size );
#endif
#ifdef RBSOCKS_DEBUG
    std::cout << " in send_chunk: " << numbytes << " of " << size
              << " bytes sent! " << " errno: " << errno << std::endl;
#endif
    if(numbytes != (signed)size)
    {
      bool canContinue = true;
      std :: ostringstream oss;
      oss << "failed sending chunk: " << std::string(descr) << "\n";
      if(numbytes == -1)
      {
        oss << "strerror: " << std::string(std::strerror(errno)) << "\n";
        canContinue = false;
      }
      throw( Error(oss.str().c_str(), canContinue) );
    }
  }

  /**
   * @brief efficiently receives an object of type CommandHeader
   *
   * @param sock    the socket indicating the network channel we are listening
   *                to
   * @param header  the command header we want to fill
   */
  void recv_commandHeader( const int sock, CommandHeader & header ) const
  {
    char buf[sizeof(header.name)+sizeof(header.length)];
    recv_chunk( sock, buf, "command header" );
    header.name = buf[0];
    memcpy( reinterpret_cast<char *>(&header.length), buf+1, sizeof(header.length) );
  }

  /**
   * @brief efficiently sends an object of type CommandHeader
   *
   * @param sock    the socket indicating the network channel we are sending to
   *                to
   * @param header  the command header for submission
   */
  void send_commandHeader( const int sock, const CommandHeader & header ) const
  {
    char buf[sizeof(header.name)+sizeof(header.length)];
    buf[0] = header.name;
    memcpy( buf+1, reinterpret_cast<const char *>(&header.length), sizeof(header.length) );
    send_chunk( sock, buf, "command header" );
  }

  /**
   * @brief sends a character string over a given network channel
   *
   * @param sock  socket number indicating the network channel we want to write
   *              to.
   * @param msg   the message we want to submit
   */
  inline void send_string(const int sock, const std::string & msg) const
  {
    const char * msg_cstr = msg.c_str();
    send_chunk(sock, msg_cstr[0], msg_cstr, msg.capacity());
  }

public:
  /**
   * @brief sends an object over a specified network channel
   *
   * @param sock     socket number indicating the network channel
   * @param chunk    The object we will receive
   * @param descr    a short description of the object
   * @param noThrow  flag indicating wether we are disallowed to throw an
   *                 exception on failure
   * @param size     the object's size
   *
   * @exception Dune::RB::RBSocksError  the chunk could not be received or
   *                                    could only be received partially
   */
  template<class T>
  int recv_chunk(const int sock,
                 T & chunk,
                 const char * descr,
                 bool noThrow = false,
                 size_t size = sizeof(T)) const
  {
    ssize_t numbytes;
    assert(sockState_ == connected);
#ifdef CHECK_WITH_FILES
    numbytes = read(sock,
                    reinterpret_cast<void *>(&chunk),
                    size);
#else
    numbytes = recvall(sock,
                       reinterpret_cast<char *>(&chunk),
                       size);
#endif
#ifdef RBSOCKS_DEBUG
    std::cout << " in recv_chunk: " << numbytes << " of " << size
              << " bytes received! " << " errno: " << errno << std::endl;
#endif
    if(numbytes != (signed)size)
    {
      bool canContinue = true;
      std :: ostringstream oss;
      oss << "failed receiving chunk: " << std::string(descr) << "\n";
      if(numbytes == -1)
      {
        oss << "strerror: " << std::string(std::strerror(errno)) << "\n";
        canContinue = false;
      }
      if(!noThrow)
      {
        throw( Error(oss.str().c_str(), canContinue) );
      }
      else
      {
        std::cerr << Error( oss.str().c_str() ).what() << std::endl;
        std::cerr << "Closing connection!" << std::endl;
        close( sock );
      }
    }
    return numbytes;
  }

public:
  /**
   * @brief returns the state of a network connection
   *
   * @return  a SockState enum
   */
  SockState getSockState()
  {
    return sockState_;
  }

protected:
  SockState sockState_;
};

/* @} */

} // end of namespace Sockets
} // end of namespace MatlabComm
} // end of namespace Dune :: RB
} // end of namespace Dune

#endif /* __DUNE_RB_MATLABCOMM_SOCKETS_HH_ */

/* vim: set et sw=2 */

