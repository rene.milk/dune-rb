/** @file main.inc
  * @brief include this file for useful main respectively mexFunction default
  * routines.
  *
  * You simply need to define a structure name ServerTraits that has a public
  * type definition ServerType. For an example usage see ...
  *
  * @see RBSocksServer, RBMexLibServer
  *
  */

#ifndef MATLAB_MEX_FILE
#include "../../misc/parameter/parameter.hh"
#include <dune/fem/misc/mpimanager.hh>
#else
#include <mex.h>
#endif

//! server type used for mexFunction delegation and output routines
typedef Dune :: RB :: ProblemTraits :: ServerType                    ServerType;
typedef Dune :: RB :: ProblemTraits :: FacadeType                    FacadeType;
typedef Dune :: RB :: ProblemTraits :: DiscrType                     DiscrType;

using namespace Dune :: RB;
using Dune :: MPIManager;
using Dune :: Parameter;

#ifndef MATLAB_MEX_FILE
/**
  * @brief entry point for a standalone application acting as a server
  */
int main (int argc, char* argv[])
{
  /* initialize MPIManager */
  MPIManager :: initialize( argc, argv );

  /* initialize Parameter */
  Parameter :: append(argc, argv);

  DiscrType discr;

  FacadeType facade(discr);

  ServerType :: bindToFacade (facade);

  ServerType :: run ();

  return 0;
}

#else

void mexFunction(int nlhs, mxArray * plhs[], int nrhs, const mxArray *prhs[])
{

  try
  {
    ServerType :: initServer();

    DiscrType discr;

    FacadeType facade(discr);

    ServerType :: bindToFacade (facade);

    ServerType :: run (nlhs, plhs, nrhs, prhs);
  }
  catch(Dune::Exception &e)
  {
    std::ostringstream errMsg;
    errMsg << e << "\n";
    mexErrMsgTxt(errMsg.str().c_str());
  }
  catch (const mwException& e)
  {
    std::ostringstream errMsg;
    errMsg << "mwException catched: " << e.what() << "\n";
    mexErrMsgTxt(errMsg.str().c_str());
  }
  catch(const std::exception& e)
  {
    mexErrMsgTxt(e.what());
  }
  catch(...)
  {
    mexErrMsgTxt("Generic exception");
  }
}

#endif
/* vim: set et sw=2 ft=cpp: */
// Local Variables: 
// mode: C++
// End: 


