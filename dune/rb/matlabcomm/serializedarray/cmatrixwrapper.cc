/*
 * cmatrixwrapper.cc
 *
 *  Created on: 17.03.2012
 *      Author: martin
 */

#include <cstring>

#include "cmatrixwrapper.hh"

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{

CMatrixWrapper :: CMatrixWrapper( const RMatrixWrapper & transposedCopy )
  : BaseType(transposedCopy.rows(), transposedCopy.cols())
{
  for (size_t i = 0; i < rows_; ++i)
  {
    for (size_t j = 0; j < cols_; ++j)
    {
      entries_ptr_[i*cols_+j] = transposedCopy(i, j);
    }
  }
}

RMatrixWrapper :: RMatrixWrapper( const CMatrixWrapper & transposedCopy )
  : BaseType(transposedCopy.rows(), transposedCopy.cols())
{
  for (size_t j = 0; j < cols_; ++j)
  {
    for (size_t i = 0; i < rows_; ++i)
    {
      entries_ptr_[i+j*rows_] = transposedCopy(i, j);
    }
  }
}
} // end of namespace SerializedArray
} // end of namespace MatlabCommon
} // end of namespace RB
} // end of namespace Dune
