#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <iomanip>
#include <fstream>


#define CHECK_WITH_FILES 1

#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)

#if GCC_VERSION < 40203
#error GCC version is too old for this test!
#endif

#ifdef HAVE_EIGEN
#include <Eigen/Core>
#endif
#include "../std.hh"
#include "../mx.hh"

using namespace Dune :: RB :: MatlabComm ::SerializedArray;

template<class Out, class In>
class SerializeArrayTest
{
  typedef IFactory<typename Out :: Traits> OutFactory;
  typedef IFactory<typename In :: Traits> InFactory;
  typedef typename OutFactory :: MatrixStorageType OutMatrixStorageType;
  typedef Interface<OutMatrixStorageType> OutInterfaceType;
  typedef typename InFactory :: MatrixStorageType InMatrixStorageType;
  typedef Interface<InMatrixStorageType> InInterfaceType;
public:
  SerializeArrayTest (const char * from, const char * to)
    : from_(from), to_(to)
  {}

  virtual ~SerializeArrayTest()
  {}

  bool run()
  {

    std::cout << "Starting test for communication between " << from_ << " and " << to_ << std::endl;

    bool ok = true;
    OutInterfaceType * c = OutFactory::createCell(3,1);
/*    size_t M, N;
 *    M = c->getM();
 *    N = c->getN();
 *    std::cout << "M: " << M << " N: " << N << std::endl;*/
    c->setCell(0, OutFactory::createString("Hello World"));
    c->setCell(1, OutFactory::createMatrix(2,3));
    c->setCell(2, OutFactory::createCell(2,1));

    OutInterfaceType & cm = c->getCell(1);
    OutMatrixStorageType & cmatrix = cm.getMatrix();
    for( size_t i = 0; i < 2; ++i )
    {
      for( size_t j = 0; j < 3; ++j)
        cmatrix(i,j) = i*10+j;
    }

    OutInterfaceType & cc = c->getCell(2);
    cc.setCell(0, OutFactory::createString("string in cell in cell"));
    std::vector<std::string> fieldnames;
    fieldnames.push_back("abc");
    fieldnames.push_back("def");
//    const char * fieldnames[] = {"abc", "def"};
    cc.setCell(1,OutFactory::createStruct(2,1,fieldnames));
    OutInterfaceType & ccs = cc.getCell(1);
    /* add empty matrix */
    ccs.setField( 0, 0, OutFactory::createMatrix(0, 0) );
    ccs.setField( 0, 1, OutFactory::createString("string in struct") );
    /* add empty cell */
    ccs.setField(1, 0, OutFactory::createCell( 0, 0 ));
    ccs.setField(1, 1, OutFactory::createMatrix( 1, 1 ));
    OutInterfaceType & ccsa = ccs.getField(1,1);
    OutMatrixStorageType & prccsa = ccsa.getMatrix();
    prccsa(0,0) = 42;

    // read in the copy
    InInterfaceType * cCopy;
    int fd = -1;
    std::string testfile(std::string("./testfile"));
    try
    {
      fd = open (testfile.c_str(), O_CREAT|O_WRONLY|O_TRUNC, 0660);
      if(fd == -1)
        std::cerr << strerror(errno) << std::endl;
      c->sendFD( fd );
      close ( fd );

      fd = open(testfile.c_str(), O_RDONLY );
      if(fd == -1)
        std::cerr << strerror(errno) << std::endl;
      cCopy = InFactory::staticRecvFD( fd );
    }
    catch(Dune::RB::MatlabComm::Sockets::Error & e)
    {
      std::cerr << "caught exception: " << e.what() << std::endl;
      ok = false;
      close( fd );
      return ok;
    }
    close( fd );

/*    M = cCopy->getM();
 *    N = cCopy->getN();
 *    std::cout << "M: " << M << " N: " << N << std::endl;*/
    if(! (cCopy->getM() == 3 && cCopy->getN() == 1) )
    {
      std::cerr << "cCopy has wrong size!" << std::endl;
      ok = false;
    }
    std::string s;
    cCopy->getCell(0).getString(s);
    if(! s.compare("Hello World") == 0)
    {
      std::cerr << "String in first cell read in incorrectly!" << std::endl;
      ok = false;
    }
    InMatrixStorageType & prCopy = cCopy->getCell(1).getMatrix();
    for( size_t i = 0; i < 2; ++i )
    {
      for( size_t j = 0; j < 3; ++j )
      {
        if(prCopy(i,j) != i*10+j)
        {
          std::cerr << "Array element (" << i << "," << j << ") read in incorrectly!" << std::endl;
          std::cerr << "Expected: " << i*10+j << " got:" << prCopy(i,j) << std::endl;
          ok = false;
        }
      }
    }
    InInterfaceType & ccCopy = cCopy->getCell(2);
    if(! (ccCopy.getM() == 2 && ccCopy.getN() == 1) )
    {
      std::cerr << "ccCopy has wrong size!" << std::endl;
      ok = false;
    }
    ccCopy.getCell(0).getString(s);
    if(! s.compare("string in cell in cell") == 0)
    {
      std::cerr << "String in second cell read in incorrectly!" << std::endl;
      ok = false;
    }
    InInterfaceType & ccsCopy = ccCopy.getCell(1);
    if(! (ccsCopy.getM() == 2 && ccsCopy.getN() == 1
          && ccsCopy.getNumberOfFields() == 2) )
    {
      std::cerr << "ccsCopy has wrong size!" << std::endl;
      ok = false;
    }
    if(! (ccsCopy.getFieldNumber("abc") == 0
          && ccsCopy.getFieldNumber("def") == 1) )
    {
      std::cerr << "Field numbers are wrong!" << std::endl;
      ok = false;
    }
    InInterfaceType & ccsaCopy1 = ccsCopy.getField(0, 0);
    if(! (ccsaCopy1.getN() == 0 && ccsaCopy1.getM() == 0) )
    {
      std::cerr << "Empty Array in Struct has wrong size!" << std::endl;
      ok = false;
    }
    ccsCopy.getField(0, 1).getString(s);
    if(! s.compare("string in struct") == 0)
    {
      std::cerr << "String in struct read in uncorrectly!" << std::endl;
      ok = false;
    }
    InInterfaceType & ccscCopy1 = ccsCopy.getField(1, 0);
    if(! (ccscCopy1.getN() == 0 && ccscCopy1.getM() == 0) )
    {
      std::cerr << "Empty Cell in Struct has wrong size!" << std::endl;
      ok = false;
    }
    InInterfaceType & ccsaCopy2 = ccsCopy.getField(1, 1);
    InMatrixStorageType & cmatrixCopy2 = ccsaCopy2.getMatrix();
    if(! cmatrixCopy2(0,0) == 42)
    {
      std::cerr << "Wrong answer!" << std::endl;
      ok = false;
    }

    if(ok)
    {
      std::cout << "Passed" << std::endl;
      std::cout.flush();
    }
    else
    {
      std::cerr << "Failed" << std::endl;
      std::cerr.flush();
    }

    delete cCopy;
    delete c;
    return ok;
  }
private:
  std::string from_;
  std::string to_;
};


int main (int argc, char const* argv[])
{
  //  const mxArray * cmarray = mxCreateString("Hallo Welt");
  //  const MXArray * cMxArray = MXArray::wrapMXArray(cmarray);
  //  std::cout << cMxArray->getCString();
  int oldmask = umask(0022);
  typedef RBArrayFactory<CMatrixWrapper> CFactoryType;
  typedef MXArrayFactory MatlabFactoryType;
  SerializeArrayTest<CFactoryType, CFactoryType> cmcmtest("CMatrix", "CMatrix");
  SerializeArrayTest<MatlabFactoryType, MatlabFactoryType> mxmxtest("MXArray", "MXArray");
  SerializeArrayTest<CFactoryType, MatlabFactoryType> cmmxtest("CMatrix", "MXArray");
  SerializeArrayTest<MatlabFactoryType, CFactoryType> mxcmtest("MXArray", "CMatrix");
#ifdef HAVE_EIGEN
  typedef RBArrayFactory<Eigen::MatrixXd> EigenFactoryType;
  SerializeArrayTest<EigenFactoryType, EigenFactoryType> ememtest("Eigen", "Eigen");
  SerializeArrayTest<CFactoryType, EigenFactoryType> cmemtest("CMatrix", "Eigen");
  SerializeArrayTest<EigenFactoryType, CFactoryType> emcmtest("Eigen", "CMatrix");
  SerializeArrayTest<EigenFactoryType, MatlabFactoryType> emmxtest("Eigen", "MXArray");
  SerializeArrayTest<MatlabFactoryType, EigenFactoryType> mxemtest("MXArray", "Eigen");
#endif
  int ret = 0;
  ret += (cmcmtest.run() ? 0 : 1);
  ret += (mxmxtest.run() ? 0 : 1);
  ret += (cmmxtest.run() ? 0 : 1);
  ret += (mxcmtest.run() ? 0 : 1);
#ifdef HAVE_EIGEN
  ret += (ememtest.run() ? 0 : 1);
  ret += (cmemtest.run() ? 0 : 1);
  ret += (emcmtest.run() ? 0 : 1);
  ret += (emmxtest.run() ? 0 : 1);
  ret += (mxemtest.run() ? 0 : 1);
#endif
  umask(oldmask);
  return ret;
}

