#ifndef __DUNE_RB_SERIALIZEDMXARRAY_HH_
#define __DUNE_RB_SERIALIZEDMXARRAY_HH_

#include <string>
#include <iostream>
#include <sstream>
#include <cstdarg>
#include <dune/common/exceptions.hh>
#include <dune/common/static_assert.hh>

#include <dune/rb/matlabcomm/sockets.hh>

#include "cmatrixwrapper.hh"

extern "C" void exit (int);

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{

class Error: public std::exception
{
public:
  Error (const std::string msg)
      : msg_(msg)
  {
  }
  ;

  Error (const Error & copy)
      : msg_(copy.msg_)
  {
  }
  ;

  virtual ~Error () throw ()
  {
  }

  /**
   * @brief returns the error exception message
   *
   * @return  explanation of the exception
   */
  virtual const char* what () const throw ()
  {
    std::ostringstream oss;
    oss << "MatlabComm::SerializedArray::Error: " << msg_;
    return oss.str().c_str();
  }

private:
  std::string msg_;
};

/**
 * @brief structure defining commands used by staticRecvFD and sendFD methods
 * in order to encode/decode the type of send/received ISerializedArray
 * object.
 */
struct ArrayProtocol
{
  static const char Cell = 'c';
  static const char Matrix = 'm';
  static const char String = 's';
  static const char Struct = 'f';
};

/** @ingroup serialization
 * @brief  wraps a pointer to an object of type "T" which can only be copied if
 * the enveloped pointer is NULL, and deletes the object it points to on
 * destruction.
 *
 * A simple usage Example:
 *
 * @code
 *   ISerializedArray * mxPtr = MXArrayFactory :: staticRecvFd( sockfd );
 * @endcode
 *
 * creates a pointer mxPtr to an MXArray that destroys the MXArray created
 * with staticRecvFD on destruction of the SingleUsePtr object
 */
template< class T >
class SingleUsePtr
{
  typedef SingleUsePtr< T > ThisType;

public:
  /**
   * @brief The copy constructor: Only NULL pointers are allowed to be copied!
   *
   * @param copy  the object to be copied.
   */
  SingleUsePtr (const ThisType & copy)
      : ptr_(copy.ptr_)
  {
    if (ptr_ != NULL || copy.ptr_ != NULL)
    {
      std::cerr << "SingleUsePtr not allowed to be copied!" << std::endl;
      throw(Error("SingleUsePtr is not allowed to be copied here!"));
    }
  }
  ;
public:
  /**
   * @brief default constructor wrapping a NULL Pointer
   */
  SingleUsePtr ()
      : ptr_(NULL)
  {
  }

  /**
   * @brief assignment operator for pointers of wrapped type "T"
   *
   * This is only allowed, if the current pointer has not been set!
   *
   * @return  this object
   */
  ThisType & operator= (T * ptr)
  {
    if (ptr_ == NULL)
    {
      ptr_ = ptr;
    }
    return *this;
  }

  ~SingleUsePtr ()
  {
    if (ptr_ != NULL)
    {
      /*#ifndef NDEBUG
       *      std :: cout << "Delete SingleUsePtr:" << ptr_ << std::endl;
       *#endif*/
      delete ptr_;
      ptr_ = NULL;
    }
  }

  /**
   * @brief  dereference operator
   *
   * @return  the dereferenced wrapped object
   */
  T & operator* ()
  {
    return *ptr_;
  }

  /**
   * @brief  dereference operator for const objects
   *
   * @return  the dereferenced wrapped object
   */
  const T & operator* () const
  {
    return *ptr_;
  }
private:
  T * ptr_;
};

/** @ingroup serialization
 * @brief enumeration of allowed types specifying the role of an ISerializedArray
 * object.
 */
enum ArrayEnum
{
  Cell, Matrix, String, Struct, Undefined, Unknown
};

/** @ingroup serialization
 * @brief Abstract base class for a data structure that can be serialized, i.e.
 * interchanged between two processes.
 *
 * This class's implementations (RBArray, MXArray) mimick some properties of
 * Matlab's mxArray data structure that we need in dune-rb. In detail it
 * defines an interface for creating and reading of strings, matrices of double
 * precision floating pointer numbers and cells and structures that may contain
 * any of these types.
 *
 * @tparam Impl   Implementation of a serializable array
 *
 * @see RBArray, MXArray
 */
template< class CMatrixImp = SerializedArray::CMatrixWrapper >
class Interface: public MatlabComm::Sockets::Base
{
public:

  //! Implementation of stored matrix type
  typedef CMatrixImp CMatrixType;
  //! Implementation
  typedef Interface ArrayType;
  //! integer type for serialization
  typedef MatlabComm::Sockets::Base::sockssize_t sockssize_t;
  //! integer type used for indices
  typedef sockssize_t IndexType;
  //! structure for a matrix size specification
  typedef struct
  {
    IndexType M, N;
  } ArraySizeHeader;

public:
  /**
   * @brief constructor defining the created data structure's type
   *
   * @param type    the created data structure's type
   *
   * @see DataType
   */
  Interface (ArrayEnum type)
      : MatlabComm::Sockets::Base(connected), type_(type)
  {
  }
  ;

  /**
   * @brief special matlab array that represents an error
   */
  Interface ()
      : MatlabComm::Sockets::Base(connected), type_(SerializedArray::Undefined)
  {
  }
  ;

  //! destructor
  virtual ~Interface ()
  {
  }
  ;

  /**
   * @brief return the data structure's type
   *
   * @return the data structure's type
   */
  ArrayEnum getType () const
  {
    return type_;
  }

  /**
   * @brief returns the number of rows in the matrix of the data structure
   *
   * This function is only implemented for data structures of type Matrix, Cell
   * and Struct
   *
   * @return  number of rows
   */
  virtual IndexType getN () const = 0;

  /**
   * @brief returns the number of columns in the matrix of the data structure
   *
   * This function is only implemented for data structures of type Matrix, Cell
   * and Struct
   *
   * @return  number of columns
   */
  virtual IndexType getM () const = 0;

  /**
   * @brief sends a character string representing the data structure through a
   * network channel.
   *
   * @param sockfd  a socket number specifying the used network channel
   */
  virtual void sendFD (int sockfd) const = 0;

  /**
   * @brief listens on a network channel for a character string from which a
   * data structure can be created.
   *
   * @param[in] sockfd  a socket number specifying the used network channel
   */
  virtual void recvFD (int sockfd) = 0;

  /**
   * @brief returns a pointer to a new data structure which is deep copy of
   * this one.
   *
   * @return  pointer to the deep copied data structure
   */
  virtual Interface< CMatrixImp > * copy () const = 0;

  /**
   * @brief returns a reference to the stored matrix
   *
   * This function is only implemented for data structures of type Matrix
   *
   * @return  a reference to the stored matrix
   */
  virtual CMatrixImp & getMatrix ()
  {

    throw(SerializedArray::Error("getMatrix not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns a constant reference to the stored matrix
   *
   * This function is only implemented for data structures of type Matrix
   *
   * @return  a pointer to the stored matrix
   */
  virtual const CMatrixImp & getMatrix () const
  {

    throw(Error("getMatrix not implemented for this type! Sorry!"));
  }

  /**
   * @brief fills cell of a cell matrix with another serializable array.
   *
   * This function is only implemented for data structures of type Cell
   *
   * @param i        index number of the cell where the new data structure of
   *                 type Cell is to be created
   * @param array    pointer to another serializable array
   */
  virtual void setCell (const IndexType i, Interface * array)
  {

    throw(Error("setCell not implemented for this type! Sorry!"));
  }

  /**
   * @brief sets a field in a data structure of type Struct to another serializable array
   *
   * This function is only implemented for data structures of type Struct
   *
   * @param i           index number of the matrix entry where the new data
   *                    structure is to be created.
   * @param field       index number of the field where the new data structure
   *                    is to be created.
   * @param array       pointer to another serializable array
   */
  virtual void setField (const IndexType i, const IndexType field,
                         Interface *array)
  {

    throw(Error("setField not implemented for this type! Sorry!"));
  }

  /**
   * @brief specialization of setField() for "scalar" structures
   *
   * If the you do not use the structure as a Matrix, this is the right function to
   * set Fields.
   * @param field       index number of the field where the new data structure
   *                    is to be created.
   * @param array       pointer to another serializable array
   */
  void setField (const IndexType field, Interface *array)
  {
    setField(0, field, array);
  }

  /**
   * @brief returns the contents of a cell in a data structure of type Cell
   *
   * This function is only implemented for data structures of type Cell
   *
   * @param i  index number of the cell which contents are be returned
   *
   * @return   Array stored in cell "i"
   */
  virtual ArrayType & getCell (const IndexType i)
  {

    throw(Error("getCell not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns the contents of a cell in a data structure of type Cell
   *
   * This function is only implemented for data structures of type Cell
   *
   * @param i  index number of the cell which contents are be returned
   *
   * @return   Array stored in cell "i"
   */
  virtual const ArrayType & getCell (const IndexType i) const
  {

    throw(Error("getCell not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns the index of a field name in a data structure of type
   * Struct
   *
   * This function is only implemented for data structures of type Struct.
   *
   * @param[in] s  name of the field whose index is requested
   *
   * @return       index number, if no field named "s" exists,
   *               getNumberOfFields() is returned, which is an invalid index
   *               number for a field.
   */
  virtual IndexType getFieldNumber (const std::string & s) const
  {

    throw(Error("getFieldNumber not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns the contents of a field in a data structure of type Struct
   *
   * This function is only implemented for data structures of type Struct
   *
   * @param[in] i      index number of the matrix entry where the requested
   *                   contents reside.
   * @param[in] field  index number of the field of the requested contents
   *
   * @return           contents of the field named "field" in matrix cell "i"
   */
  virtual ArrayType & getField (const IndexType i, const IndexType field)
  {

    throw(Error("getField not implemented for this type! Sorry!"));
  }

  /**
   * @brief specialization of getField() for "scalar" structures
   *
   * If the you do not use the structure as a Matrix, this is the right function to
   * retrieve Fields.
   *
   * @param[in] field  index number of the field of the requested contents
   */
  ArrayType & getField (const IndexType field)
  {
    return getField(0, field);
  }

  /**
   * @brief specialization of getField() for "scalar" structures
   *
   * If the you do not use the structure as a Matrix, this is the right function to
   * retrieve Fields.
   *
   * @param[in] field  index number of the field of the requested contents
   */
  const ArrayType & getField (const IndexType field) const
  {
    return getField(0, field);
  }

  /**
   * @brief returns the contents of a field in a data structure of type Struct
   *
   * This function is only implemented for data structures of type Struct
   *
   * @param[in] i      index number of the matrix entry where the requested
   *                   contents reside.
   * @param[in] field  index number of the field of the requested contents
   *
   * @return           contents of the field named "field" in matrix cell "i"
   */
  virtual const ArrayType & getField (const IndexType i,
                                      const IndexType field) const
  {

    throw(Error("getField not implemented for this type! Sorry!"));
  }

  /**
   * @brief gets a std::string out of a data structure of type String
   *
   * This function is only implemented for data structures of type String.
   *
   * @param[out] s  the requested character string
   */
  virtual void getString (std::string & s) const
  {

    throw(Error("getString not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns a pointer to the character string represented by a data
   * structure of type String.
   *
   * This function is only implemented for data structures of type String
   *
   * @return  the represented character string
   */
  virtual const char * getCString () const
  {

    throw(Error("getCString not implemented for this type! Sorry!"));
  }

  /**
   * @brief returns the number of fields in the data structure of type Struct
   *
   * This function is only implemented for data structures of type Struct
   *
   * @return  number of fields
   */
  virtual IndexType getNumberOfFields () const
  {

    throw(Error("getNumberOfFields not implemented for this type! Sorry!"));
  }

  void getVectorOfStrings (std::vector< std::string > & vOfS) const
  {
    if (!isA(Cell))
    {
      throw(Error("in getVectorOfStrings: argument must be a Cell of Strings"));
    }
    vOfS.resize(getN() * getM());
    for (IndexType i = 0; i < getM() * getN(); ++i)
    {
      const ArrayType & string = getCell(i);
      if (!string.isA(String))
        throw(Error("in getVectorOfStrings: argument must be a Cell of Strings"));

      vOfS[i] = string.getCString();
    }
  }

  /**
   * @brief checks whether the Array is of a special DataType
   */
  bool isA (const ArrayEnum type) const
  {
    return (type == type_);
  }

protected:
  void copyFieldNames (IndexType numFields, const char ** fieldnames,
                       const char ** & copy, std::string & fnString)
  {
    std::ostringstream oss;
    unsigned int offsets[numFields + 1];
    offsets[0] = 0;
    for (unsigned int i = 0; i < numFields; ++i)
    {
      std::string tmpField = fieldnames[i];
      offsets[i + 1] = offsets[i] + tmpField.size() + 1;
      oss << tmpField << '\0';
    }
    fnString.assign(oss.str());
    const char * fnCharStr = fnString.c_str();
    copy = new const char*[numFields];
    for (unsigned int i = 0; i < numFields; ++i)
    {
      copy[i] = &fnCharStr[offsets[i]];
    }
  }

protected:
  //! type of data structure
  ArrayEnum type_;
};

template< class Impl >
class IFactory
{
public:
  typedef IFactory< Impl > ThisType;
  typedef typename Impl::MatrixStorageType MatrixStorageType;
  typedef typename Impl::MatrixType MatrixType;
  typedef typename Impl::CellType CellType;
  typedef typename Impl::StructType StructType;
  typedef typename Impl::StringType StringType;
  typedef typename Impl::ArrayType ArrayType;
  typedef typename Impl::InterfaceType InterfaceType;
  typedef typename InterfaceType::IndexType IndexType;
public:

  static InterfaceType * createScalar (double scalar)
  {
    InterfaceType * matrix = new MatrixType(1, 1);
    matrix->getMatrix()(0, 0) = scalar;
    return matrix;
  }

  static InterfaceType * createMatrix ()
  {
    return new MatrixType();
  }

  static InterfaceType * createMatrix (IndexType rows, IndexType columns)
  {
    return new MatrixType(rows, columns);
  }

  static InterfaceType * createMatrix (
      SerializedArray::CMatrixWrapper * cmatrix)
  {
    return new MatrixType(cmatrix);
  }

  static InterfaceType * createVector (
      std::vector<double> vec)
  {
    InterfaceType * arrayVec = ThisType::createMatrix(vec.size(), 1);
    MatrixStorageType & cm = arrayVec->getMatrix();
    for (unsigned int i = 0; i < vec.size(); ++i)
    {
      cm(i, 0) = vec[i];
    }
    return arrayVec;
  }

  static InterfaceType * createString ()
  {
    return new StringType();
  }

  static InterfaceType * createString (const std::string & s)
  {
    return new StringType(s);
  }

  static InterfaceType * createCell (const IndexType rows, IndexType columns)
  {
    return new CellType(rows, columns);
  }

  static InterfaceType * createCell ()
  {
    return new CellType();
  }

  static InterfaceType * createCellOfStrings (
      const std::vector< std::string > & values)
  {
    InterfaceType * res = ThisType::createCell(1, values.size());
    for (unsigned int i = 0; i < values.size(); ++i)
    {
      InterfaceType * string = ThisType::createString(values[i]);
      res->setCell(i, string);
    }
    return res;
  }

  static InterfaceType * createCellOfMatrices (
      const std::vector< SerializedArray::CMatrixWrapper * > & values)
  {
    InterfaceType * res = ThisType::createCell(1, values.size());
    for (unsigned int i = 0; i < values.size(); ++i)
    {
      InterfaceType * matrix = ThisType::createMatrix(values[i]);
      res->setCell(i, matrix);
    }
    return res;
  }

  static InterfaceType * createStruct ()
  {
    return new StructType();
  }

  static InterfaceType * createStruct (const IndexType rows,
                                       const IndexType columns,
                                       const IndexType numFields,
                                       const char ** fieldnames)
  {
    return new StructType(rows, columns, numFields, fieldnames);
  }

  static InterfaceType * createStruct (const IndexType numFields,
                                       const char ** fieldnames)
  {
    return new StructType(1, 1, numFields, fieldnames);
  }

  static InterfaceType * createStruct (
      const IndexType rows, const IndexType columns,
      const std::vector< std::string > & fieldNames)
  {
    std::ostringstream oss;
    unsigned int numFields = fieldNames.size();
    unsigned int offsets[numFields];
    offsets[0] = 0;
    for (unsigned int i = 0; i < numFields; ++i)
    {
      offsets[i + 1] = offsets[i] + fieldNames[i].size() + 1;
      oss << fieldNames[i] << '\0';
    }
    const std::string & fnStr = oss.str();
    const char * fnCharStr = fnStr.c_str();
    const char * fieldnames[numFields];
    for (unsigned int i = 0; i < numFields; ++i)
    {
      fieldnames[i] = &fnCharStr[offsets[i]];
    }
    InterfaceType * ret = new StructType(rows, columns, numFields,
        (const char **) fieldnames);
    return ret;
  }

  static InterfaceType * createStruct (
      const std::vector< std::string > & fieldNames)
  {
    return ThisType::createStruct(1, 1, fieldNames);
  }

  /**
   * @brief listens on a network channel for a character string from which a
   * data structure can be created.
   *
   * This method first receives a command character that says which type the
   * new data structure has.
   *
   * @see ArrayProtocol
   *
   * @param[in] sockfd  a socket number specifying the used network channel
   *
   * @return            a pointer to a newly created data structure
   */
  static InterfaceType * staticRecvFD (int sockfd)
  {
    char command;
    InterfaceType *ret;

    std::ostringstream oss;
    MatlabComm::Sockets::Base(MatlabComm::Sockets::Base::connected).recv_chunk(
        sockfd, command, "MXArray: command");
    switch (command)
    {
    case ArrayProtocol::Cell:
      ret = ThisType::createCell();
      break;
    case ArrayProtocol::Matrix:
      ret = ThisType::createMatrix();
      break;
    case ArrayProtocol::String:
      ret = ThisType::createString();
      break;
    case ArrayProtocol::Struct:
      ret = ThisType::createStruct();
      break;
    case MatlabComm::Sockets::Protocol::ErrMsg:
      oss
        << "Protocol error: expected one of commands: \'c\',\'m\',\'s\' or \'h\' got \'";
      oss << command << "\'\n";
      throw(MatlabComm::Sockets::Error(oss.str(), false));
      break;
      /*    return new InterfaceType(Error);*/
      /*    sockssize_t len;
       *    RBSocks(connected).recv_chunk( sockfd, len, "error: length" );
       *    char buf[len];
       *    RBSocks(connected).recv_chunk( sockfd, buf[0], "error: text", false, len*sizeof(char) );
       *    throw( RBSocksError(buf, false) );
       *    return new MXString(buf);*/
    default:
      oss
        << "Protocol error: expected one of commands: \'c\',\'m\',\'s\' or \'h\' got \'";
      oss << command << "\'\n";
      throw(MatlabComm::Sockets::Error(oss.str(), false));
      /*    return new InterfaceType(Unknown);*/
    }
    ret->recvFD(sockfd);
    return ret;
  }
};

}  // namespace SerializedArray
}  // namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* __DUNE_RB_SERIALIZEDMXARRAY_HH_ */
/* vim: set et sw=2: */

