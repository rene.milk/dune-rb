#include <config.h>
#include "std.hh"

#ifdef HAVE_EIGEN
#include <fstream>
#include <Eigen/Core>
#endif

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{

/* RBMatrix */
template< class MatrixStorageImp >
RBMatrix< MatrixStorageImp >::RBMatrix (const IndexType rows,
                                        const IndexType columns)
    : BaseType( SerializedArray::ArrayEnum::Matrix ), M_( rows ), N_( columns ),
      cmatrix_( NULL )
{
  cmatrix_ = new MatrixStorageImp( M_, N_ );
}

template< class MatrixStorageImp >
RBMatrix< MatrixStorageImp >::~RBMatrix ()
{
  if (cmatrix_ != NULL)
  {
    delete cmatrix_;
  }
}

template< class MatrixStorageImp >
typename RBMatrix< MatrixStorageImp >::InterfaceType *
RBMatrix< MatrixStorageImp >::copy () const
{
  ThisType * ret = new ThisType( M_, N_ );
  MatrixStorageImp *& retcmatrix = ret->cmatrix_;
  retcmatrix = new MatrixStorageImp( *cmatrix_ );
  return ret;
}

template< class MatrixStorageImp >
void RBMatrix< MatrixStorageImp >::sendFD (int sockfd) const
{
  char command = ArrayProtocol::Matrix;
  this->send_chunk( sockfd, command, "RBMatrix: header(command)" );

  typename Interface< MatrixStorageImp >::ArraySizeHeader header;
  header.M = M_;
  header.N = N_;
  send_chunk( sockfd, header, "RBMatrix: header" );
  assert(
      !MatrixStorageImp::IsRowMajor && "overwrite RBMatrix::recvFD() and RBMatrix::sendFD() for dense matrices with row-major storage order." );
  double * data = cmatrix_->data();
  if (M_ * N_ > 0)
  {
    send_chunk( sockfd, data[0], "RBMatrix: data", M_ * N_ * sizeof(double) );
  }
}

template< class MatrixStorageImp >
void RBMatrix< MatrixStorageImp >::recvFD (int sockfd)
{
  assert( M_*N_ == 0 );
  assert( cmatrix_ == NULL );
  typename Interface< MatrixStorageImp >::ArraySizeHeader header;

  recv_chunk( sockfd, header, "RBMatrix: header" );

  M_ = header.M;
  N_ = header.N;

  if (M_ * N_ > 0)
  {
    cmatrix_ = new MatrixStorageImp( M_, N_ );
    assert(
        !MatrixStorageImp::IsRowMajor && "overwrite RBMatrix::recvFD() and RBMatrix::sendFD() for dense matrices with row-major storage order." );
    double * data = cmatrix_->data();

    assert( cmatrix_ != NULL );
    recv_chunk( sockfd, data[0], "RBMatrix: data", false,
                M_ * N_ * sizeof(double) );
  }
}

/* RBString */

template< class MatrixStorageImp >
void RBString< MatrixStorageImp >::recvFD (int sockfd)
{
  assert( s_.size() == 0 );
  IndexType size;
  recv_chunk( sockfd, size, "RBString: length" );
  /* note size == 1 means empty string */
  assert( size > 0 );

  char buf[size];
  recv_chunk( sockfd, buf[0], "RBString: data", false, size * sizeof(char) );
  s_.assign( buf );
}

template< class MatrixStorageImp >
void RBString< MatrixStorageImp >::sendFD (int sockfd) const
{
  char command = ArrayProtocol::String;
  this->send_chunk( sockfd, command, "RBString: header(command)" );
  IndexType size = s_.capacity() + 1;
  send_chunk( sockfd, size, "RBString: header" );

  const char * buf = s_.c_str();
  send_chunk( sockfd, buf[0], "RBString: data", size * sizeof(char) );
}

/* RBCell */

template< class MatrixStorageImp >
void RBCell< MatrixStorageImp >::sendFD (int sockfd) const
{
  char command = ArrayProtocol::Cell;
  this->send_chunk( sockfd, command, "RBCell: header(command)" );

  typename Interface< MatrixStorageImp >::ArraySizeHeader header;

  header.M = M_;
  header.N = N_;

  send_chunk( sockfd, header, "RBCell: header" );

  for (IndexType i = 0; i < M_ * N_; ++i)
  {
    const Interface< MatrixStorageImp > & cell = getCell( i );
    cell.sendFD( sockfd );
  }
}

template< class MatrixStorageImp >
void RBCell< MatrixStorageImp >::setCell (const IndexType i,
                                          InterfaceType *array)
{
  assert( i < M_*N_ );
  CellPtrType & cell = cells_[i];
  cell = array;
}

template< class MatrixStorageImp >
void RBCell< MatrixStorageImp >::recvFD (int sockfd)
{
  assert( M_*N_ == 0 );
  typename Interface< MatrixStorageImp >::ArraySizeHeader header;
  this->recv_chunk( sockfd, header, "RBCell: header" );
  M_ = header.M;
  N_ = header.N;
  cells_.resize( M_ * N_ );
  for (IndexType i = 0; i < M_ * N_; ++i)
  {
    CellPtrType & c = cells_[i];
    c = RBArrayFactory< MatrixStorageImp >::staticRecvFD( sockfd );
  }
}

/* RBStruct */
template< class MatrixStorageImp >
void RBStruct< MatrixStorageImp >::sendFD (int sockfd) const
{
  char command = ArrayProtocol::Struct;
  this->send_chunk( sockfd, command, "RBStruct: header(command)" );
  typename Interface< MatrixStorageImp >::ArraySizeHeader header;
  header.M = M_;
  header.N = N_;
  send_chunk( sockfd, header, "RBStruct: header" );
  send_chunk( sockfd, numFields_, "RBStruct: numfields" );
  for (size_t i = 0; i < numFields_; ++i)
  {
    const std::string & s = description_[i];
    IndexType size = s.capacity() + 1;
    send_chunk( sockfd, size, "RBStruct: fieldname header" );
    const char * buf = s.c_str();
    send_chunk( sockfd, buf[0], "RBstruct: fieldname", size * sizeof(char) );
  }
  for (size_t i = 0; i < numFields_; ++i)
  {
    const FieldMatrixType & fm = fieldstorage_[i];
    for (size_t j = 0; j < N_ * M_; ++j)
    {
      const FieldPtrType & fptr = fm[j];
      const Interface< MatrixStorageImp > & field = *fptr;
      field.sendFD( sockfd );
    }
  }
}

template< class MatrixStorageImp >
void RBStruct< MatrixStorageImp >::setField (const IndexType i,
                                             const IndexType field,
                                             InterfaceType *array)
{
  assert( i < M_*N_ );
  assert( field < numFields_ );
  FieldMatrixType & fm = fieldstorage_[field];
  FieldPtrType & fptr = fm[i];
  fptr = array;
}

template< class MatrixStorageImp >
void RBStruct< MatrixStorageImp >::recvFD (int sockfd)
{
  assert( M_*N_ == 0 );
  assert( numFields_ == 0 );
  typename Interface< MatrixStorageImp >::ArraySizeHeader header;

  this->recv_chunk( sockfd, header, "RBStruct: header" );

  M_ = header.M;
  N_ = header.N;

  recv_chunk( sockfd, numFields_, "RBStruct: numfields" );

  fieldstorage_.resize( numFields_, FieldMatrixType( M_ * N_ ) );
  description_.resize( numFields_ );

  for (size_t i = 0; i < numFields_; ++i)
  {
    IndexType size;
    recv_chunk( sockfd, size, "RBStruct: fieldname header" );
    char buf[size];
    recv_chunk( sockfd, buf[0], "RBStruct: fieldname", false, size );
    description_[i].assign( buf );
    fieldMap_.insert( make_pair( description_[i], i ) );
  }

  for (size_t i = 0; i < numFields_; ++i)
  {
    FieldMatrixType & fm = fieldstorage_[i];
    for (size_t j = 0; j < N_ * M_; ++j)
    {
      FieldPtrType & fptr = fm[j];
      fptr = RBArrayFactory< MatrixStorageImp >::staticRecvFD( sockfd );
    }
  }
}

template class RBMatrix< CMatrixWrapper > ;
template class RBCell< CMatrixWrapper > ;
template class RBStruct< CMatrixWrapper > ;
template class RBString< CMatrixWrapper > ;

template class RBMatrix< RMatrixWrapper > ;
template class RBCell< RMatrixWrapper > ;
template class RBStruct< RMatrixWrapper > ;
template class RBString< RMatrixWrapper > ;

#ifdef HAVE_EIGEN
template class RBMatrix<Eigen::MatrixXd>;
template class RBCell<Eigen::MatrixXd>;
template class RBStruct<Eigen::MatrixXd>;
template class RBString<Eigen::MatrixXd>;
#endif

} // end of namespace SerializedArray
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

/* vim: set et sw=2 */

