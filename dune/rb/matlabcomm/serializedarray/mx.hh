#ifndef __DUNE_RB_MXARRAY_HH_
#define __DUNE_RB_MXARRAY_HH_

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#define char16_t char16_t_backup
#include <mex.h>
#include <matrix.h>
#undef char16_t

#include "interface.hh"
namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{

/** @ingroup serialization
 * @brief  A base class for wrapper classes for the Matlab data structure
 * mxArray that have serialization methods recvFD() and sendFD().
 */
class MXArray: public SerializedArray::Interface< CMatrixWrapper >
{
public:
  typedef SerializedArray::Interface< CMatrixWrapper > BaseType;
public:
  /**
   * @brief default constructor creating an empty array with no data in it.
   * Such a structure can later be filled with a recvFD() call.
   */
  MXArray (SerializedArray::ArrayEnum type)
      : BaseType(type), mxPtr_(NULL), cmxPtr_(NULL)
  {
  }

  /**
   * @brief constructor that is used for wrapping an already existing mxArray
   *
   * @note The memory allocated at the address mxArray pointer points to is not
   * automatically deallocated by the MXArray destructor. Use the free() method
   * for this.
   *
   * @param type   the type of the data structure
   * @param mxPtr  pointer to an mxArray
   */
  MXArray (SerializedArray::ArrayEnum type, mxArray * mxPtr)
      : BaseType(type), mxPtr_(mxPtr), cmxPtr_(mxPtr_)
  {
  }

  /**
   * @brief constructor that is used for wrapping an already existing mxArray
   * with constant data.
   *
   * @param type   the type of the data structure
   * @param mxPtr  pointer to a constant mxArray
   */
  MXArray (SerializedArray::ArrayEnum type, const mxArray * mxPtr)
      : BaseType(type), mxPtr_(NULL), cmxPtr_(mxPtr)
  {
  }

  virtual ~MXArray ()
  {
  }
  ;

  /**
   * @copydoc SerializedArray::Interface::sendFD
   */
  virtual void sendFD (int sockfd) const
  {
    throw(MatlabComm::Sockets::Error("use of unimplemented method in MXArray!"));
  }

  /**
   * @copydoc SerializedArray::Interface::recvFD
   */
  virtual void recvFD (int sockfd)
  {
    throw(MatlabComm::Sockets::Error("use of unimplemented method in MXArray!"));
  }

  /**
   * @brief frees the memory at the place where the wrapped mxArray pointer
   * points to.
   */
  void free ()
  {
    if (mxPtr_ == NULL)
    {
      throw(SerializedArray::Error(
          "During MXArray::free(): mxPtr_ was not assigned!"));
    }
    mxFree(mxPtr_);
    mxPtr_ = NULL;
  }

  /**
   * @brief wraps an mxArray of unknown type in an MXArray and returns a
   * pointer to the created object on the heap.
   *
   * @param mxu  pointer to the mxArray of unknown type
   *
   * @return     pointer to the wrapping MXArray
   */
  static MXArray * wrapMXArray (mxArray * mxu);

  /**
   * @brief wraps an mxArray of unknown type in an MXArray and returns a
   * pointer to the created object on the heap.
   *
   * @param mxu  pointer to the mxArray of unknown type
   *
   * @return     pointer to the wrapping MXArray (const)
   */
  static const MXArray * wrapMXArray (const mxArray * mxu);

  /**
   * @brief returns a pointer to the wrapped mxArray
   *
   * @return pointer to mxArray
   */
  mxArray * getMxPtr ()
  {
    return mxPtr_;
  }

  /**
   * @copydoc MXArray :: getMxPtr
   */
  const mxArray * getMxPtr () const
  {
    return cmxPtr_;
  }

  void updateMxPtr (mxArray * array)
  {
#ifdef DEBUG
    if (array != mxPtr_)
    std::cerr << "In updateMxPtr: True thing! The pointers really differ!";
#endif
    mxPtr_ = array;
    cmxPtr_ = array;
  }
protected:

  inline void assert_field_index (IndexType field) const
  {
    if (field >= getNumberOfFields())
      throw(SerializedArray::Error("Field index out of bounds!"));
  }

  inline void assert_array_index (IndexType i) const
  {
    if (i >= getM() * getN())
      throw(SerializedArray::Error("Array index out of bounds!"));
  }

  inline void assert_empty_mx_ptr () const
  {
    if (cmxPtr_ != NULL)
      throw(SerializedArray::Error(
          "MXArray is not expected to be complete at this point!"));
  }

  inline void assert_valid_mx_ptr ()
  {
    if (mxPtr_ == NULL)
      throw(SerializedArray::Error(
          "Writable MXArray has not been fully constructed yet.\n"
              "Maybe you forgot to add a const specifier. This is necessary if initializing with const mxArray *"));
  }

  inline void assert_valid_mx_ptr () const
  {
    if (cmxPtr_ == NULL)
      throw(SerializedArray::Error(
          "MXArray has not been fully constructed yet. mxPtr_ is not set!"));
  }

  void setMxPtr (mxArray * array)
  {
    mxPtr_ = array;
    cmxPtr_ = array;
  }

protected:
  //! pointer to wrapped mxArray
  mxArray *mxPtr_;
  const mxArray *cmxPtr_;
};

/**
 * @brief an MXArray of type Array
 */
class MXMatrix: public MXArray
{
public:
  /**
   * @brief default constructor creating an empty matrix of double precision
   * floating point numbers  with no data in it.  Such a structure can later be
   * filled via a recvFD() call.
   */
  MXMatrix ()
      : MXArray(SerializedArray::Matrix)
  {
  }

  /**
   * @brief wraps an mxArray that the class id mxDOUBLE_CLASS
   *
   * @param mxa  pointer to the mxArray
   */
  MXMatrix (mxArray * mxa);

  /**
   * @brief wraps an mxArray that the class id mxDOUBLE_CLASS but cannot be
   * changed
   *
   * @param mxa  pointer to the mxArray
   */
  MXMatrix (const mxArray * mxa);

  /**
   * @brief creates a new mxArray with mxCreateDoubleMatrix and wraps it inside
   * an MXMatrix class
   *
   * @param rows      number of rows inside the matrix
   * @param columns   number of columns inside the matrix
   */
  MXMatrix (const IndexType rows, const IndexType columns)
      : MXArray(SerializedArray::Matrix,
            mxCreateDoubleMatrix(rows, columns, mxREAL)),
        rmatrix_(mxGetPr(mxPtr_), mxGetM(mxPtr_), mxGetN(mxPtr_))
  {
  }

  /**
   * @brief wraps a SerializedArray::CMatrixWrapper pointer object as a MXMatrix
   *
   * @note the argument's storage is deleted after it has been copied into the
   *  MXMatrix. So, you can see it invalidated afterwards.
   *
   * @param cmatrix   A MatrixStorageImp pointer object that needs to be copied
   */
  MXMatrix (SerializedArray::CMatrixWrapper * cmatrix)
      : MXArray(SerializedArray::Matrix,
            mxCreateDoubleMatrix(cmatrix->rows(), cmatrix->cols(), mxREAL)),
        rmatrix_(mxGetPr(mxPtr_), mxGetM(mxPtr_), mxGetN(mxPtr_))
  {
    double * pr = mxGetPr(mxPtr_);
    memcpy(reinterpret_cast< char * >(pr), reinterpret_cast< char * >(cmatrix->data()),
                 cmatrix->rows() * cmatrix->cols() * sizeof(double));
    delete cmatrix;
  }

  virtual ~MXMatrix ();

  /**
   * @copydoc SerializedArray::Interface::getPr()
   */
  CMatrixWrapper & getMatrix ()
  {
    assert_valid_mx_ptr();
    return rmatrix_;
  }

  /**
   * @copydoc SerializedArray::Interface::getPr()
   */
  const CMatrixWrapper & getMatrix () const
  {
    assert_valid_mx_ptr();
    return rmatrix_;
  }

  /**
   * @copydoc SerializedArray::Interface::getN()
   */
  IndexType getN () const
  {
    assert_valid_mx_ptr();
    return mxGetN(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getM()
   */
  IndexType getM () const
  {
    assert_valid_mx_ptr();
    return mxGetM(getMxPtr());
  }
  /*  CMatrixWrapper getCMatrixWrapper ()
   *  {
   *    CMatrixWrapper cmWrapper(M, N, pr_, 0.0);
   *    return cmWrapper;
   *  }*/

  /**
   * @copydoc SerializedArray::Interface::sendFD()
   */
  void sendFD (int sockfd) const;

  /**
   * @copydoc SerializedArray::Interface::recvFD()
   */
  void recvFD (int sockfd);

  SerializedArray::Interface< CMatrixWrapper > * copy () const
  {
    mxArray * duplicate = mxDuplicateArray(this->getMxPtr());
    return new MXMatrix(duplicate);
  }
private:
  inline void assert_double_type () const
  {
    if (mxGetClassID(cmxPtr_) != mxDOUBLE_CLASS)
      throw(SerializedArray::Error(
          "MXMatrix must be initialized with array of class mxDOUBLE_CLASS"));
  }
private:
  CMatrixWrapper rmatrix_;
};

/**
 * @brief an MXArray of type String
 */
class MXString: public MXArray
{
public:
  /**
   * @brief default constructor creating an empty data structure of type String
   * Such a structure can later be filled via a recvFD() call.
   */
  MXString ()
      : MXArray(SerializedArray::String), cptr_(NULL)
  {
  }

  /**
   * @brief creates an mxArray via a call to mxCreateString an wraps it inside
   * an MXString object
   *
   * @param text  the character string that is stored inside the MXString
   */
  MXString (const char * text)
      : MXArray(SerializedArray::String, mxCreateString(text)),
        cptr_(mxArrayToString(getMxPtr()))
  {
  }

  /**
   * @brief wraps an already existing mxArray of class type mxCHAR_CLASS
   *
   * @param mxs   pointer to the string type mxArray
   */
  MXString (mxArray * mxs)
      : MXArray(SerializedArray::String, mxs), cptr_(mxArrayToString(mxs))
  {
    assert_string_type(); //( mxGetClassID(mxs) == mxCHAR_CLASS );
  }

  /**
   * @brief wraps an already existing mxArray of class type mxCHAR_CLASS
   *
   * @param mxs   pointer to the string type mxArray
   */
  MXString (const mxArray * mxs)
      : MXArray(SerializedArray::String, mxs), cptr_(mxArrayToString(mxs))
  {
    assert_string_type(); //( mxGetClassID(mxs) == mxCHAR_CLASS );
  }

  /**
   * @brief creates an mxArray via a call to mxCreateString an wraps it inside
   * an MXString object
   *
   * @param s  the character string that is stored inside the MXString
   */
  MXString (const std::string & s)
      : MXArray(SerializedArray::String, mxCreateString(s.c_str())),
        cptr_(mxArrayToString(getMxPtr()))
  {
  }

  IndexType getN () const
  {
    return 1;
  }

  IndexType getM () const
  {
    return 1;
  }

  virtual ~MXString ()
  {
    if (cptr_ != NULL)
    {
      mxFree(cptr_);
    }
  }

  /**
   * @brief returns the C character string stored inside the MXString object
   *
   * @return  C charachter string
   */
  const char * getCString () const
  {
    return cptr_;
  }

  /**
   * @brief returns the character string represented by the object as a
   * std::string object
   *
   * @param[out] s   std::string storing the character string stored inside the
   *                 MXString object.
   */
  void getString (std::string & s) const
  {
    if (cptr_ == NULL)
      s.assign("");
    else
      s.assign(cptr_);
  }

  /**
   * @copydoc SerializedArray::Interface::recvFD()
   */
  virtual void recvFD (int sockfd);

  /**
   * @copydoc SerializedArray::Interface::sendFD()
   */
  virtual void sendFD (int sockfd) const;

  SerializedArray::Interface< CMatrixWrapper > * copy () const
  {
    mxArray * duplicate = mxDuplicateArray(this->getMxPtr());
    return new MXString(duplicate);
  }
private:
  inline void assert_string_type () const
  {
    if (mxGetClassID(cmxPtr_) != mxCHAR_CLASS)
      throw(SerializedArray::Error(
          "MXString must be initialized with array of class mxCHAR_CLASS"));
  }

private:
  char * cptr_;
};

/**
 * @brief an MXArray of type Cell
 */
class MXCell: public MXArray
{
private:
  //! pointer type pointing to an MXArray stored in a cell
  typedef SingleUsePtr< MXArray > CellPtrType;
  //! vector storing the MXArrays stored in the cells
  typedef std::vector< CellPtrType > CellStorageType;
public:
  /**
   * @brief default constructor creating an empty data structure of type Cell
   * Such a structure can later be filled via a recvFD() call.
   */
  MXCell ()
      : MXArray(SerializedArray::Cell), cells_()
  {
  }

  /**
   * @brief wraps an already existing mxArray of class type mxCELL_CLASS
   *
   * @param mxc   pointer to the string type mxArray
   */
  MXCell (mxArray * mxc)
      : MXArray(SerializedArray::Cell, mxc), cells_(mxGetM(mxc) * mxGetN(mxc))
  {
    assert_cell_type(); // ( mxGetClassID(mxc) == mxCELL_CLASS );
    for (size_t i = 0; i < cells_.size(); ++i)
    {
      CellPtrType & cell = cells_[i];
      cell = MXArray::wrapMXArray(mxGetCell(getMxPtr(), i));
    }
  }

  /**
   * @brief wraps an already existing mxArray of class type mxCELL_CLASS
   *
   * @param mxc   pointer to the string type mxArray
   */
  MXCell (const mxArray * mxc)
      : MXArray(SerializedArray::Cell, mxc), cells_(mxGetM(mxc) * mxGetN(mxc))
  {
    assert_cell_type(); //( mxGetClassID(mxc) == mxCELL_CLASS );
    for (size_t i = 0; i < cells_.size(); ++i)
    {
      CellPtrType & cell = cells_[i];
      cell = MXArray::wrapMXArray(mxGetCell(cmxPtr_, i));
    }
  }

  /**
   * @brief creates an mxArray via a call to mxCreateCellMatrix an wraps it
   * inside an MXCell object
   *
   * @param rows     number of rows in the new cell matrix
   * @param columns  number of columns in the new cell matrix
   */
  MXCell (const IndexType rows, const IndexType columns)
      : MXArray(SerializedArray::Cell, mxCreateCellMatrix(rows, columns)),
        cells_(rows * columns)
  {
    assert_cell_type(); //( mxGetClassID(getMxPtr()) == mxCELL_CLASS );
    for (size_t i = 0; i < cells_.size(); ++i)
    {
      CellPtrType & cell = cells_[i];
      cell = NULL;
    }
  }

  virtual ~MXCell ()
  {
    cells_.clear();
  }

  /**
   * @copydoc SerializedArray::Interface::setCell()
   */
  virtual void setCell (const IndexType i, BaseType * mxArray);

  /**
   * @copydoc SerializedArray::Interface::getN()
   */
  IndexType getN () const
  {
    return mxGetN(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getM()
   */
  IndexType getM () const
  {
    return mxGetM(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getCell()
   */
  BaseType & getCell (const IndexType i)
  {
    return *cells_[i];
  }

  /**
   * @copydoc SerializedArray::Interface::getCell()
   */
  const BaseType & getCell (const IndexType i) const
  {
    return *cells_[i];
  }

  /**
   * @copydoc SerializedArray::Interface::sendFD()
   */
  void sendFD (int sockfd) const;

  /**
   * @copydoc SerializedArray::Interface::recvFD()
   */
  void recvFD (int sockfd);

  BaseType * copy () const
  {
    mxArray * duplicate = mxDuplicateArray(this->getMxPtr());
    return new MXCell(duplicate);
  }
private:
  inline void assert_cell_type () const
  {
    if (mxGetClassID(cmxPtr_) != mxCELL_CLASS)
      throw(SerializedArray::Error(
          "MXCell must be initialized with array of class mxCELL_CLASS"));
  }
private:
  CellStorageType cells_;
};

/**
 * @brief an MXArray of type Struct
 */
class MXStruct: public MXArray
{
private:
  //! pointer type pointing to an MXArray stored in a field
  typedef SingleUsePtr< MXArray > FieldPtrType;
  //! vector type for storing the MXArrays stored in the fields
  typedef std::vector< FieldPtrType > FieldMatrixType;
  //! vector storing the matrix entries (i.e. vectors of fields)
  typedef std::vector< FieldMatrixType > FieldStorageType;

public:
  /**
   * @brief default constructor creating an empty data structure of type Struct
   * Such a structure can later be filled via a recvFD() call.
   */
  MXStruct ()
      : MXArray(SerializedArray::Struct), fieldnames_(NULL), fieldstorage_()
  {
  }

  /**
   * @brief wraps an already existing mxArray of class type mxSTRUCT_CLASS
   *
   * @param mxs   pointer to the string type mxArray
   */
  MXStruct (mxArray * mxs, IndexType fields, const char ** fieldnames)
      : MXArray(SerializedArray::Struct, mxs),
        fieldstorage_(mxGetNumberOfFields(mxs),
            FieldMatrixType(mxGetM(mxs) * mxGetN(mxs)))
  {
    copyFieldNames(fields, fieldnames, fieldnames_, fnString_);
    assert_valid_mx_ptr();
    assert_struct_type(); //( mxGetClassID(mxs) == mxSTRUCT_CLASS );
    for (size_t i = 0; i < fieldstorage_.size(); ++i)
    {
      FieldMatrixType & fm = fieldstorage_[i];
      for (size_t j = 0; j < fm.size(); ++j)
      {
        FieldPtrType & field = fm[j];
        field = MXArray::wrapMXArray(
            mxGetFieldByNumber(getMxPtr(), mwIndex(j), mwIndex(i)));
      }
    }
  }

  /**
   * @brief wraps an already existing mxArray of class type mxSTRUCT_CLASS
   *
   * @param mxs   pointer to the string type mxArray
   */
  MXStruct (const mxArray * mxs, IndexType fields, const char ** fieldnames)
      : MXArray(SerializedArray::Struct, mxs),
        fieldstorage_(mxGetNumberOfFields(mxs),
            FieldMatrixType(mxGetM(mxs) * mxGetN(mxs)))
  {
    copyFieldNames(fields, fieldnames, fieldnames_, fnString_);
    assert_struct_type(); //( mxGetClassID(mxs) == mxSTRUCT_CLASS );
    for (size_t i = 0; i < fieldstorage_.size(); ++i)
    {
      FieldMatrixType & fm = fieldstorage_[i];
      for (size_t j = 0; j < fm.size(); ++j)
      {
        FieldPtrType & field = fm[j];
        field = MXArray::wrapMXArray(
            mxGetFieldByNumber(cmxPtr_, mwIndex(j), mwIndex(i)));
      }
    }
  }

  /**
   * @brief creates an mxArray via a call to mxCreateStructMatrix an wraps it
   * inside an MXCell object
   *
   * @param rows        number of rows in the new struct matrix
   * @param columns     number of columns in the new struct matrix
   * @param numFields   number of fields in the new struct matrix
   * @param fieldnames  pointer to character strings describing the field names
   */
  MXStruct (const IndexType rows, const IndexType columns,
            const IndexType numFields, const char ** fieldnames)
      : MXArray(SerializedArray::Struct),
        fieldstorage_(numFields, FieldMatrixType(rows * columns))
  {
    copyFieldNames(numFields, fieldnames, fieldnames_, fnString_);
    setMxPtr(mxCreateStructMatrix(rows, columns, numFields, fieldnames_));
    assert_valid_mx_ptr();
    assert_struct_type(); //( mxGetClassID(getMxPtr()) == mxSTRUCT_CLASS );
    for (size_t i = 0; i < fieldstorage_.size(); ++i)
    {
      FieldMatrixType & fm = fieldstorage_[i];
      for (size_t j = 0; j < fm.size(); ++j)
      {
        FieldPtrType & field = fm[j];
        field = NULL;
      }
    }
  }

  virtual ~MXStruct ()
  {
    if (fieldnames_ != NULL)
    {
      delete[] fieldnames_;
    }
    fieldstorage_.clear();
  }

  /**
   * @copydoc SerializedArray::Interface::setField()
   */
  virtual void setField (const IndexType i, const IndexType field,
                         BaseType * array);

  /**
   * @copydoc SerializedArray::Interface::getN()
   */
  IndexType getN () const
  {
    assert_valid_mx_ptr();
    return mxGetN(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getM()
   */
  IndexType getM () const
  {
    assert_valid_mx_ptr();
    return mxGetM(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getNumberOfFields()
   */
  IndexType getNumberOfFields () const
  {
    assert_valid_mx_ptr();
    return mxGetNumberOfFields(getMxPtr());
  }

  /**
   * @copydoc SerializedArray::Interface::getField()
   */
  BaseType & getField (const IndexType i, const IndexType field)
  {
    assert_valid_mx_ptr();
    assert_array_index(i);
    assert_field_index(field);
    FieldMatrixType & fm = fieldstorage_[field];
    return *fm[i];
  }

  /**
   * @copydoc SerializedArray::Interface::getField()
   */
  const BaseType & getField (const IndexType i, const IndexType field) const
  {
    assert_valid_mx_ptr();
    assert_array_index(i);
    assert_field_index(field);
    const FieldMatrixType & fm = fieldstorage_[field];
    return *fm[i];
  }

  /**
   * @copydoc SerializedArray::Interface::getFieldNumber()
   */
  IndexType getFieldNumber (const std::string & s) const
  {
    assert_valid_mx_ptr();
    IndexType ret = mxGetFieldNumber(getMxPtr(), s.c_str());
    if (ret == (IndexType) -1)
      return getNumberOfFields();
    else
      return ret;
  }

  /**
   * @copydoc SerializedArray::Interface::sendFD()
   */
  void sendFD (int sockfd) const;

  /**
   * @copydoc SerializedArray::Interface::recvFD()
   */
  void recvFD (int sockfd);

  BaseType * copy () const
  {
    mxArray * duplicate = mxDuplicateArray(this->getMxPtr());
    return new MXStruct(duplicate, this->getNumberOfFields(), this->fieldnames_);
  }
private:
  inline void assert_struct_type () const
  {
    if (mxGetClassID(cmxPtr_) != mxSTRUCT_CLASS)
      throw(SerializedArray::Error(
          "MXStruct must be initialized with array of class mxSTRUCT_CLASS"));
  }
private:
  const char ** fieldnames_;
  std::string fnString_;
  FieldStorageType fieldstorage_;
};

/** @brief Factory for creation of MXArray's
 *
 */
struct MXArrayFactoryTraits
{
  typedef CMatrixWrapper MatrixStorageType;
  typedef MXMatrix MatrixType;
  typedef MXCell CellType;
  typedef MXStruct StructType;
  typedef MXString StringType;
  typedef MXArray ArrayType;
  typedef SerializedArray::Interface< > InterfaceType;
};

struct MXArrayFactory: public SerializedArray::IFactory< MXArrayFactoryTraits >
{
  typedef MXArrayFactoryTraits Traits;
};

} // end of namespace SerializedArray
} // end of namespace MatlabComm
} // end of namespace RB
} // end of namespace Dune

#endif /* __DUNE_RB_MXARRAY_HH_ */
/* vim: set et sw=2: */

