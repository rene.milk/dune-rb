#ifndef DUNE_RB_MATLABCOMM_SERIALIZEDARRAY_CMATRIXWRAPPER_HH__
#define DUNE_RB_MATLABCOMM_SERIALIZEDARRAY_CMATRIXWRAPPER_HH__

#include <cassert>
#include <cstddef>
#include <cstring>
#include <cmath>
#include <iostream>
#include <iomanip>

#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#endif

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{

/** @brief interprets a double array as a matrix with column-wise stored data.
 */
template<class Derived>
class IMatrixWrapper
{

public:

  IMatrixWrapper()
  : entries_ptr_(NULL), rows_(0), cols_(0), own_storage_(false)
  {
  };

  IMatrixWrapper( const IMatrixWrapper & copy )
     : entries_ptr_(NULL), rows_(copy.rows_), cols_(copy.cols_), own_storage_(true)
  {
    if(rows_*cols_ > 0)
    {
      entries_ptr_ = new double[rows_*cols_];
      memcpy(reinterpret_cast< char * >(entries_ptr_), reinterpret_cast< char * >(copy.entries_ptr_),
             rows_ * cols_ * sizeof(double));
    }
  }

  IMatrixWrapper(const size_t rows, const size_t cols)
     : entries_ptr_(NULL), rows_(rows), cols_(cols), own_storage_(true)
  {
    if(rows_*cols_ > 0)
      entries_ptr_ = new double[rows_*cols_];
  }
  /**
   * @brief constructor setting data to zero
   *
   * @param entries_ptr  pointer to (pre-allocated) double array
   * @param rows         number of rows
   * @param cols         number of columns
   */
  IMatrixWrapper (double * entries_ptr, const size_t rows, const size_t cols)
      : entries_ptr_( entries_ptr ), rows_( rows ), cols_( cols ), own_storage_(false)
  {
  }

//  /**
//   * @brief constructor setting data to a given double value
//   *
//   * @param entries_ptr  pointer to (pre-allocated) double array
//   * @param rows         number of rows
//   * @param cols         number of columns
//   * @param value        the initial value for all entries
//   */
//  IMatrixWrapper (double * entries_ptr, const size_t rows, const size_t cols,
//                  double value)
//      : entries_ptr_( entries_ptr ), rows_( rows ), cols_( cols ), own_storage_(false)
//  {
//    clear( value );
//  }

  virtual ~IMatrixWrapper()
  {
    if(own_storage_ && entries_ptr_ != NULL)
      delete entries_ptr_;
  }

  inline void clear (double value = 0.0)
  {
    size_t i;
    for (i = 0; i < rows_ * cols_; ++i)
    {
      entries_ptr_[i] = value;
    }
  }


  inline void init(const size_t rows, const size_t cols, double * pr)
  {
    assert(cols_ == 0 && rows_ == 0);
    assert(entries_ptr_ == NULL);
    assert(!own_storage_);
    entries_ptr_ = pr;
    rows_ = rows;
    cols_ = cols;
  }

  /**
   * @brief add scalar to matrix entry
   *
   * @param i  row position
   * @param j  column position
   * @param val  addent
   */
  void add (const size_t i, const size_t j, const double &val)
  {
    (*this)(i,j) += val;
  }

  /**
   * @brief set matrix entry
   *
   * @param i    row position
   * @param j    column position
   * @param val  new value
   */
  void set (const size_t i, const size_t j, const double &val)
  {
    (*this)(i,j) = val;
  }

  /**
   * @brief get matrix entry
   *
   * @param i    row position
   * @param j    column position
   * @param val  returned matrix value
   */
  void get (const size_t i, const size_t j, double &val) const
  {
    val = (*this)(i,j);
  }

  double & operator() (const size_t i, const size_t j)
  {
    Derived & derived = static_cast<Derived &>(*this);
    return derived(i, j);
  }

  /**
   * @brief get matrix entry
   *
   * @param i row position
   * @param j column position
   * @return returns a constant reference to the matching entry in the matrix
   */
  const double& operator() (const size_t i, const size_t j) const
  {
    const Derived & derived = static_cast<const Derived &>(*this);
    return derived(i, j);
  }

  const double * data() const
  {
    return entries_ptr_;
  }

  double * data()
  {
    return entries_ptr_;
  }

#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
  boost::test_tools::predicate_result
#else
  bool
#endif
  compare (const IMatrixWrapper & cmp, double smallEps = 8e-13)
  {
    if (rows() != cmp.rows() || cols() != cmp.cols())
    {
#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
      boost::test_tools::predicate_result res( false );
      res.message () << "Different number of rows in matrices: ("
      << rows() << ", " << cols() << ") != (" << cmp.rows() << ", " << cmp.cols() << ")";
      return res;
#else
      return false;
#endif
    }
    for (unsigned int i = 0; i < rows_; i++)
    {
      for (unsigned int j = 0; j < cols_; j++)
      {
        double val1;
        double val2;
        get( i, j, val1 );
        cmp.get( i, j, val2 );
#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
        if(! (boost::test_tools::check_is_close(val1,val2,boost::test_tools::percent_tolerance(0.00001))
                || ( boost::test_tools::check_is_small(val1, smallEps)
                    && boost::test_tools::check_is_small(val2, smallEps) ) )
        )
        {
          boost::test_tools::predicate_result res( false );
          if (cols_ < 500 && rows_ < 500)
          {
            std::ostringstream temp1, temp2;
            print(temp1);
            cmp.print(temp2);
            res.message() << "\nthis matrix:\n" << temp1.str() << "\nreference matrix:\n" << temp2.str();
          }
          res.message() << "\nmatrix entries differ at (" << i << ", " << j << "): ["
          << val1 << " != " << val2 << "]";
          return res;
        }
#else
        if (std::fabs( val1 - val2 ) > 1e-10)
        {
          return false;
        }
#endif
      }
    }
#if defined(HAVE_BOOST) && defined(BOOST_TEST_MODULE)
    boost::test_tools::predicate_result res( true );
    return res;
#else
    return true;
#endif
  }

  void print (std::ostream & os) const
  {
    os << "[ ";
    for (unsigned int i = 0; i < rows_; i++)
    {
      os << "  ";
      for (unsigned int j = 0; j < cols_; j++)
      {
        double val;
        get( i, j, val );
        os << std::setw( 12 ) << val;
        if (j != cols_ - 1)
        {
          os << ",";
        }
      }
      if (i != rows_ - 1)
        os << " ;...\n";
      else
        os << " ]\n";
    }
  }

  /**
   * @brief number of rows in matrix
   *
   * @return  double
   */
  size_t rows () const
  {
    return rows_;
  }

  /**
   * @brief number of columns in matrix
   *
   * @return  double
   */
  size_t cols () const
  {
    return cols_;
  }

  /**@brief Print the matrix to an outstream.
   *
   * @param out an ostream*/
  template< class OutStream >
  void print (OutStream &out) const
  {
    for (size_t row = 0; row != rows_; ++row)
    {
      out << "|";
      for (size_t col = 0; col != cols_; ++col)
      {
        out << (*this)(rows_,cols) << " ";
      }
      out << "|" << std::endl;
    }
    out << std::endl;
  }

protected:
  double * entries_ptr_;
  size_t rows_;
  size_t cols_;
  bool own_storage_;
};

class RMatrixWrapper;

class CMatrixWrapper : public IMatrixWrapper<CMatrixWrapper>
{
public:
  static const bool IsRowMajor = false;
private:
  typedef IMatrixWrapper<CMatrixWrapper> BaseType;
public:
  CMatrixWrapper()
    : BaseType()
  {
  }

  CMatrixWrapper( const RMatrixWrapper & transposedCopy );

  CMatrixWrapper( const CMatrixWrapper & copy )
    : BaseType(copy)
  {
  }

  CMatrixWrapper(const size_t rows, const size_t cols)
    : BaseType(rows, cols)
  {
  }

  CMatrixWrapper (double *entries_ptr, const size_t rows, const size_t cols)
    : BaseType(entries_ptr, rows, cols)
  {
  }

  virtual ~CMatrixWrapper()
  {
  }

  double & operator() (const size_t i, const size_t j)
  {
    assert( i < rows_ && j < cols_ && entries_ptr_ != NULL );
    assert( i * cols_+j < rows_*cols_ );
    return entries_ptr_[i + j * rows_];
  }

  const double & operator() (const size_t i, const size_t j) const
  {
    assert( i < rows_ && j < cols_ && entries_ptr_ != NULL );
    assert( i * cols_+j < rows_*cols_ );
    return entries_ptr_[i + j * rows_];
  }

};

class RMatrixWrapper : public IMatrixWrapper<RMatrixWrapper>
{
public:
  static const bool IsRowMajor = true;
private:
  typedef IMatrixWrapper<RMatrixWrapper> BaseType;
public:
  RMatrixWrapper()
    : BaseType()
  {
  }

  RMatrixWrapper( const CMatrixWrapper & transposedCopy );

  RMatrixWrapper( const RMatrixWrapper & copy )
    : BaseType(copy)
  {
  }

  RMatrixWrapper(const size_t rows, const size_t cols)
    : BaseType(rows, cols)
  {
  }

  RMatrixWrapper (double *entries_ptr, const size_t rows, const size_t cols)
    : BaseType(entries_ptr, rows, cols)
  {
  }

  virtual ~RMatrixWrapper()
  {
  }

  double & operator() (const size_t i, const size_t j)
  {
    assert( i < rows_ && j < cols_);
    assert( entries_ptr_ != NULL );
    assert( i * cols_+j < rows_*cols_ );
    return entries_ptr_[j+i*cols_];
  }

  const double & operator() (const size_t i, const size_t j) const
  {
    assert( i < rows_ && j < cols_ && entries_ptr_ != NULL );
    assert( i * cols_+j < rows_*cols_ );
    return entries_ptr_[j+i*cols_];
  }
};

} // end of namespace SerializedArray
} // end of namespace MatlabCommon
} // end of namespace RB
} // end of namespace Dune

#endif //__CMATRIXWRAPPER_HH__
