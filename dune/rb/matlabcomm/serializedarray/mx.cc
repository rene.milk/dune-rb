#include <config.h>
#include "mx.hh"

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{


/* MXArray */
MXArray * MXArray::wrapMXArray(mxArray * mxu)
{
  MXArray * ret;
  if (mxu == NULL) return NULL;

  switch (mxGetClassID(mxu))
  {
  case mxCHAR_CLASS:
    ret = new MXString(mxu);
    break;
  case mxDOUBLE_CLASS:
    ret = new MXMatrix(mxu);
    break;
  case mxCELL_CLASS:
    ret = new MXCell(mxu);
    break;
  case mxSTRUCT_CLASS:
    ret = new MXStruct(mxu, 0, NULL);
    break;
  case mxFUNCTION_CLASS:
    ret = new MXString("function_handle");
    break;
  default:
    std::ostringstream oss;
    oss << "Do not know, how to wrap objects of type: " << mxGetClassName(mxu)
      << std::endl;
    throw(MatlabComm::Sockets::Error(oss.str(), false));
    /*    ret = new ISerializedArray( Unknown );*/
  }
  return ret;
}

const MXArray * MXArray::wrapMXArray(const mxArray * mxu)
{
  const MXArray * ret;
  if (mxu == NULL) return NULL;

  switch (mxGetClassID(mxu))
  {
  case mxCHAR_CLASS:
    ret = new const MXString(mxu);
    break;
  case mxDOUBLE_CLASS:
    ret = new const MXMatrix(mxu);
    break;
  case mxCELL_CLASS:
    ret = new const MXCell(mxu);
    break;
  case mxSTRUCT_CLASS:
    ret = new const MXStruct(mxu, 0, NULL);
    break;
  case mxFUNCTION_CLASS:
    ret = new const MXString("function_handle");
    break;
  default:
    std::ostringstream oss;
    oss << "Do not know, how to wrap objects of type: " << mxGetClassName(mxu)
      << std::endl;
    throw(MatlabComm::Sockets::Error(oss.str(), false));
    /*    ret = new ISerializedArray(Unknown);*/
  }
  return ret;
}

/* MXMatrix */
MXMatrix::MXMatrix(mxArray * mxa)
    : MXArray(SerializedArray::Matrix, mxa),
      rmatrix_(mxGetPr(mxa), mxGetM(mxa), mxGetN(mxa))
{
  assert_double_type(); //( mxGetClassID(mxa) == mxDOUBLE_CLASS);
}

MXMatrix::MXMatrix(const mxArray * mxa)
    : MXArray(SerializedArray::Matrix, mxa),
      rmatrix_(mxGetPr(mxa), mxGetM(mxa), mxGetN(mxa))
{
  assert_double_type(); //( mxGetClassID(mxa) == mxDOUBLE_CLASS);
}

MXMatrix::~MXMatrix()
{
}

void MXMatrix::sendFD(int sockfd) const
{
  assert_valid_mx_ptr();
  char command = ArrayProtocol::Matrix;
  send_chunk(sockfd, command, "MXMatrix: header(command)");

  ArraySizeHeader header;
  header.M = mxGetM(getMxPtr());
  header.N = mxGetN(getMxPtr());
  send_chunk(sockfd, header, "MXMatrix: header");
  if (header.M * header.N > 0)
  {
    const CMatrixWrapper & rmatrix = getMatrix();
    const double * data = rmatrix.data();
    send_chunk(sockfd, data[0], "MXMatrix: data",
        header.N * header.M * sizeof(double));
  }
}

void MXMatrix::recvFD(int sockfd)
{
  assert_empty_mx_ptr();
  ArraySizeHeader header;

  recv_chunk(sockfd, header, "MXMatrix: header");

  setMxPtr(mxCreateDoubleMatrix(header.M, header.N, mxREAL));
  rmatrix_.init(header.M, header.N, mxGetPr(mxPtr_));
  assert_valid_mx_ptr();

  if (header.M * header.N > 0)
  {
    CMatrixWrapper & rmatrix = getMatrix();
    double * data = rmatrix.data();
    if (data == NULL)
      throw(SerializedArray::Error("mxArray has empty data!"));
    recv_chunk(sockfd, data[0], "MXMatrix: data", false,
        header.M * header.N * sizeof(double));
  }
}

/* MXString */
void MXString::recvFD(int sockfd)
{
  assert_empty_mx_ptr();
  IndexType size;
  recv_chunk(sockfd, size, "MXString: length");
  /* Note: size == 1 means the empty string */
  assert( size > 0);

  char buf[size];
  recv_chunk(sockfd, buf[0], "MXString: data", false, size * sizeof(char));
  setMxPtr(mxCreateString(buf));
  assert_valid_mx_ptr();
  cptr_ = mxArrayToString(getMxPtr());
}

void MXString::sendFD(int sockfd) const
{
  assert_valid_mx_ptr();
  char command = ArrayProtocol::String;
  send_chunk(sockfd, command, "MXString: header(command)");
  IndexType size = sizeof(char) + mxGetN(getMxPtr()) * mxGetM(getMxPtr());
  send_chunk(sockfd, size, "MXString: header");

  send_chunk(sockfd, cptr_[0], "MXString: data", size);
}

/* MXCell */

void MXCell::setCell(const IndexType i, BaseType * array)
{
  assert_valid_mx_ptr();
  assert (i < getM() * getN() );
  MXArray * mxArray = dynamic_cast<MXArray *>(array);
  mxSetCell(getMxPtr(), i, mxArray->getMxPtr());
  mxArray->updateMxPtr(mxGetCell(getMxPtr(), i));
  CellPtrType & cell = cells_[i];
  cell = mxArray;
}

void MXCell::sendFD(int sockfd) const
{
  char command = ArrayProtocol::Cell;
  send_chunk(sockfd, command, "MXCell: header(command)");

  ArraySizeHeader header;

  header.N = getN();
  header.M = getM();

  send_chunk(sockfd, header, "MXCell: header");

  for (IndexType i = 0; i < header.M * header.N; ++i)
  {
    const BaseType & cell = getCell(i);
    cell.sendFD(sockfd);
  }
}

void MXCell::recvFD(int sockfd)
{
  assert_empty_mx_ptr();
  ArraySizeHeader header;

  recv_chunk(sockfd, header, "MXCell: header");

  setMxPtr(mxCreateCellMatrix(header.M, header.N));
  assert_valid_mx_ptr();

  cells_.resize(header.M * header.N);

  for (IndexType i = 0; i < cells_.size(); ++i)
  {
    CellPtrType & c = cells_[i];
    c = static_cast<MXArray*>(MXArrayFactory::staticRecvFD(sockfd));
    MXArray & mxc = dynamic_cast< MXArray& >(*c);
    mxSetCell(getMxPtr(), i, mxc.getMxPtr());
  }
}

/* MXStruct */
void MXStruct::sendFD(int sockfd) const
{
  assert_valid_mx_ptr();
  const char command = ArrayProtocol::Struct;
  send_chunk(sockfd, command, "MXStruct: header(command)");

  ArraySizeHeader header;

  header.M = mxGetM(getMxPtr());
  header.N = mxGetN(getMxPtr());

  send_chunk(sockfd, header, "MXStruct: header");

  IndexType numFields = mxGetNumberOfFields(getMxPtr());
  send_chunk(sockfd, numFields, "MXStruct: numfields");

  for (size_t i = 0; i < numFields; ++i)
  {
    const char * fieldname = mxGetFieldNameByNumber(getMxPtr(), i);
    IndexType size = strlen(fieldname) + 1;
    send_chunk(sockfd, size, "MXStruct: fieldname header");
    send_chunk(sockfd, fieldname[0], "MXstruct: fieldname",
        size * sizeof(char));
  }

  for (size_t i = 0; i < numFields; ++i)
  {
    const FieldMatrixType & fm = fieldstorage_[i];
    for (size_t j = 0; j < mxGetM(getMxPtr()) * mxGetN(getMxPtr()); ++j)
    {
      const FieldPtrType & fptr = fm[j];
      const BaseType & field = *fptr;
      field.sendFD(sockfd);
    }
  }
}

void MXStruct::setField(const IndexType i, const IndexType field,
                        BaseType *array)
{
  assert_valid_mx_ptr();
  assert_array_index(i);
  assert_field_index(field);
  FieldMatrixType & fm = fieldstorage_[field];
  FieldPtrType & fptr = fm[i];
  MXArray * mxArray = dynamic_cast< MXArray * >(array);
  mxSetFieldByNumber(getMxPtr(), mwIndex(i), mwIndex(field),
      mxArray->getMxPtr());
  mxArray->updateMxPtr(
      mxGetFieldByNumber(getMxPtr(), mwIndex(i), mwIndex(field)));
  fptr = mxArray;
}

void MXStruct::recvFD(int sockfd)
{
  assert_empty_mx_ptr();
  ArraySizeHeader header;

  recv_chunk(sockfd, header, "MXStruct: header");

  IndexType numFields;
  recv_chunk(sockfd, numFields, "MXStruct: numfields");

  fieldstorage_.resize(numFields, FieldMatrixType(header.M * header.N));

  char * fieldnames[numFields];
  for (size_t i = 0; i < numFields; ++i)
  {
    IndexType size;
    recv_chunk(sockfd, size, "MXStruct: fieldname header");
    fieldnames[i] = new char[size];
    recv_chunk(sockfd, fieldnames[i][0], "MXStruct: fieldname", false, size);
  }

  setMxPtr(
      mxCreateStructMatrix(mwSize(header.M), mwSize(header.N), numFields,
          const_cast< const char ** >(fieldnames)));

  for (size_t i = 0; i < numFields; ++i)
  {
    delete fieldnames[i];
  }

  for (size_t i = 0; i < numFields; ++i)
  {
    FieldMatrixType & fm = fieldstorage_[i];
    for (size_t j = 0; j < header.M * header.N; ++j)
    {
      FieldPtrType & fptr = fm[j];
      fptr = static_cast<MXArray*>(MXArrayFactory::staticRecvFD(sockfd));
      MXArray & mxfptr = dynamic_cast< MXArray& >(*fptr);
      mxSetFieldByNumber(getMxPtr(), j, i, mxfptr.getMxPtr());
    }
  }
}

} // end of namespace SerializedArray
} // end of namespace MatlabComm
} // end of namespace Dune :: RB
} // end of namespace Dune

/* vim: set et sw=2 */

