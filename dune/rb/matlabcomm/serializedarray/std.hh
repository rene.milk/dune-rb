#ifndef __DUNE_RB_RBARRAY_HH_
#define __DUNE_RB_RBARRAY_HH_

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <cstddef>
#include <cstring>

#include "interface.hh"
#include <dune/rb/matlabcomm/serializedarray/cmatrixwrapper.hh>

namespace Dune
{
namespace RB
{
namespace MatlabComm
{
namespace SerializedArray
{


/** @ingroup serialization
 * @brief A base class for classes mimicking some of the behaviour of the
 * Matlab data structure mxArray, additionally providing serialization methods
 * recvFD() and sendFD()
 */
template<class MatrixStorageImp = SerializedArray::CMatrixWrapper>
class RBArray: public SerializedArray::Interface<MatrixStorageImp>
{
public:
  typedef SerializedArray::Interface<MatrixStorageImp> BaseType;
private:
  //! this type
  typedef RBArray ThisType;
public:

  /**
   * @brief special RBArray that represents an error
   */
  RBArray()
  {
  }
  ;

  RBArray(SerializedArray::ArrayEnum type)
      : BaseType(type)
  {
  }
  ;

  virtual ~RBArray()
  {
  }
  ;

};

/**
 * @brief an RBArray of type Matrix
 */
template<class MatrixStorageImp = SerializedArray::CMatrixWrapper>
class RBMatrix: public RBArray<MatrixStorageImp>
{
public:
  typedef RBMatrix<MatrixStorageImp> ThisType;
  typedef RBArray<MatrixStorageImp> BaseType;
  typedef typename BaseType :: IndexType IndexType;
  typedef typename BaseType :: BaseType InterfaceType;
public:
  /**
   * @brief default constructor creating an empty matrix of double precision
   * floating point numbers  with no data in it.  Such a structure can later be
   * filled via a recvFD() call.
   */
  RBMatrix()
      : BaseType(SerializedArray::ArrayEnum::Matrix), M_(0), N_(0), cmatrix_(NULL)
  {
  }

  /**
   * @brief creates a new matrix of double precision floating point numbers
   * with specified size and allocated data storage.
   *
   * @param rows      number of rows inside the matrix
   * @param columns   number of columns inside the matrix
   */
  RBMatrix(const IndexType rows, const IndexType columns);

  /**
   * @brief wraps a MatrixStorageImp object as a RBMatrix
   *
   * @param cmatrix   A MatrixStorageImp object that needs to be wrapped
   */
  RBMatrix(MatrixStorageImp * cmatrix)
  : BaseType(SerializedArray::ArrayEnum::Matrix), M_(cmatrix->rows()), N_(cmatrix->cols()), cmatrix_(cmatrix)
  {
  }

  virtual ~RBMatrix();

  /**
   * @copydoc ISerializedArray::getPr()
   */
  virtual MatrixStorageImp & getMatrix()
  {
    return *cmatrix_;
  }

  /**
   * @copydoc ISerializedArray::getPr()
   */
  virtual const MatrixStorageImp & getMatrix() const
  {
    return *cmatrix_;
  }

  /**
   * @copydoc ISerializedArray::getN()
   */
  virtual IndexType getN() const
  {
    return N_;
  }

  /**
   * @copydoc ISerializedArray::getM()
   */
  virtual IndexType getM() const
  {
    return M_;
  }

  /**
   * @copydoc ISerializedArray::sendFD()
   */
  virtual void sendFD(int sockfd) const;

  /**
   * @copydoc ISerializedArray::recvFD()
   */
  virtual void recvFD(int sockfd);

  /**
   * @copydoc ISerializedArray::copy()
   */
  virtual InterfaceType * copy() const;

private:
  //! number of rows in the matrix
  IndexType M_;
  //! number of columns in the matrix
  IndexType N_;
  //! pointer to data storage
  MatrixStorageImp *cmatrix_;
};

/**
 * @brief an RBArray of type String
 */
template<class MatrixStorageImp = SerializedArray::CMatrixWrapper>
class RBString: public RBArray<MatrixStorageImp>
{
public:
  typedef RBArray<MatrixStorageImp> BaseType;
  typedef typename BaseType :: IndexType IndexType;
  typedef typename BaseType :: BaseType InterfaceType;
public:
  /**
   * @brief default constructor creating an empty data structure of type String
   * Such a structure can later be filled via a recvFD() call.
   */
  RBString()
      : BaseType(SerializedArray::ArrayEnum::String), s_()
  {
  }

  /**
   * @brief allocates space for a new RBString object and initializes it with a
   * given character string
   *
   * @param text  the std::string object that is to be stored inside the
   *              RBString
   */
  RBString(const std::string & text)
      : BaseType(SerializedArray::ArrayEnum::String), s_(text)
  {
  }

  virtual ~RBString()
  {
  }

  virtual IndexType getN() const
  {
    return 1;
  }

  virtual IndexType getM() const
  {
    return 1;
  }

  /**
   * @copydoc ISerializedArray::getCString()
   */
  virtual const char * getCString() const
  {
    return s_.c_str();
  }

  /**
   * @copydoc ISerializedArray::getString()
   */
  virtual void getString(std::string & s) const
  {
    s = s_;
  }

  /**
   * @copydoc ISerializedArray::recvFD()
   */
  virtual void recvFD(int sockfd);

  /**
   * @copydoc ISerializedArray::sendFD()
   */
  virtual void sendFD(int sockfd) const;

  /**
   * @copydoc ISerializedArray::copy()
   */
  virtual InterfaceType * copy() const
  {
    return new RBString(s_);
  }

private:
  std::string s_;
};

/**
 * @brief an RBArray of type Cell
 */
template<class MatrixStorageImp>
class RBCell: public RBArray<MatrixStorageImp>
{
public:
  typedef RBArray<MatrixStorageImp> BaseType;
  typedef typename BaseType :: IndexType IndexType;
  typedef typename BaseType :: BaseType InterfaceType;
private:
  //! pointer type pointing to an ISerializedArray stored in a cell
  typedef SerializedArray::SingleUsePtr< SerializedArray::Interface<MatrixStorageImp> > CellPtrType;
  //! vector storing the RBArrays stored in the cells
  typedef std::vector< CellPtrType > CellStorageType;
public:
  /**
   * @brief default constructor creating an empty data structure of type Cell
   * Such a structure can later be filled via a recvFD() call.
   */
  RBCell()
      : BaseType(SerializedArray::ArrayEnum::Cell), M_(0), N_(0), cells_()
  {
  }

  /**
   * @brief creates a cell matrix object of specified size
   *
   * @param rows     number of rows in the new cell matrix
   * @param columns  number of columns in the new cell matrix
   */
  RBCell(const IndexType rows, const IndexType columns)
      : BaseType(SerializedArray::ArrayEnum::Cell), M_(rows), N_(columns), cells_()
  {
    cells_.resize((unsigned long) (M_ * N_));
  }

  virtual ~RBCell()
  {
    cells_.clear();
  }

  /**
   * @copydoc ISerializedArray::setCell()
   */
  virtual void setCell(const IndexType i, InterfaceType * array);

  /**
   * @copydoc ISerializedArray::getN()
   */
  virtual IndexType getN() const
  {
    return N_;
  }

  /**
   * @copydoc ISerializedArray::getM()
   */
  virtual IndexType getM() const
  {
    return M_;
  }

  /**
   * @copydoc ISerializedArray::getCell()
   */
  virtual InterfaceType & getCell(const IndexType i)
  {
    return *cells_[i];
  }

  /**
   * @copydoc ISerializedArray::getCell()
   */
  virtual const InterfaceType & getCell(const IndexType i) const
  {
    return *cells_[i];
  }

  /**
   * @copydoc ISerializedArray::sendFD()
   */
  virtual void sendFD(int sockfd) const;

  /**
   * @copydoc ISerializedArray::recvFD()
   */
  virtual void recvFD(int sockfd);

  /**
   * @copydoc ISerializedArray::copy()
   */
  virtual InterfaceType * copy() const
  {
    RBCell * ret = new RBCell(M_, N_);
    for (size_t i = 0; i < M_ * N_; ++i)
    {
      ret->cells_[i] = (*(cells_[i])).copy();
    }
    return ret;
  }

private:
  //! number of rows in the cell matrix
  IndexType M_;
  //! number of columns in the cell matrix
  IndexType N_;
  //! storage object of cell pointers
  CellStorageType cells_;
};

/**
 * @brief an RBArray of type Struct
 */
template<class MatrixStorageImp>
class RBStruct: public RBArray<MatrixStorageImp>
{
public:
  typedef RBArray<MatrixStorageImp> BaseType;
  typedef typename BaseType :: IndexType IndexType;
  typedef typename BaseType :: BaseType InterfaceType;
private:
  //! pointer type pointing to an RBArray stored in a field
  typedef SingleUsePtr< SerializedArray::Interface<MatrixStorageImp> > FieldPtrType;
  //! vector type for storing the RBArrays stored in the fields
  typedef std::vector< FieldPtrType > FieldMatrixType;
  //! vector storing the matrix entries (i.e. vectors of fields)
  typedef std::vector< FieldMatrixType > FieldStorageType;
  //! vector type for storing the field description table
  typedef std::vector< std::string > DescriptionType;
  //! map from fieldname to field index
  typedef std::map< std::string, IndexType > FieldToIndexMapType;

public:
  /**
   * @brief default constructor creating an empty data structure of type Struct
   * Such a structure can later be filled via a recvFD() call.
   */
  RBStruct()
      : BaseType(SerializedArray::ArrayEnum::Struct), M_(0), N_(0), numFields_(0), fieldstorage_(),
        description_(), fieldMap_()
  {
  }

  /**
   * @brief creates a new struct matrix with specified size and field names
   *
   * @param rows        number of rows in the new struct matrix
   * @param columns     number of columns in the new struct matrix
   * @param numFields   number of fields in the new struct matrix
   * @param fieldnames  pointer to character strings describing the field names
   */
  RBStruct(const IndexType rows, const IndexType columns,
           const IndexType numFields, const char ** fieldnames)
      : BaseType(SerializedArray::ArrayEnum::Struct), M_(rows), N_(columns), numFields_(numFields),
        fieldstorage_(), description_(), fieldMap_()
  {
    description_.resize(numFields);
    fieldstorage_.resize(numFields, FieldMatrixType(M_ * N_));
    for (size_t i = 0; i < description_.size(); ++i)
    {
      description_[i] = fieldnames[i];
      fieldMap_.insert(make_pair(description_[i], i));
    }
  }

  virtual ~RBStruct()
  {
    fieldstorage_.clear();
  }

  /**
   * @copydoc ISerializedArray::setField()
   */
  virtual void setField(const IndexType i, const IndexType field, InterfaceType * array);

  /**
   * @copydoc ISerializedArray::getN()
   */
  virtual IndexType getN() const
  {
    return N_;
  }

  /**
   * @copydoc ISerializedArray::getM()
   */
  virtual IndexType getM() const
  {
    return M_;
  }

  /**
   * @copydoc ISerializedArray::getNumberOfFields()
   */
  virtual IndexType getNumberOfFields() const
  {
    return numFields_;
  }

  /**
   * @copydoc ISerializedArray::getField()
   */
  virtual InterfaceType & getField(const IndexType i, const IndexType field)
  {
    assert( i < M_*N_);
    assert( field < numFields_);
    FieldMatrixType & fm = fieldstorage_[field];
    return *fm[i];
  }

  /**
   * @copydoc ISerializedArray::getField()
   */
  virtual const InterfaceType & getField(const IndexType i,
                                    const IndexType field) const
  {
    assert( i < M_*N_);
    assert( field < numFields_);
    const FieldMatrixType & fm = fieldstorage_[field];
    return *fm[i];
  }

  /**
   * @copydoc ISerializedArray::getFieldNumber()
   */
  virtual IndexType getFieldNumber(const std::string & s) const
  {
    typedef typename FieldToIndexMapType::const_iterator const_iterator;
    const_iterator it = fieldMap_.find(s);
    if (it == fieldMap_.end())
      return numFields_;
    else
      return (*it).second;
  }

  /**
   * @copydoc ISerializedArray::sendFD()
   */
  virtual void sendFD(int sockfd) const;

  /**
   * @copydoc ISerializedArray::recvFD()
   */
  virtual void recvFD(int sockfd);

  /**
   * @copydoc ISerializedArray::copy()
   */
  virtual InterfaceType * copy() const
  {
    const char * fieldnames[numFields_];
    for (size_t i = 0; i < numFields_; ++i)
    {
      fieldnames[i] = description_[i].c_str();
    }

    RBStruct * ret = new RBStruct(M_, N_, numFields_, fieldnames);

    for (size_t i = 0; i < numFields_; ++i)
    {
      const FieldMatrixType & fm = fieldstorage_[i];
      FieldMatrixType & fmr = ret->fieldstorage_[i];
      for (size_t j = 0; j < N_ * M_; ++j)
      {
        const FieldPtrType & fptr = fm[j];
        FieldPtrType & frptr = fmr[j];

        frptr = (*fptr).copy();
      }
    }
    return ret;
  }

private:
  //! number of rows in the cell matrix
  IndexType M_;
  //! number of columns in the cell matrix
  IndexType N_;
  //! number of fields in the cell matrix
  IndexType numFields_;
  //! storage object of field pointers
  FieldStorageType fieldstorage_;
  //! table of field names
  DescriptionType description_;
  //! map from field names to field indices
  FieldToIndexMapType fieldMap_;
};

template<class MatrixStorageImp=SerializedArray::CMatrixWrapper>
struct RBArrayFactoryTraits
{
  typedef MatrixStorageImp MatrixStorageType;
  typedef RBMatrix<MatrixStorageImp> MatrixType;
  typedef RBCell<MatrixStorageImp> CellType;
  typedef RBStruct<MatrixStorageImp> StructType;
  typedef RBString<MatrixStorageImp> StringType;
  typedef RBArray<MatrixStorageImp> ArrayType;
  typedef SerializedArray::Interface<MatrixStorageImp> InterfaceType;
};

/** @brief Factory for creation of MXArray's
 *
 */
template<class MatrixStorageImp = SerializedArray::CMatrixWrapper>
struct RBArrayFactory : public SerializedArray::IFactory<RBArrayFactoryTraits<MatrixStorageImp>  >
{
  typedef RBArrayFactoryTraits<MatrixStorageImp> Traits;
};

} // end of namespace SerializedArrayEnum
} // end of namespace MatlabComm
} // end of namespace Dune :: RB
} // end of namespace Dune

#endif /* __DUNE_RB_RBARRAY_HH_ */
/* vim: set et sw=2: */

