
namespace Dune{

namespace RB{

namespace Reduced{

namespace Estimator{

class Interface
public:
  typedef VectorContainerImp
    VectorContainerType;

  void init()

  double estimate(const VectorContainerType& vector)

  void update()

}; // class Interface

} // namespace Estimator

} // namespace Reduced

} // namespace RB

} // namespace Dune
