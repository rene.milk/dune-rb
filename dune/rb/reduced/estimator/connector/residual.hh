#ifndef __DUNE_RB_REDUCED_ESTIMATOR_CONNECTOR_RESIDUAL_HH__
#define __DUNE_RB_REDUCED_ESTIMATOR_CONNECTOR_RESIDUAL_HH__

namespace Dune {
namespace RB {
namespace Reduced {
namespace Estimator {
namespace Connector {
template< class DofVectorImp, class DofMatrixImp, class ModelImp >
class ResidualConnector
{
public:
  typedef DofVectorImp                      DofVectorType;
  typedef DofMatrixImp                      DofMatrixType;
  typedef ModelImp                          ModelType;
  typedef typename ModelType::ParameterType ParameterType;

  ResidualConnector(const DofMatrixType& G,
                    const ParameterType muBar,
                    const DofVectorType subdomainBasisSizes,
                    const ModelType& model)
    : G_(G),
      muBar_(muBar),
      subdomainBasisSizes_(subdomainBasisSizes),
      model_(model),
      QLh_((model_.mobility().size() + 1) * model_.dirichletValues().size() + model_.neumannValues().size()),
      QBh_(model_.mobility().size())
  {}

  ResidualConnector(const ModelType& model)
    : model_(model),
      QLh_((model_.mobility().size() + 1) * model_.dirichletValues().size() + model_.neumannValues().size()),
      QBh_(model_.mobility().size())
  {}

  void init()
  {}
  void update()
  {}

  double estimate(const ParameterType& mu, const DofVectorType& vector) const
  {
    // compute the matching Theta for this reduced solution vector
    updateTheta(mu, vector);

    // compute squared error
    double squaredResidualNorm = Theta_.transpose() * G_ * Theta_;
    if (squaredResidualNorm < 0.0) {
      if (squaredResidualNorm < -1.0)
        std::cout << "Set error " << squaredResidualNorm << " to zero!\n";
      squaredResidualNorm = 0.0;
    }
    return sqrt(squaredResidualNorm) * computeFactor(mu);
  } // estimate


  double lowerBound(const ParameterType& mu, const DofVectorType& vector) const
  {
    // compute the matching Theta for this reduced solution vector
    updateTheta(mu, vector);

    // compute squared error
    double squaredResidualNorm = Theta_.transpose() * G_ * Theta_;
    if (squaredResidualNorm < 0.0) {
      if (squaredResidualNorm < -1.0)
        std::cout << "Set error " << squaredResidualNorm << " to zero!\n";
      squaredResidualNorm = 0.0;
    }

    return sqrt(squaredResidualNorm) * computeOtherFactor(mu);
  }   /* lowerBound */

  bool save(const std::string& path) const
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Reduced::Estimator::Connector::save is not a directory, not saving!\n";
      return false;
    }
    boost::filesystem::create_directories(path + "/estimator_connector");
    bool allSaved = (G_.saveToBinaryFile(path + "/estimator_connector/G.dat")
                     && muBar_.saveToBinaryFile(path + "/estimator_connector/muBar.dat")
                     && subdomainBasisSizes_.saveToBinaryFile(path + "/estimator_connector/subdomainBasisSizes.dat"));
    return allSaved;
  } // save

  bool load(const std::string& path)
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Reduced::Estimator::Connector::load is not a directory, not loading!\n";
      return false;
    }
    bool allLoaded = (G_.loadFromBinaryFile(path + "/estimator_connector/G.dat")
                     && muBar_.loadFromBinaryFile(path + "/estimator_connector/muBar.dat")
                     && subdomainBasisSizes_.loadFromBinaryFile(path + "/estimator_connector/subdomainBasisSizes.dat"));
    return allLoaded;
  } // load

private:
  void updateTheta(const ParameterType& mu, const DofVectorType& vector) const
  {
    Theta_ = DofVectorType::Zero(1 + QLh_ + vector.rows() * (QBh_ + 1));
    // write coefficients for Lh
    Theta_(0) = 1.0;

    const int Nlambda    = model_.mobility().size();
    const int numGdComps = model_.dirichletValues().size();

    for (int i = 0; i < int(model_.dirichletValues().size()); ++i) {
      const double muForGd = model_.dirichletValues().coefficient(i, mu);
      Theta_(1 + i) = muForGd;
      for (int opComp = 0; opComp < Nlambda; ++opComp) {
        const int index = numGdComps + i * Nlambda + opComp;
        Theta_(1 + index) = model_.mobility().coefficient(opComp, mu) * muForGd;
      }
    }
    for (int i = 0; i < int(model_.neumannValues().size()); ++i) {
      Theta_(1 + numGdComps + model_.dirichletValues().size() * Nlambda + i) = model_.neumannValues().coefficient(i, mu);
    }

    // write Coefficients for Bh
    int       k                = QLh_ + 1;
    const int sizeOfCoarseGrid = subdomainBasisSizes_.rows();
    const int maximumSize      = subdomainBasisSizes_.maxCoeff();

    for (int baseFunc = 0; baseFunc < maximumSize; ++baseFunc) {
      for (int subDomain = 0; subDomain != sizeOfCoarseGrid; ++subDomain) {
        if (baseFunc < subdomainBasisSizes_(subDomain)) {
          const int index       = subdomainBasisSizes_.head(subDomain).sum() + baseFunc;
          double    vectorEntry = vector(index);
          // first: coefficient for the parameter-independent part
          Theta_(k) = -1.0 * vectorEntry;
          k++;
          // now the parameter-dependent part
          for (int i = 0; i != QBh_; ++i) {
            assert(index < vector.rows());
            Theta_(k) = -1.0 * model_.mobility().coefficient(i, mu) * vectorEntry;
            k++;
          }
        }
      }
    }
    return;
  } // updateTheta

  double computeFactor(const ParameterType& mu) const
  {
    std::vector< double > ThetaOfMu;
    const int             Nlambda = model_.mobility().size();
    double                minimum = std::numeric_limits< double >::max();
    for (int i = 0; i != Nlambda; ++i) {
      ThetaOfMu.push_back(model_.mobility().coefficient(i, mu));
    }

    for (int i = 0; i != Nlambda; ++i) {
      double quotient = ThetaOfMu[i] / model_.mobility().coefficient(i, muBar_);
      assert(quotient >= 0);
      if (quotient < minimum)
        minimum = quotient;
    }

    assert(minimum > 0.0);

    return 1.0 / (minimum);
  } // computeFactor

  double computeOtherFactor(const ParameterType& mu) const
  {
    std::vector< double > ThetaOfMu;
    const int             Nlambda = model_.mobility().size();
    double                maximum = std::numeric_limits< double >::min();
    for (int i = 0; i != Nlambda; ++i) {
      ThetaOfMu.push_back(model_.mobility().coefficient(i, mu));
    }

    for (int i = 0; i != Nlambda; ++i) {
      double quotient = ThetaOfMu[i] / model_.mobility().coefficient(i, muBar_);
      assert(quotient >= 0);
      if (quotient > maximum)
        maximum = quotient;
    }
    assert(maximum > 0.0);

    return 1.0 / (maximum);
  } // computeOtherFactor

private:
   DofMatrixType   G_;
   ParameterType   muBar_;
   DofVectorType   subdomainBasisSizes_;
  const ModelType&      model_;
  const int             QLh_;
  const int             QBh_;
  mutable DofVectorType Theta_;
}; // class Interface
} // namespace Connector
} // namespace Estimator
} // namespace Reduced
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_ESTIMATOR_CONNECTOR_RESIDUAL_HH__
