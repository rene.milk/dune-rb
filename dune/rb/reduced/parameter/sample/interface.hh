#ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_INTERFACE_HH__
#define __DUNE_RB_REDUCED_PARAMETER_SAMPLE_INTERFACE_HH__

namespace Dune{

namespace RB{

namespace Reduced{

namespace Parameter{

namespace Sample{

template< class ParameterImp >
class Interface
{
public:
  typedef ParameterImp ParameterType;

    const ParameterType& min() const;

    const ParameterType& max() const;

    const int size() const;

    ParameterType get(const int i) const;

}; // class Interface

} // namespace Sample

} // namespace Parameter

} // namespace Reduced

} // namespace RB

} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_INTERFACE_HH__