#ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_UNIFORM_HH__
#define __DUNE_RB_REDUCED_PARAMETER_SAMPLE_UNIFORM_HH__

#include <dune/rb/reduced/parameter/sample/interface.hh>

namespace Dune {
namespace RB {
namespace Reduced {
namespace Parameter {
namespace Sample {
template< class ParamImp, class DimensionVectorImp >
  class Uniform : public Interface<ParamImp>
{
  typedef ParamImp           ParameterType;
  typedef DimensionVectorImp DimensionVectorType;

public:
  Uniform(const ParameterType& begin,
          const ParameterType& end,
          const DimensionVectorType& numParams)
    : begin_(begin),
      end_(end),
      numParams_(numParams),
      parameterDimension_(begin.size()),
      dimensionBase_(ParameterType::Ones(parameterDimension_))
  {
    // compute size of this parameter space
    size_ = 1.0;
    for (int i = 0; i != parameterDimension_; ++i) {
      size_ *= numParams_[i];
    }

    for (int i = 1; i != parameterDimension_; ++i) {
      for (int j = 0; j != i; ++j) {
        dimensionBase_(i) *= numParams_[j];
      }
    }
  }

  Uniform(const ParameterType& begin,
          const ParameterType& end,
          const unsigned int size)
    : begin_(begin),
      end_(end),
      numParams_(begin.size(), size),
      parameterDimension_(begin.size()),
      dimensionBase_(ParameterType::Ones(parameterDimension_))
  {
    // compute size of this parameter space
    size_ = 1.0;
    for (int i = 0; i != parameterDimension_; ++i) {
      size_ *= numParams_[i];
    }

    for (int i = 1; i != parameterDimension_; ++i) {
      for (int j = 0; j != i; ++j) {
        dimensionBase_(i) *= numParams_[j];
      }
    }
  }

  const int size() const
  {
    return size_;
  }

  ParameterType get(const int i) const
  {
    assert(i >= 0 && i < size());
    ParameterType ret = ParameterType::Zero(parameterDimension_);
    for (int comp = 0; comp != parameterDimension_; ++comp) {
      int numberInDim = int(std::ceil(i / dimensionBase_(comp))) % numParams_[comp];
      assert(numberInDim < numParams_[comp]);
      ret(comp) = (end_(comp) - begin_(comp)) * numberInDim / numParams_[comp] + begin_(comp);
    }
  } // get

  const ParameterType& min() const
  {
    return begin_;
  }

  const ParameterType& max() const
  {
    return end_;
  }

protected:
  const ParameterType begin_;
  const ParameterType end_;
  const DimensionVectorType     numParams_;
  const int           parameterDimension_;
  ParameterType       dimensionBase_;
  int                 size_;
};
} // namespace Sample
} // namespace Parameter
} // namespace Reduced
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_UNIFORM_HH__