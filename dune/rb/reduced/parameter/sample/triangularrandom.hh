#ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_TRIANGULARRANDOM_HH__
#define __DUNE_RB_REDUCED_PARAMETER_SAMPLE_TRIANGULARRANDOM_HH__

#include <dune/rb/reduced/parameter/sample/uniform.hh>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

namespace Dune {
namespace RB {
namespace Reduced {
namespace Parameter {
namespace Sample {
template< class ParameterImp >
class TriangularRandom
  : public Uniform< ParameterImp, std::vector< int > >
{
  typedef Uniform< ParameterImp, std::vector< int > > BaseType;
  typedef ParameterImp                                ParameterType;

public:
  TriangularRandom(const ParameterType& begin,
                   const ParameterType& end,
                   const unsigned int size)
    : BaseType(begin, end, size),
      parameters_(size, ParameterType::Zero(begin.rows()))
  {
    assert(begin.rows() == end.rows());

    const int parSize = begin.size();
    for (int parEntry = 0; parEntry != parSize; ++parEntry) {
      // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
      // Define a base random number generator and initialize it with a seed.

      uint64_t      seed;
      std::ifstream urandom;
      urandom.open("/dev/urandom");
      urandom.read(reinterpret_cast< char* >(&seed), sizeof(seed));
      urandom.close();
      boost::minstd_rand baseGen(seed);
      // Define distribution U[begin(parEntry), end(parEntry)) [double values]
      boost::uniform_real< > uniDblUnit(begin(parEntry), end(parEntry));
      // Define a random variate generator using our base generator and distribution
      boost::variate_generator< boost::minstd_rand&, boost::uniform_real< > > uniDblGen(baseGen, uniDblUnit);

      for (int i = 0; i != size; ++i) {
        parameters_[i](parEntry) = uniDblGen();
      }
    }
    // scale all parameters down to have one-norm 1.
    for (int i = 0; i != size; ++i) {
      parameters_[i] /= parameters_[i].sum();
    }
  }


  TriangularRandom(const ParameterType& begin,
                   const ParameterType& end,
                   const unsigned int size,
                   const uint64_t seed)
    : BaseType(begin, end, size),
      parameters_(size, ParameterType::Zero(begin.rows()))
  {
    assert(begin.rows() == end.rows());

    const int parSize = begin.size();
    for (int parEntry = 0; parEntry != parSize; ++parEntry) {
      // boost random generator stuff taken from http://www.mitchr.me/SS/exampleCode/boost/boostRandEx.cpp.html
      // Define a base random number generator and initialize it with a seed.
      boost::minstd_rand baseGen((parEntry + 1) * seed);
      // Define distribution U[begin(parEntry), end(parEntry)) [double values]
      boost::uniform_real< > uniDblUnit(begin(parEntry), end(parEntry));
      // Define a random variate generator using our base generator and distribution
      boost::variate_generator< boost::minstd_rand&, boost::uniform_real< > > uniDblGen(baseGen, uniDblUnit);

      for (int i = 0; i != size; ++i) {
        parameters_[i](parEntry) = uniDblGen();
      }
    }
    // scale all parameters down to have one-norm 1.
    for (int i = 0; i != size; ++i) {
      parameters_[i] /= parameters_[i].sum();
    }
  }

  const int size() const
  {
    return parameters_.size();
  }

  ParameterType get(const int i) const
  {
    assert(i < this->size());
    return parameters_[i];
  }
 
  
protected:
  std::vector< ParameterType > parameters_;
};
} // namespace Sample
} // namespace Parameter
} // namespace Reduced
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_PARAMETER_SAMPLE_TRIANGULARRANDOM_HH__