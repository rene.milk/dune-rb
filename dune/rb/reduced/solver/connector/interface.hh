namespace Dune{
  
  namespace RB{
    
    namespace Reduced{
      
      namespace Solver{
        
        namespace Connector{
          
          template< class ModelImp >
          class Interface
        public:
          typedef ModelImp   ModelType;
          
          typedef MatrixContainerImp    MatrixContainerType;
          
          typedef VectorContainerImp    VectorContainerType;
          
          typedef DetailedDiscretizationMapper    MapperType;
          
          const ModelType& model() const
          
          void init()
          
          template< class ParamType >
          VectorContainerType solve(const ParamType& param)
          
          void update()
          
          const MapperType& mapper() const
          
        }; // class Interface
        
      } // namespace Connector
      
    } // namespace Solver
    
  } // namespace Reduced
  
} // namespace RB

} // namespace Dune
