#ifndef __DUNE_RB_REDUCED_SOLVER_CONNECTOR_TRIVIAL_HH__
#define __DUNE_RB_REDUCED_SOLVER_CONNECTOR_TRIVIAL_HH__

#include <Eigen/Dense>
// #include <Eigen/SuperLUSupport>

namespace Dune {
namespace RB {
namespace Reduced {
namespace Solver {
namespace Connector {
template< class ModelImp, class MatrixImp, class VectorImp >
class Trivial
{
public:
  typedef ModelImp                           ModelType;
  typedef MatrixImp                          MatrixType;
  typedef VectorImp                          VectorType;
  typedef typename VectorType::ComponentType DofVectorType;
  typedef typename ModelType::ParameterType  ParameterType;

  Trivial(ModelType& model)
  : model_(model)
  {}

  void init() {}

  const ModelType& model() const
  {
    return model_;
  }

#if 0
  void update()
  const MapperType &mapper() const
  typedef DetailedDiscretizationMapper MapperType;
#endif // if 0

  DofVectorType solve(const ParameterType& param)
  {
    return DofVectorType();
  } // solve


  bool save(const std::string& path) const
  {
    return true;
  } // save

  bool load(const std::string& path)
  {
    return true;
  } // load

private:
  const ModelType& model_;
}; // class Trivial
} // namespace Connector
} // namespace Solver
} // namespace Reduced
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_SOLVER_CONNECTOR_TRIVIAL_HH__