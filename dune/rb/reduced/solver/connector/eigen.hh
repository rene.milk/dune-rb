#ifndef __DUNE_RB_REDUCED_SOLVER_CONNECTOR_EIGEN_HH__
#define __DUNE_RB_REDUCED_SOLVER_CONNECTOR_EIGEN_HH__

#include <Eigen/Dense>
// #include <Eigen/SuperLUSupport>

#include <boost/filesystem.hpp>

namespace Dune {
namespace RB {
namespace Reduced {
namespace Solver {
namespace Connector {
template< class ModelImp, class MatrixImp, class VectorImp >
class EigenBased
{
public:
  typedef ModelImp                           ModelType;
  typedef MatrixImp                          MatrixType;
  typedef VectorImp                          VectorType;
  typedef typename VectorType::ComponentType DofVectorType;
  typedef typename ModelType::ParameterType  ParameterType;

  EigenBased(const ModelType& model, const MatrixType& systemMatrix, const VectorType& rhs)
    : model_(model),
      systemMatrix_(systemMatrix),
      rhs_(rhs)
  {}

  EigenBased(const ModelType& model)
    : model_(model),
      systemMatrix_(0, 0),
      rhs_(0)
  {}


  void init() {}

  const ModelType& model() const
  {
    return model_;
  }

#if 0
  void update()
  const MapperType &mapper() const
  typedef DetailedDiscretizationMapper MapperType;
#endif // if 0

  DofVectorType solve(const ParameterType& param)
  {
    assert(systemMatrix_.rows() != 0 && systemMatrix_.cols() != 0
           && rhs_.cols() != 0
           && "Reduced solver connector is not properly initialized, please"
           && " use either EigenBased::EigenBased(model, systemMatrix, rhs) or EigenBase::load!");

    DofVectorType p;

    // Eigen::SuperLU< typename MatrixType::CompleteType > solver(systemMatrix_.complete(param));
    // p = solver.solve(rhs_.complete(param));
    // p = systemMatrix_.complete(param).jacobiSvd(Eigen::ComputeThinU |
    //Eigen::ComputeThinV).solve(rhs_.complete(param));
    // typename MatrixType::CompleteType sysComplete = systemMatrix_.complete(param);
    // typename VectorType::CompleteType rhsComplete = rhs_.complete(param);
    p = systemMatrix_.complete(param).fullPivLu().solve(rhs_.complete(param));


//    Eigen::SelfAdjointEigenSolver< Eigen::MatrixXd > eigensolver(systemMatrix_.complete(param));
//    std::cout << "Condition number of reduced matrix: "
//              << eigensolver.eigenvalues() (systemMatrix_.complete(param).rows() - 1) / eigensolver.eigenvalues() (0)
//              << "\n";

    return p;
  } // solve


  bool save(const std::string& path) const
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Reduced::Solver::Connector::save is not a directory, not saving!\n";
      return false;
    }
    boost::filesystem::create_directories(path + "/reduced_solver/system_matrix/");
    boost::filesystem::create_directories(path + "/reduced_solver/rhs/");
    return systemMatrix_.save(path + "/reduced_solver/system_matrix/") && rhs_.save(path + "/reduced_solver/rhs/");
  } // save

  bool load(const std::string& path)
  {
    boost::filesystem::path datapath(path);

    if (!boost::filesystem::is_directory(datapath)) {
      std::cerr << "Path given to Reduced::Solver::Connector::load is not a directory, not loading!\n";
      return false;
    }
    return systemMatrix_.load(path + "/reduced_solver/system_matrix/")
           && rhs_.load(path + "/reduced_solver/rhs/");
  } // load

private:
  const ModelType& model_;
  MatrixType       systemMatrix_;
  VectorType       rhs_;
}; // class Eigen
} // namespace Connector
} // namespace Solver
} // namespace Reduced
} // namespace RB
} // namespace Dune

#endif // #ifndef __DUNE_RB_REDUCED_SOLVER_CONNECTOR_EIGEN_HH__