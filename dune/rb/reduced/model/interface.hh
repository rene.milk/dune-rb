
namespace Dune{

namespace RB{

namespace Reduced{

namespace Model{

template< class ParamImp >
class Interface
{
public:
  typedef ParamImp
    ParamType;

  const ParamType& minParam() const

  const ParamType& maxParam() const

}; // class Interface

} // namespace Model

} // namespace Reduced

} // namespace RB

} // namespace Dune
