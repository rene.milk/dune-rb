
namespace Dune{

namespace RB{

namespace Reduced{

namespace Mapper{

class Interface
{
public:
  typedef CoarseGridImp
    GridType;

  const GridType& grid() const

  const int size() const

  const int size(const int Element) const

  const int global(const int Element, const int localDofId)

}; // class Interface

} // namespace Mapper

} // namespace Reduced

} // namespace RB

} // namespace Dune
