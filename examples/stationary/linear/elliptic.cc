#include <boost/filesystem.hpp>

#include "elliptic/scalar.cc"

int main(int argc, char* argv[])
{
  try
  {
    // we simulate an interactive pymor session here
    // which is why we write stuff to disc and read it afterwards

    // (i.a) choose the example (type, grid and dimension)
    typedef StationaryLinearEllipticScalar2dSgrid ExampleType;
    const std::string filename = ExampleType::id();

    // (i.b) write the main description file (if none is there)
    const std::string mainDescriptionFilename = filename + ".description";
    if (!boost::filesystem::exists(mainDescriptionFilename))
      if (!ExampleType::writeMainDescriptionFile(mainDescriptionFilename, true))
        DUNE_THROW(Dune::IOError, Dune::Stuff::Common::colorStringRed("ERROR:")
                   << " could not write to '" << mainDescriptionFilename << "'!");

    // (i.c) now the user is supposed to edit the description file
    //       and we can create the example, which fixes everything according to the description file
    ExampleType example(Dune::Stuff::Common::String::mainArgsToVector(argc, argv), mainDescriptionFilename);

    // (ii.a) now we can write the description files
    //        * for the grid
    const std::string gridDescriptionFilename = filename + ".grid.description";
    if (!boost::filesystem::exists(gridDescriptionFilename))
      if (!example.writeGridDescriptionFile(gridDescriptionFilename))
        DUNE_THROW(Dune::IOError, Dune::Stuff::Common::colorStringRed("ERROR:")
                   << " could not write to '" << gridDescriptionFilename << "'!");
    //        * for the boundary info
    const std::string boundaryInfoDescriptionFilename = filename + ".boundaryinfo.description";
    if (!boost::filesystem::exists(boundaryInfoDescriptionFilename))
      if (!example.writeBoundaryInfoDescriptionFile(boundaryInfoDescriptionFilename))
        DUNE_THROW(Dune::IOError, Dune::Stuff::Common::colorStringRed("ERROR:")
                   << " could not write to '" << boundaryInfoDescriptionFilename << "'!");
    //        * for the model
    const std::string modelDescriptionFilename = filename + ".model.description";
    if (!boost::filesystem::exists(modelDescriptionFilename))
      if (!example.writeModelDescriptionFile(modelDescriptionFilename))
        DUNE_THROW(Dune::IOError, Dune::Stuff::Common::colorStringRed("ERROR:")
                   << " could not write to '" << modelDescriptionFilename << "'!");
    //        * for the discretizations
    const std::string discretizationDescriptionFilename = filename + ".discretization.description";
    if (!boost::filesystem::exists(discretizationDescriptionFilename))
      if (!example.writeDiscretizationsDescriptionFile(discretizationDescriptionFilename))
        DUNE_THROW(Dune::IOError, Dune::Stuff::Common::colorStringRed("ERROR:")
                   << " could not write to '" << discretizationDescriptionFilename << "'!");

  } catch(Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e.what() << std::endl;
  } catch(std::exception& e) {
    std::cerr << e.what() << std::endl;
  } catch(...) {
    std::cerr << "Unknown exception thrown!" << std::endl;
  } // try

  // if we came that far we can as well be happy about it
  return 0;
} // ... main(...)
