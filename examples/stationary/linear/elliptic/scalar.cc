#ifdef HAVE_CMAKE_CONFIG
  #include "cmake_config.h"
#elif defined (HAVE_CONFIG_H)
  #include "config.h"
#endif // ifdef HAVE_CMAKE_CONFIG

#include <ostream>
#include <string>
#include <vector>

#include <dune/common/exceptions.hh>

#include <dune/grid/sgrid.hh>

#include <dune/grid/part/leaf.hh>

#include <dune/fem/misc/mpimanager.hh>

#include <dune/stuff/grid/provider.hh>
#include <dune/stuff/common/color.hh>
#include <dune/stuff/grid/boundaryinfo.hh>
#include <dune/stuff/common/string.hh>
#include <dune/stuff/common/parameter/tree.hh>
#include <dune/stuff/la/solver.hh>

#include <dune/detailed/solvers/stationary/linear/elliptic/model.hh>
#include <dune/rb/solver/stationary/linear/detailed/eliptic.hh>


// some aliases
namespace CommonStuff = Dune::Stuff::Common;
namespace GridProvider = Dune::Stuff::Grid::Provider;
namespace BoundaryInfo = Dune::Stuff::Grid::BoundaryInfo;
namespace Model = Dune::Detailed::Solvers::Stationary::Linear::Elliptic::Model;
namespace Discretization = Dune::RB::Solver::Stationary::Linear::Detailed::Elliptic;


class StationaryLinearEllipticScalar2dSgrid
{
public:
  typedef double    DomainFieldType;
  static const int  dimDomain = 2;
  typedef double    RangeFieldType;
  static const int  dimRange = 1;

  typedef Dune::SGrid< dimDomain, dimDomain, DomainFieldType >  GridType;
  typedef Dune::grid::Part::Leaf::Const< GridType >             GridPartType;
  typedef typename GridPartType::GridViewType                   GridViewType;
  typedef GridProvider::Interface< GridType >                   GridProviderType;

  typedef BoundaryInfo::Interface< GridViewType >                                   BoundaryInfoType;
  typedef Model::Interface< DomainFieldType, dimDomain, RangeFieldType, dimRange >  ModelType;
  typedef Discretization::Interface< GridPartType, RangeFieldType, dimRange >       DiscretizationType;

  typedef Dune::Stuff::Common::ExtendedParameterTree  DescriptionType;

  static std::string id()
  {
    return "stationary.linear.elliptic";
  }

  static bool writeMainDescriptionFile(const std::string filename = id() + ".description",
                                       const bool verbose = true,
                                       const std::string prefix = "")
  {
    if (verbose)
      std::cout << prefix << "writing main description file '" << filename << "'... " << std::flush;
    std::ofstream descriptionFile(filename);
    if (descriptionFile.is_open()) {
      // header
      descriptionFile << "# main description file for example '" << id() << "'" << std::endl;
      // grid
      writeValuesForOneKey(descriptionFile, GridProvider::types(), "grid", "Dune::Stuff::Grid::Provider");
      // boundaryinfo
      writeValuesForOneKey(descriptionFile, BoundaryInfo::types(), "boundaryinfo", "Dune::Stuff::Grid::BoundaryInfo");
      // model
      writeValuesForOneKey(descriptionFile, Model::types(), "model",
                           "Dune::Detailed::Solvers::Stationary::Linear::Elliptic::Model");
      // discretization
      writeValuesForOneKey(descriptionFile, Discretization::types(), "discretization",
                           "Dune::RB::Solver::Stationary::Linear::Detailed::Elliptic");
      // logging
      descriptionFile << "[logging]" << std::endl;
      descriptionFile << "info  = true" << std::endl;
      descriptionFile << "debug = true" << std::endl;
      descriptionFile << "file  = false" << std::endl;

      // if we came that far, all should be well
      if (verbose)
        std::cout << " done" << std::endl;
      return true;
    } else { // if (descriptionFile.is_open())
      if (verbose)
        std::cout << "failed" << std::endl;
      return false;
    } // if (descriptionFile.is_open())
  } // ... writeMainDescriptionFile(...)

  StationaryLinearEllipticScalar2dSgrid(const std::vector< std::string >& args,
                                        const std::string descriptionFilename = id() + ".description")
    : debugLogging_(false)
  {
    // read the description file
    const DescriptionType description(descriptionFilename);
    // * logging
    debugLogging_ = description.hasKey("logging.debug") ? description.get< bool >("logging.debug") : false;
    int logFlags = CommonStuff::LOG_CONSOLE;
    if (debugLogging_)
      logFlags = logFlags | CommonStuff::LOG_DEBUG;
    if (description.hasKey("logging.info") ? description.get< bool >("logging.info") : false)
      logFlags = logFlags | CommonStuff::LOG_INFO;
    if (description.hasKey("logging.file") ? description.get< bool >("logging.file") : false)
      logFlags = logFlags | CommonStuff::LOG_FILE;
    Dune::Stuff::Common::Logger().create(logFlags, id(), "", "");
    Dune::Stuff::Common::LogStream& debug = Dune::Stuff::Common::Logger().debug();
    // * initialize the Dune::Fem MPIManager
    //  this is a little tricky, since we may only do this once, but he does not give us a hint if he has alredy been
    //  called
    try {
      int argc = args.size();
      char** argv = CommonStuff::String::vectorToMainArgs(args);
      Dune::MPIManager::initialize(argc, argv);
    } catch (...) {
      debug << CommonStuff::colorString("WARNING:") << " the Dune::Fem::MPIManager reported a problem!" << std::endl;
    } // initialize the Dune::Fem MPIManager
    // * rest
    gridType_           = description.get< std::string >("grid");
    boundaryinfoType_   = description.get< std::string >("boundaryinfo");
    modelType_          = description.get< std::string >("model");
    discretizationType_ = description.get< std::string >("discretization");
  } // StationaryLinearEllipticScalar2dSgrid(...)

  bool writeGridDescriptionFile(const std::string filename = id() + ".grid.description",
                                const std::string prefix = "")
  {
    std::ostream& debug = CommonStuff::Logger().debug();
    debug << prefix << "writing grid description file '" << filename << "'... " << std::flush;
    std::ofstream descriptionFile(filename);
    if (descriptionFile.is_open()) {
      // header
      descriptionFile << "# description file for grid provider '" << gridType_ << "'" << std::endl;
      const DescriptionType gridDescription = GridProvider::createSampleDescription(gridType_);
      gridDescription.report(descriptionFile);
      debug << "done" << std::endl;
      return true;
    } else {
      debug << "failed" << std::endl;
      return false;
    }
  } // ... writeGridDescriptionFile(...)

  bool writeBoundaryInfoDescriptionFile(const std::string filename = id() + ".boundaryinfo.description",
                                        const std::string prefix = "")
  {
    std::ostream& debug = CommonStuff::Logger().debug();
    debug << prefix << "writing boundaryinfo description file '" << filename << "'... " << std::flush;
    std::ofstream descriptionFile(filename);
    if (descriptionFile.is_open()) {
      // header
      descriptionFile << "# description file for boundary info '" << boundaryinfoType_ << "'" << std::endl;
      const DescriptionType boundaryInfoDescription
          = BoundaryInfo::createSampleDescription< GridViewType >(boundaryinfoType_);
      boundaryInfoDescription.report(descriptionFile);
      debug << "done" << std::endl;
      return true;
    } else {
      debug << "failed" << std::endl;
      return false;
    }
  } // ... writeBoundaryInfoDescriptionFile(...)

  bool writeModelDescriptionFile(const std::string filename = id() + ".model.description",
                                        const std::string prefix = "")
  {
    std::ostream& debug = CommonStuff::Logger().debug();
    debug << prefix << "writing model description file '" << filename << "'... " << std::flush;
    std::ofstream descriptionFile(filename);
    if (descriptionFile.is_open()) {
      // header
      descriptionFile << "# description file for model '" << modelType_ << "'" << std::endl;
      const DescriptionType modelDescription
          = Model::createSampleDescription< DomainFieldType, dimDomain, RangeFieldType, dimRange >(modelType_);
      modelDescription.report(descriptionFile);
      debug << "done" << std::endl;
      return true;
    } else {
      debug << "failed" << std::endl;
      return false;
    }
  } // ... writeModelDescriptionFile(...)

  bool writeDiscretizationsDescriptionFile(const std::string filename = id() + ".discretization.description",
                                        const std::string prefix = "")
  {
    std::ostream& debug = CommonStuff::Logger().debug();
    debug << prefix << "writing discretization description file '" << filename << "'... " << std::flush;
    std::ofstream descriptionFile(filename);
    if (descriptionFile.is_open()) {
      // header
      descriptionFile << "# description file for discretizations '" << discretizationType_ << "'" << std::endl;
      const DescriptionType discretizationDescription
          = Discretization::createSampleDescription< GridPartType, RangeFieldType, dimRange >(discretizationType_);
      discretizationDescription.report(descriptionFile);
      // we also append the linear solver description
      const std::vector< std::string > linearSolverTypes = Dune::Stuff::LA::Solver::types();
      if (linearSolverTypes.size() == 0)
        DUNE_THROW(Dune::InvalidStateException,
                   "\n" << Dune::Stuff::Common::colorStringRed("ERROR:") << " no 'Dune::Stuff::LA::Solver' available!");
      descriptionFile << "[solver.linear]" << std::endl;
      descriptionFile << "type = " << linearSolverTypes[0] << std::endl;
      for (size_t ii = 1; ii < linearSolverTypes.size(); ++ii)
        descriptionFile << "       " << linearSolverTypes[ii] << std::endl;
      descriptionFile << "maxIter   = 5000" << std::endl;
      descriptionFile << "precision = 1e-12" << std::endl;
      debug << "done" << std::endl;
      return true;
    } else {
      debug << "failed" << std::endl;
      return false;
    }
  } // ... writeDiscretizationsDescriptionFile(...)

private:
  static void writeValuesForOneKey(std::ostream& outFile,
                                   const std::vector< std::string >& values,
                                   const std::string& key,
                                   const std::string& errorId)
  {
    if (values.size() == 0)
      DUNE_THROW(Dune::InvalidStateException,
                 "\n" << CommonStuff::colorStringRed("ERROR:")
                 << " no " << errorId << " available!");
    outFile << key << " = " << values[0] << std::endl;
    const std::string whitespaces = CommonStuff::whitespaceify(key + " = ");
    for (size_t ii = 1; ii < values.size(); ++ii)
      outFile << whitespaces << values[ii] << std::endl;
  } // ... writeValuesForOneKey(...)

  bool debugLogging_;
  std::string gridType_;
  std::string boundaryinfoType_;
  std::string modelType_;
  std::string discretizationType_;
}; // class StationaryLinearEllipticScalar2dSgrid
