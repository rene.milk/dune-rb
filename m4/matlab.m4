dnl  ====================================================
dnl             MATLAB library macros
dnl  ====================================================

dnl
dnl  CHECK_MATLAB - find Matlab libraries
dnl  ----------------------------------------------------
AC_DEFUN([CHECK_MATLAB],
[
SED='sed'
AC_ARG_WITH(matlab,
            [AS_HELP_STRING([--with-matlab=DIR],
                           [the directory where Matlab is installed])
AS_HELP_STRING([--without-matlab],
                           [disable Matlab support])],
                           MATLAB=${withval},
                           MATLAB=)

dnl disabling MATLAB if set to no
AS_IF([test "x$MATLAB" != xno],[

dnl checking for mex in $MATLAB:$PATH
AC_PATH_PROG([MEX_IN_PATH],[mex],
             [],
             [$MATLAB/bin$PATH_SEPARATOR$PATH])
])
dnl extracting matlab_arch and realfilepath
AS_IF([test "x$MEX_IN_PATH" != x],
      [
        TEMP_MEX_OUTPUT=`mex -v 2>&1`
        extract_compile_flags()
        {
        $MEX_IN_PATH -v 2>&1 | \
           $SED -n "/\<${1}\>\s*=/ p" | \
           $SED "s/^.*= //"
        }
        dnl get the variable MATLAB from mex output
        MATLAB=`extract_compile_flags 'MATLAB'`
        MATLAB_DIR=$MATLAB
        MATLAB_BIN_DIR=$MATLAB/bin
       ],
       [MATLAB=])
AS_IF([test "x$MATLAB" != x],[
       AC_MSG_CHECKING(for Matlab software)
       dnl running the gccopts.sh in MATLAB_BIN_DIR
       dnl and extracting CXXFLAGS, LDFLAGS, LDADD
       cc_mat_ver_check()
       {
          echo ${1} | $SED -ne "/^gcc-${2}/p"
       }
       case $build_os in
       *linux*|*darwin*|*sol*)
               AC_MSG_CHECKING([for gnu compiler compatible with matlab version])
               MATVER=`$MATLAB/bin/matlab -nojvm -nosplash -r "disp(version('-release')); exit" 2>/dev/null | tail -2`
               AC_MSG_NOTICE(["R$MATVER"])
               dnl The following data is received from the Mathworks compiler compatiblity pages
               dnl e.g. http://www.mathworks.de/support/compilers/R2011b/glnxa64.html
               case $MATVER in
                    *2012a*)
                      REQ_CXX_MAJOR=4
                      REQ_CXX_MINOR=4
                      REQ_CXX_REL='*'
                      REQ_CXX_VER='4.4'
                      ;;
                    *2011*)
                      REQ_CXX_MAJOR=4
                      REQ_CXX_MINOR=3
                      REQ_CXX_REL='*'
                      REQ_CXX_VER='4.3'
                      ;;
                    *2010*|*2009*)
                      REQ_CXX_MAJOR=4
                      REQ_CXX_MINOR=2
                      REQ_CXX_REL=3
                      REQ_CXX_VER='4.2.3'
                      ;;
                    *2008*)
                      REQ_CXX_MAJOR=4
                      REQ_CXX_MINOR=1
                      REQ_CXX_REL=1
                      REQ_CXX_VER='4.1.1'
                      ;;
                    *)
                      REQ_CXX_MAJOR=
                      REQ_CXX_MINOR=
                      REQ_CXX_REL=
                      REQ_CXX_VER='unsupported'
                      AC_MSG_WARN([Matlab version R$MATVER is unsupported])
                      ;;
               esac
               MATLAB_MEX_CC=
               MATLAB_MEX_CXX=
               for i in '' "-$REQ_CXX_MAJOR" "-$REQ_CXX_MAJOR.$REQ_CXX_MINOR" "-$REQ_CXX_MAJOR.$REQ_CXX_MINOR.$REQ_CXX_REL";
               do
                  for path in /usr/bin /usr/local/bin
                  do
                    CC_CAND=`which $path/gcc$i`
                    CXX_CAND=`which $path/g++$i`
                    if test "x$CC_CAND" != "x"
                    then
                      CC_VER=`$CC_CAND --version | head -1`
                      VER_MATCH=`cc_mat_ver_check "$CC_VER" $REQ_CXX_VER`
                      if test "z$VER_MATCH" != "z"
                      then
                          MATLAB_MEX_CC=$CC_CAND
                          MATLAB_MEX_CXX=$CXX_CAND
                          break;
                      fi
                    fi
                  done
               done
               if test "z$MATLAB_MEX_CC" != "z"
               then
                 AC_MSG_RESULT(["found ($MATLAB_MEX_CC)"])
               else
                 AC_MSG_RESULT(["not found ($REQ_CXX_VER) No mex files can be compiled!"])
               fi
               MATLAB_CPPFLAGS="-I$MATLAB/extern/include -I$MATLAB/simulink/include -DMATLAB_MEX_FILE"
               MATLAB_MEX_CXXFLAGS=`extract_compile_flags 'CXXFLAGS'`
               MATLAB_CXXFLAGS=`echo $MATLAB_MEX_CXXFLAGS | $SED 's/-fPIC//;'`
               MATLAB_MEX_LDFLAGS=`extract_compile_flags 'LDFLAGS'`
               MATLAB_LDFLAGS=`echo $MATLAB_MEX_LDFLAGS | $SED 's/-shared //'`
               MATLAB_MEX_LDADD=`extract_compile_flags 'CXXLIBS'`
               MATLAB_LDADD=`echo $MATLAB_MEX_LDADD | $SED 's/-rpath-link/-rpath/g'`
               MEXEXT=`extract_compile_flags 'LDEXTENSION'`
               ;;
       *mingw*)
               MATLAB_CPPFLAGS="-I$MATLAB/extern/include -I$MATLAB/simulink/include -DMATLAB_MEX_FILE"
               MATLAB_CXXFLAGS="$MATLAB_CPPFLAGS -fno-exceptions -DNDEBUG";
               MATLAB_LDFLAGS="-shared -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
               MATLAB_LDADD="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/win32,--rpath-link,${MATLAB_DIR}/bin/win32 ${MATLAB_DIR}/bin/win32/libmx.a ${MATLAB_DIR}/bin/win32/libmex.a ${MATLAB_DIR}/bin/win32/libmat.a -lm";
               MATLAB_LDFLAGS="-shared -L${MATLAB_DIR}/bin/win32 -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
               MATLAB_LDADD="-lmx -lmex -lmat -lm";
               MEXEXT=dll;
               if test ! -e "${MATLAB_DIR}/bin/win32/libmx.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmx=`dlltool -llibmx.a -d${MATLAB_DIR}/extern/include/libmx.def -Dlibmx.dll`
                 cd -
               fi
               if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmex=`dlltool -llibmex.a -d${MATLAB_DIR}/extern/include/libmex.def -Dlibmex.dll`
                 cd -
               fi
               if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmat=`dlltool -llibmat.a -d${MATLAB_DIR}/extern/include/libmat.def -Dlibmat.dll`
                 cd -
               fi
               ;;
       *cygwin*)
               MATLAB_CPPFLAGS="-I$MATLAB/extern/include -I$MATLAB/simulink/include -DMATLAB_MEX_FILE"
               MATLAB_CXXFLAGS="$MATLAB_CPPFLAGS -fno-exceptions -mno-cygwin -DNDEBUG";
               MATLAB_LDFLAGS="-shared -mno-cygwin -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
               MATLAB_LDADD="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/win32,--rpath-link,${MATLAB_DIR}/bin/win32 ${MATLAB_DIR}/bin/win32/libmx.a ${MATLAB_DIR}/bin/win32/libmex.a ${MATLAB_DIR}/bin/win32/libmat.a -lm";
               MATLAB_LDFLAGS="-shared -mno-cygwin -L${MATLAB_DIR}/bin/win32 -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
               MATLAB_LDADD="-lmx -lmex -lmat -lm";
               MEXEXT=dll;
               if test ! -e "${MATLAB_DIR}/bin/win32/libmx.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmx=`dlltool -llibmx.a -d${MATLAB_DIR}/extern/include/libmx.def -Dlibmx.dll`
                 cd -
               fi
               if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmex=`dlltool -llibmex.a -d${MATLAB_DIR}/extern/include/libmex.def -Dlibmex.dll`
                 cd -
               fi
               if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
               then
                 cd ${MATLAB_DIR}/bin/win32
                 libmat=`dlltool -llibmat.a -d${MATLAB_DIR}/extern/include/libmat.def -Dlibmat.dll`
                 cd -
               fi
               ;;
       esac])
AC_MSG_RESULT([MATLAB_LDADD=$MATLAB_LDADD MATLAB_LDFLAGS=$MATLAB_LDFLAGS])
AC_LANG_PUSH([C])
ac_save_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$CPPFLAGS $MATLAB_CPPFLAGS"
AC_CHECK_HEADER([mex.h],
  [HAVE_MATLAB="yes"],
  [HAVE_MATLAB="no"])

dnl if no compiler could be found, the matlab is still unusable...
AS_IF([test "x$MATLAB_MEX_CC" == "x"], [HAVE_MATLAB="no"])

CPPFLAGS="$ac_save_CPPFLAGS"
AC_LANG_POP
AS_IF([test "x$HAVE_MATLAB" != "xno"],[
      AC_DEFINE([HAVE_MATLAB],[ENABLE_MATLAB],[Define wether the Matlab mex interface is available.])
      AC_SUBST(MATLAB_DIR)
      AC_SUBST(MATLAB_CPPFLAGS)
      AC_SUBST(MATLAB_LDADD)
      AC_SUBST(MATLAB_MEX_LDADD)
      AC_SUBST(MATLAB_LDFLAGS)
      AC_SUBST(MATLAB_MEX_LDFLAGS)
      AC_SUBST(MATLAB_MEX_CC)
      AC_SUBST(MATLAB_MEX_CXX)
      AC_SUBST(MATLAB_CXXFLAGS)
      AC_SUBST(MATLAB_MEX_CXXFLAGS)
      AC_SUBST(MEXEXT)
      ])
AM_CONDITIONAL(HAVE_MATLAB,[test "x$HAVE_MATLAB" != "xno" && test "x$MATLAB_MEX_CC" != "x" ])
DUNE_ADD_SUMMARY_ENTRY([mex headers],[ $HAVE_MATLAB ])
])
