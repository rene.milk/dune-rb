AC_DEFUN([DUNE_RB_CHECKS],
[
  PKG_CHECK_MODULES([EIGEN],
                    [eigen3],
                    [AC_DEFINE([HAVE_EIGEN],
                               [1],
                               [Define wether the eigen includes were found.])])
  DUNE_CPPFLAGS="$DUNE_CPPFLAGS $EIGEN_CFLAGS"
])

AC_DEFUN([DUNE_RB_CHECK_MODULE],[
  DUNE_CHECK_MODULES([dune-rb],[dune/rb/exists.hh])
])
