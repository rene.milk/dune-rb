AC_DEFUN([LIBTOOL_REMOVE_RPATH],
[
# by Marcelo Magallon <mmagallo@efis.ucr.ac.cr>
# Turn around -rpath problem with libtool 1.0c
# This define should be improbable enough to not conflict with anything
case ${host} in
*-linux-gnu)
AC_MSG_RESULT([Fixing libtool for -rpath problems.])
  sed < libtool > libtool-2 \
      's/^hardcode_libdir_flag_spec.*$'/'hardcode_libdir_flag_spec=""/'
      mv libtool-2 libtool
      chmod 755 libtool
      ;;
esac
])
