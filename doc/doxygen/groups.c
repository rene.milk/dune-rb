/** @defgroup decomposed Affinely decomposed operators and functions
 */

/** @defgroup discretization Discretization Operators
 */

/** @defgroup fv Finite volume schemes for linear non-stationary advection-diffusion equations
 * @ingroup discretization
 */

/** @defgroup datafunc Parametrized data functions
 */

/** @defgroup rbmatlab Communication interface to RBmatlab
 */

/** @defgroup serialization Serialization of mxArrays
 * @ingroup rbmatlab
 */

/** @defgroup rbsocks Generic RBmatlab server
 * @ingroup rbmatlab
 */

/** @defgroup linevol Linear evolution equations
 */

/** @defgroup DiscFuncList Discrete function lists
 */

/** @defgroup examples Examples
 */

/** @defgroup models Models
 * @ingroup examples
 */

/** @defgroup tests Tests
 *  @ingroup fv
 */
