/** @mainpage dune-rb module
 * @section intro Introduction
 *
 * dune-rb is a DUNE module providing routines for affinely decomposed and
 * parameter dependent Finite Volume discretizations, which can be used for a
 * reduced basis generation with high-level packages like e.g. RBmatlab.
 *
 * In detail, the module includes:
 *
 * -# @ref decomposed
 *  "Classes for affinely decomposed operators and functions"
 * -# @ref DiscFuncList
 * -# @ref discretization "Implementations of discretization operators" for
 *    - @ref fv
 *    .
 * -# @ref datafunc "Parametrized data functions for initial, boundary and model data"
 * -# @ref rbmatlab "A communication interface to RBmatlab"
 *    - with abstract classes for
 *      - @ref serialization and
 *      - @ref rbsocks "Network resp. mex interface communication"
 *      .
 *    - and generic implementations for
 *      - @ref linevol
 *      .
 *    .
 * -# Examples
 *
 * @section learning How To learn to use dune-rb
 *
 * @section Prospect
 */
